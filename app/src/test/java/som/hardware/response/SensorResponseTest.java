package som.hardware.response;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class SensorResponseTest {

    SensorResponse sensorResponse;
    int timeTaken = 1000;
    boolean isSuccess = true;

    @Before
    public void setUp() throws Exception {

    }

    @Test(expected = UnsupportedOperationException.class)
    public void getSensorData_readOnlyBehavior() {
        final Map<String, List<Double>> sensorReading = new HashMap<>();
        this.sensorResponse = new SensorResponse(this.timeTaken, this.isSuccess, sensorReading);
        final Map<String, List<Double>> sensorData = this.sensorResponse.getSensorData();
        sensorData.put("dummySensor", new ArrayList<>(Arrays.asList(1.3, 1.5)));

    }
}