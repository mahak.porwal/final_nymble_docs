package som.hardware.response;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class HWActionResponseTest {

    HWActionResponse hwActionResponse;
    int dummyTimeTaken = 1000;
    boolean isSuccess = true;

    @Before
    public void setUp() throws Exception {
        this.hwActionResponse = new HWActionResponse(this.dummyTimeTaken, this.isSuccess, null);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getActionResponseData_readOnlyBehavior() {
        final Map<String, String> parametersMap = new HashMap<>();
        this.hwActionResponse = new HWActionResponse(this.dummyTimeTaken, this.isSuccess, parametersMap);
        this.hwActionResponse.getActionResponseData().put("dummy_key", "dummy_value");
    }

}