package som.hardware.message;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Map;

import static org.junit.Assert.*;

public class HWMessageTest {

    HWMessage hwMessage;
    String testKey = "test_key";
    String testValue = "test_value";

    @Before
    public void setUp() throws Exception {
        this.hwMessage = Mockito.mock(HWMessage.class, Mockito.CALLS_REAL_METHODS);
    }

    @Test
    public void setEntryParametersMap_MapInsertion() {
        this.hwMessage.setEntryParametersMap(this.testKey, this.testValue);
        assertEquals("map insertion failed", this.testValue, this.hwMessage.getParametersMap().get(this.testKey));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getParametersMap_ReadOnlyBehavior() {
        this.hwMessage.setEntryParametersMap(this.testKey, this.testValue);
        final Map<String, String> map = this.hwMessage.getParametersMap();
        map.put("dummy_key", "dummy_value");
    }
}