package som.hardware.message.command;

import org.junit.Before;
import org.junit.Test;

import som.hardware.message.Component;
import som.hardware.message.HWMessageKeys;
import som.hardware.message.RequestType;


import static org.junit.Assert.*;

public class CommandRequestTest {

    CommandRequest commandRequest;
    double speed = 20.0;
    Component component;

    @Before
    public void setUp() throws Exception {
        this.component = Component.EXHAUST_FAN;
        this.commandRequest = new CommandRequest(this.component);
    }

    @Test
    public void setParameters_MapInsertion() {
        this.commandRequest.setParameters(CommandRequestParameters.SPEED, Double.toString(this.speed));
        assertEquals("incorrect value of key speed", this.speed,
                Double.parseDouble(this.commandRequest.getParametersMap().get(CommandRequestParameters.SPEED.getParameter())), 0.0001);
    }

    @Test
    public void constructorTest_RequestType() {
        assertEquals("incorrect request type",
                RequestType.COMMAND_REQUEST.getRequestType(), this.commandRequest.getParametersMap().get(HWMessageKeys.RequestType));
    }

    @Test
    public void constructorTest_ComponentType() {
        assertEquals("incorrect component",
                Component.EXHAUST_FAN.getComponent(), this.commandRequest.getParametersMap().get(HWMessageKeys.Component));
    }
}