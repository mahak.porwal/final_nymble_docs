package som.hardware.message.sensor;

import org.junit.Test;

import som.hardware.message.Component;


import static org.junit.Assert.*;

public class HWSensorRequestTest {

    HWSensorRequest hwSensorRequest;
    Component component = Component.MACRO_SERVO_1;
    SensorUnit sensorUnit = SensorUnit.MICRO_VOLTS;

    @Test
    public void constructorTest_MapInsertionNullSensorUnit() {
        this.hwSensorRequest = new HWSensorRequest(this.component, null);
        assertEquals("key inserted even with null value", false,
                this.hwSensorRequest.getParametersMap().containsKey
                        (SensorRequestParameters.SENSOR_UNIT.getSensorRequestParameter()));
    }


    @Test
    public void constructorTest_MapKeyInsertionSensorUnit() {
        this.hwSensorRequest = new HWSensorRequest(this.component, this.sensorUnit);
        assertEquals("sensor_unit key not inserted", true,
                this.hwSensorRequest.getParametersMap().
                        containsKey(SensorRequestParameters.SENSOR_UNIT.getSensorRequestParameter()));
    }

    @Test
    public void constructorTest_MapValueInsertionSensorUnit() {
        this.hwSensorRequest = new HWSensorRequest(this.component, this.sensorUnit);
        assertEquals("incorrect unit inserted", this.sensorUnit.getSensorUnit(),
                this.hwSensorRequest.getParametersMap()
                        .get(SensorRequestParameters.SENSOR_UNIT.getSensorRequestParameter()));
    }


    @Test
    public void constructorTest_MapKeyInsertionSensorName() {
        this.hwSensorRequest = new HWSensorRequest(this.component, null);
        assertEquals("sensor_name key not inserted", true,
                this.hwSensorRequest.getParametersMap().containsKey
                        (SensorRequestParameters.SENSOR_NAME.getSensorRequestParameter()));
    }

    @Test
    public void constructorTest_MapValueInsertionSensorName() {
        this.hwSensorRequest = new HWSensorRequest(this.component, null);
        assertEquals("sensor_name value incorrect", this.component.getComponent(),
                this.hwSensorRequest.getParametersMap()
                        .get(SensorRequestParameters.SENSOR_NAME.getSensorRequestParameter()));
    }
}