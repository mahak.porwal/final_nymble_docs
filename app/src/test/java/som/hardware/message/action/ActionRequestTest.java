package som.hardware.message.action;


import org.junit.Before;
import org.junit.Test;

import som.hardware.message.Component;
import som.hardware.message.HWMessageKeys;
import som.hardware.message.RequestType;
import som.hardware.message.action.ActionRequest;
import som.hardware.message.action.ActionRequestParameters;
import som.hardware.message.action.ActionType;


import static org.junit.Assert.*;

public class ActionRequestTest {

    ActionRequest actionRequest;
    double angle = 20.0;
    Component component;
    ActionType actionType;

    @Before
    public void setUp() throws Exception {
        this.component = Component.EXHAUST_FAN;
        this.actionType = ActionType.MICRO_ANTICLOCKWISE_DISPENSE;
        this.actionRequest = new ActionRequest(this.component, this.actionType);
    }

    @Test
    public void setActionParameters_MapInsertion_Angle() {
        this.actionRequest.setActionParameters(ActionRequestParameters.ANGLE, Double.toString(this.angle));
        assertEquals("double value equality failed", this.angle,
                Double.parseDouble(this.actionRequest.getParametersMap()
                        .get(ActionRequestParameters.ANGLE.getParameter())), 0.00001);
    }

    @Test
    public void constructorTest_ComponentType() {
        assertEquals("incorrect component", Component.EXHAUST_FAN.getComponent(),
                this.actionRequest.getParametersMap().get(HWMessageKeys.Component));
    }

    @Test
    public void constructorTest_RequestType() {
        assertEquals("incorrect request type", RequestType.ACTION_REQUEST.getRequestType(),
                this.actionRequest.getParametersMap().get(HWMessageKeys.RequestType));
    }

    @Test
    public void constructorTest_ActionType() {
        assertEquals("incorrect action type", this.actionType.getActionType(),
                this.actionRequest.getParametersMap().get(ActionRequestParameters.BEHAVIOR_TYPE.getParameter()));
    }
}