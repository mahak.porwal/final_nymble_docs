package som.hardware.request.builder.hwActionRequest;

import org.junit.Before;
import org.junit.Test;


import som.hardware.message.Component;
import som.hardware.message.HWMessage;
import som.hardware.message.HWMessageKeys;
import som.hardware.message.RequestType;

import som.hardware.request.HWActionRequest;
import som.hardware.request.HWRequest;
import som.hardware.request.HWRequestType;
import som.instruction.handler.InstructionHandler;

import som.instruction.request.heat.Heat;
import som.instruction.request.heat.HeatPowerLevel;
import som.instruction.request.visionCamera.VisionCamera;

import static org.junit.Assert.*;

public class HWActionRequestBuilderTest {

    HWActionRequestBuilder hwActionRequestBuilder;
    final InstructionHandler instructionHandler = new InstructionHandler();
    final VisionCamera visionCamera = new VisionCamera(this.instructionHandler);

    @Before
    public void setUp() throws Exception {
        this.hwActionRequestBuilder = new HWActionRequestBuilder();
    }

    @Test(expected = IllegalStateException.class)
    public void buildRequest_UnexpectedRequestType() throws Exception {
        this.hwActionRequestBuilder.buildRequest(this.visionCamera);
    }

    @Test(expected = Exception.class)
    public void heatRequestBuilder_NoHeatPowerLevelSet() throws Exception {
        final Heat heat = new Heat(this.instructionHandler);
        this.hwActionRequestBuilder.buildRequest(heat);
    }

    @Test
    public void heatRequestBuilder_SettingInstructionHandler() throws Exception {
        final Heat heat = new Heat(this.instructionHandler);
        heat.setPowerLevel(HeatPowerLevel.POWER_LEVEL_FIVE);
        final HWRequest hwRequest = this.hwActionRequestBuilder.buildRequest(heat);
        assertEquals("Unexpected instruction handler", this.instructionHandler,
                hwRequest.getInstructionHandler());
    }


    @Test
    public void heatRequestBuilder_HWRequestType() throws Exception {
        final Heat heat = new Heat(this.instructionHandler);
        heat.setPowerLevel(HeatPowerLevel.POWER_LEVEL_FIVE);
        final HWRequest hwRequest = this.hwActionRequestBuilder.buildRequest(heat);
        assertEquals("Unexpected HWRequestType", HWRequestType.HW_ACTION_REQUEST,
                hwRequest.getHwRequestType());
    }

    @Test
    public void heatRequestBuilder_RequestType() throws Exception {
        final Heat heat = new Heat(this.instructionHandler);
        heat.setPowerLevel(HeatPowerLevel.POWER_LEVEL_FIVE);
        final HWActionRequest hwRequest = (HWActionRequest) this.hwActionRequestBuilder.buildRequest(heat);
        final HWMessage hwMessage = hwRequest.getRequestParams().get(0);
        assertEquals("Unexpected RequestType",
                RequestType.COMMAND_REQUEST, hwMessage.getRequestType());
    }

    @Test
    public void heatRequestBuilder_ComponentKeyPresence() throws Exception {
        final Heat heat = new Heat(this.instructionHandler);
        heat.setPowerLevel(HeatPowerLevel.POWER_LEVEL_FIVE);
        final HWActionRequest hwRequest = (HWActionRequest) this.hwActionRequestBuilder.buildRequest(heat);
        final HWMessage hwMessage = hwRequest.getRequestParams().get(0);
        assertEquals("Component key not present in the parametersMap", true,
                hwMessage.getParametersMap().containsKey(HWMessageKeys.Component));
    }

    @Test
    public void heatRequestBuilder_ComponentType() throws Exception {
        final Heat heat = new Heat(this.instructionHandler);
        heat.setPowerLevel(HeatPowerLevel.POWER_LEVEL_FIVE);
        final HWActionRequest hwRequest = (HWActionRequest) this.hwActionRequestBuilder.buildRequest(heat);
        final HWMessage hwMessage = hwRequest.getRequestParams().get(0);
        final String componentName = hwMessage.getParametersMap().get(HWMessageKeys.Component);
        assertEquals("Unexpected component name", Component.INDUCTION_HEATER.getComponent(), componentName);
    }
}