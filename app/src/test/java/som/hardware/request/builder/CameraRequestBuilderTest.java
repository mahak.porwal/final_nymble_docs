package som.hardware.request.builder;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import som.hardware.request.CameraRequest;
import som.hardware.request.HWRequestType;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.visionCamera.VisionCamera;
import som.instruction.request.visionCamera.VisionCameraCommand;

import static org.junit.Assert.*;

public class CameraRequestBuilderTest {

    CameraRequestBuilder cameraRequestBuilder;
    VisionCamera visionCamera;
    InstructionHandler instructionHandler;
    VisionCameraCommand visionCameraCommand;

    @Before
    public void setUp() throws Exception {
        this.instructionHandler = new InstructionHandler();
        this.visionCamera = new VisionCamera(this.instructionHandler);
        this.cameraRequestBuilder = new CameraRequestBuilder();
        this.visionCameraCommand = VisionCameraCommand.STILL_CAPTURE;
    }

    @Test(expected = Exception.class)
    public void buildRequest_VisualCommandNotSet() throws Exception {
        this.cameraRequestBuilder.buildRequest(this.visionCamera);
    }

    @Test
    public void buildRequest_SettingVisionCameraCommand() throws Exception {
        this.visionCamera.setCameraCommand(this.visionCameraCommand);
        final CameraRequest cameraRequest = (CameraRequest) this.cameraRequestBuilder.buildRequest(this.visionCamera);
        assertEquals("Wrong vision camera command set", this.visionCameraCommand,
                cameraRequest.getVisionCameraCommand());
    }

    @Test
    public void buildRequest_HWRequestType() throws Exception {
        this.visionCamera.setCameraCommand(this.visionCameraCommand);
        final CameraRequest cameraRequest = (CameraRequest) this.cameraRequestBuilder.buildRequest(this.visionCamera);
        assertEquals("Wrong HWRequest type", HWRequestType.CAMERA_REQUEST,
                cameraRequest.getHwRequestType());
    }

    @Test(expected = Exception.class)
    public void extractVisionCameraCommand_IllegalCameraCommandString() throws Exception {
        final Map<String, String> parametersMap = new HashMap<>();
        parametersMap.put(RequestParameters.VISUAL_CAMERA_COMMAND.getRequestParameter(),
                "dummyString");
        this.cameraRequestBuilder.extractVisionCameraCommand(parametersMap);
    }

}