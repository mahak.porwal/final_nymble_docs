package som.hardware.request.builder.hwActionRequest;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;


import som.hardware.message.Component;
import som.hardware.message.HWMessage;
import som.hardware.message.HWMessageKeys;
import som.hardware.message.RequestType;

import som.hardware.request.HWRequest;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.stir.StirringProfile;

import static org.junit.Assert.*;

public class StirRequestBuilderTest {

    InstructionHandler instructionHandler = new InstructionHandler();
    Map<String, String> parameters;

    @Before
    public void setUp() throws Exception {
        this.parameters = new HashMap<>();
    }

    @Test
    public void buildStirrerRequest_SettingInstructionHandler() throws Exception {
        this.parameters.put(RequestParameters.STIRRER_PROFILE.getRequestParameter(),
                StirringProfile.SLOW.getStirringProfile());

        final HWRequest hwRequest = StirRequestBuilder.buildStirrerRequest
                (this.parameters, this.instructionHandler);

        assertEquals("Instruction Handler set incorrectly", this.instructionHandler,
                hwRequest.getInstructionHandler());
    }

    @Test(expected = Exception.class)
    public void buildStirrerRequest_MissingKeyStirrerProfile() throws Exception {
        StirRequestBuilder.buildStirrerRequest
                (this.parameters, this.instructionHandler);
    }

    @Test
    public void buildStirrerRequest_RequestType() throws Exception {
        this.parameters.put(RequestParameters.STIRRER_PROFILE.getRequestParameter(),
                StirringProfile.SLOW.getStirringProfile());

        final HWMessage hwMessage = StirRequestBuilder.buildStirrerRequest
                (this.parameters, this.instructionHandler).getRequestParams().get(0);

        assertEquals("Incorrect RequestType set", RequestType.ACTION_REQUEST,
                hwMessage.getRequestType());
    }

    @Test
    public void buildStirrerRequest_ComponentType() throws Exception {
        this.parameters.put(RequestParameters.STIRRER_PROFILE.getRequestParameter(),
                StirringProfile.SLOW.getStirringProfile());

        final HWMessage hwMessage = StirRequestBuilder.buildStirrerRequest
                (this.parameters, this.instructionHandler).getRequestParams().get(0);

        assertEquals("Incorrect Component set", Component.STIRRER_MOTOR.getComponent(),
                hwMessage.getParametersMap().get(HWMessageKeys.Component));
    }

    @Test(expected = Exception.class)
    public void getActionTypeFromString_InvalidStirringProfile() throws Exception {
        final String dummyString = "dummyString";
        StirRequestBuilder.getActionTypeFromString(dummyString);
    }
}