package som.hardware.request.builder.hwActionRequest;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import som.hardware.message.HWMessage;
import som.hardware.message.RequestType;
import som.hardware.request.HWActionRequest;
import som.hardware.request.HWRequest;
import som.hardware.request.HWRequestType;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.dispense.Dispense;
import som.instruction.request.dispense.Ingredient;

import static org.junit.Assert.*;

public class DispenseRequestBuilderTest {

    Dispense dispense;
    final InstructionHandler instructionHandler = new InstructionHandler();

    @Before
    public void setUp() throws Exception {
        this.dispense = new Dispense(this.instructionHandler);
    }

    @Test
    public void buildDispenseRequest_HWRequestType() throws Exception {
        this.dispense.setIngredient(Ingredient.POTATO);
        this.dispense.setQuantity(2.3);
        final HWRequest hwRequest = DispenseRequestBuilder.buildDispenseRequest(this.dispense.getParameters(),
                this.instructionHandler);
        assertEquals("Unexpected HWRequest type", HWRequestType.HW_ACTION_REQUEST,
                hwRequest.getHwRequestType());
    }

    @Test
    public void buildDispenseRequest_InstructionHandler() throws Exception {
        this.dispense.setIngredient(Ingredient.POTATO);
        this.dispense.setQuantity(2.3);
        final HWRequest hwRequest = DispenseRequestBuilder.buildDispenseRequest(this.dispense.getParameters(),
                this.instructionHandler);
        assertEquals("Unexpected instruction handler", this.instructionHandler,
                hwRequest.getInstructionHandler());
    }

    @Test(expected = Exception.class)
    public void buildDispenseRequest_throwExceptionOnMissingIngredientKey() throws Exception {
        final Map<String, String> map = new HashMap<>();
        DispenseRequestBuilder.buildDispenseRequest(map, this.instructionHandler);
    }

    @Test(expected = Exception.class)
    public void buildDispenseRequest_throwExceptionOnMissingQuantityKey() throws Exception {
        final Map<String, String> map = new HashMap<>();
        map.put(RequestParameters.INGREDIENT.getRequestParameter(), Ingredient.POTATO.getIngredient());
        DispenseRequestBuilder.buildDispenseRequest(map, this.instructionHandler);
    }

    @Test
    public void buildDispenseRequest_RequestType() throws Exception {
        final Map<String, String> map = new HashMap<>();
        map.put(RequestParameters.INGREDIENT.getRequestParameter(), Ingredient.POTATO.getIngredient());
        map.put(RequestParameters.QUANTITY.getRequestParameter(), Double.toString(2.3));
        final HWActionRequest hwActionRequest = DispenseRequestBuilder.buildDispenseRequest(map, this.instructionHandler);
        final HWMessage hwMessage = hwActionRequest.getRequestParams().get(0);
        assertEquals("Unexpected RequestType", RequestType.ACTION_REQUEST, hwMessage.getRequestType());
    }
}