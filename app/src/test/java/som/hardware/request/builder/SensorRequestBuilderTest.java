package som.hardware.request.builder;

import org.junit.Before;
import org.junit.Test;

import som.hardware.request.HWRequest;
import som.hardware.request.HWRequestType;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.sensor.Sensor;
import som.instruction.request.sensor.Sensors;

import static org.junit.Assert.*;

public class SensorRequestBuilderTest {

    InstructionHandler instructionHandler = new InstructionHandler();
    SensorRequestBuilder sensorRequestBuilder;
    Sensor sensorRequest = new Sensor(this.instructionHandler);
    Sensors sensor = Sensors.THERMAL_CAMERA;

    @Before
    public void setUp() throws Exception {
        this.sensorRequestBuilder = new SensorRequestBuilder();
    }

    @Test(expected = Exception.class)
    public void buildRequest_SensorKeyCheck() throws Exception {
        this.sensorRequestBuilder.buildRequest(this.sensorRequest);
    }

    @Test
    public void buildRequest_HWRequestType() throws Exception {
        this.sensorRequest.setSensor(this.sensor);
        final HWRequest hwRequest = this.sensorRequestBuilder.buildRequest(this.sensorRequest);
        assertEquals("Wrong HWRequestType ", HWRequestType.SENSOR_REQUEST,
                hwRequest.getHwRequestType());
    }

    @Test
    public void buildRequest_InstructionHandler() throws Exception {
        this.sensorRequest.setSensor(this.sensor);
        final HWRequest hwRequest = this.sensorRequestBuilder.buildRequest(this.sensorRequest);
        assertEquals("Instruction handler not set ", this.instructionHandler,
                hwRequest.getInstructionHandler());
    }

    @Test(expected = Exception.class)
    public void mapComponentFromSensor() throws Exception {
        final String dummyString = "dummyString";
        this.sensorRequestBuilder.mapComponentFromSensor(dummyString);
    }
}