package som.hardware.request;

import org.junit.Before;
import org.junit.Test;

import som.instruction.request.visionCamera.VisionCameraCommand;

import static org.junit.Assert.*;

public class CameraRequestTest {

    CameraRequest cameraRequest;
    VisionCameraCommand visionCameraCommand = VisionCameraCommand.STILL_CAPTURE;
    String filename = "dummyPath";

    @Before
    public void setUp() throws Exception {
        this.cameraRequest = new CameraRequest(null);
    }

    @Test
    public void constructor_hwRequestType() {
        assertEquals("incorrect hardware request type", HWRequestType.CAMERA_REQUEST,
                this.cameraRequest.getHwRequestType());
    }
}