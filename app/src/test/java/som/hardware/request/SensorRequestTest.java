package som.hardware.request;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import som.hardware.message.Component;
import som.hardware.message.sensor.HWSensorRequest;
import som.hardware.message.sensor.SensorUnit;


import static org.junit.Assert.*;

public class SensorRequestTest {

  SensorRequest sensorRequest;
  HWSensorRequest hwSensorRequest;
  HWSensorRequest hwSensorRequest1;

  @Before
  public void setUp() throws Exception {
    this.sensorRequest = new SensorRequest(null);
    this.hwSensorRequest = new HWSensorRequest(Component.EXHAUST_FAN, SensorUnit.MICRO_VOLTS);
    this.hwSensorRequest1 = new HWSensorRequest(Component.LOADCELL_SENSOR, SensorUnit.GRAMS);
  }

  @Test
  public void constructor_hwRequestType() {
    assertEquals("incorrect request type", HWRequestType.SENSOR_REQUEST,
      this.sensorRequest.getHwRequestType());
  }


  @Test(expected = UnsupportedOperationException.class)
  public void getRequestParams_readOnlyBehavior() {
    this.sensorRequest.setSensor(this.hwSensorRequest);
    final List<HWSensorRequest> sensorRequestList = this.sensorRequest.getRequestParams();
    sensorRequestList.add(null);
  }

  @Test
  public void setSensor_listInsertionInNullList() {
    this.sensorRequest.setSensor(this.hwSensorRequest);
    assertEquals("incorrect HWSensorRequest inserted",
      this.hwSensorRequest, this.sensorRequest.getRequestParams().get(0));
  }

  @Test
  public void setSensor_listInsertionInNonNullList() {
    this.sensorRequest.setSensor(this.hwSensorRequest);
    this.sensorRequest.setSensor(this.hwSensorRequest1);
    assertEquals("two request were inserted, list size is not equal to two",
      2, this.sensorRequest.getRequestParams().size());
  }
}