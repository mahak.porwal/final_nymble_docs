package som.hardware.request.handler;

import org.junit.Before;
import org.junit.Test;

import som.hardware.request.HWRequest;
import som.hardware.request.HWRequestType;
import som.hardware.response.Response;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.dispense.Dispense;
import som.instruction.request.dispense.Ingredient;
import som.instruction.request.heat.Heat;
import som.instruction.request.heat.HeatPowerLevel;
import som.instruction.request.sensor.Sensor;
import som.instruction.request.sensor.Sensors;
import som.instruction.request.stir.Stir;
import som.instruction.request.stir.StirringProfile;
import som.instruction.request.visionCamera.VisionCamera;
import som.instruction.request.visionCamera.VisionCameraCommand;

import static org.junit.Assert.*;

public class RequestHandlerTest {

    RequestHandler requestHandler;
    InstructionHandler instructionHandler;

    @Before
    public void setUp() throws Exception {
        this.requestHandler = new RequestHandler();
    }

  /*  @Test
    public void handleRequest_HeatRequest() throws Exception {
        final Heat heat = new Heat(this.instructionHandler);
        heat.setPowerLevel(HeatPowerLevel.POWER_LEVEL_THREE);
        final Response response = this.requestHandler.handleRequest(heat);
        //TODO
    }*/

    @Test
    public void buildRequest_Heat_HWRequestType() throws Exception {
        final Heat heat = new Heat(this.instructionHandler);
        heat.setPowerLevel(HeatPowerLevel.POWER_LEVEL_THREE);
        final HWRequest hwRequest = this.requestHandler.buildRequest(heat);
        assertEquals("Incorrect HWRequest type", HWRequestType.HW_ACTION_REQUEST,
                hwRequest.getHwRequestType());
    }

    @Test
    public void buildRequest_Dispense_HWRequestType() throws Exception {
        final Dispense dispense = new Dispense(this.instructionHandler);
        dispense.setIngredient(Ingredient.POTATO);
        dispense.setQuantity(Double.valueOf(14));
        final HWRequest hwRequest = this.requestHandler.buildRequest(dispense);
        assertEquals("Incorrect HWRequest type", HWRequestType.HW_ACTION_REQUEST,
                hwRequest.getHwRequestType());
    }

    @Test
    public void buildRequest_Stir_HWRequestType() throws Exception {
        final Stir stir = new Stir(this.instructionHandler);
        stir.setStirringProfile(StirringProfile.SLOW);
        final HWRequest hwRequest = this.requestHandler.buildRequest(stir);
        assertEquals("Incorrect HWRequest type", HWRequestType.HW_ACTION_REQUEST,
                hwRequest.getHwRequestType());
    }

    @Test
    public void buildRequest_Sensor_HWRequestType() throws Exception {
        final Sensor sensor = new Sensor(this.instructionHandler);
        sensor.setSensor(Sensors.WEIGHT_SENSOR);
        final HWRequest hwRequest = this.requestHandler.buildRequest(sensor);
        assertEquals("Incorrect HWRequest type", HWRequestType.SENSOR_REQUEST,
                hwRequest.getHwRequestType());
    }

    @Test
    public void buildRequest_Camera_HWRequestType() throws Exception {
        final VisionCamera visionCamera = new VisionCamera(this.instructionHandler);
        visionCamera.setCameraCommand(VisionCameraCommand.STILL_CAPTURE);
        final HWRequest hwRequest = this.requestHandler.buildRequest(visionCamera);
        assertEquals("Incorrect HWRequest type", HWRequestType.CAMERA_REQUEST,
                hwRequest.getHwRequestType());
    }

}