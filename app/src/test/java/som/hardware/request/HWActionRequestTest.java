package som.hardware.request;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import som.hardware.message.Component;
import som.hardware.message.HWMessage;
import som.hardware.message.RequestType;
import som.hardware.message.action.ActionRequest;
import som.hardware.message.action.ActionRequestParameters;
import som.hardware.message.action.ActionType;
import som.hardware.message.command.CommandRequest;
import som.hardware.message.command.CommandRequestParameters;
import som.instruction.handler.InstructionHandler;

import static org.junit.Assert.*;

public class HWActionRequestTest {

    HWActionRequest hwActionRequest;
    ActionRequest actionRequest;
    CommandRequest commandRequest;
    InstructionHandler instructionHandler;

    @Before
    public void setUp() throws Exception {
        this.instructionHandler = new InstructionHandler();
        this.hwActionRequest = new HWActionRequest(this.instructionHandler);
        this.actionRequest = new ActionRequest(Component.MACRO_SERVO_1, ActionType.MACRO_LIQUID_DISPENSE);
        this.actionRequest.setActionParameters(ActionRequestParameters.HARDWARE_UNITS, Integer.toString(4));
        this.hwActionRequest.setRequestParams(this.actionRequest);
        this.commandRequest = new CommandRequest(Component.EXHAUST_FAN);
        this.commandRequest.setParameters(CommandRequestParameters.SPEED, Double.toString(50.0));

    }

    @Test(expected = UnsupportedOperationException.class)
    public void getRequestParams() {
        final List<HWMessage> list = this.hwActionRequest.getRequestParams();
        list.add(null);
    }

    @Test
    public void setRequestParams_NonNullList() {
        assertNotEquals("null list", null, this.hwActionRequest.getRequestParams());
    }

    @Test
    public void setRequestParams_ListInsertionInNullList() {
        assertEquals("incorrect object inserted", RequestType.ACTION_REQUEST,
                this.hwActionRequest.getRequestParams().get(0).getRequestType());
    }

    @Test
    public void setRequestParams_ListInsertionInNonNullList() {
        this.hwActionRequest.setRequestParams(this.commandRequest);
        assertEquals("two requests were inserted, but size is not equal to two", 2,
                this.hwActionRequest.getRequestParams().size());
    }

    @Test
    public void HWRequestType_Test() {
        assertEquals("incorrect HW request type", HWRequestType.HW_ACTION_REQUEST,
                this.hwActionRequest.getHwRequestType());
    }
}