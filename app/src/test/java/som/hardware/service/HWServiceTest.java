package som.hardware.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import som.hardware.request.CameraRequest;
import som.hardware.request.HWActionRequest;
import som.hardware.request.SensorRequest;
import som.hardware.response.CameraResponse;
import som.hardware.response.HWActionResponse;
import som.hardware.response.SensorResponse;
import som.instruction.handler.InstructionHandler;

import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({HWService.class})
public class HWServiceTest {

    InstructionHandler instructionHandler = new InstructionHandler();
    HWService hwService;
    CameraRequest cameraRequest = new CameraRequest(this.instructionHandler);
    SensorRequest sensorRequest = new SensorRequest(this.instructionHandler);
    HWActionRequest hwActionRequest = new HWActionRequest(this.instructionHandler);

    CameraResponse cameraResponse = new CameraResponse();
    HWActionResponse hwActionResponse = new HWActionResponse(1, true, null);
    SensorResponse sensorResponse = new SensorResponse(1, true, null);


    @Before
    public void setUp() throws Exception {
        this.hwService = Mockito.mock(HWService.class);
        Mockito.when(this.hwService.executeRequest(this.cameraRequest)).thenCallRealMethod();
        Mockito.when(this.hwService.executeRequest(this.sensorRequest)).thenCallRealMethod();
        Mockito.when(this.hwService.executeRequest(this.hwActionRequest)).thenCallRealMethod();
        Mockito.when(this.hwService.executeCameraRequest(this.cameraRequest)).thenReturn(this.cameraResponse);
        Mockito.when(this.hwService.executeHWActionRequest(this.hwActionRequest)).thenReturn(this.hwActionResponse);
        Mockito.when(this.hwService.executeSensorRequest(this.sensorRequest)).thenReturn(this.sensorResponse);
    }

    @Test
    public void executeRequest_HWActionRequest() throws Exception {
        assertEquals("Wrong response ", this.hwActionResponse,
                this.hwService.executeRequest(this.hwActionRequest));
    }

    @Test
    public void executeRequest_CameraRequest() throws Exception {
        assertEquals("Wrong response", this.cameraResponse,
                this.hwService.executeRequest(this.cameraRequest));
    }

    @Test
    public void executeRequest_CameraRequest_CameraV8_3() {

    }

    @Test
    public void executeRequest_SensorRequest() throws Exception {
        assertEquals("Wrong response", this.sensorResponse,
                this.hwService.executeRequest(this.sensorRequest));
    }
}