package som.instruction.request.sensor;

import org.junit.Before;
import org.junit.Test;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;

import static org.junit.Assert.*;

public class SensorTest {

    InstructionHandler instructionHandler;
    Sensor sensor;

    @Before
    public void setUp() throws Exception {
        this.instructionHandler = new InstructionHandler();
        this.sensor = new Sensor(this.instructionHandler);
    }

    @Test
    public void constructor_InstructionHandler() {
        assertEquals("Incorrect instruction handler set", this.instructionHandler,
                this.sensor.getRequester());
    }

    @Test
    public void constructor_RequestType() {
        assertEquals("Incorrect request type set", RequestType.SENSOR,
                this.sensor.getRequestType());
    }

    @Test
    public void setSensor() {
        this.sensor.setSensor(Sensors.THERMAL_CAMERA);
        assertEquals("Incorrect sensor string set", Sensors.THERMAL_CAMERA.getSensorName(),
                this.sensor.getParameters().get(RequestParameters.SENSOR.getRequestParameter()));
    }
}