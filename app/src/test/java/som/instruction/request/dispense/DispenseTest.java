package som.instruction.request.dispense;

import org.junit.Before;
import org.junit.Test;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;

import static org.junit.Assert.*;

public class DispenseTest {


    InstructionHandler instructionHandler;
    Dispense dispense;

    @Before
    public void setUp() throws Exception {
        this.instructionHandler = new InstructionHandler();
        this.dispense = new Dispense(this.instructionHandler);
    }


    @Test
    public void constructor_InstructionHandler() {
        assertEquals("Incorrect instruction handler", this.instructionHandler,
                this.dispense.getRequester());
    }

    @Test
    public void constructor_RequestType() {
        assertEquals("Incorrect request type", RequestType.DISPENSE,
                this.dispense.getRequestType());
    }

    @Test
    public void setIngredient() {
        this.dispense.setIngredient(Ingredient.MILK);
        assertEquals("Incorrect ingredient string set", Ingredient.MILK.getIngredient(),
                this.dispense.getParameters().get(RequestParameters.INGREDIENT.getRequestParameter()));
    }

    @Test
    public void setQuantity() {
        this.dispense.setQuantity(Double.valueOf(23.0));
        assertEquals("Incorrect quantity string set", 23.0,
                Double.parseDouble(this.dispense.getParameters().get(RequestParameters.QUANTITY.getRequestParameter())), 0.00001);
    }
}