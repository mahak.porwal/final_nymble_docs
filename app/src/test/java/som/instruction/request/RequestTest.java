package som.instruction.request;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import timber.log.Timber;


public class RequestTest {

  Request request;
  String testKey = "test_key";
  String testValue = "test_value";

  @Before
  public void setUp() throws Exception {
    this.request = Mockito.mock(Request.class, Mockito.CALLS_REAL_METHODS);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void getParameters_readOnlyBehavior() {
    this.request.parameters = new HashMap<>();
    this.request.getParameters().put(this.testKey, this.testValue);
  }

  @Test
  public void forLoopTimeTest() {


    final List<Double> doubleList = new ArrayList<>();
    for (int i = 0; i < 1024; i++) {
      String testValue = "test_value";
      String testkey = testValue + "key";
      doubleList.add((double) i);
    }
    double[] array = new double[1024];
    int itr=0;
    final long timeLogger = System.currentTimeMillis();
    for(Double doubleElement : doubleList){
      array[itr] = doubleElement;
      itr++;
    }
    System.out.println("Time taken "  + (System.currentTimeMillis() - timeLogger));
    ///Timber.d("time taken %d", System.currentTimeMillis() - timeLogger);
  }
}