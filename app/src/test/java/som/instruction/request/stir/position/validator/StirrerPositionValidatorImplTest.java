package som.instruction.request.stir.position.validator;

import static org.junit.Assert.*;

import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.LIQUID_LOWER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.LIQUID_UPPER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MACRO_LOWER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MACRO_UPPER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_FIVE_LOWER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_FIVE_UPPER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_FOUR_LOWER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_FOUR_UPPER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_ONE_LOWER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_ONE_UPPER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_SIX_LOWER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_SIX_UPPER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_THREE_LOWER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_THREE_UPPER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_TWO_LOWER_ANGLE;
import static som.instruction.request.stir.position.finder.StirrerRangeFinderImpl.MICRO_POD_TWO_UPPER_ANGLE;

import org.junit.Before;
import org.junit.Test;

public class StirrerPositionValidatorImplTest {

  StirrerPositionValidator stirrerPositionValidator;

  @Before
  public void setUp() throws Exception {
    this.stirrerPositionValidator = new StirrerPositionValidatorImpl();
  }

  @Test
  public void isPositionValid_Micro_Pod_ONE_ValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_ONE_LOWER_ANGLE;
    range[1] = MICRO_POD_ONE_UPPER_ANGLE;
    assertTrue(this.stirrerPositionValidator.isPositionValid(45, range));
  }

  @Test
  public void isPositionValid_Micro_Pod_ONE_InValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_ONE_LOWER_ANGLE;
    range[1] = MICRO_POD_ONE_UPPER_ANGLE;
    assertFalse(this.stirrerPositionValidator.isPositionValid(180, range));
  }


  @Test
  public void isPositionValid_Micro_Pod_TWO_ValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_TWO_LOWER_ANGLE;
    range[1] = MICRO_POD_TWO_UPPER_ANGLE;
    assertTrue(this.stirrerPositionValidator.isPositionValid(90, range));
  }

  @Test
  public void isPositionValid_Micro_Pod_TWO_InValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_TWO_LOWER_ANGLE;
    range[1] = MICRO_POD_TWO_UPPER_ANGLE;
    assertFalse(this.stirrerPositionValidator.isPositionValid(270, range));
  }


  @Test
  public void isPositionValid_Micro_Pod_THREE_ValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_THREE_LOWER_ANGLE;
    range[1] = MICRO_POD_THREE_UPPER_ANGLE;
    assertTrue(this.stirrerPositionValidator.isPositionValid(180, range));
  }

  @Test
  public void isPositionValid_Micro_Pod_THREE_InValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_THREE_LOWER_ANGLE;
    range[1] = MICRO_POD_THREE_UPPER_ANGLE;
    assertFalse(this.stirrerPositionValidator.isPositionValid(360, range));
  }


  @Test
  public void isPositionValid_Micro_Pod_FOUR_ValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_FOUR_LOWER_ANGLE;
    range[1] = MICRO_POD_FOUR_UPPER_ANGLE;
    assertTrue(this.stirrerPositionValidator.isPositionValid(225, range));
  }

  @Test
  public void isPositionValid_Micro_Pod_FOUR_InValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_FOUR_LOWER_ANGLE;
    range[1] = MICRO_POD_FOUR_UPPER_ANGLE;
    assertFalse(this.stirrerPositionValidator.isPositionValid(75, range));
  }

  @Test
  public void isPositionValid_Micro_Pod_FIVE_ValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_FIVE_LOWER_ANGLE;
    range[1] = MICRO_POD_FIVE_UPPER_ANGLE;
    assertTrue(this.stirrerPositionValidator.isPositionValid(255, range));
  }

  @Test
  public void isPositionValid_Micro_Pod_FIVE_InValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_FIVE_LOWER_ANGLE;
    range[1] = MICRO_POD_FIVE_UPPER_ANGLE;
    assertFalse(this.stirrerPositionValidator.isPositionValid(90, range));
  }

  @Test
  public void isPositionValid_Micro_Pod_SIX_ValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_SIX_LOWER_ANGLE;
    range[1] = MICRO_POD_SIX_UPPER_ANGLE;
    assertTrue(this.stirrerPositionValidator.isPositionValid(255, range));
  }

  @Test
  public void isPositionValid_Micro_Pod_SIX_InValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MICRO_POD_SIX_LOWER_ANGLE;
    range[1] = MICRO_POD_SIX_UPPER_ANGLE;
    assertFalse(this.stirrerPositionValidator.isPositionValid(180, range));
  }

  @Test
  public void isPositionValid_LIQUID_ValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = LIQUID_LOWER_ANGLE;
    range[1] = LIQUID_UPPER_ANGLE;
    assertTrue(this.stirrerPositionValidator.isPositionValid(270, range));
  }

  @Test
  public void isPositionValid_LIQUID_InValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = LIQUID_LOWER_ANGLE;
    range[1] = LIQUID_UPPER_ANGLE;
    assertFalse(this.stirrerPositionValidator.isPositionValid(90, range));
  }

  @Test
  public void isPositionValid_MACRO_ValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MACRO_LOWER_ANGLE;
    range[1] = MACRO_UPPER_ANGLE;
    assertTrue(this.stirrerPositionValidator.isPositionValid(0, range));
  }

  @Test
  public void isPositionValid_MACRO_InValidAngle() {
    final Integer[] range = new Integer[2];
    range[0] = MACRO_LOWER_ANGLE;
    range[1] = MACRO_UPPER_ANGLE;
    assertFalse(this.stirrerPositionValidator.isPositionValid(180, range));
  }

}