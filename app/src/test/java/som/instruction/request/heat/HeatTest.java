package som.instruction.request.heat;

import org.junit.Before;
import org.junit.Test;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;

import static org.junit.Assert.*;

public class HeatTest {

    InstructionHandler instructionHandler;
    Heat heat;

    @Before
    public void setUp() throws Exception {
        this.instructionHandler = new InstructionHandler();
        this.heat = new Heat(this.instructionHandler);
    }

    @Test
    public void constructor_InstructionHandler() {
        assertEquals("Incorrect instruction handler set", this.instructionHandler,
                this.heat.getRequester());
    }

    @Test
    public void constructor_RequestType() {
        assertEquals("Incorrect request type set", RequestType.HEAT,
                this.heat.getRequestType());
    }

    @Test
    public void setPowerLevel_SetPowerLevelZero() {
        this.heat.setPowerLevel(HeatPowerLevel.POWER_LEVEL_ZERO);
        assertEquals("Incorrect power level set", HeatPowerLevel.POWER_LEVEL_ZERO.getPowerLevel(),
                this.heat.getParameters().get(RequestParameters.HEAT_POWER_LEVEL.getRequestParameter()));
    }
}