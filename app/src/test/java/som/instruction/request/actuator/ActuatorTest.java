package som.instruction.request.actuator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import som.instruction.request.heat.HeatPowerLevel;

public class ActuatorTest {

  Actuator actuator;

  @Before
  public void setUp() {
    actuator = new Actuator(null);
  }

  @Test
  public void checkHeatLevelShutdown() {
    Assert.assertEquals(HeatPowerLevel.POWER_LEVEL_ONE, actuator.createHeatLevel(190));
  }

  @Test
  public void checkHeatLevelTwo() {
    Assert.assertEquals(HeatPowerLevel.POWER_LEVEL_TWO, actuator.createHeatLevel(490));
  }

  @Test
  public void checkHeatLevelMax() {
    Assert.assertEquals(HeatPowerLevel.POWER_LEVEL_SEVEN, actuator.createHeatLevel(2000));
  }

  @Test
  public void checkHeatLevelMidway() {
    Assert.assertEquals(HeatPowerLevel.POWER_LEVEL_ONE, actuator.createHeatLevel(300));
  }

  @Test
  public void checkQuantizedValue() {
    Assert.assertEquals(java.util.Optional.of(1400).get(), actuator.getQuantizedValue(1301));
  }

}
