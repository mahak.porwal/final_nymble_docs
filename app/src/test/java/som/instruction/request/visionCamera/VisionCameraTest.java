package som.instruction.request.visionCamera;

import org.junit.Before;
import org.junit.Test;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;

import static org.junit.Assert.*;

public class VisionCameraTest {

    InstructionHandler instructionHandler;
    VisionCamera visionCamera;

    @Before
    public void setUp() throws Exception {
        this.instructionHandler = new InstructionHandler();
        this.visionCamera = new VisionCamera(this.instructionHandler);
    }

    @Test
    public void constructor_InstructionHandler() {
        assertEquals("Incorrect instruction handler set", this.instructionHandler,
                this.visionCamera.getRequester());
    }

    @Test
    public void constructor_RequestType() {
        assertEquals("Incorrect request type set", RequestType.VISION_CAMERA,
                this.visionCamera.getRequestType());
    }

    @Test
    public void setCameraCommand() {
        this.visionCamera.setCameraCommand(VisionCameraCommand.STILL_CAPTURE);
        assertEquals("Incorrect vision camera command set", VisionCameraCommand.STILL_CAPTURE.getVisionCameraCommand(),
                this.visionCamera.getParameters().get(RequestParameters.VISUAL_CAMERA_COMMAND.getRequestParameter()));
    }
}