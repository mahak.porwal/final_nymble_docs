package com.example.recipeengine.engine.chef;


import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.executor.factory.ExecutorFactory;
import com.example.recipeengine.instruction.executor.InstructionExecutor;
import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.utils.Asserts;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class BaseChefTest {

  BaseChef baseChef;

  InstructionExecutor instructionExecutor;

  ExecutorFactory executorFactory;

  Cook cook;

  CompositeDisposable compositeDisposable;

  @Before
  public void setUp() throws Exception {

    this.compositeDisposable = new CompositeDisposable();

    this.cook = new Cook(0,0,null, null, null, null, null);

    this.instructionExecutor = new InstructionExecutor() {
      @Override
      public void execute() {
        return;
      }

      @Override
      public BaseInstStatus getStatus() {
        return null;
      }

      @Override
      public void pause() throws Exception {
        return;
      }
    };

    this.executorFactory = new ExecutorFactory() {
      @Override
      public InstructionExecutor getExecutor(Instruction instruction) {
        return instructionExecutor;
      }
    };

    this.baseChef = new BaseChef(() -> {
      List<Instruction> list = new ArrayList<>();
      list.add(cook);
      return list;
    }, this.executorFactory);
  }

  @Test
  public void startCooking_CheckIfTheLoopBreaksOnSettingError() {
    Observable.just(1)
      .delay(100, TimeUnit.MILLISECONDS)
      .subscribe(l -> this.baseChef.interruptWithError("Error"));
    this.baseChef.startCooking();
    Asserts.assertNotNull(this.baseChef.getError(),
      "Cooking loop broken without setting error");
  }

  @Test
  public void startCooking_CheckIfTheLoopBreaksWhenNullInstructionIsPassed() {

    AtomicInteger instructionCount = new AtomicInteger();
    final int DUMMY_RECIPE_SIZE = 5;
    this.baseChef = new BaseChef(() -> {
      if (instructionCount.getAndIncrement() > DUMMY_RECIPE_SIZE) {
        return null;
      }
      List<Instruction> list = new ArrayList<>();
      list.add(this.cook);
      return list;
    }, executorFactory);

    this.baseChef.startCooking();
  }


  @Test
  public void getInstStatus_CheckIfTheMethodReturnsAListOfSizeOneWhileAnInstructionExecutionIsGoingOn() {

    this.instructionExecutor = new InstructionExecutor() {
      @Override
      public void execute() {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        return;
      }

      @Override
      public BaseInstStatus getStatus() {
        return new BaseInstStatus() {
          @Override
          public String getStatus() {
            return "Dummy status";
          }

          @Override
          public String getVerboseStatus() {
            return "Dummy verbose status";
          }

          @Override
          public Integer remainingTimeSec() {
            return 50;
          }
        };
      }

      @Override
      public void pause() throws Exception {
        return;
      }
    };

    this.executorFactory = new ExecutorFactory() {
      @Override
      public InstructionExecutor getExecutor(Instruction instruction) {
        return instructionExecutor;
      }
    };

    AtomicBoolean isStatusCaptured = new AtomicBoolean(false);
    this.baseChef = new BaseChef(() -> {
      if (isStatusCaptured.get()) {
        return null;
      }
      List<Instruction> list = new ArrayList<>();
      list.add(this.cook);
      return list;
    }, this.executorFactory);

    this.compositeDisposable.add(Observable.just(1).observeOn(Schedulers.io()).subscribe(
      l -> this.baseChef.startCooking()));
    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    Assert.assertEquals(1, this.baseChef.getInstStatus().size());
    isStatusCaptured.set(true);
  }

  @Test
  public void getInstStatus_CheckIfTheMethodReturnsCorrectRemainingTimeForTheExcutingInstruction() {

    final int REMAINING_TIME = 50;

    this.instructionExecutor = new InstructionExecutor() {
      @Override
      public void execute() {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        return;
      }

      @Override
      public BaseInstStatus getStatus() {
        return new BaseInstStatus() {
          @Override
          public String getStatus() {
            return "Dummy status";
          }

          @Override
          public String getVerboseStatus() {
            return "Dummy verbose status";
          }

          @Override
          public Integer remainingTimeSec() {
            return Integer.valueOf(REMAINING_TIME);
          }
        };
      }

      @Override
      public void pause() throws Exception {
        return;
      }
    };

    this.executorFactory = new ExecutorFactory() {
      @Override
      public InstructionExecutor getExecutor(Instruction instruction) {
        return instructionExecutor;
      }
    };

    AtomicBoolean isStatusCaptured = new AtomicBoolean(false);
    this.baseChef = new BaseChef(() -> {
      if (isStatusCaptured.get()) {
        return null;
      }
      List<Instruction> list = new ArrayList<>();
      list.add(this.cook);
      return list;
    }, this.executorFactory);

    this.compositeDisposable.add(Observable.just(1).observeOn(Schedulers.io()).subscribe(
      l -> this.baseChef.startCooking()));
    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    Assert.assertEquals(REMAINING_TIME,
      this.baseChef.getInstStatus().get(0).second.remainingTimeSec().longValue());
    isStatusCaptured.set(true);
  }

  @Test
  public void getStatus_NoCurrentExcutingInstructions() {
    this.baseChef = new BaseChef(() -> {
      return null;
    }, executorFactory);
    this.baseChef.startCooking();
    Assert.assertEquals(0, this.baseChef.getInstStatus().size());
  }

  @Test
  public void pauseWithInstructionId_InstructionIdNotPresent() throws Exception {
    AtomicBoolean shouldReturnNull = new AtomicBoolean(false);
    this.baseChef = new BaseChef(() -> {
      if (shouldReturnNull.get()) {
        return null;
      }
      List<Instruction> list = new ArrayList<>();
      list.add(this.cook);
      return list;
    }, this.executorFactory);

    this.compositeDisposable.add(Observable.just(1).observeOn(Schedulers.io()).subscribe(
      l -> this.baseChef.startCooking()));
    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    this.baseChef.pause(1);
    shouldReturnNull.set(true);
  }

  @Test
  public void pauseWithInstructionId_InstructionIdPresent() throws Exception {

    this.instructionExecutor = new InstructionExecutor() {
      @Override
      public void execute() {
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }

      @Override
      public BaseInstStatus getStatus() {
        return new BaseInstStatus() {
          @Override
          public String getStatus() {
            return "Dummy status";
          }

          @Override
          public String getVerboseStatus() {
            return "Dummy verbose status";
          }

          @Override
          public Integer remainingTimeSec() {
            return Integer.valueOf(50);
          }
        };
      }

      @Override
      public void pause() throws Exception {
      }
    };

    this.executorFactory = new ExecutorFactory() {
      @Override
      public InstructionExecutor getExecutor(Instruction instruction) {
        return instructionExecutor;
      }
    };

    AtomicInteger instructionCount = new AtomicInteger();
    final int DUMMY_RECIPE_SIZE = 5;
    this.baseChef = new BaseChef(() -> {
      if (instructionCount.getAndIncrement() > DUMMY_RECIPE_SIZE) {
        return null;
      }
      List<Instruction> list = new ArrayList<>();
      list.add(cook);
      return list;
    }, this.executorFactory);

    this.compositeDisposable.add(Observable
      .just(1)
      .observeOn(Schedulers.io())
      .subscribe(
        l -> this.baseChef.startCooking()));
    try {
      Thread.sleep(100);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    this.baseChef.pause(0);
    Assert.assertEquals(0, this.baseChef.getInstStatus().size());
    this.compositeDisposable.clear();
  }

  @Test
  public void pause_breaksTheCookingLoop() {
    this.compositeDisposable.add(
      Observable.just(1)
        .observeOn(Schedulers.io())
        .delay(100, TimeUnit.MILLISECONDS)
        .subscribe(l -> this.baseChef.pause()));
    this.baseChef.startCooking();
  }

  @Test
  public void interruptWithError() {
  }
}