package com.example.recipeengine.engine;

import com.example.recipeengine.recipe.reader.RecipeReaderModule;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.powermock.utils.Asserts;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BaseEngineTest {

  Engine engine;

  @Before
  public void setUp() throws Exception {
    Object object = null;
    try {
      object = new JSONParser().parse(new FileReader(
        "./src/main/java/com/example/recipeengine/resources/Recipe_StirringHeatingTest.json"));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    JSONObject recipeJsonObject = (JSONObject) object;

    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", "4");
    parameters.put("potato_size", "78");
    this.engine = DaggerEngineComponent.builder().recipeReaderModule(
      new RecipeReaderModule(recipeJsonObject, 0, parameters)).build().createEngine();
  }

  @Test
  public void getExecutionStatus() {
    Asserts.assertNotNull(this.engine, "null engine");
  }

  @Test
  public void runHardwareChecks() {
  }

  @Test
  public void executeHwRequest() {
  }

  @Test
  public void startCooking() {

  }

  @Test
  public void stopCooking() {
  }

  @Test
  public void stopInstruction() {
  }
}