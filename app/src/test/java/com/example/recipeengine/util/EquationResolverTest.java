package com.example.recipeengine.util;

import com.example.recipeengine.instruction.info.cook.ActionInfo;
import com.example.recipeengine.instruction.info.cook.ActionInformation;
import com.example.recipeengine.instruction.info.cook.BaseCookInfo;
import com.example.recipeengine.instruction.info.cook.CookInfo;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class EquationResolverTest {

  EquationResolver equationResolver;
  ActionInfo actionInfo;
  CookInfo cookInfo;
  final String parametricField = "defaultWattageString";
  final String servingSize = "serving_size";
  final String potatoSize = "potato_size";

  @Before
  public void setUp() throws Exception {
    this.equationResolver = new MxEquationResolver();
    this.actionInfo = new ActionInformation("100*12",
      "120", "12.65", "55.2",
      "230-10");
    this.cookInfo = new BaseCookInfo(null, this.actionInfo, null,
      null, null, null, null, null);
  }

  @Test
  public void setResolvedValue() {
    MxEquationResolver.setResolvedValue("defaultWattageString", "123",
      this.actionInfo);
  }

  @Test
  public void resolveAndCheck_completelyResolvedExpression() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(String.valueOf(120));
    stringBuilder.append("*");
    stringBuilder.append(servingSize);
    Map paramsMap = new HashMap();
    paramsMap.put(servingSize, String.valueOf(3));
    /*assertTrue(this.equationResolver.resolveAndCheck(parametricField, stringBuilder.toString(),
      Collections.unmodifiableMap(paramsMap)));*/
  }

  @Test
  public void resolveAndCheck_partiallyResolvedExpression() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(String.valueOf(120));
    stringBuilder.append("*");
    stringBuilder.append(servingSize);
    stringBuilder.append("+");
    stringBuilder.append(String.valueOf(12));
    stringBuilder.append("*");
    stringBuilder.append(potatoSize);
    Map paramsMap = new HashMap();
    paramsMap.put(servingSize, String.valueOf(3));
    /*assertFalse(this.equationResolver.resolveAndCheck(parametricField, stringBuilder.toString(),
      Collections.unmodifiableMap(paramsMap)));*/
  }

  @Test
  public void resolveAndCheck_partiallyResolvedAndThenCompletelyResolveExpression() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(String.valueOf(120));
    stringBuilder.append("*");
    stringBuilder.append(servingSize);
    stringBuilder.append("+");
    stringBuilder.append(String.valueOf(12));
    stringBuilder.append("*");
    stringBuilder.append(potatoSize);
    Map paramsMap = new HashMap();
    paramsMap.put(servingSize, String.valueOf(3));
    /*this.equationResolver.resolveAndCheck(parametricField, stringBuilder.toString(),
      Collections.unmodifiableMap(paramsMap));
    paramsMap.put(potatoSize, String.valueOf(45));
    assertTrue(this.equationResolver.resolveAndCheck(parametricField, stringBuilder.toString(),
      Collections.unmodifiableMap(paramsMap)));*/
  }
}