package com.example.recipeengine.util;

import org.junit.Before;
import org.junit.Test;

import som.instruction.request.stir.position.UnableToPositionException;

public class RetryTest {
  private int n;

  private void testConsumerSucceedsAfterTwoTries(Object object) throws UnableToPositionException {
    if (1 >= n++) {
      throw new UnableToPositionException("test");
    }
  }

  private void testConsumerThrowsException(Object object) throws Exception {
    throw new Exception("test");
  }

  private void testConsumerThrowsExceptionAfterOneRetry(Object object) throws Exception {
    if (0 >= n++) {
      throw new UnableToPositionException("test");
    }
    throw new Exception("test");
  }

  @Before
  public void setUp() throws Exception {
  }

  @Test(expected = Exception.class)
  public void throwsExceptionIfMaxRetriesExceed() throws Exception {
    Retry.Do(this::testConsumerSucceedsAfterTwoTries, null,
      1, UnableToPositionException.class, "exceeded tries");
  }

  @Test
  public void consumerSuccessfullyCompletesAfterSomeRetries() throws Exception {
    Retry.Do(this::testConsumerSucceedsAfterTwoTries, null,
      3, UnableToPositionException.class, "exceeded tries");
  }

  @Test(expected = Exception.class)
  public void consumerThrowsUnexpectedException() throws Exception {
    Retry.Do(this::testConsumerThrowsException, null,
      3, UnableToPositionException.class, "exceeded tries");
  }

  @Test(expected = Exception.class)
  public void consumerThrowsUnexpectedExceptionAfterFirstTry() throws Exception {
    Retry.Do(this::testConsumerThrowsExceptionAfterOneRetry, null,
      3, UnableToPositionException.class, "exceeded tries");
  }
}