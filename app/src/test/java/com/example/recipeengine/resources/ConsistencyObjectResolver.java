package com.example.recipeengine.resources;

import androidx.core.util.Pair;

import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.cook.params.action.ActionParameters;
import com.example.recipeengine.instruction.decorator.exchanges.ExecutionParams;
import com.example.recipeengine.instruction.decorator.exchanges.InstructionInputParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.decorator.source.InstructionDecoration;
import com.example.recipeengine.instruction.decorator.source.InstructionDecorationMap;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;


import java.util.HashMap;
import java.util.Map;

public class ConsistencyObjectResolver {


  public static InstructionLog createInstructionLogObject(String bucketScore) {
    bucketScore = bucketScore == null ? "5.2" : bucketScore;
    Map<ExecutionParams, String> executionParamsStringMap = new HashMap<>();
    executionParamsStringMap.put(ExecutionParams.TEMPERATURE, "30.00");
    Map<OutputParams, String> outputParamsStringMap = new HashMap<>();
    outputParamsStringMap.put(OutputParams.CONSISTENCY_SCORE, bucketScore);
    return new InstructionLog(
      123, 0, executionParamsStringMap, outputParamsStringMap);
  }


  public static InstructionDecorationMap createInstructionDecorationMapObject() {
    Map<OutputParams, Double> paramsDecorationWeight = new HashMap<>();
    paramsDecorationWeight.put(OutputParams.CONSISTENCY_SCORE, 1.0);

    //Parameters
    Map<OutputParams, Map<String, Map<InstructionInputParams, String>>> parameters =
      new HashMap<>();
    Map<String, Map<InstructionInputParams, String>> bucketMap = new HashMap<>();
    Map<InstructionInputParams, String> instructionInputParamsStringMap1 = new HashMap<>();
    instructionInputParamsStringMap1.put(InstructionInputParams.INGREDIENT_QUANTITY, "1.2");
    bucketMap.put("bucket 1", instructionInputParamsStringMap1);
    Map<InstructionInputParams, String> instructionInputParamsStringMap2 = new HashMap<>();
    instructionInputParamsStringMap2.put(InstructionInputParams.INGREDIENT_QUANTITY, "0.7");
    bucketMap.put("bucket 2", instructionInputParamsStringMap2);
    parameters.put(OutputParams.ONION_SIZE_SCORE, bucketMap);

    //Discrete Map
    Map<OutputParams, Map<String, Pair<Double, Double>>> discretizationMap = new HashMap<>();
    Map<String, Pair<Double, Double>> buckets = new HashMap<>();
    Pair<Double, Double> pair1 = new Pair<>(5.0, 7.0);
    Pair<Double, Double> pair2 = new Pair<>(7.0, 10.0);
    buckets.put("bucket 1", pair1);
    buckets.put("bucket 2", pair2);
    //System.out.println(buckets.get("bucket 1").first);
    discretizationMap.put(OutputParams.CONSISTENCY_SCORE, buckets);
    InstructionDecoration instructionDecoration =
      new InstructionDecoration(1.0, paramsDecorationWeight, parameters, discretizationMap);
    Map<Integer, InstructionDecoration> integerInstructionDecorationMap = new HashMap<>();
    integerInstructionDecorationMap.put(123, instructionDecoration);
    return new InstructionDecorationMap(integerInstructionDecorationMap);
  }

  public static Cook createCookObject() {
    final ActionParameters actionParameters = new ActionParameters(0, 90, 90.0, 0.0, 0);
    return new Cook(0,0,null, null, actionParameters, null, null);
  }

}
