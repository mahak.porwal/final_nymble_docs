package com.example.recipeengine.resources;

import androidx.core.util.Pair;

import com.example.recipeengine.instruction.decorator.exchanges.ExecutionParams;
import com.example.recipeengine.instruction.decorator.exchanges.InstructionInputParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.decorator.source.InstructionDecoration;
import com.example.recipeengine.instruction.decorator.source.InstructionDecorationMap;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;
import com.example.recipeengine.instruction.dispense.BaseDispense;
import com.example.recipeengine.instruction.dispense.Dispense;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DispenseObjectsResolver {

  public static InstructionDecorationMap createInstructionDecorationMapObject() {
    Map<OutputParams, Double> paramsDecorationWeight = new HashMap<>();
    paramsDecorationWeight.put(OutputParams.ONION_SIZE_SCORE, 1.0);

    //Parameters
    Map<OutputParams, Map<String, Map<InstructionInputParams, String>>> parameters =
      new HashMap<>();
    Map<String, Map<InstructionInputParams, String>> bucketMap = new HashMap<>();
    Map<InstructionInputParams, String> instructionInputParamsStringMap1 = new HashMap<>();
    instructionInputParamsStringMap1.put(InstructionInputParams.INGREDIENT_QUANTITY, "1.2");
    bucketMap.put("bucket 1", instructionInputParamsStringMap1);
    Map<InstructionInputParams, String> instructionInputParamsStringMap2 = new HashMap<>();
    instructionInputParamsStringMap2.put(InstructionInputParams.INGREDIENT_QUANTITY, "0.7");
    bucketMap.put("bucket 2", instructionInputParamsStringMap2);
    parameters.put(OutputParams.ONION_SIZE_SCORE, bucketMap);

    //Discrete Map
    Map<OutputParams, Map<String, Pair<Double, Double>>> discretizationMap = new HashMap<>();
    Map<String, Pair<Double, Double>> buckets = new HashMap<>();
    Pair<Double, Double> pair1 = new Pair<>(5.0, 7.0);
    Pair<Double, Double> pair2 = new Pair<>(7.0, 10.0);
    buckets.put("bucket 1", pair1);
    buckets.put("bucket 2", pair2);
    //System.out.println(buckets.get("bucket 1").first);
    discretizationMap.put(OutputParams.ONION_SIZE_SCORE, buckets);
    InstructionDecoration instructionDecoration =
      new InstructionDecoration(1.0, paramsDecorationWeight, parameters, discretizationMap);
    Map<Integer, InstructionDecoration> integerInstructionDecorationMap = new HashMap<>();
    integerInstructionDecorationMap.put(123, instructionDecoration);
    return new InstructionDecorationMap(integerInstructionDecorationMap);
  }

  public static InstructionLog createInstructionLogObject(String bucketScore) {
    bucketScore = bucketScore == null ? "5.2" : bucketScore;
    Map<ExecutionParams, String> executionParamsStringMap = new HashMap<>();
    executionParamsStringMap.put(ExecutionParams.TEMPERATURE, "30.00");
    executionParamsStringMap.put(ExecutionParams.PRE_DISPENSE_WEIGHT, "25");
    Map<OutputParams, String> outputParamsStringMap = new HashMap<>();
    outputParamsStringMap.put(OutputParams.ONION_SIZE_SCORE, bucketScore);
    return new InstructionLog(
      123, 0, executionParamsStringMap, outputParamsStringMap);
  }

  public static BaseDispense createInstructionObject(Integer quantity) {
    return new Dispense(123, 0, 50,
      Arrays.asList("salt", "paneer"), null, null, null,
      null);
  }

  public static InstructionDecorationMap createInstructionDecorationMapObjectComplex() {
    Map<OutputParams, Double> paramsDecorationWeight = new HashMap<>();
    paramsDecorationWeight.put(OutputParams.ONION_SIZE_SCORE, 0.8);
    paramsDecorationWeight.put(OutputParams.CONSISTENCY_SCORE, 0.2);
    //Parameters
    Map<OutputParams, Map<String, Map<InstructionInputParams, String>>> parameters =
      new HashMap<>();
    //ONION_SIZE_SCORE
    Map<String, Map<InstructionInputParams, String>> bucketMap = new HashMap<>();
    Map<InstructionInputParams, String> instructionInputParamsStringMap1 = new HashMap<>();
    instructionInputParamsStringMap1.put(InstructionInputParams.TARGET_TEMPERATURE, "1.2");
    instructionInputParamsStringMap1.put(InstructionInputParams.DEFAULT_WATTAGE, "1.2");
    instructionInputParamsStringMap1.put(InstructionInputParams.TIME_TO_COOK, "1.2");
    instructionInputParamsStringMap1.put(InstructionInputParams.INGREDIENT_QUANTITY, "1.2");
    bucketMap.put("bucket 1", instructionInputParamsStringMap1);
    Map<InstructionInputParams, String> instructionInputParamsStringMap2 = new HashMap<>();
    instructionInputParamsStringMap2.put(InstructionInputParams.TARGET_TEMPERATURE, "0.7");
    instructionInputParamsStringMap2.put(InstructionInputParams.DEFAULT_WATTAGE, "0.7");
    instructionInputParamsStringMap2.put(InstructionInputParams.TIME_TO_COOK, "0.7");
    instructionInputParamsStringMap2.put(InstructionInputParams.INGREDIENT_QUANTITY, "0.7");
    bucketMap.put("bucket 2", instructionInputParamsStringMap2);
    parameters.put(OutputParams.ONION_SIZE_SCORE, bucketMap);

    Map<String, Map<InstructionInputParams, String>> bucketMap2 = new HashMap<>();
    Map<InstructionInputParams, String> instructionInputParamsStringMap3 = new HashMap<>();
    instructionInputParamsStringMap3.put(InstructionInputParams.TARGET_TEMPERATURE, "1.5");
    instructionInputParamsStringMap3.put(InstructionInputParams.DEFAULT_WATTAGE, "1.5");
    instructionInputParamsStringMap3.put(InstructionInputParams.TIME_TO_COOK, "1.5");
    instructionInputParamsStringMap3.put(InstructionInputParams.INGREDIENT_QUANTITY, "1.5");
    bucketMap2.put("bucket 1", instructionInputParamsStringMap1);
    Map<InstructionInputParams, String> instructionInputParamsStringMap4 = new HashMap<>();
    instructionInputParamsStringMap4.put(InstructionInputParams.TARGET_TEMPERATURE, "0.7");
    instructionInputParamsStringMap4.put(InstructionInputParams.DEFAULT_WATTAGE, "0.7");
    instructionInputParamsStringMap4.put(InstructionInputParams.TIME_TO_COOK, "0.7");
    instructionInputParamsStringMap4.put(InstructionInputParams.INGREDIENT_QUANTITY, "0.7");
    bucketMap2.put("bucket 2", instructionInputParamsStringMap2);
    parameters.put(OutputParams.CONSISTENCY_SCORE, bucketMap2);

    //Discrete Map
    //ONION_SIZE_SCORE
    Map<OutputParams, Map<String, Pair<Double, Double>>> discretizationMap = new HashMap<>();
    Map<String, Pair<Double, Double>> buckets = new HashMap<>();
    Pair<Double, Double> pair1 = new Pair<>(5.0, 7.0);
    Pair<Double, Double> pair2 = new Pair<>(7.0, 10.0);
    buckets.put("bucket 1", pair1);
    buckets.put("bucket 2", pair2);
    //System.out.println(buckets.get("bucket 1").first);
    discretizationMap.put(OutputParams.ONION_SIZE_SCORE, buckets);
    Map<String, Pair<Double, Double>> buckets2 = new HashMap<>();
    Pair<Double, Double> pair3 = new Pair<>(3.0, 5.0);
    Pair<Double, Double> pair4 = new Pair<>(6.0, 10.0);
    buckets2.put("bucket 1", pair3);
    buckets2.put("bucket 2", pair4);
    //System.out.println(buckets.get("bucket 1").first);
    discretizationMap.put(OutputParams.CONSISTENCY_SCORE, buckets2);
    InstructionDecoration instructionDecoration =
      new InstructionDecoration(1.0, paramsDecorationWeight, parameters, discretizationMap);
    Map<Integer, InstructionDecoration> integerInstructionDecorationMap = new HashMap<>();
    integerInstructionDecorationMap.put(123, instructionDecoration);
    return new InstructionDecorationMap(integerInstructionDecorationMap);
  }

  public static InstructionLog createInstructionLogObjectComplex(String bucketScore) {
    bucketScore = bucketScore == null ? "5.2" : bucketScore;
    Map<ExecutionParams, String> executionParamsStringMap = new HashMap<>();
    executionParamsStringMap.put(ExecutionParams.TEMPERATURE, "30.00");
    executionParamsStringMap.put(ExecutionParams.PRE_DISPENSE_WEIGHT, "25");
    Map<OutputParams, String> outputParamsStringMap = new HashMap<>();
    outputParamsStringMap.put(OutputParams.ONION_SIZE_SCORE, bucketScore);
    outputParamsStringMap.put(OutputParams.CONSISTENCY_SCORE, "9.7");
    return new InstructionLog(
      123, 0, executionParamsStringMap, outputParamsStringMap);
  }

}
