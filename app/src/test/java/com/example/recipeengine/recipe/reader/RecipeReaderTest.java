package com.example.recipeengine.recipe.reader;

import com.example.recipeengine.instruction.Instruction;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.powermock.utils.Asserts;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecipeReaderTest {

  RecipeReader recipeReader;

  JSONObject recipeJsonObject;

  @Before
  public void setUp() throws Exception {

    Object object = null;
    try {
      object = new JSONParser().parse(new FileReader(
        "./src/main/java/com/example/recipeengine/resources/Recipe.json"));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    this.recipeJsonObject = (JSONObject) object;
  }

  @Test
  public void recipeReaderCreation() {
    Map<String, String> parameters = new HashMap<>();
    RecipeReaderComponent component = DaggerRecipeReaderComponent
      .builder()
      .recipeReaderModule(new RecipeReaderModule(this.recipeJsonObject, 0, parameters))
      .build();
    this.recipeReader = component.buildRecipeReader();
    Asserts.assertNotNull(this.recipeReader, "null recipe reader");
  }

  @Test
  public void getNextInstructions_returnsNonNullInstructionObject() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", String.valueOf(4));
    parameters.put("potato_size", String.valueOf(44));
    RecipeReaderComponent component = DaggerRecipeReaderComponent
      .builder()
      .recipeReaderModule(new RecipeReaderModule(this.recipeJsonObject, 0, parameters))
      .build();
    this.recipeReader = component.buildRecipeReader();
    Asserts.assertNotNull(this.recipeReader.getNextInstructions(), "null next instruction");
  }

  @Test
  public void getNextInstructions_dispenseInstructionTest() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", String.valueOf(4));
    parameters.put("potato_size", String.valueOf(44));
    RecipeReaderComponent component = DaggerRecipeReaderComponent
      .builder()
      .recipeReaderModule(new RecipeReaderModule(this.recipeJsonObject, 0, parameters))
      .build();
    this.recipeReader = component.buildRecipeReader();
    List<Instruction> inst = null;
    inst = this.recipeReader.getNextInstructions();
    inst = this.recipeReader.getNextInstructions();
    inst = this.recipeReader.getNextInstructions();
    Asserts.assertNotNull(inst, "null next instruction");
  }
}