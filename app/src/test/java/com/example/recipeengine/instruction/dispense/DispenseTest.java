package com.example.recipeengine.instruction.dispense;


import com.example.recipeengine.instruction.adaptor.DispenseAdaptor;
import com.example.recipeengine.instruction.dispense.decorator.IngredientQuantityDecorator;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class DispenseTest {

  Dispense dispense;
  BaseDispense adaptedDispense;
  ObjectMapper objectMapper;
  DispenseAdaptor dispenseAdaptorMock;

  @Before
  public void setUp() throws IOException {
    //Read JSON of dispense
    objectMapper = new ObjectMapper();
    dispenseAdaptorMock = Mockito.mock(DispenseAdaptor.class);
    dispense = new Dispense(123, 0, 30,
      Arrays.asList("salt", "paneer"), null, null,
      null, null);
    adaptedDispense = new IngredientQuantityDecorator(
      new Dispense(123, 0, 35,
        Arrays.asList("salt", "paneer"), null, null,
        null, null), null, null);

  }

  @Test
  public void checkInstructionId() {
    dispense.setInstructionId(dispense.getInstructionId());
    Assert.assertEquals(java.util.Optional.of(123).get(), dispense.getInstructionId());
  }

  @Test
  public void checkInstructionType() {
    Assert.assertTrue(dispense instanceof BaseDispense);
  }

  @Test
  public void checkAttemptNum() {
    Assert.assertEquals(java.util.Optional.of(0).get(), dispense.getAttemptNum());
  }

  @Test
  public void checkGetAdaptedDispenseInitial() {
    Assert.assertEquals(null, dispense.getAdaptedDispense());
  }

  @Test
  public void checkGetAdaptedDispensePostAdapt() throws Exception {
    Dispense dispenseSpy = Mockito.spy(dispense);
    Mockito.doReturn(dispenseAdaptorMock).when(dispenseSpy).makeDispenseAdaptorObject(dispenseSpy);
    Mockito.doReturn(adaptedDispense).when(dispenseAdaptorMock).adapt();
    dispenseSpy.adapt();
    Assert.assertNotNull(dispenseSpy.getAdaptedDispense());
  }


  @Test
  public void checkQuantityInGrams() {
    Assert.assertEquals(java.util.Optional.of(30).get(), dispense.getQuantityInGrams());
  }

  @Test
  public void checkIngredients() {
    Assert.assertEquals("salt", dispense.getIngredients().get(0));
  }

  @Test
  public void checkIngredientsImmutable() {
    Assert.assertTrue(true);
  }

  @Test
  public void checkDispenseAdaptorObject() {
    Assert.assertTrue(dispense.makeDispenseAdaptorObject(dispense) instanceof DispenseAdaptor);
  }

  @Test
  public void checkAdaptedDispenseQuantityInGrams() throws Exception {
    Dispense dispenseSpy = Mockito.spy(dispense);
    Mockito.doReturn(dispenseAdaptorMock).when(dispenseSpy).makeDispenseAdaptorObject(dispenseSpy);
    Mockito.doReturn(adaptedDispense).when(dispenseAdaptorMock).adapt();
    Assert.assertEquals(java.util.Optional.of(35).get(), dispenseSpy.adapt().getQuantityInGrams());
  }

}
