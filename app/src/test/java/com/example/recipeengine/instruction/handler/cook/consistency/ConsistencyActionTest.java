package com.example.recipeengine.instruction.handler.cook.consistency;



import com.example.recipeengine.instruction.cook.params.action.ActionParameters;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import junit.framework.TestCase;

import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.List;

import som.hardware.request.handler.RequestHandler;

import static org.mockito.Mockito.mock;

public class ConsistencyActionTest extends TestCase {

    ConsistencyAction consistencyAction;
    RequestHandler hwRequestHandler;

    public void setUp() throws Exception {
        super.setUp();
        this.hwRequestHandler = mock(RequestHandler.class);

        this.consistencyAction = new ConsistencyAction(this.hwRequestHandler, null, null);
    }

    public void testTakeAction() throws Exception {

        List<Double> score = new LinkedList<>();
        score.add(0.1);   ///Dummy score value
        final InferredResult result = new InferredResult(score);
        final ActionParameters actionParameters = new ActionParameters(0, 0, 95.0, 0.0, 0);
        final ConsistencyAction mockConsistencyAction = mock(ConsistencyAction.class);
        //case 0 -> no score, exception check
        Mockito.doThrow(IllegalArgumentException.class).when(mockConsistencyAction).takeAction(null, actionParameters);
        try {
            this.consistencyAction.takeAction(null, actionParameters);
        } catch (IllegalArgumentException e) {
            assertNotSame(new IllegalArgumentException(), e);
        }

        //case 0.5 -> null list in infer results

        try {
            this.consistencyAction.takeAction(new InferredResult(new LinkedList<>()), actionParameters);
        } catch (AssertionError e) {
            assertNotSame(new AssertionError(), e);
        }

//        case 1 -> intial score
        score.clear();
        score.add(0.5);
        this.consistencyAction.takeAction(result, actionParameters);
        boolean completed = this.consistencyAction.isInstCompleted();
        assertEquals("target not reached", false, completed);

        score.clear();
        ///case 2 -> reaching score
        score.add(0.75); /// score is still not enough to reach target
        this.consistencyAction.takeAction(result, actionParameters);
        completed = this.consistencyAction.isInstCompleted();
        assertEquals("target still not reached", false, completed);


        score.clear();
        score.add(0.94);
        this.consistencyAction.takeAction(result, actionParameters);
        completed = this.consistencyAction.isInstCompleted();
        assertEquals("target reached", true, completed);
    }

    public void testIsInstCompleted() {
        final boolean completed = this.consistencyAction.isInstCompleted();
        assertEquals("Getting instruction status", completed, false);
    }

    public void testPause() throws Exception {
        try {
            this.consistencyAction.pause();
        } catch (Exception e) {
            assertFalse(false);
        }
    }

    public void testCalcRemainingTime() {
        final int ret = this.consistencyAction.calcRemainingTimeSec();
        assertEquals("Nothing to test yet",0, ret);
    }

}