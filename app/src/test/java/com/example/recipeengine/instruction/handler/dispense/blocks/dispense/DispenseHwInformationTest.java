package com.example.recipeengine.instruction.handler.dispense.blocks.dispense;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DispenseHwInformationTest {

  DispenseHwInformation dispenseHwInformation;

  @Before
  public void setUp() {
    dispenseHwInformation =
        new DispenseHwInformation(new DispenseType(DispenseType.LIQUID),
            "leafy", "4", "macro4");
  }

  @Test
  public void checkDispenseType() {
    Assert.assertEquals(DispenseType.LIQUID,dispenseHwInformation.getHardwareSubsystem().getDispenseType());
  }

  @Test
  public void checkDispenseMechanism() {
    Assert.assertEquals("leafy",dispenseHwInformation.getDispenseMechanism());
  }

  @Test
  public void checkHardwareFormQuantity() {
    Assert.assertEquals("4",dispenseHwInformation.getHardwareFormQuantity());
  }

  @Test
  public void checkHardwareContainer() {
    Assert.assertEquals("macro4",dispenseHwInformation.getHardwareContainer());
  }

}
