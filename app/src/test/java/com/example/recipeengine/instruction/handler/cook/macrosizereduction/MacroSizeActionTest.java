package com.example.recipeengine.instruction.handler.cook.macrosizereduction;


import com.example.recipeengine.instruction.cook.params.action.ActionParameters;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import junit.framework.TestCase;

import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.List;


import io.reactivex.subjects.PublishSubject;
import som.hardware.request.handler.RequestHandler;

import static org.mockito.Mockito.mock;

public class MacroSizeActionTest extends TestCase {

	RequestHandler hwRequestHandler;
	PublishSubject<UIData> uiEmitter;
	PublishSubject<ProgressData> dataLogger;
	MacroSizeAction macroSizeAction;

	public void setUp() throws Exception {
		super.setUp();
		this.hwRequestHandler = mock(RequestHandler.class);
		this.dataLogger = PublishSubject.create();
		this.uiEmitter = PublishSubject.create();
		this.macroSizeAction = new MacroSizeAction(this.hwRequestHandler, this.uiEmitter, this.dataLogger);
	}

	public void testTakeAction() throws Exception {

		List<Double> score = new LinkedList<>();
		score.add(0.1);   ///Dummy score value
		final InferredResult result = new InferredResult(score);
		final ActionParameters actionParameters = new ActionParameters(0, 0, 95.0, 0.0, 0);
		final MacroSizeAction mockMacroSizeAction = mock(MacroSizeAction.class);
		//case 0 -> no score, exception check
		Mockito.doThrow(IllegalArgumentException.class).when(mockMacroSizeAction).takeAction(null, actionParameters);
		try {
			this.macroSizeAction.takeAction(null, actionParameters);
		} catch (IllegalArgumentException e) {
			assertNotSame(new IllegalArgumentException(), e);
		}
		//case 0.5 -> null score list
		try {
			this.macroSizeAction.takeAction(new InferredResult(new LinkedList<>()), actionParameters);
		} catch (AssertionError e) {
			assertNotSame(new AssertionError(), e);
		}
//        case 1 -> intial score
		score.clear();
		score.add(0.5);
		this.macroSizeAction.takeAction(result, actionParameters);
		boolean completed = this.macroSizeAction.isInstCompleted();
		assertEquals("target not reached", false, completed);

		score.clear();
		///case 2 -> reaching score
		score.add(0.75); /// score is still not enough to reach target
		this.macroSizeAction.takeAction(result, actionParameters);
		completed = this.macroSizeAction.isInstCompleted();
		assertEquals("target still not reached", false, completed);


		score.clear();
		score.add(0.94);
		this.macroSizeAction.takeAction(result, actionParameters);
		completed = this.macroSizeAction.isInstCompleted();
		assertEquals("target reached", true, completed);
		
	}

	public void testIsInstCompleted() {
		boolean completed = this.macroSizeAction.isInstCompleted();
		assertEquals("Getting instruction status", completed, false);
	}

	public void testPause() throws Exception {
		try {
			this.macroSizeAction.pause();
		} catch (Exception e) {
			assertFalse(false);
		}
	}

	public void testCalcRemainingTimeSec() {
		int val = this.macroSizeAction.calcRemainingTimeSec();
		assertEquals("Zero val return, not in use now", val, 0);
	}
}