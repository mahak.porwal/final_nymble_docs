package com.example.recipeengine.instruction.handler.cook.boiling;


import com.example.recipeengine.instruction.cook.params.action.ActionParameters;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import junit.framework.TestCase;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;
import som.hardware.request.handler.RequestHandler;

import static org.mockito.Mockito.mock;

public class BoilingActionTest extends TestCase {

	RequestHandler hwRequestHandler;
	PublishSubject<UIData> uiEmitter;
	PublishSubject<ProgressData> dataLogger;
	BoilingAction boilingAction;

	public void setUp() throws Exception {
		super.setUp();
		this.hwRequestHandler = mock(RequestHandler.class);
		this.dataLogger = PublishSubject.create();
		this.uiEmitter = PublishSubject.create();
		this.boilingAction = new BoilingAction(this.hwRequestHandler, this.uiEmitter, this.dataLogger);
	}

	public void testTakeAction() throws Exception {
		List<Double> score = new LinkedList<>();
		score.add(0.1);   ///Dummy score value
		final InferredResult result = new InferredResult(score);
		final ActionParameters actionParameters = new ActionParameters(0, 0, 95.0, 0.0, 0);

		//case 0 -> no score, exception check
//		final BoilingAction mockBoilingAcion = mock(BoilingAction.class);
//		Mockito.doThrow(IllegalArgumentException.class).when(mockBoilingAcion).takeAction(null, actionParameters);

		try {
			this.boilingAction.takeAction(null, actionParameters);
		} catch (IllegalArgumentException e) {
			assertNotSame(new IllegalArgumentException(), e);
		}

		//case 1/2 -> null score list
		try {
			this.boilingAction.takeAction(new InferredResult(new LinkedList<>()), actionParameters);
		} catch (AssertionError e) {
			assertNotSame(new AssertionError(), e);
		}

		//case 1 = result zero
		score.clear();
		score.add(0.0);
		this.boilingAction.takeAction(result, actionParameters);
		boolean completed = this.boilingAction.isInstCompleted();
		assertEquals("target not reached", false, completed);

		score.clear();
		///case 2 -> result 1.0
		score.add(1.00);
		this.boilingAction.takeAction(result, actionParameters);
		completed = this.boilingAction.isInstCompleted();
		assertEquals("target not reached", true, completed);
	}

	public void testIsInstCompleted() {
		boolean res = this.boilingAction.isInstCompleted();
		assertEquals("Getting instruction status", res, false);
	}

	public void testPause() {
		try {
			this.boilingAction.pause();
		} catch (Exception e) {
			assertFalse(false);
		}
	}

	public void testCalcRemainingTimeSec() {
		int val = this.boilingAction.calcRemainingTimeSec();
		assertEquals("Zero val return, not in use now", val, 0);
	}

	public void testUpdateActionStatus() {
		this.boilingAction.updateActionStatus();
		assertEquals("Action status check", "Boiling saute in progress", this.boilingAction.getBlockStatus());
	}

	public void testUpdateScoreStatus() {
		this.boilingAction.updateScoreStatus(0.0);
		assertEquals("Action status check", "Julia is trying to reach the target boiling by " +
						"constantly heating and stirring. " +
						"Currently, it has achieved 0.0" +
						" of the target and will reach the target " +
						"in approximately " + this.boilingAction.calcRemainingTimeSec() + " seconds", this.boilingAction.getScoreStatus());
	}

}