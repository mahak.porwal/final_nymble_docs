package com.example.recipeengine.instruction.blocks.capture;

import com.example.recipeengine.instruction.cook.params.capture.CaptureParameters;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import com.example.recipeengine.instruction.task.capture.base.CapturedDataProvider;
import com.example.recipeengine.instruction.task.capture.vision.VisionCapture;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import som.instruction.request.sensor.Sensors;

@RunWith(PowerMockRunner.class)
@PrepareForTest(BaseCaptureBlock.class)
public class BaseCaptureBlockTest {

  BaseCaptureBlock baseCaptureBlock;
  CapturableTask capturableTask;
  CaptureParameters captureParameters;

  @Before
  public void setUp() {
    capturableTask = Mockito.mock(CapturableTask.class);
    List<CapturableTask> capturableTasks = new ArrayList<>();
    capturableTasks.add(capturableTask);
    capturableTasks.add(capturableTask);
    List<String> taskNames = new ArrayList<String>() {{
      add("PRE_DISPENSE_IMAGE");
      add("PRE_DISPENSE_WEIGHT");
    }};
    captureParameters = new CaptureParameters(capturableTasks, taskNames);
    baseCaptureBlock = new BaseCaptureBlock(captureParameters);
  }

  @Test
  public void checkStartCapture() throws Exception {
    Mockito.doNothing().when(capturableTask).beginTask();
    BaseCaptureBlock baseCaptureBlockSpy = PowerMockito.spy(baseCaptureBlock);
    PowerMockito.doReturn(new ArrayList<CapturedTaskData>()).when(baseCaptureBlockSpy,
        PowerMockito.method(BaseCaptureBlock.class, "consolidateTaskResult"))
        .withArguments(Mockito.any(List.class));
    Assert.assertTrue(baseCaptureBlockSpy.getCapturedData() instanceof List);
  }

  @Test
  public void checkGetCapturedData() throws Exception {
    List<CapturableTask> capturableTasks = new ArrayList<>();
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 5,
            null,
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    capturableTasks.add(visionCapture);
    List<String> taskNames = new ArrayList<String>() {{
      add("PRE_DISPENSE_IMAGE");
      add("PRE_DISPENSE_WEIGHT");
    }};
    captureParameters = new CaptureParameters(capturableTasks, taskNames);
    BaseCaptureBlock captureBlock =
        new BaseCaptureBlock(captureParameters);
    List<CapturedTaskData> capturedTaskData = captureBlock.getCapturedData();
    Assert.assertEquals("PRE_DISPENSE_IMAGE", capturedTaskData.get(0).getTaskName());
  }

  @Test
  public void checkGetStatus() {
    Assert.assertEquals("Capture Status", baseCaptureBlock.getBlockStatus());
  }

  @Test
  public void checkConsolidateTaskResult() throws Exception {
    List<CapturableTask> capturableTasks = new ArrayList<>();
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 500,
            new TriggerType(TriggerType.STIRRER_STARTS),
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    capturableTasks.add(visionCapture);
    List<CapturedTaskData> capturedTaskData =
        Whitebox.invokeMethod(baseCaptureBlock, "consolidateTaskResult",
            capturableTasks);
    Assert.assertNotNull(capturedTaskData);
    System.out.println(capturedTaskData);
  }

  @Test
  public void checkConsolidateTaskResultExecuted() throws Exception {
    List<CapturableTask> capturableTasks = new ArrayList<>();
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 5,
            null,
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    List<CapturedDataProvider> executedTasks = new ArrayList<>();
    executedTasks.add(visionCapture);
    HashMap hashMap = new HashMap<Sensors, List<List<Double>>>();
    hashMap.put(Sensors.THERMAL_CAMERA, new ArrayList<>());
    visionCapture.setCapturedData(
        new CapturedTaskData(hashMap, null, null, "PRE_DISPENSE_IMAGE"));
    List<String> taskNames = new ArrayList<String>() {{
      add("PRE_DISPENSE_IMAGE");
    }};
    captureParameters = new CaptureParameters(null, taskNames);
    BaseCaptureBlock captureBlock =
        new BaseCaptureBlock(captureParameters);
    captureBlock.setExecutedTasks(executedTasks);
    List<CapturedTaskData> capturedTaskData = captureBlock.getCapturedData();
    Assert.assertNotNull(capturedTaskData.get(0).getSensorData().get(Sensors.THERMAL_CAMERA));
  }

  @Test
  public void checkConsolidateTaskResultExecutedException() throws Exception {
    List<CapturableTask> capturableTasks = new ArrayList<>();
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 5,
            null,
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    List<CapturedDataProvider> executedTasks = new ArrayList<>();
    executedTasks.add(visionCapture);
    HashMap hashMap = new HashMap<Sensors, List<List<Double>>>();
    hashMap.put(Sensors.THERMAL_CAMERA, new ArrayList<>());
    visionCapture.setCapturedData(
        new CapturedTaskData(hashMap, null, null,"PRE_DISPENSE_IMAGE" ));
    List<String> taskNames = new ArrayList<String>() {{
      add("PRE_DISPENSE_IMAGE");
      add("POST_DISPENSE_WEIGHT");
    }};
    captureParameters = new CaptureParameters(null, taskNames);
    BaseCaptureBlock captureBlock =
        new BaseCaptureBlock(captureParameters);
    captureBlock.setExecutedTasks(executedTasks);
    Assert.assertThrows(Exception.class, () -> {
      captureBlock.getCapturedData();
    });
  }

  @Test
  public void checkConsolidateTaskResultBothNotNull() throws Exception {
    List<CapturableTask> capturableTasks = new ArrayList<>();
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 5,
            null,
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    capturableTasks.add(visionCapture);
    List<String> taskNames = new ArrayList<String>() {{
      add("PRE_DISPENSE_IMAGE");
    }};
    captureParameters = new CaptureParameters(capturableTasks, taskNames);
    BaseCaptureBlock captureBlock =
        new BaseCaptureBlock(captureParameters);
    Assert.assertEquals("PRE_DISPENSE_IMAGE",
        captureBlock.getCapturedData().get(0).getTaskName());
  }

  /*
    Need to be tested
   */
  @Test
  public void checkCaptureBlockPause() {
    baseCaptureBlock.pause();
  }

}
