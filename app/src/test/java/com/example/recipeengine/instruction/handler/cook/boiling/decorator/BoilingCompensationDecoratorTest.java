package com.example.recipeengine.instruction.handler.cook.boiling.decorator;


import com.example.recipeengine.instruction.cook.BaseCook;
import junit.framework.TestCase;

import static com.example.recipeengine.resources.ConsistencyObjectResolver.createCookObject;

public class BoilingCompensationDecoratorTest extends TestCase {

	BaseCook baseCook;
	BoilingCompensationDecorator boilingCompensationDecorator;

	public void setUp() throws Exception {
		super.setUp();
		this.baseCook = createCookObject();
		this.boilingCompensationDecorator = new BoilingCompensationDecorator(this.baseCook);
	}

	public void testGetVisualScore() {
		assertEquals("Visual score", java.util.Optional.of(90.0).get(), this.boilingCompensationDecorator.getVisualScore());
	}
}