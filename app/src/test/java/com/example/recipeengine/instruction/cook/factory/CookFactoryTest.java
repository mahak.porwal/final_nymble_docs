package com.example.recipeengine.instruction.cook.factory;

import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.cook.Cook_Factory;
import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory;
import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.CookFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;
import com.example.recipeengine.instruction.info.cook.ActionInfoBuilder_Factory;
import com.example.recipeengine.instruction.info.cook.CaptureInfoBuilder_Factory;
import com.example.recipeengine.instruction.info.cook.CookInfo;
import com.example.recipeengine.instruction.info.cook.CookInfoFactory;
import com.example.recipeengine.instruction.info.cook.CookInfoFactory_Factory;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;
import com.example.recipeengine.instruction.info.cook.InferInfoBuilder_Factory;
import com.example.recipeengine.instruction.info.cook.ParametricFieldsBuilder_Factory;
import com.example.recipeengine.instruction.info.cook.StirrerInfoBuilder_Factory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class CookFactoryTest {

  CookFactory cookFactory;
  Cook cook;
  private String filename;
  CookInfoFactory cookInfoFactory;
  CookInfo cookInfo;

  @Before
  public void setUp() {
    this.cookFactory = CookFactory_Factory.newCookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(),
      CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(),
      ActionParamsFactory_Factory.newActionParamsFactory());
    filename = "./src/main/java/com/example/recipeengine/resources/Cook.json";
    cook = cookFactory.createCookObject(filename);
    cookInfoFactory = CookInfoFactory_Factory
      .newCookInfoFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
        CaptureInfoBuilder_Factory.newCaptureInfoBuilder(),
        InferInfoBuilder_Factory.newInferInfoBuilder(),
        ActionInfoBuilder_Factory.newActionInfoBuilder(),
        StirrerInfoBuilder_Factory.newStirrerInfoBuilder(),
        ParametricFieldsBuilder_Factory.newParametricFieldsBuilder());
    filename = "./src/main/java/com/example/recipeengine/resources/ParametricCook.json";
    cookInfo = cookInfoFactory.createCookInfo(filename);
  }

  @Test
  public void checkCookObject() {
    Assert.assertNotNull(cook);
  }

  @Test
  public void checkCookActionParameters() {
    Assert.assertEquals(java.util.Optional.of(200).get(), cook.getDefaultWattage());
  }

  @Test(expected = Exception.class)
  public void createCookObject_NonExecutableCookInfo() throws Exception {
    Map<String, String> parameters = new HashMap<>();
    this.cookFactory.createCookObject(this.cookInfo, parameters);
  }

  @Test(expected = NullPointerException.class)
  public void createCookObject_NullCookInfo() throws Exception {
    this.cookFactory.createCookObject(null, new HashMap<>());
  }

  @Test
  public void createCookObject_ExecutableCookInfo() throws Exception {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", "10");
    parameters.put("potato_size", "100");
    Cook cook = this.cookFactory.createCookObject(this.cookInfo, parameters);
    Assert.assertNotNull(cook);
  }
}
