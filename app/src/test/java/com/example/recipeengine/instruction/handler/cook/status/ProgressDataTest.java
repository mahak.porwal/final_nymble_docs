package com.example.recipeengine.instruction.handler.cook.status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProgressDataTest {

  ProgressData progressData;

  @Before
  public void setUp() {
    progressData = new ProgressData(null,null,null);
  }

  @Test
  public void getImageData() {
    progressData.setImageData(progressData.getImageData());
    Assert.assertEquals(null,progressData.getImageData());
  }

  @Test
  public void getExecutionData() {
    progressData.setExecutionData(progressData.getExecutionData());
    Assert.assertEquals(null,progressData.getExecutionData());
  }

  @Test
  public void getOutputData() {
    progressData.setOutputData(progressData.getOutputData());
    Assert.assertEquals(null,progressData.getOutputData());
  }

}
