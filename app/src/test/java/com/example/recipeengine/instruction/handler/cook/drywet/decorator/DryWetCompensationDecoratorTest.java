package com.example.recipeengine.instruction.handler.cook.drywet.decorator;


import com.example.recipeengine.instruction.cook.BaseCook;
import junit.framework.TestCase;

import static com.example.recipeengine.resources.ConsistencyObjectResolver.createCookObject;

public class DryWetCompensationDecoratorTest extends TestCase {
	BaseCook baseCook;
	DryWetCompensationDecorator dryWetCompensationDecorator;
	public void setUp() throws Exception {
		super.setUp();
		this.baseCook = createCookObject();   //manually setting a visual score
		this.dryWetCompensationDecorator = new DryWetCompensationDecorator(this.baseCook);
	}

	public void testGetVisualScore() {
		assertEquals("Visual score", java.util.Optional.of(90.0).get(), this.dryWetCompensationDecorator.getVisualScore());
	}
}