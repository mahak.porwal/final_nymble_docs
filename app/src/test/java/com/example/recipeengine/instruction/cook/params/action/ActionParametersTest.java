package com.example.recipeengine.instruction.cook.params.action;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ActionParametersTest {

  ActionParams actionParams;

  @Before
  public void setUp() {
    actionParams = new ActionParameters(30, 20, 5.2, 8.2, 200);
  }

  @Test
  public void checkTargetTemperature() {
    Assert.assertEquals(java.util.Optional.of(30).get(), actionParams.getTemperature());
  }

  @Test
  public void checkTimeToCook() {
    Assert.assertEquals(java.util.Optional.of(20).get(), actionParams.getTimeToCook());
  }

  @Test
  public void checkVisualScore() {
    Assert.assertEquals(java.util.Optional.of(5.2).get(), actionParams.getVisualScore());
  }

  @Test
  public void checkThermalScore() {
    Assert.assertEquals(java.util.Optional.of(8.2).get(), actionParams.getThermalScore());
  }

  @Test
  public void checkDefaultWattage() {
    Assert.assertEquals(java.util.Optional.of(200).get(), actionParams.getDefaultWattage());
  }

}
