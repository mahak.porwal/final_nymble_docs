package com.example.recipeengine.instruction.adaptor;


import com.example.recipeengine.instruction.cook.Cook;

import com.example.recipeengine.instruction.cook.decorator.CompensationDecorator;
import com.example.recipeengine.instruction.cook.decorator.ParamsDecorator;
import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DispenseAdaptor.class)
public class CookAdaptorTest {

  CookAdaptor cookAdaptor;
  CookAdaptor cookAdaptorSpy;
  Cook cook;

  @Before
  public void setUp() {
    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    this.cook = cookFactory.createCookObject(CookFactory.filename);
    cookAdaptor = new CookAdaptor(cook);
    cookAdaptorSpy = PowerMockito.spy(cookAdaptor);
  }

  @Test
  public void checkAdaptDecoration() throws Exception {
    //Verify If adapt() returns an Object of ParamsDecorator because attemptNum is 0
    PowerMockito.doNothing().when(cookAdaptorSpy, "saveInstruction");
    Assert.assertTrue(cookAdaptorSpy.adapt() instanceof ParamsDecorator);
  }

  @Test
  public void checkAdaptSaveInstructionCalled() throws Exception {
    PowerMockito.doNothing().when(cookAdaptorSpy, "saveInstruction");
    cookAdaptorSpy.adapt();
    PowerMockito.verifyPrivate(cookAdaptorSpy).invoke("saveInstruction");
  }

  @Test
  public void checkAdaptSaveInstructionImpl() {
    Assert.assertTrue(true);
  }

  @Test
  public void checkAdaptIncrementNumber() throws Exception {
    PowerMockito.doNothing().when(cookAdaptorSpy, "saveInstruction");
    Assert.assertEquals(java.util.Optional.of(1).get(), cookAdaptorSpy.adapt().getAttemptNum());
  }

  @Test
  public void checkApplyOneTimeDecoration() {
    Assert.assertTrue(cookAdaptor.applyOneTimeDecoration() instanceof ParamsDecorator);
  }

  @Test
  public void checkApplyCompensatoryDecoration() {
    Assert.assertTrue(cookAdaptor.applyCompensatoryDecoration() instanceof CompensationDecorator);
  }
}
