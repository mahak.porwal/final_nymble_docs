package com.example.recipeengine.instruction.task.capture.vision.threads;

import android.graphics.Bitmap;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.vision.BitmapUtility;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import som.hardware.request.handler.RequestHandler;
import som.hardware.response.SensorResponse;

public class ThermalCameraThreadTest {

  ThermalCameraThread thermalCameraThread;

  @Before
  public void setUp() {
    thermalCameraThread = new ThermalCameraThread("THERMAL_CAMERA",new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
        new ThermalType(ThermalType.IMAGE), 10,500, new TriggerType(TriggerType.STIRRER_STARTS),
        new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION),0.8,1.0),
        new ArrayList<Bitmap>(),null,null);
  }

  @Test
  public void checkThermalCameraThreadRun() throws Exception {
    RequestHandler requestHandlerMock = Mockito.mock(RequestHandler.class);
    BitmapUtility bitmapUtilityMock = Mockito.mock(BitmapUtility.class);
    ThermalCameraThread thermalCameraThreadSpy = Mockito.spy(thermalCameraThread);
    Map<String, List<Double>> sensorData= new HashMap<>();
    List<Double> list = new ArrayList<>();
    sensorData.put(VisualCaptureType.THERMAL_CAMERA,new ArrayList<Double>(
        Arrays.asList(10.0)));
    Mockito.doReturn(requestHandlerMock).when(thermalCameraThreadSpy).getRequestHandlerInstance();
    Mockito.doReturn(bitmapUtilityMock).when(thermalCameraThreadSpy).getBitmapUtilityInstance();
    Mockito.doReturn(new SensorResponse(10,true,sensorData)).when(requestHandlerMock).handleRequest(Mockito.any());
    Mockito.doReturn(null).when(bitmapUtilityMock).convertToBitmap(Mockito.anyList());

    thermalCameraThreadSpy.run();
  }

  @Test
  public void checkThermalCameraThreadName() {
    Assert.assertEquals("THERMAL_CAMERA",thermalCameraThread.getName());
  }

  @Test
  public void checkGetInstanceOfBitmap() {
    Assert.assertTrue(thermalCameraThread.getBitmapUtilityInstance() instanceof BitmapUtility);
  }

  @Test
  public void checkGetInstanceOfRequestHandler() {
    Assert.assertTrue(thermalCameraThread.getRequestHandlerInstance() instanceof RequestHandler);
  }

}
