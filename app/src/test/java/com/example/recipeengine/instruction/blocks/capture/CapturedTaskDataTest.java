package com.example.recipeengine.instruction.blocks.capture;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CapturedTaskDataTest {

  CapturedTaskData capturedTaskData;

  @Before
  public void setUp() {
    capturedTaskData = new CapturedTaskData();
  }

  @Test
  public void checkGetSensorData() {
    Assert.assertNotNull(capturedTaskData.getSensorData());
  }

  @Test
  public void checkGetVisionCamera() {
    Assert.assertNotNull(capturedTaskData.getVisionCamera());
  }

  @Test
  public void checkGetThermalCamera() {
    Assert.assertNotNull(capturedTaskData.getThermalCamera());
  }

}
