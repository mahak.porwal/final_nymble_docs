package com.example.recipeengine.instruction.handler.cook.action;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class BaseActionBlockTest {

  BaseActionBlock baseActionBlock;

  @Before
  public void setUp() {
    baseActionBlock = Mockito.mock(BaseActionBlock.class,Mockito.CALLS_REAL_METHODS);
  }

  @Test
  public void checkGetBlockStatus() {
    Assert.assertNull(baseActionBlock.getBlockStatus());
  }

  @Test
  public void checkGetScoreStatus() {
    Assert.assertNull(baseActionBlock.getScoreStatus());
  }

  @Test
  public void checkGetUiEmitter() {
    baseActionBlock.setUiEmitter(baseActionBlock.getUiEmitter());
    Assert.assertNull(baseActionBlock.getUiEmitter());
  }

  @Test
  public void checkGetDataLogger() {
    baseActionBlock.setDataLogger(baseActionBlock.getDataLogger());
    Assert.assertNull(baseActionBlock.getDataLogger());
  }

  @Test
  public void checkGetRemainingTimeInSec() {
    Assert.assertEquals(java.util.Optional.of(0).get(),baseActionBlock.calcRemainingTimeSec());
  }

  @Test
  public void checkGetInferredResult() {
    baseActionBlock.setInferredResult(baseActionBlock.getInferredResult());
    Assert.assertNull(baseActionBlock.getInferredResult());
  }

}
