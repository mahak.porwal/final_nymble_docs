package com.example.recipeengine.instruction.task.capture.weight;

import com.example.recipeengine.instruction.task.capture.SensorParams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class WeightCaptureTest {

  WeightCapture weightCapture;

  @Before
  public void setUp() {
    weightCapture = new WeightCapture("PRE_DISPENSE_WEIGHT",
        new SensorParams(null,null,10,10,
            null,null,null,null)
        );
  }

  @Test
  public void checkGetSensor() {
    Assert.assertEquals("WEIGHT_SENSOR",weightCapture.getSensor().getCaptureSensor());
  }

  @Test
  public void checkGetParamsNumOfSamples() {
    Assert.assertEquals(java.util.Optional.of(10).get(),weightCapture.getParams().getNumOfSamples());
  }

  @Test
  public void checkGetCapturedWeight() {
    Assert.assertNotNull(weightCapture.getCapturedWeight());
  }

  @Test
  public void checkBeginTaskCalledOnce() {
    WeightCapture weightCaptureSpy = Mockito.spy(weightCapture);
    WeightSensorThread weightSensorThreadMock = Mockito.mock(WeightSensorThread.class);
    Mockito.doReturn(weightSensorThreadMock).when(weightCaptureSpy).getWeightSensorThreadInstance();
    Mockito.doNothing().when(weightSensorThreadMock).start();
    weightCaptureSpy.beginTask();
    Mockito.verify(weightSensorThreadMock,Mockito.times(1)).start();
  }

  /*
  Bottleneck : Not Checking for thread.isAlive()
   */
  @Test
  public void checkAbortTask() throws InterruptedException {
    WeightCapture weightCaptureSpy = Mockito.spy(weightCapture);
    WeightSensorThread weightSensorThreadMock = Mockito.mock(WeightSensorThread.class);
    Mockito.doReturn(weightSensorThreadMock).when(weightCaptureSpy).getWeightSensorThreadInstance();
    Mockito.doNothing().when(weightSensorThreadMock).start();
    weightCaptureSpy.beginTask();
    weightCaptureSpy.abortTask();
    Mockito.verify(weightSensorThreadMock,Mockito.times(1)).interrupt();
  }

  @Test
  public void checkGetWeightSensorThreadInstance() {
    Assert.assertTrue(weightCapture.getWeightSensorThreadInstance() instanceof WeightSensorThread);
  }

}
