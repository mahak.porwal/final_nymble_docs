package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.instruction.cook.CookType;
import com.example.recipeengine.instruction.factory.cook.CookFactory;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CookTypeBuilderTest {

  CookTypeBuilder cookTypeBuilder;

  @Before
  public void setUp() throws Exception {
    this.cookTypeBuilder = new CookTypeBuilder();
  }

  @Test
  public void provideCookType() {
    CookType cookType = this.cookTypeBuilder.provideCookType(CookFactory.filename);
  }

  @Test
  public void testProvideCookType() {
  }
}