package com.example.recipeengine.instruction.task;

import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SensorParamsTest {

  SensorParams sensorParams;

  @Before
  public void setUp() {
     sensorParams = new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
        new ThermalType(ThermalType.IMAGE), 10,500,new TriggerType(TriggerType.STIRRER_STARTS),
        new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION),0.8,1.0);
  }

  @Test
  public void checkVisualCaptureType() {
    Assert.assertEquals("VISION_CAMERA",sensorParams.getVisualCaptureType().getVisualCaptureType());
  }

  @Test
  public void checkNumOfSamples() {
    Assert.assertEquals(java.util.Optional.of(10).get(),sensorParams.getNumOfSamples());
  }

  @Test
  public void checkTimeBetweenCaptureMS() {
    Assert.assertEquals(java.util.Optional.of(500).get(),sensorParams.getTimeBetweenCaptureMS());
  }

  @Test
  public void checkThermalType() {
    Assert.assertEquals("IMAGE",sensorParams.getThermalType().getThermalType());
  }

  @Test
  public void checkTriggerEvent() {
    Assert.assertEquals("STIRRER_STARTS",sensorParams.getTriggerEvent().getTriggerType());
  }

  @Test
  public void checkValidationType() {
    Assert.assertEquals("EXPOSURE_VALIDATION",sensorParams.getValidationType().getImageValidationType());
  }

  @Test
  public void checkValidationLowerScore() {
    Assert.assertEquals(java.util.Optional.of(0.8).get(),sensorParams.getValidationLowerScore());
  }

  @Test
  public void checkValidationHigherScore() {
    Assert.assertEquals(java.util.Optional.of(1.0).get(),sensorParams.getValidationHigherScore());
  }

}
