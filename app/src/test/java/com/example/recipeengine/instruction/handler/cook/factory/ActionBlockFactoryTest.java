package com.example.recipeengine.instruction.handler.cook.factory;

import com.example.recipeengine.instruction.handler.cook.consistency.ConsistencyAction;
import com.example.recipeengine.instruction.handler.cook.frying.FryingAction;
import org.junit.Assert;
import org.junit.Test;

public class ActionBlockFactoryTest {

  @Test
  public void checkActionBlockConsistency() {
    Assert.assertTrue(
        ActionBlockFactory.getActionBlockObject("consistency") instanceof ConsistencyAction);
  }

  @Test
  public void checkActionBlockFrying() {
    Assert.assertTrue(ActionBlockFactory.getActionBlockObject("frying") instanceof FryingAction);
  }

}
