package com.example.recipeengine.instruction.dispense.decorator;

import static com.example.recipeengine.resources.DispenseObjectsResolver.createInstructionDecorationMapObject;
import static com.example.recipeengine.resources.DispenseObjectsResolver.createInstructionDecorationMapObjectComplex;
import static com.example.recipeengine.resources.DispenseObjectsResolver.createInstructionLogObject;
import static com.example.recipeengine.resources.DispenseObjectsResolver.createInstructionLogObjectComplex;
import static com.example.recipeengine.resources.DispenseObjectsResolver.createInstructionObject;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.when;

import com.example.recipeengine.instruction.decorator.source.InstructionDecorationMap;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(IngredientQuantityDecorator.class)
public class IngredientQuantityDecoratorTest {

  IngredientQuantityDecorator ingredientQuantityDecorator;
  List<InstructionLog> instructionLogs;
  InstructionDecorationMap instructionDecorationMap;

  @Before
  public void setUp() {
    instructionLogs = new ArrayList<>();
    instructionLogs.add(createInstructionLogObject(null));
    ingredientQuantityDecorator =
        new IngredientQuantityDecorator(createInstructionObject(null), instructionLogs,
            createInstructionDecorationMapObject());
  }

  @Test
  public void checkGetQuantityInGramsBucket1() {
    Assert.assertEquals(java.util.Optional.of(60).get(),
        ingredientQuantityDecorator.getQuantityInGrams());
  }

  @Test
  public void checkGetQuantityInGramsNoWrappedObject() {
    //TO BE HANDLED
    IngredientQuantityDecorator ingredientQuantityDecorator2 =
        new IngredientQuantityDecorator(null, instructionLogs,
            createInstructionDecorationMapObject());
    Assert.assertThrows(NullPointerException.class, () -> {
      ingredientQuantityDecorator2.getQuantityInGrams();
    });
  }

  @Test
  public void checkGetQuantityInGramsNoInstructionLogs() {
    //TO BE HANDLED
    IngredientQuantityDecorator ingredientQuantityDecorator2 =
        new IngredientQuantityDecorator(createInstructionObject(null), null,
            createInstructionDecorationMapObject());
    Assert.assertThrows(NullPointerException.class, () -> {
      ingredientQuantityDecorator2.getQuantityInGrams();
    });
  }

  @Test
  public void checkGetQuantityInGramsNoDecoration() {
    //TO BE HANDLED
    IngredientQuantityDecorator ingredientQuantityDecorator2 =
        new IngredientQuantityDecorator(createInstructionObject(null), instructionLogs,
            null);
    Assert.assertEquals(java.util.Optional.of(50).get(),
        ingredientQuantityDecorator2.getQuantityInGrams());
  }

  @Test
  public void checkGetQuantityInGramsNegative() throws Exception {
    IngredientQuantityDecorator ingredientQuantityDecoratorSpy =
        PowerMockito.spy(ingredientQuantityDecorator);
    PowerMockito.doReturn(-100).when(ingredientQuantityDecoratorSpy, "deltaIngredientQuantity");
    Assert.assertEquals(java.util.Optional.of(0).get(),
        ingredientQuantityDecoratorSpy.getQuantityInGrams());
  }

  @Test
  public void checkGetQuantityInGramsNoBucket() {
    //TO BE HANDLED
    ArrayList<InstructionLog> instructionLogs2 = new ArrayList<>();
    instructionLogs2.add(createInstructionLogObject("1"));
    IngredientQuantityDecorator ingredientQuantityDecorator2 =
        new IngredientQuantityDecorator(createInstructionObject(null), instructionLogs2,
            createInstructionDecorationMapObject());
    Assert.assertThrows(NullPointerException.class, () -> {
      ingredientQuantityDecorator2.getQuantityInGrams();
    });
  }

  @Test
  public void checkGetQuantityInGramsComplex() {
    ArrayList<InstructionLog> instructionLogs2 = new ArrayList<>();
    instructionLogs2.add(createInstructionLogObjectComplex(null));
    IngredientQuantityDecorator ingredientQuantityDecorator2 =
        new IngredientQuantityDecorator(createInstructionObject(null), instructionLogs2,
            createInstructionDecorationMapObjectComplex());
    Assert.assertEquals(java.util.Optional.of(55).get(),
        ingredientQuantityDecorator2.getQuantityInGrams());
  }
}
