package com.example.recipeengine.instruction.handler.cook.status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UIDataTest {

  UIData uiData;

  @Before
  public void setUp() {
    uiData = new UIData(null,null);
  }

  @Test
  public void checkImage() {
    uiData.setImage(uiData.getImage());
    Assert.assertNull(uiData.getImage());
  }

  @Test
  public void checkData() {
    uiData.setData(uiData.getData());
    Assert.assertNull(uiData.getData());
  }

}
