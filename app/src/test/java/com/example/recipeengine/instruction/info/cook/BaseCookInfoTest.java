package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.util.MxEquationResolver;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class BaseCookInfoTest {

  CookInfo cookInfo;
  CookInfoFactory cookInfoFactory;

  @Before
  public void setUp() throws Exception {
    cookInfoFactory = CookInfoFactory_Factory
      .newCookInfoFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
        CaptureInfoBuilder_Factory.newCaptureInfoBuilder(),
        InferInfoBuilder_Factory.newInferInfoBuilder(),
        ActionInfoBuilder_Factory.newActionInfoBuilder(),
        StirrerInfoBuilder_Factory.newStirrerInfoBuilder(),
        ParametricFieldsBuilder_Factory.newParametricFieldsBuilder());
    this.cookInfo = cookInfoFactory.createCookInfo(
      "./src/main/java/com/example/recipeengine/resources/ParametricCook.json");
  }

  @Test
  public void resolveParametricFields_NoFieldsResolved() {
    Map<String, String> parameters = new HashMap<>();
    this.cookInfo.resolveParametricFields(parameters);
    assertEquals(5, this.cookInfo.getParametricFields().size());
  }

  @Test
  public void resolveParametricFields_OneFieldsResolved() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", String.valueOf(4));
    this.cookInfo.resolveParametricFields(parameters);
    assertEquals(4, this.cookInfo.getParametricFields().size());
  }

  @Test
  public void resolveParametricFields_AllFieldsResolved() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", String.valueOf(4));
    parameters.put("potato_size", String.valueOf(44));
    this.cookInfo.resolveParametricFields(parameters);
    assertEquals(0, this.cookInfo.getParametricFields().size());
  }

  @Test
  public void resolveParametricFields_AllFieldsResolvedInMoreThanOneAttempt() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", String.valueOf(4));
    this.cookInfo.resolveParametricFields(parameters);
    parameters.put("potato_size", String.valueOf(44));
    this.cookInfo.resolveParametricFields(parameters);
    assertEquals(0, this.cookInfo.getParametricFields().size());
  }


  @Test
  public void isExecutable_AllFieldsResolved() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", String.valueOf(4));
    parameters.put("potato_size", String.valueOf(44));
    this.cookInfo.resolveParametricFields(parameters);
    assertTrue(this.cookInfo.isExecutable());
  }

  @Test
  public void isExecutable_AllFieldsNotResolved() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("potato_size", String.valueOf(44));
    this.cookInfo.resolveParametricFields(parameters);
    assertFalse(this.cookInfo.isExecutable());
  }

  @Test
  public void getActionInfo_TemperatureFieldResolved() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", String.valueOf(4));
    parameters.put("potato_size", String.valueOf(44));
    this.cookInfo.resolveParametricFields(parameters);
    ActionInfo actionInfo = this.cookInfo.getActionInfo();
    assertTrue(MxEquationResolver.isResolvedExpression(actionInfo.getTemperatureString()));
  }

  @Test
  public void getStirrerInfo_StirringPercentFieldResolved() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", String.valueOf(4));
    parameters.put("potato_size", String.valueOf(44));
    this.cookInfo.resolveParametricFields(parameters);
    StirrerInfo stirrerInfo = this.cookInfo.getStirrerInfo();
    assertTrue(MxEquationResolver.isResolvedExpression(stirrerInfo.getStirringPercentString()));
  }

  @Test
  public void getActionInfo_TemperatureFieldNotResolved() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("potato_size", String.valueOf(44));
    this.cookInfo.resolveParametricFields(parameters);
    ActionInfo actionInfo = this.cookInfo.getActionInfo();
    assertFalse(MxEquationResolver.isResolvedExpression(actionInfo.getTemperatureString()));
  }

  @Test
  public void getStirrerInfo_StirringPercentFieldNotResolved() {
    Map<String, String> parameters = new HashMap<>();
    parameters.put("serving_size", String.valueOf(4));
    this.cookInfo.resolveParametricFields(parameters);
    StirrerInfo stirrerInfo = this.cookInfo.getStirrerInfo();
    assertFalse(MxEquationResolver.isResolvedExpression(stirrerInfo.getStirringPercentString()));
  }
}