package com.example.recipeengine.instruction.task.action;

import com.example.recipeengine.instruction.cook.params.stirrer.StirrerProfile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ActionTaskParamsTest {

  ActionTaskParams actionTaskParams;

  @Before
  public void setUp() {
    actionTaskParams = new ActionTaskParams(new StirrerProfile(20, 20, 20), 105.0, 5, 5,false);
  }

  @Test
  public void checkStirrerParamsStirringPercent() {
    Assert.assertEquals(java.util.Optional.of(20).get(),actionTaskParams.getStirrerParams().getStirringPercent());
  }

  @Test
  public void checkActuatorSpeed() {
    Assert.assertEquals(java.util.Optional.of(100.0).get(),actionTaskParams.getActuatorSpeed());
  }

  @Test
  public void checkActuatorFreq() {
    Assert.assertEquals(java.util.Optional.of(5).get(),actionTaskParams.getActuatorFreq());
  }

  @Test
  public void checkHeatLevel() {
    Assert.assertEquals(java.util.Optional.of(5).get(),actionTaskParams.getHeatLevel());
  }
}
