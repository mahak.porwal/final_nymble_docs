package com.example.recipeengine.instruction.dispense.factory;

import com.example.recipeengine.instruction.dispense.Dispense;
import com.example.recipeengine.instruction.factory.dispense.DispenseFactory;

import com.example.recipeengine.instruction.task.action.BaseActionableTask;

import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DispenseFactoryTest {

  DispenseFactory dispenseFactory;

  @Before
  public void setUp() {
    this.dispenseFactory = new DispenseFactory();
  }

  @Test
  public void checkDispenseObject() throws Exception {
    Dispense dispense = this.dispenseFactory.createDispenseObject(
        "./src/main/java/com/example/recipeengine/resources/Dispense.json");
    Assert.assertNotNull(dispense);

  }

  @Test
  public void checkDispenseIngredientListSize() throws Exception {
    Assert.assertEquals(2, this.dispenseFactory.createDispenseObject(
      "./src/main/java/com/example/recipeengine/resources/Dispense.json")
      .getIngredients().size());
  }

  @Test
  public void checkPreDispenseBlockSize() throws Exception {
    Assert.assertEquals(4, this.dispenseFactory.createDispenseObject(
      "./src/main/java/com/example/recipeengine/resources/Dispense.json")
      .getPreDispenseBlock().getSequentialTasks().size());
  }

  @Test
  public void checkPostDispenseBlockSize() throws Exception {
    final Dispense dispense = this.dispenseFactory.createDispenseObject(
      "./src/main/java/com/example/recipeengine/resources/Dispense.json");
    Assert.assertEquals(3, dispense
      .getPostDispenseBlock().getSequentialTasks().size());
  }

  @Test
  public void checkPostDispenseBlockActionableTask() throws Exception {
    final Dispense dispense = this.dispenseFactory.createDispenseObject(
      "./src/main/java/com/example/recipeengine/resources/Dispense.json");
    Assert.assertTrue(dispense
      .getPostDispenseBlock().getSequentialTasks().get(2) instanceof BaseActionableTask);
  }

  @Test
  public void checkPostDispenseBlockActionableTaskValues() throws Exception {
    final Dispense dispense = this.dispenseFactory.createDispenseObject(
      "./src/main/java/com/example/recipeengine/resources/Dispense.json");
    Assert.assertEquals("turnOnFan", dispense
      .getPostDispenseBlock().getSequentialTasks().get(2).getTaskName());
  }
}
