package com.example.recipeengine.instruction.task.action;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import som.instruction.request.actuator.Actuators;

public class ActuatorsTest {

  Actuators actuators;

  @Before
  public void setUp() {
    actuators = new Actuators(Actuators.DESIGN_LED);
  }

  @Test
  public void checkGetActuator() {
    Assert.assertEquals("DESIGN_LED", actuators.getActuator());
  }

}
