package com.example.recipeengine.instruction.cook.params.capture;

import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.vision.VisionCapture;
import com.example.recipeengine.instruction.task.capture.weight.WeightCapture;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CaptureParametersTest {

  CaptureParams captureParams;

  @Before
  public void setUp() {
    SensorParams sensorParams = new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
        new ThermalType(ThermalType.IMAGE), 10,500, new TriggerType(TriggerType.STIRRER_STARTS),
        new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION),0.8,1.0);
    VisionCapture visionCapture = new VisionCapture("PRE_DISPENSE_IMAGE",sensorParams);
    WeightCapture weightCapture = new WeightCapture("PRE_DISPENSE_WEIGHT",
        new SensorParams(null,null,10,10,
            null,null,null,null)
        );
    List<CapturableTask> capturableTasks = new ArrayList<>();
    capturableTasks.add(visionCapture);
    capturableTasks.add(weightCapture);
    List<String> taskNames = new ArrayList<String>() {{
      add("PRE_DISPENSE_IMAGE");
      add("POST_DISPENSE_WEIGHT");
    }};

    captureParams = new CaptureParameters(capturableTasks,taskNames);
  }

  @Test
  public void checkCaptureParamsListSize() {
    Assert.assertEquals(2,captureParams.getCaptureTasks().size());
  }

//  @Test
//  public void checkTimeBetweenCaptureMS() {
//    Assert.assertEquals(
//        java.util.Optional.of(500).get(),captureParams.getCaptureTasks().get(0).getParams().getTimeBetweenCaptureMS());
//  }

}
