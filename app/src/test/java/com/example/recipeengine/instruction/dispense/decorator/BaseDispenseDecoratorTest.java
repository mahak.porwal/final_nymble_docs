package com.example.recipeengine.instruction.dispense.decorator;

import static com.example.recipeengine.resources.DispenseObjectsResolver.createInstructionObject;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BaseDispenseDecoratorTest {

  BaseDispenseDecorator baseDispenseDecorator;

  @Before
  public void setUp() {
    baseDispenseDecorator = new BaseDispenseDecorator(createInstructionObject(null));
  }

  @Test
  public void checkQuantityInGrams() {
    Assert
        .assertEquals(java.util.Optional.of(50).get(), baseDispenseDecorator.getQuantityInGrams());
  }

  @Test
  public void checkGetAttemptNum() {
    Assert.assertEquals(java.util.Optional.of(0).get(), baseDispenseDecorator.getAttemptNum());
  }

  @Test
  public void checkGetIngredients() {
    Assert.assertEquals(java.util.Optional.of("salt").get(),
        baseDispenseDecorator.getIngredients().get(0));
  }

  @Test
  public void checkInstructionId() {
    Assert.assertEquals(java.util.Optional.of(123).get(), baseDispenseDecorator.getInstructionId());
  }
}
