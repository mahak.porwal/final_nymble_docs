package com.example.recipeengine.instruction.handler.cook.infer;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InferredResultTest {

  InferredResult inferredResult;

  @Before
  public void setUp() {
    inferredResult = new InferredResult(new ArrayList<>());
  }

  @Test
  public void checkGetResult() {
    inferredResult.setResult(inferredResult.getResult());
    Assert.assertNotNull(inferredResult.getResult());
  }

}
