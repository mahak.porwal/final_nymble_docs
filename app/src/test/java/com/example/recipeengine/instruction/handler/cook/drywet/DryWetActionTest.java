package com.example.recipeengine.instruction.handler.cook.drywet;


import static org.mockito.Mockito.mock;

import com.example.recipeengine.instruction.cook.params.action.ActionParameters;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import io.reactivex.subjects.PublishSubject;
import java.util.LinkedList;
import java.util.List;
import junit.framework.TestCase;
import org.mockito.Mockito;
import som.hardware.request.handler.RequestHandler;

public class DryWetActionTest extends TestCase {

  DryWetAction dryWetAction;
  RequestHandler hwRequestHandler;
  PublishSubject<UIData> uiEmitter;
  PublishSubject<ProgressData> dataLogger;

  public void setUp() throws Exception {
    super.setUp();
    this.hwRequestHandler = mock(RequestHandler.class);
    this.dataLogger = PublishSubject.create();
    this.uiEmitter = PublishSubject.create();
    this.dryWetAction = new DryWetAction(this.hwRequestHandler, this.uiEmitter, this.dataLogger);
  }

  public void testTakeAction() throws Exception {
    List<Double> score = new LinkedList<>();
    score.add(0.1);   ///Dummy score value
    final InferredResult result = new InferredResult(score);
    final ActionParameters actionParameters = new ActionParameters(0, 0, 95.0, 0.0, 0);
    final DryWetAction mockdryWetAction = mock(DryWetAction.class);
    //case 0 -> no score, exception check
    Mockito.doThrow(IllegalArgumentException.class).when(mockdryWetAction)
        .takeAction(null, actionParameters);

    try {
      this.dryWetAction.takeAction(null, actionParameters);
    } catch (IllegalArgumentException e) {
      assertNotSame(new IllegalArgumentException(), e);
    }


    //case 0.5 -> null score list
    try {
      this.dryWetAction.takeAction(new InferredResult(new LinkedList<>()), actionParameters);
    } catch (AssertionError e) {
      assertNotSame(new AssertionError(), e);
    }
//        case 1 -> intial score
    score.clear();
    score.add(0.5);
    this.dryWetAction.takeAction(result, actionParameters);
    boolean completed = this.dryWetAction.isInstCompleted();
    assertEquals("target not reached", false, completed);

    score.clear();
    ///case 2 -> reaching score
    score.add(0.75); /// score is still not enough to reach target
    this.dryWetAction.takeAction(result, actionParameters);
    completed = this.dryWetAction.isInstCompleted();
    assertEquals("target still not reached", false, completed);


    score.clear();
    score.add(1 - 0.94);
    this.dryWetAction.takeAction(result, actionParameters);
    completed = this.dryWetAction.isInstCompleted();
    assertEquals("target reached", true, completed);
  }

  public void testIsInstCompleted() {
    boolean completed = this.dryWetAction.isInstCompleted();
    assertEquals("Getting instruction status", completed, false);
  }

  public void testPause() throws Exception {
    try {
      this.dryWetAction.pause();
    } catch (Exception e) {
      assertFalse(false);
    }
  }

  public void testCalcRemainingTimeSec() {
    int val = this.dryWetAction.calcRemainingTimeSec();
    assertEquals("Zero val return, not in use now", val, 0);
  }

  public void testActionStatus() {
    this.dryWetAction.updateActionStatus();
    assertEquals("update action status", "Dry wet saute in progress",
        this.dryWetAction.getBlockStatus());
  }

  public void testScoreStatus() {
    this.dryWetAction.updateScoreStatus(0.0);
    assertEquals("update score status", "Julia is trying to reach the target consistency by " +
            "constantly heating and stirring. " +
            "Currently, it has achieved 0.0" +
            " of the target consistency and will reach the target consistency " +
            "in approximately " + this.dryWetAction.calcRemainingTimeSec() + " seconds",
        this.dryWetAction.getScoreStatus());
  }


}