package com.example.recipeengine.instruction.decorator.source;

import com.example.recipeengine.instruction.decorator.exchanges.ExecutionParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InstructionLogTest {

  InstructionLog instructionLog;

  @Before
  public void setUp() {
    Map<ExecutionParams, String> executionParamsStringMap = new HashMap<>();
    executionParamsStringMap.put(ExecutionParams.TEMPERATURE, "30.00");
    executionParamsStringMap.put(ExecutionParams.PRE_DISPENSE_WEIGHT, "25");
    Map<OutputParams, String> outputParamsStringMap = new HashMap<>();
    outputParamsStringMap.put(OutputParams.ONION_SIZE_SCORE, "5.2");
    instructionLog = new InstructionLog(
        123, 0, executionParamsStringMap, outputParamsStringMap);
  }

  @Test
  public void checkInstructionId() {
    instructionLog.setInstructionId(instructionLog.getInstructionId());
    Assert.assertEquals(java.util.Optional.of(123).get(), instructionLog.getInstructionId());
  }

  @Test
  public void checkAttemptNum() {
    instructionLog.setAttemptNum(instructionLog.getAttemptNum());
    Assert.assertEquals(java.util.Optional.of(0).get(), instructionLog.getAttemptNum());
  }

  @Test
  public void checkExecutionParamsMap() {
    instructionLog.setExecutionParams(instructionLog.getExecutionParams());
    Assert.assertEquals(java.util.Optional.of("25").get(),
        instructionLog.getExecutionParams().get(ExecutionParams.PRE_DISPENSE_WEIGHT));
  }

  @Test
  public void checkOutputParamsMap() {
    instructionLog.setOutputParams(instructionLog.getOutputParams());
    Assert.assertEquals(java.util.Optional.of("5.2").get(),
        instructionLog.getOutputParams().get(OutputParams.ONION_SIZE_SCORE));
  }
}
