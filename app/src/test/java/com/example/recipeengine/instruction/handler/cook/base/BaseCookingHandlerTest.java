package com.example.recipeengine.instruction.handler.cook.base;

import com.example.recipeengine.instruction.cook.Cook;

import com.example.recipeengine.instruction.cook.params.stirrer.StirrerProfile;

import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;
import com.example.recipeengine.instruction.handler.cook.action.ActionBlock;
import com.example.recipeengine.instruction.blocks.capture.CaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.handler.cook.infer.InferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;

import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;


import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import som.hardware.request.handler.RequestHandler;

@RunWith(PowerMockRunner.class)
@PrepareForTest(BaseCookingHandler.class)
public class BaseCookingHandlerTest {

  BaseCookingHandler baseCookingHandler;
  Cook cook;

  @Before
  public void setUp() {
    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    this.cook = cookFactory.createCookObject(CookFactory.filename);

    baseCookingHandler = Mockito.mock(BaseCookingHandler.class, Mockito.CALLS_REAL_METHODS);
  }

  @Test
  public void checkSetUpStirring() throws Exception {
    baseCookingHandler.setIsCompleted(false);
    RequestHandler requestHandlerMock = Mockito.mock(RequestHandler.class);
    Mockito.doReturn(requestHandlerMock).when(baseCookingHandler).getNewRequestHandlerInstance();
    Mockito.doReturn(null).when(requestHandlerMock).handleRequest(Mockito.any());
    baseCookingHandler.setUpStirring(new Cook(0,0,null, null, null,
      new StirrerProfile(5, 75, 9), null));
    //Run at least one cycle
    Thread.sleep(7000);
    baseCookingHandler.setIsCompleted(true);
  }

  @Test
  public void checkHandle() throws Exception {
    BaseCookingHandler baseCookingHandlerMock = PowerMockito.mock(BaseCookingHandler.class);
    //PowerMockito.doCallRealMethod().when(baseCookingHandler).setUpStirring(Mockito.any());
    PowerMockito.doCallRealMethod().when(baseCookingHandlerMock).handle();
    PowerMockito.doCallRealMethod().when(baseCookingHandlerMock).setIsCompleted(true);
    CaptureBlock captureBlock = Mockito.mock(CaptureBlock.class);
    InferBlock inferBlock = Mockito.mock(InferBlock.class);
    ActionBlock actionBlock = Mockito.mock(ActionBlock.class);
    Whitebox.setInternalState(baseCookingHandlerMock, "cook", cook);
    Whitebox.setInternalState(baseCookingHandlerMock, "captureBlock", captureBlock);
    Whitebox.setInternalState(baseCookingHandlerMock, "inferBlock", inferBlock);
    Whitebox.setInternalState(baseCookingHandlerMock, "actionBlock", actionBlock);

    Mockito.doReturn(new ArrayList<CapturedTaskData>()).when(captureBlock).getCapturedData();
    Mockito.doReturn(new InferredResult(null)).when(inferBlock).infer(Mockito.any());
    Mockito.doNothing().when(actionBlock).takeAction(Mockito.any(), Mockito.any());
    new Thread(() -> {
      try {
        Thread.currentThread().sleep(3000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      baseCookingHandlerMock.setIsCompleted(true);
    }).start();
    baseCookingHandlerMock.handle();
  }

  @Test
  public void checkPause() throws Exception {
    BaseCookingHandler baseCookingHandlerMock = PowerMockito.mock(BaseCookingHandler.class);
    PowerMockito.doCallRealMethod().when(baseCookingHandlerMock).pause();
    Thread thread = Mockito.mock(Thread.class);
    CaptureBlock captureBlock = Mockito.mock(CaptureBlock.class);
    Whitebox.setInternalState(baseCookingHandlerMock, "stirrerThread", thread);
    Whitebox.setInternalState(baseCookingHandlerMock, "currentBlock", captureBlock);
    Mockito.doNothing().when(thread).interrupt();
    Mockito.doNothing().when(captureBlock).pause();
    baseCookingHandlerMock.pause();
  }

  @Test
  public void checkGetNewRequestHandlerInstance() {
    Assert.assertTrue(baseCookingHandler.getNewRequestHandlerInstance() instanceof RequestHandler
    );
  }

}
