package com.example.recipeengine.instruction.handler.cook.factory;

import com.example.recipeengine.instruction.blocks.capture.BaseCaptureBlock;
import org.junit.Assert;
import org.junit.Test;

public class CaptureBlockFactoryTest {

  @Test
  public void checkCaptureBlockObject() {
    Assert.assertTrue(CaptureBlockFactory.getCaptureBlockObject() instanceof BaseCaptureBlock);
  }

}
