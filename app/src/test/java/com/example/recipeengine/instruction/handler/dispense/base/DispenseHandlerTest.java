package com.example.recipeengine.instruction.handler.dispense.base;

import com.example.recipeengine.instruction.blocks.capture.CaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.dispense.Dispense;
import com.example.recipeengine.instruction.handler.cook.action.ActionBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.base.Task;
import com.example.recipeengine.instruction.task.capture.vision.VisionCapture;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class DispenseHandlerTest {

  DispenseHandler dispenseHandler;
  PreDispenseBlock preDispenseBlock;
  DispenseBlock dispenseBlock;
  PostDispenseBlock postDispenseBlock;
  InferInstruction inferInstruction;
  List<Task> tasks;

  @Before
  public void setUp() {
    preDispenseBlock = Mockito.mock(PreDispenseBlock.class);
    dispenseBlock = Mockito.mock(DispenseBlock.class);
    postDispenseBlock = Mockito.mock(PostDispenseBlock.class);
    inferInstruction = Mockito.mock(InferInstruction.class);
    List<InferInstruction> instructionList = new ArrayList<>();
    Dispense dispense = new Dispense(123, 0, 50,
          Arrays.asList("salt", "paneer"),preDispenseBlock,dispenseBlock,postDispenseBlock,instructionList);
    dispenseHandler = new DispenseHandler(dispense);
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 500,
            null,
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    tasks = new ArrayList<Task>() {{
      add(visionCapture);
    }};
  }

  @Test
  public void checkHandleDispense() throws Exception {
    Mockito.doNothing().when(preDispenseBlock).startSequentialTasks();
    Mockito.doNothing().when(dispenseBlock).startParallelTasks();
    Mockito.doNothing().when(postDispenseBlock).startSequentialTasks();
    Mockito.doReturn(tasks).when(preDispenseBlock).getSequentialTasks();
    Mockito.doReturn(tasks).when(dispenseBlock).getParallelTasks();
    Mockito.doReturn(tasks).when(postDispenseBlock).getSequentialTasks();

    dispenseHandler.handle();
  }

  @Test
  public void checkHandleInferInstruction() throws Exception {
    List<InferInstruction> instructionList = new ArrayList<>();
    instructionList.add(inferInstruction);
    Dispense dispense = new Dispense(123, 0,  50,Arrays.asList("salt", "paneer"),
      preDispenseBlock,dispenseBlock,postDispenseBlock,instructionList);
    DispenseHandler dispenseHandler2 = new DispenseHandler(dispense);
    Mockito.doNothing().when(preDispenseBlock).startSequentialTasks();
    Mockito.doNothing().when(dispenseBlock).startParallelTasks();
    Mockito.doNothing().when(postDispenseBlock).startSequentialTasks();
    CaptureBlock captureBlockMock = Mockito.mock(CaptureBlock.class);
    InferBlock inferBlockMock = Mockito.mock(InferBlock.class);
    ActionBlock actionBlockMock = Mockito.mock(ActionBlock.class);
    Mockito.doReturn(tasks).when(preDispenseBlock).getSequentialTasks();
    Mockito.doReturn(tasks).when(dispenseBlock).getParallelTasks();
    Mockito.doReturn(tasks).when(postDispenseBlock).getSequentialTasks();

    Mockito.doReturn(captureBlockMock).when(inferInstruction).getCaptureBlock();
    Mockito.doReturn(Arrays.asList(new CapturedTaskData())).when(captureBlockMock).getCapturedData();
    Mockito.doReturn(inferBlockMock).when(inferInstruction).getInferBlock();
    Mockito.doReturn(new InferredResult(null)).when(inferBlockMock).infer(Mockito.any());
    Mockito.doReturn(actionBlockMock).when(inferInstruction).getActionBlock();
    Mockito.doNothing().when(actionBlockMock).takeAction(Mockito.any(),Mockito.any());

    dispenseHandler2.handle();
  }

  @Test
  public void checkGetBlockStatus() throws Exception {
    Mockito.doNothing().when(preDispenseBlock).startSequentialTasks();
    Mockito.doNothing().when(dispenseBlock).startParallelTasks();
    Mockito.doNothing().when(postDispenseBlock).startSequentialTasks();
    Mockito.doReturn(tasks).when(preDispenseBlock).getSequentialTasks();
    Mockito.doReturn(tasks).when(dispenseBlock).getParallelTasks();
    Mockito.doReturn(tasks).when(postDispenseBlock).getSequentialTasks();
    Mockito.doReturn("Post Dispense Block").when(postDispenseBlock).getBlockStatus();
    dispenseHandler.handle();
    Assert.assertEquals("Post Dispense Block",dispenseHandler.getStatus().getStatus());
  }

}
