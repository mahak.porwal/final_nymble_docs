package com.example.recipeengine.instruction.cook.params.infer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InferParametersTest {

  InferParams inferParams;

  @Before
  public void setUp() {
    inferParams = new InferParameters("CONSISTENCY_KHEER","CONSISTENCY_KHEER.model");
  }

  @Test
  public void checkGetModelId() {
    Assert.assertEquals("CONSISTENCY_KHEER",inferParams.getModelId());
  }

  @Test
  public void checkGetModelFilename() {
    Assert.assertEquals("CONSISTENCY_KHEER.model",inferParams.getModelFileName());
  }

}
