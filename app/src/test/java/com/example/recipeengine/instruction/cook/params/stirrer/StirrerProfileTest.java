package com.example.recipeengine.instruction.cook.params.stirrer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StirrerProfileTest {

  StirrerParams stirrerParams;

  @Before
  public void setUp() {
    stirrerParams = new StirrerProfile(20, 75, 18);
  }

  @Test
  public void checkCycleDurationSeconds() {
    Assert.assertEquals(java.util.Optional.of(20).get(), stirrerParams.getCycleDurationSeconds());
  }

  @Test
  public void checkStirringPercent() {
    Assert.assertEquals(java.util.Optional.of(75).get(), stirrerParams.getStirringPercent());
  }

  @Test
  public void checkStirringSpeed() {
    Assert.assertEquals(java.util.Optional.of(18).get(), stirrerParams.getStirringSpeed());
  }

}
