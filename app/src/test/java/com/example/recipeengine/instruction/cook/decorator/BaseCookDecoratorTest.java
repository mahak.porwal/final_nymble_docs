package com.example.recipeengine.instruction.cook.decorator;

import com.example.recipeengine.instruction.cook.BaseCook;
import com.example.recipeengine.instruction.cook.Cook;

import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BaseCookDecoratorTest {

  BaseCookDecorator baseCookDecorator;

  @Before
  public void setUp() {
    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    Cook cook = cookFactory.createCookObject(CookFactory.filename);
    baseCookDecorator = new BaseCookDecorator(cook);
  }

  @Test
  public void checkInstructionId() {
    Assert.assertEquals(java.util.Optional.of(0).get(), baseCookDecorator.getInstructionId());
  }

  @Test
  public void checkInstructionType() {
    Assert.assertTrue(baseCookDecorator instanceof BaseCook);
  }

  @Test
  public void checkAttemptNum() {
    Assert.assertEquals(java.util.Optional.of(0).get(), baseCookDecorator.getAttemptNum());
  }

  @Test
  public void checkDefaultWattage() {
    Assert.assertEquals(java.util.Optional.of(200).get(), baseCookDecorator.getDefaultWattage());
  }

  @Test
  public void checkTargetTemperature() {
    Assert.assertEquals(java.util.Optional.of(30).get(), baseCookDecorator.getTemperature());
  }

  @Test
  public void checkTimeToCook() {
    Assert.assertEquals(java.util.Optional.of(20).get(), baseCookDecorator.getTimeToCook());
  }

  @Test
  public void checkVisualScore() {
    Assert.assertEquals(java.util.Optional.of(5.2).get(), baseCookDecorator.getVisualScore());
  }

  @Test
  public void checkThermalScore() {
    Assert.assertEquals(java.util.Optional.of(8.2).get(), baseCookDecorator.getThermalScore());
  }

  @Test
  public void checkCycleDurationSeconds() {
    Assert.assertEquals(java.util.Optional.of(20).get(),
      baseCookDecorator.getCycleDurationSeconds());
  }

  @Test
  public void checkStirringPercent() {
    Assert.assertEquals(java.util.Optional.of(75).get(), baseCookDecorator.getStirringPercent());
  }

  @Test
  public void checkStirringSpeed() {
    Assert.assertEquals(java.util.Optional.of(18).get(), baseCookDecorator.getStirringSpeed());
  }

  @Test
  public void checkCaptureParamsList() {
    Assert.assertEquals(2, baseCookDecorator.getCaptureTasks().size());
  }

  @Test
  public void checkModelId() {
    Assert.assertEquals("consistency_kheer", baseCookDecorator.getModelId());
  }

  @Test
  public void checkModelFileName() {
    Assert.assertEquals("consistency_kheer.model", baseCookDecorator.getModelFileName());
  }
}
