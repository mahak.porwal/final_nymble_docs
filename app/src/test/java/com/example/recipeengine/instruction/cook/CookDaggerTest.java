package com.example.recipeengine.instruction.cook;

import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CookDaggerTest {

  Cook cook;

  @Before
  public void setUp() {
    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    this.cook = cookFactory.createCookObject(CookFactory.filename);
  }

  @Test
  public void generateCookObject() {
    Assert.assertNotNull(cook);
  }

  @Test
  public void checkDependencyInjectionOfActionParams() {
    Assert.assertNotNull(cook.actionParams);
  }

  @Test
  public void checkDependencyInjectionOfCaptureParams() {
    Assert.assertNotNull(cook.captureParams);
  }

  @Test
  public void checkDependencyInjectionOfStirrerParams() {
    Assert.assertNotNull(cook.stirrerParams);
  }
}
