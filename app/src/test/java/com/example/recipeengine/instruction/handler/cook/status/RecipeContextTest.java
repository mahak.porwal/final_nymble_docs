package com.example.recipeengine.instruction.handler.cook.status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RecipeContextTest {

  RecipeContext recipeContext;

  @Before
  public void setUp() {
    recipeContext = new RecipeContext(0,null);
  }

  @Test
  public void checkGetServingSize() {
    Assert.assertEquals(0,recipeContext.getServingSize());
  }

  @Test
  public void checkGetRecipeName() {
    Assert.assertNull(recipeContext.getRecipeName());
  }

}
