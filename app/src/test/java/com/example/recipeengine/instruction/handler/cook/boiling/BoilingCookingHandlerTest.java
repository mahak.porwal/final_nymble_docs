package com.example.recipeengine.instruction.handler.cook.boiling;


import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;

import com.example.recipeengine.instruction.blocks.capture.BaseCaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;


import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


public class BoilingCookingHandlerTest {

  BoilingCookingHandler boilingCookingHandler;
  BaseCaptureBlock baseCaptureBlockMock;
  BoilingInfer boilingInferMock;
  BoilingAction boilingActionMock;

  @Before
  public void setUp() {
    baseCaptureBlockMock = Mockito.mock(BaseCaptureBlock.class);
    boilingInferMock = Mockito.mock(BoilingInfer.class);
    boilingActionMock = Mockito.mock(BoilingAction.class);
    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    Cook cook = cookFactory.createCookObject(CookFactory.filename);
    boilingCookingHandler =
      new BoilingCookingHandler(cook,
        baseCaptureBlockMock, boilingInferMock, boilingActionMock);
  }

  @Test
  public void checkBoilingHandle() throws Exception {
    runHandler();
    boilingCookingHandler.handle();
  }


  @Test
  public void checkBoilingStatus() throws Exception {
    runHandler();
    boilingCookingHandler.handle();
    Assert.assertEquals(null, boilingCookingHandler.getStatus().getVerboseStatus());
  }

  private void runHandler() throws Exception {
    Mockito.doReturn(new ArrayList<CapturedTaskData>()).when(baseCaptureBlockMock).getCapturedData();
    Mockito.doReturn(new InferredResult(new ArrayList<>())).when(boilingInferMock)
      .infer(Mockito.any());
    Mockito.doNothing().when(boilingActionMock).takeAction(Mockito.any(), Mockito.any());
    new Thread(() -> {
      try {
        Thread.currentThread().sleep(200);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      boilingCookingHandler.setIsCompleted(true);
    }).start();
  }
}
