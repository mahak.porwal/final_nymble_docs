package com.example.recipeengine.instruction.decorator.source;

import androidx.core.util.Pair;
import com.example.recipeengine.instruction.decorator.exchanges.InstructionInputParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InstructionDecorationTest {

  InstructionDecoration instructionDecoration;

  @Before
  public void setUp() {
    Map<OutputParams, Double> paramsDecorationWeight = new HashMap<>();
    paramsDecorationWeight.put(OutputParams.ONION_SIZE_SCORE, 1.0);

    //Parameters
    Map<OutputParams, Map<String, Map<InstructionInputParams, String>>> parameters =
        new HashMap<>();
    Map<String, Map<InstructionInputParams, String>> bucketMap = new HashMap<>();
    Map<InstructionInputParams, String> instructionInputParamsStringMap1 = new HashMap<>();
    instructionInputParamsStringMap1.put(InstructionInputParams.INGREDIENT_QUANTITY, "1.2");
    bucketMap.put("bucket 1", instructionInputParamsStringMap1);
    Map<InstructionInputParams, String> instructionInputParamsStringMap2 = new HashMap<>();
    instructionInputParamsStringMap2.put(InstructionInputParams.INGREDIENT_QUANTITY, "0.7");
    bucketMap.put("bucket 2", instructionInputParamsStringMap2);
    parameters.put(OutputParams.ONION_SIZE_SCORE, bucketMap);

    //Discrete Map
    Map<OutputParams, Map<String, Pair<Double, Double>>> discretizationMap = new HashMap<>();
    Map<String, Pair<Double, Double>> buckets = new HashMap<>();
    Pair<Double, Double> pair1 = new Pair<>(5.0, 7.0);
    Pair<Double, Double> pair2 = new Pair<>(7.0, 10.0);
    buckets.put("bucket 1", pair1);
    buckets.put("bucket 2", pair2);
    //System.out.println(buckets.get("bucket 1").first);
    discretizationMap.put(OutputParams.ONION_SIZE_SCORE, buckets);
    instructionDecoration =
        new InstructionDecoration(1.0, paramsDecorationWeight, parameters, discretizationMap);
  }

  @Test
  public void checkInstructionWeight() {
    instructionDecoration.setInstructionWeight(instructionDecoration.getInstructionWeight());
    Assert.assertEquals(java.util.Optional.of(1.0).get(),
        instructionDecoration.getInstructionWeight());
  }

  @Test
  public void checkParamsDecorationWeight() {
    instructionDecoration
        .setParamsDecorationWeight(instructionDecoration.getParamsDecorationWeight());
    Assert.assertEquals(
        java.util.Optional.of(1.0).get(),
        instructionDecoration.getParamsDecorationWeight().get(OutputParams.ONION_SIZE_SCORE));
  }

  @Test
  public void checkParameters() {
    instructionDecoration.setParameters(instructionDecoration.getParameters());
    Assert.assertEquals("1.2",
        instructionDecoration.getParameters().get(OutputParams.ONION_SIZE_SCORE).get("bucket 1")
            .get(InstructionInputParams.INGREDIENT_QUANTITY));
  }

  @Test
  public void checkDiscretizationMap() {
    instructionDecoration.setDiscretizationMap(instructionDecoration.getDiscretizationMap());
    //System.out.println(instructionDecoration.getDiscretizationMap().get(OutputParams.ONION_SIZE_SCORE).get("bucket 1"));
    Assert.assertEquals(java.util.Optional.of(5.0).get(),
        instructionDecoration.getDiscretizationMap().get(OutputParams.ONION_SIZE_SCORE)
            .get("bucket 1").first);
  }

}
