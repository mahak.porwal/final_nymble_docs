package com.example.recipeengine.instruction.task.capture.weight;

import com.example.recipeengine.instruction.cook.params.capture.captureconstants.CaptureSensor;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import som.hardware.request.handler.RequestHandler;
import som.hardware.response.SensorResponse;

public class WeightSensorThreadTest {

  WeightSensorThread weightSensorThread;

  @Before
  public void setUp() {
    weightSensorThread =
        new WeightSensorThread("WEIGHT_SENSOR", new SensorParams(null, null, 10, 10,
            null, null, null, null), new ArrayList<>(), null);
  }

  @Test
  public void checkWeightSensorThreadRun() throws Exception {
    /*WeightSensorThread weightSensorThreadSpy = Mockito.spy(weightSensorThread);
    RequestHandler requestHandlerMock = Mockito.mock(RequestHandler.class);
    Mockito.doReturn(requestHandlerMock).when(weightSensorThreadSpy).getRequestHandlerInstance();
    List<Double> list = new ArrayList<>();
    Map<String, List<Double>> sensorData = new HashMap<>();
    sensorData.put(CaptureSensor.WEIGHT_SENSOR, new ArrayList<Double>(
        Arrays.asList(10.0)));
    Mockito.doReturn(new SensorResponse(5, true, sensorData)).when(requestHandlerMock)
        .handleRequest(Mockito.any());
    weightSensorThreadSpy.run();*/
  }

  @Test
  public void checkWeightSensorThreadName() {
    Assert.assertEquals("WEIGHT_SENSOR", weightSensorThread.getName());
  }

  @Test
  public void checkWeightSensorThreadListSize() throws Exception {
  /*  WeightSensorThread weightSensorThreadSpy = Mockito.spy(weightSensorThread);
    RequestHandler requestHandlerMock = Mockito.mock(RequestHandler.class);
    Mockito.doReturn(requestHandlerMock).when(weightSensorThreadSpy).getRequestHandlerInstance();
    List<Double> list = new ArrayList<>();
    Map<String, List<Double>> sensorData = new HashMap<>();
    sensorData.put(CaptureSensor.WEIGHT_SENSOR, new ArrayList<Double>(
        Arrays.asList(10.0)));
    Mockito.doReturn(new SensorResponse(5, true, sensorData)).when(requestHandlerMock)
        .handleRequest(Mockito.any());
    weightSensorThreadSpy.run();
    Mockito.verify(requestHandlerMock, Mockito.times(10)).handleRequest(Mockito.any());*/
  }

  @Test
  public void checkGetRequestHandlerInstance() {
    Assert.assertTrue(weightSensorThread.getRequestHandlerInstance() instanceof RequestHandler);
  }
}
