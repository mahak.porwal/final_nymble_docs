package com.example.recipeengine.instruction.dispense.decorator;

import static com.example.recipeengine.resources.DispenseObjectsResolver.createInstructionDecorationMapObject;
import static com.example.recipeengine.resources.DispenseObjectsResolver.createInstructionLogObject;
import static com.example.recipeengine.resources.DispenseObjectsResolver.createInstructionObject;

import com.example.recipeengine.instruction.decorator.source.InstructionDecorationMap;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;
import com.example.recipeengine.instruction.dispense.BaseDispense;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

public class DispensedQuantityDecoratorTest {

  DispensedQuantityDecorator dispensedQuantityDecorator;
  BaseDispense baseDispense;
  InstructionLog instructionLog;
  InstructionDecorationMap instructionDecorationMap;

  @Before
  public void setUp() {
    baseDispense = createInstructionObject(null);
    instructionLog = createInstructionLogObject(null);
    instructionDecorationMap = createInstructionDecorationMapObject();
    dispensedQuantityDecorator = new DispensedQuantityDecorator(
        baseDispense, instructionLog, 30.0);
  }

  @Test
  public void checkGetQuantityInGrams() {
    Assert.assertEquals(java.util.Optional.of(45).get(),
        dispensedQuantityDecorator.getQuantityInGrams());
  }

  @Test
  public void checkGetQuantityInGramsNoWrappedObject() {
    //TO BE HANDLED
    DispensedQuantityDecorator dispensedQuantityDecorator2 = new DispensedQuantityDecorator(
        null, instructionLog, 30.0);
    Assert.assertThrows(NullPointerException.class, () -> {
      dispensedQuantityDecorator2.getQuantityInGrams();
    });
  }

  @Test
  public void checkGetQuantityInGramsNoInstructionLog() {
    //TO BE HANDLED
    DispensedQuantityDecorator dispensedQuantityDecorator2 = new DispensedQuantityDecorator(
        baseDispense, null, 30.0);
    Assert.assertThrows(NullPointerException.class, () -> {
      dispensedQuantityDecorator2.getQuantityInGrams();
    });
  }

  @Test
  public void checkGetQuantityInGramsNegative() throws Exception {
    DispensedQuantityDecorator dispensedQuantityDecorator2 = new DispensedQuantityDecorator(
        createInstructionObject(null), createInstructionLogObject(null), 130.0);
    Assert.assertEquals(java.util.Optional.of(0).get(),
        dispensedQuantityDecorator2.getQuantityInGrams());
  }

  @Test
  public void checkGetQuantityInGramsDispenseExceed() throws Exception {
    DispensedQuantityDecorator dispensedQuantityDecorator2 = new DispensedQuantityDecorator(
        createInstructionObject(null), createInstructionLogObject(null), 10.0);
    Assert.assertEquals(java.util.Optional.of(50).get(),
        dispensedQuantityDecorator2.getQuantityInGrams());
  }
}
