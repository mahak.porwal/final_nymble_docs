package com.example.recipeengine.instruction.handler.dispense.blocks.postdispense;

import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import com.example.recipeengine.instruction.task.capture.vision.VisionCapture;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PostDispenseBlockTest {

  PostDispenseBlock postDispenseBlock;

  @Before
  public void setUp() {
    List<CapturableTask> capturableTasks = new ArrayList<>();
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 500,
            null,
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    SensorParams sensorParams2 =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 500,
            null,
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture2 =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams2);
    capturableTasks.add(visionCapture);
    capturableTasks.add(visionCapture2);
    postDispenseBlock = new PostDispenseBlock(capturableTasks);
  }

  @Test
  public void checkStartSequentialTasks() throws InterruptedException {
    postDispenseBlock.startSequentialTasks();
  }

  @Test
  public void checkGetSequentialTasks() {
    Assert.assertEquals(2, postDispenseBlock.getSequentialTasks().size());
  }

  @Test
  public void checkGetBlockStatus() {
    Assert.assertEquals("Post Dispense Block", postDispenseBlock.getBlockStatus());
  }

  @Test
  public void checkGetExecutingTask() throws InterruptedException {
    new Thread(() -> {
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      Assert.assertEquals("PRE_DISPENSE_IMAGE",
          postDispenseBlock.getExecutingTask().getTaskName());
    }).start();
    postDispenseBlock.setExecutingTask(null);
    postDispenseBlock.startSequentialTasks();
  }

  @Test
  public void checkPauseTasks() throws Exception {
    new Thread(() -> {
      Assert.assertThrows(InterruptedException.class,() -> {
        postDispenseBlock.startSequentialTasks();
      });
    }).start();
    Thread.sleep(100);
    postDispenseBlock.pause();
  }
}
