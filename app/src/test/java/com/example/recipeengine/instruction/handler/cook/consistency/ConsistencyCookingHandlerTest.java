package com.example.recipeengine.instruction.handler.cook.consistency;

import com.example.recipeengine.instruction.cook.Cook;

import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;

import com.example.recipeengine.instruction.blocks.capture.BaseCaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class ConsistencyCookingHandlerTest {

  ConsistencyCookingHandler consistencyCookingHandler;
  BaseCaptureBlock baseCaptureBlockMock;
  ConsistencyInfer consistencyInferMock;
  ConsistencyAction consistencyActionMock;

  @Before
  public void setUp() {
    baseCaptureBlockMock = Mockito.mock(BaseCaptureBlock.class);
    consistencyInferMock = Mockito.mock(ConsistencyInfer.class);
    consistencyActionMock = Mockito.mock(ConsistencyAction.class);
    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    Cook cook = cookFactory.createCookObject(CookFactory.filename);
    consistencyCookingHandler = new ConsistencyCookingHandler(cook,
      baseCaptureBlockMock, consistencyInferMock, consistencyActionMock);
  }

  @Test
  public void checkConsistencyHandle() throws Exception {
    runHandler();
    consistencyCookingHandler.handle();
  }

  @Test
  public void checkConsistencyStatus() throws Exception {
    runHandler();
    consistencyCookingHandler.handle();
    Assert.assertEquals(null, consistencyCookingHandler.getStatus().getVerboseStatus());
  }

  private void runHandler() throws Exception {
    Mockito.doReturn(new ArrayList<CapturedTaskData>()).when(baseCaptureBlockMock).getCapturedData();
    Mockito.doReturn(new InferredResult(new ArrayList<>())).when(consistencyInferMock).infer(Mockito.any());
    Mockito.doNothing().when(consistencyActionMock).takeAction(Mockito.any(), Mockito.any());
    new Thread(() -> {
      try {
        Thread.currentThread().sleep(200);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      consistencyCookingHandler.setIsCompleted(true);
    }).start();
  }

}
