package com.example.recipeengine.instruction.handler.cook.infer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class BaseInferBlockTest {

  BaseInferBlock baseInferBlock;

  @Before
  public void setUp() {
    baseInferBlock = Mockito.mock(BaseInferBlock.class,Mockito.CALLS_REAL_METHODS);
  }

  @Test
  public void checkGetBlockStatus() {
    baseInferBlock.setBlockStatus(baseInferBlock.getBlockStatus());
    Assert.assertNull(baseInferBlock.getBlockStatus());
  }

  @Test
  public void checkGetUiEmitter() {
    baseInferBlock.setUiEmitter(baseInferBlock.getUiEmitter());
    Assert.assertNull(baseInferBlock.getUiEmitter());
  }

  @Test
  public void checkGetDataLogger() {
    baseInferBlock.setDataLogger(baseInferBlock.getDataLogger());
    Assert.assertNull(baseInferBlock.getDataLogger());
  }

  @Test
  public void checkGetRecipeContext() {
    baseInferBlock.setRecipeContext(baseInferBlock.getRecipeContext());
    Assert.assertNull(baseInferBlock.getRecipeContext());
  }

}
