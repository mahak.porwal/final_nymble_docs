package com.example.recipeengine.instruction.adaptor;

import com.example.recipeengine.instruction.adaptor.DispenseAdaptor;
import com.example.recipeengine.instruction.dispense.decorator.DispensedQuantityDecorator;
import com.example.recipeengine.instruction.dispense.decorator.IngredientQuantityDecorator;
import com.example.recipeengine.instruction.dispense.BaseDispense;
import com.example.recipeengine.instruction.dispense.Dispense;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DispenseAdaptor.class)
public class DispenseAdaptorTest {

  DispenseAdaptor dispenseAdaptor;
  DispenseAdaptor dispenseAdaptorSpy;

  @Before
  public void setUp() {
    BaseDispense baseDispense = new Dispense(123, 0, 30, Arrays
      .asList("salt", "paneer"), null, null, null, null);
    dispenseAdaptor = new DispenseAdaptor(baseDispense);
    dispenseAdaptorSpy = PowerMockito.spy(dispenseAdaptor);
  }

  @Test
  public void checkAdaptDecoration() throws Exception {
    //Verify If adapt() returns an Object of IngredientQuantityDecorator because attemptNum is 0
    PowerMockito.doNothing().when(dispenseAdaptorSpy, "saveInstruction");
    Assert.assertTrue(dispenseAdaptorSpy.adapt() instanceof IngredientQuantityDecorator);
  }

  @Test
  public void checkAdaptSaveInstructionCalled() throws Exception {
    //Verify If saveInstruction is called once
    PowerMockito.doNothing().when(dispenseAdaptorSpy, "saveInstruction");
    dispenseAdaptorSpy.adapt();
    PowerMockito.verifyPrivate(dispenseAdaptorSpy).invoke("saveInstruction");
  }

  @Test
  public void checkAdaptSaveInstructionImpl() {
    Assert.assertTrue(true);
  }

  @Test
  public void checkAdaptIncrementNumber() throws Exception {
    //Verify if attemptNum is Incremented after decoration
    PowerMockito.doNothing().when(dispenseAdaptorSpy, "saveInstruction");
    Assert.assertEquals(java.util.Optional.of(1).get(), dispenseAdaptorSpy.adapt().getAttemptNum());

  }

  @Test
  public void checkApplyOneTimeDecoration() {
    Assert.assertTrue(
      dispenseAdaptor.applyOneTimeDecoration() instanceof IngredientQuantityDecorator);
  }

  @Test
  public void checkApplyCompensatoryDecoration() {
    Assert.assertTrue(
      dispenseAdaptor.applyCompensatoryDecoration() instanceof DispensedQuantityDecorator);
  }

}
