package com.example.recipeengine.instruction.handler.cook.status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InstructionStatusTest {

  InstructionStatus instructionStatus;

  @Before
  public void setUp() {
    instructionStatus = new InstructionStatus("status","verbose status",0);
  }

  @Test
  public void checkGetStatus() {
    Assert.assertEquals("status",instructionStatus.getStatus());
  }

  @Test
  public void checkGetVerboseStatus() {
    Assert.assertEquals("verbose status",instructionStatus.getVerboseStatus());
  }

  @Test
  public void checkGetRemainingTimeSec() {
    Assert.assertEquals(java.util.Optional.of(0).get(),instructionStatus.remainingTimeSec());
  }

}
