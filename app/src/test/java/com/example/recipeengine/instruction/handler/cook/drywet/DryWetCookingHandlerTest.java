package com.example.recipeengine.instruction.handler.cook.drywet;


import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;

import com.example.recipeengine.instruction.blocks.capture.BaseCaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;

import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class DryWetCookingHandlerTest {

  DryWetCookingHandler dryWetCookingHandler;
  BaseCaptureBlock baseCaptureBlockMock;
  DryWetInfer dryWetInferMock;
  DryWetAction dryWetActionMock;

  @Before
  public void setUp() {
    baseCaptureBlockMock = Mockito.mock(BaseCaptureBlock.class);
    dryWetInferMock = Mockito.mock(DryWetInfer.class);
    dryWetActionMock = Mockito.mock(DryWetAction.class);
    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    Cook cook = cookFactory.createCookObject(CookFactory.filename);
    dryWetCookingHandler = new DryWetCookingHandler(cook,
      baseCaptureBlockMock, dryWetInferMock, dryWetActionMock);
  }

  @Test
  public void checkDryWetHandle() throws Exception {
    runHandler();
    dryWetCookingHandler.handle();
  }

  @Test
  public void checkDryWetStatus() throws Exception {
    runHandler();
    dryWetCookingHandler.handle();
    Assert.assertEquals(null, dryWetCookingHandler.getStatus().getVerboseStatus());
  }

  private void runHandler() throws Exception {
    Mockito.doReturn(new ArrayList<CapturedTaskData>()).when(baseCaptureBlockMock).getCapturedData();
    Mockito.doReturn(new InferredResult(new ArrayList<>())).when(dryWetInferMock).infer(Mockito.any());
    Mockito.doNothing().when(dryWetActionMock).takeAction(Mockito.any(), Mockito.any());
    new Thread(() -> {
      try {
        Thread.currentThread().sleep(200);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      dryWetCookingHandler.setIsCompleted(true);
    }).start();
  }

}
