package com.example.recipeengine.instruction.handler.dispense.blocks;

import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DispenseTypeTest {

  DispenseType dispenseType;

  @Before
  public void setUp() {
    dispenseType = new DispenseType(DispenseType.MACRO);
  }

  @Test
  public void getDispenseType() {
    Assert.assertEquals("MACRO",dispenseType.getDispenseType());
  }

}
