package com.example.recipeengine.instruction.handler.cook.frying.decorator;


import com.example.recipeengine.instruction.cook.BaseCook;
import junit.framework.TestCase;

import static com.example.recipeengine.resources.ConsistencyObjectResolver.createCookObject;

public class FryingCompensationDecoratorTest extends TestCase {

	BaseCook baseCook;
	FryingCompensationDecorator fryingCompensationDecorator;

	public void setUp() throws Exception {
		super.setUp();
		this.baseCook = createCookObject();
		this.fryingCompensationDecorator = new FryingCompensationDecorator(this.baseCook);
	}

	public void testGetVisualScore() {
		assertEquals("Visual score", java.util.Optional.of(90.0).get(), this.fryingCompensationDecorator.getVisualScore());

	}
}