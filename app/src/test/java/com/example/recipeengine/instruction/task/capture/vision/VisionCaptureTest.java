package com.example.recipeengine.instruction.task.capture.vision;

import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.resources.JuliaStateDemo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VisionCaptureTest {

  VisionCapture visionCapture;
  private JuliaStateDemo juliaStateDemo = JuliaStateDemo.getInstance();

  @Before
  public void setUp() {
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 500,
            new TriggerType(TriggerType.STIRRER_STARTS),
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    visionCapture = new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    juliaStateDemo.setStirrerStatus("idle");
  }

  @Test
  public void checkGetSensor() {
    Assert.assertEquals("VISUAL_CAMERA", visionCapture.getSensor().getCaptureSensor());
  }

  @Test
  public void checkGetParamsNumOfSamples() {
    Assert
        .assertEquals(java.util.Optional.of(10).get(), visionCapture.getParams().getNumOfSamples());
  }

  @Test
  public void checkGetThermalImages() {
    Assert.assertNotNull(visionCapture.getThermalImages());
  }

  @Test
  public void checkGetVisionImages() {
    Assert.assertNotNull(visionCapture.getVisionImages());
  }

  @Test
  public void checkGetThermalMatrices() {
    Assert.assertNotNull(visionCapture.getThermalMatrices());
  }

  //TODO Modify the test cases.
  @Test
  public void checkBeginTaskVisionCapture() throws InterruptedException {
    new Thread(() -> {
      //JuliaStateDemo juliaStateDemo = JuliaStateDemo.getInstance();
      try {
        Thread.currentThread().sleep(3000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      //juliaStateDemo.addPropertyChangeListener(visionCapture);
      juliaStateDemo.setStirrerStatus("running");
    }).start();
    visionCapture.beginTask();
    //BOTTLENECK : CHECK HOW MANY SECS SLEEP SHOULD HAPPEN
    Thread.sleep(5000);
    Assert.assertNotEquals(10, visionCapture.getVisionImages().size());
  }


  @Test
  public void checkAbortTaskVisionCapture() throws InterruptedException {
    new Thread(() -> {
      try {
        Thread.currentThread().sleep(3000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      juliaStateDemo.setStirrerStatus("running");
    }).start();
    visionCapture.beginTask();
    //BOTTLENECK : CHECK HOW MANY SECS SLEEP SHOULD HAPPEN
    Thread.sleep(1000);
    visionCapture.abortTask();
    Assert.assertEquals(0, visionCapture.getVisionImages().size());
  }

  @Test
  public void checkBeginTaskSyncVisionThermalCaptureListSize() throws InterruptedException {
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.SYNC_THERMAL_VISUAL),
            new ThermalType(ThermalType.IMAGE), 10, 500,
            new TriggerType(TriggerType.STIRRER_STARTS),
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture2 =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    new Thread(() -> {
      try {
        Thread.currentThread().sleep(3000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      //JuliaStateDemo juliaStateDemo = JuliaStateDemo.getInstance();
      //juliaStateDemo.addPropertyChangeListener(visionCapture2);
      //juliaStateDemo.setStirrerStatus("idle");
      juliaStateDemo.setStirrerStatus("running");
    }).start();
    visionCapture2.beginTask();
    Thread.sleep(5000);
    Assert.assertNotEquals(10, visionCapture2.getVisionImages().size());
  }
}
