package com.example.recipeengine.instruction.handler.dispense.blocks.dispense;

import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import com.example.recipeengine.instruction.task.capture.vision.VisionCapture;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DispenseBlockTest {

  DispenseBlock dispenseBlock;

  @Before
  public void setUp() {
    List<CapturableTask> capturableTasks = new ArrayList<>();
    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 500,
            null,
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    SensorParams sensorParams2 =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 500,
            null,
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION), 0.8, 1.0);
    VisionCapture visionCapture2 =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams2);
    capturableTasks.add(visionCapture);
    capturableTasks.add(visionCapture2);
    dispenseBlock = new DispenseBlock(capturableTasks,new DispenseHwInformation(new DispenseType(DispenseType.LIQUID),null,null,null));
  }

  @Test
  public void checkStartParallelTasks() throws InterruptedException {
    dispenseBlock.startParallelTasks();
  }

  @Test
  public void checkGetParallelTasks() {
    Assert.assertEquals(2,dispenseBlock.getParallelTasks().size());
  }

  @Test
  public void checkPauseTasks() throws Exception {
    new Thread(() -> {
        Assert.assertThrows(InterruptedException.class,() -> {
          dispenseBlock.startParallelTasks();
        });
    }).start();
      Thread.sleep(100);
      dispenseBlock.pause();
  }

  @Test
  public void checkGetBlockStatus() {
    Assert.assertEquals("Dispense Block", dispenseBlock.getBlockStatus());
  }

  @Test
  public void checkDispenseHWInformation() {
    Assert.assertEquals("LIQUID",dispenseBlock.getDispenseHwInformation().getHardwareSubsystem().getDispenseType());
  }

}
