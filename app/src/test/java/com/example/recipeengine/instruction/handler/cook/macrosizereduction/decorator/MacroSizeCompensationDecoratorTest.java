package com.example.recipeengine.instruction.handler.cook.macrosizereduction.decorator;


import com.example.recipeengine.instruction.cook.BaseCook;
import junit.framework.TestCase;

import static com.example.recipeengine.resources.ConsistencyObjectResolver.createCookObject;

public class MacroSizeCompensationDecoratorTest extends TestCase {
	BaseCook baseCook;
	MacroSizeCompensationDecorator macroSizeCompensationDecorator;
	public void setUp() throws Exception {
		super.setUp();
		this.baseCook = createCookObject();   //manually setting a visual score
		this.macroSizeCompensationDecorator = new MacroSizeCompensationDecorator(this.baseCook);
	}

	public void testGetVisualScore() {
		assertEquals("Visual score", java.util.Optional.of(90.0).get(), this.macroSizeCompensationDecorator.getVisualScore());
	}
}