package com.example.recipeengine.instruction.cook.decorator;



import static com.example.recipeengine.resources.CookObjectsResolver.createInstructionDecorationMapObject;
import static com.example.recipeengine.resources.CookObjectsResolver.createInstructionDecorationMapObjectComplex;
import static com.example.recipeengine.resources.CookObjectsResolver.createInstructionLogObject;
import static com.example.recipeengine.resources.CookObjectsResolver.createInstructionLogObjectComplex;
import static com.example.recipeengine.resources.CookObjectsResolver.createInstructionObject;

import com.example.recipeengine.instruction.decorator.source.InstructionDecorationMap;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ParamsDecoratorTest {

  ParamsDecorator paramsDecorator;
  List<InstructionLog> instructionLogs;
  InstructionDecorationMap instructionDecorationMap;

  @Before
  public void setUp() {
    instructionLogs = new ArrayList<>();
    instructionLogs.add(createInstructionLogObject(null));
    paramsDecorator =
        new ParamsDecorator(createInstructionObject(null), instructionLogs,
            createInstructionDecorationMapObject());
  }

  @Test
  public void checkGetTargetTemperature() {
    Assert.assertEquals(java.util.Optional.of(36).get(),paramsDecorator.getTemperature());
  }

  @Test
  public void checkGetTimeToCook(){
    Assert.assertEquals(java.util.Optional.of(24).get(),paramsDecorator.getTimeToCook());
  }

  @Test
  public void checkGetDefaultWattage() {
    Assert.assertEquals(java.util.Optional.of(240).get(),paramsDecorator.getDefaultWattage());
  }

  @Test
  public void checkGetTargetTemperatureNoDecorationMap() {
    instructionLogs = new ArrayList<>();
    instructionLogs.add(createInstructionLogObject(null));
    ParamsDecorator paramsDecorator2 =
        new ParamsDecorator(createInstructionObject(null), instructionLogs,
            null);
    Assert.assertEquals(java.util.Optional.of(30).get(),paramsDecorator2.getTemperature());
  }

  @Test
  public void checkGetTimeToCookNoDecorationMap(){
    instructionLogs = new ArrayList<>();
    instructionLogs.add(createInstructionLogObject(null));
    ParamsDecorator paramsDecorator2 =
        new ParamsDecorator(createInstructionObject(null), instructionLogs,
            null);
    Assert.assertEquals(java.util.Optional.of(20).get(),paramsDecorator2.getTimeToCook());
  }

  @Test
  public void checkGetDefaultWattageNoDecorationMap() {
    instructionLogs = new ArrayList<>();
    instructionLogs.add(createInstructionLogObject(null));
    ParamsDecorator paramsDecorator2 =
        new ParamsDecorator(createInstructionObject(null), instructionLogs,
            null);
    Assert.assertEquals(java.util.Optional.of(200).get(),paramsDecorator2.getDefaultWattage());
  }

  @Test
  public void checkGetTargetTemperatureComplex() {
    instructionLogs = new ArrayList<>();
    instructionLogs.add(createInstructionLogObjectComplex(null));
    ParamsDecorator paramsDecorator2 =
        new ParamsDecorator(createInstructionObject(null), instructionLogs,
            createInstructionDecorationMapObjectComplex());
    Assert.assertEquals(java.util.Optional.of(33).get(),paramsDecorator2.getTemperature());
  }

  @Test
  public void checkGetTimeToCookComplex() {
    instructionLogs = new ArrayList<>();
    instructionLogs.add(createInstructionLogObjectComplex(null));
    ParamsDecorator paramsDecorator2 =
        new ParamsDecorator(createInstructionObject(null), instructionLogs,
            createInstructionDecorationMapObjectComplex());
    Assert.assertEquals(java.util.Optional.of(22).get(),paramsDecorator2.getTimeToCook());
  }

  @Test
  public void checkGetDefaultWattageComplex() {
    instructionLogs = new ArrayList<>();
    instructionLogs.add(createInstructionLogObjectComplex(null));
    ParamsDecorator paramsDecorator2 =
        new ParamsDecorator(createInstructionObject(null), instructionLogs,
            createInstructionDecorationMapObjectComplex());
    Assert.assertEquals(java.util.Optional.of(220).get(),paramsDecorator2.getDefaultWattage());
  }
}
