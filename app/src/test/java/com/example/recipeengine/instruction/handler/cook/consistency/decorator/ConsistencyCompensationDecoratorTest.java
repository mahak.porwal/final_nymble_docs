package com.example.recipeengine.instruction.handler.cook.consistency.decorator;

import com.example.recipeengine.instruction.cook.BaseCook;
import com.example.recipeengine.instruction.decorator.source.InstructionDecorationMap;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;

import junit.framework.TestCase;

import static com.example.recipeengine.resources.ConsistencyObjectResolver.createInstructionDecorationMapObject;
import static com.example.recipeengine.resources.ConsistencyObjectResolver.createInstructionLogObject;
import static com.example.recipeengine.resources.ConsistencyObjectResolver.createCookObject;

public class ConsistencyCompensationDecoratorTest extends TestCase {
    BaseCook baseCook;
    InstructionLog instructionLog;
    InstructionDecorationMap instructionDecorationMap;
    ConsistencyCompensationDecorator consistencyDecorator;
    public void setUp() throws Exception {
        super.setUp();
        this.baseCook = createCookObject();
        this.instructionLog = createInstructionLogObject(null);
        this.instructionDecorationMap = createInstructionDecorationMapObject();
        this.consistencyDecorator = new ConsistencyCompensationDecorator(this.baseCook);
    }

    public void testGetTimeToCook() {
        assertEquals("time to cook", java.util.Optional.of(90).get(), this.consistencyDecorator.getTimeToCook());
    }
}