package com.example.recipeengine.instruction.cook;

import com.example.recipeengine.instruction.adaptor.CookAdaptor;

import com.example.recipeengine.instruction.dispense.BaseDispense;
import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;

import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;

import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;

import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import java.io.IOException;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class CookTest {

  Cook cook;
  Cook adaptedCook;
  CookAdaptor cookAdaptorMock;

  @Before
  public void setUp() throws IOException, ParseException {


    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());

    cook = cookFactory.createCookObject("./src/main/java/com/example/recipeengine/resources/Cook.json");

    adaptedCook = cookFactory.createCookObject("./src/main/java/com/example/recipeengine/resources/Cook.json");
    cookAdaptorMock = Mockito.mock(CookAdaptor.class);
  }

  @Test
  public void checkInstructionId() {
    cook.setInstructionId(cook.getInstructionId());
    Assert.assertEquals(java.util.Optional.of(0).get(), cook.getInstructionId());
  }

  @Test
  public void checkInstructionType() {
    Assert.assertTrue(cook instanceof BaseCook);
  }

  @Test
  public void checkAttemptNum() {
    Assert.assertEquals(java.util.Optional.of(0).get(), cook.getAttemptNum());
  }

  @Test
  public void checkDefaultWattage() {
    Assert.assertEquals(java.util.Optional.of(200).get(), cook.getDefaultWattage());
  }

  @Test
  public void checkTargetTemperature() {
    Assert.assertEquals(java.util.Optional.of(30).get(), cook.getTemperature());
  }

  @Test
  public void checkTimeToCook() {
    Assert.assertEquals(java.util.Optional.of(20).get(), cook.getTimeToCook());
  }

  @Test
  public void checkVisualScore() {
    Assert.assertEquals(java.util.Optional.of(5.2).get(), cook.getVisualScore());
  }

  @Test
  public void checkThermalScore() {
    Assert.assertEquals(java.util.Optional.of(8.2).get(), cook.getThermalScore());
  }

  @Test
  public void checkCycleDurationSeconds() {
    Assert.assertEquals(java.util.Optional.of(20).get(), cook.getCycleDurationSeconds());
  }

  @Test
  public void checkStirringPercent() {
    Assert.assertEquals(java.util.Optional.of(75).get(), cook.getStirringPercent());
  }

  @Test
  public void checkStirringSpeed() {
    Assert.assertEquals(java.util.Optional.of(18).get(), cook.getStirringSpeed());
  }

  @Test
  public void checkMakeCookObject() {
    Assert.assertTrue(cook.makeCookAdaptorObject(cook) instanceof CookAdaptor);
  }

  @Test
  public void checkCaptureParamsList() {
    Assert.assertEquals(2, cook.getCaptureTasks().size());
  }

  @Test
  public void checkCaptureParamsValues() {
    Assert.assertEquals(java.util.Optional.of(501).get(), cook.getCaptureTasks().get(0).getParams().getTimeBetweenCaptureMS());
  }

  @Test
  public void checkModelId() {
    Assert.assertEquals("consistency_kheer", cook.getModelId());
  }

  @Test
  public void checkModelFileName() {
    Assert.assertEquals("consistency_kheer.model", cook.getModelFileName());
  }

  @Test
  public void checkAdaptedCookTargetTemperature() {
    Cook cookSpy = Mockito.spy(cook);
    Mockito.doReturn(cookAdaptorMock).when(cookSpy).makeCookAdaptorObject(cookSpy);
    Mockito.doReturn(adaptedCook).when(cookAdaptorMock).adapt();
    Assert.assertEquals(java.util.Optional.of(30).get(), cookSpy.adapt().getTemperature());
  }
}
