package com.example.recipeengine.instruction.handler.cook.frying;

import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;

import com.example.recipeengine.instruction.blocks.capture.BaseCaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;

import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class FryingCookingHandlerTest {

  FryingCookingHandler fryingCookingHandler;
  BaseCaptureBlock baseCaptureBlockMock;
  FryingInfer fryingInferMock;
  FryingAction fryingActionMock;

  @Before
  public void setUp() {
    baseCaptureBlockMock = Mockito.mock(BaseCaptureBlock.class);
    fryingInferMock = Mockito.mock(FryingInfer.class);
    fryingActionMock = Mockito.mock(FryingAction.class);
    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    Cook cook = cookFactory.createCookObject(CookFactory.filename);
    fryingCookingHandler = new FryingCookingHandler(cook,
      baseCaptureBlockMock, fryingInferMock, fryingActionMock);
  }

  @Test
  public void checkFryingHandle() throws Exception {
    runHandler();
    fryingCookingHandler.handle();
  }

  @Test
  public void checkFryingStatus() throws Exception {
    runHandler();
    fryingCookingHandler.handle();
    Assert.assertEquals(null, fryingCookingHandler.getStatus().getVerboseStatus());
  }

  private void runHandler() throws Exception {
    Mockito.doReturn(new ArrayList<CapturedTaskData>()).when(baseCaptureBlockMock).getCapturedData();
    Mockito.doReturn(new InferredResult(new ArrayList<>())).when(fryingInferMock)
      .infer(Mockito.any());
    Mockito.doNothing().when(fryingActionMock).takeAction(Mockito.any(), Mockito.any());
    new Thread(() -> {
      try {
        Thread.currentThread().sleep(200);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      fryingCookingHandler.setIsCompleted(true);
    }).start();
  }
}
