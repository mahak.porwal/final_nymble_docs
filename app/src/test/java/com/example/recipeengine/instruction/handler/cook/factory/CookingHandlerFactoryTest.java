package com.example.recipeengine.instruction.handler.cook.factory;

import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.cook.CookType;
import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;
import com.example.recipeengine.instruction.handler.cook.boiling.BoilingCookingHandler;
import com.example.recipeengine.instruction.handler.cook.consistency.ConsistencyCookingHandler;
import com.example.recipeengine.instruction.handler.cook.drywet.DryWetCookingHandler;
import com.example.recipeengine.instruction.handler.cook.frying.FryingCookingHandler;
import com.example.recipeengine.instruction.handler.cook.macrosizereduction.MacroSizeCookingHandler;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.example.recipeengine.instruction.cook.CookType.CONSISTENCY;
import static com.example.recipeengine.instruction.cook.CookType.FRYING;

public class CookingHandlerFactoryTest {

  CookingHandlerFactory cookingHandlerFactory;
  Cook cook;
  CookFactory cookFactory;

  @Before
  public void setUp() {
    this.cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    this.cookingHandlerFactory = new CookingHandlerFactory();
  }

//  @Test
//  public void checkGetFryingHandler() {
//    this.cook = cookFactory.createCookObject("./src/main/java/com/example/recipeengine/resources/FryingCook.json");
//    Assert.assertTrue(
//      cookingHandlerFactory.getCookingHandlerObject(this.cook) instanceof FryingCookingHandler);
//  }
//
//  @Test
//  public void checkGetConsistencyHandler() {
//    this.cook = cookFactory.createCookObject("./src/main/java/com/example/recipeengine/resources/Cook.json");
//    Assert.assertTrue(cookingHandlerFactory
//      .getCookingHandlerObject(this.cook) instanceof ConsistencyCookingHandler);
//  }

}
