package com.example.recipeengine.instruction.handler.cook.macrosizereduction;


import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.factory.cook.ActionParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CaptureParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.cook.InferParamsFactory_Factory;
import com.example.recipeengine.instruction.factory.cook.StirrerParamsFactory_Factory;

import com.example.recipeengine.instruction.blocks.capture.BaseCaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;

import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder_Factory;

import java.util.ArrayList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class MacroSizeCookingHandlerTest {

  MacroSizeCookingHandler macroSizeCookingHandler;
  BaseCaptureBlock baseCaptureBlockMock;
  MacroSizeInfer MacroSizeInferMock;
  MacroSizeAction MacroSizeActionMock;

  @Before
  public void setUp() {
    baseCaptureBlockMock = Mockito.mock(BaseCaptureBlock.class);
    MacroSizeInferMock = Mockito.mock(MacroSizeInfer.class);
    MacroSizeActionMock = Mockito.mock(MacroSizeAction.class);
    CookFactory cookFactory = new CookFactory(CookTypeBuilder_Factory.newCookTypeBuilder(),
      StirrerParamsFactory_Factory.newStirrerParamsFactory(), CaptureParamsFactory_Factory.newCaptureParamsFactory(),
      InferParamsFactory_Factory.newInferParamsFactory(), ActionParamsFactory_Factory.newActionParamsFactory());
    Cook cook = cookFactory.createCookObject(CookFactory.filename);
    macroSizeCookingHandler =
      new MacroSizeCookingHandler(cook,
        baseCaptureBlockMock, MacroSizeInferMock, MacroSizeActionMock);
  }

  @Test
  public void checkMacroSizeHandle() throws Exception {
    runHandler();
    macroSizeCookingHandler.handle();
  }

  private void runHandler() throws Exception {
    Mockito.doReturn(new ArrayList<CapturedTaskData>()).when(baseCaptureBlockMock).getCapturedData();
    Mockito.doReturn(new InferredResult(new ArrayList<>())).when(MacroSizeInferMock)
      .infer(Mockito.any());
    Mockito.doNothing().when(MacroSizeActionMock).takeAction(Mockito.any(), Mockito.any());
    new Thread(() -> {
      try {
        Thread.currentThread().sleep(200);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      macroSizeCookingHandler.setIsCompleted(true);
    }).start();
  }

  @Test
  public void checkMacroSizeStatus() throws Exception {
    runHandler();
    macroSizeCookingHandler.handle();
    Assert.assertEquals(null, macroSizeCookingHandler.getStatus().getVerboseStatus());
  }

}
