package com.example.recipeengine.instruction.task.capture.vision.threads;

import android.graphics.Bitmap;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import som.hardware.request.handler.RequestHandler;

public class VisionCameraThreadTest {

  VisionCameraThread visionCameraThread;

  @Before
  public void setUp() {
    visionCameraThread = new VisionCameraThread("VISION_CAMERA",new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10,500, new TriggerType(TriggerType.STIRRER_STARTS),
            new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION),0.8,1.0),
        new ArrayList<Bitmap>(),new RequestHandler());
  }

  @Test
  public void checkVisionCameraThreadRunMAIN_THREAD() {
    visionCameraThread.run();
  }

  @Test
  public void checkVisionCameraThreadName() {
    Assert.assertEquals("VISION_CAMERA",visionCameraThread.getName());
  }
}
