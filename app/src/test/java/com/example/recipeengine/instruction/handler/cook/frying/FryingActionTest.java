package com.example.recipeengine.instruction.handler.cook.frying;



import com.example.recipeengine.instruction.cook.params.action.ActionParameters;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import junit.framework.TestCase;

import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;
import som.hardware.request.handler.RequestHandler;

import static org.mockito.Mockito.mock;

public class FryingActionTest extends TestCase {

	RequestHandler hwRequestHandler;
	PublishSubject<UIData> uiEmitter;
	PublishSubject<ProgressData> dataLogger;
	FryingAction fryingAction;

	public void setUp() throws Exception {
		super.setUp();
		this.hwRequestHandler = mock(RequestHandler.class);
		this.dataLogger = PublishSubject.create();
		this.uiEmitter = PublishSubject.create();
		this.fryingAction = new FryingAction(this.hwRequestHandler, this.uiEmitter, this.dataLogger);
	}

	public void testTakeAction() throws Exception {
		List<Double> score = new LinkedList<>();
		score.add(0.1);   ///Dummy score value
		final InferredResult result = new InferredResult(score);
		final ActionParameters actionParameters = new ActionParameters(0, 0, 95.0, 0.0, 0);
		final FryingAction mockFryingAction = mock(FryingAction.class);
		//case 0 -> no score, exception check
		Mockito.doThrow(IllegalArgumentException.class).when(mockFryingAction).takeAction(null, actionParameters);

		try {
			this.fryingAction.takeAction(null, actionParameters);
		} catch (IllegalArgumentException e) {
			assertNotSame(new IllegalArgumentException() , e);
		}

		//case 0.5 -> null score list
		try {
			this.fryingAction.takeAction(new InferredResult(new LinkedList<>()), actionParameters);
		} catch (AssertionError | Exception e) {
			assertNotSame(new AssertionError(), e);
		}
//        case 1 -> intial score
		score.clear();
		score.add(0.5);
		this.fryingAction.takeAction(result, actionParameters);
		boolean completed = this.fryingAction.isInstCompleted();
		assertEquals("target not reached", false, completed);

		score.clear();
		///case 2 -> reaching score
		score.add(0.75); /// score is still not enough to reach target
		this.fryingAction.takeAction(result, actionParameters);
		completed = this.fryingAction.isInstCompleted();
		assertEquals("target still not reached", false, completed);


		score.clear();
		score.add(0.94);
		this.fryingAction.takeAction(result, actionParameters);
		completed = this.fryingAction.isInstCompleted();
		assertEquals("target reached", true, completed);
	}

	public void testIsInstCompleted() {
		boolean completed = this.fryingAction.isInstCompleted();
		assertEquals("Getting instruction status", completed, false);
	}

	public void testPause() {
		try {
			this.fryingAction.pause();
		} catch (Exception e) {
			assertFalse(false);
		}
	}

	public void testCalcRemainingTimeSec() {
		int rem = this.fryingAction.calcRemainingTimeSec();
		assertEquals("Zero val return, not in use now", rem, 0);
	}
}