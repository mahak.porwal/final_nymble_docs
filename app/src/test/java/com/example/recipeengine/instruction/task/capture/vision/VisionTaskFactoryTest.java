package com.example.recipeengine.instruction.task.capture.vision;

import com.example.recipeengine.instruction.task.capture.vision.threads.SyncVisionThermalThread;
import com.example.recipeengine.instruction.task.capture.vision.threads.ThermalCameraThread;
import com.example.recipeengine.instruction.task.capture.vision.threads.VisionCameraThread;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class VisionTaskFactoryTest {

  VisionTaskFactory visionTaskFactory;

  @Before
  public void setUp() {
    visionTaskFactory = new VisionTaskFactory(
        null,null,null,null,null);
  }

  @Test
  public void checkGetVisionTaskInstanceVision() {
    Assert.assertTrue(visionTaskFactory.getVisionTaskInstance("VISION_CAMERA") instanceof VisionCameraThread);
  }

  @Test
  public void checkGetVisionTaskInstanceThermal() {
    Assert.assertTrue(visionTaskFactory.getVisionTaskInstance("THERMAL_CAMERA") instanceof ThermalCameraThread);
  }

  @Test
  public void checkGetVisionTaskInstanceSync() {
    Assert.assertTrue(visionTaskFactory.getVisionTaskInstance("SYNC_THERMAL_VISUAL") instanceof SyncVisionThermalThread);
  }

}
