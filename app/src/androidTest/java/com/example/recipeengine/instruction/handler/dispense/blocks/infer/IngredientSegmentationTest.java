package com.example.recipeengine.instruction.handler.dispense.blocks.infer;

import android.graphics.Bitmap;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

@RunWith(AndroidJUnit4.class)
public class IngredientSegmentationTest {
  IngredientSegmentation ingredientSegmentation;
  String modelName;

  @Before
  public void setUp() throws Exception {
    OpenCVLoader.initDebug();
    this.modelName = "ing_seg_babycorn_eggplant_onion.tflite";
    this.ingredientSegmentation = new IngredientSegmentation(modelName);
  }

  @Test
  public void testIngredientSegmentation() {

    final Bitmap.Config conf = Bitmap.Config.ARGB_4444; // see other conf types
    final Bitmap image = Bitmap.createBitmap(1920, 1080, conf); // this creates a MUTABLE bitmap
    try {
      ingredientSegmentation.loadModel();
    } catch (IOException e) {
      e.printStackTrace();
    }
    Mat mat = ingredientSegmentation.runSegmentation(image);
  }
}
