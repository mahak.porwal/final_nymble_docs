package com.example.recipeengine.instruction.handler.cook.boiling;

import android.graphics.Bitmap;


import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import java.util.ArrayList;
import junit.framework.TestCase;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;


//Not working - cannot test boiling.  No implementation found for long org.opencv.video.Video.createOptFlow_DualTVL1_0()
//Although already tested in Vision dev branch in Julia_android repo
public class BoilingInferTest extends TestCase {

	BoilingInfer boilingInfer;
	RecipeContext recipeContext;
	PublishSubject<UIData> uiEmitter;
	PublishSubject<ProgressData> dataLogger;

	public void setUp() throws Exception {
		super.setUp();
		this.recipeContext = new RecipeContext(2, "Kheer");
		this.uiEmitter = PublishSubject.create();
		this.dataLogger = PublishSubject.create();
		this.boilingInfer = new BoilingInfer(this.recipeContext, this.uiEmitter, this.dataLogger);
	}

	public void testInfer() {
		List<Bitmap> bmps = new LinkedList<>();

		final Bitmap.Config conf = Bitmap.Config.ARGB_4444; // see other conf types
		final Bitmap image = Bitmap.createBitmap(1920, 1080, conf); // this creates a MUTABLE bitmap

		bmps.add(image);
		bmps.add(image);
		bmps.add(image);
		bmps.add(image);
		bmps.add(image);
		CapturedTaskData capturedTaskData = new CapturedTaskData(null,bmps,null,null);
		List<CapturedTaskData> capturedTaskDataList = new ArrayList<CapturedTaskData>();
		capturedTaskDataList.add(capturedTaskData);
		this.boilingInfer.infer(capturedTaskDataList);
	}

	public void testPause() {
	}
}