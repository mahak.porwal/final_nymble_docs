package com.example.recipeengine.instruction.handler.cook.consistency;

import android.graphics.Bitmap;

import com.example.recipeengine.instruction.cook.params.infer.InferParameters;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;

import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import java.util.ArrayList;
import junit.framework.TestCase;

import org.junit.Rule;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.pytorch.Module;

import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.mock;

public class ConsistencyInferTest extends TestCase {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();


    CapturedTaskData capturedTaskData;
    ConsistencyInfer infer ;
    InferParameters inferParameters;
    RecipeContext recipeContext;

    public void setUp() throws Exception {
        super.setUp();
        this.inferParameters = new InferParameters("consistency_kheer", "consistency_model.pt");
        this.recipeContext = new RecipeContext(2, "Kheer");
        this.infer = new ConsistencyInfer(this.inferParameters, this.recipeContext, null, null);
    }

    public void tearDown() throws Exception {
    }

    public void testLoadModel() throws Exception {
        ConsistencyInfer consInfer = mock(ConsistencyInfer.class);
        Mockito.doThrow(Exception.class).when(consInfer).loadModel();

        Mockito.doNothing().when(consInfer).loadModel();
    }

    public void testInfer() {
        final Module model = mock(Module.class);
        this.infer.setModel(model);   //setting a mock model object
        this.capturedTaskData = new CapturedTaskData();  // Giving null captured data
        List<CapturedTaskData> capturedTaskDataList = new ArrayList<>();
        /* test case 1: null capture data */
        capturedTaskDataList.add(capturedTaskData);
        final InferredResult res = this.infer.infer(capturedTaskDataList);
        assertEquals("null for no images found", res, null);

        final List<Bitmap> bmps = new LinkedList<>();
        final Bitmap.Config conf = Bitmap.Config.ARGB_4444; // see other conf types
        final Bitmap image = Bitmap.createBitmap(1920, 1080, conf); // this creates a MUTABLE bitmap
        bmps.add(image);
        bmps.add(image);
        bmps.add(image);
        bmps.add(image);
        this.capturedTaskData = new CapturedTaskData();  //giving some required images
        capturedTaskDataList.clear();
        capturedTaskDataList.add(capturedTaskData);
        /*test case 2: With all data and mocked model object */
        final InferredResult res2 = this.infer.infer(capturedTaskDataList);
        assert null != res2;
        assertEquals("Result is null for mocked model object", res2.getResult(), null);
    }

}