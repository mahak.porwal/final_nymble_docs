package com.example.recipeengine.instruction.handler.dispense.blocks.infer;

import android.graphics.Bitmap;


import androidx.test.ext.junit.runners.AndroidJUnit4;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParameters;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import java.util.ArrayList;
import junit.framework.TestCase;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.opencv.android.OpenCVLoader;

@RunWith(AndroidJUnit4.class)
public class OnionInferTest{

  OnionInfer onionInfer;
  RecipeContext recipeContext;
  PublishSubject<UIData> uiEmitter;
  PublishSubject<ProgressData> dataLogger;
  InferParams inferParams;

  @Before
  public void setUp() throws Exception {
    OpenCVLoader.initDebug();
    this.recipeContext = new RecipeContext(2, "Kheer");
    this.inferParams = new InferParameters("","ing_seg_babycorn_eggplant_onion.tflite");
    this.uiEmitter = PublishSubject.create();
    this.dataLogger = PublishSubject.create();
    this.onionInfer = new OnionInfer(recipeContext,inferParams,uiEmitter,dataLogger);
  }

  @Test
  public void testOnionInfer() {
    List<Bitmap> bmps = new LinkedList<>();

    final Bitmap.Config conf = Bitmap.Config.ARGB_4444; // see other conf types
    final Bitmap image = Bitmap.createBitmap(1920, 1080, conf); // this creates a MUTABLE bitmap

    bmps.add(image);
    bmps.add(image);
    CapturedTaskData capturedTaskData = new CapturedTaskData(null,bmps,null,null);
    List<CapturedTaskData> capturedTaskDataList = new ArrayList<CapturedTaskData>();
    capturedTaskDataList.add(capturedTaskData);
    try {
      this.onionInfer.infer(capturedTaskDataList);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
