package som.instruction.request;

public enum RequestParameters {
  HEAT_POWER_LEVEL("heat_power_level"),
  STIRRER_PROFILE("stirrer_profile"),
  STIRRER_ANGLE("stirrer_angle"),
  INGREDIENT("ingredient"),
  QUANTITY("quantity"),
  DISPENSE_CONTAINER("dispense_container"),
  DISPENSE_BEHAVIOR("dispense_behavior"),
  SENSOR("sensor"),
  ACTUATOR("actuator"),
  ACTUATOR_SPEED("actuator_speed"),
  ACTUATOR_FREQUENCY("actuator_frequency"),
  ACTUATOR_STATE("actuator_state"),
  VISUAL_CAMERA_COMMAND("visual_camera_command"),
  ACTUATOR_DIRECTION("actuator_direction"),
  SAVE_FILE_PATH_VISUAL_CAMERA("save_path_visual_camera");
  private final String requestParameter;

  RequestParameters(String requestParameter) {
    this.requestParameter = requestParameter;
  }

  public String getRequestParameter() {
    return this.requestParameter;
  }
}
