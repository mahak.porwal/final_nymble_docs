package som.instruction.request.heat;

public enum HeatPowerLevel {
  POWER_LEVEL_ZERO("Shutdown",0),
  POWER_LEVEL_ONE("Power_Level_One",200),
  POWER_LEVEL_TWO("Power_Level_Two",400),
  POWER_LEVEL_THREE("Power_Level_Three",600),
  POWER_LEVEL_FOUR("Power_Level_Four",800),
  POWER_LEVEL_FIVE("Power_Level_Five",1000),
  POWER_LEVEL_SIX("Power_Level_Six",1200),
  POWER_LEVEL_SEVEN("Power_Level_Seven",1400);

  private final String powerLevel;
  private final int powerLevelWattage;

  HeatPowerLevel(String powerLevel,int wattage) {
    this.powerLevel = powerLevel;this.powerLevelWattage = wattage;
  }

  public String getPowerLevel() {
    return this.powerLevel;
  }

  public int getPowerLevelWattage() {
    return this.powerLevelWattage;
  }
}
