package som.instruction.request.heat;

import androidx.annotation.NonNull;

import java.util.HashMap;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.Request;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;

/**
 * Heat request from the instruction handler.
 */
public class Heat extends Request {

  /**
   * Set the requester, requestType and instantiates the parameters map.
   *
   * @param instructionHandler The {@link InstructionHandler} which requested this request
   */
  public Heat(InstructionHandler instructionHandler) {
    this.requester = instructionHandler;
    this.requestType = RequestType.HEAT;
    this.parameters = new HashMap<>();
  }

  public void setPowerLevel(@NonNull HeatPowerLevel heatPowerLevel) {
    this.parameters.put(RequestParameters.HEAT_POWER_LEVEL.getRequestParameter(),
        heatPowerLevel.getPowerLevel());
  }
}
