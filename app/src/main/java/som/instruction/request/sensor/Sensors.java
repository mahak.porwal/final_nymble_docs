package som.instruction.request.sensor;

public enum Sensors {
  THERMAL_CAMERA("Thermal_Camera"),
  WEIGHT_SENSOR("Weight_Sensor"),
  STIRRER_CURRENT("Stirrer_Current");

  private final String sensorName;

  Sensors(String name) {
    this.sensorName = name;
  }

  public String getSensorName() {
    return this.sensorName;
  }
}

