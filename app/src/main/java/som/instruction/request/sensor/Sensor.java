package som.instruction.request.sensor;

import androidx.annotation.NonNull;

import java.util.HashMap;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.Request;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;

/**
 * Sensor from the instruction handler.
 */
public class Sensor extends Request {
  /**
   * Set the requester, requestType and instantiates the parameters map.
   *
   * @param instructionHandler The {@link InstructionHandler} which requested this request
   */
  public Sensor(InstructionHandler instructionHandler) {
    this.requester = instructionHandler;
    this.requestType = RequestType.SENSOR;
    this.parameters = new HashMap<>();
  }

  public void setSensor(@NonNull Sensors sensor) {
    this.parameters.put(RequestParameters.SENSOR.getRequestParameter(),
        sensor.getSensorName());
  }
}
