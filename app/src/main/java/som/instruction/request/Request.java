package som.instruction.request;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import som.instruction.handler.InstructionHandler;

/**
 * Base class for requests from the instruction handler.
 */
public abstract class Request {
  protected RequestType requestType;
  protected InstructionHandler requester;
  protected HashMap<String, String> parameters;

  public RequestType getRequestType() {
    return this.requestType;
  }

  public InstructionHandler getRequester() {
    return this.requester;
  }

  public @Nullable
  Map<String, String> getParameters() {
    return Collections.unmodifiableMap(this.parameters);
  }
}
