package som.instruction.request.stir;

public enum StirringProfile {
  SLOW("Slow_Stirring"),
  MEDIUM("Medium_Stirring"),
  FAST("Fast_Stirring");

  private final String stirringProfile;

  StirringProfile(String profile) {
    this.stirringProfile = profile;
  }

  public String getStirringProfile() {
    return this.stirringProfile;
  }
}
