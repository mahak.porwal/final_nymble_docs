package som.instruction.request.stir.position.manager;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;

import som.instruction.request.stir.position.actuator.StirrerActuator;
import som.instruction.request.stir.position.actuator.StirrerActuatorImpl;
import som.instruction.request.stir.position.detector.ArucoBasedDetector;
import som.instruction.request.stir.position.detector.ModelBasedDetector;
import som.instruction.request.stir.position.detector.StirrerAngleDetector;
import som.instruction.request.stir.position.finder.StirrerRangeFinder;
import som.instruction.request.stir.position.finder.StirrerRangeFinderImpl;
import som.instruction.request.stir.position.validator.StirrerPositionValidator;
import som.instruction.request.stir.position.validator.StirrerPositionValidatorImpl;

@Module
public abstract class StirPositionManagerModule {
  @Binds
  abstract StirrerRangeFinder bindsStirrerRangeFinder(StirrerRangeFinderImpl stirrerRangeFinder);

  @Binds
  abstract StirrerActuator bindsStirrerActuator(StirrerActuatorImpl stirrerActuator);

  @Binds
  abstract StirrerPositionValidator bindsStirrerPositionValidator(StirrerPositionValidatorImpl stirrerPositionValidator);

  @Binds
  @Named("ArucoBasedDetector")
  abstract StirrerAngleDetector bindsArucoBasedDetector(ArucoBasedDetector arucoBasedDetector);

  @Binds
  @Named("ModelBasedDetector")
  abstract StirrerAngleDetector bindsModelBasedDetector(ModelBasedDetector modelBasedDetector);
}
