package som.instruction.request.stir.position.detector;

import android.graphics.Bitmap;

import com.example.recipeengine.util.ImageUtil;

import org.opencv.android.Utils;
import org.opencv.aruco.Aruco;
import org.opencv.aruco.DetectorParameters;
import org.opencv.aruco.Dictionary;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class ArucoBasedDetector implements StirrerAngleDetector {
  final private Dictionary dictionary;

  @Inject
  ArucoBasedDetector() {
    this.dictionary = Aruco.custom_dictionary(2, 4);
  }

  @Override
  public Integer getStirrerAngle(Bitmap bitmap) {
    Integer detectedAngle = null;
    final Bitmap bmp32 = bitmap.copy(Bitmap.Config.RGB_565, true);
    final Mat markerIds = new Mat();

    final DetectorParameters parameters = DetectorParameters.create();
    final List<Mat> markerCorners = new ArrayList<>();

    Mat markerImage = new Mat();
    Utils.bitmapToMat(bmp32, markerImage);

    markerImage = ImageUtil._cropImage(markerImage);

    Imgproc.cvtColor(markerImage, markerImage, Imgproc.COLOR_RGBA2GRAY);

    org.opencv.core.Core.flip(markerImage, markerImage, 1);
    Aruco.detectMarkers(markerImage, this.dictionary, markerCorners, markerIds, parameters);

    if (markerCorners.isEmpty()) {
      Timber.d("Aruco not found");
    } else {
      double stirrerAngle = 0;
      for (Mat i : markerCorners) {
        final double[] zerothCorner = i.get(0, 0);
        final double[] firstCorner = i.get(0, 1);
        final double[] secondCorner = i.get(0, 2);
        final double[] thirdCorner = i.get(0, 3);
        final double[] center = new double[2];
        center[0] = (((zerothCorner[0] + firstCorner[0]) / 2) + ((secondCorner[0] +
          thirdCorner[0]) / 2)) / 2;
        center[1] = (((zerothCorner[1] + firstCorner[1]) / 2) + ((secondCorner[1] +
          thirdCorner[1]) / 2)) / 2;
        stirrerAngle = (Math.atan2(zerothCorner[1] - firstCorner[1], zerothCorner[0] -
          firstCorner[0]));
        stirrerAngle = (0 < stirrerAngle ? stirrerAngle : (2 * Math.PI + stirrerAngle));
        stirrerAngle = Math.toDegrees(stirrerAngle);
        Timber.d("Aruco Angle stir : %f ", stirrerAngle);
      }
      if (!markerCorners.isEmpty()) {
        Aruco.drawDetectedMarkers(markerImage, markerCorners, markerIds);
      }

      Bitmap markerBmp = null;
      try {
        markerBmp = Bitmap.createBitmap(markerImage.cols(), markerImage.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(markerImage, markerBmp);
      } catch (Exception e) {
        Timber.e(e);
      }
      if (null != markerBmp) {
        // Publish to UI
        //uiSubject.onNext(new ImageEvent.StirrerDetect(markerBmp));
      }
      detectedAngle = (int) stirrerAngle;
    }
    return detectedAngle;
  }
}
