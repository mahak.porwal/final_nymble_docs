package som.instruction.request.stir.position.manager;

import android.graphics.Bitmap;

import com.example.recipeengine.util.Retry;

import javax.inject.Inject;
import javax.inject.Named;

import som.hardware.request.handler.RequestHandler;
import som.hardware.response.CameraResponse;
import som.instruction.request.dispense.DispenseContainer;
import som.instruction.request.stir.position.validator.StirrerPositionValidator;
import som.instruction.request.stir.position.UnableToPositionException;
import som.instruction.request.stir.position.actuator.StirrerActuator;
import som.instruction.request.stir.position.detector.StirrerAngleDetector;
import som.instruction.request.stir.position.finder.StirrerRangeFinder;
import som.instruction.request.visionCamera.VisionCamera;
import som.instruction.request.visionCamera.VisionCameraCommand;

public class StirrerPositionManager {
  final StirrerAngleDetector arucoBasedDetector;
  final StirrerAngleDetector modelBasedDetector;
  final StirrerRangeFinder stirrerRangeFinder;
  final StirrerPositionValidator stirrerPositionValidator;
  final StirrerActuator stirrerActuator;
  int attemptNum;

  @Inject
  public StirrerPositionManager(@Named("ArucoBasedDetector") StirrerAngleDetector arucoBasedDetector,
                                @Named("ModelBasedDetector") StirrerAngleDetector modelBasedDetector,
                                StirrerRangeFinder stirrerRangeFinder,
                                StirrerPositionValidator stirPosValidator,
                                StirrerActuator stirrerActuator) {
    this.arucoBasedDetector = arucoBasedDetector;
    this.modelBasedDetector = modelBasedDetector;
    this.stirrerRangeFinder = stirrerRangeFinder;
    this.stirrerPositionValidator = stirPosValidator;
    this.stirrerActuator = stirrerActuator;
  }

  /**
   * Positions the stirrer to a safe location
   *
   * @param dispenseContainers containers that will get dispensed
   * @throws Exception
   */
  public void positionStirrer(final DispenseContainer[] dispenseContainers) throws Exception {
    Retry.Do(this::executePositionLogic, dispenseContainers, 3,
      UnableToPositionException.class, "Max retries for positioning exceeded");
  }

  private void executePositionLogic(final DispenseContainer[] dispenseContainers) throws Exception {
    //calculate stirrer angle
    final int stirrerAngle = this.calcStirrerAngle();

    //find target angle range of stirrer
    final Integer[] validRange = this.stirrerRangeFinder.getStirrerRange(dispenseContainers);

    //validate the current angle, if valid return, if not proceed further
    final boolean isRangeValid = this.stirrerPositionValidator.isPositionValid(stirrerAngle,
      validRange);

    if (isRangeValid) {
      this.attemptNum = 0;
      return;
    }
    //actuate stirrer
    this.stirrerActuator.actuateStirrer(stirrerAngle, validRange, this.attemptNum);

    //calculate stirrer angle
    final int verificationAngle = this.calcStirrerAngle();

    //validate the current angle of stirrer
    final boolean isVerified = this.stirrerPositionValidator.isPositionValid(verificationAngle,
      validRange);

    if (!isVerified) {
      this.attemptNum++;
      throw new UnableToPositionException("Stirrer is in invalid position");
    }
  }


  private int calcStirrerAngle() throws Exception {
    //capture image
    final Bitmap image = this.captureImage();
    //find current angle of stirrer -> use aruco detection, if failed use stirrer model detection
    Integer detectedAngle = this.arucoBasedDetector.getStirrerAngle(image);
    if (null == detectedAngle) {
      detectedAngle = this.modelBasedDetector.getStirrerAngle(image);
    }
    return detectedAngle.intValue();
  }

  private Bitmap captureImage() throws Exception {
    final RequestHandler requestHandler = new RequestHandler();
    final VisionCamera camera = new VisionCamera(null);
    camera.setCameraCommand(VisionCameraCommand.STILL_CAPTURE);
    final CameraResponse cameraResponse = (CameraResponse) requestHandler.handleRequest(camera);
    return cameraResponse.getImage();
  }
}
