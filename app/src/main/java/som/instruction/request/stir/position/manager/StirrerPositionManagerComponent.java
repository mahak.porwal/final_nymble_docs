package som.instruction.request.stir.position.manager;

import dagger.Component;

@Component(modules = StirPositionManagerModule.class)
public interface StirrerPositionManagerComponent {
  StirrerPositionManager buildsStirrerPositionManager();
}
