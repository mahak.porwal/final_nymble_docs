package som.instruction.request.stir.position.actuator;

public interface StirrerActuator {
  /**
   * Rotates the stirrer to move to a valid range, blockingly
   *
   * @param currentAngle    current angle of stirrer
   * @param validAngleRange valid range for the stirrer to reside
   */
  void actuateStirrer(Integer currentAngle, Integer[] validAngleRange, Integer stirrerActuator) throws Exception;
}
