package som.instruction.request.stir.position.finder;

import javax.inject.Inject;

import som.instruction.request.dispense.DispenseContainer;

public class StirrerRangeFinderImpl implements StirrerRangeFinder {

  public static final int MICRO_POD_ONE_LOWER_ANGLE = 255;
  public static final int MICRO_POD_ONE_UPPER_ANGLE = 135;
  public static final int MICRO_POD_TWO_LOWER_ANGLE = 315;
  public static final int MICRO_POD_TWO_UPPER_ANGLE = 180;
  public static final int MICRO_POD_THREE_LOWER_ANGLE = 75;
  public static final int MICRO_POD_THREE_UPPER_ANGLE = 255;
  public static final int MICRO_POD_FOUR_LOWER_ANGLE = 135;
  public static final int MICRO_POD_FOUR_UPPER_ANGLE = 360;
  public static final int MICRO_POD_FIVE_LOWER_ANGLE = 180;
  public static final int MICRO_POD_FIVE_UPPER_ANGLE = 45;
  public static final int MICRO_POD_SIX_LOWER_ANGLE = 225;
  public static final int MICRO_POD_SIX_UPPER_ANGLE = 135;
  public static final int LIQUID_LOWER_ANGLE = 135;
  public static final int LIQUID_UPPER_ANGLE = 45;
  public static final int MACRO_LOWER_ANGLE = 255;
  public static final int MACRO_UPPER_ANGLE = 75;

  @Inject
  StirrerRangeFinderImpl() {
  }

  @Override
  public Integer[] getStirrerRange(DispenseContainer[] containers) {
    final Integer[] validRanges = new Integer[2];
    int upperAngle, lowerAngle;
    switch (containers[0]) {
      case LIQUID_OIL_CONTAINER:
      case LIQUID_WATER_CONTAINER:
        lowerAngle = LIQUID_LOWER_ANGLE;
        upperAngle = LIQUID_UPPER_ANGLE;
        break;
      case MACRO_CONTAINER_1:
      case MACRO_CONTAINER_2:
      case MACRO_CONTAINER_3:
      case MACRO_CONTAINER_4:
        lowerAngle = MACRO_LOWER_ANGLE;
        upperAngle = MACRO_UPPER_ANGLE;
        break;
      case MICRO_POD_1:
        lowerAngle = MICRO_POD_ONE_LOWER_ANGLE;
        upperAngle = MICRO_POD_ONE_UPPER_ANGLE;
        break;
      case MICRO_POD_2:
        lowerAngle = MICRO_POD_TWO_LOWER_ANGLE;
        upperAngle = MICRO_POD_TWO_UPPER_ANGLE;
        break;
      case MICRO_POD_3:
        lowerAngle = MICRO_POD_THREE_LOWER_ANGLE;
        upperAngle = MICRO_POD_THREE_UPPER_ANGLE;
        break;
      case MICRO_POD_4:
        lowerAngle = MICRO_POD_FOUR_LOWER_ANGLE;
        upperAngle = MICRO_POD_FOUR_UPPER_ANGLE;
        break;
      case MICRO_POD_5:
        lowerAngle = MICRO_POD_FIVE_LOWER_ANGLE;
        upperAngle = MICRO_POD_FIVE_UPPER_ANGLE;
        break;
      case MICRO_POD_6:
        lowerAngle = MICRO_POD_SIX_LOWER_ANGLE;
        upperAngle = MICRO_POD_SIX_UPPER_ANGLE;
        break;
      default:
        throw new IllegalStateException("Unexpected value: " + containers[0]);
    }
    validRanges[0] = lowerAngle;
    validRanges[1] = upperAngle;
    return validRanges;
  }
}
