package som.instruction.request.stir.position.finder;

import som.instruction.request.dispense.DispenseContainer;

public interface StirrerRangeFinder {
  /**
   * Returns a safe range for the stirrer to be placed, in order to dispense
   * the containers
   *
   * @return safe range for the stirrer to reside
   */
  Integer[] getStirrerRange(final DispenseContainer[] containers);
}
