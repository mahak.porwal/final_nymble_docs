package som.instruction.request.stir.position.detector;

import android.graphics.Bitmap;

import com.example.recipeengine.util.FileUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.pytorch.IValue;
import org.pytorch.Module;
import org.pytorch.Tensor;
import org.pytorch.torchvision.TensorImageUtils;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

import timber.log.Timber;

public class StirrerDetector {
  private Module module;
  private static final String MODEL_NAME = "stirrer_detect_mobilenet.pt";
  private static final int MODEL_INPUT_SIZE = 224;

  @Inject
  public StirrerDetector() {
  }

  public void loadModel() {
    try {
      this.module = Module.load(fetchModelFile());
      Timber.d("Model loaded");
    } catch (IOException e) {
      Timber.d("Model loading error");
      Timber.e(e);
    }
    Timber.d("Model loaded");
  }

  public @Nullable String fetchModelFile() throws IOException {
    final File file = FileUtil.getModelFile(this.MODEL_NAME);

    if (file.exists() && 0 < file.length()) {
      Timber.d("Model file found");
      return file.getAbsolutePath();
    }
    return null;
  }

  public double recognizeImage(@NotNull final Bitmap bitmap) throws Exception {
    if (null == this.module) {
      throw new Exception("Stirrer detector model file not loaded");
    }
    final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap,
      MODEL_INPUT_SIZE, MODEL_INPUT_SIZE, true);
    final Mat image = new Mat();
    Utils.bitmapToMat(bitmap, image);

    final Tensor input = TensorImageUtils.bitmapToFloat32Tensor(
      scaledBitmap,
      TensorImageUtils.TORCHVISION_NORM_MEAN_RGB,
      TensorImageUtils.TORCHVISION_NORM_STD_RGB
    );

    //Calling the forward of the model to run our input

    final long start = System.currentTimeMillis();
    final Tensor output = this.module.forward(IValue.from(input)).toTensor();

    final long end = System.currentTimeMillis();
    Timber.d("Stirrer detect Time in infer = %d", end - start);
//        Timber.d("%s", output);

    final float[] scoreArr = output.getDataAsFloatArray();
    for (int i = 0; i < scoreArr.length; i++) {
      //Mapping stirrer from 224 image size from model's to original size
      scoreArr[i] = (scoreArr[i] * bitmap.getWidth()) / MODEL_INPUT_SIZE;
    }

    final Point midPoint1 = new Point((scoreArr[0] + scoreArr[2]) / 2,
      (scoreArr[1] + scoreArr[3]) / 2);
    final Point midPoint2 = new Point((scoreArr[6] + scoreArr[4]) / 2,
      (scoreArr[7] + scoreArr[5]) / 2);
    double angle = Math.atan2(midPoint1.y - midPoint2.y, midPoint1.x - midPoint2.x);
    angle = angle < 0 ? 2 * Math.PI + angle : angle;
    angle = Math.toDegrees(angle);
    return angle;
  }

  /**
   * Close the pytorch module
   */
  public void close() {
    if (null != this.module) {
      this.module.destroy();
    }
  }

}
