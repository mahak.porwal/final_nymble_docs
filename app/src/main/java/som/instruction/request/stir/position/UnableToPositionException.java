package som.instruction.request.stir.position;

public class UnableToPositionException extends Exception {
  public UnableToPositionException(String err) {
    super(err);
  }
}
