package som.instruction.request.stir.position.detector;

import android.graphics.Bitmap;

public interface StirrerAngleDetector {
  /**
   * Returns the angle of stirrer by capturing and inferring a visual image
   *
   * @param image Bitmap to detect the angle of stirrer
   * @return detected angle
   */
  Integer getStirrerAngle(final Bitmap image);
}
