package som.instruction.request.stir;

import androidx.annotation.NonNull;

import java.util.HashMap;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.Request;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;
import som.instruction.request.dispense.DispenseContainer;

/**
 * Stirring request from the instruction handler.
 */
public class Stir extends Request {

  /**
   * Sets the requestType, requester and instantiates the parameters map.
   *
   * @param instructionHandler {@link InstructionHandler} requested the stir request
   */
  public Stir(InstructionHandler instructionHandler) {
    this.requestType = RequestType.STIR;
    this.requester = instructionHandler;
    this.parameters = new HashMap<>();
  }

  public void setStirrer(@NonNull Integer speed) {

  }

  public void setStirringProfile(@NonNull StirringProfile stirringProfile) {
    this.parameters.put(RequestParameters.STIRRER_PROFILE.getRequestParameter(),
      stirringProfile.getStirringProfile());
  }

  public void setAngle(@NonNull Double angle) {
    this.parameters.put(RequestParameters.STIRRER_ANGLE.getRequestParameter(),
      angle.toString());
  }


  /**
   * Positions stirrer according to the given dispenseContainers
   *
   * @param dispenseContainers Containers to be dispensed
   */
  public void positionStirrer(final DispenseContainer[] dispenseContainers) {
    //capture image
    //find current angle of stirrer -> use aruco detection, if failed use stirrer model detection
    //find target angle of stirrer
    //validate the current angle, if valid return, if not proceed further
    //actuate stirrer
    //find current angle of stirrer
    //validate the current angle of stirrer
  }
}
