package som.instruction.request.stir.position.validator;

public interface StirrerPositionValidator {
  /**
   * Returns whether or not the position of the stirrer is valid, given the
   * the current position and the valid range
   *
   * @param currentAngle current angle of the stirrer
   * @param validRange   valid range for the stirrer to reside
   * @return
   */
  boolean isPositionValid(Integer currentAngle, Integer[] validRange);
}
