package som.instruction.request.stir.position.detector;

import static com.example.recipeengine.util.ImageUtil._cropImage;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;


import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;

import javax.inject.Inject;

import timber.log.Timber;


public class ModelBasedDetector implements StirrerAngleDetector {

  final private StirrerDetector stirrerDetector;

  @Inject
  ModelBasedDetector(StirrerDetector stirrerDetector) {
    this.stirrerDetector = stirrerDetector;
    this.stirrerDetector.loadModel();
  }

  @Override
  public Integer getStirrerAngle(@NonNull Bitmap bitmap) {
    Mat tempImage = new Mat();
    Utils.bitmapToMat(bitmap, tempImage);
    tempImage = _cropImage(tempImage);
    Core.flip(tempImage, tempImage, 1);
    Bitmap croppedImage = null;
    double angle = 0;
    try {
      croppedImage = Bitmap.createBitmap(tempImage.cols(), tempImage.rows(), Bitmap.Config.ARGB_8888);
      Utils.matToBitmap(tempImage, croppedImage);
      angle = this.stirrerDetector.recognizeImage(croppedImage);
    } catch (Exception e) {
      Timber.e(e);
    } finally {
      Timber.d("Utility: Model Found Angle stir : %f", angle);
    }
    return (int) angle;
  }
}
