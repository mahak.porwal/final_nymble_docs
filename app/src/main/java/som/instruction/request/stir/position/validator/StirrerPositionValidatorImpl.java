package som.instruction.request.stir.position.validator;

import javax.inject.Inject;

public class StirrerPositionValidatorImpl implements StirrerPositionValidator {

  @Inject
  StirrerPositionValidatorImpl() {
  }

  @Override
  public boolean isPositionValid(Integer currentAngle, Integer[] validRange) {
    final int lowerAngle = validRange[0];
    final int upperAngle = validRange[1];
    boolean isAngleValid;
    if (lowerAngle > upperAngle) {
      isAngleValid = !((currentAngle > upperAngle) && (currentAngle < lowerAngle));
    } else {
      isAngleValid = ((currentAngle > lowerAngle) && (currentAngle < upperAngle));
    }
    return isAngleValid;
  }
}
