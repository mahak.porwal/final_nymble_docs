package som.instruction.request.stir.position.actuator;

import android.util.Log;
import javax.inject.Inject;

import org.opencv.core.Mat;
import som.hardware.message.action.ActionRequestParameters.ActuatorDirection;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.actuator.Actuator;
import som.instruction.request.actuator.Actuators;

public class StirrerActuatorImpl implements StirrerActuator {

  private static final int SPEED_INCREMENT_FACTOR = 6;
  private static final int DEFAULT_STIRRER_SPEED = 20;
  private static final int MAX_STIRRER_SPEED = 32;
  private static final float DEFAULT_STIRRER_VOLTAGE = 4.0f;
  public static final int FULL_CIRCLE = 360;
  private float speed;
  private float voltage;

  @Inject
  StirrerActuatorImpl() {
  }

  /**
   * Rotates the stirrer to move to a valid range, in a blocking manner
   *
   * @param currentAngle    current angle of stirrer
   * @param validAngleRange valid range for the stirrer to reside
   * @param attemptNum
   */
  @Override
  public void actuateStirrer(Integer currentAngle, Integer[] validAngleRange, Integer attemptNum)
    throws Exception {
    final int targetAngle = this.calcTargetAngle(validAngleRange);
    this.calculateAndSetSpeed(attemptNum);
    final float stirTime = this.stirrerTimeCalc(currentAngle, targetAngle);
    this.createAndSendStirrerCommand(stirTime);
  }

  private float stirrerTimeCalc(final int currentAngle, final int targetAngle) {
    int diffAngle = targetAngle - currentAngle;
    if ((-180 > diffAngle) || (0 < diffAngle && 180 > diffAngle)) {
      //positive speed
      if (-180 > diffAngle) {
        diffAngle = Math.abs(diffAngle);
        diffAngle = 360 - diffAngle;
      }
      return diffAngle / (this.speed * -1);
    } else {
      //negative speed
      if (180 < diffAngle) {
        diffAngle = 360 - diffAngle;
      }
      return Math.abs(diffAngle) / this.speed;
    }
  }

  private void calculateAndSetSpeed(final int attemptNum) {
    this.speed = SPEED_INCREMENT_FACTOR * attemptNum +
      DEFAULT_STIRRER_SPEED;
    if (MAX_STIRRER_SPEED < this.speed) {
      this.speed = MAX_STIRRER_SPEED;
    }
  }

  private void createAndSendStirrerCommand(double time) throws Exception {
    this.calculateAndSetVoltage();
    final RequestHandler requestHandler = new RequestHandler();
    final Actuator actuator = new Actuator(null);
    actuator.setActuator(Actuators.STIRRER);
    actuator.setActuatorSpeed((int) this.voltage);
    if (0 < time) {
      //stirrer rotates anti-clockwise
      actuator.setActuatorDirection(ActuatorDirection.DIRECTION_ANTICLOCKWISE);
    } else {
      //stirrer rotates clockwise
      actuator.setActuatorDirection(ActuatorDirection.DIRECTION_CLOCKWISE);
    }
    requestHandler.handleRequest(actuator);
    time = Math.abs(time*1000);
    Thread.sleep((long) time);
    actuator.setActuatorSpeed(0);
    requestHandler.handleRequest(actuator);
  }

  private void calculateAndSetVoltage() {
    this.voltage = Math.abs(((this.speed - DEFAULT_STIRRER_SPEED) / SPEED_INCREMENT_FACTOR)
        + DEFAULT_STIRRER_VOLTAGE);
  }

  private int calcTargetAngle(Integer[] validRange) {
    final int lowerAngle = validRange[0];
    final int upperAngle = validRange[1];
    int targetAngle = (lowerAngle + upperAngle) / 2;
    if (lowerAngle > upperAngle) {
      //isAngleValid = !((currentAngle > upperAngle) && (currentAngle < lowerAngle));
      targetAngle = (targetAngle + (FULL_CIRCLE / 2)) % FULL_CIRCLE;
    }
    return targetAngle;
  }
}
