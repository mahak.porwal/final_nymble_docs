package som.instruction.request;

public enum RequestType {
  STIR,
  DISPENSE,
  VISION_CAMERA,
  HEAT,
  SENSOR,
  ACTUATOR
}
