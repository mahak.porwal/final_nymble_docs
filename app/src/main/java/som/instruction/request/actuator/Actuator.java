package som.instruction.request.actuator;

import androidx.annotation.NonNull;

import java.util.HashMap;

import som.hardware.message.action.ActionRequestParameters.ActuatorDirection;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.Request;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;
import som.instruction.request.actuator.Actuators.AnnotationActuator;
import som.instruction.request.heat.HeatPowerLevel;


public class Actuator extends Request {
  /**
   * Set the requester, requestType and instantiates the parameters map.
   *
   * @param instructionHandler The {@link InstructionHandler} which requested this request
   */
  public Actuator(InstructionHandler instructionHandler) {
    this.requester = instructionHandler;
    this.requestType = RequestType.ACTUATOR;
    this.parameters = new HashMap<>();
  }

  public Actuator setActuator(@AnnotationActuator String actuators) {
    this.parameters.put(RequestParameters.ACTUATOR.getRequestParameter(), actuators.toString());
    return this;
  }

  public Actuator setActuatorSpeed(@NonNull Integer speed) {
    this.parameters.put(RequestParameters.ACTUATOR_SPEED.getRequestParameter(), speed.toString());
    return this;
  }

  public Actuator setActuatorFrequency(@NonNull Integer actuatorFreq) {
    this.parameters.put(RequestParameters.ACTUATOR_FREQUENCY.getRequestParameter(),
      actuatorFreq.toString());
    return this;
  }

  public Actuator setActuatorDirection(@NonNull ActuatorDirection actuatorDirection) {
    this.parameters.put(RequestParameters.ACTUATOR_DIRECTION.getRequestParameter(),
      actuatorDirection.getParameter());
    return this;
  }

  public String getActuatorDirection() {
    return this.parameters.get(RequestParameters.ACTUATOR_DIRECTION.getRequestParameter());
  }

  public Actuator setInductionPowerLevel(@NonNull HeatPowerLevel heatPowerLevel) {
    this.parameters.put(RequestParameters.HEAT_POWER_LEVEL.getRequestParameter(),
      heatPowerLevel.getPowerLevel());
    return this;
  }

  /**
   * Accepts an integer, which represents a power level and converts it into one of the enums
   * from {@link HeatPowerLevel}.
   * Quantizes the integer value of power level into the eight power levels starting from
   * 0 to 1400 at a gap of 200 watts(0,200,400...1200,1400),corresponding to {@link HeatPowerLevel}.
   * First, this method finds out the closest integral value from the set
   * S = {0,200,400,600,800,1000,1200,1400}, using the quantization rule :
   * {find the closest integer for the given heat level from the set S,
   * if the given heat level lies midway from two power levels then pick up the smaller power level}
   * then it finds out the enum using the following rule :
   * POWER_LEVEL_X is the enum for X*200 quantized integral value.
   * For example, an integer value of 200 should result in the enum {@link HeatPowerLevel#POWER_LEVEL_ONE}
   * an integer value of 490 should result in the enum {@link HeatPowerLevel#POWER_LEVEL_TWO}
   * <p>
   *
   * @param heatLevel Integral value representing the power level
   * @return Enum from {@link HeatPowerLevel}
   */
  public static HeatPowerLevel createHeatLevel(Integer heatLevel) {
    HeatPowerLevel heatPowerLevel = null;

    Integer quantizedValue = getQuantizedValue(heatLevel);
    quantizedValue = Math.max(0, Math.min(quantizedValue, 1400));
    if (quantizedValue == 0) {
      heatPowerLevel = HeatPowerLevel.POWER_LEVEL_ZERO;
    } else if (quantizedValue == 200) {
      heatPowerLevel = HeatPowerLevel.POWER_LEVEL_ONE;
    } else if (quantizedValue == 400) {
      heatPowerLevel = HeatPowerLevel.POWER_LEVEL_TWO;
    } else if (quantizedValue == 600) {
      heatPowerLevel = HeatPowerLevel.POWER_LEVEL_THREE;
    } else if (quantizedValue == 800) {
      heatPowerLevel = HeatPowerLevel.POWER_LEVEL_FOUR;
    } else if (quantizedValue == 1000) {
      heatPowerLevel = HeatPowerLevel.POWER_LEVEL_FIVE;
    } else if (quantizedValue == 1200) {
      heatPowerLevel = HeatPowerLevel.POWER_LEVEL_SIX;
    } else if (quantizedValue == 1400) {
      heatPowerLevel = HeatPowerLevel.POWER_LEVEL_SEVEN;
    }

    return heatPowerLevel;
  }

  public static Integer getQuantizedValue(Integer heatLevel) {
    if (heatLevel == null) {
      return 0;
    }
    if (heatLevel % 200 == 100) {
      return (int) (200 * (Math.floor(Math.abs((double) heatLevel / 200))));
    }

    return Math.toIntExact(200 * (Math.round((double) heatLevel / 200)));
  }

  public Actuator setActuatorState(Boolean actuatorState){
    this.parameters.put(RequestParameters.ACTUATOR_STATE.getRequestParameter(),
      actuatorState.toString());
    return this;
  }
}
