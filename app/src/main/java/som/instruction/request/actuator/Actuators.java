package som.instruction.request.actuator;

import androidx.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Actuators {

  public static final String EXHAUST_FAN = "EXHAUST_FAN";
  public static final String INDUCTION_POWER = "INDUCTION_POWER";
  public static final String STIRRER = "STIRRER";
  public static final String DESIGN_LED = "DESIGN_LED";
  public static final String BUZZER = "BUZZER";
  private String actuator;

  public Actuators(@AnnotationActuator String actuator) {
    this.actuator = actuator;
  }

  public String getActuator() {
    return actuator;
  }

  @StringDef({EXHAUST_FAN, INDUCTION_POWER, STIRRER, DESIGN_LED, BUZZER})
  @Retention(RetentionPolicy.SOURCE)
  public @interface AnnotationActuator {
  }
}
