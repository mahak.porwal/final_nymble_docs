package som.instruction.request.visionCamera.camera;

import android.graphics.Bitmap;

public interface CameraController {

  Bitmap getStillCapture() throws InterruptedException;

  void setVideoStart();

  void setVideoStop();

  void setVideoPause();

  void setVideoResume();

  boolean isAvailable();

}
