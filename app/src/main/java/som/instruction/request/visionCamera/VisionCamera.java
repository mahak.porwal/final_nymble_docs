package som.instruction.request.visionCamera;

import androidx.annotation.NonNull;

import java.util.HashMap;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.Request;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;

/**
 * Vision camera request from the instruction handler.
 */
public class VisionCamera extends Request {

  /**
   * Set the requester, requestType and instantiates the parameters map.
   *
   * @param instructionHandler The {@link InstructionHandler} which requested this request
   */
  public VisionCamera(InstructionHandler instructionHandler) {
    this.requester = instructionHandler;
    this.requestType = RequestType.VISION_CAMERA;
    this.parameters = new HashMap<>();
  }

  public void setCameraCommand(@NonNull VisionCameraCommand visionCameraCommand) {
    this.parameters.put(RequestParameters.VISUAL_CAMERA_COMMAND.getRequestParameter(),
        visionCameraCommand.getVisionCameraCommand());
  }
}
