package som.instruction.request.visionCamera;

import androidx.annotation.Nullable;

/**
 * Possible commands related to a vision camera.
 */
public enum VisionCameraCommand {
  STILL_CAPTURE("Still_Capture"),
  VIDEO_START("Video_Start"),
  VIDEO_STOP("Video_Stop"),
  VIDEO_PAUSE("Video_Pause"),
  VIDEO_RESUME("Video_Resume");

  private final String visionCameraCommand;

  /**
   * Sets the {@link VisionCameraCommand#visionCameraCommand}.
   *
   * @param command String value of the enum
   */
  VisionCameraCommand(String command) {
    this.visionCameraCommand = command;
  }

  public String getVisionCameraCommand() {
    return this.visionCameraCommand;
  }

  /**
   * Returns Vision Camera command which is same as the function parameter.
   * @param string VisionCameraCommand
   * @return: VisionCameraCommand
   */
  public static @Nullable
      VisionCameraCommand fromString(String string) {
    for (VisionCameraCommand b : VisionCameraCommand.values()) {
      if (b.visionCameraCommand.equals(string)) {
        return b;
      }
    }
    return null;
  }
}
