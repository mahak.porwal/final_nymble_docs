package som.instruction.request.visionCamera.camera;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import com.example.recipeengine.MainActivity;

import com.example.recipeengine.util.FileUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import timber.log.Timber;

/**
 * This class provides the methods to capture an image or
 * record a video.
 */
public final class CameraV8_3 implements CameraController {

  private static final int MAX_INIT_CAMERA_ATTEMPTS = 2;
  private static CameraController cameraController;
  private CameraExecutor cameraExecutor;
  private Bitmap visionImage;
  private CountDownLatch countDownLatch;
  private int count;
  private final AtomicInteger cameraInitAttempts;
  private final AtomicBoolean isAvailable;

  private CameraV8_3() throws InterruptedException {
    this.cameraExecutor = new CameraExecutor(MainActivity.getAppContext());
    this.cameraInitAttempts = new AtomicInteger(0);
    this.isAvailable = new AtomicBoolean(true);
    initCamera();

  }

  private void initCamera() throws InterruptedException {
    count = 0;
    this.cameraExecutor.setBitmapAvailableListener(v -> {
      Timber.d("Image available----------------------------------------");
      setVisionImage(v);
      //TODO : Call in new thread if required.
      //this.notify();
      this.countDownLatch.countDown();
    });
    this.cameraExecutor.setDeviceFailureListener(() -> {
      // Check for max attempts to re-initialize the controller.
      // Practically this shouldn't happen unless it's actual Camera hardware issue.
      if (MAX_INIT_CAMERA_ATTEMPTS <= this.cameraInitAttempts.getAndIncrement()) {
        Timber.e("Maximum initialization attempts reached. Camera Hardware failure.");
        //VisionController.this.uiSubject.onNext(Failures.FAILURE_CAMERA_DISCONNECTION);
        this.isAvailable.set(false);
        return;
      }
      Timber.i("Reinitializing Camera controller...");
      if (this.cameraExecutor.isOpened()) {
        this.cameraExecutor.close();
      }
      this.cameraExecutor = new CameraExecutor(MainActivity.getAppContext());
      Timber.i("Camera controller reinitialized after failure");
    });
    this.cameraExecutor.openCamera();
  }

  public static CameraController getInstance() throws InterruptedException {
    if (null == cameraController) {
      createInstance();
    }
    return cameraController;
  }

  public static void createInstance() throws InterruptedException {
    if (null == cameraController) {
      cameraController = new CameraV8_3();
    }
  }

  /**
   * Uses the {@link CameraExecutor#captureImage()} to capture
   * an image. It waits on a countdown latch shared with the bitmap listener for atmost 10 seconds.
   * And then returns. If the timeout occurred, then the visionImage will be null.
   *
   * @return Bitmap captured image.
   */
  @Override
  public synchronized Bitmap getStillCapture() throws InterruptedException {
    setVisionImage(null);
    this.countDownLatch = new CountDownLatch(1);
    this.cameraExecutor.captureImage();
    this.countDownLatch.await(10, TimeUnit.SECONDS);
    this.saveImage(this.visionImage,"image_"+String.valueOf(count++)+".jpeg");
    return this.visionImage;
  }

  private void saveImage(@NonNull Bitmap image, @NonNull String filename) {
    final File imageFile = FileUtil.getAsFile("/camera_test2/", filename);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename);
    }
  }

  @Override
  public void setVideoStart() {

  }

  @Override
  public void setVideoStop() {

  }

  @Override
  public void setVideoPause() {

  }

  @Override
  public void setVideoResume() {

  }

  @Override
  public boolean isAvailable() {
    return this.isAvailable.get();
  }

  public void setVisionImage(Bitmap visionImage) {
    this.visionImage = visionImage;
  }
}
