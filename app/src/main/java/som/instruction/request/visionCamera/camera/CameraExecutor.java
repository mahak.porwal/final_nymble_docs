package som.instruction.request.visionCamera.camera;

import static android.hardware.camera2.CaptureRequest.JPEG_ORIENTATION;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Size;
import android.view.Surface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.recipeengine.util.FileUtil;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

import timber.log.Timber;

public class CameraExecutor implements Closeable {

  private static final int CAMERA_LOCK_TIMEOUT_MS = 2500;

  private static final int VIDEO_FRAME_RATE = 30;
  private static final int VIDEO_ENCODING_BIT_RATE = 8000000;

  private static final int WIDTH = 1920;
  private static final int HEIGHT = 1080;

  private final Context context;
  private final Semaphore cameraOpenCloseLock = new Semaphore(1);
  private final AtomicBoolean isOpened = new AtomicBoolean(false);

  private BitmapAvailableListener bitmapAvailableListener;
  private DeviceFailureListener deviceFailureListener;

  private String cameraId;
  private CameraDevice cameraDevice;

  private HandlerThread cameraThread;
  private Handler cameraHandler;
  private ImageReader imageReader;
  private ImageReader previewReader;

  private CameraCaptureSession previewCaptureSession;
  private CameraCaptureSession recordCaptureSession;
  private MediaRecorder mMediaRecorder;

  private int videoSessionNumber;

  private final CountDownLatch countDownLatch;

  @Inject
  public CameraExecutor(@NonNull final Context context) {
    this.context = context;
    this.countDownLatch = new CountDownLatch(1);
  }

  /**
   * Set a callback to listen to bitmap images.
   *
   * @param listener the bitmap available listener
   */
  public void setBitmapAvailableListener(@NonNull final BitmapAvailableListener listener) {
    this.bitmapAvailableListener = listener;
  }

  /**
   * Set a listener for camera device failure
   *
   * @param listener the device failure listener
   */
  public void setDeviceFailureListener(@Nullable final DeviceFailureListener listener) {
    this.deviceFailureListener = listener;
  }

  /**
   * State callback of Camera
   */
  private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {

    @Override
    public void onOpened(@NonNull CameraDevice device) {
      Timber.i("Camera opened");
      CameraExecutor.this.isOpened.set(true);
      CameraExecutor.this.cameraOpenCloseLock.release();
      CameraExecutor.this.cameraDevice = device;
      createCameraSession();
    }

    /**
     * <p>The disconnection could be due to a
     * change in security policy or permissions; the physical disconnection
     * of a removable camera device; or the camera being needed for a
     * higher-priority camera API client.</p>
     *
     * Call the {@link DeviceFailureListener#onCameraFail()} callback
     * to reinitialize a new controller.
     */
    @Override
    public void onDisconnected(@NonNull CameraDevice device) {
      Timber.e("Camera disconnected");
      CameraExecutor.this.cameraOpenCloseLock.release();
      closeCamera();
      onCameraFailCallback();
    }

    /**
     * Unrecoverable error.
     *
     * Call the {@link DeviceFailureListener#onCameraFail()} callback
     * to reinitialize a new controller.
     */
    @Override
    public void onError(@NonNull CameraDevice device, int code) {
      Timber.e("Camera device error %d", code);
      CameraExecutor.this.cameraOpenCloseLock.release();
      closeCamera();
      onCameraFailCallback();
    }

    @Override
    public void onClosed(CameraDevice camera) {
      Timber.i("Camera closed");
      CameraExecutor.this.isOpened.set(false);
      CameraExecutor.this.cameraDevice = null;
    }
  };

  private final CameraCaptureSession.CaptureCallback captureCallback =
    new CameraCaptureSession.CaptureCallback() {
      @Override
      public void onCaptureStarted(@NonNull CameraCaptureSession session,
                                   @NonNull CaptureRequest request, long timestamp,
                                   long frameNumber) {
        super.onCaptureStarted(session, request, timestamp, frameNumber);
      }

      @Override
      public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                     @NonNull CaptureRequest request,
                                     @NonNull TotalCaptureResult result) {
        super.onCaptureCompleted(session, request, result);
        Timber.i("Camera image capture done");
      }

      @Override
      public void onCaptureFailed(@NonNull CameraCaptureSession session,
                                  @NonNull CaptureRequest request,
                                  @NonNull CaptureFailure failure) {
        super.onCaptureFailed(session, request, failure);
        Timber.e("Camera capture failure reason: %s, Image captured? %b, Frame: %d",
          failure.getReason(), failure.wasImageCaptured(), failure.getFrameNumber());
        closeCamera();
        onCameraFailCallback();
      }

      @Override
      public void onCaptureBufferLost(@NonNull CameraCaptureSession session,
                                      @NonNull CaptureRequest request, @NonNull Surface target,
                                      long frameNumber) {
        super.onCaptureBufferLost(session, request, target, frameNumber);
        Timber.e("Camera capture buffer lost");
      }
    };

  private final CameraManager.AvailabilityCallback availabilityCallback =
    new CameraManager.AvailabilityCallback() {
      @Override
      public void onCameraAvailable(@NonNull String cameraId) {
        super.onCameraAvailable(cameraId);
        Timber.i("Camera available for use: %s", cameraId);
        if (cameraId.contentEquals(CameraExecutor.this.cameraId) && !isOpened()) {
          try {
            openCamera();
          } catch (InterruptedException e) {
            Timber.e(e);
          }
        }
      }

      @Override
      public void onCameraUnavailable(@NonNull String cameraId) {
        super.onCameraUnavailable(cameraId);
        Timber.i("Camera '%s' is in use", cameraId);
      }
    };

  protected void onCameraFailCallback() {
    if (null != CameraExecutor.this.deviceFailureListener) {
      CameraExecutor.this.deviceFailureListener.onCameraFail();
    }
  }

  /**
   * Open the camera
   */
  @SuppressLint("MissingPermission")
  public boolean openCamera() throws InterruptedException {
    if (isOpened()) {
      Timber.i("Camera is already open");
      return false;
    }
    final CameraManager manager =
      (CameraManager) this.context.getSystemService(Context.CAMERA_SERVICE);
    try {
      if (!this.cameraOpenCloseLock.tryAcquire(CAMERA_LOCK_TIMEOUT_MS, TimeUnit.MILLISECONDS)) {
        throw new RuntimeException("Time out waiting to lock camera opening.");
      }
      startBackgroundThread();
      setupCameraOutputs();
      manager.openCamera(this.cameraId, this.stateCallback, this.cameraHandler);
      manager.registerAvailabilityCallback(this.availabilityCallback, this.cameraHandler);
    } catch (CameraAccessException e) {
      Timber.e(e, "Failed to open Camera");
      onCameraFailCallback();
    } catch (InterruptedException e) {
      Timber.e(e, "Interrupted while trying to lock camera opening.");
      onCameraFailCallback();
    }
    return this.countDownLatch.await(10, TimeUnit.SECONDS);
  }

  /**
   * Create a camera capture session
   */
  private void createCameraSession() {
    try {
      final List<Surface> surfaces = Arrays.asList(this.previewReader.getSurface(),
        this.imageReader.getSurface());
      this.cameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(CameraCaptureSession session) {
          Timber.i("Capture session configured");
          CameraExecutor.this.previewCaptureSession = session;
          _createCameraPreview();
        }

        @Override
        public void onConfigureFailed(CameraCaptureSession session) {
          Timber.e("Capture session configuration failed");
        }
      }, null);
    } catch (CameraAccessException e) {
      Timber.e(e, "Error while creating camera session");
    }
  }

  /**
   * Background camera preview to improve gains and lighting conditions.
   */
  private void _createCameraPreview() {
    try {
      final CaptureRequest.Builder previewSession =
        this.cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
      previewSession.addTarget(this.previewReader.getSurface());
      previewSession.set(JPEG_ORIENTATION, 0);
      this.previewCaptureSession.setRepeatingRequest(previewSession.build(),
        null, this.cameraHandler);
    } catch (CameraAccessException e) {
      Timber.e(e, "Error on camera preview request");
    }
    this.countDownLatch.countDown();
  }

  /**
   * Capture an image
   */
  public void captureImage() {
    if (null == this.cameraDevice) {
      Timber.d("camera device null");
      return;
    }
    if (!isOpened()) {
      return;
    }
    Timber.d("Capture request received");
    try {
      final CaptureRequest.Builder captureRequest =
        this.cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
      captureRequest.addTarget(this.imageReader.getSurface());
      captureRequest.set(JPEG_ORIENTATION, 0);

      if (null != this.previewCaptureSession && isOpened()) {
        this.previewCaptureSession.capture(captureRequest.build(),
          this.captureCallback, this.cameraHandler);
      }
    } catch (CameraAccessException e) {
      Timber.e(e, "Cannot access camera while capture image request");
    } catch (Exception e) {
      Timber.e(e, "Error capturing image");
    }
  }

  /**
   * Setup camera output to the {@code imageReader}
   */
  private void setupCameraOutputs() {
    final CameraManager manager =
      (CameraManager) this.context.getSystemService(Context.CAMERA_SERVICE);
    try {
      for (String cameraId : manager.getCameraIdList()) {
        final CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
        // Not using front camera
        if (null == characteristics.get(CameraCharacteristics.LENS_FACING)) {
          continue;
        }

        final Size[] sizes =
          characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
            .getOutputSizes(ImageFormat.JPEG);
        for (Size size : sizes) {
          Timber.d("Available sizes = %d, %d", size.getHeight(), size.getWidth());
        }

        Timber.i("Image size: %dx%d", WIDTH, HEIGHT);
        this.imageReader = ImageReader.newInstance(WIDTH, HEIGHT, ImageFormat.JPEG, 2);
        this.imageReader
          .setOnImageAvailableListener(this.imageAvailableListener, this.cameraHandler);

        this.previewReader = ImageReader.newInstance(WIDTH, HEIGHT, ImageFormat.JPEG, 1);
        this.previewReader.setOnImageAvailableListener(reader -> {
          final Image image = reader.acquireLatestImage();
          if (null == image) {
            return;
          }
          image.close();
        }, this.cameraHandler);

        this.cameraId = cameraId;
        Timber.i("Using Camera ID: %s", cameraId);
        return;
      }
    } catch (CameraAccessException e) {
      Timber.e("Failed to access Camera");
    } catch (NullPointerException e) {
      Timber.e(e, "Device doesn't support Camera2 API");
    }
  }

  /**
   * Start the camera thread
   */
  private void startBackgroundThread() {
    this.cameraThread = new HandlerThread("CameraThread");
    this.cameraThread.start();
    this.cameraHandler = new Handler(this.cameraThread.getLooper());
  }

  /**
   * Stop the camera thread
   */
  private void stopBackgroundThread() {
    if (null != this.cameraHandler) {
      this.cameraHandler.removeCallbacksAndMessages(null);
      this.cameraThread.quit();
      try {
        this.cameraThread.join(1000);
        this.cameraThread = null;
        this.cameraHandler = null;
      } catch (InterruptedException e) {
        Timber.e(e);
      }
    }
  }

  /**
   * Callback on {@code imageReader}
   */
  private final ImageReader.OnImageAvailableListener imageAvailableListener = imageReader -> {
    final Image image = imageReader.acquireLatestImage();
    if (null == image) {
      return;
    }
    Image.Plane[] planes = image.getPlanes();
    ByteBuffer buffer = planes[0].getBuffer();
    byte[] bytes = new byte[buffer.capacity()];
    buffer.get(bytes);

    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    //Bitmap rotated = rotateImage(bitmap);
    this.bitmapAvailableListener.onBitmapAvailable(bitmap);
    image.close();
  };

  // START -  All Video related code

  /**
   * Start video capture
   */
  public void startVideoCapture() {
    this.videoSessionNumber++;
    this.mMediaRecorder = new MediaRecorder();
    this.startRecord();
    this.mMediaRecorder.start();
  }

  /**
   * Stop video capture
   */
  public void stopVideoCapture() {
    if (null != this.mMediaRecorder) {
      this.mMediaRecorder.stop();
      this.mMediaRecorder.reset();
    }
  }

  /**
   * Start recording the video
   */
  private void startRecord() {
    try {
      setupMediaRecorder();
      final CaptureRequest.Builder captureRequest =
        this.cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
      captureRequest.addTarget(this.mMediaRecorder.getSurface());

      final List<Surface> surfaces = Arrays.asList(this.mMediaRecorder.getSurface(),
        this.imageReader.getSurface());
      this.cameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(CameraCaptureSession session) {
          CameraExecutor.this.recordCaptureSession = session;
          try {
            CameraExecutor.this.recordCaptureSession.setRepeatingRequest(captureRequest.build(),
              null, null);
          } catch (CameraAccessException e) {
            Timber.e(e, "Error while creating video repeating request");
          }
        }

        @Override
        public void onConfigureFailed(CameraCaptureSession session) {
          Timber.e("Video recording configuration failed");
        }
      }, null);

    } catch (Exception e) {
      Timber.e(e, "Error while creating video record session");
    }
  }

  /**
   * Setup the mediaRecorder video settings
   */
  private void setupMediaRecorder() throws IOException {
    this.mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
    this.mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
    this.mMediaRecorder.setOutputFile(
      FileUtil.getVideoSessionFile(String.format("video_%d.mp4", this.videoSessionNumber)));
    this.mMediaRecorder.setVideoEncodingBitRate(VIDEO_ENCODING_BIT_RATE);
    this.mMediaRecorder.setVideoFrameRate(VIDEO_FRAME_RATE);
    Timber.d("Video Size %d %d", WIDTH, HEIGHT);
    this.mMediaRecorder.setVideoSize(WIDTH, HEIGHT);
    this.mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
    this.mMediaRecorder.prepare();
  }

  // END - All video related code

  /**
   * Check if CameraExecution is opened successfully,
   * and is ready to capture images.
   *
   * @return true, if controller is open
   */
  public boolean isOpened() {
    return this.isOpened.get();
  }

  /**
   * Close camera and all related sessions.
   * Use this to close the camera if it must be reopened.
   */
  private void closeCamera() {
    try {
      this.cameraOpenCloseLock.acquire();
      if (null != this.previewCaptureSession) {
        this.previewCaptureSession.close();
        this.previewCaptureSession = null;
      }
      if (null != this.recordCaptureSession) {
        this.recordCaptureSession.close();
        this.recordCaptureSession = null;
      }
      if (null != this.imageReader) {
        this.imageReader.close();
        this.imageReader = null;
      }
      if (null != this.previewReader) {
        this.previewReader.close();
        this.previewReader = null;
      }
      Timber.i("Camera components closed");
    } catch (InterruptedException e) {
      Timber.e(e, "Unable to acquire lock for closing camera");
    } finally {
      stopBackgroundThread();
      this.cameraOpenCloseLock.release();
      this.isOpened.set(false);
    }
  }

  /**
   * Close the controller completely disposing it's resources.
   */
  @Override
  public void close() {
    this.closeCamera();
    final CameraManager manager =
      (CameraManager) this.context.getSystemService(Context.CAMERA_SERVICE);
    manager.unregisterAvailabilityCallback(this.availabilityCallback);
    if (null != this.cameraDevice) {
      this.cameraDevice.close();
      this.cameraDevice = null;
    }
  }

  /**
   * Image listener interface
   */
  public interface BitmapAvailableListener {
    void onBitmapAvailable(Bitmap bitmap);
  }

  /**
   * Camera device failure listener
   */
  public interface DeviceFailureListener {

    /**
     * Unrecoverable.
     * A new {@link CameraExecutor} must be created and initialized.
     */
    void onCameraFail();
  }

}
