package som.instruction.request.dispense;


public enum Ingredient {
  POTATO("Potato"),
  ONION("Onion"),
  MILK("Milk");

  private final String ingredient;

  Ingredient(String ingredient) {
    this.ingredient = ingredient;
  }

  public String getIngredient() {
    return this.ingredient;
  }
}
