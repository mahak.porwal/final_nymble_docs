package som.instruction.request.dispense;

import androidx.annotation.NonNull;

import java.util.HashMap;

import som.hardware.message.action.ActionType;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.Request;
import som.instruction.request.RequestParameters;
import som.instruction.request.RequestType;

/**
 * Dispense request from the instruction handler.
 */
public class Dispense extends Request {

  /**
   * Sets the requester, requestType and instantiates parameters map.
   *
   * @param instructionHandler The which {@link InstructionHandler}, which requested dispense
   *                           request
   */
  public Dispense(InstructionHandler instructionHandler) {
    this.requester = instructionHandler;
    this.requestType = RequestType.DISPENSE;
    this.parameters = new HashMap<>();
  }

  public void setIngredient(@NonNull Ingredient ingredient) {
    this.parameters.put(RequestParameters.INGREDIENT.getRequestParameter(),
      ingredient.getIngredient());
  }

  public void setQuantity(@NonNull Double quantity) {
    this.parameters.put(RequestParameters.QUANTITY.getRequestParameter(),
      quantity.toString());
  }

  public Dispense setContainer(DispenseContainer dispenseContainer) {
    this.parameters.put(RequestParameters.DISPENSE_CONTAINER.getRequestParameter(),
      dispenseContainer.toString());
    return this;
  }

  public Dispense setDispenseBehavior(ActionType actionType) {
    this.parameters.put(RequestParameters.DISPENSE_BEHAVIOR.getRequestParameter(),
      actionType.getActionType());
    return this;
  }

}
