package som.hardware.response;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Response for the {@link som.hardware.request.CameraRequest}.
 */
public class CameraResponse extends Response {

  private Bitmap image;

  public @Nullable
      Bitmap getImage() {
    return this.image;
  }

  public void setImage(@NonNull Bitmap image) {
    this.image = image;
  }
}
