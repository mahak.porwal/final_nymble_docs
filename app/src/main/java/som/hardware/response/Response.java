package som.hardware.response;

public abstract class Response {
  protected int timeTakenMS;
  protected boolean success;

  public int getTimeTakenMS() {
    return this.timeTakenMS;
  }

  public boolean isSuccess() {
    return this.success;
  }
}
