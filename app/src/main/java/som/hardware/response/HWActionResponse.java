package som.hardware.response;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.Map;


/**
 * Response for {@link som.hardware.message.action.ActionRequest}.
 */
public class HWActionResponse extends Response {

  private final Map<String, String> actionResponseData;

  /**
   * Sets the timeTakenMS, success boolean and the response map.
   *
   * @param timeTakenMS time taken to complete the request
   * @param success     whether the request was successful or not
   * @param response    action response
   */
  public HWActionResponse(int timeTakenMS, boolean success, @Nullable Map<String,
      String> response) {
    this.timeTakenMS = timeTakenMS;
    this.success = success;
    this.actionResponseData = response;
  }

  public @Nullable
      Map<String, String> getActionResponseData() {
    return Collections.unmodifiableMap(this.actionResponseData);
  }
}
