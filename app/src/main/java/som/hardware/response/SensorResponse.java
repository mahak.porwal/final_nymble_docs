package som.hardware.response;

import androidx.annotation.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Response for {@link som.hardware.request.SensorRequest}.
 */
public class SensorResponse extends Response {

  private final Map<String, List<Double>> sensorData;

  public @Nullable
  Map<String, List<Double>> getSensorData() {
    return Collections.unmodifiableMap(this.sensorData);
  }

  /**
   * Sets the timeTakenMS, success boolean and the response map.
   *
   * @param timeTakenMS time taken to complete the request
   * @param success     whether the request was successful or not
   * @param response    sensor reading
   */
  public SensorResponse(int timeTakenMS, boolean success,
                        @Nullable Map<String, List<Double>> response) {
    this.timeTakenMS = timeTakenMS;
    this.success = success;
    this.sensorData = response;
  }
}
