package som.hardware.service;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import com.example.recipeengine.util.Native.NativeUtil;
import com.example.recipeengine.util.ValueSaver;
import com.example.recipeengine.util.ValueSaverImpl_Factory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Provider;
import javax.inject.Singleton;

import som.hardware.request.CameraRequest;
import som.hardware.request.HWActionRequest;
import som.hardware.request.HWRequest;
import som.hardware.request.SensorRequest;
import som.hardware.response.CameraResponse;
import som.hardware.response.HWActionResponse;
import som.hardware.response.Response;
import som.hardware.response.SensorResponse;
import som.instruction.request.visionCamera.VisionCameraCommand;
import som.instruction.request.visionCamera.camera.CameraController;
import som.instruction.request.visionCamera.camera.CameraV8_3;
import timber.log.Timber;

/**
 * Processes {@link HWRequest}.
 */
@Singleton
public class HWService {
  private static final Map<String, String> hardwareFactors = new HashMap<>();
  private static volatile boolean isStarted;

  public static void startHwService(Context context) throws InterruptedException {
    if (!isStarted) {
      scanAndSetHardwareFactors(context);
      NativeUtil.StartHardwareService();
      //CameraV8_3.createInstance();
      //StartInductionControl();
      //NativeUtil.InitThermalCamera();
      isStarted = true;
    }
  }

  private static void scanAndSetHardwareFactors(Context context) {
    //scan hardware factors from shared preferences
    final ValueSaver valueSaver = ValueSaverImpl_Factory.newValueSaverImpl(context);
    hardwareFactors.clear();
    for (String key : valueSaver.getKeySet()) {
      hardwareFactors.put(key, valueSaver.getValue(key));
    }
    NativeUtil.populateHardwareFactors(hardwareFactors);
  }

  private static void StartInductionControl() {
    Observable.just(1).observeOn(Schedulers.newThread()).subscribe(l -> {
      Thread.currentThread().setName("Induction control thread");
      NativeUtil.StartInductionControl();
    });/*
    Observable.just(1)
      .delay(10000, TimeUnit.MILLISECONDS)
      .doOnNext(l->Timber.d("Stop Induction"))
      .subscribe(l->{NativeUtil.StopInductionControl();
      NativeUtil.StopHardwareService();});*/
  }

  @VisibleForTesting
  CameraResponse executeCameraRequest(CameraRequest cameraRequest) throws Exception {
    //TODO : camera response
    return getCameraResponse(cameraRequest);
  }

  private CameraResponse getCameraResponse(CameraRequest cameraRequest) throws Exception {
    CameraController cameraController = null;
    try {
      cameraController = CameraV8_3.getInstance();
    } catch (InterruptedException e) {
      Timber.e(e);
    }
    final CameraResponse cameraResponse = new CameraResponse();
    if (VisionCameraCommand.STILL_CAPTURE == cameraRequest.getVisionCameraCommand()) {
      if (null != cameraController) {
        final Bitmap image = cameraController.getStillCapture();
        if (null == image) {
          if (cameraController.isAvailable()) {
            return this.getCameraResponse(cameraRequest);
          }
          throw new Exception("Camera hardware failure");
        } else {
          cameraResponse.setImage(image);
        }
      }
    }
    return cameraResponse;
  }

  @VisibleForTesting
  SensorResponse executeSensorRequest(SensorRequest sensorRequest) {
    //native call
    return NativeUtil.ExecuteSensorRequest(sensorRequest);
  }

  @VisibleForTesting
  HWActionResponse executeHWActionRequest(HWActionRequest hwActionRequest) {
    //native call
    return NativeUtil.ExecuteHWActionRequest(hwActionRequest);
  }

  /**
   * Delegates the execution of the {@link HWRequest} appropriate executor.
   *
   * @param hwRequest {@link HWRequest}
   * @return Response
   */
  public Response executeRequest(@NonNull HWRequest hwRequest) throws Exception {
    Response response = null;
    switch (hwRequest.getHwRequestType()) {
      case CAMERA_REQUEST:
        response = this.executeCameraRequest((CameraRequest) hwRequest);
        break;
      case SENSOR_REQUEST:
        response = this.executeSensorRequest((SensorRequest) hwRequest);
        break;
      case HW_ACTION_REQUEST:
        response = this.executeHWActionRequest((HWActionRequest) hwRequest);
        break;
      default:
        throw new IllegalStateException("Unexpected value: "
          + hwRequest.getHwRequestType());
    }
    return response;
  }
}
