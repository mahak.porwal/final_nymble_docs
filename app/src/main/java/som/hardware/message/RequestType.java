package som.hardware.message;

public enum RequestType {
  COMMAND_REQUEST,
  ACTION_REQUEST,
  SENSOR_REQUEST;

  public String getRequestType() {
    return this.name();
  }
}
