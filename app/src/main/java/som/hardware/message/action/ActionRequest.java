package som.hardware.message.action;

import androidx.annotation.NonNull;

import som.hardware.message.Component;
import som.hardware.message.HWMessage;
import som.hardware.message.HWMessageKeys;
import som.hardware.message.RequestType;

/**
 * A sub-class of HWMessage, containing the request parameters for the Action type.
 */
public class ActionRequest extends HWMessage {
  /**
   * Sets the entry in the parametersMap for {@link RequestType},
   * {@link Component} and {@link ActionType}. {@link RequestType} takes the value of
   * {@link RequestType#ACTION_REQUEST}.
   *
   * @param component  {@link Component} for which the request is
   * @param actionType {@link ActionType}
   */
  public ActionRequest(@NonNull Component component, @NonNull ActionType actionType) {
    this.requestType = RequestType.ACTION_REQUEST;
    this.setEntryParametersMap(HWMessageKeys.RequestType, this.requestType.getRequestType());
    this.setEntryParametersMap(ActionRequestParameters.BEHAVIOR_TYPE.getParameter(),
        actionType.getActionType());
    this.setEntryParametersMap(HWMessageKeys.Component, component.getComponent());
  }

  /**
   * Sets the entry in the parametersMap.
   * The key being the enum of type {@link ActionRequestParameters}
   * The value being a {@link String}
   *
   * @param requestParameter Key for the parametersMap
   * @param value            {@link String} value corresponding to the requestParameter
   */
  public void setActionParameters(@NonNull ActionRequestParameters requestParameter,
                                  @NonNull String value) {
    this.setEntryParametersMap(requestParameter.getParameter(), value);
  }
}
