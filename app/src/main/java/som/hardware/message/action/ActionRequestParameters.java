package som.hardware.message.action;

public enum ActionRequestParameters {
  SPEED,
  DIRECTION,
  ANGLE,
  HARDWARE_UNITS,
  BEHAVIOR_TYPE;

  public String getParameter() {
    return this.name();
  }

  public enum ActuatorDirection {
    DIRECTION_CLOCKWISE,
    DIRECTION_ANTICLOCKWISE;

    public String getParameter() {
      return this.name();
    }
  }
}
