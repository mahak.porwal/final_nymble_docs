package som.hardware.message;

public final class HWMessageKeys {
  public static final String Component = "COMPONENT";
  public static final String RequestType = "REQUEST_TYPE";

  private HWMessageKeys() {
  }
}
