package som.hardware.message.sensor;

public enum SensorUnit {
  ANGLE_VALUE,
  RAW_VALUE,
  MICRO_VOLTS,
  MILLI_VOLTS,
  MILLI_AMPERES,
  GRAMS;

  public String getSensorUnit() {
    return this.name();
  }
}
