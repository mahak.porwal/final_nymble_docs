package som.hardware.message.sensor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import som.hardware.message.Component;
import som.hardware.message.HWMessage;
import som.hardware.message.HWMessageKeys;
import som.hardware.message.RequestType;

/**
 * Sensor request for the hardware.
 */
public class HWSensorRequest extends HWMessage {

  /**
   * Creates sensor request for the Hardware.
   * Sets the {@link SensorRequestParameters#SENSOR_NAME} in the {@link HWMessage#parametersMap}
   * and if the sensorUnit is non-null, then sets the {@link SensorRequestParameters#SENSOR_UNIT}
   * in the parametersMap.
   *
   * @param component  component to sense
   * @param sensorUnit unit of the sensor reading, can be left as null value, in this case the HW
   *                   library will give sensor reading in default units
   */
  public HWSensorRequest(@NonNull Component component, @Nullable SensorUnit sensorUnit) {

    this.setEntryParametersMap(HWMessageKeys.RequestType,
      RequestType.SENSOR_REQUEST.getRequestType());

    this.setEntryParametersMap(SensorRequestParameters.SENSOR_NAME.getSensorRequestParameter(),
      component.getComponent());

    if (null != sensorUnit) {
      this.setEntryParametersMap(SensorRequestParameters.SENSOR_UNIT
        .getSensorRequestParameter(), sensorUnit.getSensorUnit());
    }
  }

  public HWSensorRequest setParameters(@NonNull final SensorRequestParameters sensorRequestParameters,
                                       @NonNull final String parameterValue) {
    this.setEntryParametersMap(sensorRequestParameters.getSensorRequestParameter(),
      parameterValue);
    return this;
  }
}
