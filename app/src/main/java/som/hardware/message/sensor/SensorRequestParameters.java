package som.hardware.message.sensor;

public enum SensorRequestParameters {
  SENSOR_NAME,
  SENSOR_UNIT,
  COUNTER_REQ;

  public String getSensorRequestParameter() {
    return this.name();
  }
}
