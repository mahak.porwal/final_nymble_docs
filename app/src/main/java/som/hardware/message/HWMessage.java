package som.hardware.message;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import som.hardware.message.action.ActionRequestParameters;
import som.hardware.message.command.CommandRequestParameters;

/**
 * This class acts as a base class for ActionRequest and CommandRequest.
 * This class is declared as abstract because there are only two types of Hardware Message :
 * ActionRequest message and CommandRequest message,
 * therefore an instance of HWMessage does not make sense.
 */
public abstract class HWMessage {

  private Map<String, String> parametersMap;

  protected RequestType requestType;

  /**
   * Sets the entry in the parametersMap.
   *
   * @param key   The key for @parametersMap can be taken from {@link HWMessageKeys},
   *              {@link ActionRequestParameters}
   *              or {@link CommandRequestParameters}
   * @param value The value for the param key
   */
  protected void setEntryParametersMap(final String key, final String value) {
    if (null == this.parametersMap) {
      this.parametersMap = new HashMap<>();
    }
    this.parametersMap.put(key, value);
  }

  /**
   * Returns a read-only instance of @parametersMap.
   *
   * @return Map
   */
  public Map<String, String> getParametersMap() {
    return Collections.unmodifiableMap(this.parametersMap);
  }

  public RequestType getRequestType() {
    return this.requestType;
  }
}
