package som.hardware.message.command;

import androidx.annotation.NonNull;

import som.hardware.message.Component;
import som.hardware.message.HWMessage;
import som.hardware.message.HWMessageKeys;
import som.hardware.message.RequestType;

/**
 * Sets the request parameters for a given component.
 * These parameters pertains to the Command type of request and
 * can only be a type of {@link CommandRequestParameters}.
 * These parameters can be set through the method setParameters.
 */
public class CommandRequest extends HWMessage {
  /**
   * Sets the requestType, and puts the string entries in the parameters map.
   *
   * @param component Component for which the request is created
   */
  public CommandRequest(@NonNull Component component) {
    this.requestType = RequestType.COMMAND_REQUEST;
    this.setEntryParametersMap(HWMessageKeys.RequestType, this.requestType.getRequestType());
    this.setEntryParametersMap(HWMessageKeys.Component, component.getComponent());
  }

  /**
   * Sets the entry in the parameter map.
   * requestParameter is the key and value as the map value.
   *
   * @param requestParameter Type of {@link CommandRequestParameters}
   * @param value            String value corresponding to the requestParameter
   */
  public void setParameters(@NonNull CommandRequestParameters requestParameter,
                            @NonNull String value) {
    this.setEntryParametersMap(requestParameter.getParameter(), value);
  }
}
