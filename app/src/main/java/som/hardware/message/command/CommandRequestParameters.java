package som.hardware.message.command;

public enum CommandRequestParameters {
  ANGLE,
  SPEED,
  DIRECTION,
  COMPONENT_ON_OFF_STATE,
  MUX_SELECTION,
  INDUCTION_CONTROL_INPUT,
  INDUCTION_MACHINE_REQ,
  DIGITAL_POT_REQ;

  public String getParameter() {
    return this.name();
  }
}
