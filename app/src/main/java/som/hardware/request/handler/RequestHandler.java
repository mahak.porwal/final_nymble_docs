package som.hardware.request.handler;

import androidx.annotation.VisibleForTesting;

import javax.inject.Singleton;

import som.hardware.request.HWRequest;
import som.hardware.request.builder.CameraRequestBuilder;
import som.hardware.request.builder.SensorRequestBuilder;
import som.hardware.request.builder.hwActionRequest.HWActionRequestBuilder;
import som.hardware.response.Response;
import som.hardware.service.HWService;
import som.instruction.request.Request;

/**
 * Handles {@link Request} of types {@link som.instruction.request.RequestType}.
 */
@Singleton
public class RequestHandler {

  HWActionRequestBuilder hwActionRequestBuilder;
  SensorRequestBuilder sensorRequestBuilder;
  CameraRequestBuilder cameraRequestBuilder;
  HWService hwService;

  //changed to public
  public RequestHandler() {
    this.hwService = new HWService();
    this.hwActionRequestBuilder = new HWActionRequestBuilder();
    this.sensorRequestBuilder = new SensorRequestBuilder();
    this.cameraRequestBuilder = new CameraRequestBuilder();
  }

  /**
   * Processes request by first creating the {@link HWRequest} using the request builders.
   * And then delegates the task of executing the {@link HWRequest} to the {@link HWService}.
   *
   * @param request {@link Request}
   * @return som.hardware.response.Response
   * @throws Exception throws an exception if the @param request is unknown
   */
  public Response handleRequest(Request request) throws Exception {
    final HWRequest hwRequest = this.buildRequest(request);
    return this.hwService.executeRequest(hwRequest);
  }

  /**
   * Delegates the building of the {@link HWRequest} to any one of the following builders.
   * {@link HWActionRequestBuilder}
   * {@link SensorRequestBuilder}
   * {@link CameraRequestBuilder}
   *
   * @param request {@link Request}
   * @return HWRequest
   * @throws Exception Throws an exception if the {@link som.instruction.request.RequestType}
   *                   does not matches the following
   *                   {@link som.instruction.request.RequestType#HEAT}
   *                   {@link som.instruction.request.RequestType#DISPENSE}
   *                   {@link som.instruction.request.RequestType#STIR}
   *                   {@link som.instruction.request.RequestType#SENSOR}
   *                   {@link som.instruction.request.RequestType#VISION_CAMERA}
   */
  @VisibleForTesting
  HWRequest buildRequest(Request request) throws Exception {
    final HWRequest hwRequest;
    switch (request.getRequestType()) {
      case HEAT:
      case STIR:
      case DISPENSE:
      case ACTUATOR:
        hwRequest = this.hwActionRequestBuilder.buildRequest(request);
        break;
      case SENSOR:
        hwRequest = this.sensorRequestBuilder.buildRequest(request);
        break;
      case VISION_CAMERA:
        hwRequest = this.cameraRequestBuilder.buildRequest(request);
        break;
      default:
        throw new IllegalStateException("Illegal request type : "
          + request.getRequestType());
    }
    return hwRequest;
  }
}
