package som.hardware.request;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import som.hardware.message.HWMessage;
import som.hardware.message.action.ActionRequest;
import som.hardware.message.command.CommandRequest;
import som.instruction.handler.InstructionHandler;


/**
 * Contains parameters pertaining to an action or command request for the hardware.
 */
public class HWActionRequest extends HWRequest {
  /**
   * List of {@link CommandRequest} or {@link ActionRequest}.
   */
  private List<HWMessage> requestParams;

  public HWActionRequest(InstructionHandler instructionHandler) {
    this.instructionHandler = instructionHandler;
    this.hwRequestType = HWRequestType.HW_ACTION_REQUEST;
  }

  /**
   * Returns a read-only list of {@link HWMessage}.
   *
   * @return List
   */
  public @Nullable
      List<HWMessage> getRequestParams() {
    return Collections.unmodifiableList(this.requestParams);
  }

  /**
   * Adds the @param requestParams to the list of requestParams.
   *
   * @param requestParams {@link HWMessage} to add to the requestParams
   */
  public void setRequestParams(@NonNull HWMessage requestParams) {
    if (null == this.requestParams) {
      this.requestParams = new ArrayList<>();
    }
    this.requestParams.add(requestParams);
  }
}
