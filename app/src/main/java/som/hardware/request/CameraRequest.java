package som.hardware.request;

import androidx.annotation.NonNull;

import som.instruction.handler.InstructionHandler;
import som.instruction.request.visionCamera.VisionCameraCommand;

/**
 * Captures all parameters pertaining to a Vision Camera.
 */
public class CameraRequest extends HWRequest {

  private VisionCameraCommand visionCameraCommand;

  /**
   * filename with path for saving image/video.
   */
  private String filenameWithPath;

  public CameraRequest(@NonNull InstructionHandler instructionHandler) {
    this.instructionHandler = instructionHandler;
    this.hwRequestType = HWRequestType.CAMERA_REQUEST;
  }


  public VisionCameraCommand getVisionCameraCommand() {
    return this.visionCameraCommand;
  }

  public void setVisionCameraCommand(@NonNull VisionCameraCommand visionCameraCommand) {
    this.visionCameraCommand = visionCameraCommand;
  }

  public String getFilenameWithPath() {
    return this.filenameWithPath;
  }

  public void setFilenameWithPath(@NonNull String filenameWithPath) {
    this.filenameWithPath = filenameWithPath;
  }
}
