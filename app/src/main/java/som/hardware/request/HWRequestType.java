package som.hardware.request;

public enum HWRequestType {
  CAMERA_REQUEST,
  SENSOR_REQUEST,
  HW_ACTION_REQUEST
}
