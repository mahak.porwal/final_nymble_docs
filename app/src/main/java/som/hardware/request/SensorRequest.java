package som.hardware.request;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import som.hardware.message.sensor.HWSensorRequest;
import som.instruction.handler.InstructionHandler;

/**
 * Sensor request for the HW Library.
 */
public class SensorRequest extends HWRequest {

  private List<HWSensorRequest> requestParams;

  public SensorRequest(@NonNull InstructionHandler instructionHandler) {
    this.instructionHandler = instructionHandler;
    this.hwRequestType = HWRequestType.SENSOR_REQUEST;
  }

  public @Nullable
      List<HWSensorRequest> getRequestParams() {
    return Collections.unmodifiableList(this.requestParams);
  }

  /**
   * Adds a {@link HWSensorRequest} to the {@link SensorRequest#requestParams}.
   *
   * @param sensorRequest request to add
   */
  public void setSensor(@NonNull HWSensorRequest sensorRequest) {
    if (null == this.requestParams) {
      this.requestParams = new ArrayList<>();
    }
    this.requestParams.add(sensorRequest);
  }
}
