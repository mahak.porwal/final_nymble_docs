package som.hardware.request;

import som.instruction.handler.InstructionHandler;

/**
 * Base class for Hardware Request.
 */
public abstract class HWRequest {
  /**
   * Object of {@link InstructionHandler}, which generated this specific request.
   */
  protected InstructionHandler instructionHandler;

  public int getTimeout() {
    return this.timeout;
  }

  protected int timeout;

  protected HWRequestType hwRequestType;

  public HWRequestType getHwRequestType() {
    return this.hwRequestType;
  }

  public InstructionHandler getInstructionHandler() {
    return this.instructionHandler;
  }
}
