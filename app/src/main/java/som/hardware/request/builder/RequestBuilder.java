package som.hardware.request.builder;

import som.hardware.request.HWRequest;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.Request;

public interface RequestBuilder {
  /**
   * Returns a {@link HWRequest} object.
   *
   * @param request {@link Request} from the {@link InstructionHandler}
   * @return HWRequest object which the Hardware understands
   * @throws Exception Throws an exception if it fails to understand the request
   */
  HWRequest buildRequest(Request request) throws Exception;
}
