package som.hardware.request.builder;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.Map;

import som.hardware.request.CameraRequest;
import som.hardware.request.HWRequest;
import som.instruction.request.Request;
import som.instruction.request.RequestParameters;
import som.instruction.request.visionCamera.VisionCameraCommand;

/**
 * Creates request for Vision Camera.
 */
public class CameraRequestBuilder implements RequestBuilder {

  /**
   * Generates {@link CameraRequest} from
   * {@link som.instruction.request.visionCamera.VisionCamera} request.
   *
   * @param request {@link Request} from the {@link InstructionHandler}
   * @return CameraRequest
   * @throws Exception If the {@link RequestParameters#VISUAL_CAMERA_COMMAND} is not found in the
   *                   {@link Request#parameters} map, an exception is thrown
   */
  @Override
  public HWRequest buildRequest(@NonNull Request request) throws Exception {
    final CameraRequest cameraRequest = new CameraRequest(request.getRequester());
    final Map<String, String> requestParameters = request.getParameters();

    if (null == requestParameters) {
      throw new Exception("Null parameters map.");
    }

    if (!requestParameters.containsKey(RequestParameters.VISUAL_CAMERA_COMMAND
        .getRequestParameter())) {
      //no visual camera command present
      throw new Exception("Key visual command not found");
    }

    //TODO : Need to accommodate the saving path of files instruction

    cameraRequest.setVisionCameraCommand(this.extractVisionCameraCommand(requestParameters));
    return cameraRequest;
  }

  /**
   * Extracts the {@link VisionCameraCommand} from the {@link Request#parameters} map.
   *
   * @param requestParameters map of type string to string
   * @return VisionCameraCommand
   * @throws Exception throw exception if the value in key-value pair does not corresponds to any
   *                   {@link VisionCameraCommand}
   */
  @VisibleForTesting
  VisionCameraCommand extractVisionCameraCommand(Map<String, String> requestParameters) throws
      Exception {
    final String string = requestParameters.get(
        RequestParameters.VISUAL_CAMERA_COMMAND.getRequestParameter());
    final VisionCameraCommand visionCameraCommand = VisionCameraCommand.fromString(string);
    if (null == visionCameraCommand) {
      throw new Exception("No matching enum VisionCameraCommand value found for the string "
          + string);
    }
    return visionCameraCommand;
  }
}
