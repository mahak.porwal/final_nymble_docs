package som.hardware.request.builder;

import static som.hardware.message.action.ActionRequestParameters.ActuatorDirection.DIRECTION_ANTICLOCKWISE;

import androidx.annotation.NonNull;
import com.example.recipeengine.resources.JuliaStateDemo;
import java.util.HashMap;
import java.util.Map;
import som.hardware.message.Component;
import som.hardware.message.command.CommandRequest;
import som.hardware.message.command.CommandRequestParameters;
import som.hardware.request.HWActionRequest;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.actuator.Actuators;

public class ActuatorRequestBuilder {
  private static final Map<String, Component> actuatorToComponent =
      new HashMap<String, Component>() {
        {
          put(Actuators.BUZZER, Component.BUZZER);
          put(Actuators.INDUCTION_POWER, Component.INDUCTION_HEATER);
          put(Actuators.STIRRER, Component.STIRRER_MOTOR);
          put(Actuators.DESIGN_LED, Component.LED_DESIGN);
          put(Actuators.EXHAUST_FAN, Component.EXHAUST_FAN);
        }
      };

  /**
   * Accepts a map from String to String, for creating {@link HWActionRequest} for request type
   * {@link som.instruction.request.RequestType#ACTUATOR}
   *
   * @param parameters         Map of String to String containing parameters to create a {@link HWActionRequest}
   * @param instructionHandler
   * @return
   * @throws Exception if the {@link RequestParameters#ACTUATOR} is not present. Throws exception if
   *                   the value of the {@link RequestParameters#ACTUATOR} key is not known.
   */
  public static HWActionRequest actionRequestBuilder(@NonNull Map<String, String> parameters,
                                                     @NonNull InstructionHandler instructionHandler)
      throws Exception {
    //changed actuator key
    final String stringComponent =
        parameters.getOrDefault(RequestParameters.ACTUATOR.getRequestParameter(), null);

    if (stringComponent == null) {
      throw new Exception("No actuator defined");
    }
    final Component component =
        actuatorToComponent.getOrDefault(stringComponent, null);
    if (component == null) {
      throw new Exception(
          "No hardware component associated with the given acutator " + stringComponent);
    }
    HWActionRequest hwActionRequest = null;
    switch (stringComponent) {
      case Actuators.BUZZER:
        break;
      case Actuators.INDUCTION_POWER:
        hwActionRequest = heatRequestBuilder(parameters, instructionHandler);
        break;
      case Actuators.DESIGN_LED:
        break;
      case Actuators.STIRRER:
        hwActionRequest = stirrerRequestBuilder(parameters, instructionHandler);
        break;
      case Actuators.EXHAUST_FAN:
        hwActionRequest = exhaustFanRequestBuilder(parameters, instructionHandler);
        break;
    }
    return hwActionRequest;
  }


  /**
   * Builds the heat request from the {@link som.instruction.request.heat.Heat#} map.
   *
   * @param parameters         contains the information regarding the heat request
   * @param instructionHandler the {@link InstructionHandler} which invoked this request
   * @return HWActionRequest representing the heat request
   * @throws Exception An exception is thrown when there are different keys in the parameters map than
   *                   expected to build the heat request
   */
  private static HWActionRequest heatRequestBuilder(@NonNull Map<String, String> parameters,
                                                    @NonNull InstructionHandler instructionHandler)
      throws Exception {

    if (!parameters.containsKey(RequestParameters.HEAT_POWER_LEVEL.getRequestParameter())) {
      throw new Exception("Key not found "
          + RequestParameters.HEAT_POWER_LEVEL.getRequestParameter());
    }

    final HWActionRequest hwActionRequest = new HWActionRequest(instructionHandler);
    final String powerLevel = parameters.get(RequestParameters.HEAT_POWER_LEVEL
        .getRequestParameter());
    final CommandRequest commandRequest = new CommandRequest(Component.INDUCTION_HEATER);
    hwActionRequest.setRequestParams(commandRequest);
    commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT, powerLevel);
    return hwActionRequest;
  }

  private static HWActionRequest exhaustFanRequestBuilder(@NonNull Map<String, String> parameters,
                                                          @NonNull
                                                              InstructionHandler instructionHandler)
      throws Exception {
    final HWActionRequest hwActionRequest = new HWActionRequest(instructionHandler);
    final String exhaustFanState =
        parameters.getOrDefault(RequestParameters.ACTUATOR_STATE.getRequestParameter(),
            null);
    if (null != exhaustFanState) {
      final Boolean state = Boolean.parseBoolean(exhaustFanState);
      final CommandRequest commandRequest = new CommandRequest(Component.EXHAUST_FAN);
      if (state) {
        commandRequest.setParameters(CommandRequestParameters.COMPONENT_ON_OFF_STATE,
            "ON_STATE");
      } else {
        commandRequest.setParameters(CommandRequestParameters.COMPONENT_ON_OFF_STATE,
            "OFF_STATE");
      }
      hwActionRequest.setRequestParams(commandRequest);
    } else {
      final String stirrerSpeedString = parameters.getOrDefault(RequestParameters.ACTUATOR_SPEED,
          null);
      if (stirrerSpeedString == null) {
        throw new Exception("No speed defined for exhaust fan");
      }
      final Integer stirrerSpeed = Integer.valueOf(Double.valueOf(stirrerSpeedString).intValue());
      final CommandRequest commandRequest = new CommandRequest(Component.EXHAUST_FAN);
      commandRequest.setParameters(CommandRequestParameters.SPEED, String.valueOf(stirrerSpeed));
      hwActionRequest.setRequestParams(commandRequest);
    }
    return hwActionRequest;
  }

  private static HWActionRequest stirrerRequestBuilder(@NonNull Map<String, String> parameters,
                                                       @NonNull
                                                           InstructionHandler instructionHandler)
      throws Exception {
    final String stirrerSpeedString =
        parameters.getOrDefault(RequestParameters.ACTUATOR_SPEED.getRequestParameter(), null);
    if (stirrerSpeedString == null) {
      throw new Exception("No speed defined for stirrer");
    }
    //changed actuator key
    final String stirrerDirection =
        parameters.getOrDefault(RequestParameters.ACTUATOR_DIRECTION.getRequestParameter(),
            DIRECTION_ANTICLOCKWISE.getParameter());
    final Integer stirrerSpeed = Integer.valueOf(Double.valueOf(stirrerSpeedString).intValue());
    final HWActionRequest hwActionRequest = new HWActionRequest(instructionHandler);
    final CommandRequest commandRequest = new CommandRequest(Component.STIRRER_MOTOR);
    commandRequest.setParameters(CommandRequestParameters.SPEED, String.valueOf(stirrerSpeed));
    commandRequest.setParameters(CommandRequestParameters.DIRECTION, stirrerDirection);
    hwActionRequest.setRequestParams(commandRequest);
    setStirrerState(0 == stirrerSpeed);
    return hwActionRequest;
  }

  private static void setStirrerState(boolean isStationary) {
    //set stirrer running or idle
    JuliaStateDemo juliaStateDemo = JuliaStateDemo.getInstance();
    if (isStationary) {
      juliaStateDemo.setStirrerStatus("idle");
    } else {
      juliaStateDemo.setStirrerStatus("running");
    }
  }
}
