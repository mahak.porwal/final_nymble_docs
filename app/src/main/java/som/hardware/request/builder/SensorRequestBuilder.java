package som.hardware.request.builder;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;

import java.util.HashMap;
import java.util.Map;

import som.hardware.message.Component;
import som.hardware.message.sensor.HWSensorRequest;
import som.hardware.request.HWRequest;
import som.hardware.request.SensorRequest;
import som.instruction.request.Request;
import som.instruction.request.RequestParameters;
import som.instruction.request.sensor.Sensors;


/**
 * Creates request for sensors.
 */
public class SensorRequestBuilder implements RequestBuilder {
  private static final Map<String, Component> sensorToComponent =
      new HashMap<String, Component>() {
        {
          put(Sensors.STIRRER_CURRENT.getSensorName(), Component.STIRRER_MOTOR);
          put(Sensors.THERMAL_CAMERA.getSensorName(), Component.THERMAL_CAMERA);
          put(Sensors.WEIGHT_SENSOR.getSensorName(), Component.LOADCELL_SENSOR);
        }
      };

  /**
   * Creates the {@link SensorRequest} from {@link som.instruction.request.sensor.Sensor} request.
   *
   * @param request {@link Request} from the {@link InstructionHandler}
   * @return sensorRequest
   * @throws Exception If the key {@link RequestParameters#SENSOR} is not found in the
   *                   {@link Request#parameters} map, then an exception is thrown
   */
  @Override
  public HWRequest buildRequest(@NonNull Request request) throws Exception {

    final SensorRequest sensorRequest = new SensorRequest(request.getRequester());
    final Map<String, String> parametersMap = request.getParameters();
    if (null == parametersMap) {
      throw new Exception("Null request parameters map");
    }

    if (!parametersMap.containsKey(RequestParameters.SENSOR.getRequestParameter())) {
      throw new Exception("Key sensor not present");
    } else {
      final String sensor = parametersMap.get(
          RequestParameters.SENSOR.getRequestParameter());
      final HWSensorRequest hwSensorRequest =
          new HWSensorRequest(this.mapComponentFromSensor(sensor), null);
      sensorRequest.setSensor(hwSensorRequest);
    }
    return sensorRequest;
  }

  /**
   * Maps sensor to {@link Component}.
   *
   * @param sensor String indicating the sensor
   * @return Component
   * @throws Exception If sensor is not found in {@link SensorRequestBuilder#sensorToComponent},
   *                   an exception is thrown
   */
  @VisibleForTesting
  Component mapComponentFromSensor(String sensor) throws Exception {
    if (!sensorToComponent.containsKey(sensor)) {
      throw new Exception("Unknown sensor " + sensor);
    }
    return sensorToComponent.get(sensor);
  }
}
