package som.hardware.request.builder.hwActionRequest;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import som.hardware.message.Component;
import som.hardware.message.action.ActionRequest;
import som.hardware.message.action.ActionRequestParameters;
import som.hardware.message.action.ActionType;
import som.hardware.request.HWActionRequest;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.dispense.Dispense;
import som.instruction.request.dispense.DispenseContainer;
import som.instruction.request.dispense.Ingredient;
import som.instruction.request.sensor.Sensors;


/**
 * This class creates a {@link HWActionRequest} for a{@link Dispense} object.
 */
public final class DispenseRequestBuilder {
  private static final Map<String, Component> containerToComponent =
    new HashMap<String, Component>() {
      {
        put(DispenseContainer.MACRO_CONTAINER_1.toString(), Component.MACRO_SERVO_1);
        put(DispenseContainer.MACRO_CONTAINER_2.toString(), Component.MACRO_SERVO_2);
        put(DispenseContainer.MACRO_CONTAINER_3.toString(), Component.MACRO_SERVO_3);
        put(DispenseContainer.MACRO_CONTAINER_4.toString(), Component.MACRO_SERVO_4);
        put(DispenseContainer.MICRO_POD_1.toString(), Component.MICRO_SERVO_1);
        put(DispenseContainer.MICRO_POD_2.toString(), Component.MICRO_SERVO_2);
        put(DispenseContainer.MICRO_POD_3.toString(), Component.MICRO_SERVO_3);
        put(DispenseContainer.MICRO_POD_4.toString(), Component.MICRO_SERVO_4);
        put(DispenseContainer.MICRO_POD_5.toString(), Component.MICRO_SERVO_5);
        put(DispenseContainer.MICRO_POD_6.toString(), Component.MICRO_SERVO_6);
        put(DispenseContainer.LIQUID_OIL_CONTAINER.toString(), Component.OIL_PUMP);
        put(DispenseContainer.LIQUID_WATER_CONTAINER.toString(), Component.WATER_PUMP);
      }
    };

  private DispenseRequestBuilder() {
  }

  /**
   * Returns a {@link HWActionRequest}, created using the parameters map.
   *
   * @param parameters         request parameters for dispense
   * @param instructionHandler {@link InstructionHandler} object that invoked the request
   * @return HWActionRequest object representing the dispense request
   * @throws Exception Exception thrown when expected is not present
   */
  public static HWActionRequest buildDispenseRequest(@NonNull Map<String, String> parameters,
                                                     @NonNull InstructionHandler instructionHandler)
    throws Exception {
    final HWActionRequest hwActionRequest = new HWActionRequest(instructionHandler);
    final String ingredient;
    final String quantity;

    if (parameters.containsKey(RequestParameters.INGREDIENT.getRequestParameter())) {

      ingredient = parameters.get(RequestParameters.INGREDIENT.getRequestParameter());

    } else {
      throw new Exception("Key missing "
        + RequestParameters.INGREDIENT.getRequestParameter());
    }

    if (parameters.containsKey(RequestParameters.QUANTITY.getRequestParameter())) {
      quantity = parameters.get(RequestParameters.QUANTITY.getRequestParameter());
    } else {
      throw new Exception("Key missing " + RequestParameters.QUANTITY.getRequestParameter());
    }


    //get component from the ingredient string
    //final Component component = getComponentFromIngredient(ingredient);
    //get component from parameters map
    final Component component = getComponentFromMap(parameters);

    //get action type from ingredient string
    //final ActionType actionType = getActionTypeFromIngredient(ingredient);
    //get action type from parameters map
    final ActionType actionType = getActionTypeFromMap(parameters);

    //get corresponding hardware units from ingredient quantity
    final String hardwareUnits = getHardwareUnitsFromQuantity(ingredient, quantity);

    final ActionRequest dispenseActionRequest = new ActionRequest(component, actionType);

    dispenseActionRequest.setActionParameters(ActionRequestParameters.HARDWARE_UNITS,
      hardwareUnits);

    hwActionRequest.setRequestParams(dispenseActionRequest);

    return hwActionRequest;
  }

  /**
   * Returns the {@link Component}, which contains the given ingredient.
   *
   * @param ingredient {@link Ingredient} for which the container is needed
   * @return Component containing the ingredient
   */
  private static Component getComponentFromIngredient(String ingredient) {
    return Component.MACRO_SERVO_1;
  }

  /**
   * Returns the {@link ActionType} through which the ingredient is dispensed.
   *
   * @param ingredient {@link Ingredient} whose {@link ActionType} is required
   * @return ActionType
   */
  private static ActionType getActionTypeFromIngredient(String ingredient) {
    return ActionType.MACRO_LIQUID_DISPENSE;
  }

  /**
   * Returns the dimension-less quantity which the Hardware understands.
   * This dimension-less quantity represents the quantity of the ingredient in the Hardware world.
   *
   * @param ingredient {@link Ingredient} for which the dimension-less quantity is needed
   * @param quantity   Recipe-understandable version of amount of ingredient (grams or
   *                   milli-litres)
   * @return Hardware world version of quantity (which is dependent on what mechanism is used
   * to dispense)
   */
  private static String getHardwareUnitsFromQuantity(String ingredient, String quantity) {
    return quantity;
  }

  private static Component getComponentFromMap(Map<String, String> parameters) throws Exception {
    final String containerString =
      parameters.getOrDefault(RequestParameters.DISPENSE_CONTAINER.getRequestParameter(),
        null);
    if (containerString == null) {
      throw new Exception("No dispense container mentioned");
    }
    final Component component = containerToComponent.getOrDefault(containerString, null);
    if (component == null) {
      throw new Exception("No component associated with the dispense container " + containerString);
    }
    return component;
  }


  private static ActionType getActionTypeFromMap(Map<String, String> parameters) throws Exception {
    final String actionTypeString = parameters.getOrDefault(
      RequestParameters.DISPENSE_BEHAVIOR.getRequestParameter(), null);
    if (actionTypeString == null) {
      throw new Exception("Dispense Behavior key not present");
    }
    final ActionType actionType = ActionType.valueOf(actionTypeString);
    return actionType;
  }
}
