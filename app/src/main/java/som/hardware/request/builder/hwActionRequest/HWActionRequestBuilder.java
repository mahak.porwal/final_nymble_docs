package som.hardware.request.builder.hwActionRequest;

import androidx.annotation.NonNull;

import java.util.Map;

import som.hardware.message.Component;

import som.hardware.message.command.CommandRequest;
import som.hardware.message.command.CommandRequestParameters;
import som.hardware.request.HWActionRequest;
import som.hardware.request.HWRequest;

import som.hardware.request.builder.ActuatorRequestBuilder;
import som.hardware.request.builder.RequestBuilder;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.Request;
import som.instruction.request.RequestParameters;

/**
 * Builds {@link HWRequest} for the following types of {@link som.instruction.request.RequestType}.
 * {@link som.instruction.request.RequestType#HEAT}
 * {@link som.instruction.request.RequestType#STIR}
 * {@link som.instruction.request.RequestType#DISPENSE}
 */
public class
HWActionRequestBuilder implements RequestBuilder {

  /**
   * Builds {@link HWActionRequest} for request types mentioned in the class description.
   *
   * @param request {@link Request} from the {@link InstructionHandler}
   * @return HWActionRequest
   * @throws Exception throws {@link IllegalStateException} for the
   *                   {@link som.hardware.message.RequestType} other
   *                   than the ones mentioned in the class description
   */
  @Override
  public HWRequest buildRequest(@NonNull Request request) throws Exception {
    final HWRequest hwRequest;
    final Map<String, String> parametersMap = request.getParameters();
    if (null == parametersMap) {
      throw new Exception("Null parameters map");
    }
    switch (request.getRequestType()) {
      case HEAT:
        hwRequest = this.heatRequestBuilder(parametersMap,
          request.getRequester());
        break;
      case DISPENSE:
        hwRequest = DispenseRequestBuilder.buildDispenseRequest(parametersMap,
          request.getRequester());
        break;
      case STIR:
        hwRequest = StirRequestBuilder.buildStirrerRequest(parametersMap,
          request.getRequester());
        break;
      case ACTUATOR:
        hwRequest = ActuatorRequestBuilder.actionRequestBuilder(parametersMap, request.getRequester());
        break;
      default:
        throw new IllegalStateException("Unexpected value: " + request.getRequestType());
    }

    return hwRequest;
  }

  /**
   * Builds the heat request from the {@link som.instruction.request.heat.Heat} map.
   *
   * @param parameters         contains the information regarding the heat request
   * @param instructionHandler the {@link InstructionHandler} which invoked this request
   * @return HWActionRequest representing the heat request
   * @throws Exception An exception is thrown when there different keys in the parameters map than
   *                   expected to build the heat request
   */
  private HWActionRequest heatRequestBuilder(@NonNull Map<String, String> parameters,
                                             @NonNull InstructionHandler instructionHandler)
    throws Exception {

    if (!parameters.containsKey(RequestParameters.HEAT_POWER_LEVEL.getRequestParameter())) {
      throw new Exception("Key not found "
        + RequestParameters.HEAT_POWER_LEVEL.getRequestParameter());
    }

    final HWActionRequest hwActionRequest = new HWActionRequest(instructionHandler);
    final String powerLevel = parameters.get(RequestParameters.HEAT_POWER_LEVEL
      .getRequestParameter());
    final CommandRequest commandRequest = new CommandRequest(Component.INDUCTION_ACTUATOR);
    hwActionRequest.setRequestParams(commandRequest);
    commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT, powerLevel);
    return hwActionRequest;
  }
}
