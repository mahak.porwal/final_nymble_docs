package som.hardware.request.builder.hwActionRequest;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import som.hardware.message.Component;
import som.hardware.message.action.ActionRequest;
import som.hardware.message.action.ActionType;
import som.hardware.request.HWActionRequest;
import som.instruction.handler.InstructionHandler;
import som.instruction.request.RequestParameters;
import som.instruction.request.stir.Stir;
import som.instruction.request.stir.StirringProfile;

/**
 * Creates a {@link HWActionRequest} for {@link Stir} request.
 */
public final class StirRequestBuilder {

  private StirRequestBuilder() {
  }

  /**
   * Maps the stirring profile in String type to the Stirring Profile in ActionType.
   * There are three stirring profile in the ActionType
   * {@link ActionType#STIRRER_SLOW_PROFILE}
   * {@link ActionType#STIRRER_MEDIUM_PROFILE}
   * {@link ActionType#STIRRER_FAST_PROFILE}
   */
  private static final Map<String, ActionType> stirrerProfileToActionType =
    new HashMap<String, ActionType>() {
      {
          
      }
    };


  /**
   * Creates the {@link ActionRequest} from the {@link Stir} request.
   *
   * @param parameters         {@link Stir#parameters} parameters map of type string to string
   * @param instructionHandler {@link InstructionHandler} that generated the request
   * @return HWActionRequest
   * @throws Exception If the {@link RequestParameters#STIRRER_PROFILE} is not present in
   *                   the parameters, then an exception is thrown.
   */
  public static HWActionRequest buildStirrerRequest(@NonNull final Map<String, String> parameters,
                                                    @NonNull final InstructionHandler instructionHandler)
    throws Exception {


    final HWActionRequest hwActionRequest = new HWActionRequest(instructionHandler);

    if (!parameters.containsKey(RequestParameters.STIRRER_PROFILE.getRequestParameter())) {
      throw new Exception("Key missing " + RequestParameters.STIRRER_PROFILE);
    }

    final String stirringProfile = parameters.get(
      RequestParameters.STIRRER_PROFILE.getRequestParameter());


    final ActionRequest stirActionRequest = new ActionRequest(Component.STIRRER_MOTOR,
      getActionTypeFromString(stirringProfile));

    hwActionRequest.setRequestParams(stirActionRequest);

    return hwActionRequest;
  }

  /**
   * Gets the {@link ActionType} from the stirrerProfileToActionType map.
   *
   * @param stirringProfile String representing the type of stirring profile
   * @return ActionType {@link ActionType}
   * @throws Exception If the stirringProfile does not matches any key in the
   *                   stirrerProfileToActionType map, then an exception is thrown
   */
  static ActionType getActionTypeFromString(@NonNull String stirringProfile) throws Exception {
    if (!stirrerProfileToActionType.containsKey(stirringProfile)) {
      throw new Exception("Invalid Stirrer Profile");
    }
    return stirrerProfileToActionType.get(stirringProfile);
  }
}
