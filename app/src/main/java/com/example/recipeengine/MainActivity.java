package com.example.recipeengine;

import android.Manifest;
import android.Manifest.permission;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.example.recipeengine.engine.DaggerEngineComponent;
import com.example.recipeengine.engine.Engine;
import com.example.recipeengine.recipe.reader.RecipeReaderModule;
import com.example.recipeengine.util.AppPrefs;
import com.example.recipeengine.util.FileUtil;
import com.example.recipeengine.util.Native.NativeUtil;
import com.example.recipeengine.util.debug.DebugActivity;
import com.example.recipeengine.util.AppPrefs;
import com.example.recipeengine.util.FileUtil;
import com.example.recipeengine.util.Native.NativeUtil;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import som.hardware.message.Component;
import som.hardware.message.action.ActionRequest;
import som.hardware.message.action.ActionRequestParameters;
import som.hardware.message.action.ActionType;
import som.hardware.message.command.CommandRequest;
import som.hardware.message.command.CommandRequestParameters;
import som.hardware.message.sensor.HWSensorRequest;
import som.hardware.message.sensor.SensorRequestParameters;
import som.hardware.message.sensor.SensorUnit;
import som.hardware.request.HWActionRequest;
import som.hardware.request.SensorRequest;
import som.hardware.request.handler.RequestHandler;
import som.hardware.response.CameraResponse;
import som.hardware.response.HWActionResponse;
import som.hardware.response.SensorResponse;
import som.hardware.service.HWService;
import som.instruction.request.visionCamera.VisionCamera;
import som.instruction.request.visionCamera.VisionCameraCommand;
import timber.log.Timber;
import timber.log.Timber.DebugTree;


public class MainActivity extends AppCompatActivity implements
  AdapterView.OnItemSelectedListener {

  private CompositeDisposable compositeDisposable;
  private static Context context;

  String[] input = {
    "NullEVent",
    "PowerLevelOne",
    "PowerLevelTwo",
    "PowerLevelThree",
    "PowerLevelFour",
    "PowerLevelFive",
    "PowerLevelSix",
    "PowerLevelSeven",
    "DetectPan",
    "Shutdown",
    "ManualMode"};
  String[] voltageInputs = {
    VoltageSensorInput.BODY_TEMPERATURE.name(),
    VoltageSensorInput.WATER_TEMPERATURE.name()
  };

  String[] gpioOutputs = {
    GpioOutputPins.MUX_A0.name(),
    GpioOutputPins.MUX_A1.name(),
    GpioOutputPins.FAN_ON_OFF.name(),
    GpioOutputPins.HEATER_ENABLE.name()
  };

  String[] gpioInputs = {
    GpioInputPins.LIMIT_SW1.name(),
    GpioInputPins.LIMIT_SW2.name(),
    GpioInputPins.INDUCTIVE_S1.name(),
    GpioInputPins.INDUCTIVE_S2.name()
  };

  private volatile String selectedOutputPin = GpioOutputPins.MUX_A0.name();
  private volatile String selectedInputPin = GpioOutputPins.MUX_A1.name();
  private volatile String selectedVoltageInput = VoltageSensorInput.BODY_TEMPERATURE.name();

  private final GpioControls gpioControls = new GpioControls();
  private final VoltageSensor voltageSensor = new VoltageSensor();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    Timber.plant(new DebugTree());
    AppPrefs.init(getApplicationContext());
    this.compositeDisposable = new CompositeDisposable();
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    context = getApplicationContext();
    ActivityCompat.requestPermissions(MainActivity.this,
      new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA},
      1);
    final Spinner spinner = (Spinner) findViewById(R.id.spinner);

    //spinner.setOnItemSelectedListener(this);
    spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        MainActivity.this.selectedVoltageInput = MainActivity.this.voltageInputs[position];
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });
    //Creating the ArrayAdapter instance having the country list
    final ArrayAdapter aa = new ArrayAdapter(this, android.R.layout.simple_spinner_item, this.voltageInputs);
    aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    //Setting the ArrayAdapter data on the Spinner
    spinner.setAdapter(aa);
    final Button panDetectionButton = findViewById(R.id.button);
    panDetectionButton.setText("Sense Voltage");
    /*panDetectionButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        DetectPanAndSetStatus();
      }
    });*/
    panDetectionButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        final float value = MainActivity.this.voltageSensor.sensorVoltage(VoltageSensorInput.valueOf(
          MainActivity.this.selectedVoltageInput));
        final TextView textView = findViewById(R.id.pan_detect_status);
        textView.setText(Float.valueOf(value).toString());
      }
    });


    final Spinner gpioOutputSpinner = (Spinner) findViewById(R.id.gpio_output_pins);
    gpioOutputSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        MainActivity.this.selectedOutputPin = MainActivity.this.gpioOutputs[position];
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });
    final ArrayAdapter gpioOutputPinsArray = new ArrayAdapter(this, android.R.layout.simple_spinner_item, this.gpioOutputs);
    gpioOutputPinsArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    gpioOutputSpinner.setAdapter(gpioOutputPinsArray);

    final Spinner gpioInputSpinner = (Spinner) findViewById(R.id.gpio_input_pins);
    gpioInputSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        MainActivity.this.selectedInputPin = MainActivity.this.gpioInputs[position];
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });
    final ArrayAdapter gpioInputPinsArray = new ArrayAdapter(this, android.R.layout.simple_spinner_item, this.gpioInputs);
    gpioInputPinsArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    gpioInputSpinner.setAdapter(gpioInputPinsArray);

    final Button setGpioButton = findViewById(R.id.set_gpio);
    setGpioButton.setOnClickListener(v -> {
      final RadioButton highState = findViewById(R.id.gpio_state_high);
      if (highState.isChecked()) {
        this.gpioControls.setGpioState(GpioOutputPins.valueOf(this.selectedOutputPin), 1);
      } else {
        this.gpioControls.setGpioState(GpioOutputPins.valueOf(this.selectedOutputPin), 0);
      }
      Toast.makeText(context, "Gpio status updated", Toast.LENGTH_LONG).show();
    });

    final Button readGpioButton = findViewById(R.id.read_gpio);
    readGpioButton.setOnClickListener(v -> {
      final TextView gpioStatus = findViewById(R.id.gpio_read_status);
      final int gpioState = this.gpioControls.getGpioState(GpioInputPins.valueOf(this.selectedInputPin));
      if (-1 == gpioState) {
        Toast.makeText(context, "Unable to read gpio state", Toast.LENGTH_LONG).show();
        return;
      } else if (0 == gpioState) {
        gpioStatus.setText("LOW");
      } else {
        gpioStatus.setText("HIGH");
      }
      Toast.makeText(context, "Gpio status updated", Toast.LENGTH_LONG).show();
    });


    final Switch waterSwitch = findViewById(R.id.water_pump);
    waterSwitch.setOnCheckedChangeListener(((buttonView, isChecked) -> {
      final EditText water_duty_cycle = findViewById(R.id.water_duty_cycle);
      final String editableString = water_duty_cycle.getText().toString();
      int duty = 0;
      if (isChecked) {
        if (editableString.isEmpty()) {
          duty = 50;
        } else {
          duty = Integer.valueOf(editableString).intValue();
        }
      }
      this.WaterPumpActuate(duty);
    }));

    final Switch oilSwitch = findViewById(R.id.oil_pump);
    oilSwitch.setOnCheckedChangeListener(((buttonView, isChecked) -> {
      final EditText oil_duty_cycle = findViewById(R.id.oil_duty_cycle);
      final String editableString = oil_duty_cycle.getText().toString();
      int duty = 0;
      if (isChecked) {
        if (editableString.isEmpty()) {
          duty = 50;
        } else {
          duty = Integer.valueOf(editableString).intValue();
        }
      }
      this.OilPumpActuate(duty);
    }));

    final Switch fanSwitch = findViewById(R.id.ex_fan_switch);
    fanSwitch.setOnCheckedChangeListener(((buttonView, isChecked) -> {
      final EditText fan_duty_cycle = findViewById(R.id.ex_fan_duty);
      final String editableString = fan_duty_cycle.getText().toString();
      int duty = 0;
      if (isChecked) {
        if (editableString.isEmpty()) {
          duty = 50;
        } else {
          duty = Integer.valueOf(editableString).intValue();
        }
      }
      this.ExhaustFanActuate(duty);
    }));

    /*try {
      HWService.startHwService();
      //UpdateInductionStatus();
    } catch (InterruptedException e) {
      Timber.e(e);
    }*/
    /*String filepath = Environment.getExternalStorageDirectory().getAbsolutePath();
    Object object = null;
    try {
      object = new JSONParser().parse(new FileReader(
        filepath + "/recipe_test/Recipe_DispenseInfer.json"));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }

    JSONObject recipeJsonObject = (JSONObject) object;
    Map<String, String> parameters = new HashMap<>();
    Engine baseEngine = DaggerEngineComponent.builder().recipeReaderModule(
        new RecipeReaderModule(recipeJsonObject, 0, parameters)).build().createEngine();
    baseEngine.startCooking();*/
    //this.setPwmDuty(50);
    //this.setPot(Component.INDUCTION_PWM_DUTY_POT, 255);
    /*for (int i = 1; i < 101; i++) {
      this.setPot(Component.INDUCTION_PWM_DUTY_POT, i);
      try {
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }*/
    startActivity(new Intent(getApplicationContext(), DebugActivity.class));
    /*for (int i = 0; i < 256; i++) {
      this.setPot(Component.INDUCTION_PWM_DUTY_POT, i);

      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      final float voltage = this.bodyTempSensor();
      final float resistance = ((5 * (4700)) / voltage) - 5700;
      Timber.d("%d, %.4fv, %.4fOhms", i, voltage, resistance);
    }*/
    /*this.readAdc(Component.INDUCTION_AC_CURRENT_SENSOR);
    this.readAdc(Component.INDUCTION_AC_VOLTAGE_SENSOR);
    while(true){
      this.readAdc(Component.INDUCTION_PAN_TEMPERATURE_SENSOR);
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }*/
    /*this.turnOnExhaustFan();
    long prevCounterVal = 0;
    long time = System.currentTimeMillis();
    while (true) {
      final long counterValue = this.captureCounter();
      if (prevCounterVal != counterValue) {
        Timber.d("Counter val = %d, Diff from prev value = %d, time elapsed %d",
          counterValue, counterValue - prevCounterVal, System.currentTimeMillis() - time);
        prevCounterVal = counterValue;
        time = System.currentTimeMillis();
      }
    }*/
  }


  private void setPwmDuty(int duty) {
    final HWActionResponse hwActionResponse;
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest = new HWActionRequest(null);
    final CommandRequest commandRequest = new CommandRequest(Component.INDUCTION_ACTUATOR);
    commandRequest.setParameters(CommandRequestParameters.SPEED, String.valueOf(duty));
    hwActionRequest.setRequestParams(commandRequest);
    hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> map = hwActionResponse.getActionResponseData();
    System.out.println("native test " + map);
  }

  private float readAdc(Component component) {
    final SensorRequest sensorRequest = new SensorRequest(null);
    final HWSensorRequest hwSensorRequest = new HWSensorRequest(component, SensorUnit.MILLI_VOLTS);
    sensorRequest.setSensor(hwSensorRequest);
    final SensorResponse sensorResponse = NativeUtil.ExecuteSensorRequest(sensorRequest);
    final Map<String, List<Double>> listMap = sensorResponse.getSensorData();
    final Float sensorValue = listMap.get("SENSOR_VALUE").get(0).floatValue();
    Timber.d("%.4f", sensorValue.floatValue());
    return sensorValue.floatValue();
  }

  private float waterTempSensor() {
    final SensorRequest sensorRequest = new SensorRequest(null);
    final HWSensorRequest hwSensorRequest = new HWSensorRequest(Component.WATER_TEMPERATURE,
      null);
    sensorRequest.setSensor(hwSensorRequest);
    final SensorResponse sensorResponse = NativeUtil.ExecuteSensorRequest(sensorRequest);
    final Map<String, List<Double>> listMap = sensorResponse.getSensorData();
    final Float sensorValue = listMap.get("SENSOR_VALUE").get(0).floatValue();
    System.out.println("native test " + listMap);
    return sensorValue;
  }

  private float bodyTempSensor() {
    final SensorRequest sensorRequest = new SensorRequest(null);
    final HWSensorRequest hwSensorRequest = new HWSensorRequest(Component.BODY_TEMPERATURE,
      null);
    sensorRequest.setSensor(hwSensorRequest);
    final SensorResponse sensorResponse = NativeUtil.ExecuteSensorRequest(sensorRequest);
    final Map<String, List<Double>> listMap = sensorResponse.getSensorData();
    final Float sensorValue = listMap.get("SENSOR_VALUE").get(0).floatValue();
    System.out.println("native test " + listMap);
    return sensorValue;
  }

  private void setPot(Component component, int steps) {
    final HWActionResponse hwActionResponse;
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest = new HWActionRequest(null);
    final CommandRequest commandRequest = new CommandRequest(component);
    commandRequest.setParameters(CommandRequestParameters.SPEED, String.valueOf(steps));
    commandRequest.setParameters(CommandRequestParameters.DIGITAL_POT_REQ, "WRITE_POT");
    hwActionRequest.setRequestParams(commandRequest);
    hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> map = hwActionResponse.getActionResponseData();
    System.out.println("native test " + map);
  }

  private long captureCounter() {
    final HWSensorRequest hwSensorRequest =
      new HWSensorRequest(Component.EXHAUST_FAN_COUNTER, null);
    hwSensorRequest.setParameters(SensorRequestParameters.COUNTER_REQ, "COUNTER_READ");
    final SensorRequest sensorRequest = new SensorRequest(null);
    sensorRequest.setSensor(hwSensorRequest);
    final SensorResponse sensorResponse = NativeUtil.ExecuteSensorRequest(sensorRequest);
    final Double sensorValue = sensorResponse.getSensorData().get("SENSOR_VALUE").get(0);
    return sensorValue.longValue();
  }

  private void turnOnExhaustFan() {
    final HWActionRequest hwActionRequest = new HWActionRequest(null);
    final CommandRequest commandRequest = new CommandRequest(Component.EXHAUST_FAN);
    commandRequest.setParameters(CommandRequestParameters.SPEED, String.valueOf(10));
    hwActionRequest.setRequestParams(commandRequest);
    final HWActionResponse hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    hwActionResponse.getActionResponseData();
  }

  private void ExhaustFanActuate(int speed) {
    final HWActionRequest hwActionRequest = new HWActionRequest(null);
    final CommandRequest commandRequest = new CommandRequest(Component.EXHAUST_FAN);
    commandRequest.setParameters(CommandRequestParameters.SPEED, String.valueOf(speed));
    hwActionRequest.setRequestParams(commandRequest);
    final HWActionResponse hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    hwActionResponse.getActionResponseData();
  }

  private void captureImage(final String filename) {
    final VisionCamera camera = new VisionCamera(null);
    camera.setCameraCommand(VisionCameraCommand.STILL_CAPTURE);
    final RequestHandler requestHandler = new RequestHandler();
    try {
      final CameraResponse cameraResponse = (CameraResponse) requestHandler.handleRequest(camera);
      final Bitmap image = cameraResponse.getImage();
      this.saveImage(image, filename);
    } catch (Exception e) {
      Timber.e(e);
    }
  }

  private void saveImage(@NonNull Bitmap image, @NonNull String filename) {
    final File imageFile = FileUtil.getAsFile("/camera_test/", filename);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename);
    }
  }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    Toast.makeText(getApplicationContext(), this.input[position], Toast.LENGTH_LONG).show();
    inductionControl(this.input[position]);
  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {

  }

  private void getThermalMatrix() {
    final HWSensorRequest hwSensorRequest = new HWSensorRequest(Component.THERMAL_CAMERA, null);
    final SensorRequest sensorRequest = new SensorRequest(null);
    sensorRequest.setSensor(hwSensorRequest);
    final SensorResponse sensorResponse = NativeUtil.ExecuteSensorRequest(sensorRequest);
    final Map<String, List<Double>> sensor = sensorResponse.getSensorData();
    final List<Double> sensorValues = sensor.get("SENSOR_VALUE");
    for (int i = 0; i < 32; i++) {
      for (int j = 0; j < 32; j++) {
        System.out.print("native test " + sensorValues.get(32 * i + j));
      }
      System.out.println();
    }
  }

  private void readFileFromSDCard() {
    final File directory = Environment.getExternalStorageDirectory();
    // Assumes that a file article.rss is available on the SD card
    final File file = new File(directory + "/recipe_test/Recipe_WeightSensorTest.json");
    if (!file.exists()) {
      throw new RuntimeException("File not found");
    }
    Log.e("Testing", "Starting to read");
    BufferedReader reader = null;
    StringBuilder builder = null;
    try {
      reader = new BufferedReader(new FileReader(file));
      builder = new StringBuilder();
      String line;
      while ((line = reader.readLine()) != null) {
        builder.append(line);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      if (reader != null) {
        try {
          reader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void inductionControl(String input) {
    final HWActionRequest hwActionRequest = new HWActionRequest(null);
    final CommandRequest commandRequest = new CommandRequest(Component.INDUCTION_HEATER);
    switch (input) {
      case "PowerLevelOne":
        commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT,
          "Power_Level_One");
        break;
      case "PowerLevelTwo":
        commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT,
          "Power_Level_Two");
        break;
      case "PowerLevelThree":
        commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT,
          "Power_Level_Three");
        break;
      case "PowerLevelFour":
        commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT,
          "Power_Level_Four");
        break;
      case "PowerLevelFive":
        commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT,
          "Power_Level_Five");
        break;
      case "PowerLevelSix":
        commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT,
          "Power_Level_Six");
        break;
      case "PowerLevelSeven":
        commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT,
          "Power_Level_Seven");
        break;
      case "Shutdown":
        commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT,
          "Shutdown");
        break;
      case "ManualMode":
        commandRequest.setParameters(CommandRequestParameters.INDUCTION_CONTROL_INPUT,
          "Manual_Mode");
        break;
      default:
        return;
    }

    hwActionRequest.setRequestParams(commandRequest);
    NativeUtil.ExecuteHWActionRequest(hwActionRequest);
  }

  private void parallelMicros() {
    Disposable disposableMicro = Observable.just(1).observeOn(Schedulers.newThread()).subscribe(l ->
      this.MicroDispense(1, Component.MICRO_SERVO_1, ActionType.MICRO_CLOCKWISE_DISPENSE));

    Disposable disposableMacro = Observable.just(1).observeOn(Schedulers.newThread()).subscribe(l ->
      this.MicroDispense(1, Component.MICRO_SERVO_3, ActionType.MICRO_CLOCKWISE_DISPENSE));

    /*Disposable disposableMacro1 = Observable.just(1).observeOn(Schedulers.newThread()).subscribe(l ->
      this.MicroDispense(1, Component.MICRO_SERVO_5, ActionType.MICRO_CLOCKWISE_DISPENSE));*/
    this.compositeDisposable.add(disposableMacro);
    this.compositeDisposable.add(disposableMicro);
    //this.compositeDisposable.add(disposableMacro1);
  }

  private void parallelMacros() {
    Disposable disposableMicro = Observable.just(1).observeOn(Schedulers.newThread()).subscribe(l ->
      this.MicroDispense(1, Component.MACRO_SERVO_1, ActionType.MACRO_NON_LIQUID_DISPENSE));

    Disposable disposableMacro = Observable.just(1).observeOn(Schedulers.newThread()).subscribe(l ->
      this.MicroDispense(1, Component.MACRO_SERVO_3, ActionType.MACRO_NON_LIQUID_DISPENSE));

    Disposable disposableMacro1 =
      Observable.just(1).observeOn(Schedulers.newThread()).subscribe(l ->
        this.MicroDispense(1, Component.MACRO_SERVO_4, ActionType.MACRO_NON_LIQUID_DISPENSE));

    /*Disposable disposableMacro1 = Observable.just(1).observeOn(Schedulers.newThread()).subscribe(l ->
      this.MicroDispense(1, Component.MICRO_SERVO_5, ActionType.MICRO_CLOCKWISE_DISPENSE));*/
    this.compositeDisposable.add(disposableMacro);
    this.compositeDisposable.add(disposableMicro);
    this.compositeDisposable.add(disposableMacro1);
  }

  private void MacroServoSequence(int angle, int speed) {
    final HWActionResponse hwActionResponse;
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest = new HWActionRequest(null);
    ActionRequest actionRequest = new ActionRequest(Component.MACRO_SERVO_2,
      ActionType.MACRO_REACH_POSITION_AT_SPEED);
    actionRequest.setActionParameters(ActionRequestParameters.ANGLE, String.valueOf(angle));
    actionRequest.setActionParameters(ActionRequestParameters.SPEED, String.valueOf(speed));
    hwActionRequest.setRequestParams(actionRequest);
    hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> map = hwActionResponse.getActionResponseData();
    System.out.println("native test " + map);
  }

  private void MicroServoSequence(int angle, int speed, String direction) {
    final HWActionResponse hwActionResponse;
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest = new HWActionRequest(null);
    ActionRequest actionRequest = new ActionRequest(Component.MICRO_SERVO_5,
      ActionType.MICRO_REACH_ANGLE_WITH_SPEED);
    actionRequest.setActionParameters(ActionRequestParameters.ANGLE, String.valueOf(angle));
    actionRequest.setActionParameters(ActionRequestParameters.SPEED, String.valueOf(speed));
    actionRequest.setActionParameters(ActionRequestParameters.DIRECTION, direction);
    hwActionRequest.setRequestParams(actionRequest);
    hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> map = hwActionResponse.getActionResponseData();
    System.out.println("native test " + map);
  }


  private void OilPumpActuate(int speed) {
    final HWActionResponse hwActionResponse;
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest = new HWActionRequest(null);
    final CommandRequest commandRequest = new CommandRequest(Component.OIL_PUMP);
    commandRequest.setParameters(CommandRequestParameters.SPEED, String.valueOf(speed));
    hwActionRequest.setRequestParams(commandRequest);
    hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> map = hwActionResponse.getActionResponseData();
    System.out.println("native test " + map);
  }

  private void WaterPumpActuate(int speed) {
    final HWActionResponse hwActionResponse;
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest = new HWActionRequest(null);
    final CommandRequest commandRequest = new CommandRequest(Component.WATER_PUMP);
    commandRequest.setParameters(CommandRequestParameters.SPEED, String.valueOf(speed));
    hwActionRequest.setRequestParams(commandRequest);
    hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> map = hwActionResponse.getActionResponseData();
    System.out.println("native test " + map);
  }

  private void WaterDispense(int quantity) {
    final HWActionResponse hwActionResponse;
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest = new HWActionRequest(null);
    ActionRequest actionRequest = new ActionRequest(Component.WATER_PUMP,
      ActionType.LIQUID_WATER_DISPENSE);
    actionRequest
      .setActionParameters(ActionRequestParameters.HARDWARE_UNITS, String.valueOf(quantity));
    hwActionRequest.setRequestParams(actionRequest);
    hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> map = hwActionResponse.getActionResponseData();
    System.out.println("native test " + map);
  }

  private void OilDispense(int quantity) {
    final HWActionResponse hwActionResponse;
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest = new HWActionRequest(null);
    ActionRequest actionRequest = new ActionRequest(Component.OIL_PUMP,
      ActionType.LIQUID_OIL_DISPENSE);
    actionRequest
      .setActionParameters(ActionRequestParameters.HARDWARE_UNITS, String.valueOf(quantity));
    hwActionRequest.setRequestParams(actionRequest);
    hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> map = hwActionResponse.getActionResponseData();
    System.out.println("native test " + map);
  }

  private void MicroDispense(int quantity, Component component, ActionType actionType) {
    HWActionResponse hwActionResponse;
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    ActionRequest actionRequest = new ActionRequest(component,
      actionType);
    actionRequest
      .setActionParameters(ActionRequestParameters.HARDWARE_UNITS, String.valueOf(quantity));
    hwActionRequest.setRequestParams(actionRequest);
    hwActionResponse = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> map = hwActionResponse.getActionResponseData();
    System.out.println("native test " + map);
  }
  private void DetectPanAndSetStatus() {
    final Button button = findViewById(R.id.button);
    button.setEnabled(false);
    final AtomicReference<String> panStatus = new AtomicReference<>();
    this.compositeDisposable.add(Observable.just(1).subscribeOn(Schedulers.newThread()).doOnNext(l -> {
      panStatus.set(this.getPanDetectionStatus());
    }).observeOn(AndroidSchedulers.mainThread()).subscribe(l -> {
      final TextView textView = findViewById(R.id.pan_detect_status);
      textView.setText("Pan Status : " + panStatus);
      button.setEnabled(true);
    }));
  }

  private void UpdateInductionStatus() {
    final AtomicReference<String> inductionState = new AtomicReference<>();
    final AtomicReference<String> inductionError = new AtomicReference<>();
    final AtomicReference<String> inductionPowerLevel = new AtomicReference<>();
    Observable.interval(1000, TimeUnit.MILLISECONDS)
      .subscribeOn(Schedulers.newThread())
      .doOnSubscribe(l -> Thread.currentThread().setName("ind status log"))
      .doOnNext(l -> {
        inductionState.set(this.getInductionState());
        inductionError.set(this.getInductionError());
        inductionPowerLevel.set(this.getInductionPowerLevel());

      })
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(l -> {
          TextView textView = this.findViewById(R.id.text_box);
          textView.setText("Induction State : " + inductionState +
            "\n" + "Induction Error : " + inductionError +
            "\n" + "Induction Power Level : " + inductionPowerLevel);
        }
      );
  }

  private String getInductionState() {
    final CommandRequest commandRequest = new CommandRequest(Component.INDUCTION_HEATER);
    commandRequest.setParameters(CommandRequestParameters.INDUCTION_MACHINE_REQ,
      "INDUCTION_STATE");
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest.setRequestParams(commandRequest);
    final HWActionResponse response = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> listMap = response.getActionResponseData();
    return listMap.getOrDefault("COMMAND_RESULT", "null");
  }

  private String getInductionError() {
    final CommandRequest commandRequest = new CommandRequest(Component.INDUCTION_HEATER);
    commandRequest.setParameters(CommandRequestParameters.INDUCTION_MACHINE_REQ,
      "INDUCTION_ERROR");
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest.setRequestParams(commandRequest);
    final HWActionResponse response = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> listMap = response.getActionResponseData();
    return listMap.getOrDefault("COMMAND_RESULT", "null");

  }

  private String getPanDetectionStatus() {
    final CommandRequest commandRequest = new CommandRequest(Component.INDUCTION_HEATER);
    commandRequest.setParameters(CommandRequestParameters.INDUCTION_MACHINE_REQ,
      "INDUCTION_PAN_DETECT");
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest.setRequestParams(commandRequest);
    final HWActionResponse response = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> listMap = response.getActionResponseData();
    return listMap.getOrDefault("COMMAND_RESULT", "null");
  }

  private String getInductionPowerLevel() {
    final CommandRequest commandRequest = new CommandRequest(Component.INDUCTION_HEATER);
    commandRequest.setParameters(CommandRequestParameters.INDUCTION_MACHINE_REQ,
      "INDUCTION_POWER_LEVEL");
    HWActionRequest hwActionRequest = new HWActionRequest(null);
    hwActionRequest.setRequestParams(commandRequest);
    final HWActionResponse response = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> listMap = response.getActionResponseData();
    return listMap.getOrDefault("COMMAND_RESULT", "null");

  }


  @Override
  protected void onDestroy() {
    super.onDestroy();
    this.compositeDisposable.dispose();
  }

  public static Context getAppContext() {
    return context;
  }
}
