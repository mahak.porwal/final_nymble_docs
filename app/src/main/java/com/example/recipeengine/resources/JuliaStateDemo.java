package com.example.recipeengine.resources;


import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

/**
 * This class is a demo container class which triggers
 * {@link CapturableTask} when JuliaState changes.
 */
public class JuliaStateDemo {

  private static String stirrerStatus = "idle";
  private PropertyChangeSupport supportStart;
  private PropertyChangeSupport supportStop;

  private static Map<String, String> juilaStateMap;

  private static Mat transformationMatrix;

  private static JuliaStateDemo juliaStateDemo;

  public static JuliaStateDemo getInstance() {
    if(juliaStateDemo == null) {
      juliaStateDemo = new JuliaStateDemo();
    }

    return juliaStateDemo;
  }

  private JuliaStateDemo() {
    this.supportStart = new PropertyChangeSupport(this);
    this.juilaStateMap = new HashMap<>();
  }

  public void addPropertyChangeListener(PropertyChangeListener pcl) {
    supportStart.addPropertyChangeListener(pcl);
  }

  public void removePropertyChangeListener(PropertyChangeListener pcl) {
    supportStart.removePropertyChangeListener(pcl);
  }

  public void setStirrerStatus(String value) {
    supportStart.firePropertyChange("stirrerStatus", stirrerStatus, value);
    stirrerStatus = value;
  }

  public void addToJuliaState(String key, String value) {
    this.juilaStateMap.put(key, value);
  }

  public static Map<String, String> getJuilaStateMap() {
    return Collections.unmodifiableMap(juilaStateMap);
  }

  public static Mat getTransformationMatrix() {
    transformationMatrix = new Mat(3, 3, CvType.CV_64F);
    double[] matrix = new double[] {
        -0.5161062006494175,
        -0.057310364137946865,
        382.78431776287215,
        -0.023125853065100628,
        0.4530553096256876,
        58.683610301859865,
        -0.00010614189982097482,
        -0.0002425123285727282,
        1.0
    };
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        transformationMatrix.put(i, j, matrix[3 * i + j]);
      }
    }
    return transformationMatrix;
  }
}


