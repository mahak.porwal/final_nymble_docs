package com.example.recipeengine;

public enum GpioInputPins {
  LIMIT_SW1,
  LIMIT_SW2,
  INDUCTIVE_S1,
  INDUCTIVE_S2,
  HEATER_STATUS
}
