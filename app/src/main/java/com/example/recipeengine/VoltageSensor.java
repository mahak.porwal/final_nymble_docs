package com.example.recipeengine;

import com.example.recipeengine.util.Native.NativeUtil;

import java.util.List;
import java.util.Map;

import som.hardware.message.Component;
import som.hardware.message.sensor.HWSensorRequest;
import som.hardware.message.sensor.SensorUnit;
import som.hardware.request.SensorRequest;
import som.hardware.response.SensorResponse;
import timber.log.Timber;

public class VoltageSensor {

  public float sensorVoltage(VoltageSensorInput input) {
    Component component;
    switch (input) {
      case BODY_TEMPERATURE:
        component = Component.BODY_TEMPERATURE;
        break;
      case WATER_TEMPERATURE:
        component = Component.WATER_TEMPERATURE;
        break;
      default:
        return -1;
    }
    return this.readAdc(component);
  }

  private float readAdc(Component component) {
    final SensorRequest sensorRequest = new SensorRequest(null);
    final HWSensorRequest hwSensorRequest = new HWSensorRequest(component, SensorUnit.MILLI_VOLTS);
    sensorRequest.setSensor(hwSensorRequest);
    final SensorResponse sensorResponse = NativeUtil.ExecuteSensorRequest(sensorRequest);
    final Map<String, List<Double>> listMap = sensorResponse.getSensorData();
    final Float sensorValue = listMap.get("SENSOR_VALUE").get(0).floatValue();
    Timber.d("%.4f", sensorValue.floatValue());
    return sensorValue.floatValue();
  }
}
