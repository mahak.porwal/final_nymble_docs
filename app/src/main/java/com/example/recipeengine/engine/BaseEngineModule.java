package com.example.recipeengine.engine;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class BaseEngineModule {
  @Binds
  abstract Engine bindEngine(BaseEngine baseEngine);
}
