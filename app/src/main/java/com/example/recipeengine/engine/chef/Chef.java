package com.example.recipeengine.engine.chef;



import androidx.core.util.Pair;

import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;

import java.util.List;

/**
 * Here is the HLD:
 * \image html Chef_HLD.jpg
*/

public interface Chef extends Interruptable, InterruptableWithError {
  void startCooking();

  List<Pair<Integer, BaseInstStatus>> getInstStatus();
}
