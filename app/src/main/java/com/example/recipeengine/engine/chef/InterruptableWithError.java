package com.example.recipeengine.engine.chef;

public interface InterruptableWithError {
  void interruptWithError(Object error) throws Exception;
}
