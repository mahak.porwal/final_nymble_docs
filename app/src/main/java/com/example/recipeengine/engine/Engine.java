package com.example.recipeengine.engine;



import androidx.core.util.Pair;

import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;

import java.util.List;

import som.hardware.response.Response;
import som.instruction.request.Request;

/**
 * Here is the HLD:
 * \image html Engine_HLD.jpg
*/

public interface Engine {
  List<Pair<Integer, BaseInstStatus>> getExecutionStatus();

  Boolean runHardwareChecks();

  Response executeHwRequest(Request request);

  void startCooking();

  void stopCooking();

  void stopInstruction(Integer instructionId);
}
