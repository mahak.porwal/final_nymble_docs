package com.example.recipeengine.engine;

import com.example.recipeengine.engine.chef.BaseChefModule;
import com.example.recipeengine.instruction.executor.BaseExecutorModule;

import com.example.recipeengine.recipe.reader.RecipeReaderModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {BaseEngineModule.class, BaseChefModule.class, RecipeReaderModule.class,
  BaseExecutorModule.class})
public interface EngineComponent {
  Engine createEngine();
}
