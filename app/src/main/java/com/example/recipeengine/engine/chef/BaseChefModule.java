package com.example.recipeengine.engine.chef;

import com.example.recipeengine.recipe.reader.InstructionProvider;
import com.example.recipeengine.recipe.reader.RecipeReader;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class BaseChefModule {
  @Binds
  abstract InstructionProvider bindInstructionProvider(RecipeReader recipeReader);

  @Binds
  abstract Chef bindChef(BaseChef chef);
}
