package com.example.recipeengine.engine.chef;


import androidx.core.util.Pair;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.executor.factory.ExecutorFactory;
import com.example.recipeengine.instruction.executor.InstructionExecutor;
import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;
import com.example.recipeengine.recipe.reader.InstructionProvider;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.inject.Inject;

/**
 * This class is responsible for executing recipe instructions provided by the
 * {@link InstructionProvider}.
 */
public class BaseChef implements Chef {

  private Map<Integer, InstructionExecutor> currentExecInstructions;
  private InstructionProvider instructionProvider;
  private ExecutorFactory executorFactory;
  private Object error;
  private AtomicBoolean isInterrupted;

  public Object getError() {
    return this.error;
  }

  @Inject
  public BaseChef(InstructionProvider instructionProvider, ExecutorFactory executorFactory) {
    this.currentExecInstructions = new ConcurrentHashMap<>();
    this.instructionProvider = instructionProvider;
    this.executorFactory = executorFactory;
    this.isInterrupted = new AtomicBoolean(false);
  }

  /**
   * EXECUTES A SINGLE INSTRUCTION AT A TIME
   * Gets an instruction from the {@link BaseChef#instructionProvider}.
   * Gets the executor for the instruction from the {@link BaseChef#executorFactory}.
   * Puts the instruction id and the excutor as key and value in the {@link BaseChef#currentExecInstructions}
   * Executes the instruction through the executor
   * Keeps asking the {@link BaseChef#instructionProvider} for the instructions until it gives
   * a null instruction
   * If the {@link BaseChef#error} object becomes null, this means some error occurred, this makes
   * the loop to break
   */
  @Override
  public void startCooking() {
    List<Instruction> instructionList = this.instructionProvider.getNextInstructions();
    while (instructionList != null && !this.isInterrupted.get()) {
      Instruction instruction = instructionList.get(0);
      InstructionExecutor instructionExecutor = this.executorFactory.getExecutor(instruction);
      this.currentExecInstructions.put(instruction.getInstructionId(),
        instructionExecutor);
      instructionExecutor.execute();
      this.currentExecInstructions.remove(instruction.getInstructionId());
      instructionList = this.instructionProvider.getNextInstructions();
    }
  }

  /**
   * For each of the executing instructions, this method returns a pair.
   * The pair's first parameter is the instruction id of the executing instruction and the
   * second parameter is the instruction's execution status in the form of {@link BaseInstStatus}
   *
   * @return List of instruction id and instruction status
   */
  @Override
  public List<Pair<Integer, BaseInstStatus>> getInstStatus() {
    List<Pair<Integer, BaseInstStatus>> instructionStatus = new ArrayList<>();
    for (Map.Entry<Integer, InstructionExecutor> executingInstruction :
      this.currentExecInstructions.entrySet()) {
      Pair pair = new Pair(executingInstruction.getKey(), executingInstruction.getValue().getStatus());
      instructionStatus.add(pair);
    }
    return instructionStatus;
  }

  /**
   * This method pauses an instruction with the passed @param instructionId and
   * deletes it from the {@link BaseChef#currentExecInstructions}
   * In case the passed instruction id is not present in the map, the method simply returns
   *
   * @param instructionId whose execution needs to paused
   */
  @Override
  public void pause(Integer instructionId) throws Exception {
    if (this.currentExecInstructions.containsKey(instructionId)) {
      this.currentExecInstructions.get(instructionId).pause();
      this.currentExecInstructions.remove(instructionId);
    }
  }

  /**
   * This method pauses all the current executing instructions present in
   * {@link BaseChef#currentExecInstructions} and deletes them from it.
   * This pause method sets the {@link BaseChef#isInterrupted} member to true.
   */
  @Override
  public void pause() throws Exception {
    this.isInterrupted.set(true);
    for (Map.Entry<Integer, InstructionExecutor> executingInstruction :
      this.currentExecInstructions.entrySet()) {
      this.pause(executingInstruction.getKey());
    }
    this.currentExecInstructions.clear();
  }

  /**
   * This method is exposed with the {@link InterruptableWithError} interface,
   * to certain external components which can detect errors and
   * interrupt the chef with that error.
   * <p>
   * This method then sets the {@link BaseChef#error} object and uses the public method
   * {@link Interruptable#pause()} to pause all the executing instruction
   *
   * @param error Detected error
   */
  @Override
  public void interruptWithError(Object error) throws Exception {
    this.error = error;
    this.pause();
  }
}
