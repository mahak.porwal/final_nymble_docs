package com.example.recipeengine.engine.chef;

public interface Interruptable {
  void pause(Integer instructionId) throws Exception;

  void pause() throws Exception;
}
