package com.example.recipeengine.engine;


import androidx.core.util.Pair;

import com.example.recipeengine.engine.chef.Chef;
import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;
import com.example.recipeengine.recipe.auditor.RecipeAuditor;
import com.example.recipeengine.recipe.reader.RecipeReader;

import java.util.List;

import javax.inject.Inject;

import som.hardware.response.Response;
import som.instruction.request.Request;

public class BaseEngine implements Engine {

  private Chef chef;
  private RecipeReader recipeReader;
  private RecipeAuditor recipeAuditor;

  @Inject
  public BaseEngine(Chef chef, RecipeReader recipeReader, RecipeAuditor recipeAuditor) {
    this.chef = chef;
    this.recipeAuditor = recipeAuditor;
    this.recipeReader = recipeReader;
  }

  @Override
  public List<Pair<Integer, BaseInstStatus>> getExecutionStatus() {
    return this.chef.getInstStatus();
  }

  //TODO
  @Override
  public Boolean runHardwareChecks() {
    return true;
  }

  //TODO
  @Override
  public Response executeHwRequest(Request request) {
    return null;
  }

  @Override
  public void startCooking() {
    this.chef.startCooking();
  }

  @Override
  public void stopCooking() {
    try {
      this.chef.pause();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void stopInstruction(Integer instructionId) {
    try {
      this.chef.pause(instructionId);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
