package com.example.recipeengine;

public enum VoltageSensorInput {
  WATER_TEMPERATURE,
  BODY_TEMPERATURE
}
