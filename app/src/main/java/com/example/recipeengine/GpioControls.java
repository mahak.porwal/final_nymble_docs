package com.example.recipeengine;

import com.example.recipeengine.util.Native.NativeUtil;

import java.util.List;
import java.util.Map;

import som.hardware.message.Component;
import som.hardware.message.command.CommandRequest;
import som.hardware.message.command.CommandRequestParameters;
import som.hardware.message.sensor.HWSensorRequest;
import som.hardware.request.HWActionRequest;
import som.hardware.request.SensorRequest;
import som.hardware.response.HWActionResponse;
import som.hardware.response.SensorResponse;
import timber.log.Timber;

public class GpioControls {

  public void setGpioState(GpioOutputPins gpioOutputPin, int state) {

    final HWActionRequest hwActionRequest = new HWActionRequest(null);
    final Component component;
    switch (gpioOutputPin) {

      case MUX_A0:
        component = Component.COUNTER_MUX_SELECT_0;
        break;
      case MUX_A1:
        component = Component.COUNTER_MUX_SELECT_1;
        break;
      case FAN_ON_OFF:
        component = Component.EXHAUST_FAN_ON_OFF;
        break;
      case HEATER_ENABLE:
        component = Component.HEATER_ENABLE;
        break;
      default:
        return;
    }
    final CommandRequest commandRequest;
    commandRequest = new CommandRequest(component);
    final String stateValue = 0 == state ? "OFF_STATE" : "ON_STATE";
    commandRequest.setParameters(CommandRequestParameters.COMPONENT_ON_OFF_STATE, stateValue);
    hwActionRequest.setRequestParams(commandRequest);
    Timber.d("%s", commandRequest.getParametersMap().toString());
    final HWActionResponse response = NativeUtil.ExecuteHWActionRequest(hwActionRequest);
    final Map<String, String> listMap = response.getActionResponseData();
    if (null != listMap) {
      Timber.d("%s", listMap.toString());
    }
  }

  public int getGpioState(GpioInputPins gpioInputPin) {
    final SensorRequest sensorRequest = new SensorRequest(null);
    Component component = null;
    switch (gpioInputPin) {

      case LIMIT_SW1:
        component = Component.LIMIT_SWITCH_1;
        break;
      case LIMIT_SW2:
        component = Component.LIMIT_SWITCH_2;
        break;
      case INDUCTIVE_S1:
        component = Component.INDUCTIVE_SENSOR_1;
        break;
      case INDUCTIVE_S2:
        component = Component.INDUCTIVE_SENSOR_2;
        break;
      case HEATER_STATUS:
      default:
        return -1;
    }
    final HWSensorRequest hwSensorRequest = new HWSensorRequest(component, null);
    sensorRequest.setSensor(hwSensorRequest);
    Timber.d("%s",hwSensorRequest.getParametersMap().toString());
    final SensorResponse response = NativeUtil.ExecuteSensorRequest(sensorRequest);
    final Map<String, List<Double>> sensorData = response.getSensorData();
    if (null != sensorData) {
      Timber.d("%s", sensorData.toString());
      final List<Double> list = sensorData.getOrDefault("SENSOR_VALUE", null);
      if (null != list && !list.isEmpty()) {
        return list.get(0).intValue();
      }
    }
    return -1;
  }
}
