package com.example.recipeengine.instruction.task.capture.base;

public interface Task {
  void beginTask();

  void abortTask();

  Thread getThread();

  String getTaskName();
}
