package com.example.recipeengine.instruction.executor.factory;

import com.example.recipeengine.instruction.Instruction;

import dagger.Module;

@Module
public class InstructionExecutorModule {
  private Instruction instruction;

  public InstructionExecutorModule(Instruction instruction) {
    this.instruction = instruction;
  }


}
