package com.example.recipeengine.instruction.adaptor;

import com.example.recipeengine.instruction.dispense.BaseDispense;
import com.example.recipeengine.instruction.dispense.decorator.DispensedQuantityDecorator;
import com.example.recipeengine.instruction.dispense.decorator.IngredientQuantityDecorator;

/**
 * This class is the implementation of {@link InstructionAdaptor},
 * which overrides {@link #applyOneTimeDecoration()} which is responsible for applying decoration
 * on Dispense objects which have attemptNum as 0.
 * and {@link #applyCompensatoryDecoration()} which is responsible for applying decoration
 *  * on Dispense objects which have attemptNum greater than 0.
 */
public class DispenseAdaptor extends InstructionAdaptor<BaseDispense> {

  private BaseDispense dispenseInstruction;

  /**
   * Constructor for {@link DispenseAdaptor}.
   * @param dispenseInstruction the current {@link BaseDispense} instruction
   */
  public DispenseAdaptor(
      BaseDispense dispenseInstruction) {
    super(dispenseInstruction);
    this.dispenseInstruction = dispenseInstruction;
  }

  @Override
  protected BaseDispense applyOneTimeDecoration() {
    dispenseInstruction = new IngredientQuantityDecorator(dispenseInstruction, null, null);
    return dispenseInstruction;
  }

  @Override
  protected BaseDispense applyCompensatoryDecoration() {
    dispenseInstruction = new DispensedQuantityDecorator(dispenseInstruction, null, 0.0);
    return dispenseInstruction;
  }


}
