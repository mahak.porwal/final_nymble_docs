package com.example.recipeengine.instruction.cook;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class CookType {
  public static final String BOILING = "boiling";
  public static final String CONSISTENCY = "consistency";
  public static final String DRY_WET = "dry_wet";
  public static final String FRYING = "frying";
  public static final String MACRO_SIZE_REDUCTION = "macro_size_reduction";
  public static final String WATTAGE = "wattage";
  public static final String TEMPERATURE = "temperature";
  private final String cookType;

  public CookType(@AnnotationCookType String cookType) {
    this.cookType = cookType;
  }

  public String getCookType() {
    return this.cookType;
  }

  @StringDef({BOILING, CONSISTENCY, DRY_WET, FRYING, MACRO_SIZE_REDUCTION, WATTAGE, TEMPERATURE})
  @Retention(RetentionPolicy.SOURCE)
  public @interface AnnotationCookType {
  }
}
