package com.example.recipeengine.instruction.blocks;

import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;

public interface ExecutionBlock {
  BaseInstStatus getStatus();

  void pause() throws Exception;
}
