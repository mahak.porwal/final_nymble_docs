package com.example.recipeengine.instruction.blocks;

import com.example.recipeengine.instruction.task.capture.base.CapturedDataProvider;
import java.util.List;

public interface InstructionHandler extends ExecutionBlock {
  void handle() throws Exception;

  List<CapturedDataProvider> getExecutedTasks();
}
