package com.example.recipeengine.instruction.handler.cook.boiling;

import static org.opencv.core.CvType.CV_16S;
import static org.opencv.core.CvType.CV_32FC1;
import static org.opencv.core.CvType.CV_8UC1;

import android.graphics.Bitmap;
import android.util.Pair;
import com.example.recipeengine.BuildConfig;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.handler.cook.infer.BaseInferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.util.ImageUtil;
import io.reactivex.subjects.PublishSubject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.DualTVL1OpticalFlow;
import org.opencv.video.Video;

/**
 * Core class for detecting boiling.
 *
 * @author Pranshu Gupta
 */
public class BoilingInfer extends BaseInferBlock {

  public static final float MIN_INIT = 1000.0f;
  public static final float MAX_INIT = -1000.0f;
  public static final float FLOW_BOUND = 20.0f;
  public static final float NORM_UPPER_LIMIT = 255.0f;
  public static final double FLOW_LAPLACE_THRESHOLD = 127.5;
  public static final int INPUT_FLOW_IMAGE_SIZE = 180;
  public static final int OUTPUT_FLOW_IMAGE_SIZE = 140;
  public static final int NO_OF_IMAGE_REQUIRED = 5;
  private static final Point INPUT_IMAGE_CROP_ORIGIN = new Point(420, 0);
  private static final int INPUT_IMAGE_CROP_SIZE = 1080;
  private static final int FLOW_IMAGE_CROP_CUT_VAL = 20;
  final DualTVL1OpticalFlow opticalFlow = Video.createOptFlow_DualTVL1();
  private boolean isFirst;
  private Double firstBoilingFlow;

  /**
   * BaseInfer class constructor. Parent class for all vision based sautes.
   *
   * @param recipeContext Contains recipe specific information.
   * @param uiEmitter     Publisher for UI
   * @param dataLogger    Publisher for data logging
   */
  public BoilingInfer(RecipeContext recipeContext,
                      PublishSubject<UIData> uiEmitter,
                      PublishSubject<ProgressData> dataLogger) {
    super(recipeContext, null, uiEmitter, dataLogger);
    this.isFirst = true;
  }

  /**
   * Core method for boiling ability.
   *
   * @param captureData Contains all the captured data
   * @return Inferred result
   */
  @Override
  public InferredResult infer(List<CapturedTaskData> captureData) {
    final List<Double> inferResult = new ArrayList<>();
    CapturedTaskData capturedTaskData = captureData.get(0);
    if (null != captureData) {
      final List<Bitmap> bmps = capturedTaskData.getVisionCamera();

      final UIData uiData = new UIData(null, null);
      final ProgressData progressData = new ProgressData(null, null, null);

      if (BuildConfig.DEBUG && NO_OF_IMAGE_REQUIRED >= bmps.size()) {
        throw new AssertionError("Assertion failed");
      }
      List<Integer> flow_val = new ArrayList<>();
      for (int i = 0; i < bmps.size() - 1; i++) {
        final Mat flow = new Mat();
        final Mat in1 = preProcessInput(bmps.get(i));
        final Mat in2 = preProcessInput(bmps.get(i + 1));
        this.opticalFlow.calc(in1, in2, flow);

        uiData.setImage(bmps.get(i));
        getUiEmitter().onNext(uiData);

        final Mat flow_image = postProcessFlow(flow);
        flow_val.add(getFlowValue(flow_image));
      }

      //finding median of flow vals
      final Double medianFlow = resultProcess(flow_val);
      if (this.isFirst) {
        this.isFirst = false;
        this.firstBoilingFlow = medianFlow;
      }

      final Pair<OutputParams, String> outLog =
          new Pair<>(OutputParams.BOILING_SCORE, String.valueOf(medianFlow));
      progressData.setOutputData(outLog);
      getDataLogger().onNext(progressData);

      if (medianFlow >= 2 * this.firstBoilingFlow) {
        inferResult.add(1.0);   ///Only two state of result either 1 or 0, Boiled or not boiled.
      } else {
        inferResult.add(0.0);
      }
      return new InferredResult(inferResult);
    } else {
      throw new IllegalArgumentException("Capture Data cannont be null");
    }
  }

  /**
   * Preparing captured images for flow calculations.
   *
   * @param bmp input image
   * @return Mat image
   */
  private Mat preProcessInput(Bitmap bmp) {
    bmp = ImageUtil.centerCrop(bmp, INPUT_IMAGE_CROP_ORIGIN, INPUT_IMAGE_CROP_SIZE);
    final Mat img = new Mat();
    Utils.bitmapToMat(bmp, img);
    Imgproc.resize(img, img, new Size(INPUT_FLOW_IMAGE_SIZE, INPUT_FLOW_IMAGE_SIZE));
    Imgproc.cvtColor(img, img, Imgproc.COLOR_RGB2GRAY);
    return img;
  }

  /**
   * Making a observable flow image with bounds.
   *
   * @param flow Output of TVL1 algorithm
   * @return Processed flow image in Mat
   */
  private Mat postProcessFlow(final Mat flow) {
    float max1 = MAX_INIT;
    float min1 = MIN_INIT;
    float max2;

    Mat flow_image = new Mat(flow.rows(), flow.cols(), CV_32FC1);
    for (int i = 0; i < flow.rows(); i++) {                //finding min, max of image 1 in bounds.
      for (int j = 0; j < flow.cols(); j++) {
        final float[] data = new float[2];
        flow.get(i, j, data);
        for (int k = 0; k < data.length; k++) {
          if (FLOW_BOUND < data[k]) {
            data[k] = FLOW_BOUND;
          }
          if (-FLOW_BOUND > data[k]) {
            data[k] = -FLOW_BOUND;
          }
          if (0 > data[k]) {
            data[k] *= data[k];
          }
          data[k] *= (NORM_UPPER_LIMIT / FLOW_BOUND);
        }
        if (max1 < data[0]) {
          max1 = data[0];
        }
        if (min1 > data[0]) {
          min1 = data[0];
        }
      }
    }

    for (int i = 0; i < flow_image.rows(); i++) {                //Making a flow image
      for (int j = 0; j < flow_image.cols(); j++) {
        final float[] data = new float[2];
        final float[] flow_data = new float[1];
        flow.get(i, j, data);
        for (int k = 0; k < data.length; k++) {
          if (FLOW_BOUND < data[k]) {
            data[k] = FLOW_BOUND;
          }
          if (-FLOW_BOUND > data[k]) {
            data[k] = -FLOW_BOUND;
          }
          if (0 > data[k]) {
            data[k] *= data[k];
          }
          data[k] *= (NORM_UPPER_LIMIT / FLOW_BOUND);
        }
        max2 = Math.max(data[0], data[1]);
        flow_data[0] = ((NORM_UPPER_LIMIT * (max2 - min1)) / (max1 - min1));
        flow_image.put(i, j, flow_data);
      }
    }

    flow_image.convertTo(flow_image, CV_8UC1);
    flow_image = centerCrop(flow_image,
        new Point(FLOW_IMAGE_CROP_CUT_VAL, FLOW_IMAGE_CROP_CUT_VAL),
        OUTPUT_FLOW_IMAGE_SIZE);
    return flow_image;
  }

  /**
   * Calculate flow pixels sum.
   *
   * @param flowImage Processed flow image
   * @return sum of bright flow pixels
   */
  private int getFlowValue(final Mat flowImage) {
    //Mat flow_image_c = _cropImage(flow_image);
    final Mat copy = new Mat();
    Imgproc.Laplacian(flowImage, copy, CV_16S, 3);
    final Mat kernal = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(1, 1));
    Imgproc.erode(copy, copy, kernal);
    Core.convertScaleAbs(copy, copy);
    copy.convertTo(copy, CV_32FC1);
    int sum = 0;
    for (int i = 0; i < copy.rows(); i++) {
      for (int j = 0; j < copy.cols(); j++) {
        final float[] data = new float[1];
        copy.get(i, j, data);
        if (FLOW_LAPLACE_THRESHOLD < data[0]) {
          sum += 1;
        }
      }
    }
    return sum;
  }


  /**
   * Finding median of flow values of all image diffs.
   *
   * @param val list of flow values
   * @return Double : median flow value
   */
  private Double resultProcess(List<Integer> val) {
    val.sort(Collections.reverseOrder());
    final int size = val.size();
    final Double median;
    if (0 == size % 2) {
      median = ((double) val.get(size / 2) + (double) val.get(size / (2 + 1)) / 2);
    } else {
      median = (double) val.get(size / 2);
    }
    System.out.printf("Chef Flow median val = %d", median);
    return median;
  }


  /**
   * Center Cropping the Opencv.Mat image
   * Overloaded function of center crop which deals with Mat object only.
   *
   * @param image  input Mat image
   * @param origin Starting point of crop image from input
   * @param size   Size of cropped image
   * @return Cropped image in Mat
   */
  private Mat centerCrop(Mat image, Point origin, int size) {
    final Point secondPoint;
    secondPoint = new Point(origin.x + size, origin.y + size);
    final Rect region_of_interest = new Rect(origin, secondPoint);
    final Mat a = new Mat();
    Imgproc.resize(image.submat(region_of_interest), a, new Size(size, size));
    return a;
  }

  /**
   * Convert Opencv.Mat to Bitmap
   *
   * @param mat opencv mat object
   * @return Bitmap
   */
  private Bitmap _parseBitmap(Mat mat) {
    Bitmap bmp = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
    Utils.matToBitmap(mat, bmp);
    return bmp;
  }


  @Override
  public void pause() {
    //Nothing to close in boiling
  }

  @Override
  public void close() throws IOException {

  }
}
