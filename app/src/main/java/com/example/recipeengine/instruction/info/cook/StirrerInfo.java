package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.instruction.cook.params.stirrer.StirrerParams;

public interface StirrerInfo extends StirrerParams {
  Integer getCycleDurationSeconds();

  Integer getStirringPercent();

  Integer getStirringSpeed();

  String getCycleDurationSecondsString();

  String getStirringPercentString();

  String getStirringSpeedString();

  void setCycleDurationSeconds(Integer cycleDurationSeconds);

  void setStirringPercent(Integer stirringPercent);

  void setStirringSpeed(Integer stirringSpeed);
}
