package com.example.recipeengine.instruction;

import static com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper.getBaseInstructionObjects;

import java.io.IOException;
import org.json.simple.parser.ParseException;

/**
 * Defines the basic methods an object inheriting Instruction abstract class must have.
 *
 * @param <T> takes in generic of type Instruction
 */
public abstract class BaseInstruction<T extends Instruction> implements Instruction<T> {
  private Integer instructionId;
  private Integer attemptNum;

  /**
   * Constructor for BaseInstruction abstract class.
   *
   * @param instructionId   current Instruction's ID
   * @param attemptNum      current Instruction's attempt number
   */
  public BaseInstruction(Integer instructionId, Integer attemptNum) {
    this.instructionId = instructionId;
    this.attemptNum = attemptNum;
  }

  @Override
  public Integer getInstructionId() {
    return instructionId;
  }

  public void setInstructionId(Integer instructionId) {
    this.instructionId = instructionId;
  }

  @Override
  public Integer getAttemptNum() {
    return attemptNum;
  }

  public void incrementAttemptNum() {
    this.attemptNum += 1;
  }

}
