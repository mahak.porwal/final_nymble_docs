package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.cook.CookType;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.info.InstructionInfo;

public interface CookInfo extends InstructionInfo {
  ActionInfo getActionInfo();

  StirrerInfo getStirrerInfo();

  InferParams getInferParams();

  CaptureParams getCaptureParams();

  CookType getCookType();
}
