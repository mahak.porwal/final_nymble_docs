package com.example.recipeengine.instruction.factory.cook;

import com.example.recipeengine.instruction.cook.params.action.ActionParameters;
import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.cookmodules.CookInfoConverter;
import com.example.recipeengine.instruction.info.cook.CookInfo;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

public class ActionParamsFactory {
  @Inject
  public ActionParamsFactory() {
  }

  /**
   * Creates a new {@link ActionParameters} object and returns it to
   * {@link com.example.recipeengine.instruction.cook.CookComponent}.
   *
   * @return {@link ActionParams}
   */
  public ActionParams provideActionParameters(String filename, CookInfo cookInfo) {
    ActionParams actionParams = null;
    if (filename != null) {
      try {
        actionParams = createActionParamsObject(filename);
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ParseException e) {
        e.printStackTrace();
      }
    } else {
      actionParams = CookInfoConverter.provideActionParameters(cookInfo);
    }
    return actionParams;
  }

  private ActionParams createActionParamsObject(String filename) throws IOException, ParseException {
    Object object = new JSONParser().parse(new FileReader(filename));
    JSONObject jsonObject = (JSONObject) object;
    Map address = ((Map) jsonObject.get("actionParams"));

    return new ActionParameters(((Long) address.get("targetTemperature")).intValue(),
      ((Long) address.get("timeToCook")).intValue(),
      (Double) address.get("visualScore"),
      (Double) address.get("thermalScore"),
      ((Long) address.get("defaultWattage")).intValue());
  }
}
