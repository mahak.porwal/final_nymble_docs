package com.example.recipeengine.instruction.info.cook;

public class StirrerInformation implements StirrerInfo {

  private String cycleDurationSecondsString;
  private String stirringPercentString;
  private String stirringSpeedString;

  public StirrerInformation(String cycleDurationSecondsString, String stirringPercentString,
                            String stirringSpeedString) {
    this.cycleDurationSecondsString = cycleDurationSecondsString;
    this.stirringPercentString = stirringPercentString;
    this.stirringSpeedString = stirringSpeedString;
  }


  @Override
  public Integer getCycleDurationSeconds() {
    Double value = Double.parseDouble(this.cycleDurationSecondsString);
    return value.intValue();
  }

  @Override
  public Integer getStirringPercent() {
    Double value = Double.parseDouble(this.stirringPercentString);
    return value.intValue();
  }

  @Override
  public Integer getStirringSpeed() {
    Double value = Double.parseDouble(this.stirringSpeedString);
    return value.intValue();
  }

  @Override
  public String getCycleDurationSecondsString() {
    return this.cycleDurationSecondsString;
  }

  @Override
  public String getStirringPercentString() {
    return this.stirringPercentString;
  }

  @Override
  public String getStirringSpeedString() {
    return this.stirringSpeedString;
  }

  @Override
  public void setCycleDurationSeconds(Integer cycleDurationSeconds) {
    this.cycleDurationSecondsString = String.valueOf(cycleDurationSeconds);
  }

  @Override
  public void setStirringPercent(Integer stirringPercent) {
    this.stirringPercentString = String.valueOf(stirringPercent);
  }

  @Override
  public void setStirringSpeed(Integer stirringSpeed) {
    this.stirringSpeedString = String.valueOf(stirringSpeed);
  }
}
