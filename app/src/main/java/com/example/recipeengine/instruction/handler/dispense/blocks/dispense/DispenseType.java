package com.example.recipeengine.instruction.handler.dispense.blocks.dispense;

import androidx.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Contains Annotations for different types of Dispense.
 */
public class DispenseType {
  public static final String MACRO = "MACRO";
  public static final String MICRO = "MICRO";
  public static final String LIQUID = "LIQUID";
  private String dispenseType;

  /**
   * Constructor for DispenseType.
   *
   * @param dispenseType DispenseType
   */
  public DispenseType(@AnnotationDispenseType String dispenseType) {
    this.dispenseType = dispenseType;
  }

  public String getDispenseType() {
    return dispenseType;
  }

  @StringDef({MACRO, MICRO, LIQUID})
  @Retention(RetentionPolicy.SOURCE)
  public @interface AnnotationDispenseType {

  }
}
