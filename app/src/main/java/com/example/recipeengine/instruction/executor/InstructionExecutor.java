package com.example.recipeengine.instruction.executor;


import com.example.recipeengine.instruction.blocks.ExecutionBlock;

public interface InstructionExecutor extends ExecutionBlock {
  void execute();
}
