package com.example.recipeengine.instruction.blocks;

import com.example.recipeengine.instruction.task.capture.base.Task;
import java.util.List;

public interface SequentialTaskBlock extends Block {

  void startSequentialTasks() throws InterruptedException;

  List<? extends Task> getSequentialTasks();

  Task getExecutingTask();
}
