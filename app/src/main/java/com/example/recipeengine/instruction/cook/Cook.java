package com.example.recipeengine.instruction.cook;

import androidx.annotation.VisibleForTesting;
import com.example.recipeengine.instruction.BaseInstruction;
import com.example.recipeengine.instruction.adaptor.CookAdaptor;
import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.cook.params.stirrer.StirrerParams;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import java.util.List;
import javax.inject.Inject;

/**
 * This class is a blueprint of a Cook Object. It implements {@link BaseCook}
 * and extends {@link BaseInstruction} and it overrides the methods of {@link ActionParams}
 * {@link CaptureParams}, {@link StirrerParams},{@link InferParams} and
 * {@link #adapt()} which calls the{@link CookAdaptor#adapt()} method
 * and returns the adaptedSaute object.
 */
public class Cook extends BaseInstruction<BaseCook> implements BaseCook {

  @VisibleForTesting
  CaptureParams captureParams;
  @VisibleForTesting
  ActionParams actionParams;
  @VisibleForTesting
  StirrerParams stirrerParams;
  @VisibleForTesting
  InferParams inferParams;
  private CookType cookType;
  BaseCook adaptedCook;
  CookAdaptor cookAdaptor;

  /**
   * Constructor of Cook class.
   *
   * @param captureParams contains CaptureParameters
   * @param actionParams  contains ActionParameters
   * @param stirrerParams contains StirrerProfile
   * @param inferParams   contains InferParameters
   */
  @Inject
  public Cook(Integer instructionId, Integer attemptNum, CookType cookType,
              CaptureParams captureParams,
              ActionParams actionParams,
              StirrerParams stirrerParams,
              InferParams inferParams) {
    super(instructionId, attemptNum);
    this.captureParams = captureParams;
    this.actionParams = actionParams;
    this.stirrerParams = stirrerParams;
    this.inferParams = inferParams;
    this.cookType = cookType;
  }

  @Override
  public BaseCook adapt() {
    cookAdaptor = makeCookAdaptorObject(this);
    adaptedCook = cookAdaptor.adapt();
    return adaptedCook;
  }

  @Override
  public Integer getDefaultWattage() {
    return actionParams.getDefaultWattage();
  }

  @Override
  public Integer getTimeToCook() {
    return actionParams.getTimeToCook();
  }

  @Override
  public Double getVisualScore() {
    return actionParams.getVisualScore();
  }

  @Override
  public Integer getTemperature() {
    return actionParams.getTemperature();
  }

  @Override
  public Double getThermalScore() {
    return actionParams.getThermalScore();
  }

  @Override
  public Integer getCycleDurationSeconds() {
    return stirrerParams.getCycleDurationSeconds();
  }

  @Override
  public Integer getStirringPercent() {
    return stirrerParams.getStirringPercent();
  }

  @Override
  public Integer getStirringSpeed() {
    return stirrerParams.getStirringSpeed();
  }


  public CookAdaptor makeCookAdaptorObject(Cook cook) {
    return new CookAdaptor(this);
  }

  @Override
  public CookType getCookType() {
    return this.cookType;
  }

  @Override
  public List<CapturableTask> getCaptureTasks() {
    return captureParams.getCaptureTasks();
  }

  @Override
  public List<String> getSourceTasks() {
    return captureParams.getSourceTasks();
  }

  @Override
  public String getModelId() {
    return inferParams.getModelId();
  }

  @Override
  public String getModelFileName() {
    return inferParams.getModelFileName();
  }
}
