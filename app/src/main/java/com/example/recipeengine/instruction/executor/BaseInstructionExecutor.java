package com.example.recipeengine.instruction.executor;


import com.example.recipeengine.instruction.blocks.ExecutionBlock;
import com.example.recipeengine.instruction.blocks.InstructionHandler;
import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;
import com.example.recipeengine.instruction.rectifier.InstructionRectifier;
import com.example.recipeengine.instruction.validator.InstructionValidator;

import javax.inject.Inject;

public class BaseInstructionExecutor implements InstructionExecutor {

  private ExecutionBlock currentExecutionBlock;
  private final InstructionHandler handler;
  private final InstructionValidator validator;
  private final InstructionRectifier rectifier;

  @Inject
  public BaseInstructionExecutor(InstructionHandler handler, InstructionValidator validator,
                                 InstructionRectifier rectifier) {
    this.handler = handler;
    this.validator = validator;
    this.rectifier = rectifier;
  }

  @Override
  public void execute() {
    this.currentExecutionBlock = this.handler;
    try {
      this.handler.handle();
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (this.validator != null) {
      this.currentExecutionBlock = this.validator;
      if (!this.validator.isValid()) {
        this.currentExecutionBlock = this.rectifier;
        this.rectifier.rectify();
      }
    }
  }

  @Override
  public BaseInstStatus getStatus() {
    return this.currentExecutionBlock.getStatus();
  }

  @Override
  public void pause() throws Exception {
    this.currentExecutionBlock.pause();
  }
}
