package com.example.recipeengine.instruction.blocks.capture;

import com.example.recipeengine.instruction.blocks.Block;
import com.example.recipeengine.instruction.task.capture.base.CapturedDataProvider;
import java.util.List;

public interface CaptureBlock extends Block {
  List<CapturedTaskData> getCapturedData() throws Exception;

  void setExecutedTasks(
      List<CapturedDataProvider> executedTasks);
}
