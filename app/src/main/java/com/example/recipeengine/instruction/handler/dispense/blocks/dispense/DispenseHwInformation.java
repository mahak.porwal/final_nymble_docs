package com.example.recipeengine.instruction.handler.dispense.blocks.dispense;

/**
 * Information related to DispenseHWInformation.
 */
public class DispenseHwInformation {
  private DispenseType hardwareSubsystem;
  private String dispenseMechanism;
  private String hardwareFormQuantity;
  private String hardwareContainer;

  /**
   * Constructor of DispenseHwInformation class.
   *
   * @param hardwareSubsystem hardwareSubsystem
   * @param dispenseMechanism dispenseMechanism
   * @param hardwareFormQuantity hardwareFormQuantity
   * @param hardwareContainer hardwareContainer
   */
  public DispenseHwInformation(
      DispenseType hardwareSubsystem, String dispenseMechanism, String hardwareFormQuantity,
      String hardwareContainer) {
    this.hardwareSubsystem = hardwareSubsystem;
    this.dispenseMechanism = dispenseMechanism;
    this.hardwareFormQuantity = hardwareFormQuantity;
    this.hardwareContainer = hardwareContainer;
  }

  public DispenseType getHardwareSubsystem() {
    return hardwareSubsystem;
  }

  public String getDispenseMechanism() {
    return dispenseMechanism;
  }

  public String getHardwareFormQuantity() {
    return hardwareFormQuantity;
  }

  public String getHardwareContainer() {
    return hardwareContainer;
  }
}
