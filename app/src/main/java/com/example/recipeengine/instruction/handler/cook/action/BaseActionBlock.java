package com.example.recipeengine.instruction.handler.cook.action;

import androidx.annotation.NonNull;

import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;

import java.io.IOException;

import io.reactivex.subjects.PublishSubject;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.actuator.Actuator;
import som.instruction.request.actuator.Actuators;
import som.instruction.request.heat.HeatPowerLevel;

/**
 * This abstract class implements {@link ActionBlock} and overrides
 * its methods. It also contains abstract methods which will be
 * implemented by the concrete implementation of BaseActionBlock.
 */
public abstract class BaseActionBlock implements ActionBlock {

  protected String actionStatus;
  protected String scoreStatus;
  private RequestHandler hwRequestHandler;
  private InferredResult inferredResult;
  private PublishSubject<UIData> uiEmitter;
  private PublishSubject<ProgressData> dataLogger;
  private Integer remainingTimeSec;

  /**
   * Constructor of BaseActionBlock.
   *
   * @param hwRequestHandler request handler object
   * @param uiEmitter        RxJava subject to send results to UI
   * @param dataLogger       RxJava subject to log results
   */
  public BaseActionBlock(RequestHandler hwRequestHandler,
                         PublishSubject<UIData> uiEmitter,
                         PublishSubject<ProgressData> dataLogger) {
    this.hwRequestHandler = hwRequestHandler;
    this.uiEmitter = uiEmitter;
    this.dataLogger = dataLogger;
  }

  @Override
  public abstract void takeAction(InferredResult inferredResult, ActionParams actionParams)
    throws Exception;

  @Override
  public abstract Boolean isInstCompleted();

  @Override
  public abstract void pause() throws Exception;

  @Override
  public String getScoreStatus() {
    return scoreStatus;
  }

  @Override
  public String getBlockStatus() {
    return actionStatus;
  }

  @Override
  public Integer getRemainingTimeSec() {
    return remainingTimeSec;
  }

  protected InferredResult getInferredResult() {
    return inferredResult;
  }

  protected void setInferredResult(
    InferredResult inferredResult) {
    this.inferredResult = inferredResult;
  }

  protected PublishSubject<UIData> getUiEmitter() {
    return uiEmitter;
  }

  protected void setUiEmitter(
    PublishSubject<UIData> uiEmitter) {
    this.uiEmitter = uiEmitter;
  }

  protected PublishSubject<ProgressData> getDataLogger() {
    return dataLogger;
  }

  protected void setDataLogger(
    PublishSubject<ProgressData> dataLogger) {
    this.dataLogger = dataLogger;
  }

  public RequestHandler getHwRequestHandler() {
    return hwRequestHandler;
  }

  protected abstract Integer calcRemainingTimeSec();

  protected void changePowerLevel(@NonNull HeatPowerLevel heatPowerLevel) throws Exception {
    HeatPowerLevel powerLevel = heatPowerLevel;
    final Actuator actuator = new Actuator(null);
    if (HeatPowerLevel.POWER_LEVEL_ONE == powerLevel ||
      HeatPowerLevel.POWER_LEVEL_TWO == powerLevel) {
      powerLevel = HeatPowerLevel.POWER_LEVEL_THREE;
    }
    actuator.setActuator(Actuators.INDUCTION_POWER)
      .setInductionPowerLevel(powerLevel);
    getHwRequestHandler().handleRequest(actuator);
  }
  @Override
  public void close() throws IOException {

  }
}
