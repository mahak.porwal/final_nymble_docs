package com.example.recipeengine.instruction.task.hardware;

import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseHwInformation;
import com.example.recipeengine.instruction.task.capture.base.Task;
import java.util.ArrayList;
import java.util.List;

/**
 * This class provides the Hardware tasks for PostDispenseBlock.
 */
public class PostDispenseTaskProvider implements HardwareTaskProvider {

  @Override
  public List<Task> hwTasksProvider() {
    return new ArrayList<>();
  }

  @Override
  public List<Task> hwTasksProvider(DispenseHwInformation[] dispenseHwInformation) {
    return null;
  }
}
