package com.example.recipeengine.instruction.handler.cook.status;

/**
 * This class stores the information about recipe.
 */
public class RecipeContext {

  private final int servingSize;
  private final String recipeName;

  public RecipeContext(int servingSize, String recipeName) {
    this.servingSize = servingSize;
    this.recipeName = recipeName;
  }

  public int getServingSize() {
    return this.servingSize;
  }

  public String getRecipeName() {
    return this.recipeName;
  }


}
