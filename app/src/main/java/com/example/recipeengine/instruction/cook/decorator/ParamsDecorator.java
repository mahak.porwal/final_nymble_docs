package com.example.recipeengine.instruction.cook.decorator;

import androidx.core.util.Pair;
import com.example.recipeengine.instruction.cook.BaseCook;
import com.example.recipeengine.instruction.decorator.exchanges.InstructionInputParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.decorator.source.InstructionDecoration;
import com.example.recipeengine.instruction.decorator.source.InstructionDecorationMap;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;
import java.util.List;

/**
 * This class extends {@link BaseCookDecorator} and applies OneTime Decoration
 * to a {@link com.example.recipeengine.instruction.cook.Cook} Object.
 * It overrides the methods {@link #getDefaultWattage()} to return the modified
 * defaultWattage, {@link #getTemperature()} to return the modified temperature
 * and {@link #getTimeToCook()} to return modified timeToCook.
 */
public class ParamsDecorator extends BaseCookDecorator {

  private List<InstructionLog> instructionLogs;
  private InstructionDecorationMap instructionDecorationMap;

  /**
   * Constructor for DispensedQuantityDecorator.
   *
   * @param wrappedObject contains the dispense object here.
   * @param instructionLogs log of instructions that want to
   *                       decorate the incoming dispense instruction.
   * @param instructionDecorationMap Contains the actual decorations of parameters
   *                                 and target parameters
   */
  public ParamsDecorator(BaseCook wrappedObject, List<InstructionLog> instructionLogs,
                         InstructionDecorationMap instructionDecorationMap) {
    super(wrappedObject);
    this.instructionLogs = instructionLogs;
    this.instructionDecorationMap = instructionDecorationMap;
  }

  public Integer getTemperature() {
    Integer temperature = wrappedObject.getTemperature() - deltaTargetTemperatureParams();
    return temperature <= Integer.valueOf(0) ? Integer.valueOf(0) : temperature;
  }

  private Integer deltaTargetTemperatureParams() {
    if (instructionDecorationMap == null) {
      return 0;
    }

    Double finalScore = 0.0;
    for (InstructionLog instructionLog : instructionLogs) {
      if (!instructionDecorationMap.getDecorationMap()
          .containsKey(instructionLog.getInstructionId())) {
        continue;
      }
      InstructionDecoration instructionDecoration = instructionDecorationMap
          .getDecorationMap()
          .get(instructionLog.getInstructionId());

      Double score = 0.0;
      for (OutputParams outputParams : instructionDecoration.getParamsDecorationWeight().keySet()) {
        /*
        Bottleneck 1 : No bucket found
        Bottleneck 2 : Output Param does not have TARGET_TEMPERATURE in it's bucket,
                       How ParamDecoration Weight will affect it.
        */
        score += instructionDecoration.getParamsDecorationWeight().get(outputParams)
            * Double.parseDouble(instructionDecoration.getParameters().get(outputParams)
            .get(bucketResolver(outputParams, instructionLog, instructionDecoration))
            .get(InstructionInputParams.TARGET_TEMPERATURE));
      }
      finalScore += instructionDecoration.getInstructionWeight() * score;
    }
    finalScore = wrappedObject.getTemperature() * (1 - finalScore);
    return Math.toIntExact(Math.round(finalScore));
  }

  public Integer getTimeToCook() {
    Integer time = wrappedObject.getTimeToCook() - deltaTimeToCookParams();
    return time <= Integer.valueOf(0) ? Integer.valueOf(0) : time;
  }

  private Integer deltaTimeToCookParams() {
    if (instructionDecorationMap == null) {
      return 0;
    }

    Double finalScore = 0.0;
    for (InstructionLog instructionLog : instructionLogs) {
      if (!instructionDecorationMap.getDecorationMap()
          .containsKey(instructionLog.getInstructionId())) {
        continue;
      }
      InstructionDecoration instructionDecoration = instructionDecorationMap
          .getDecorationMap()
          .get(instructionLog.getInstructionId());

      Double score = 0.0;
      for (OutputParams outputParams : instructionDecoration.getParamsDecorationWeight().keySet()) {
        /*
        Bottleneck 1 : No bucket found
        Bottleneck 2 : Output Param does not have TIME_TO_COOK in it's bucket,
                       How ParamDecoration Weight will affect it.
        */
        score += instructionDecoration.getParamsDecorationWeight().get(outputParams)
            * Double.parseDouble(instructionDecoration.getParameters().get(outputParams)
            .get(bucketResolver(outputParams, instructionLog, instructionDecoration))
            .get(InstructionInputParams.TIME_TO_COOK));
      }
      finalScore += instructionDecoration.getInstructionWeight() * score;
    }
    finalScore = wrappedObject.getTimeToCook() * (1 - finalScore);
    return Math.toIntExact(Math.round(finalScore));
  }

  public Integer getDefaultWattage() {
    Integer wattage = wrappedObject.getDefaultWattage() - deltaDefaultWattageParams();
    return wattage <= Integer.valueOf(0) ? Integer.valueOf(0) : wattage;
  }

  private Integer deltaDefaultWattageParams() {
    if (instructionDecorationMap == null) {
      return 0;
    }

    Double finalScore = 0.0;
    for (InstructionLog instructionLog : instructionLogs) {
      if (!instructionDecorationMap.getDecorationMap()
          .containsKey(instructionLog.getInstructionId())) {
        continue;
      }
      InstructionDecoration instructionDecoration = instructionDecorationMap
          .getDecorationMap()
          .get(instructionLog.getInstructionId());

      Double score = 0.0;
      for (OutputParams outputParams : instructionDecoration.getParamsDecorationWeight().keySet()) {
        /*
        Bottleneck 1 : No bucket found
        Bottleneck 2 : Output Param does not have DEFAULT_WATTAGE in it's bucket,
                       How ParamDecoration Weight will affect it.
        */
        score += instructionDecoration.getParamsDecorationWeight().get(outputParams)
            * Double.parseDouble(instructionDecoration.getParameters().get(outputParams)
            .get(bucketResolver(outputParams, instructionLog, instructionDecoration))
            .get(InstructionInputParams.DEFAULT_WATTAGE));
      }
      finalScore += instructionDecoration.getInstructionWeight() * score;
    }
    finalScore = wrappedObject.getDefaultWattage() * (1 - finalScore);
    return Math.toIntExact(Math.round(finalScore));
  }

  /**
   * This method is responsible to find the right bucket needed to find
   * the right target param value.
   * 1. Extract the buckets by going through the Discretization Map
   * which contains all the values ranges for an OutputParam
   * 2. Get the pair of values for that particular bucket
   * 3. Read the actual observed value from the instruction log and find out under which
   * bucket it falls
   * 4. Once found return the bucket name.
   * 5. Else return a null value.
   *
   * @return String This returns which bucket is selected based on
   *     the values present in instruction Log
   */
  private String bucketResolver(OutputParams outputParams, InstructionLog instructionLog,
                                InstructionDecoration instructionDecoration) {
    for (String bucket : instructionDecoration.getDiscretizationMap().get(outputParams).keySet()) {
      Pair<Double, Double> pair =
          instructionDecoration.getDiscretizationMap().get(outputParams).get(bucket);
      if (pair.first <= Double.parseDouble(instructionLog.getOutputParams().get(outputParams))
          && pair.second > Double.parseDouble(instructionLog.getOutputParams().get(outputParams))) {
        return bucket;
      }
    }
    return null;
  }
}
