package com.example.recipeengine.instruction.handler.dispense.blocks.predispense;

import com.example.recipeengine.instruction.handler.dispense.base.BaseSequentialBlock;
import com.example.recipeengine.instruction.task.capture.base.Task;

import java.io.IOException;
import java.util.List;

/**
 * This class contains tasks to be run before Dispense.
 */
public class PreDispenseBlock extends BaseSequentialBlock {
  private List<? extends Task> sequentialTasks;
  private String blockStatus;

  /**
   * Constructor of PostDispenseBlock.
   *
   * @param sequentialTasks List of Tasks
   */
  public PreDispenseBlock(
      List<? extends Task> sequentialTasks) {
    super(sequentialTasks);
    this.sequentialTasks = sequentialTasks;
    this.blockStatus = "Pre Dispense Block";
  }

  @Override
  public String getBlockStatus() {
    return blockStatus;
  }

  @Override
  public List<? extends Task> getSequentialTasks() {
    return sequentialTasks;
  }
}
