package com.example.recipeengine.instruction.cook.params.capture;

import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import java.util.List;

public interface CaptureParams {

  List<CapturableTask> getCaptureTasks();

  List<String> getSourceTasks();
}
