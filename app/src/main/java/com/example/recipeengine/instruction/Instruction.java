package com.example.recipeengine.instruction;

import com.example.recipeengine.instruction.adaptor.Adaptor;

/**
 * This interface contains the required methods of {@link Instruction} object.
 *
 * @param <T> {@link Instruction}
 */
public interface Instruction<T extends Instruction> extends Adaptor<T> {
  /**
   * Returns instructionId value.
   *
   * @return Integer current Instruction object ID
   */

  Integer getInstructionId();
  /**
   * Returns attemptNum value.
   *
   * @return Integer current Instruction object Attempt Number
   */

  Integer getAttemptNum();


  /**
   * Helper method to increment attempt number after it has been decorated.
   */
  void incrementAttemptNum();
}
