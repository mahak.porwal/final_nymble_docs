package com.example.recipeengine.instruction.info.dispense;

import com.example.recipeengine.instruction.factory.dispense.DispenseFactory;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;

import org.json.simple.JSONObject;

import javax.inject.Inject;

public class PreDispenseBlockBuilder {
  @Inject
  public PreDispenseBlockBuilder() {
  }

  public PreDispenseBlock providePreDispenseInfo(JSONObject instructionJSON) {
    return DispenseFactory.resolvePreDispenseBlock(instructionJSON);
  }
}
