package com.example.recipeengine.instruction.executor.factory;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.blocks.InstructionHandler;
import com.example.recipeengine.instruction.executor.BaseInstructionExecutor;
import com.example.recipeengine.instruction.executor.InstructionExecutor;
import com.example.recipeengine.instruction.handler.HandlerFactory;

import com.example.recipeengine.instruction.rectifier.InstructionRectifier;
import com.example.recipeengine.instruction.rectifier.RectifierFactory;
import com.example.recipeengine.instruction.validator.InstructionValidator;
import com.example.recipeengine.instruction.validator.ValidatorFactory;

import javax.inject.Inject;

public class BaseExecutorFactory implements ExecutorFactory {
  private HandlerFactory handlerFactory;
  private ValidatorFactory validatorFactory;
  private RectifierFactory rectifierFactory;

  @Inject
  public BaseExecutorFactory(HandlerFactory handlerFactory, ValidatorFactory validatorFactory,
                             RectifierFactory rectifierFactory) {
    this.handlerFactory = handlerFactory;
    this.validatorFactory = validatorFactory;
    this.rectifierFactory = rectifierFactory;
  }

  @Override
  public InstructionExecutor getExecutor(Instruction instruction) {
    InstructionHandler instructionHandler = this.handlerFactory.createInstructionHandler(instruction);
    InstructionValidator instructionValidator = this.validatorFactory.createInstructionValidator();
    InstructionRectifier instructionRectifier = this.rectifierFactory.createInstructionRectifier();
    return new BaseInstructionExecutor(instructionHandler, instructionValidator, instructionRectifier);
  }
}
