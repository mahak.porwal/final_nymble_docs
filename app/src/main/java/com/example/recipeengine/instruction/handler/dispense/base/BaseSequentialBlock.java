package com.example.recipeengine.instruction.handler.dispense.base;

import com.example.recipeengine.instruction.blocks.SequentialTaskBlock;
import com.example.recipeengine.instruction.task.capture.base.Task;

import java.io.IOException;
import java.util.List;

/**
 * This class implements the methods of {@link SequentialTaskBlock}.
 * It is responsible for running a list of tasks sequentially.
 */
public abstract class BaseSequentialBlock implements SequentialTaskBlock {

  private List<? extends Task> sequentialTasks;
  private Task executingTask;

  /**
   * Constructor of BaseSequential Block.
   *
   * @param sequentialTasks list of tasks
   */
  public BaseSequentialBlock(
      List<? extends Task> sequentialTasks) {
    this.sequentialTasks = sequentialTasks;
  }

  /**
   * Runs tasks sequentially by calling{@link Task#beginTask()}.
   * method.
   *
   * @throws InterruptedException Exception
   */
  @Override
  public void startSequentialTasks() throws InterruptedException {
    for (Task task : sequentialTasks) {
      executingTask = task;
      task.beginTask();
      task.getThread().join();
    }
  }

  @Override
  public List<? extends Task> getSequentialTasks() {
    return sequentialTasks;
  }

  @Override
  public Task getExecutingTask() {
    return executingTask;
  }

  public void setExecutingTask(Task executingTask) {
    this.executingTask = executingTask;
  }

  public abstract String getBlockStatus();

  @Override
  public void pause() throws Exception {
    executingTask.getThread().interrupt();
  }
  @Override
  public void close() throws IOException {

  }
}
