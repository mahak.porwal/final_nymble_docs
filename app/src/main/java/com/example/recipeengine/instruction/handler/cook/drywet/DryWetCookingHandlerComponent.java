package com.example.recipeengine.instruction.handler.cook.drywet;

import com.example.recipeengine.instruction.handler.cook.handlermodules.DryWetHandlerModule;
import dagger.Component;

@Component(modules = DryWetHandlerModule.class)
public interface DryWetCookingHandlerComponent {
  DryWetCookingHandler buildDryWetHandlerObject();
}
