package com.example.recipeengine.instruction.handler.cook.temperature;

public class PID {

    private final double kp;
    private final double ki;
    private final double kd;
    private double lastError;
    private int targetValue;
    private int currentValue;
    private double integral;
    private final static double PID_CAP = 1400;

    public PID(double kproportional, double kintegral, double kderivative)
    {
        this.kp = kproportional;
        this.ki = kintegral;
        this.kd = kderivative;
    }



    public void setTargetValue(int targetValue) {
        this.targetValue = targetValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    public void setIntegral(double val){
        this.integral = val;
    }

    public double getIntegral(){
        return this.integral;
    }


    /**
     *	PID control function
     *	parmas: Null
     *	ret: pid_output signed integer type
     */
    public double pidLoop(){

        final double error = calcError(this.currentValue, this.targetValue);
        final double proportional = error * this.kp;
        this.integral += error;
        final double derivative = error - this.lastError;
        final double output = (proportional + this.ki * this.integral + this.kd * derivative);
        this.lastError = error;
        if(PID_CAP < output || 0 > output){
            this.integral -= error;
        }
        return output;
    }

    public void resetPID(){
        this.integral = 0;
    }


    /**
     * Calculate error
     */
    private double calcError(int currVal, int targetVal){

        final int alpha = 2;
        if((currVal > targetVal- alpha) && (currVal < targetVal + alpha)){
            return 0;
        }
        else if(currVal > targetVal + alpha){

            return targetVal - alpha -currVal;
        }
        else{
            return targetVal + alpha - currVal;
        }
        //return (targetVal-currVal)*1.0;
    }
}
