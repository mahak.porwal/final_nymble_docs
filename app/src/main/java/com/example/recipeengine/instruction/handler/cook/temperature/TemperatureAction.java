package com.example.recipeengine.instruction.handler.cook.temperature;

import androidx.annotation.NonNull;

import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.handler.cook.action.BaseActionBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.util.FileUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicLong;

import io.reactivex.subjects.PublishSubject;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.actuator.Actuator;

import som.instruction.request.heat.HeatPowerLevel;
import timber.log.Timber;

public class TemperatureAction extends BaseActionBlock {

  public static final double PID_TO_TARGET_KINTEGRAL = 1.5;
  public static final int MAX_INDUCTION_WATTAGE = 1400;
  private final PID tempNormalizePid;
  private final PID controlTempPid;
  private int tempControlState;
  private int lastInductionPower;
  private int lastPIDOutput;
  private final AtomicLong startTime;
  private volatile boolean isCompleted;
  private long timerLog;
  final int INIT_HEATER = 101;
  final int TARGET_TEMP_NORMALIZE = 102;
  final int CONTROL_TEMPERATURE = 103;
  final int DEINIT_HEATER = 104;
  final int PAUSE_HEATER = 105;
  final int IDLE_STATE = 106;

  /**
   * Constructor of BaseActionBlock.
   *
   * @param hwRequestHandler request handler object
   * @param uiEmitter        RxJava subject to send results to UI
   * @param dataLogger       RxJava subject to log results
   */
  public TemperatureAction(RequestHandler hwRequestHandler, PublishSubject<UIData> uiEmitter,
                           PublishSubject<ProgressData> dataLogger) {
    super(hwRequestHandler, uiEmitter, dataLogger);
    this.tempControlState = INIT_HEATER;
    this.startTime = new AtomicLong(0);
    this.tempNormalizePid = new PID(10, PID_TO_TARGET_KINTEGRAL, 0);
    this.controlTempPid = new PID(10, 5.0, 0.1);
    this.writeControlFileHeaders();
  }

  @Override
  public void takeAction(@NonNull InferredResult inferredResult, ActionParams actionParams) throws Exception {
    final List<Double> inferResult = inferredResult.getResult();
    if (null == inferResult) {
      throw new Exception("Null infer result");
    }
    final Double foodTemperature = inferResult.isEmpty() ? null : inferResult.get(0);
    if (null == foodTemperature) {
      throw new Exception("Food temperature not present at index zero of inferred result");
    }
    final Double foodTempSd = 2 > inferResult.size() ? null : inferResult.get(1);
    if (null == foodTempSd) {
      throw new Exception("Food temperature standard deviation not present at index one of inferred result");
    }

    final Double panTemperature = 3 > inferResult.size() ? null : inferResult.get(2);
    if (null == panTemperature) {
      throw new Exception("Food temperature not present at index two of inferred result");
    }
    final Double panTempSd = 4 > inferResult.size() ? null : inferResult.get(3);
    if (null == panTempSd) {
      throw new Exception("Food temperature standard deviation not present at index three of inferred result");
    }

    switch (this.tempControlState) {

      case INIT_HEATER: {
        this.tempNormalizePid.setTargetValue(actionParams.getTemperature());
        this.controlTempPid.setTargetValue(actionParams.getTemperature());
        this.lastInductionPower = HeatPowerLevel.POWER_LEVEL_THREE.getPowerLevelWattage();
        this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_THREE);
        this.timerLog = System.currentTimeMillis();
        this.tempControlState = TARGET_TEMP_NORMALIZE;
      }
      break;
      case TARGET_TEMP_NORMALIZE: {

        this.tempNormalizePid.setCurrentValue(foodTemperature.intValue());
        int pidOutput = (int) Math.floor(this.tempNormalizePid.pidLoop());
        Timber.d("Raw pid output %d", pidOutput);
        if (!(0 <= pidOutput)) {
          pidOutput = -1;
        } else if (MAX_INDUCTION_WATTAGE < pidOutput) {
          pidOutput = HeatPowerLevel.POWER_LEVEL_SEVEN
            .getPowerLevelWattage();
        }
        Timber.d("Normalize pid output %d", pidOutput);
        final int quantizedPower = this.quantizePower(pidOutput);
        Timber.d("Normalize quantized output %d", quantizedPower);
        this.lastPIDOutput = pidOutput;
        if (-1 != quantizedPower && this.lastInductionPower != quantizedPower) {

          final HeatPowerLevel heatPowerLevel = Actuator.createHeatLevel(quantizedPower);
          this.changePowerLevel(heatPowerLevel);
          this.lastInductionPower = quantizedPower;
        }

        _writeControlToFile(foodTemperature * 1.0, foodTempSd, panTemperature * 1.0,
          panTempSd, this.lastInductionPower);

        if (foodTemperature > actionParams.getTemperature() * 0.95) {
          Timber.d("Chef Start time set");
          this.lastPIDOutput = 0;
          this.startTime.set(System.currentTimeMillis());
          this.tempControlState = CONTROL_TEMPERATURE;
          this.controlTempPid.setIntegral((0.3) * this.tempNormalizePid.getIntegral());
          _writeControlToFile(0, 0, 0, 0, 0);
        }
      }
      break;
      case CONTROL_TEMPERATURE: {
        this.controlTempPid.setCurrentValue(foodTemperature.intValue());
        int pidOutput = (int) Math.floor(this.controlTempPid.pidLoop());
        if (!(0 <= pidOutput)) {
          pidOutput = -1;
        } else if (MAX_INDUCTION_WATTAGE < pidOutput) {
          pidOutput = HeatPowerLevel.POWER_LEVEL_SEVEN
            .getPowerLevelWattage();
        }
        Timber.d("Control pid output %d", pidOutput);
        final int quantizedPower = this.quantizePower(pidOutput);
        Timber.d("Control quantized output %d", quantizedPower);
        this.lastPIDOutput = pidOutput;
        if (-1 != quantizedPower && this.lastInductionPower != quantizedPower) {
          final HeatPowerLevel heatPowerLevel = Actuator.createHeatLevel(quantizedPower);
          this.changePowerLevel(heatPowerLevel);
          this.lastInductionPower = quantizedPower;
        }
        _writeControlToFile(foodTemperature * 1.0, foodTempSd, panTemperature, panTempSd,
          this.lastInductionPower);
        final long timeElapsed = System.currentTimeMillis() - this.startTime.get();
        if (timeElapsed > actionParams.getTimeToCook() * 1000) {
          this.tempControlState = DEINIT_HEATER;
          this.isCompleted = true;
        }
      }
      break;
      case DEINIT_HEATER:
        break;
      case IDLE_STATE:
        break;
    }
  }

  @Override
  public Boolean isInstCompleted() {
    return this.isCompleted;
  }

  @Override
  public void pause() throws Exception {

  }

  @Override
  protected Integer calcRemainingTimeSec() {
    return null;
  }

  /**
   * Hysteresis applied to the input power level
   *
   * @param input input power
   * @return output power
   */
  private int quantizePower(final int input) {
    int quantizedPower = -1;
    if (0 > input) {
      quantizedPower = HeatPowerLevel.POWER_LEVEL_ONE.getPowerLevelWattage();
    } else if (225 > input) {
      quantizedPower = HeatPowerLevel.POWER_LEVEL_TWO.getPowerLevelWattage();
    } else if ((0 == ((int) Math.floor(input / 100.0)) % 2 &&
      25 < (input % 100)) || (((int) Math.floor(input / 100.0)) % 2 == 1 &&
      25 > (input % 100))) {

      //Deadzone encountered -> Check if a valid zone was skipped
      if (100 <= Math.abs(input - this.lastPIDOutput)) {
        final int temp = (input / 100) / 2;
        quantizedPower = HeatPowerLevel.POWER_LEVEL_ONE.getPowerLevelWattage() +
          temp * 200;
        Timber.d("Chef to return %d", quantizedPower);
        if (quantizedPower > HeatPowerLevel.POWER_LEVEL_SEVEN.getPowerLevelWattage()) {
          quantizedPower = HeatPowerLevel.POWER_LEVEL_SEVEN.getPowerLevelWattage();
        }
      }
    } else if (input >= 325 && input <= 425) {
      //return Constants.Set_Power_Level_2;
      quantizedPower = HeatPowerLevel.POWER_LEVEL_TWO.getPowerLevelWattage();
    } else if (input >= 525 && input <= 625) {
      //return Constants.Set_Power_Level_3;
      quantizedPower = HeatPowerLevel.POWER_LEVEL_THREE.getPowerLevelWattage();
    } else if (input >= 725 && input <= 825) {
      //return Constants.Set_Power_Level_4;
      quantizedPower = HeatPowerLevel.POWER_LEVEL_FOUR.getPowerLevelWattage();
    } else if (input >= 925 && input <= 1025) {
      //return Constants.Set_Power_Level_5;
      quantizedPower = HeatPowerLevel.POWER_LEVEL_FIVE.getPowerLevelWattage();
    } else if (input >= 1125 && input <= 1225) {
      //return Constants.Set_Power_Level_6;
      quantizedPower = HeatPowerLevel.POWER_LEVEL_SIX.getPowerLevelWattage();
    } else if (input > 1225) {
      //return Constants.Set_Power_Level_7;
      quantizedPower = HeatPowerLevel.POWER_LEVEL_SEVEN.getPowerLevelWattage();
    }
    return quantizedPower;
  }

  private void _writeControlToFile(final double tempMat, final double standardDeviation,
                                   final double panTemp, final double panTempSD,
                                   final int currentPowerLevel) {
    final File file = FileUtil.getTempControlDataFile();

    try (final FileWriter writer = new FileWriter(file, true)) {
      final StringBuilder b = new StringBuilder();
      String value;
      value = String.format(Locale.getDefault(), "%.3f, %.4f, %.4f, %.4f, %.4f, %d",
        (System.currentTimeMillis() - this.timerLog) / 1000.0, tempMat, standardDeviation, panTemp, panTempSD,
        currentPowerLevel);

      b.append(value);
      writer.append(b.append('\n').toString());
      //writer.append("\n\n");
      writer.flush();
    } catch (IOException e) {
      Timber.e(e, "Error writing data to file");
    }
  }

  private void writeControlFileHeaders() {
    final File file = FileUtil.getTempControlDataFile();
    try (final FileWriter writer = new FileWriter(file, true)) {
      final StringBuilder b = new StringBuilder();
      String value;
      value = String.format(Locale.getDefault(), "Inst Time(sec), Food Temp, Food SD, Pan Temp, Pan SD, Power Lvl");
      b.append(value);
      writer.append(b.append('\n').toString());
    } catch (IOException e) {
      Timber.e(e);
    }
  }
}
