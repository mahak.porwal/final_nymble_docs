package com.example.recipeengine.instruction.validator;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;

import javax.inject.Inject;

public class ValidatorFactory {
  @Inject
  public ValidatorFactory() {
  }

  public InstructionValidator createInstructionValidator() {
    return new InstructionValidator() {
      @Override
      public boolean isValid() {
        return true;
      }

      @Override
      public BaseInstStatus getStatus() {
        return null;
      }

      @Override
      public void pause() throws Exception {

      }
    };
  }
}
