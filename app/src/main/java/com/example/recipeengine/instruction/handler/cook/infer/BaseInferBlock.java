package com.example.recipeengine.instruction.handler.cook.infer;

import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import io.reactivex.subjects.PublishSubject;
import java.util.List;

/**
 * This abstract class implements {@link InferBlock} and overrides
 * its methods. It also contains abstract methods which will be
 * implemented by the concrete implementation of BaseInferBlock.
 */
public abstract class BaseInferBlock implements InferBlock {

  protected String inferStatus;
  private RecipeContext recipeContext;
  private InferParams inferParams;
  private PublishSubject<UIData> uiEmitter;
  private PublishSubject<ProgressData> dataLogger;

  /**
   * Constructor for BaseInferBlock.
   *
   * @param recipeContext information about recipe
   * @param inferParams   {@link InferParams} object
   * @param uiEmitter     RxJava subject to send results to UI
   * @param dataLogger    RxJava subject to log results
   */
  public BaseInferBlock(RecipeContext recipeContext,
                        InferParams inferParams,
                        PublishSubject<UIData> uiEmitter,
                        PublishSubject<ProgressData> dataLogger) {
    this.recipeContext = recipeContext;
    this.inferParams = inferParams;
    this.uiEmitter = uiEmitter;
    this.dataLogger = dataLogger;
  }

  @Override
  public abstract void pause();

  @Override
  public String getBlockStatus() {
    return inferStatus;
  }

  protected void setBlockStatus(String inferStatus) {
    this.inferStatus = inferStatus;
  }

  @Override
  public abstract InferredResult infer(List<CapturedTaskData> capturedTaskData) throws Exception;

  protected RecipeContext getRecipeContext() {
    return recipeContext;
  }

  protected void setRecipeContext(RecipeContext recipeContext) {
    this.recipeContext = recipeContext;
  }

  protected InferParams getInferParams() {
    return inferParams;
  }

  protected void setInferParams(
      InferParams inferParams) {
    this.inferParams = inferParams;
  }

  protected PublishSubject<UIData> getUiEmitter() {
    return uiEmitter;
  }

  protected void setUiEmitter(PublishSubject<UIData> uiEmitter) {
    this.uiEmitter = uiEmitter;
  }

  protected PublishSubject<ProgressData> getDataLogger() {
    return dataLogger;
  }

  protected void setDataLogger(PublishSubject<ProgressData> dataLogger) {
    this.dataLogger = dataLogger;
  }
}
