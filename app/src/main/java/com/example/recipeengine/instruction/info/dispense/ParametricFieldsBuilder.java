package com.example.recipeengine.instruction.info.dispense;

import com.example.recipeengine.util.MxEquationResolver;

import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class ParametricFieldsBuilder {
  @Inject
  public ParametricFieldsBuilder() {
  }

  public Map<String, String> provideParametricFields(JSONObject jsonObject) {

    String quantityInfo = (String) jsonObject.get("quantityInGrams");
    Map<String, String> parametricFields = new HashMap<>();

    if (!MxEquationResolver.isResolvedExpression(quantityInfo)) {
      parametricFields.put("quantityInGrams", quantityInfo);
    }
    return parametricFields;
  }
}
