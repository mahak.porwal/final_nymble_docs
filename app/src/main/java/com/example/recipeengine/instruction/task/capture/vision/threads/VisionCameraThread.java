package com.example.recipeengine.instruction.task.capture.vision.threads;

import android.graphics.Bitmap;
import android.util.Log;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import java.util.List;
import som.hardware.request.handler.RequestHandler;
import som.hardware.response.CameraResponse;
import som.instruction.request.Request;
import som.instruction.request.visionCamera.VisionCamera;
import som.instruction.request.visionCamera.VisionCameraCommand;

/**
 * This class is responsible for making request to Vision Camera to
 * collect still RGB images.
 * {@link #run()} contains the logic to capture vision images.
 */
public class VisionCameraThread extends Thread {

  private SensorParams sensorParams;
  private List<Bitmap> visionImages;
  private RequestHandler requestHandler;

  /**
   * Constructor of VisionCameraThread class.
   *
   * @param sensorParams   Contains all the required params related to sensor
   * @param visionImages   A list to store images in the form of Bitmap
   * @param requestHandler {@link RequestHandler} object to make a request.
   */
  public VisionCameraThread(String name, SensorParams sensorParams, List<Bitmap> visionImages,
                            RequestHandler requestHandler) {
    super(name);
    this.sensorParams = sensorParams;
    this.visionImages = visionImages;
    this.sensorParams = sensorParams;
  }

  /**
   * This function contains the logic of capturing Vision Images.
   * 1. First we run a for loop for how many samples we are trying to
   * collect, it is present in {@link SensorParams} object.
   * 2. Then we create an object {@link VisionCamera} and set the command to
   * STILL_CAPTURE.
   * 3. Then we call the {@link RequestHandler#handleRequest(Request)} method
   * to execute the given request and return the {@link CameraResponse}.
   * 4. The camera response gives us a Bitmap image which is then stored inside
   * the {@link #visionImages} object.
   * 5. The current thread is put to sleep for a specific amount of time
   * mentioned in the sensorParams.
   */

  @Override
  public void run() {

    System.out.println(
        Thread.currentThread().getName() + " " + Thread.currentThread().getId() + " "
            + Thread.currentThread().getPriority());
    requestHandler = new RequestHandler();
    for (int i = 0; i < sensorParams.getNumOfSamples(); i++) {
      VisionCamera visionCamera = new VisionCamera(null);
      visionCamera.setCameraCommand(VisionCameraCommand.STILL_CAPTURE);
      CameraResponse response;
      try {
        long startTime = System.currentTimeMillis();
        //Log.d("TESTING_IMAGE","Start time of Vision Camera " + String.valueOf(startTime));
        response = (CameraResponse) requestHandler.handleRequest(visionCamera);
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        //Log.d("TESTING_IMAGE","The total time for Vision Camera Thread : " + totalTime);
        visionImages.add(response.getImage());
      } catch (Exception e) {
        e.printStackTrace();
      }
      if (i != sensorParams.getNumOfSamples() - 1) {
        try {
          Thread.sleep(sensorParams.getTimeBetweenCaptureMS());
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

    //System.out.println("The total time for Vision Camera Thread : " + totalTime);
  }
}
