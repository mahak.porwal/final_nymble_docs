package com.example.recipeengine.instruction.cook.decorator;

import com.example.recipeengine.instruction.cook.BaseCook;

import com.example.recipeengine.instruction.cook.CookType;

import com.example.recipeengine.instruction.task.capture.base.CapturableTask;

import java.util.List;

/**
 * It is the decorator implementation of {@link BaseCook} which contains
 * a {@link BaseCook} object and overrides its methods.
 */
/**
 * Here is the HLD:
 * \image html Decorator_HLD.jpg width=80%
*/
public class BaseCookDecorator implements BaseCook {

  protected BaseCook wrappedObject;

  public BaseCookDecorator(BaseCook wrappedObject) {
    this.wrappedObject = wrappedObject;
  }

  @Override
  public Integer getInstructionId() {
    return wrappedObject.getInstructionId();
  }

  @Override
  public Integer getAttemptNum() {
    return wrappedObject.getAttemptNum();
  }

  @Override
  public void incrementAttemptNum() {
    wrappedObject.incrementAttemptNum();
  }

  @Override
  public BaseCook adapt() {
    return wrappedObject.adapt();
  }

  @Override
  public Integer getDefaultWattage() {
    return wrappedObject.getDefaultWattage();
  }

  @Override
  public Integer getTimeToCook() {
    return wrappedObject.getTimeToCook();
  }

  @Override
  public Double getVisualScore() {
    return wrappedObject.getVisualScore();
  }

  @Override
  public Integer getTemperature() {
    return wrappedObject.getTemperature();
  }

  @Override
  public Double getThermalScore() {
    return wrappedObject.getThermalScore();
  }


  @Override
  public Integer getCycleDurationSeconds() {
    return wrappedObject.getCycleDurationSeconds();
  }

  @Override
  public Integer getStirringPercent() {
    return wrappedObject.getStirringPercent();
  }

  @Override
  public Integer getStirringSpeed() {
    return wrappedObject.getStirringSpeed();
  }

  @Override
  public CookType getCookType() {
    return wrappedObject.getCookType();
  }

  @Override
  public List<CapturableTask> getCaptureTasks() {
    return wrappedObject.getCaptureTasks();
  }

  @Override
  public List<String> getSourceTasks() {
    return wrappedObject.getSourceTasks();
  }

  @Override
  public String getModelId() {
    return wrappedObject.getModelId();
  }

  @Override
  public String getModelFileName() {
    return wrappedObject.getModelFileName();
  }
}
