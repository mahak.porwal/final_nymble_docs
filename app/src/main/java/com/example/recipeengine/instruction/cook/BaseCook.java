package com.example.recipeengine.instruction.cook;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.cook.params.stirrer.StirrerParams;

public interface BaseCook
    extends CaptureParams, ActionParams, StirrerParams, InferParams, Instruction<BaseCook> {
  /**
   * Returns {@link BaseCook} object.
   * @return {@link BaseCook}
   */
  CookType getCookType();
}
