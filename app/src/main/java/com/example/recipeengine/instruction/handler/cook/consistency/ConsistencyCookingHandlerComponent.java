package com.example.recipeengine.instruction.handler.cook.consistency;

import com.example.recipeengine.instruction.handler.cook.handlermodules.ConsistencyHandlerModule;
import dagger.Component;

@Component(modules = ConsistencyHandlerModule.class)
public interface ConsistencyCookingHandlerComponent {
  ConsistencyCookingHandler buildConsistencyHandlerObject();
}
