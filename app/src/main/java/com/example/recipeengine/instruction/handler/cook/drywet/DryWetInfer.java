package com.example.recipeengine.instruction.handler.cook.drywet;

import static com.example.recipeengine.util.FileUtil.baseDir;
import static com.example.recipeengine.util.ImageUtil.centerCrop;
import static com.example.recipeengine.util.ImageUtil.scaleBitmapAndKeepRatio;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.NonNull;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.handler.cook.infer.BaseInferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.util.FileUtil;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.gpu.GpuDelegate;
import timber.log.Timber;


/**
 * Dry wet detection core class.
 */
public class DryWetInfer extends BaseInferBlock {

  private static final int INPUT_MODEL_IMAGE_SIZE = 256;
  private static final int OUTPUT_MODEL_IMAGE_SIZE = 256;
  private static final int INPUT_MODEL_DIMENSION = 6;
  private static final int NUM_CLASSES = 2;
  private static final float[] IMG_MEAN = {0.485f, 0.456f, 0.406f};
  private static final float[] IMG_STD = {0.229f, 0.224f, 0.225f};
  private static final float IMG_NORM = 255.0f;
  private static final Point INPUT_IMAGE_CROP_ORIGIN = new Point(432, 12);
  private static final int INPUT_IMAGE_CROP_SIZE = 1056;
  private static boolean isDeleted;
  private final InferParams inferParameters;
  private final Interpreter.Options tfliteOptions = new Interpreter.Options();
  private final PublishSubject<Bitmap> imageEmitter1;
  private final PublishSubject<Bitmap> imageEmitter2;
  private final PublishSubject<Bitmap> maskEmitter;
  List<Double> result = new ArrayList<>();
  private Interpreter interpreter;
  private final GpuDelegate gpuDelegate;
  private boolean isModelLoaded;
  private Integer synchronous_number1;
  private Integer synchronous_number2;
  private double modelOutput;
  private Integer synchronous_number_mask;

  /**
   * Dry wet class constructor, initializes base class with.
   *
   * @param inferParameters Contains saute specific information like model name and id.
   * @param recipeContext   Contains recipe specific information
   * @param uiEmitter       publisher for posting anything to UI
   * @param dataLogger      publisher for updating progress logs
   */
  public DryWetInfer(InferParams inferParameters,
                     RecipeContext recipeContext,
                     PublishSubject<UIData> uiEmitter,
                     PublishSubject<ProgressData> dataLogger) {
    super(recipeContext, inferParameters, uiEmitter, dataLogger);
    this.inferParameters = inferParameters;
    this.gpuDelegate = new GpuDelegate();
    this.tfliteOptions.addDelegate(this.gpuDelegate);
    this.tfliteOptions.setNumThreads(1);
    this.imageEmitter1 = PublishSubject.create();
    this.imageEmitter2 = PublishSubject.create();
    this.imageEmitter1.observeOn(Schedulers.io()).subscribe(this::saveImage, Timber::e);
    this.imageEmitter2.observeOn(Schedulers.io()).subscribe(this::saveImage2, Timber::e);
    this.synchronous_number1 = 1;
    this.synchronous_number2 = 1;
    this.synchronous_number_mask = 1;
    this.maskEmitter = PublishSubject.create();
    this.maskEmitter.observeOn(Schedulers.io()).subscribe(this::saveImage_mask, Timber::e);
  }

  private void saveImage(@NonNull Bitmap image1) {
    final String number = String.valueOf(this.synchronous_number1);
    final String filename1 =
        "iteration_" + number + "_1_" + this.modelOutput + ".jpg";
    final File imageFile = FileUtil.getDryWetImageFile(filename1);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image1.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename1);
    }
    this.synchronous_number1 += 1;
  }

  private void saveImage2(@NonNull Bitmap image1) {
    final String number = String.valueOf(this.synchronous_number2);
    final String filename1 =
        "iteration_" + number + "_2_" + this.modelOutput + ".jpg";
    final File imageFile = FileUtil.getDryWetImageFile(filename1);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image1.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename1);
    }
    this.synchronous_number2 += 1;
  }

  private void saveImage_mask(@NonNull Bitmap image1) {
    final String number = String.valueOf(this.synchronous_number_mask);
    final String filename1 =
        "Mask_" + number + "_" + this.modelOutput + ".jpg";
    final File imageFile = FileUtil.getDryWetImageFile(filename1);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image1.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename1);
    }
    this.synchronous_number_mask += 1;
  }

  public static File getAsFile(String dir, String name) {
    final File parentDir = new File(baseDir(), dir);
    if (!isDeleted) {
      for (File file : parentDir.listFiles()) {
        if (!file.isDirectory()) {
          file.delete();
        }
      }
    }
    isDeleted = true;
    if (!parentDir.exists()) {
      parentDir.mkdirs();
    }
    return new File(parentDir, name);
  }

  /**
   * Loading tflite model to memory.
   *
   * @throws Exception exception
   */
  void loadModel() throws Exception {
    this.interpreter = new Interpreter(fetchModelFile(), this.tfliteOptions);
  }

  /**
   * Fetching model file from memory.
   *
   * @return Mapped buffer of model
   * @throws IOException exception
   */
  public MappedByteBuffer fetchModelFile() throws IOException {
    final File modelFile = FileUtil.getModelFile(this.inferParameters.getModelFileName());
    final FileInputStream inputStream = new FileInputStream(modelFile);
    final FileChannel fileChannel = inputStream.getChannel();
    return fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
  }

  /**
   * Method to read image.
   *
   * @param filename filepath of the image
   * @return Bitmap
   */
  public Bitmap readImage(String filename) {
    try {
      final File file =
          new File(Environment.getExternalStorageDirectory(), "/julia_files/" + filename);
      return BitmapFactory.decodeFile(file.getPath());
    } catch (Exception ignored) {
      ignored.printStackTrace();
    }
    return null;
  }

  /**
   * core method for dry wet functionality.
   *
   * @param captureData Contains all the captured data
   * @return Inferred result for taking actions
   */
  @Override
  public InferredResult infer(List<CapturedTaskData> captureData) throws Exception {
    if (null != captureData) {
      CapturedTaskData capturedTaskData = captureData.get(0);
      final List<Bitmap> bmps = capturedTaskData.getVisionCamera();
      final UIData uiData = new UIData(null, null);
      final ProgressData progressData = new ProgressData(null, null, null);

      if (2 > bmps.size()) {
        throw new AssertionError("Assertion failed");
      }
      this.loadInferenceModel();
      for (int i = 0; i < bmps.size() - 1; i++) {
        final Bitmap first =
            centerCrop(bmps.get(i), INPUT_IMAGE_CROP_ORIGIN, INPUT_IMAGE_CROP_SIZE);
        final Bitmap second =
            centerCrop(bmps.get(i + 1), INPUT_IMAGE_CROP_ORIGIN, INPUT_IMAGE_CROP_SIZE);
        final Bitmap scaledBitmap =
            scaleBitmapAndKeepRatio(first, INPUT_MODEL_IMAGE_SIZE, INPUT_MODEL_IMAGE_SIZE);
        final Bitmap scaledBitmap2 =
            scaleBitmapAndKeepRatio(second, INPUT_MODEL_IMAGE_SIZE, INPUT_MODEL_IMAGE_SIZE);
        final ByteBuffer inputBuffer = concatBitmapToByteBuffer(scaledBitmap, scaledBitmap2);
        final ByteBuffer segmentationMasks = ByteBuffer.allocateDirect(
            OUTPUT_MODEL_IMAGE_SIZE * OUTPUT_MODEL_IMAGE_SIZE * NUM_CLASSES * 4);

        segmentationMasks.order(ByteOrder.nativeOrder());
        this.interpreter.run(inputBuffer, segmentationMasks);
        final Bitmap outMask = postProcessModelOutput(segmentationMasks);
        maskEmitter.onNext(outMask);

        uiData.setImage(outMask);
        getUiEmitter().onNext(uiData);    ///updating UI
        this.modelOutput =
            Math.round((this.result.get(this.result.size() - 1)) * 10000.0) / 10000.0;
        this.imageEmitter1.onNext(bmps.get(0));
        this.imageEmitter2.onNext(bmps.get(1));

        final Pair<String, Bitmap> imageData = new Pair<>("mask", outMask);
        final Pair<OutputParams, String> outPutData =
            new Pair<OutputParams, String>(OutputParams.DRY_SCORE,
                String.valueOf(this.result.get(i)));
        progressData.setImageData(imageData);
        progressData.setOutputData(outPutData);
        getDataLogger().onNext(progressData);   //Updating logs
      }
      double sum = 0;
      final double mean;
      for (double val : this.result) {
        sum += val;
      }
      mean = sum / this.result.size();   //Getting mean of all images infer results

      updateInferStatus(mean);         //updating infer status string
      final List<Double> res = new ArrayList<>();
      res.add(mean);
      final InferredResult inferredResult = new InferredResult(res);
      this.result.clear();
      return inferredResult;
    } else {
      throw new IllegalArgumentException("Captured data cannot be null");
    }
  }


  /**
   * Makes an input buffer for model inferring.
   * Concat two image to one buffer.
   *
   * @param in1 First image
   * @param in2 Second image
   * @return ByteBuffer
   */
  private ByteBuffer concatBitmapToByteBuffer(Bitmap in1, Bitmap in2) {
    final Bitmap scaleIm1 =
        scaleBitmapAndKeepRatio(in1, INPUT_MODEL_IMAGE_SIZE, INPUT_MODEL_IMAGE_SIZE);
    final Bitmap scaleIm2 =
        scaleBitmapAndKeepRatio(in2, INPUT_MODEL_IMAGE_SIZE, INPUT_MODEL_IMAGE_SIZE);
    final ByteBuffer inputBuffer = ByteBuffer.allocateDirect(
        INPUT_MODEL_IMAGE_SIZE * INPUT_MODEL_IMAGE_SIZE * INPUT_MODEL_DIMENSION * 4);
    inputBuffer.order(ByteOrder.nativeOrder());
    inputBuffer.rewind();
    final int[] intValues1 = new int[INPUT_MODEL_IMAGE_SIZE * INPUT_MODEL_IMAGE_SIZE];
    final int[] intValues2 = new int[INPUT_MODEL_IMAGE_SIZE * INPUT_MODEL_IMAGE_SIZE];

    scaleIm1.getPixels(intValues1,
        0,
        INPUT_MODEL_IMAGE_SIZE,
        0,
        0,
        INPUT_MODEL_IMAGE_SIZE,
        INPUT_MODEL_IMAGE_SIZE);
    scaleIm2.getPixels(intValues2,
        0,
        INPUT_MODEL_IMAGE_SIZE,
        0,
        0,
        INPUT_MODEL_IMAGE_SIZE,
        INPUT_MODEL_IMAGE_SIZE);

    int pixel = 0;
    int pixel2 = 0;
    for (int i = 0; INPUT_MODEL_IMAGE_SIZE > i; i++) {
      for (int j = 0; INPUT_MODEL_IMAGE_SIZE > j; j++) {
        for (int c = 0; NUM_CLASSES > c; c++) {
          final int value;
          if (0 == c) {
            value = intValues1[pixel];
            pixel++;
          } else {
            value = intValues2[pixel2];
            pixel2++;
          }
          inputBuffer.putFloat((((((value >> 16) & 0xff) / IMG_NORM)) - IMG_MEAN[0]) / IMG_STD[0]);
          inputBuffer.putFloat((((((value >> 8) & 0xff) / IMG_NORM)) - IMG_MEAN[1]) / IMG_STD[1]);
          inputBuffer.putFloat((((((value) & 0xff) / IMG_NORM)) - IMG_MEAN[2]) / IMG_STD[2]);
        }
      }
    }
    return inputBuffer;
  }

  /**
   * This method reads the model's output and creates a mask
   * Calculates the wet pixels percentage and stores into results list.
   *
   * @param buffer model's output
   * @return Mask image to store and posting to UI
   */
  private Bitmap postProcessModelOutput(ByteBuffer buffer) {
    buffer.rewind();
    final int[][] seg = new int[OUTPUT_MODEL_IMAGE_SIZE][OUTPUT_MODEL_IMAGE_SIZE];
    for (int i = 0; OUTPUT_MODEL_IMAGE_SIZE > i; i++) {
      for (int j = 0; OUTPUT_MODEL_IMAGE_SIZE > j; j++) {
        float maxVal = 0;
        seg[j][i] = 0;
        for (int c = 0; NUM_CLASSES > c; c++) {
          final float value = buffer.getFloat(
              (i * OUTPUT_MODEL_IMAGE_SIZE * NUM_CLASSES + j * NUM_CLASSES + c) * 4);
          if (0 == c || value > maxVal) {
            maxVal = value;
            seg[j][i] = c;
          }
        }
      }
    }
    final Mat segImage = new Mat(OUTPUT_MODEL_IMAGE_SIZE, OUTPUT_MODEL_IMAGE_SIZE, CvType.CV_8UC3);
    int wetPixels = 0;
    for (int i = 0; OUTPUT_MODEL_IMAGE_SIZE > i; i++) {
      for (int j = 0; OUTPUT_MODEL_IMAGE_SIZE > j; j++) {
        if (0 == seg[j][i]) {
          final byte[] data = {0, 0, 0};
          segImage.put(i, j, data);
        } else if (1 == seg[j][i]) {
          final byte[] data = {125, 125, 0};
          segImage.put(i, j, data);
          wetPixels++;
        }
      }
    }
    this.result.add(((double) wetPixels / (OUTPUT_MODEL_IMAGE_SIZE * OUTPUT_MODEL_IMAGE_SIZE)));

    Imgproc.resize(segImage, segImage, new Size(INPUT_IMAGE_CROP_SIZE, INPUT_IMAGE_CROP_SIZE));
    final Bitmap segBitmap = _parseBitmap(segImage);
    return segBitmap;
  }


  /**
   * Convert Opencv.Mat to Bitmap
   *
   * @param mat opencv mat object
   * @return Bitmap
   */
  private Bitmap _parseBitmap(Mat mat) {
    final Bitmap bmp = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
    Utils.matToBitmap(mat, bmp);
    return bmp;
  }

  /**
   * Method to update the infer status string for debugging.
   *
   * @param mean score from infer results
   */
  private void updateInferStatus(double mean) {
    this.inferStatus =
        "Reaching target dry state, current infer result is = " + mean * 100 + " percent dry";
    Log.d("DRY_INFER", this.inferStatus);
  }


  /**
   * Method to halt the current operations of dry wet.
   */
  @Override
  public void pause() {
    if (null != this.interpreter) {
      this.interpreter.close();
      this.interpreter = null;
    }
  }

  @Override
  public void close() throws IOException {
    if (null != this.interpreter) {
      this.interpreter.close();
      this.interpreter = null;
    }
  }

  /**
   * Loads inference model once
   *
   * @throws Exception
   */
  private void loadInferenceModel() throws Exception {
    if (!this.isModelLoaded) {
      this.loadModel();
      this.isModelLoaded = true;
    }
  }
}
