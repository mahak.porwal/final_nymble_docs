package com.example.recipeengine.instruction.handler.cook.wattage;


import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.handler.cook.action.BaseActionBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;

import java.util.concurrent.atomic.AtomicLong;

import io.reactivex.subjects.PublishSubject;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.actuator.Actuator;
import som.instruction.request.heat.HeatPowerLevel;

public class WattageAction extends BaseActionBlock {
  private boolean isCompleted;
  private final AtomicLong startTime;
  private int wattageState;
  final int START = 1;
  final int DONE = 2;

  /**
   * Constructor of BaseActionBlock.
   *
   * @param hwRequestHandler request handler object
   * @param uiEmitter        RxJava subject to send results to UI
   * @param dataLogger       RxJava subject to log results
   */
  public WattageAction(RequestHandler hwRequestHandler, PublishSubject<UIData> uiEmitter, PublishSubject<ProgressData> dataLogger) {
    super(hwRequestHandler, uiEmitter, dataLogger);
    this.startTime = new AtomicLong();
    this.wattageState = START;
  }

  @Override
  public void takeAction(InferredResult inferredResult, ActionParams actionParams) throws Exception {
    switch (this.wattageState) {
      case START:
        final int defaultWattage = actionParams.getDefaultWattage();
        final HeatPowerLevel heatPowerLevel = Actuator.createHeatLevel(defaultWattage);
        this.changePowerLevel(heatPowerLevel);
        this.startTime.set(System.currentTimeMillis());
        this.wattageState = DONE;
        break;
      case DONE:
        final long timeSpent = System.currentTimeMillis() - this.startTime.get();
        if (timeSpent > actionParams.getTimeToCook() * 1000) {
          this.isCompleted = true;
        }
        break;
    }
  }

  @Override
  public Boolean isInstCompleted() {
    return this.isCompleted;
  }

  @Override
  public void pause() throws Exception {

  }

  @Override
  protected Integer calcRemainingTimeSec() {
    final long timeSpent = System.currentTimeMillis() - this.startTime.get();
    return Math.toIntExact(timeSpent / 1000);
  }
}