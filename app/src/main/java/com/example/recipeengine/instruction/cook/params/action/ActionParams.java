package com.example.recipeengine.instruction.cook.params.action;


public interface ActionParams {
  Integer getDefaultWattage();

  Integer getTimeToCook();

  Double getVisualScore();

  Integer getTemperature();

  Double getThermalScore();
}
