package com.example.recipeengine.instruction.task.dispense;

import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseHwInformation;
import som.hardware.message.action.ActionType;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.dispense.Dispense;
import som.instruction.request.dispense.DispenseContainer;
import som.instruction.request.dispense.Ingredient;

public class DispenseTaskThread extends Thread {
  private DispenseHwInformation dispenseHwInformation;

  public DispenseTaskThread(String name, DispenseHwInformation dispenseHwInformation) {
    super(name);
    this.dispenseHwInformation = dispenseHwInformation;
  }

  @Override
  public void run() {
    Dispense dispense = new Dispense(null);
    final String dispenseContainer = this.dispenseHwInformation.getHardwareContainer();
    final String quantity = this.dispenseHwInformation.getHardwareFormQuantity();
    final String dispenseMechanism = this.dispenseHwInformation.getDispenseMechanism();
    dispense.setContainer(DispenseContainer.valueOf(dispenseContainer))
        .setDispenseBehavior(ActionType.valueOf(dispenseMechanism))
        .setQuantity(Double.valueOf(quantity));
    //TODO : Proper logic to setIngredient needs to be added.
    dispense.setIngredient(Ingredient.MILK);
    RequestHandler requestHandler = new RequestHandler();
    try {
      requestHandler.handleRequest(dispense);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
