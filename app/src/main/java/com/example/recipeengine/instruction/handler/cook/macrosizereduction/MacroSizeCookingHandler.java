package com.example.recipeengine.instruction.handler.cook.macrosizereduction;

import com.example.recipeengine.instruction.blocks.capture.CaptureBlock;
import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.handler.cook.action.ActionBlock;
import com.example.recipeengine.instruction.handler.cook.base.BaseCookingHandler;
import com.example.recipeengine.instruction.handler.cook.infer.InferBlock;
import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;
import com.example.recipeengine.instruction.handler.cook.status.InstructionStatus;
import javax.inject.Inject;

public class MacroSizeCookingHandler extends BaseCookingHandler {

  private final ActionBlock actionBlock;

  @Inject
  public MacroSizeCookingHandler(Cook cook,
                                 CaptureBlock captureBlock,
                                 InferBlock inferBlock,
                                 ActionBlock actionBlock) {
    super(cook, captureBlock, inferBlock, actionBlock);
    this.actionBlock = actionBlock;
  }

  @Override
  public BaseInstStatus getStatus() {
    this.instructionStatus = new InstructionStatus(this.actionBlock.getScoreStatus(),
        super.getCurrentBlock().getBlockStatus(),
      this.actionBlock.getRemainingTimeSec());
    return this.instructionStatus;
  }
}
