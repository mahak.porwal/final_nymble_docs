package com.example.recipeengine.instruction.task.hardware.stirrer;

import com.example.recipeengine.instruction.task.capture.base.Task;

import som.hardware.request.handler.RequestHandler;
import som.instruction.request.actuator.Actuator;
import som.instruction.request.actuator.Actuators;
import som.instruction.request.dispense.DispenseContainer;
import timber.log.Timber;

public class StirrerPositionTask implements Task {
  private final Thread stirPosThread;
  private final String taskName = "Stir Position";

  public StirrerPositionTask(DispenseContainer[] dispenseContainers) {
    this.stirPosThread = new StirPosTaskThread(
      this.taskName, dispenseContainers);
  }

  @Override
  public void beginTask() {
    this.stirPosThread.run();
  }

  @Override
  public void abortTask() {
    this.stirPosThread.interrupt();
    try {
      this.stopStirrer();
    } catch (Exception exception) {
      Timber.e(exception);
    }
  }

  @Override
  public Thread getThread() {
    return this.stirPosThread;
  }

  @Override
  public String getTaskName() {
    return this.taskName;
  }

  private void stopStirrer() throws Exception {
    final Actuator actuator = new Actuator(null);
    actuator.setActuator(Actuators.STIRRER).setActuatorSpeed(0);
    final RequestHandler requestHandler = new RequestHandler();
    requestHandler.handleRequest(actuator);
  }
}
