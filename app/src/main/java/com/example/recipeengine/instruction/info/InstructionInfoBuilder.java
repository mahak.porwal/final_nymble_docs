package com.example.recipeengine.instruction.info;


import com.example.recipeengine.instruction.info.cook.CookInfoFactory;
import com.example.recipeengine.instruction.info.dispense.DispenseInfoFactory;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Builds List of ojbect of {@link InstructionInfo}, given a List of {@link JSONObject}
 */
public class InstructionInfoBuilder {

  private CookInfoFactory cookInfoFactory;
  private DispenseInfoFactory dispenseInfoFactory;

  @Inject
  public InstructionInfoBuilder(CookInfoFactory cookInfoFactory,
                                DispenseInfoFactory dispenseInfoFactory) {
    this.cookInfoFactory = cookInfoFactory;
    this.dispenseInfoFactory = dispenseInfoFactory;
  }

  /**
   * Delegates the the task of building {@link InstructionInfo} objects
   * to {@link CookInfoFactory} and DispenseInfoFactory
   *
   * @return List of {@link InstructionInfo}
   */
  public List<InstructionInfo> buildInstructionInfo(List<JSONObject> instructionList) {
    List<InstructionInfo> instructionInfoList = new ArrayList<>();
    Iterator<JSONObject> iterator = instructionList.listIterator();
    InstructionInfo instructionInfo = null;
    while (iterator.hasNext()) {
      JSONObject jsonObject = iterator.next();
      String instructionType = ((Map) jsonObject).get("instructionType").toString();
      if (instructionType.compareTo("Cook") == 0) {
        instructionInfo = this.cookInfoFactory.createCookInfo(jsonObject);
      } else {
        //Dispense
        instructionInfo = this.dispenseInfoFactory.createDispenseInfo(jsonObject);
      }
      instructionInfoList.add(instructionInfo);
    }
    return instructionInfoList;
  }

  public static Integer buildInstructionId(JSONObject jsonObject) {
    Integer instructionId = Integer.valueOf((String) jsonObject.get("instructionId"));
    return instructionId;
  }

  public static Integer buildAttemptNum(JSONObject jsonObject) {
    Integer attempNum = Integer.valueOf((String) jsonObject.get("attemptNum"));
    return attempNum;
  }
}
