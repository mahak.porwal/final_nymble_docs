package com.example.recipeengine.instruction.executor.factory;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.executor.InstructionExecutor;

public interface ExecutorFactory {
  InstructionExecutor getExecutor(Instruction instruction);
}
