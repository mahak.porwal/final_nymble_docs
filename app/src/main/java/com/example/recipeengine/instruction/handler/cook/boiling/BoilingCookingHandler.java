package com.example.recipeengine.instruction.handler.cook.boiling;

import com.example.recipeengine.instruction.blocks.capture.CaptureBlock;
import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.handler.cook.action.ActionBlock;
import com.example.recipeengine.instruction.handler.cook.base.BaseCookingHandler;
import com.example.recipeengine.instruction.handler.cook.infer.InferBlock;
import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;
import com.example.recipeengine.instruction.handler.cook.status.InstructionStatus;
import javax.inject.Inject;

public class BoilingCookingHandler extends BaseCookingHandler {

  private ActionBlock actionBlock;

  @Inject
  public BoilingCookingHandler(Cook cook,
                               CaptureBlock captureBlock,
                               InferBlock inferBlock,
                               ActionBlock actionBlock) {
    super(cook, captureBlock, inferBlock, actionBlock);
    this.actionBlock = actionBlock;
  }

  @Override
  public BaseInstStatus getStatus() {
    instructionStatus = new InstructionStatus(actionBlock.getScoreStatus(),
        super.getCurrentBlock().getBlockStatus(),
        actionBlock.getRemainingTimeSec());
    return instructionStatus;
  }
}
