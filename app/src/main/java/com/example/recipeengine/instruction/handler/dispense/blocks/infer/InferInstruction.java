package com.example.recipeengine.instruction.handler.dispense.blocks.infer;

import com.example.recipeengine.instruction.blocks.capture.CaptureBlock;
import com.example.recipeengine.instruction.handler.cook.action.ActionBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferBlock;

/**
 * This class runs an Infer on previously captured data.
 */
public class InferInstruction {

  private CaptureBlock captureBlock;
  private InferBlock inferBlock;
  private ActionBlock actionBlock;

  /**
   * Constructor for InferInstruction.
   *
   * @param captureBlock CaptureBlock object
   * @param inferBlock   InferBlock object
   * @param actionBlock  ActionBlock object
   */
  public InferInstruction(
      CaptureBlock captureBlock,
      InferBlock inferBlock,
      ActionBlock actionBlock) {
    this.captureBlock = captureBlock;
    this.inferBlock = inferBlock;
    this.actionBlock = actionBlock;
  }

  public CaptureBlock getCaptureBlock() {
    return captureBlock;
  }

  public InferBlock getInferBlock() {
    return inferBlock;
  }

  public ActionBlock getActionBlock() {
    return actionBlock;
  }
}
