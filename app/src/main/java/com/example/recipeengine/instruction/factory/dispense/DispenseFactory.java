package com.example.recipeengine.instruction.factory.dispense;

import com.example.recipeengine.instruction.cook.params.stirrer.StirrerParams;
import com.example.recipeengine.instruction.cook.params.stirrer.StirrerProfile;
import com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper;
import com.example.recipeengine.instruction.dispense.Dispense;
import com.example.recipeengine.instruction.handler.cook.factory.CaptureBlockFactory;
import com.example.recipeengine.instruction.handler.cook.factory.InferBlockFactory;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseHwInformation;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseType;
import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;
import com.example.recipeengine.instruction.info.dispense.DispenseBlockBuilder_Factory;
import com.example.recipeengine.instruction.info.dispense.DispenseInfo;
import com.example.recipeengine.instruction.info.dispense.DispenseInfoFactory_Factory;
import com.example.recipeengine.instruction.info.dispense.InferInstructionListBuilder_Factory;
import com.example.recipeengine.instruction.info.dispense.IngredientListBuilder_Factory;
import com.example.recipeengine.instruction.info.dispense.ParametricFieldsBuilder_Factory;
import com.example.recipeengine.instruction.info.dispense.PostDispenseBlockBuilder_Factory;
import com.example.recipeengine.instruction.info.dispense.PreDispenseBlockBuilder_Factory;
import com.example.recipeengine.instruction.info.dispense.QuantityInfoBuilder_Factory;
import com.example.recipeengine.instruction.task.action.ActionTaskParams;
import com.example.recipeengine.instruction.task.action.ActionableTask;
import com.example.recipeengine.instruction.task.action.BaseActionableTask;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import com.example.recipeengine.instruction.task.capture.base.Task;
import com.example.recipeengine.instruction.task.hardware.PostDispenseTaskProvider;
import com.example.recipeengine.instruction.task.hardware.PreDispenseTaskProvider;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import som.instruction.request.actuator.Actuators;


/**
 * This class creates a Dispense object by reading
 * values from the JSON.
 */

public class DispenseFactory {
  @Inject
  public DispenseFactory() {
  }

  public static String filename =
      "./src/main/java/com/example/recipeengine/resources/Dispense.json";

  /**
   * This method is responsible for returning the Dispense Object.
   * 1. Store the filename in static variable/SharedPreferences
   * 2. Read the values from given JSON file using JSONParser.
   * 3. Create a new Dispense object by passing the values to the constructor.
   *
   * @param filename : String the filepath of Dispense.json
   * @return Dispense Object
   * @throws IOException    : Incorrect Input or Output
   * @throws ParseException : String failed to parse
   */
  public Dispense createDispenseObject(String filename) throws Exception {
    this.filename = filename;

    final DispenseInfo dispenseInfo = DispenseInfoFactory_Factory.newDispenseInfoFactory(
        QuantityInfoBuilder_Factory.newQuantityInfoBuilder(),
        IngredientListBuilder_Factory.newIngredientListBuilder(),
        PreDispenseBlockBuilder_Factory.newPreDispenseBlockBuilder(),
        DispenseBlockBuilder_Factory.newDispenseBlockBuilder(),
        PostDispenseBlockBuilder_Factory.newPostDispenseBlockBuilder(),
        InferInstructionListBuilder_Factory.newInferInstructionListBuilder(),
        ParametricFieldsBuilder_Factory.newParametricFieldsBuilder()).createDispenseInfo(filename);
    return this.createDispenseObject(dispenseInfo, null);
  }

  public Dispense createDispenseObject(DispenseInfo dispenseInfo, Map<String, String> parameters)
      throws Exception {
    if (null == dispenseInfo) {
      throw new NullPointerException();
    }
    dispenseInfo.resolveParametricFields(parameters);
    if (!dispenseInfo.isExecutable()) {
      throw new Exception("Unexecutable dispense information");
    }
    final Integer quantityInGrams = DispenseInfoConvertor.provideQuantityInGrams(dispenseInfo);
    final List<String> ingredients = dispenseInfo.getIngredients();
    final PreDispenseBlock preDispenseBlock =
        DispenseInfoConvertor.providePreDispenseBlock(dispenseInfo);
    final DispenseBlock dispenseBlock = DispenseInfoConvertor.provideDispenseBlock(dispenseInfo);
    final PostDispenseBlock postDispenseBlock =
        DispenseInfoConvertor.providePostDispenseBlock(dispenseInfo);
    final List<InferInstruction> inferInstructions =
        DispenseInfoConvertor.provideInferInstructions(dispenseInfo);
    final Integer instructionId = dispenseInfo.getInstructionId();
    final Integer attemptNum = dispenseInfo.getAttemptNum();

    final Dispense dispense = new Dispense(instructionId, attemptNum, quantityInGrams, ingredients,
        preDispenseBlock, dispenseBlock, postDispenseBlock, inferInstructions);
    return dispense;
  }

  public static List<InferInstruction> resolveInferInstructions(JSONObject jsonObject) {
    final List<InferInstruction> inferInstructions = new ArrayList<InferInstruction>();
    final List inferList = (List) jsonObject.get("inferInstructions");
    for (int i = 0; i < inferList.size(); i++) {
      final Map infer = (Map) inferList.get(i);
      inferInstructions.add(
          new InferInstruction(
              CaptureBlockFactory.getCaptureBlockObjectJson(infer),
              InferBlockFactory.getDispenseInferBlockObject(infer), null));
    }
    return inferInstructions;
  }

  public static PostDispenseBlock resolvePostDispenseBlock(JSONObject jsonObject) {
    final Map address = ((Map) jsonObject.get("postDispense"));
    final PostDispenseTaskProvider postDispenseTaskProvider = new PostDispenseTaskProvider();
    final List<Task> tasks = resolveTasks((List) address.getOrDefault("capturableTasks", null),
        (List) address.getOrDefault("actionableTasks", null),
        (List) address.getOrDefault("sequenceList", null));
    final String hwTasksPosition =
        (String) jsonObject.getOrDefault("postDispenseHwTaskPosition", null);
    if (hwTasksPosition != null) {
      handleHardwareTasks(postDispenseTaskProvider.hwTasksProvider(), tasks, hwTasksPosition);
    }
    return new PostDispenseBlock(tasks);
  }

  public static DispenseBlock resolveDispenseBlock(JSONObject jsonObject) {
    final Map address = ((Map) jsonObject.get("dispense"));
    final Map dispenseHwInformation = (Map) jsonObject.getOrDefault("dispenseHwInformation", null);
    final List<? extends Task> tasks =
        resolveTasks((List) address.getOrDefault("capturableTasks", null),
            (List) address.getOrDefault("actionableTasks", null),
            (List) address.getOrDefault("sequenceList", null));

    final DispenseHwInformation[] dispenseHwInformations = resolveDispenseHwInfo(jsonObject);
    return new DispenseBlock(tasks, dispenseHwInformations[0]);
  }

  public static DispenseType resolveDispenseType(String hardwareSubsystem) {
    if (hardwareSubsystem.equalsIgnoreCase("macro")) {
      return new DispenseType(DispenseType.MACRO);
    } else if (hardwareSubsystem.equalsIgnoreCase("micro")) {
      return new DispenseType(DispenseType.MICRO);
    } else if (hardwareSubsystem.equalsIgnoreCase("liquid")) {
      return new DispenseType(DispenseType.LIQUID);
    }

    return null;
  }

  public static PreDispenseBlock resolvePreDispenseBlock(JSONObject jsonObject) {
    final Map address = ((Map) jsonObject.get("preDispense"));
    final PreDispenseTaskProvider preDispenseTaskProvider = new PreDispenseTaskProvider();
    final List<Task> tasks = resolveTasks((List) address.getOrDefault("capturableTasks", null),
        (List) address.getOrDefault("actionableTasks", null),
        (List) address.getOrDefault("sequenceList", null));
    final DispenseHwInformation[] dispenseHwInformation = resolveDispenseHwInfo(jsonObject);
    final String hwTasksPosition =
        (String) jsonObject.getOrDefault("preDispenseHwTaskPosition", null);
    if (hwTasksPosition != null && !hwTasksPosition.equals("")) {
      handleHardwareTasks(preDispenseTaskProvider.hwTasksProvider(dispenseHwInformation),
          tasks, hwTasksPosition);
    }
    return new PreDispenseBlock(tasks);
  }

  public static void handleHardwareTasks(List<Task> hardwareTaskList, List<Task> tasks,
                                         String hwTasksPosition) {
    if (tasks == null) {
      return;
    }

    tasks.addAll(Integer.parseInt(hwTasksPosition), hardwareTaskList);
  }

  public static List<Task> resolveTasks(List capturableTasks,
                                        List actionableTasks, List taskNames) {
    final List<Task> tasks = new ArrayList<>();
    final List<CapturableTask> capturableTaskList = new ArrayList<>();
    final List<ActionableTask> actionableTaskList = new ArrayList<>();

    if (capturableTasks != null) {
      for (int j = 0; j < capturableTasks.size(); j++) {
        final CapturableTask capturableTask = resolveCapturableTasks((Map) capturableTasks.get(j));
        if (capturableTask != null) {
          capturableTaskList.add(capturableTask);
        }
      }
    }
    if (actionableTasks != null) {
      for (int i = 0; i < actionableTasks.size(); i++) {
        final ActionableTask actionableTask = resolveActionableTasks((Map) actionableTasks.get(i));
        if (actionableTask != null) {
          actionableTaskList.add(actionableTask);
        }
      }
    }

    if (taskNames == null || taskNames.size() == 0) {
      tasks.addAll(capturableTaskList);
      tasks.addAll(actionableTaskList);
      return tasks;
    }

    for (int i = 0; i < taskNames.size(); i++) {
      final String taskName = (String) taskNames.get(i);
      for (int j = 0; j < capturableTaskList.size(); j++) {
        if (capturableTaskList.get(j).getTaskName()
            .equals(taskName)) {
          tasks.add(capturableTaskList.get(j));
        }
      }

      for (int j = 0; j < actionableTaskList.size(); j++) {
        if (actionableTaskList.get(j).getTaskName()
            .equals(taskName)) {
          tasks.add(actionableTaskList.get(j));
        }
      }
    }

    return tasks;
  }

  public static ActionableTask resolveActionableTasks(Map taskMap) {
    return new BaseActionableTask((String) taskMap.getOrDefault("taskName", null),
        resolveActuator((String) taskMap.getOrDefault("actuator", null)),
        resolveActionParams((Map) taskMap.getOrDefault("actionParameters", null)));
  }

  private static ActionTaskParams resolveActionParams(Map actionParameters) {
    return new ActionTaskParams(
        resolveStirrerParams((Map) actionParameters.getOrDefault("stirrerParams", null)),
        parseDouble((String) actionParameters.getOrDefault("actuatorSpeed", null)),
        parseInteger((String) actionParameters.getOrDefault("actuatorFreq", null)),
        parseInteger((String) actionParameters.getOrDefault("heatLevel", null)),
        parseBoolean((String) actionParameters.getOrDefault("actuatorState", null)));
  }


  private static StirrerParams resolveStirrerParams(Map stirrerParams) {
    if (stirrerParams == null) {
      return null;
    }

    return new StirrerProfile(
        Integer.parseInt((String) stirrerParams.getOrDefault("cycleDurationSeconds", "0")),
        Integer.parseInt((String) stirrerParams.getOrDefault("stirringPercent", "0")),
        Integer.parseInt((String) stirrerParams.getOrDefault("stirringSpeed", "0")));
  }

  private static String resolveActuator(String actuator) {
    Actuators actuators = null;

    switch (actuator) {
      case "ExhaustFan":
        actuators = new Actuators(Actuators.EXHAUST_FAN);
        break;
      case "InductionPower":
        actuators = new Actuators(Actuators.INDUCTION_POWER);
        break;
      case "Buzzer":
        actuators = new Actuators(Actuators.BUZZER);
        break;
      case "Stirrer":
        actuators = new Actuators(Actuators.STIRRER);
        break;
      case "DesignLed":
        actuators = new Actuators(Actuators.DESIGN_LED);
        break;
      default:
        actuators = null;
    }

    return actuators.getActuator();
  }

  public static CapturableTask resolveCapturableTasks(Map taskMap) {
    if (((String) taskMap.get("captureSensor")).equalsIgnoreCase("WeightSensor")) {
      return CookParamsModuleHelper.resolveWeightCaptureTask(taskMap);
    } else {
      return CookParamsModuleHelper.resolveVisionCaptureTask(taskMap);
    }
  }

  private static DispenseHwInformation[] resolveDispenseHwInfo(JSONObject jsonObject) {
    final Map dispenseHwInformation = (Map) jsonObject.getOrDefault("dispenseHwInformation", null);
    final DispenseHwInformation[] dispenseHwInformations = new DispenseHwInformation[1];
    dispenseHwInformations[0] = new DispenseHwInformation(
        resolveDispenseType(
            (String) dispenseHwInformation.getOrDefault("hardwareSubsystem", null)),
        (String) dispenseHwInformation.getOrDefault("dispenseMechanism", null),
        (String) dispenseHwInformation.getOrDefault("hardwareFormQuantity", null),
        (String) dispenseHwInformation.getOrDefault("hardwareContainer", null));
    return dispenseHwInformations;
  }

  private static Double parseDouble(String number) {
    return number != null ? Double.parseDouble(number) : null;
  }

  private static Integer parseInteger(String number) {
    return number != null ? Integer.parseInt(number) : null;
  }

  private static Boolean parseBoolean(String actuatorState) {
    return actuatorState != null ? Boolean.parseBoolean(actuatorState) : null;
  }

}
