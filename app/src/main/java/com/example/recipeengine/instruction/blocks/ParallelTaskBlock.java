package com.example.recipeengine.instruction.blocks;

import com.example.recipeengine.instruction.task.capture.base.Task;
import java.util.List;

public interface ParallelTaskBlock extends Block {

  void startParallelTasks() throws InterruptedException;

  List<? extends Task> getParallelTasks();

}
