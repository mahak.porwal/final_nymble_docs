package com.example.recipeengine.instruction.handler.cook.action;

import com.example.recipeengine.instruction.blocks.Block;
import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;

public interface ActionBlock extends Block {

  void takeAction(InferredResult inferredResult, ActionParams actionParams) throws Exception;

  Boolean isInstCompleted();

  String getScoreStatus();

  Integer getRemainingTimeSec();


}
