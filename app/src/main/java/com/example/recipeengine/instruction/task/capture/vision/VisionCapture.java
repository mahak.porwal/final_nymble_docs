package com.example.recipeengine.instruction.task.capture.vision;

import static com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType.STIRRER_STARTS;
import static com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType.STIRRER_STOPS;

import android.graphics.Bitmap;

import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.CaptureSensor;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.base.BaseCapturableTask;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;

import com.example.recipeengine.resources.JuliaStateDemo;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import som.hardware.request.handler.RequestHandler;
import som.instruction.request.sensor.Sensors;
import timber.log.Timber;

/**
 * This class implements {@link CapturableTask} and contains the properties
 * and methods to capture either RGB or thermal images by running the
 * algorithm in separate threads.
 */
public class VisionCapture extends BaseCapturableTask implements PropertyChangeListener {

  private final CaptureSensor sensor = new CaptureSensor(CaptureSensor.VISUAL_CAMERA);
  private final SensorParams sensorParams;
  private final List<Bitmap> thermalImages;
  private final List<Bitmap> visionImages;
  private final List<List<Double>> thermalMatrices;
  private final RequestHandler hwRequestHandler;
  private Thread thread;
  private boolean begin;
  private CountDownLatch countDownLatch;
  private JuliaStateDemo juliaStateDemo;

  /**
   * Constructor for VisionCapture class.
   *
   * @param sensorParams Contains all the required params related to sensor
   */
  public VisionCapture(String taskName,
                       SensorParams sensorParams) {
    super(null, null, null);
    this.sensorParams = sensorParams;
    this.thermalImages = new ArrayList<>();
    this.visionImages = new ArrayList<>();
    this.thermalMatrices = new ArrayList<>();
    final HashMap hashMap = new HashMap<Sensors, List<List<Double>>>();
    hashMap.put(Sensors.THERMAL_CAMERA, this.thermalMatrices);
    //TASK NAME ADD
    super.capturedTaskData = new CapturedTaskData(hashMap, this.visionImages, this.thermalImages, taskName);
    super.taskName = taskName;
    this.hwRequestHandler = new RequestHandler();
  }

  public synchronized boolean isBegin() {
    return this.begin;
  }

  public synchronized void setBegin(boolean begin) {
    this.begin = begin;
  }

  @Override
  public CaptureSensor getSensor() {
    return this.sensor;
  }

  @Override
  public SensorParams getParams() {
    return this.sensorParams;
  }

  @Override
  public void resetCapturedTaskData() {
    this.visionImages.clear();
    this.thermalImages.clear();
    this.thermalMatrices.clear();
  }

  public List<Bitmap> getThermalImages() {
    return this.thermalImages;
  }

  public List<Bitmap> getVisionImages() {
    return this.visionImages;
  }

  public List<List<Double>> getThermalMatrices() {
    return this.thermalMatrices;
  }

  /**
   * This function is responsible for running the CaptureTask of
   * a specific {@link VisualCaptureType}.
   * 1. First the function checks if there is a trigger event given in the
   * sensor params or not.
   * 2. If there is no trigger event the execution shall continue as usual.
   * 3. If it is present then the we wait for Julia State to change from idle to
   * running we fire a propertyChangeEvent to change begin from false to true.
   * The timeout is set for 120000 ms.
   * 4. Then we use the {@link VisionTaskFactory} to get the correct Thread
   * implementation based on {@link VisualCaptureType} present in {@link #sensorParams}
   * 5. Then we run the start method of the thread object.
   */
  @Override
  public synchronized void beginTask() {
    //VISION
    //THERMAL
    //SYNC_THERMAL
    if (null != this.sensorParams.getTriggerEvent()) {
      /*
        When the begin is false. It waits for propertyChange() method to be invoked.
        After invocation of the method begin is set to true and notifyAll() is called
        to resume all the thread.
       */
      juliaStateDemo = JuliaStateDemo.getInstance();
      juliaStateDemo.addPropertyChangeListener(this);
      this.countDownLatch = new CountDownLatch(1);
      try {
        this.countDownLatch.await(120, TimeUnit.SECONDS);//wait(120000);
      } catch (InterruptedException e) {
        Timber.e(e);
      }
    }

    final VisionTaskFactory visionTaskFactory =
      new VisionTaskFactory(this.sensorParams, this.thermalImages, this.visionImages,
        this.thermalMatrices, this.hwRequestHandler);
    this.thread = visionTaskFactory
      .getVisionTaskInstance(this.sensorParams.getVisualCaptureType().getVisualCaptureType());
    this.thread.start();
  }

  /**
   * This method is responsible to interrupt the execution {@link CapturableTask} thread
   * and stop it. We must check if the thread is not null and isAlive() is true.
   */
  @Override
  public void abortTask() {
    /*
    BottleNeck : Abort called when no thread is present.
     */
    if (this.thread != null) {
      this.thread.interrupt();
    }
  }

  @Override
  public Thread getThread() {
    return this.thread;
  }

  /**
   * Whenever propertyChange happens on JuliaState side we setBegin(true).
   */
  @Override
  public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    final String oldValue = (String) propertyChangeEvent.getOldValue();
    final String newValue = (String) propertyChangeEvent.getNewValue();
    final TriggerType triggerType = this.sensorParams.getTriggerEvent();

    final boolean isStartConditionTrue = triggerType.getTriggerType().equalsIgnoreCase(STIRRER_STARTS)
      && (oldValue.equalsIgnoreCase("idle") && newValue.equalsIgnoreCase("running"));

    final boolean isStopConditionTrue = triggerType.getTriggerType().equalsIgnoreCase(STIRRER_STOPS)
      && (oldValue.equalsIgnoreCase("running") && newValue.equalsIgnoreCase("idle"));

    if (isStartConditionTrue || isStopConditionTrue) {
      this.countDownLatch.countDown();
    }
  }
}
