package com.example.recipeengine.instruction.dispense;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;

import java.util.List;

/**
 * Interface containing required methods for {@link Dispense} object.
 */
public interface BaseDispense extends Instruction<BaseDispense> {
  /**
   * Returns quantityInGrams of the {@link Dispense} object.
   *
   * @return Integer quantityInGrams
   */
  Integer getQuantityInGrams();

  /**
   * Returns ingredients list of the {@link Dispense} object.
   *
   * @return List of String ingredients
   */
  List<String> getIngredients();

  PreDispenseBlock getPreDispenseBlock();

  DispenseBlock getDispenseBlock();

  PostDispenseBlock getPostDispenseBlock();

  List<InferInstruction> getInferInstructions();
}
