package com.example.recipeengine.instruction.handler.cook.boiling;

import com.example.recipeengine.instruction.handler.cook.handlermodules.BoilingHandlerModule;
import dagger.Component;

@Component(modules = BoilingHandlerModule.class)
public interface BoilingCookingHandlerComponent {
  BoilingCookingHandler buildBoilingHandlerObject();
}
