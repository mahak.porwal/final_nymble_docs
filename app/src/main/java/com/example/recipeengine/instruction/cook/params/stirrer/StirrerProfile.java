package com.example.recipeengine.instruction.cook.params.stirrer;

/**
 * This class contains all values related to StirrerParams of Saute Instruction.
 */
public class StirrerProfile implements StirrerParams {

  private Integer cycleDurationSeconds;
  private Integer stirringPercent;
  private Integer stirringSpeed;

  /**
   * Constructor of StirrerProfile class.
   *
   * @param cycleDurationSeconds current Saute Instructions Cycle duration seconds
   * @param stirringPercent current Saute Instructions Stirring percentage
   * @param stirringSpeed current Saute Instructions Stirring speed
   */
  public StirrerProfile(Integer cycleDurationSeconds, Integer stirringPercent,
                        Integer stirringSpeed) {
    this.cycleDurationSeconds = cycleDurationSeconds;
    this.stirringPercent = stirringPercent;
    this.stirringSpeed = stirringSpeed;
  }

  @Override
  public Integer getCycleDurationSeconds() {
    return cycleDurationSeconds;
  }

  @Override
  public Integer getStirringPercent() {
    return stirringPercent;
  }

  @Override
  public Integer getStirringSpeed() {
    return stirringSpeed;
  }
}
