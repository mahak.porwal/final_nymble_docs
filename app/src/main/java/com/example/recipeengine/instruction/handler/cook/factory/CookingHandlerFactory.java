package com.example.recipeengine.instruction.handler.cook.factory;

import androidx.annotation.VisibleForTesting;

import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.cook.CookType;
import com.example.recipeengine.instruction.cook.CookType.AnnotationCookType;
import com.example.recipeengine.instruction.handler.cook.base.BaseCookingHandler;
import com.example.recipeengine.instruction.handler.cook.boiling.BoilingCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.boiling.DaggerBoilingCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.consistency.ConsistencyCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.consistency.DaggerConsistencyCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.drywet.DaggerDryWetCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.drywet.DryWetCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.frying.DaggerFryingCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.frying.FryingCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.handlermodules.BoilingHandlerModule;
import com.example.recipeengine.instruction.handler.cook.handlermodules.ConsistencyHandlerModule;
import com.example.recipeengine.instruction.handler.cook.handlermodules.DryWetHandlerModule;
import com.example.recipeengine.instruction.handler.cook.handlermodules.FryingHandlerModule;
import com.example.recipeengine.instruction.handler.cook.handlermodules.MacroSizeHandlerModule;
import com.example.recipeengine.instruction.handler.cook.handlermodules.TemperatureHandlerModule;
import com.example.recipeengine.instruction.handler.cook.handlermodules.WattageHandlerModule;
import com.example.recipeengine.instruction.handler.cook.macrosizereduction.DaggerMacroSizeCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.macrosizereduction.MacroSizeCookingHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.temperature.DaggerTemperatureHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.temperature.TemperatureHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.wattage.DaggerWattageHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.wattage.WattageHandlerComponent;

import com.example.recipeengine.instruction.handler.cook.temperature.DaggerTemperatureHandlerComponent;
import com.example.recipeengine.instruction.handler.cook.temperature.TemperatureHandlerComponent;
import javax.inject.Inject;

import static com.example.recipeengine.instruction.cook.CookType.BOILING;
import static com.example.recipeengine.instruction.cook.CookType.CONSISTENCY;
import static com.example.recipeengine.instruction.cook.CookType.DRY_WET;
import static com.example.recipeengine.instruction.cook.CookType.FRYING;
import static com.example.recipeengine.instruction.cook.CookType.MACRO_SIZE_REDUCTION;
import static com.example.recipeengine.instruction.cook.CookType.TEMPERATURE;
import static com.example.recipeengine.instruction.cook.CookType.WATTAGE;

public class CookingHandlerFactory {
  @Inject
  public CookingHandlerFactory() {
  }

  private Cook cook;

  public BaseCookingHandler getCookingHandlerObject(Cook cook) {
    final CookType cookType = cook.getCookType();
    this.cook = cook;
    return this.getCookingHandlerObject(cookType.getCookType());
  }

  @VisibleForTesting
  BaseCookingHandler getCookingHandlerObject(@AnnotationCookType String cooktype) {
    BaseCookingHandler baseCookingHandler = null;
    switch (cooktype) {
      case BOILING:
        final BoilingCookingHandlerComponent boilingCookingHandlerComponent =
          DaggerBoilingCookingHandlerComponent
            .builder()
            .boilingHandlerModule(new BoilingHandlerModule(this.cook))
            .build();
        baseCookingHandler = boilingCookingHandlerComponent.buildBoilingHandlerObject();
        break;
      case CONSISTENCY:
        final ConsistencyCookingHandlerComponent consistencyCookingHandlerComponent =
          DaggerConsistencyCookingHandlerComponent
            .builder()
            .consistencyHandlerModule(new ConsistencyHandlerModule(this.cook))
            .build();
        baseCookingHandler = consistencyCookingHandlerComponent.buildConsistencyHandlerObject();
        break;
      case DRY_WET:
        final DryWetCookingHandlerComponent dryWetCookingHandlerComponent =
          DaggerDryWetCookingHandlerComponent
            .builder()
            .dryWetHandlerModule(new DryWetHandlerModule(this.cook))
            .build();
        baseCookingHandler = dryWetCookingHandlerComponent.buildDryWetHandlerObject();
        break;
      case FRYING:
        final FryingCookingHandlerComponent fryingCookingHandlerComponent =
          DaggerFryingCookingHandlerComponent.builder()
            .fryingHandlerModule(new FryingHandlerModule(this.cook))
            .build();
        baseCookingHandler = fryingCookingHandlerComponent.buildFryingHandlerObject();
        break;
      case MACRO_SIZE_REDUCTION:
        final MacroSizeCookingHandlerComponent macroSizeCookingHandlerComponent =
          DaggerMacroSizeCookingHandlerComponent
            .builder()
            .macroSizeHandlerModule(new MacroSizeHandlerModule(this.cook))
            .build();
        baseCookingHandler = macroSizeCookingHandlerComponent.buildMacroSizeHandlerObject();
        break;
      case TEMPERATURE:
        final TemperatureHandlerComponent temperatureHandlerComponent =
          DaggerTemperatureHandlerComponent
            .builder()
            .temperatureHandlerModule(new TemperatureHandlerModule(this.cook))
            .build();
        baseCookingHandler = temperatureHandlerComponent.buildTemperatureHandlerObject();
        break;
      case WATTAGE:
        final WattageHandlerComponent wattageHandlerComponent =
          DaggerWattageHandlerComponent
            .builder()
            .wattageHandlerModule(new WattageHandlerModule(this.cook))
            .build();
        baseCookingHandler = wattageHandlerComponent.buildWattageHandlerObject();
        break;
    }

    return baseCookingHandler;
  }

}
