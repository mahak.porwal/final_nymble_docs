package com.example.recipeengine.instruction.handler.cook.macrosizereduction;

import com.example.recipeengine.instruction.handler.cook.handlermodules.MacroSizeHandlerModule;
import dagger.Component;

@Component(modules = MacroSizeHandlerModule.class)
public interface MacroSizeCookingHandlerComponent {

  MacroSizeCookingHandler buildMacroSizeHandlerObject();

}
