package com.example.recipeengine.instruction.dispense.decorator;

import androidx.core.util.Pair;
import com.example.recipeengine.instruction.decorator.exchanges.InstructionInputParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.decorator.source.InstructionDecoration;
import com.example.recipeengine.instruction.decorator.source.InstructionDecorationMap;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;
import com.example.recipeengine.instruction.dispense.BaseDispense;
import java.util.List;
/**
 * This class extends {@link BaseDispenseDecorator} and applies Compensatory Decoration
 * to a {@link com.example.recipeengine.instruction.dispense.Dispense} Object.
 * It overrides the {@link #getQuantityInGrams()} which returns the modified
 * quantityInGrams value.
 */

public class IngredientQuantityDecorator extends BaseDispenseDecorator {

  private List<InstructionLog> instructionLogs;
  private InstructionDecorationMap instructionDecorationMap;

  /**
   * Constructor for DispensedQuantityDecorator.
   *
   * @param wrappedObject contains the dispense object here.
   * @param instructionLogs log of instructions that want to
   *                       decorate the incoming dispense instruction.
   * @param instructionDecorationMap Contains the actual decorations of parameters
   *                                 and target parameters
   */
  public IngredientQuantityDecorator(
      BaseDispense wrappedObject, List<InstructionLog> instructionLogs,
      InstructionDecorationMap instructionDecorationMap) {
    super(wrappedObject);
    this.instructionLogs = instructionLogs;
    this.instructionDecorationMap = instructionDecorationMap;
  }

  @Override
  public Integer getQuantityInGrams() {
    Integer quantity = wrappedObject.getQuantityInGrams() + deltaIngredientQuantity();
    return quantity <= Integer.valueOf(0) ? Integer.valueOf(0) : quantity;
  }

  /**
   * This method is responsible for calculating the difference in quantity
   * that must be added to original quantity.
   * 1. Check if an instructionDecorationMap exists or not,
   * if it doesn't return zero since no change is expected
   * 2. Loop through the List of InstructionLog
   * and check which instruction is a modifier instruction
   * 3. This can be checked using the decoration map if that particular instruction ID
   * is present as a key or not
   * 4. Inside the modifier instruction loop through the OutputParams
   * present and compute the score.
   * 5. The score will be calculated as sum of all
   * params_weight * InstructionInputParams.INGREDIENT_QUANTITY
   * 6. To choose the correct bucket we will need to call the
   * helper method bucketResolver()
   * 7. The score of params is then multiplied to the instruction_weight.
   * 8. The resultant score is then adjusted to contain only the change in value.
   * 9. The adjusted value is then sent back to caller method.
   *
   * @return Integer This returns change in value of quantity in grams.
   */
  private Integer deltaIngredientQuantity() {

    if (instructionDecorationMap == null) {
      return 0;
    }

    Double finalScore = 0.0;
    for (InstructionLog instructionLog : instructionLogs) {
      if (!instructionDecorationMap.getDecorationMap()
          .containsKey(instructionLog.getInstructionId())) {
        continue;
      }
      InstructionDecoration instructionDecoration = instructionDecorationMap
          .getDecorationMap()
          .get(instructionLog.getInstructionId());

      Double score = 0.0;
      for (OutputParams outputParams : instructionDecoration.getParamsDecorationWeight().keySet()) {
        /*
        Bottleneck 1 : No bucket found
        Bottleneck 2 : Output Param does not have INGREDIENT_QUANTITY in it's bucket,
                       How ParamDecoration Weight will affect it.
        */
        score += instructionDecoration.getParamsDecorationWeight().get(outputParams)
            * Double.parseDouble(instructionDecoration.getParameters().get(outputParams)
            .get(bucketResolver(outputParams, instructionLog, instructionDecoration))
            .get(InstructionInputParams.INGREDIENT_QUANTITY));
      }
      finalScore += instructionDecoration.getInstructionWeight() * score;
    }
    finalScore = wrappedObject.getQuantityInGrams() * (finalScore - 1);
    return Math.toIntExact(Math.round(finalScore));
  }

  /**
   * This method is responsible to find the right bucket needed to find
   * the right target param value.
   * 1. Extract the buckets by going through the Discretization Map
   * which contains all the values ranges for an OutputParam
   * 2. Get the pair of values for that particular bucket
   * 3. Read the actual observed value from the instruction log and find out under which
   * bucket it falls
   * 4. Once found return the bucket name.
   * 5. Else return a null value.
   *
   * @return String This returns which bucket is selected based on
   *     the values present in instruction Log
   */
  private String bucketResolver(OutputParams outputParams, InstructionLog instructionLog,
                                InstructionDecoration instructionDecoration) {
    for (String bucket : instructionDecoration.getDiscretizationMap().get(outputParams).keySet()) {
      Pair<Double, Double> pair =
          instructionDecoration.getDiscretizationMap().get(outputParams).get(bucket);
      if (pair.first <= Double.parseDouble(instructionLog.getOutputParams().get(outputParams))
          && pair.second > Double.parseDouble(instructionLog.getOutputParams().get(outputParams))) {
        return bucket;
      }
    }
    return null;
  }
}
