package com.example.recipeengine.instruction.adaptor;

import static android.content.Context.MODE_PRIVATE;

import android.app.Activity;
import android.content.SharedPreferences;
import com.example.recipeengine.instruction.Instruction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class implements {@link Adaptor} and is responsible for containing the
 * template method of our template design pattern and also contains the
 * logic for saving the adapted object locally.
 *
 * @param <T> takes in generic of type Instruction
 */

public abstract class InstructionAdaptor<T extends Instruction> implements Adaptor<T> {
  private T instruction;
  Activity activity;

  public InstructionAdaptor(T instruction) {
    this.instruction = instruction;
  }

  /**
   * This method is the template method which calls the appropriate decoration
   * method based on Attempt number of that particular instruction.
   * It also calls the method that saves it locally and increments the attempt number.
   *
   * @return BaseDispense/BaseSaute
   */
  public T adapt() {
    instruction = instruction.getAttemptNum() == 0 ? this.applyOneTimeDecoration() :
        this.applyCompensatoryDecoration();
    instruction.incrementAttemptNum(); //AttemptNum + 1
    saveInstruction(); // Save Instruction locally on Android OS
    return instruction;
  }

  protected abstract T applyOneTimeDecoration();

  protected abstract T applyCompensatoryDecoration();

  private void saveInstruction() {
    //shared preferences
    //activity = new MainActivity();
    SharedPreferences sharedPreferences =
        activity.getApplicationContext().getSharedPreferences("decoratedInstruction", MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPreferences.edit();
    try {
      editor.putString("instruction", new ObjectMapper().writeValueAsString(instruction));
      editor.apply();
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }
}
