package com.example.recipeengine.instruction.info.dispense;

import com.example.recipeengine.instruction.factory.dispense.DispenseFactory;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;

import org.json.simple.JSONObject;

import javax.inject.Inject;

public class PostDispenseBlockBuilder {
  @Inject
  public PostDispenseBlockBuilder() {
  }

  public PostDispenseBlock providePostDispenseInfo(JSONObject instructionJSON) {
    return DispenseFactory.resolvePostDispenseBlock(instructionJSON);
  }
}
