package com.example.recipeengine.instruction.cookmodules;

import com.example.recipeengine.instruction.cook.params.action.ActionParameters;
import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.cook.params.stirrer.StirrerParams;
import com.example.recipeengine.instruction.cook.params.stirrer.StirrerProfile;
import com.example.recipeengine.instruction.info.cook.ActionInfo;
import com.example.recipeengine.instruction.info.cook.CookInfo;
import com.example.recipeengine.instruction.info.cook.StirrerInfo;

public class CookInfoConverter {
  public static ActionParams provideActionParameters(CookInfo cookInfo) {
    ActionParameters actionParameters = null;
    if (cookInfo.isExecutable()) {
      final ActionInfo actionInfo = cookInfo.getActionInfo();
      actionParameters = new ActionParameters(actionInfo.getTemperature(),
        actionInfo.getTimeToCook(), actionInfo.getVisualScore(), actionInfo.getThermalScore(),
        actionInfo.getDefaultWattage());
    }
    return actionParameters;
  }

  public static StirrerParams provideStirrerParameters(CookInfo cookInfo) {
    StirrerProfile stirrerProfile = null;
    if (cookInfo.isExecutable()) {
      StirrerInfo stirrerInfo = cookInfo.getStirrerInfo();
      stirrerProfile = new StirrerProfile(stirrerInfo.getCycleDurationSeconds(),
        stirrerInfo.getStirringPercent(), stirrerInfo.getStirringSpeed());
    }
    return stirrerProfile;
  }

  public static InferParams provideInferParameters(CookInfo cookInfo) {
    return cookInfo.getInferParams();
  }

  public static CaptureParams provideCaptureParameters(CookInfo cookInfo) {
    return cookInfo.getCaptureParams();
  }
}
