package com.example.recipeengine.instruction.info.dispense;

import org.json.simple.JSONObject;

import javax.inject.Inject;


public class QuantityInfoBuilder {
  @Inject
  public QuantityInfoBuilder() {
  }

  String provideQuantityInfo(JSONObject instructionJSON) {
    String quantityInfo = ((String) instructionJSON.get("quantityInGrams"));
    return quantityInfo;
  }
}
