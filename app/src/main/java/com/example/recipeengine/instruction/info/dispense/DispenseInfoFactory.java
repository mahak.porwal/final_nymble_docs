package com.example.recipeengine.instruction.info.dispense;

import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;
import com.example.recipeengine.instruction.info.InstructionInfoBuilder;
import com.example.recipeengine.util.JSONObjectUtil;

import org.json.simple.JSONObject;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;


public class DispenseInfoFactory {

  private QuantityInfoBuilder quantityInfoBuilder;
  private IngredientListBuilder ingredientListBuilder;
  private PreDispenseBlockBuilder preDispenseBlockBuilder;
  private DispenseBlockBuilder dispenseBlockBuilder;
  private PostDispenseBlockBuilder postDispenseBlockBuilder;
  private InferInstructionListBuilder inferInstructionListBuilder;
  private ParametricFieldsBuilder parametricFieldsBuilder;

  @Inject
  public DispenseInfoFactory(QuantityInfoBuilder quantityInfoBuilder,
                             IngredientListBuilder ingredientListBuilder,
                             PreDispenseBlockBuilder preDispenseBlockBuilder,
                             DispenseBlockBuilder dispenseBlockBuilder,
                             PostDispenseBlockBuilder postDispenseBlockBuilder,
                             InferInstructionListBuilder inferInstructionListBuilder,
                             ParametricFieldsBuilder parametricFieldsBuilder) {
    this.quantityInfoBuilder = quantityInfoBuilder;
    this.ingredientListBuilder = ingredientListBuilder;
    this.preDispenseBlockBuilder = preDispenseBlockBuilder;
    this.dispenseBlockBuilder = dispenseBlockBuilder;
    this.postDispenseBlockBuilder = postDispenseBlockBuilder;
    this.inferInstructionListBuilder = inferInstructionListBuilder;
    this.parametricFieldsBuilder = parametricFieldsBuilder;
  }

  public DispenseInfo createDispenseInfo(String filename) {
    final JSONObject jsonObject = JSONObjectUtil.createJSONObject(filename);
    return this.createDispenseInfo(jsonObject);
  }

  public DispenseInfo createDispenseInfo(JSONObject jsonObject) {
    final PreDispenseBlock preDispenseBlock = this.preDispenseBlockBuilder.providePreDispenseInfo(jsonObject);
    final DispenseBlock dispenseBlock = this.dispenseBlockBuilder.provideDispenseInfo(jsonObject);
    final PostDispenseBlock postDispenseBlock = this.postDispenseBlockBuilder.providePostDispenseInfo(jsonObject);
    final List<InferInstruction> inferInstructions = this.inferInstructionListBuilder.provideListInferInstructions(jsonObject);
    final String quantityInfo = this.quantityInfoBuilder.provideQuantityInfo(jsonObject);
    final List<String> ingredientList = this.ingredientListBuilder.provideIngredientList(jsonObject);
    final Map<String, String> parametricFields = this.parametricFieldsBuilder.provideParametricFields(jsonObject);
    final Integer instructionId = InstructionInfoBuilder.buildInstructionId(jsonObject);
    final Integer attemptNum = InstructionInfoBuilder.buildAttemptNum(jsonObject);
    return new BaseDispenseInfo(quantityInfo, ingredientList, preDispenseBlock, dispenseBlock, postDispenseBlock,
      inferInstructions, parametricFields, instructionId, attemptNum);
  }
}
