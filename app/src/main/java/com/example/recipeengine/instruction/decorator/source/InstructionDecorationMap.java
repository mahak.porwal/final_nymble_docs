package com.example.recipeengine.instruction.decorator.source;

import java.util.Map;
import lombok.NoArgsConstructor;
/**
 * This class contains a map of Instruction ID and {@link InstructionDecoration} which contains,
 * information about modifier parameters and decoration on target instruction.
 */

@NoArgsConstructor
public class InstructionDecorationMap {
  private Map<Integer, InstructionDecoration> decorationMap;

  /**
   * Constructor of {{@link InstructionDecorationMap}.
   *
   * @param decorationMap of type {@link InstructionDecorationMap}
   */
  public InstructionDecorationMap(
      Map<Integer, InstructionDecoration> decorationMap) {
    this.decorationMap = decorationMap;
  }

  public Map<Integer, InstructionDecoration> getDecorationMap() {
    return decorationMap;
  }

  public void setDecorationMap(
      Map<Integer, InstructionDecoration> decorationMap) {
    this.decorationMap = decorationMap;
  }
}
