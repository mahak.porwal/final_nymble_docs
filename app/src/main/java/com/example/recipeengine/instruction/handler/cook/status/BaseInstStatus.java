package com.example.recipeengine.instruction.handler.cook.status;

public interface BaseInstStatus {

  String getStatus();

  String getVerboseStatus();

  Integer remainingTimeSec();

}
