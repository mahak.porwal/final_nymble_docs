package com.example.recipeengine.instruction.task.dispense;

import androidx.annotation.NonNull;

import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseHwInformation;
import com.example.recipeengine.instruction.task.capture.base.Task;

public class DispenseTask implements Task {

  private DispenseHwInformation dispenseHwInformation;
  private Thread thread;
  private String taskName;

  public DispenseTask(@NonNull DispenseHwInformation dispenseHwInformation) {
    this.dispenseHwInformation = dispenseHwInformation;
    this.taskName = "Dispense task";
  }

  @Override
  public void beginTask() {
    this.thread = new DispenseTaskThread(this.taskName,
      this.dispenseHwInformation);
    this.thread.start();
  }

  @Override
  public void abortTask() {
    if (this.thread != null) {
      this.thread.interrupt();
    }
  }

  @Override
  public Thread getThread() {
    return this.thread;
  }

  @Override
  public String getTaskName() {
    return this.taskName;
  }
}

