package com.example.recipeengine.instruction.handler.cook.factory;

import static com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper.resolveVisionCaptureTask;
import static com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper.resolveWeightCaptureTask;

import com.example.recipeengine.instruction.blocks.capture.BaseCaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CaptureBlock;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParameters;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import com.example.recipeengine.instruction.task.capture.vision.VisionCapture;
import com.example.recipeengine.instruction.task.capture.weight.WeightCapture;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Generated CaptureBlock object.
 */
public class CaptureBlockFactory {

  /**
   * Creates an CaptureBlock object irrespective of cooking type.
   *
   * @return CaptureBlock object
   */
  public static CaptureBlock getCaptureBlockObject() {

    SensorParams sensorParams =
        new SensorParams(new VisualCaptureType(VisualCaptureType.VISION_CAMERA),
            new ThermalType(ThermalType.IMAGE), 10, 501, null, null, null, null);
    VisionCapture visionCapture =
        new VisionCapture("PRE_DISPENSE_IMAGE", sensorParams);
    WeightCapture weightCapture = new WeightCapture("PRE_DISPENSE_WEIGHT",
        new SensorParams(null, null, 10, 10,
            null, null, null, null)
    );
    List<CapturableTask> capturableTasks = new ArrayList<>();
    capturableTasks.add(visionCapture);
    capturableTasks.add(weightCapture);
    List<String> taskNames = new ArrayList<String>() {{
      add("PRE_DISPENSE_IMAGE");
      add("POST_DISPENSE_WEIGHT");
    }};
    CaptureParameters captureParameters = new CaptureParameters(capturableTasks, taskNames);
    return new BaseCaptureBlock(captureParameters);
  }

  public static CaptureBlock getCaptureBlockObjectJson(Map jsonObject) {

    Map addressCapture = ((Map) jsonObject.get("captureParams"));
    List address = (List) addressCapture.get("capturableTasks");
    List source = (List) addressCapture.get("sourceTasks");
    List<CapturableTask> captureTasks = new ArrayList<>();
    if (address != null) {
      for (int i = 0; i < address.size(); i++) {
        Map captureTaskMap = (Map) address.get(i);
        if (captureTaskMap.get("captureSensor").equals("WeightSensor")) {
          captureTasks.add(resolveWeightCaptureTask(captureTaskMap));
        } else {
          captureTasks.add(resolveVisionCaptureTask(captureTaskMap));
        }
      }
    }
    List<String> sourceTasks = new ArrayList<>();
    if (source != null) {
      for (int i = 0; i < source.size(); i++) {
        sourceTasks.add((String) source.get(i));
      }

    }
    CaptureParameters captureParameters = new CaptureParameters(captureTasks, sourceTasks);
    return new BaseCaptureBlock(captureParameters);
  }

}
