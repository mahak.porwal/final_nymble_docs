package com.example.recipeengine.instruction.info.cook;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

public class StirrerInfoBuilder {
  @Inject
  public StirrerInfoBuilder() {
  }

  public StirrerInfo provideStirrerProfile(JSONObject jsonObject) {
    StirrerInfo stirrerInfo = null;
    try {
      stirrerInfo = createStirrerInfoObject(jsonObject);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return stirrerInfo;
  }

  private StirrerInfo createStirrerInfoObject(JSONObject jsonObject) throws IOException, ParseException {
    Map address = ((Map) jsonObject.get("stirrerParams"));
    return new StirrerInformation(address.get("cycleDurationSeconds").toString(),
      address.get("stirringPercent").toString(),
      address.get("stirringSpeed").toString());
  }
}
