package com.example.recipeengine.instruction.decorator.exchanges;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum OutputParams {
  @JsonProperty("CONSISTENCY_SCORE")
  CONSISTENCY_SCORE,
  @JsonProperty("FRYING_SCORE")
  FRYING_SCORE,
  @JsonProperty("DRY_SCORE")
  DRY_SCORE,
  @JsonProperty("FOOD_PERCENTAGE_SCORE")
  FOOD_PERCENTAGE,
  @JsonProperty("BOILING_SCORE")
  BOILING_SCORE,
  @JsonProperty("ONION_SIZE_SCORE")
  ONION_SIZE_SCORE,
  @JsonProperty("POTATO_SIZE_SCORE")
  POTATO_SIZE_SCORE,
  @JsonProperty("DISPENSED_WEIGHT")
  DISPENSED_WEIGHT
}
