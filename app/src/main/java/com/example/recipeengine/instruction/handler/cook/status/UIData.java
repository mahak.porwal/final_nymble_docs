package com.example.recipeengine.instruction.handler.cook.status;

import android.graphics.Bitmap;
import java.util.Map;

/**
 * This class stores the data to be sent to UI.
 */
public class UIData {

  private Bitmap image;
  private Map<String, String> data;

  /**
   * Constructor of UIData class.
   *
   * @param image image in the form of Bitmap
   * @param data  Information stored in a Map.
   */
  public UIData(Bitmap image, Map<String, String> data) {
    this.image = image;
    this.data = data;
  }

  public Bitmap getImage() {
    return image;
  }

  public void setImage(Bitmap image) {
    this.image = image;
  }

  public Map<String, String> getData() {
    return data;
  }

  public void setData(Map<String, String> data) {
    this.data = data;
  }
}
