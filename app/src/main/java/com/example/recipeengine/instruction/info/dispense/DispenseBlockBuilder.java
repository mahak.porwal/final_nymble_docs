package com.example.recipeengine.instruction.info.dispense;

import com.example.recipeengine.instruction.factory.dispense.DispenseFactory;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;

import org.json.simple.JSONObject;

import javax.inject.Inject;

public class DispenseBlockBuilder {
  @Inject
  public DispenseBlockBuilder() {
  }

  public DispenseBlock provideDispenseInfo(JSONObject instructionJSON) {
    return DispenseFactory.resolveDispenseBlock(instructionJSON);
  }
}
