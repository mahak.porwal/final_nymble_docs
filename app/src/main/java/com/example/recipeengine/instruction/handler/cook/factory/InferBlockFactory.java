package com.example.recipeengine.instruction.handler.cook.factory;

import static com.example.recipeengine.instruction.cook.CookType.BOILING;
import static com.example.recipeengine.instruction.cook.CookType.CONSISTENCY;
import static com.example.recipeengine.instruction.cook.CookType.DRY_WET;
import static com.example.recipeengine.instruction.cook.CookType.FRYING;
import static com.example.recipeengine.instruction.cook.CookType.MACRO_SIZE_REDUCTION;
import static com.example.recipeengine.instruction.cook.CookType.TEMPERATURE;
import static com.example.recipeengine.instruction.cook.CookType.WATTAGE;

import com.example.recipeengine.instruction.cook.params.infer.InferParameters;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.handler.cook.boiling.BoilingInfer;
import com.example.recipeengine.instruction.handler.cook.consistency.ConsistencyInfer;
import com.example.recipeengine.instruction.handler.cook.drywet.DryWetInfer;
import com.example.recipeengine.instruction.handler.cook.frying.FryingInfer;
import com.example.recipeengine.instruction.handler.cook.infer.InferBlock;
import com.example.recipeengine.instruction.handler.cook.macrosizereduction.MacroSizeInfer;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.instruction.handler.cook.temperature.TemperatureInfer;
import com.example.recipeengine.instruction.handler.cook.wattage.WattageInfer;
import com.example.recipeengine.instruction.handler.dispense.blocks.infer.OnionInfer;
import io.reactivex.subjects.PublishSubject;
import java.util.Map;

/**
 * Generated InferBlock object.
 */
public class InferBlockFactory {

  /**
   * Creates an InferBlock object based cooking type.
   *
   * @param cookingType cooking type
   * @return InferBlock object
   */
  public static InferBlock getInferBlockObject(String cookingType, InferParams inferParameters) {
    InferBlock inferBlock = null;
    PublishSubject<UIData> uiData;
    PublishSubject<ProgressData> progressData;
    switch (cookingType) {
      case BOILING:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        inferBlock = new BoilingInfer(new RecipeContext(5, "kheer"), uiData, progressData);
        break;
      case CONSISTENCY:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        inferBlock = new ConsistencyInfer(inferParameters,
            new RecipeContext(5, "kheer"), uiData, progressData);
        break;
      case FRYING:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        inferBlock = new FryingInfer(inferParameters,
            new RecipeContext(5, "kheer"), uiData, progressData);
        break;
      case DRY_WET:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        inferBlock = new DryWetInfer(inferParameters,
            new RecipeContext(5, "kheer"), uiData, progressData);
        break;
      case MACRO_SIZE_REDUCTION:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        inferBlock = new MacroSizeInfer(inferParameters,
            new RecipeContext(5, "kheer"), uiData, progressData);
        break;
      case WATTAGE:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        inferBlock = new WattageInfer(null, uiData, progressData);
        break;
      case TEMPERATURE:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        inferBlock = new TemperatureInfer(inferParameters,
            new RecipeContext(5, "kheer"), uiData, progressData);
        break;
      default:
        break;
    }

    return inferBlock;
  }

  public static InferBlock getDispenseInferBlockObject(Map jsonObject) {
    InferBlock inferBlock = null;
    PublishSubject<UIData> uiData;
    PublishSubject<ProgressData> progressData;
    Map inferParams = ((Map) jsonObject.get("inferParams"));
    String dispenseInfer = (String) inferParams.get("dispenseInfer");
    switch (dispenseInfer) {
      case "ingredient":
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        inferBlock = new OnionInfer(new RecipeContext(2, "demo"),
            new InferParameters((String) inferParams.get("modelId"),
                (String) inferParams.get("modelFileName")), uiData, progressData);
        break;
      default:
        break;
    }

    return inferBlock;
  }

}
