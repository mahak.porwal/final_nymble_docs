package com.example.recipeengine.instruction.handler;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.blocks.InstructionHandler;
import com.example.recipeengine.instruction.cook.BaseCook;
import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.dispense.Dispense;
import com.example.recipeengine.instruction.handler.cook.factory.CookingHandlerFactory;
import com.example.recipeengine.instruction.handler.dispense.base.DispenseHandler;

import javax.inject.Inject;

public class HandlerFactory {
  private CookingHandlerFactory cookingHandlerFactory;

  @Inject
  public HandlerFactory(CookingHandlerFactory cookingHandlerFactory) {
    this.cookingHandlerFactory = cookingHandlerFactory;
  }

  public InstructionHandler createInstructionHandler(Instruction instruction) {
    InstructionHandler instructionHandler = null;
    if (instruction instanceof BaseCook) {
      Cook cook = (Cook) instruction;
      instructionHandler = this.cookingHandlerFactory.getCookingHandlerObject(cook);
    } else {
      Dispense dispense = (Dispense) instruction;
      instructionHandler = new DispenseHandler(dispense);
    }
    return instructionHandler;
  }
}
