package com.example.recipeengine.instruction.task.capture.base;

import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.CaptureSensor;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import som.hardware.request.handler.RequestHandler;

/**
 * This class contains the abstract methods for a
 * {@link CapturableTask}.
 */
public abstract class BaseCapturableTask implements CapturableTask {

  protected CaptureSensor captureSensor;
  protected String taskName;
  protected CapturedTaskData capturedTaskData;
  protected SensorParams sensorParams;
  protected RequestHandler requestHandler;

  /**
   * Constructor for BaseCapturableTask.
   *
   * @param captureSensor captureSensor
   * @param sensorParams sensorParams
   * @param requestHandler requestHandler
   */
  public BaseCapturableTask(
      CaptureSensor captureSensor,
      SensorParams sensorParams, RequestHandler requestHandler) {
    this.captureSensor = captureSensor;
    this.sensorParams = sensorParams;
    this.requestHandler = requestHandler;
  }

  @Override
  public CaptureSensor getSensor() {
    return captureSensor;
  }

  @Override
  public SensorParams getParams() {
    return sensorParams;
  }

  @Override
  public CapturedTaskData getCapturedData() {
    return capturedTaskData;
  }

  public void setCapturedData(
      CapturedTaskData capturedTaskData) {
    this.capturedTaskData = capturedTaskData;
  }

  public abstract void beginTask();

  public abstract void abortTask();

  @Override
  public String getTaskName() {
    return taskName;
  }

}
