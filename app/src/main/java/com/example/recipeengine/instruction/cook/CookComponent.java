package com.example.recipeengine.instruction.cook;

//@Component(modules = CookParamsModule.class)
public interface CookComponent {
  /**
   * Returns a Saute object with injected values.
   * @return {@link Cook}
   */
  Cook buildCookObject();
}
