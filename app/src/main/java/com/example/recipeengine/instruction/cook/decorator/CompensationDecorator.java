package com.example.recipeengine.instruction.cook.decorator;

import com.example.recipeengine.instruction.cook.BaseCook;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;

public class CompensationDecorator extends BaseCookDecorator {

  private InstructionLog instructionLog;

  public CompensationDecorator(BaseCook wrappedObject,
                               InstructionLog instructionLog) {
    super(wrappedObject);
    this.instructionLog = instructionLog;
  }
}
