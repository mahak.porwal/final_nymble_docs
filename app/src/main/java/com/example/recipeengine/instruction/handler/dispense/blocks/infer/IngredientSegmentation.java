package com.example.recipeengine.instruction.handler.dispense.blocks.infer;

import static com.example.recipeengine.util.ImageUtil.scaleBitmapAndKeepRatio;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;
import com.example.recipeengine.util.FileUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.gpu.GpuDelegate;
import timber.log.Timber;

/**
 * IngredientSegmentation class.
 */
public class IngredientSegmentation {
  public static final int IMAGE_SIZE = 256;
  public static final int OUTPUT_SIZE = 256;
  public static final int OUT_ACTUAL_SIZE = 1080;
  private static final int NUM_CLASSES = 4;
  private static final float[] IMG_MEAN = {0.485f, 0.456f, 0.406f};
  private static final float[] IMG_STD = {0.229f, 0.224f, 0.225f};
  private static final float IMAGE_NORM = 255.0f;
  private final Interpreter.Options tfliteOptions = new Interpreter.Options();
  private Interpreter interpreter;
  private GpuDelegate gpuDelegate;
  private String modelName;
  private long timeLogger;

  /**
   * Constructor for IngredientSegmentation class.
   *
   * @param modelName name of the model
   */
  public IngredientSegmentation(String modelName) {
    this.gpuDelegate = new GpuDelegate();
    this.tfliteOptions.addDelegate(this.gpuDelegate);
    this.tfliteOptions.setNumThreads(1);
    this.modelName = modelName;
  }

  /**
   * Converting bitmap image to tensorflow lite byte buffer for model's input.
   *
   * @param inputImage Bitmap of image
   * @return ByteBuffer
   */
  public static ByteBuffer bitmapToByteBuffer(Bitmap inputImage) {
    final Bitmap image = scaleBitmapAndKeepRatio(inputImage, IMAGE_SIZE, IMAGE_SIZE);
    final ByteBuffer inputBuffer = ByteBuffer.allocateDirect(IMAGE_SIZE * IMAGE_SIZE * 3 * 4);
    inputBuffer.order(ByteOrder.nativeOrder());
    inputBuffer.rewind();
    final int[] intValues = new int[IMAGE_SIZE * IMAGE_SIZE];
    image.getPixels(intValues, 0, IMAGE_SIZE, 0, 0, IMAGE_SIZE, IMAGE_SIZE);
    int pixel = 0;
    for (int i = 0; IMAGE_SIZE > i; i++) {
      for (int j = 0; IMAGE_SIZE > j; j++) {
        final int value = intValues[pixel];
        pixel++;
        inputBuffer.putFloat((((((value >> 16) & 0xff) / IMAGE_NORM)) - IMG_MEAN[0]) / IMG_STD[0]);
        inputBuffer.putFloat((((((value >> 8) & 0xff) / IMAGE_NORM)) - IMG_MEAN[1]) / IMG_STD[1]);
        inputBuffer.putFloat((((((value) & 0xff) / IMAGE_NORM)) - IMG_MEAN[2]) / IMG_STD[2]);

      }
    }
    //inputBuffer.rewind();
    return inputBuffer;
  }


  /**
   * Convert Gray scale Mat to Bitmap.
   */
  public static Bitmap parseBitmapGrey(Mat mat) {
    final Mat img = new Mat();
    Imgproc.cvtColor(mat, img, Imgproc.COLOR_GRAY2RGB);
    Bitmap bmp = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
    Utils.matToBitmap(img, bmp);
    return bmp;
  }

  /**
   * Load model when required.
   *
   * @throws IOException exception
   */
  public void loadModel() throws IOException {
    MappedByteBuffer mappedByteBuffer = fetchModelFile();
    this.interpreter = new Interpreter(mappedByteBuffer, this.tfliteOptions);
  }

  /**
   * Run a segmentation on the image.
   */
  public Mat runSegmentation(final Bitmap bitmap) {
    final ByteBuffer contentArray = bitmapToByteBuffer(bitmap);
    final ByteBuffer segmentationMasks =
        ByteBuffer.allocateDirect(OUTPUT_SIZE * OUTPUT_SIZE * NUM_CLASSES * 4);
    segmentationMasks.order(ByteOrder.nativeOrder());
    this.timeLogger = System.currentTimeMillis();
    this.interpreter.run(contentArray, segmentationMasks);
    Timber.d("inference time %d", System.currentTimeMillis() - this.timeLogger);
    return createMask(segmentationMasks);
  }

  /**
   * Creating a mask image from output of model.
   *
   * @param buffer : model's output
   * @return Bitmap: segmented mask
   */
  private Mat createMask(ByteBuffer buffer) {
    this.timeLogger = System.currentTimeMillis();
    buffer.rewind();
    final int[][] seg = new int[OUTPUT_SIZE][OUTPUT_SIZE];
    Long startTime1 = System.currentTimeMillis();
    for (int i = 0; OUTPUT_SIZE > i; i++) {
      Long startTime = System.currentTimeMillis();
      for (int j = 0; OUTPUT_SIZE > j; j++) {
        float maxVal = 0;
        seg[j][i] = 0;
        for (int c = 0; NUM_CLASSES > c; c++) {
          final float value =
              buffer.getFloat((i * OUTPUT_SIZE * NUM_CLASSES + j * NUM_CLASSES + c) * 4);
          if (0 == c || value > maxVal) {
            maxVal = value;
            seg[j][i] = c;
          }
        }
      }
      Log.d("LOOP_TIME1", "Loop 1 Time Taken : " + (System.currentTimeMillis() - startTime));
    }
    Log.d("LOOP_TIME2", "Loop 2 Time Taken : " + (System.currentTimeMillis() - startTime1));
    final Mat segImage = new Mat(OUTPUT_SIZE, OUTPUT_SIZE, CvType.CV_8UC1);
    //startTime = System.currentTimeMillis();
    for (int i = 0; OUTPUT_SIZE > i; i++) {
      for (int j = 0; OUTPUT_SIZE > j; j++) {
        if (0 == seg[j][i]) {
          final byte[] data = {0};
          segImage.put(i, j, data);
        } else if (1 == seg[j][i]) {
          final byte[] data = {50};
          segImage.put(i, j, data);
        } else if (2 == seg[j][i]) {
          final byte[] data = {100};
          segImage.put(i, j, data);
        } else if(3 == seg[j][i]) {
          final byte[] data = {127};
          segImage.put(i,j,data);
        }
      }
    }
    //Log.d("LOOP_TIME", "Loop 2 Time Taken : " + (System.currentTimeMillis() - startTime));
    //Imgproc.resize(segImage, segImage,
    //new Size(OUT_ACTUAL_SIZE, OUT_ACTUAL_SIZE), 0, 0, Imgproc.INTER_NEAREST);
    Timber.d("Create mask time %d", System.currentTimeMillis() - this.timeLogger);
    return segImage;
  }

  /**
   * Fetch the model the file.
   *
   * @return MappedByteBuffer
   * @throws IOException exception
   */
  public MappedByteBuffer fetchModelFile() throws IOException {
    final File modelFile = FileUtil.getModelFile(this.modelName);
    final FileInputStream inputStream = new FileInputStream(modelFile);
    final FileChannel fileChannel = inputStream.getChannel();
    return fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
  }

  /**
   * Closes tflite to release resources.
   */
  public void close() {
    if (null != this.gpuDelegate) {
      this.gpuDelegate.close();
      this.gpuDelegate = null;
    }
    if (null != this.interpreter) {
      this.interpreter.close();
      this.interpreter = null;
    }
  }
}

