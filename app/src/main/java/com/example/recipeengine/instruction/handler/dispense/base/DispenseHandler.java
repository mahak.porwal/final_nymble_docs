package com.example.recipeengine.instruction.handler.dispense.base;

import com.example.recipeengine.instruction.blocks.Block;
import com.example.recipeengine.instruction.blocks.InstructionHandler;
import com.example.recipeengine.instruction.blocks.capture.CaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.dispense.BaseDispense;
import com.example.recipeengine.instruction.dispense.Dispense;
import com.example.recipeengine.instruction.handler.cook.action.ActionBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;
import com.example.recipeengine.instruction.handler.cook.status.InstructionStatus;
import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;
import com.example.recipeengine.instruction.task.capture.base.CapturedDataProvider;
import com.example.recipeengine.instruction.task.capture.base.Task;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for handling a {@link Dispense}
 * instruction.
 * It implements the methods of {@link InstructionHandler}
 */
public class DispenseHandler implements InstructionHandler {

  private PreDispenseBlock preDispenseBlock;
  private DispenseBlock dispenseBlock;
  private PostDispenseBlock postDispenseBlock;
  private List<InferInstruction> inferInstructions;
  private Block currentBlock;
  private List<CapturedDataProvider> executedTasks;

  /**
   * Constructor of DispenseHandler class.
   *
   * @param dispenseInstruction Dispense Instruction
   */
  public DispenseHandler(BaseDispense dispenseInstruction) {
    this.preDispenseBlock = dispenseInstruction.getPreDispenseBlock();
    this.dispenseBlock = dispenseInstruction.getDispenseBlock();
    this.postDispenseBlock = dispenseInstruction.getPostDispenseBlock();
    this.inferInstructions = dispenseInstruction.getInferInstructions();
    this.executedTasks = new ArrayList<>();
  }

  /**
   * This method runs the blocks present in the Dispense instruction.
   * It first executes {@link PreDispenseBlock#startSequentialTasks()}
   * Then it executes {@link DispenseBlock#startParallelTasks()}
   * And finally it executes {@link PostDispenseBlock#startSequentialTasks()}
   * Then it runs the list of InferInstructions by calling
   * {@link #handleInferInstruction(InferInstruction)}
   *
   * @throws Exception Exception thrown
   */
  @Override
  public void handle() throws Exception {
    currentBlock = preDispenseBlock;
    preDispenseBlock.startSequentialTasks();
    addExecutedTasks(preDispenseBlock.getSequentialTasks());
    currentBlock = dispenseBlock;
    dispenseBlock.startParallelTasks();
    addExecutedTasks(dispenseBlock.getParallelTasks());
    currentBlock = postDispenseBlock;
    postDispenseBlock.startSequentialTasks();
    addExecutedTasks(postDispenseBlock.getSequentialTasks());

    if (inferInstructions != null && inferInstructions.size() != 0) {
      for (InferInstruction inferInstruction : inferInstructions) {
        handleInferInstruction(inferInstruction);
      }
    }

  }

  @Override
  public List<CapturedDataProvider> getExecutedTasks() {
    return executedTasks;
  }

  /**
   * This method adds the tasks of type {@link CapturedDataProvider} to
   * the {@link #executedTasks} list.
   *
   * @param tasks list of tasks
   */
  private void addExecutedTasks(List<? extends Task> tasks) {
    for (Task task : tasks) {
      if (task instanceof CapturedDataProvider) {
        executedTasks.add((CapturedDataProvider) task);
      }
    }
  }

  /**
   * This method handles a single InferInstruction.
   * It runs the {@link CaptureBlock#getCapturedData()} the
   * result is then passed to {@link InferBlock#infer(List)}
   * Then it checks if {@link ActionBlock} is
   * present or not then it runs {@link ActionBlock#takeAction(InferredResult, ActionParams)}
   *
   * @param inferInstruction Infer Instruction
   * @throws Exception Exception
   */
  private void handleInferInstruction(InferInstruction inferInstruction) throws Exception {
    currentBlock = inferInstruction.getCaptureBlock();
    inferInstruction.getCaptureBlock().setExecutedTasks(executedTasks);
    List<CapturedTaskData> capturedTaskData = inferInstruction.getCaptureBlock().getCapturedData();
    currentBlock = inferInstruction.getInferBlock();
    InferredResult inferredResult = inferInstruction.getInferBlock().infer(capturedTaskData);
    if (inferInstruction.getActionBlock() != null) {
      currentBlock = inferInstruction.getActionBlock();
      //TO BE COMPLETED
      inferInstruction.getActionBlock().takeAction(inferredResult, null);
    }
  }

  @Override
  public BaseInstStatus getStatus() {
    //TO BE COMPLETED
    return new InstructionStatus(currentBlock.getBlockStatus(), null, null);
  }

  @Override
  public void pause() throws Exception {
    currentBlock.pause();
  }
}
