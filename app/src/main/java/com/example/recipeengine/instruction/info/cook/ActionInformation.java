package com.example.recipeengine.instruction.info.cook;

public class ActionInformation implements ActionInfo {

  private String targetTemperatureString;
  private String timeToCookString;
  private String visualScoreString;
  private String thermalScoreString;
  private String defaultWattageString;

  public ActionInformation(String targetTemperatureString, String timeToCookString,
                           String visualScoreString, String thermalScoreString, String defaultWattageString) {
    this.targetTemperatureString = targetTemperatureString;
    this.timeToCookString = timeToCookString;
    this.visualScoreString = visualScoreString;
    this.thermalScoreString = thermalScoreString;
    this.defaultWattageString = defaultWattageString;
  }


  @Override
  public String getDefaultWattageString() {
    return this.defaultWattageString;
  }

  @Override
  public String getTimeToCookString() {
    return this.timeToCookString;
  }

  @Override
  public String getVisualScoreString() {
    return this.visualScoreString;
  }

  @Override
  public String getTemperatureString() {
    return this.targetTemperatureString;
  }

  @Override
  public String getThermalScoreString() {
    return this.thermalScoreString;
  }

  @Override
  public void setDefaultWattage(Integer defaultWattage) {
    this.defaultWattageString = String.valueOf(defaultWattage);
  }

  @Override
  public void setTimeToCook(Integer timeToCook) {
    this.timeToCookString = String.valueOf(timeToCook);
  }

  @Override
  public void setVisualScore(Double visualScore) {
    this.visualScoreString = String.valueOf(visualScore);
  }

  @Override
  public void setTemperature(Integer temperature) {
    this.targetTemperatureString = String.valueOf(temperature);
  }

  @Override
  public void setThermalScore(Double thermalScore) {
    this.thermalScoreString = String.valueOf(thermalScore);
  }

  @Override
  public Integer getDefaultWattage() {
    if(this.defaultWattageString == null) {
      return null;
    }
    Double value = Double.parseDouble(this.defaultWattageString);
    return value.intValue();
  }

  @Override
  public Integer getTimeToCook() {
    if(this.timeToCookString == null) {
      return null;
    }
    Double value = Double.parseDouble(this.timeToCookString);
    return value.intValue();
  }

  @Override
  public Double getVisualScore() {
    if(this.visualScoreString == null) {
      return null;
    }
    return Double.valueOf(this.visualScoreString);
  }

  @Override
  public Integer getTemperature() {
    if(this.targetTemperatureString == null) {
      return null;
    }
    Double value = Double.parseDouble(this.targetTemperatureString);
    return value.intValue();
  }

  @Override
  public Double getThermalScore() {
    if(this.thermalScoreString == null) {
      return null;
    }
    return Double.valueOf(this.thermalScoreString);
  }
}
