package com.example.recipeengine.instruction.blocks.capture;

import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.handler.dispense.base.BaseParallelBlock;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import com.example.recipeengine.instruction.task.capture.base.CapturedDataProvider;
import com.example.recipeengine.instruction.task.capture.base.Task;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for running the {@link CapturableTask} in
 * parallel using {@link #startCapture()}
 * also it stores the results in {@link CapturedTaskData} using
 * {@link #consolidateTaskResult(List)} .
 */
public class BaseCaptureBlock extends BaseParallelBlock implements CaptureBlock {

  private CaptureParams captureParams;
  private List<CapturedTaskData> capturedBlockData;
  private String captureStatus;
  private List<CapturedDataProvider> executedTasks;

  /**
   * Constructor of BaseCaptureBlock.
   *
   * @param captureParams All params required for capture
   */

  public BaseCaptureBlock(
      CaptureParams captureParams) {
    super(captureParams.getCaptureTasks());
    this.captureParams = captureParams;
    this.captureStatus = "Capture Status";

  }

  /**
   * The client uses this method to get the List of {@link CapturedTaskData}.
   * 1. It first checks if the {@link #capturedBlockData} member is null or not
   * 2. Then it calls the {@link #startCapture()} method, which
   * returns List of {@link CapturedTaskData}.
   * 3. The same is returned to parent method.
   *
   * @return List of {@link CapturedTaskData}
   * @throws Exception Exception
   */
  public List<CapturedTaskData> getCapturedData() throws Exception {
    return startCapture();
  }

  /**
   * This method has 3 main conditions.
   * 1. Check if captureParams.getCaptureTasks() is not null &&
   * captureParams.getSourceTasks() is null :
   * 1.1. Run startParallelTasks()
   * 1.2. consolidateTaskResults(parallelTasks)
   * 2. Check if captureParams.getCaptureTasks() is null &&
   * captureParams.getSourceTasks() is not null
   * 2.1 Call the method consolidateTaskResultExecuted() to
   * generate the results.
   * 2.2 If taskName is not found in executedTasks throw an exception.
   * 3. Check if captureParams.getCaptureTasks() is not null &&
   * captureParams.getSourceTasks() is not null :
   * 3.1 Run all parallel tasks.
   * 3.2 Call method consolidateTaskResultBothNonNull() to return results.
   *
   * @return List of {@link CapturedTaskData}
   * @throws Exception Exception
   */
  private List<CapturedTaskData> startCapture()
      throws Exception {
    List<CapturableTask> capturableTasks = captureParams.getCaptureTasks();
    if(capturableTasks != null) {
      for (CapturableTask capturableTask : capturableTasks) {
        capturableTask.resetCapturedTaskData();
      }
    }
    List<String> sourceTasks = captureParams.getSourceTasks();
    List<CapturedTaskData> capturedTaskData = null;
    if (capturableTasks != null) {
      startParallelTasks();
      capturedTaskData = consolidateTaskResult(capturableTasks);
    } else if (sourceTasks != null) {
      capturedTaskData = consolidateTaskResult(null);
    }
    return capturedTaskData;
  }


  @Override
  public String getBlockStatus() {
    return captureStatus;
  }

  @Override
  public void pause() {
    for (Task task : captureParams.getCaptureTasks()) {
      task.abortTask();
    }
  }

  /**
   * Runs all the tasks by calling {@link Task#beginTask()} method.
   * It then waits for threads to complete by using join() method.
   * The tasks are then added to executed tasks list.
   *
   * @throws InterruptedException Exception
   */
  @Override
  public void startParallelTasks() throws InterruptedException {
    for (Task task : captureParams.getCaptureTasks()) {
      task.beginTask();
    }
    for (Task task : captureParams.getCaptureTasks()) {
      CapturableTask capturableTask = (CapturableTask) task;
      if (capturableTask.getThread() != null) {
        capturableTask.getThread().join();
      }
    }
  }

  /**
   * Gets all the respective {@link CapturedTaskData} in a list
   * and returns it.
   *
   * @param capturableTasks List of {@link Task}
   * @return List of {@link CapturedTaskData}
   */
  private List<CapturedTaskData> consolidateTaskResult(List<CapturableTask> capturableTasks)
      throws Exception {
    if (capturableTasks == null) {
      return consolidateTaskResultExecuted();
    }

    List<CapturedTaskData> capturedTaskData = new ArrayList<>();
    for (CapturableTask capturableTask : capturableTasks) {
      capturedTaskData.add(capturableTask.getCapturedData());
    }
    if (captureParams.getSourceTasks() != null && executedTasks != null) {
      for (CapturedDataProvider capturedDataProvider : executedTasks) {
        capturedTaskData.add(capturedDataProvider.getCapturedData());
      }
    }

    return capturedTaskData;
  }

  /**
   * Gets all the respective {@link CapturedTaskData} in a list
   * for already executed tasks and returns it.
   *
   * @return List of {@link CapturedTaskData}
   * @throws Exception exception
   */
  private List<CapturedTaskData> consolidateTaskResultExecuted() throws Exception {
    List<CapturedTaskData> capturedTaskData = new ArrayList<>();
    for (String taskName : captureParams.getSourceTasks()) {
      boolean flag = false;
      for (CapturedDataProvider capturedDataProvider : executedTasks) {
        if (capturedDataProvider.getCapturedData().getTaskName()
            .equals(taskName)) {
          capturedTaskData.add(capturedDataProvider.getCapturedData());
          flag = true;
          break;
        }
      }
      if (!flag) {
        throw new Exception();
      }
    }
    return capturedTaskData;
  }

  public void setCapturedBlockData(
      List<CapturedTaskData> capturedBlockData) {
    this.capturedBlockData = capturedBlockData;
  }

  public List<CapturedDataProvider> getExecutedTasks() {
    return executedTasks;
  }

  public void setExecutedTasks(
      List<CapturedDataProvider> executedTasks) {
    this.executedTasks = executedTasks;
  }

}
