package com.example.recipeengine.instruction.adaptor;

import com.example.recipeengine.instruction.cook.BaseCook;
import com.example.recipeengine.instruction.cook.decorator.CompensationDecorator;
import com.example.recipeengine.instruction.cook.decorator.ParamsDecorator;

/**
 * This class is the implementation of {@link InstructionAdaptor},
 * which overrides {@link #applyOneTimeDecoration()} which is responsible for applying decoration
 * on Cook objects which have attemptNum as 0.
 * and {@link #applyCompensatoryDecoration()} which is responsible for applying decoration
 *  * on Cook objects which have attemptNum greater than 0.
 */
public class CookAdaptor extends InstructionAdaptor<BaseCook> {

  private BaseCook cookInstruction;

  /**
   * Constructor for {@link CookAdaptor}.
   * @param cookInstruction the current {@link BaseCook} instruction
   */
  public CookAdaptor(BaseCook cookInstruction) {
    super(cookInstruction);
    this.cookInstruction = cookInstruction;
  }

  @Override
  protected BaseCook applyOneTimeDecoration() {
    cookInstruction = new ParamsDecorator(cookInstruction, null, null);
    return cookInstruction;
  }

  @Override
  protected BaseCook applyCompensatoryDecoration() {
    cookInstruction = new CompensationDecorator(cookInstruction, null);
    return cookInstruction;
  }
}
