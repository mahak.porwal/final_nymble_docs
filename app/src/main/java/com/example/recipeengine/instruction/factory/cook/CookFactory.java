package com.example.recipeengine.instruction.factory.cook;

import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.cook.CookType;
import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.cook.params.stirrer.StirrerParams;
import com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper;
import com.example.recipeengine.instruction.info.cook.CookInfo;
import com.example.recipeengine.instruction.info.cook.CookTypeBuilder;
import java.io.IOException;
import java.util.Map;
import javax.inject.Inject;
import org.json.simple.parser.ParseException;


/**
 * This is the Factory implementation of {@link Cook} class.
 * It uses static variable to store the filepath.
 * The method {@link #createCookObject(String)} is responsible for
 * returning a newly created {@link Cook} object thus keeping the
 * object creation logic abstract from other components.
 */
public class CookFactory {
  public static String filename =
      "./src/main/java/com/example/recipeengine/resources/Cook.json";

  private CaptureParamsFactory captureParamsFactory;
  private InferParamsFactory inferParamsFactory;
  private ActionParamsFactory actionParamsFactory;
  private StirrerParamsFactory stirrerParamsFactory;
  private CookTypeBuilder cookTypeBuilder;

  @Inject
  public CookFactory(CookTypeBuilder cookTypeBuilder, StirrerParamsFactory stirrerParamsFactory,
                     CaptureParamsFactory captureParamsFactory,
                     InferParamsFactory inferParamsFactory,
                     ActionParamsFactory actionParamsFactory) {
    this.cookTypeBuilder = cookTypeBuilder;
    this.captureParamsFactory = captureParamsFactory;
    this.inferParamsFactory = inferParamsFactory;
    this.actionParamsFactory = actionParamsFactory;
    this.stirrerParamsFactory = stirrerParamsFactory;
  }

  /**
   * This method is responsible to create a Cook object using Dagger components.
   * Approach 1 : Static variable for filename
   * Approach 2 : SharedPreferences for storing filename which will
   * later be accessed by
   * {@link }
   *
   * @param filename String : path of the Saute Json file
   * @return Cook object
   */

  public Cook createCookObject(String filename) {
    this.filename = filename;
    Integer[] baseInstructionValues = new Integer[0];
    try {
      baseInstructionValues = resolveBaseInstructionValues(filename);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    StirrerParams stirrerParams = this.stirrerParamsFactory.provideStirrerProfile(filename,
        null);
    CaptureParams captureParams = this.captureParamsFactory.provideCaptureParameters(filename,
        null);
    InferParams inferParams = this.inferParamsFactory.provideInferParameters(filename,
        null);
    ActionParams actionParams = this.actionParamsFactory.provideActionParameters(filename,
        null);
    CookType cookType = this.cookTypeBuilder.provideCookType(filename);
    Cook cook =
        new Cook(baseInstructionValues[0], baseInstructionValues[1], cookType, captureParams,
            actionParams, stirrerParams,
            inferParams);
    return cook;
  }

  private Integer[] resolveBaseInstructionValues(String filename)
      throws IOException, ParseException {
    if (filename == null) {
      return new Integer[] {1, 0};
    }

    return CookParamsModuleHelper.getBaseInstructionObjects(filename);
  }

  public Cook createCookObject(CookInfo cookInfo, Map<String, String> parameters) throws Exception {
    if (null == cookInfo) {
      throw new NullPointerException();
    }
    cookInfo.resolveParametricFields(parameters);
    if (!cookInfo.isExecutable()) {
      /**
       * TODO : Incase of unresolvable parameters, use default values instead of throwing exception
       */
      throw new Exception("Unexecutable cook information");
    }
    Integer[] baseInstructionValues = resolveBaseInstructionValues(null);
    StirrerParams stirrerParams = this.stirrerParamsFactory.provideStirrerProfile(null,
        cookInfo);
    CaptureParams captureParams = this.captureParamsFactory.provideCaptureParameters(null,
        cookInfo);
    InferParams inferParams = this.inferParamsFactory.provideInferParameters(null,
        cookInfo);
    ActionParams actionParams = this.actionParamsFactory.provideActionParameters(null,
        cookInfo);
    //Add instruction id and attempt num to the constructor
    Cook cook = new Cook(baseInstructionValues[0], baseInstructionValues[1], cookInfo.getCookType(),
        captureParams, actionParams, stirrerParams,
        inferParams);
    return cook;
  }
}
