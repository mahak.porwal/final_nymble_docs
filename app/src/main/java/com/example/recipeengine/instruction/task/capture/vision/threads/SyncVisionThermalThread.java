package com.example.recipeengine.instruction.task.capture.vision.threads;

/**
 * Thread class is responsible for running both RGB and Thermal
 * camera with thread objects of {@link VisionCameraThread} and
 * {@link ThermalCameraThread}.
 */
public class SyncVisionThermalThread extends Thread {

  private VisionCameraThread visionCameraThread;
  private ThermalCameraThread thermalCameraThread;

  /**
   * Constructor for SyncVisionThermalThread class.
   *
   * @param visionCameraThread  object that runs VisionCamera request.
   * @param thermalCameraThread object that runs ThermalCamera request.
   */
  public SyncVisionThermalThread(String name,
                                 VisionCameraThread visionCameraThread,
                                 ThermalCameraThread thermalCameraThread) {
    super(name);
    this.visionCameraThread = visionCameraThread;
    this.thermalCameraThread = thermalCameraThread;
  }

  /**
   * This method starts both VisionCamera and ThermalCamera in separate threads.
   */

  @Override
  public void run() {
    visionCameraThread.start();
    thermalCameraThread.start();
    try {
      visionCameraThread.join();
      thermalCameraThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
