package com.example.recipeengine.instruction.task.hardware;

import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseHwInformation;
import com.example.recipeengine.instruction.task.capture.base.Task;
import com.example.recipeengine.instruction.task.hardware.stirrer.StirrerPositionTask;

import java.util.ArrayList;
import java.util.List;

import som.instruction.request.dispense.Dispense;
import som.instruction.request.dispense.DispenseContainer;
import som.instruction.request.stir.Stir;

/**
 * This class provides the Hardware tasks for PreDispenseBlock.
 */
public class PreDispenseTaskProvider implements HardwareTaskProvider {

  @Override
  public List<Task> hwTasksProvider() {
    return new ArrayList<>();
  }

  @Override
  public List<Task> hwTasksProvider(DispenseHwInformation[] hwInformation) {
    final List<Task> preDispenseHwList = new ArrayList<>();
    final Task stirPosTask = this.createStirPosTask(hwInformation);
    preDispenseHwList.add(stirPosTask);
    return preDispenseHwList;
  }

  private Task createStirPosTask(DispenseHwInformation[]
                                   hwInformation) {
    final DispenseContainer[] dispenseContainers = this.createContainerList(
      hwInformation);
    return new StirrerPositionTask(dispenseContainers);
  }

  private DispenseContainer[] createContainerList(
    DispenseHwInformation[] hwInformation) {
    final int numDispenseContainers = hwInformation.length;
    final DispenseContainer[] dispenseContainers =
      new DispenseContainer[numDispenseContainers];
    for (int i = 0; i < numDispenseContainers; i++) {
      final String dispenseContainer = hwInformation[i].getHardwareContainer();
      dispenseContainers[i] =
        DispenseContainer.valueOf(dispenseContainer);
    }
    return dispenseContainers;
  }
}
