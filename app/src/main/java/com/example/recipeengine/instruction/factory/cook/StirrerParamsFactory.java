package com.example.recipeengine.instruction.factory.cook;

import com.example.recipeengine.instruction.cook.params.stirrer.StirrerParams;
import com.example.recipeengine.instruction.cook.params.stirrer.StirrerProfile;
import com.example.recipeengine.instruction.cookmodules.CookInfoConverter;
import com.example.recipeengine.instruction.info.cook.CookInfo;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

public class StirrerParamsFactory {
  @Inject
  public StirrerParamsFactory() {
  }

  public StirrerParams provideStirrerProfile(String filename, CookInfo cookInfo) {
    StirrerParams stirrerParams = null;
    if (filename != null) {
      try {
        stirrerParams = createStirrerParamsObject(filename);
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ParseException e) {
        e.printStackTrace();
      }
    } else {
      stirrerParams = CookInfoConverter.provideStirrerParameters(cookInfo);
    }
    return stirrerParams;
  }

  private StirrerParams createStirrerParamsObject(String filename) throws IOException, ParseException {
    Object object = new JSONParser().parse(new FileReader(filename));
    JSONObject jsonObject = (JSONObject) object;
    Map address = ((Map) jsonObject.get("stirrerParams"));

    return new StirrerProfile(((Long) address.get("cycleDurationSeconds")).intValue(),
      ((Long) address.get("stirringPercent")).intValue(),
      ((Long) address.get("stirringSpeed")).intValue());
  }
}
