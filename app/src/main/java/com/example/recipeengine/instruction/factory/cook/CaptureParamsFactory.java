package com.example.recipeengine.instruction.factory.cook;

import com.example.recipeengine.instruction.cook.params.capture.CaptureParameters;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;

import com.example.recipeengine.instruction.cookmodules.CookInfoConverter;
import com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper;
import com.example.recipeengine.instruction.info.cook.CookInfo;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;


import static com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper.resolveVisionCaptureTask;
import static com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper.resolveWeightCaptureTask;

public class CaptureParamsFactory {
  @Inject
  public CaptureParamsFactory() {
  }


  /**
   * Creates a new {@link CaptureParameters} object and returns it to
   * {@link com.example.recipeengine.instruction.cook.CookComponent}.
   *
   * @return {@link CaptureParams}
   */
  public CaptureParams provideCaptureParameters(String filename, CookInfo cookInfo) {
    CaptureParams captureParams = null;
    if (filename != null) {
      try {
        captureParams = createCaptureParamsObject(filename);
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ParseException e) {
        e.printStackTrace();
      }
    } else {
      captureParams = CookInfoConverter.provideCaptureParameters(cookInfo);
    }
    return captureParams;
  }

  /**
   * This method uses two static methods from class {@link CookParamsModuleHelper}
   * to get the objects.
   * It checks the CaptureSensor value and then calls respective methods.
   *
   * @return CaptureParams object
   * @throws IOException    Exception
   * @throws ParseException Exception
   */
  private CaptureParams createCaptureParamsObject(String filename) throws IOException, ParseException {
    Object object = new JSONParser().parse(new FileReader(filename));
    JSONObject jsonObject = (JSONObject) object;
    Map addressCapture = ((Map) jsonObject.get("captureParams"));
    List address = (List) addressCapture.get("capturableTasks");
    List source = (List) addressCapture.get("sourceTasks");
    List<CapturableTask> captureTasks = new ArrayList<>();
    for (int i = 0; i < address.size(); i++) {
      Map captureTaskMap = (Map) address.get(i);
      if (captureTaskMap.get("captureSensor").equals("WeightSensor")) {
        captureTasks.add(resolveWeightCaptureTask(captureTaskMap));
      } else {
        captureTasks.add(resolveVisionCaptureTask(captureTaskMap));
      }
    }
    List<String> sourceTasks = new ArrayList<>();
    for(int i = 0; i < source.size(); i++) {
      sourceTasks.add((String) source.get(i));
    }

    return new CaptureParameters(captureTasks, sourceTasks);
  }
}
