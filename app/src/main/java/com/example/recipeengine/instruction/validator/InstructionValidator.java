package com.example.recipeengine.instruction.validator;

import com.example.recipeengine.instruction.blocks.ExecutionBlock;

public interface InstructionValidator extends ExecutionBlock {
  boolean isValid();
}
