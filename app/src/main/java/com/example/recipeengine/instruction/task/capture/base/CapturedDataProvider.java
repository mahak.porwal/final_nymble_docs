package com.example.recipeengine.instruction.task.capture.base;

import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;

public interface CapturedDataProvider {

  CapturedTaskData getCapturedData();

}
