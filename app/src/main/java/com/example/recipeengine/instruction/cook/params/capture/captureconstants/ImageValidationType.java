package com.example.recipeengine.instruction.cook.params.capture.captureconstants;

import androidx.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ImageValidationType {
  public static final String EXPOSURE_VALIDATION = "EXPOSURE_VALIDATION";
  private String imageValidationType;

  public ImageValidationType(@AnnotationImageValidationType String imageValidationType) {
    this.imageValidationType = imageValidationType;
  }

  public String getImageValidationType() {
    return imageValidationType;
  }

  @StringDef({EXPOSURE_VALIDATION})
  @Retention(RetentionPolicy.SOURCE)
  public @interface AnnotationImageValidationType {

  }
}
