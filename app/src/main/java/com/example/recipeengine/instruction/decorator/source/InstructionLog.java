package com.example.recipeengine.instruction.decorator.source;

import com.example.recipeengine.instruction.decorator.exchanges.ExecutionParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import java.util.Map;
import lombok.NoArgsConstructor;

/**
 * This class contains log of instruction that want to decorate the incoming dispense instruction.
 */
@NoArgsConstructor
public class InstructionLog {
  private Integer instructionId;
  private Integer attemptNum;
  private Map<ExecutionParams, String> executionParams;
  private Map<OutputParams, String> outputParams;

  /**
   * Constructor for InstructionLog class.
   *
   * @param instructionId current Instruction Id
   * @param attemptNum current Instruction attempt number
   * @param executionParams Map of ExecutionParams and its values
   * @param outputParams Map of OutputParams and its values
   */
  public InstructionLog(Integer instructionId, Integer attemptNum,
                        Map<ExecutionParams, String> executionParams,
                        Map<OutputParams, String> outputParams) {
    this.instructionId = instructionId;
    this.attemptNum = attemptNum;
    this.executionParams = executionParams;
    this.outputParams = outputParams;
  }

  public Integer getInstructionId() {
    return instructionId;
  }

  public void setInstructionId(Integer instructionId) {
    this.instructionId = instructionId;
  }

  public Integer getAttemptNum() {
    return attemptNum;
  }

  public void setAttemptNum(Integer attemptNum) {
    this.attemptNum = attemptNum;
  }

  public Map<ExecutionParams, String> getExecutionParams() {
    return executionParams;
  }

  public void setExecutionParams(
      Map<ExecutionParams, String> executionParams) {
    this.executionParams = executionParams;
  }

  public Map<OutputParams, String> getOutputParams() {
    return outputParams;
  }

  public void setOutputParams(
      Map<OutputParams, String> outputParams) {
    this.outputParams = outputParams;
  }
}
