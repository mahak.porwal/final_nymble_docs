package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.instruction.cook.params.action.ActionParams;

public interface ActionInfo extends ActionParams {
  String getDefaultWattageString();

  String getTimeToCookString();

  String getVisualScoreString();

  String getTemperatureString();

  String getThermalScoreString();

  void setDefaultWattage(Integer defaultWattage);

  void setTimeToCook(Integer timeToCook);

  void setVisualScore(Double visualScore);

  void setTemperature(Integer temperature);

  void setThermalScore(Double thermalScore);
}
