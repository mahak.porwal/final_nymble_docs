package com.example.recipeengine.instruction.handler.cook.consistency.decorator;

import com.example.recipeengine.instruction.cook.BaseCook;
import com.example.recipeengine.instruction.cook.decorator.BaseCookDecorator;

/**
 * Consistency decorator class, responsible for providing updated instruction after resuming.
 *
 * @author Pranshu Gupta
 */
public class ConsistencyCompensationDecorator extends BaseCookDecorator {

  BaseCook wrapperObject;
  //    private InstructionLog instructionLog;  //for future use
  //    private Double currentTimeToCook;

  /**
   * Constructor of {@link BaseCookDecorator} class.
   *
   * @param wrappedObject contains {@link BaseCook} object to be decorated
   */
  public ConsistencyCompensationDecorator(BaseCook wrappedObject
                                            /*InstructionLog instructionLog,
                                            Double currentTimeToCook*/) {
    super(wrappedObject);
    //        this.instructionLog = instructionLog;
    //        this.currentTimeToCook = currentTimeToCook;
    this.wrapperObject = wrappedObject;
  }


  /**
   * Method to get the adapted time to cook value while resuming consistency instruction.
   *
   * @return It returns no change in time to cook value. Not required as of now.
   */
  @Override
  public Integer getTimeToCook() {
    final Integer target = this.wrapperObject.getTimeToCook() - deltaTime();

    if (target > this.wrapperObject.getTimeToCook()) {
      return this.wrapperObject.getTimeToCook();
    }

    return target <= Integer.valueOf(0) ? Integer.valueOf(0) : target;
  }

  /**
   * This method is responsible for calculating the difference in target value.
   * that must be added to original target
   *
   * @return Integer This returns change in value of target.
   */
  private Integer deltaTime() {
    return 0;
  }

}
