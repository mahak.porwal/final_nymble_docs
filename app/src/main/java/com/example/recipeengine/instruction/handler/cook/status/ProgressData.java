package com.example.recipeengine.instruction.handler.cook.status;

import android.graphics.Bitmap;
import android.util.Pair;
import com.example.recipeengine.instruction.decorator.exchanges.ExecutionParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;

/**
 * This class is responsible for storing pairs properties
 * and results.
 */
public class ProgressData {

  private Pair<String, Bitmap> imageData;
  private Pair<ExecutionParams, String> executionData;
  private Pair<OutputParams, String> outputData;

  /**
   * Constructor of ProgressData class.
   *
   * @param imageData     a pair of String and Bitmap
   * @param executionData a pair of {@link ExecutionParams} and String
   * @param outputData    a pair of {@link OutputParams} and String
   */
  public ProgressData(Pair<String, Bitmap> imageData,
                      Pair<ExecutionParams, String> executionData,
                      Pair<OutputParams, String> outputData) {
    this.imageData = imageData;
    this.executionData = executionData;
    this.outputData = outputData;
  }

  public Pair<String, Bitmap> getImageData() {
    return imageData;
  }

  public void setImageData(Pair<String, Bitmap> imageData) {
    this.imageData = imageData;
  }

  public Pair<ExecutionParams, String> getExecutionData() {
    return executionData;
  }

  public void setExecutionData(
      Pair<ExecutionParams, String> executionData) {
    this.executionData = executionData;
  }

  public Pair<OutputParams, String> getOutputData() {
    return outputData;
  }

  public void setOutputData(
      Pair<OutputParams, String> outputData) {
    this.outputData = outputData;
  }
}
