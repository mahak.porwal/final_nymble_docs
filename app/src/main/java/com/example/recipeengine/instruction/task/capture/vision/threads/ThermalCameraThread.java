package com.example.recipeengine.instruction.task.capture.vision.threads;

import android.graphics.Bitmap;
import android.util.Log;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.vision.BitmapUtility;
import java.util.List;
import java.util.Map;
import som.hardware.request.handler.RequestHandler;
import som.hardware.response.SensorResponse;
import som.instruction.request.Request;
import som.instruction.request.sensor.Sensor;
import som.instruction.request.sensor.Sensors;

/**
 * This class is responsible for making request to Thermal Camera to
 * collect thermal images.
 * {@link #run()} contains the logic to capture thermal images.
 */
public class ThermalCameraThread extends Thread {

  private final SensorParams sensorParams;
  private final List<Bitmap> thermalImages;
  private final List<List<Double>> thermalMatrices;
  private RequestHandler requestHandler;
  private BitmapUtility bitmapUtility;

  /**
   * Constructor for ThermalCameraThread class.
   *
   * @param sensorParams    Contains all the required params related to sensor
   * @param thermalImages   A list to store images in the form of Bitmap
   * @param thermalMatrices A list of list to store images as matrices.
   * @param requestHandler  {@link RequestHandler} object to make a request.
   */
  public ThermalCameraThread(String name,
                             SensorParams sensorParams, List<Bitmap> thermalImages,
                             List<List<Double>> thermalMatrices, RequestHandler requestHandler) {
    super(name);
    this.sensorParams = sensorParams;
    this.thermalImages = thermalImages;
    this.thermalMatrices = thermalMatrices;
    this.requestHandler = requestHandler;
  }

  /**
   * This function contains the logic of capturing Thermal Images.
   * 1. First we run a for loop for how many samples we are trying to
   * collect, it is present in {@link SensorParams} object.
   * 2. Then we create an object {@link Sensor} and set the sensor to
   * Thermal Camera.
   * 3. Then we call the {@link RequestHandler#handleRequest(Request)} method
   * to execute the given request and return the {@link SensorResponse}.
   * 4. The sensor response returns a Map (String,List Double  object and we store
   * it by converting it into a Bitmap image using {@link BitmapUtility#convertToBitmap(List)}
   * method.
   * 5. The current thread is put to sleep for a specific amount
   * of time mentioned in the sensorParams.
   */
  @Override
  public void run() {
    this.requestHandler = getRequestHandlerInstance();
    this.bitmapUtility = getBitmapUtilityInstance();
    for (int i = 0; i < this.sensorParams.getNumOfSamples(); i++) {
      final Sensor sensor = new Sensor(null);
      sensor.setSensor(Sensors.THERMAL_CAMERA);
      final SensorResponse response;
      try {
        long startTime = System.currentTimeMillis();
        Log.d("TESTING_IMAGE", "Start time of Thermal Camera " + String.valueOf(startTime));
        response = (SensorResponse) this.requestHandler.handleRequest(sensor);
        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        Log.d("TESTING_IMAGE", "The total time for Thermal Camera Thread : " + totalTime);
        final Map<String, ? extends List<Double>> sensorData = response.getSensorData();
        if (null != sensorData) {
          final List<Double> thermalMatrix = sensorData.get("SENSOR_VALUE");
          this.thermalMatrices.add(thermalMatrix);
          thermalImages
              .add(bitmapUtility
                  .convertToBitmap(
                      thermalMatrix));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
      if (i != sensorParams.getNumOfSamples() - 1) {
        try {
          Thread.sleep(sensorParams.getTimeBetweenCaptureMS());
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    //System.out.println("The total time for Vision Camera Thread : " + totalTime);
  }

  public BitmapUtility getBitmapUtilityInstance() {
    return new BitmapUtility();
  }

  public RequestHandler getRequestHandlerInstance() {
    return new RequestHandler();
  }
}

