package com.example.recipeengine.instruction.cook.params.capture.captureconstants;

import androidx.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class VisualCaptureType {
  public static final String VISION_CAMERA = "VISION_CAMERA";
  public static final String THERMAL_CAMERA = "THERMAL_CAMERA";
  public static final String SYNC_THERMAL_VISUAL = "SYNC_THERMAL_VISUAL";
  private String visualCaptureType;

  public VisualCaptureType(@AnnotationVisualCaptureType String visualCaptureType) {
    this.visualCaptureType = visualCaptureType;
  }

  public String getVisualCaptureType() {
    return visualCaptureType;
  }

  @StringDef({VISION_CAMERA,THERMAL_CAMERA,SYNC_THERMAL_VISUAL})
  @Retention(RetentionPolicy.SOURCE)
  public @interface AnnotationVisualCaptureType {}
}
