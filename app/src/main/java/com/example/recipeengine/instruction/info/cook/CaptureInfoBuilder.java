package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.instruction.cook.params.capture.CaptureParameters;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;

import java.io.FileReader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import static com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper.resolveVisionCaptureTask;
import static com.example.recipeengine.instruction.cookmodules.CookParamsModuleHelper.resolveWeightCaptureTask;

public class CaptureInfoBuilder {

  @Inject
  public CaptureInfoBuilder() {
  }

  public CaptureParams provideCaptureParameters(JSONObject jsonObject) {
    CaptureParams captureParams = null;
    try {
      captureParams = createCaptureParamsObject(jsonObject);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return captureParams;
  }

  private CaptureParams createCaptureParamsObject(JSONObject jsonObject) throws IOException, ParseException {
    Map addressCapture = ((Map) jsonObject.get("captureParams"));
    List address = (List) addressCapture.get("capturableTasks");
    List source = (List) addressCapture.get("sourceTasks");
    List<CapturableTask> captureTasks = new ArrayList<>();
    for (int i = 0; i < address.size(); i++) {
      Map captureTaskMap = (Map) address.get(i);
      if (captureTaskMap.get("captureSensor").equals("WeightSensor")) {
        captureTasks.add(resolveWeightCaptureTask(captureTaskMap));
      } else {
        captureTasks.add(resolveVisionCaptureTask(captureTaskMap));
      }
    }
    List<String> sourceTasks = new ArrayList<>();
    for(int i = 0; i < source.size(); i++) {
      sourceTasks.add((String) source.get(i));
    }

    return new CaptureParameters(captureTasks, sourceTasks);
  }

}
