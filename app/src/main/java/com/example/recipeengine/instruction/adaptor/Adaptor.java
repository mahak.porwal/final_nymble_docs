package com.example.recipeengine.instruction.adaptor;

import com.example.recipeengine.instruction.Instruction;

/**
 * Here is the HLD:
 * \image html Adaptor_HLD.jpg
*/

public interface Adaptor<T extends Instruction> {
  /**
   * This method calls the required decoration method based on
   * attempt number and returns an {@link Instruction} object.
   * @return {@link Instruction}
   */
  T adapt();
}
