package com.example.recipeengine.instruction.handler.cook.wattage;

import com.example.recipeengine.instruction.handler.cook.handlermodules.WattageHandlerModule;

import dagger.Component;

@Component(modules = WattageHandlerModule.class)
public interface WattageHandlerComponent {
  WattageHandler buildWattageHandlerObject();
}
