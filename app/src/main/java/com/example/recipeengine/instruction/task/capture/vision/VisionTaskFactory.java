package com.example.recipeengine.instruction.task.capture.vision;

import android.graphics.Bitmap;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.vision.threads.SyncVisionThermalThread;
import com.example.recipeengine.instruction.task.capture.vision.threads.ThermalCameraThread;
import com.example.recipeengine.instruction.task.capture.vision.threads.VisionCameraThread;
import java.util.List;
import som.hardware.request.handler.RequestHandler;

/**
 * This is the factory class for {@link VisionCapture} to provide the correct thread
 * implementation to execute.
 */
public class VisionTaskFactory {

  private SensorParams sensorParams;
  private List<Bitmap> thermalImages;
  private List<Bitmap> visionImages;
  private List<List<Double>> thermalMatrices;
  private RequestHandler hwRequestHandler;

  /**
   * Constructor for VisionCapture class.
   *
   * @param sensorParams     Contains all the required params related to sensor
   * @param thermalImages    A list to store thermal images in the form of Bitmap
   * @param visionImages     A list to store RGB images in the form of Bitmap
   * @param thermalMatrices  A list of list to store images as matrices.
   * @param hwRequestHandler {@link RequestHandler} object to make a request.
   */
  public VisionTaskFactory(
      SensorParams sensorParams, List<Bitmap> thermalImages,
      List<Bitmap> visionImages, List<List<Double>> thermalMatrices,
      RequestHandler hwRequestHandler) {
    this.sensorParams = sensorParams;
    this.thermalImages = thermalImages;
    this.visionImages = visionImages;
    this.thermalMatrices = thermalMatrices;
    this.hwRequestHandler = hwRequestHandler;
  }

  /**
   * This method takes in a String of task type that needs to be run.
   * It then returns a new object based on what implementation is asked for.
   *
   * @param taskType The task that is to be executed.
   * @return an object of Thread type
   */
  public Thread getVisionTaskInstance(String taskType) {
    if (taskType.equals("VISION_CAMERA")) {
      return new VisionCameraThread(taskType, sensorParams, visionImages, hwRequestHandler);
    } else if (taskType.equals("THERMAL_CAMERA")) {
      return new ThermalCameraThread(taskType, sensorParams, thermalImages, thermalMatrices,
          hwRequestHandler);
    }
    return new SyncVisionThermalThread(taskType,
        new VisionCameraThread("VISION_CAMERA", sensorParams, visionImages, hwRequestHandler),
        new ThermalCameraThread("THERMAL_CAMERA", sensorParams, thermalImages, thermalMatrices,
            hwRequestHandler));
  }

}
