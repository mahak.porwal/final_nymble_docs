package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.util.MxEquationResolver;

import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class ParametricFieldsBuilder {
  @Inject
  public ParametricFieldsBuilder() {
  }

  public Map<String, String> provideParametricFields(JSONObject jsonObject) {
    Map<String, String> parametricFields = new HashMap<>();
    Map address = ((Map) jsonObject.get("actionParams"));

    for (Object parameter : address.keySet()) {
      if (!MxEquationResolver.isResolvedExpression(address.get(parameter.toString()).toString())) {
        parametricFields.put(parameter.toString(), address.get(parameter.toString()).toString());
      }
    }

    address = ((Map) jsonObject.get("stirrerParams"));

    for (Object parameter : address.keySet()) {
      if (!MxEquationResolver.isResolvedExpression(address.get(parameter.toString()).toString())) {
        parametricFields.put(parameter.toString(), address.get(parameter.toString()).toString());
      }
    }
    return parametricFields;
  }
}
