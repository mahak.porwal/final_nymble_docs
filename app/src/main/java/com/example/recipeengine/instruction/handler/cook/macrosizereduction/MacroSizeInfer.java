package com.example.recipeengine.instruction.handler.cook.macrosizereduction;

import static com.example.recipeengine.util.ImageUtil.centerCrop;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.NonNull;
import com.example.recipeengine.BuildConfig;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.handler.cook.infer.BaseInferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.segmentation.FoodSegmentation;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.util.FileUtil;
import io.reactivex.subjects.PublishSubject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import timber.log.Timber;

/**
 * Saute class to find food size reduction during cooking.
 */
public class MacroSizeInfer extends BaseInferBlock {

  private static final Point INPUT_IMAGE_CROP_ORIGIN = new Point(420, 0);
  private static final int INPUT_IMAGE_CROP_SIZE = 1080;
  private final InferParams inferParameters;
  private final FoodSegmentation foodSegmentation;
  List<Double> result = new ArrayList<>();
  private boolean isFirstBatch;
  private double firstFoodPercentage;
  private int iterationNumber;

  /**
   * BaseInfer class constructor. Parent class for all vision based sautes.
   *
   * @param recipeContext Contains recipe specific information.
   * @param uiEmitter     Publisher for UI
   * @param dataLogger    Publisher for data logging
   */
  public MacroSizeInfer(InferParams inferParameters,
                        RecipeContext recipeContext,
                        PublishSubject<UIData> uiEmitter,
                        PublishSubject<ProgressData> dataLogger) {
    super(recipeContext, inferParameters, uiEmitter, dataLogger);
    this.inferParameters = inferParameters;
    this.foodSegmentation = new FoodSegmentation(this.inferParameters.getModelFileName());
    this.isFirstBatch = true;
    this.firstFoodPercentage = 0.0;
    iterationNumber = 1;
    try {
      loadModel();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Load food segmentation model.
   *
   * @throws IOException exception
   */
  public void loadModel() throws IOException {
    this.foodSegmentation.loadModel();
  }

  /**
   * Core method for macro size reduction ability.
   *
   * @param captureData Contains all the captured data
   * @return InferredResult
   */
  @Override
  public InferredResult infer(List<CapturedTaskData> captureData) {
    if (null != captureData) {
      CapturedTaskData capturedTaskData = captureData.get(0);
      final List<Bitmap> bmps = capturedTaskData.getVisionCamera();

      final UIData uiData = new UIData(null, null);
      final ProgressData progressData = new ProgressData(null, null, null);
      this.result.clear();
      if (BuildConfig.DEBUG && 2 > bmps.size()) {
        throw new AssertionError("Assertion failed");
      }
      List<Double> foodPixelPercentage = new ArrayList<>();
      for (int i = 0; i < bmps.size(); i++) {
        final Bitmap bmp = centerCrop(bmps.get(i), INPUT_IMAGE_CROP_ORIGIN, INPUT_IMAGE_CROP_SIZE);
        final Mat foodMask = this.foodSegmentation.runSegmentation(bmp);
        uiData.setImage(_parseBitmap(foodMask));
        getUiEmitter().onNext(uiData);
        foodPixelPercentage.add(postProcessModelOutput(foodMask));
      }
      double mean = 0;
      for (Double val : foodPixelPercentage) {
        mean += val;
      }
      mean /= foodPixelPercentage.size();

      final Pair<OutputParams, String> outData =
          new Pair<>(OutputParams.FOOD_PERCENTAGE, String.valueOf(mean));
      progressData.setOutputData(outData);
      getDataLogger().onNext(progressData);
      updateInferStatus(mean);

      if (this.isFirstBatch) {
        if (iterationNumber <= 5) {
          this.firstFoodPercentage = (this.firstFoodPercentage + mean);
          iterationNumber += 1;
          this.result.add(0.0);
          return new InferredResult(this.result);
        } else {
          this.isFirstBatch = false;
          this.firstFoodPercentage /= 5;
          //Log.d("MACRO", "The first food percentage is : " + this.firstFoodPercentage);
        }
      }
      final double diff = (this.firstFoodPercentage - mean) / this.firstFoodPercentage;
      this.result.add(diff);

      for (int i = 0; i < bmps.size(); i++) {
        saveImage(bmps.get(i), i + 1, mean);
      }

      //Log.d("MACRO", "The difference from first batch is " + diff);
      return new InferredResult(this.result);
    } else {
      throw new IllegalArgumentException("Capture data cannot be null");
    }

  }


  /**
   * Getting the food pixel percentage from food mask.
   *
   * @param mask output of model
   * @return double
   */
  private double postProcessModelOutput(Mat mask) {
    int sum = 0;
    for (int x = 0; FoodSegmentation.OUTPUT_SIZE > x; x++) {
      for (int y = 0; FoodSegmentation.OUTPUT_SIZE > y; y++) {
        byte[] data = new byte[1];
        mask.get(x, y, data);
        if (127 == data[0]) {
          sum++;
        }
      }
    }
    return (double) sum / (FoodSegmentation.OUTPUT_SIZE * FoodSegmentation.OUTPUT_SIZE);
  }

  private void saveImage(@NonNull Bitmap image1, int number, double mean) {
    final String filename1 =
        "Image_" + number + "_" + (mean * 100) + ".jpg";
    final File imageFile = FileUtil.getMacroSizeImageFile(filename1);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image1.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename1);
    }
  }

  /**
   * Convert Opencv.Mat to Bitmap
   *
   * @param mat opencv mat object
   * @return Bitmap
   */
  private Bitmap _parseBitmap(Mat mat) {
    Bitmap bmp = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
    Utils.matToBitmap(mat, bmp);
    return bmp;
  }

  /**
   * Method to update the infer status string for debugging.
   *
   * @param mean score from infer results
   */
  private void updateInferStatus(double mean) {
    this.inferStatus = "Reaching target food percentage, current infer result is = " + mean * 100
        + " percent food";
    //Log.d("MACRO_INFER", this.inferStatus);
  }

  /**
   * Method to halt the current operations of size reduction saute.
   */
  @Override
  public void pause() {
    this.foodSegmentation.close();
  }

  @Override
  public void close() throws IOException {
    this.foodSegmentation.close();
  }
}
