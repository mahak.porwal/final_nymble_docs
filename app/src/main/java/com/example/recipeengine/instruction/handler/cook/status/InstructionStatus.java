package com.example.recipeengine.instruction.handler.cook.status;

/**
 * This class contains the current instruction status.
 * It implements {@link BaseInstStatus}.
 */
public class InstructionStatus implements BaseInstStatus {

  private String status;
  private String verboseStatus;
  private Integer remainingTimeSec;

  /**
   * Constructor for InstructionStatus.
   *
   * @param status           current status in a concise form.
   * @param verboseStatus    current status in a verbose form.
   * @param remainingTimeSec time left for current instruction.
   */
  public InstructionStatus(String status, String verboseStatus, Integer remainingTimeSec) {
    this.status = status;
    this.verboseStatus = verboseStatus;
    this.remainingTimeSec = remainingTimeSec;
  }

  @Override
  public String getStatus() {
    return status;
  }

  @Override
  public String getVerboseStatus() {
    return verboseStatus;
  }

  @Override
  public Integer remainingTimeSec() {
    return remainingTimeSec;
  }
}
