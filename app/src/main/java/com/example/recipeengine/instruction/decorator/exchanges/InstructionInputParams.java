package com.example.recipeengine.instruction.decorator.exchanges;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum InstructionInputParams {
  @JsonProperty("TARGET_TEMPERATURE")
  TARGET_TEMPERATURE,
  @JsonProperty("TIME_TO_COOK")
  TIME_TO_COOK,
  @JsonProperty("DEFAULT_WATTAGE")
  DEFAULT_WATTAGE,
  @JsonProperty("INGREDIENT_QUANTITY")
  INGREDIENT_QUANTITY
}
