package com.example.recipeengine.instruction.factory.cook;

import com.example.recipeengine.instruction.cook.params.infer.InferParameters;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.cookmodules.CookInfoConverter;
import com.example.recipeengine.instruction.info.cook.CookInfo;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

import dagger.Provides;

public class InferParamsFactory {
  @Inject
  public InferParamsFactory() {
  }

  /**
   * Creates a new {@link InferParams} object and returns it to
   * {@link com.example.recipeengine.instruction.cook.CookComponent}.
   *
   * @return {@link InferParams}
   */
  public InferParams provideInferParameters(String filename, CookInfo cookInfo) {
    InferParams inferParams = null;
    if (filename != null) {
      try {
        inferParams = createInferParamsObject(filename);
      } catch (IOException e) {
        e.printStackTrace();
      } catch (ParseException e) {
        e.printStackTrace();
      }
    } else {
      inferParams = CookInfoConverter.provideInferParameters(cookInfo);
    }
    return inferParams;
  }

  private InferParams createInferParamsObject(String filename) throws IOException, ParseException {
    Object object = new JSONParser().parse(new FileReader(filename));
    JSONObject jsonObject = (JSONObject) object;
    Map address = ((Map) jsonObject.get("inferParams"));

    return new InferParameters((String) address.get("modelId"),
      (String) address.get("modelFileName"));
  }
}
