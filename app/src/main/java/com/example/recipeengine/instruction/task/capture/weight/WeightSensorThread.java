package com.example.recipeengine.instruction.task.capture.weight;

import com.example.recipeengine.instruction.task.capture.SensorParams;
import java.util.List;
import java.util.Map;
import som.hardware.request.handler.RequestHandler;
import som.hardware.response.SensorResponse;
import som.instruction.request.sensor.Sensor;
import som.instruction.request.sensor.Sensors;

/**
 * This is the thread class for {@link WeightCapture}.
 */
public class WeightSensorThread extends Thread {

  private SensorParams sensorParams;
  private List<Double> capturedWeight;
  private RequestHandler hwRequestHandler;

  /**
   * Constructor for WeightSensorThread class.
   *
   * @param name             Thread name
   * @param sensorParams     Contains all the required params related to sensor
   * @param capturedWeight   Result of the captured object.
   * @param hwRequestHandler Object responsible for making requests to hardware
   */
  WeightSensorThread(String name, SensorParams sensorParams, List<Double> capturedWeight,
                     RequestHandler hwRequestHandler) {
    super(name);
    this.sensorParams = sensorParams;
    this.capturedWeight = capturedWeight;
    this.hwRequestHandler = hwRequestHandler;
  }

  @Override
  public void run() {
    hwRequestHandler = getRequestHandlerInstance();

    //TODO: WeightSensor response issue.
    while (true) {
      Sensor sensor = new Sensor(null);
      sensor.setSensor(Sensors.WEIGHT_SENSOR);
      SensorResponse sensorResponse;
      try {
        sensorResponse = (SensorResponse) hwRequestHandler.handleRequest(sensor);
        Map<String, ? extends List<Double>> sensorData = sensorResponse.getSensorData();
        if (sensorData != null) {
          capturedWeight.addAll(sensorData.get("SENSOR_VALUE"));
        }
        if (capturedWeight.get(capturedWeight.size() - 1) != 0.0) {
          break;
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    for (int i = 0; i < sensorParams.getNumOfSamples(); i++) {
      Sensor sensor = new Sensor(null);
      sensor.setSensor(Sensors.WEIGHT_SENSOR);
      SensorResponse sensorResponse;
      try {
        sensorResponse = (SensorResponse) hwRequestHandler.handleRequest(sensor);
        Map<String, ? extends List<Double>> sensorData = sensorResponse.getSensorData();
        if (sensorData != null) {
          capturedWeight.addAll(sensorData.get("SENSOR_VALUE"));
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
      if(i != sensorParams.getNumOfSamples() -1 ) {
        try {
          Thread.sleep(sensorParams.getTimeBetweenCaptureMS());
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public RequestHandler getRequestHandlerInstance() {
    return new RequestHandler();
  }
}
