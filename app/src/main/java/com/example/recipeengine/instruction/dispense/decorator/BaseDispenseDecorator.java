package com.example.recipeengine.instruction.dispense.decorator;

import com.example.recipeengine.instruction.dispense.BaseDispense;
import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;

import java.util.List;

/**
 * It is the decorator implementation of {@link BaseDispense} which contains,
 * a {@link BaseDispense} object and overrides its methods.
 */
public class BaseDispenseDecorator implements BaseDispense {

  protected BaseDispense wrappedObject;

  /**
   * Constructor of {@link BaseDispenseDecorator} class.
   *
   * @param wrappedObject contains the {@link BaseDispense} object to be decorated.
   */
  public BaseDispenseDecorator(
    BaseDispense wrappedObject) {
    this.wrappedObject = wrappedObject;
  }


  @Override
  public Integer getQuantityInGrams() {
    return wrappedObject.getQuantityInGrams();
  }

  @Override
  public List<String> getIngredients() {
    return wrappedObject.getIngredients();
  }

  @Override
  public PreDispenseBlock getPreDispenseBlock() {
    return this.wrappedObject.getPreDispenseBlock();
  }

  @Override
  public DispenseBlock getDispenseBlock() {
    return this.wrappedObject.getDispenseBlock();
  }

  @Override
  public PostDispenseBlock getPostDispenseBlock() {
    return this.wrappedObject.getPostDispenseBlock();
  }

  @Override
  public List<InferInstruction> getInferInstructions() {
    return this.wrappedObject.getInferInstructions();
  }

  @Override
  public Integer getInstructionId() {
    return wrappedObject.getInstructionId();
  }

  @Override
  public Integer getAttemptNum() {
    return wrappedObject.getAttemptNum();
  }

  @Override
  public void incrementAttemptNum() {
    wrappedObject.incrementAttemptNum();
  }

  @Override
  public BaseDispense adapt() {
    return wrappedObject.adapt();
  }
}
