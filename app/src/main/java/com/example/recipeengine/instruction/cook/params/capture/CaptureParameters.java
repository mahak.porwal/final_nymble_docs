package com.example.recipeengine.instruction.cook.params.capture;

import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import java.util.List;


public class CaptureParameters implements CaptureParams {

  private List<CapturableTask> capturableTasks;
  private List<String> sourceTasks;

  public CaptureParameters(
      List<CapturableTask> capturableTasks,List<String> sourceTasks) {
    this.capturableTasks = capturableTasks;
    this.sourceTasks = sourceTasks;
  }

  @Override
  public List<CapturableTask> getCaptureTasks() {
    return capturableTasks;
  }

  @Override
  public List<String> getSourceTasks() {
    return sourceTasks;
  }
}
