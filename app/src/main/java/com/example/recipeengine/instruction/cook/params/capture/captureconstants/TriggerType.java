package com.example.recipeengine.instruction.cook.params.capture.captureconstants;

import androidx.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class TriggerType {
  public static final String STIRRER_STARTS = "STIRRER_STARTS";
  public static final String STIRRER_STOPS = "STIRRER_STOPS";
  private String triggerType;


  public TriggerType(@AnnotationTriggerType String triggerType) {
    this.triggerType = triggerType;
  }

  public String getTriggerType() {
    return triggerType;
  }

  @StringDef({STIRRER_STARTS, STIRRER_STOPS})
  @Retention(RetentionPolicy.SOURCE)
  public @interface AnnotationTriggerType {
  }
}
