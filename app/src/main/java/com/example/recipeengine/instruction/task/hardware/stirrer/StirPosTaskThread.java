package com.example.recipeengine.instruction.task.hardware.stirrer;

import som.instruction.request.dispense.DispenseContainer;
import som.instruction.request.stir.position.manager.DaggerStirrerPositionManagerComponent;
import som.instruction.request.stir.position.manager.StirrerPositionManager;
import timber.log.Timber;

public class StirPosTaskThread extends Thread {
  private final DispenseContainer[] dispenseContainers;

  public StirPosTaskThread(String name,
                           DispenseContainer[] dispenseContainers) {
    super(name);
    this.dispenseContainers = dispenseContainers;
  }

  @Override
  public void run() {
    final StirrerPositionManager stirrerPositionManager =
      DaggerStirrerPositionManagerComponent.create().buildsStirrerPositionManager();
    try {
      stirrerPositionManager.positionStirrer(this.dispenseContainers);
    } catch (Exception exception) {
      Timber.e(exception);
    }
  }
}
