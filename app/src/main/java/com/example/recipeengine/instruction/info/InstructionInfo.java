package com.example.recipeengine.instruction.info;

import java.util.Map;


public interface InstructionInfo {

  Integer getInstructionId();

  Integer getAttemptNum();

  String getInstructionType();

  Map<String, String> getParametricFields();

  void resolveParametricFields(Map<String, String> parameters);

  boolean isExecutable();
}
