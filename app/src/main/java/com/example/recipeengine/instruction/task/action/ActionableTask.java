package com.example.recipeengine.instruction.task.action;

import com.example.recipeengine.instruction.task.capture.base.Task;

import som.instruction.request.actuator.Actuators;

public interface ActionableTask extends Task {

  String getActuator();

  ActionTaskParams getActionTaskParams();

}
