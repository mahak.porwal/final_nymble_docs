package com.example.recipeengine.instruction.cook.params.stirrer;

public interface StirrerParams {
  Integer getCycleDurationSeconds();

  Integer getStirringPercent();

  Integer getStirringSpeed();
}
