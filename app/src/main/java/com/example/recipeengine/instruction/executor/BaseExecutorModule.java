package com.example.recipeengine.instruction.executor;

import com.example.recipeengine.instruction.executor.factory.BaseExecutorFactory;
import com.example.recipeengine.instruction.executor.factory.ExecutorFactory;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class BaseExecutorModule {
  @Binds
  abstract ExecutorFactory bindExecutorFactory(BaseExecutorFactory baseExecutorFactory);
}
