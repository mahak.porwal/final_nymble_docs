package com.example.recipeengine.instruction.blocks.capture;

import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import som.instruction.request.sensor.Sensors;

/**
 * This class is responsible for storing the results of
 * {@link CaptureBlock}.
 */
public class CapturedTaskData {

  private Map<Sensors, List<List<Double>>> sensorData;
  private List<Bitmap> visionCamera;
  private List<Bitmap> thermalCamera;
  private String taskName;

  /**
   * Constructor for CapturedData class.
   */
  public CapturedTaskData() {
    this.sensorData = new HashMap<Sensors, List<List<Double>>>();
    this.visionCamera = new ArrayList<>();
    this.thermalCamera = new ArrayList<>();
  }

  /**
   * Constructor for CapturedData class.
   */
  public CapturedTaskData(
      Map<Sensors, List<List<Double>>> sensorData,
      List<Bitmap> visionCamera, List<Bitmap> thermalCamera,
      String taskName) {
    this.sensorData = sensorData;
    this.visionCamera = visionCamera;
    this.thermalCamera = thermalCamera;
    this.taskName = taskName;
  }

  public String getTaskName() {
    return taskName;
  }

  public Map<Sensors, List<List<Double>>> getSensorData() {
    return sensorData;
  }

  public List<Bitmap> getVisionCamera() {
    return visionCamera;
  }

  public List<Bitmap> getThermalCamera() {
    return thermalCamera;
  }

}
