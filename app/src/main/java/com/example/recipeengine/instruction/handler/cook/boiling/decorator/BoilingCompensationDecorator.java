package com.example.recipeengine.instruction.handler.cook.boiling.decorator;

import com.example.recipeengine.instruction.cook.BaseCook;
import com.example.recipeengine.instruction.cook.decorator.BaseCookDecorator;

/**
 * Compensation decorator for resuming boiling saute.
 */
public class BoilingCompensationDecorator extends BaseCookDecorator {

  BaseCook wrapperObject;

  /**
   * Constructor of {@link BaseCookDecorator} class.
   *
   * @param wrappedObject contains {@link BaseCook} object to be decorated
   */
  public BoilingCompensationDecorator(BaseCook wrappedObject) {
    super(wrappedObject);
    this.wrapperObject = wrappedObject;
  }

  /**
   * Method to get the adapted time to cook value while resuming boiling instruction.
   *
   * @return It returns no change in visual score. Not required as of now.
   */
  @Override
  public Double getVisualScore() {
    final Double target = this.wrapperObject.getVisualScore() - deltaScore();

    if (target > this.wrapperObject.getTimeToCook()) {
      return this.wrapperObject.getVisualScore();
    }

    return target <= Double.valueOf(0) ? Double.valueOf(0) : target;
  }

  /**
   * This method is responsible for calculating the difference in target value
   * that must be added to original target.
   *
   * @return Integer This returns change in value of target.
   */
  private Double deltaScore() {
    return 0.0;
  }


}
