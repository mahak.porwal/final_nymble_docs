package com.example.recipeengine.instruction.cook.params.capture.captureconstants;

import androidx.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class CaptureSensor {
  public static final String VISUAL_CAMERA = "VISUAL_CAMERA";
  public static final String WEIGHT_SENSOR = "WEIGHT_SENSOR";
  private String captureSensor;

  public CaptureSensor(@AnnotationCaptureSensor String captureSensor) {
    this.captureSensor = captureSensor;
  }

  public String getCaptureSensor() {
    return captureSensor;
  }

  @StringDef({VISUAL_CAMERA,WEIGHT_SENSOR})
  @Retention(RetentionPolicy.SOURCE)
  public @interface AnnotationCaptureSensor {
  }
}