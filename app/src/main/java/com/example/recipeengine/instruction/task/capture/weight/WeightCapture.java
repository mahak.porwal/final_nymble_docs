package com.example.recipeengine.instruction.task.capture.weight;

import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.CaptureSensor;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.base.BaseCapturableTask;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.sensor.Sensors;

/**
 * This class is responsible for capturing the data from Weight Sensor.
 * It runs the {@link WeightSensorThread} class which contains the
 * algorithm.
 */
public class WeightCapture extends BaseCapturableTask {
  private final CaptureSensor sensor = new CaptureSensor(CaptureSensor.WEIGHT_SENSOR);
  private SensorParams sensorParams;
  private List<Double> capturedWeight;
  private RequestHandler hwRequestHandler;
  private Thread thread;

  /**
   * Constructor for WeightCapture class.
   *
   * @param sensorParams contains all the parameters required while capturing sensor data
   */
  public WeightCapture(String taskName,
                       SensorParams sensorParams) {
    super(null,null,null);
    hwRequestHandler = new RequestHandler();
    this.sensorParams = sensorParams;
    this.capturedWeight = new ArrayList<>();
    HashMap hashMap = new HashMap<Sensors, List<List<Double>>>();
    ArrayList<List<Double>> list = new ArrayList();
    list.add(capturedWeight);
    hashMap.put(Sensors.WEIGHT_SENSOR, list);
    this.taskName = taskName;
    super.capturedTaskData = new CapturedTaskData(hashMap, null, null, taskName);
  }

  @Override
  public CaptureSensor getSensor() {
    return sensor;
  }

  @Override
  public SensorParams getParams() {
    return sensorParams;
  }

  @Override
  public void resetCapturedTaskData() {
    capturedWeight.clear();
  }

  public List<Double> getCapturedWeight() {
    return capturedWeight;
  }

  @Override
  public void beginTask() {
    thread = getWeightSensorThreadInstance();
    thread.start();
  }

  public Thread getWeightSensorThreadInstance() {
    return new WeightSensorThread(CaptureSensor.WEIGHT_SENSOR, sensorParams, capturedWeight,
        hwRequestHandler);
  }

  @Override
  public void abortTask() {
    if (thread != null) {
      thread.interrupt();
    }
  }

  @Override
  public Thread getThread() {
    return thread;
  }

}
