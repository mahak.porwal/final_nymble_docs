package com.example.recipeengine.instruction.handler.cook.factory;

import static com.example.recipeengine.instruction.cook.CookType.BOILING;
import static com.example.recipeengine.instruction.cook.CookType.CONSISTENCY;
import static com.example.recipeengine.instruction.cook.CookType.DRY_WET;
import static com.example.recipeengine.instruction.cook.CookType.FRYING;
import static com.example.recipeengine.instruction.cook.CookType.MACRO_SIZE_REDUCTION;
import static com.example.recipeengine.instruction.cook.CookType.TEMPERATURE;
import static com.example.recipeengine.instruction.cook.CookType.WATTAGE;

import com.example.recipeengine.instruction.handler.cook.action.ActionBlock;
import com.example.recipeengine.instruction.handler.cook.boiling.BoilingAction;
import com.example.recipeengine.instruction.handler.cook.consistency.ConsistencyAction;
import com.example.recipeengine.instruction.handler.cook.drywet.DryWetAction;
import com.example.recipeengine.instruction.handler.cook.frying.FryingAction;
import com.example.recipeengine.instruction.handler.cook.macrosizereduction.MacroSizeAction;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.instruction.handler.cook.temperature.TemperatureAction;
import com.example.recipeengine.instruction.handler.cook.wattage.WattageAction;

import io.reactivex.subjects.PublishSubject;
import som.hardware.request.handler.RequestHandler;

/**
 * Generated ActionBlock object.
 */
public class ActionBlockFactory {

  /**
   * Creates an ActionBlock object based cooking type.
   *
   * @param cookingType cooking type
   * @return ActionBlock object
   */
  public static ActionBlock getActionBlockObject(String cookingType) {
    ActionBlock actionBlock = null;
    PublishSubject<UIData> uiData;
    PublishSubject<ProgressData> progressData;
    switch (cookingType) {
      case BOILING:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        actionBlock = new BoilingAction(new RequestHandler(), uiData, progressData);
        break;
      case CONSISTENCY:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        actionBlock = new ConsistencyAction(new RequestHandler(), uiData, progressData);
        break;
      case FRYING:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        actionBlock = new FryingAction(new RequestHandler(), uiData, progressData);
        break;
      case DRY_WET:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        actionBlock = new DryWetAction(new RequestHandler(), uiData, progressData);
        break;
      case MACRO_SIZE_REDUCTION:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        actionBlock = new MacroSizeAction(new RequestHandler(), uiData, progressData);
        break;
      case TEMPERATURE:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        actionBlock = new TemperatureAction(new RequestHandler(), uiData, progressData);
        break;
      case WATTAGE:
        uiData = PublishSubject.create();
        progressData = PublishSubject.create();
        actionBlock = new WattageAction(new RequestHandler(), uiData, progressData);
        break;
      default:
        break;
    }

    return actionBlock;
  }

}
