package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.instruction.cook.CookType;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;

import com.example.recipeengine.util.EquationResolver;
import com.example.recipeengine.util.MxEquationResolver;


import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.inject.Named;


public class BaseCookInfo implements CookInfo {

  private ActionInfo actionInfo;
  private StirrerInfo stirrerInfo;
  private CaptureParams captureParams;
  private InferParams inferParams;
  private Map<String, String> parametricFields;
  private EquationResolver equationResolver;
  private CookType cookType;
  private Integer instructionId;
  private Integer attemptNum;


  public BaseCookInfo(CookType cookType, ActionInfo actionInfo, StirrerInfo stirrerInfo, CaptureParams captureParams,
                      InferParams inferParams, Map<String, String> parametricFields,
                      Integer instructionId, Integer attemptNum) {
    this.cookType = cookType;
    this.actionInfo = actionInfo;
    this.stirrerInfo = stirrerInfo;
    this.captureParams = captureParams;
    this.inferParams = inferParams;
    this.parametricFields = parametricFields;
    this.equationResolver = new MxEquationResolver();
    this.instructionId = instructionId;
    this.attemptNum = attemptNum;
  }

  @Override
  public ActionInfo getActionInfo() {
    return this.actionInfo;
  }

  @Override
  public StirrerInfo getStirrerInfo() {
    return this.stirrerInfo;
  }

  @Override
  public InferParams getInferParams() {
    return this.inferParams;
  }

  @Override
  public CaptureParams getCaptureParams() {
    return this.captureParams;
  }

  @Override
  public CookType getCookType() {
    return this.cookType;
  }

  @Override
  public Integer getInstructionId() {
    return this.instructionId;
  }

  @Override
  public Integer getAttemptNum() {
    return this.attemptNum;
  }

  @Override
  public String getInstructionType() {
    return "Cook";
  }

  @Override
  public Map<String, String> getParametricFields() {
    return Collections.unmodifiableMap(this.parametricFields);
  }

  @Override
  public void resolveParametricFields(Map<String, String> parameters) {

    Iterator<Entry<String, String>> iter = this.parametricFields.entrySet().iterator();
    while (iter.hasNext()) {
      Map.Entry<String, String> parametricField = iter.next();
      this.equationResolver.setParametricField(parametricField.getKey(), parametricField.getValue());
      this.equationResolver.resolveParametricField(parametricField.getKey(), parameters);
      if (this.equationResolver.isFieldResolved(parametricField.getKey()) == true) {
        Double resolvedValue = this.equationResolver.getResolvedValue(parametricField.getKey());
        this.setResolvedValue(parametricField.getKey(), Double.toString(resolvedValue));
        iter.remove();
      }
    }
  }

  private void setResolvedValue(String parametricField, String resolvedValue) {
    //use reflection to set the parametric field with the resolvedValue
    //find in which the parametric field resides
    String parameterFieldString = parametricField +
      "String";
    if (MxEquationResolver.setResolvedValue(parameterFieldString, resolvedValue,
      this.actionInfo)) {
      return;
    }
    if (MxEquationResolver.setResolvedValue(parameterFieldString, resolvedValue,
      this.stirrerInfo)) {
      return;
    }
  }

  @Override
  public boolean isExecutable() {
    return this.parametricFields.isEmpty();
  }

}
