package com.example.recipeengine.instruction.blocks;

import java.io.Closeable;

public interface Block extends Closeable {
  String getBlockStatus();

  void pause() throws Exception;
}
