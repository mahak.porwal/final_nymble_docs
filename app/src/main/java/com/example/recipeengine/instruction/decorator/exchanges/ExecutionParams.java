package com.example.recipeengine.instruction.decorator.exchanges;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ExecutionParams {
  @JsonProperty("PRE_DISPENSE_WEIGHT")
  PRE_DISPENSE_WEIGHT,
  @JsonProperty("COMPLETE_DISPENSE_CYCLE")
  COMPLETE_DISPENSE_CYCLE,
  @JsonProperty("TEMPERATURE")
  TEMPERATURE,
  @JsonProperty("TIME_SPENT")
  TIME_SPENT
}
