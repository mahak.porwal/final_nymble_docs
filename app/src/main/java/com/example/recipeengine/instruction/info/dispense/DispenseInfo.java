package com.example.recipeengine.instruction.info.dispense;

import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;
import com.example.recipeengine.instruction.info.InstructionInfo;

import java.util.List;

public interface DispenseInfo extends InstructionInfo {
  String getQuantityInfo();

  List<String> getIngredients();

  PreDispenseBlock getPreDispenseInfo();

  DispenseBlock getDispenseInfo();

  PostDispenseBlock getPostDispenseInfo();

  List<InferInstruction> getInferInstructionsInfo();
}
