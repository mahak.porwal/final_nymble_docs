package com.example.recipeengine.instruction.decorator.source;

import androidx.core.util.Pair;
import com.example.recipeengine.instruction.decorator.exchanges.InstructionInputParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import java.util.Map;
import lombok.NoArgsConstructor;
/**
 * This class contains information about modifier parameters and decoration on target instruction.
 */

@NoArgsConstructor
public class InstructionDecoration {
  private Double instructionWeight;
  private Map<OutputParams, Double> paramsDecorationWeight;
  private Map<OutputParams, Map<String, Map<InstructionInputParams, String>>> parameters;
  private Map<OutputParams, Map<String, Pair<Double, Double>>> discretizationMap;

  /**
   * Constructor for Instruction Decoration.
   *
   * @param instructionWeight weightage of a particular instruction
   * @param paramsDecorationWeight Map of multiple parameters and their weightage
   * @param parameters Map of multiple parameters and their bucket of target values
   * @param discretizationMap Map of multiple parameters and their bucket value
   *                          corresponding to a range of values
   */
  public InstructionDecoration(Double instructionWeight,
                               Map<OutputParams, Double> paramsDecorationWeight,
                               Map<OutputParams, Map<String,
                                   Map<InstructionInputParams, String>>> parameters,
                               Map<OutputParams,
                                   Map<String, Pair<Double, Double>>> discretizationMap) {
    this.instructionWeight = instructionWeight;
    this.paramsDecorationWeight = paramsDecorationWeight;
    this.parameters = parameters;
    this.discretizationMap = discretizationMap;
  }

  public Double getInstructionWeight() {
    return instructionWeight;
  }

  public void setInstructionWeight(Double instructionWeight) {
    this.instructionWeight = instructionWeight;
  }

  public Map<OutputParams, Double> getParamsDecorationWeight() {
    return paramsDecorationWeight;
  }

  public void setParamsDecorationWeight(
      Map<OutputParams, Double> paramsDecorationWeight) {
    this.paramsDecorationWeight = paramsDecorationWeight;
  }

  public Map<OutputParams, Map<String, Map<InstructionInputParams, String>>> getParameters() {
    return parameters;
  }

  public void setParameters(
      Map<OutputParams, Map<String, Map<InstructionInputParams, String>>> parameters) {
    this.parameters = parameters;
  }

  public Map<OutputParams, Map<String, Pair<Double, Double>>> getDiscretizationMap() {
    return discretizationMap;
  }

  public void setDiscretizationMap(
      Map<OutputParams, Map<String, Pair<Double, Double>>> discretizationMap) {
    this.discretizationMap = discretizationMap;
  }
}
