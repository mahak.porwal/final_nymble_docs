package com.example.recipeengine.instruction.handler.dispense.blocks.infer;

import static com.example.recipeengine.util.ImageUtil.centerCrop;

import android.graphics.Bitmap;
import android.util.Pair;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.handler.cook.infer.BaseInferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.util.FileUtil;
import io.reactivex.subjects.PublishSubject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.pytorch.IValue;
import org.pytorch.Module;
import org.pytorch.Tensor;
import org.pytorch.torchvision.TensorImageUtils;

public class OnionSizeInfer extends BaseInferBlock {

  public static final int MODEL_INPUT_IMAGE_SIZE = 224;
  private static final Point INPUT_IMAGE_CROP_ORIGIN = new Point(420, 0);
  private static final int INPUT_IMAGE_CROP_SIZE = 1080;
  List<Double> result = new ArrayList<>();
  private Module model;
  private final InferParams inferParameters;

  /**
   * Constructor for BaseInferBlock.
   *
   * @param recipeContext information about recipe
   * @param inferParams   {@link InferParams} object
   * @param uiEmitter     RxJava subject to send results to UI
   * @param dataLogger    RxJava subject to log results
   */
  public OnionSizeInfer(
      RecipeContext recipeContext,
      InferParams inferParams,
      PublishSubject<UIData> uiEmitter,
      PublishSubject<ProgressData> dataLogger) {
    super(recipeContext, inferParams, uiEmitter, dataLogger);
    this.inferParameters = inferParams;
    try {
      loadModel();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Method to load model file in memory. Uses infer parameters.
   *
   * @throws Exception exception
   */
  public void loadModel() throws Exception {
    final String modelData = fetchModelFile(this.inferParameters.getModelFileName());
    if (null != modelData) {
      this.model = Module.load(modelData);
    }
    if (null == this.model) {
      throw new Exception("Model not loaded");
    }
  }

  /**
   * Loading the model from the internal memory.
   *
   * @param modelName model file name
   * @return file's absolute path
   */
  @VisibleForTesting
  @Nullable
  String fetchModelFile(final String modelName) {
    final File file = FileUtil.getModelFile(modelName);
    if (file.exists() && file.length() > 0) {
      return file.getAbsolutePath();
    }
    System.out.printf("File not found");
    return null;
  }


  @Override
  public InferredResult infer(List<CapturedTaskData> capturedTaskData) throws Exception {
    List<Bitmap> bitmaps = new ArrayList<>();
    final ProgressData progressData = new ProgressData(null, null, null);
    final UIData uiData = new UIData(null, null);
    for (CapturedTaskData capturedTaskDataItem : capturedTaskData) {
      bitmaps.addAll(capturedTaskDataItem.getVisionCamera());
    }
    for (Bitmap bmp : bitmaps) {
      final Mat image = new Mat();
      Utils.bitmapToMat(bmp, image);
//        final boolean isValid = isValidImageBrightness(image);
      if (true) {
        //infer here when image is fine
        bmp = centerCrop(bmp, INPUT_IMAGE_CROP_ORIGIN, INPUT_IMAGE_CROP_SIZE);
        final Bitmap croppedBmp = _parseBitmap(image);
        final Tensor output = this.model.forward(getImageTensor(bmp)).toTensor();
        final float[] score_arr = output.getDataAsFloatArray();
        result.add(getIndexOfMaxScore(score_arr));
        final Pair<OutputParams, String> logData =
            new Pair<>(OutputParams.ONION_SIZE_SCORE, String.valueOf(result.get(0)));
        progressData.setOutputData(logData);
        getDataLogger().onNext(progressData);
        updateInferStatus(result.get(result.size() - 1));
        uiData.setImage(bmp);
        getUiEmitter().onNext(uiData);
      }
    }
    return new InferredResult(result);
  }

  private Double getIndexOfMaxScore(float[] score_arr) {

    if (score_arr.length == 0) {
      return 0.0;
    } else if (score_arr.length == 1) {
      return Double.valueOf(score_arr[0]);
    }

    float max = score_arr[0];
    int index = 0;
    for (int i = 1; i < score_arr.length; i++) {
      if (score_arr[i] > max) {
        max = score_arr[i];
        index = i;
      }
    }
    return Double.valueOf(index);
  }

  @Override
  public void close() throws IOException {
    this.model.destroy();
  }

  private IValue getImageTensor(Bitmap bmp) {
    final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, 224, 224, true);
    final Tensor input = TensorImageUtils.bitmapToFloat32Tensor(
        scaledBitmap,
        TensorImageUtils.TORCHVISION_NORM_MEAN_RGB,
        TensorImageUtils.TORCHVISION_NORM_STD_RGB
    );
    return IValue.from(input);
  }

  /**
   * Pausing or halting the current frying operation.
   */
  @Override
  public void pause() {
    this.model.destroy();
  }

  /**
   * Method to update the infer status string for debugging.
   *
   * @param mean score from infer results
   */
  private void updateInferStatus(double mean) {
    this.inferStatus =
        "The class index is : " + mean;
  }

  /**
   * Convert Mat to Bitmap.
   */
  private Bitmap _parseBitmap(final Mat mat) {
    final Bitmap bmp = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
    Utils.matToBitmap(mat, bmp);
    return bmp;
  }

}
