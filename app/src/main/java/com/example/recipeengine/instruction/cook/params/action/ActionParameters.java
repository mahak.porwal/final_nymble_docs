package com.example.recipeengine.instruction.cook.params.action;

/**
 * This class contains all values related to ActionParams of Cook Instruction.
 */
public class ActionParameters implements ActionParams {

  private Integer targetTemperature;
  private Integer timeToCook;
  private Double visualScore;
  private Double thermalScore;
  private Integer defaultWattage;

  /**
   * Constructor of ActionParameters class.
   *
   * @param targetTemperature current Cook Instructions target temperature
   * @param timeToCook        current Cook Instructions time needed to cook
   * @param visualScore       current Cook Instructions visual score
   * @param thermalScore      current Cook Instructions thermal score
   * @param defaultWattage    current Cook Instructions wattage
   */
  public ActionParameters(Integer targetTemperature, Integer timeToCook, Double visualScore,
                          Double thermalScore, Integer defaultWattage) {
    this.targetTemperature = targetTemperature;
    this.timeToCook = timeToCook;
    this.visualScore = visualScore;
    this.thermalScore = thermalScore;
    this.defaultWattage = defaultWattage;
  }

  @Override
  public Integer getDefaultWattage() {
    return defaultWattage;
  }

  @Override
  public Integer getTimeToCook() {
    return timeToCook;
  }

  @Override
  public Double getVisualScore() {
    return visualScore;
  }

  @Override
  public Integer getTemperature() {
    return targetTemperature;
  }

  @Override
  public Double getThermalScore() {
    return thermalScore;
  }
}
