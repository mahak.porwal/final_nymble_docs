package com.example.recipeengine.instruction.task.action;


import som.instruction.request.actuator.Actuators.AnnotationActuator;

public class BaseActionableTask implements ActionableTask {

  private String actuator;
  private ActionTaskParams actionTaskParams;
  private String taskName;
  private Thread thread;


  public BaseActionableTask(String taskName, @AnnotationActuator String actuator, ActionTaskParams actionTaskParams) {

    this.actuator = actuator;
    this.actionTaskParams = actionTaskParams;
    this.taskName = taskName;
  }

  @Override
  public String getActuator() {
    return this.actuator;
  }

  @Override
  public ActionTaskParams getActionTaskParams() {
    return this.actionTaskParams;
  }

  @Override
  public void beginTask() {
    this.thread = new ActionTaskThread(this.getTaskName(), this.getActuator(),
      this.actionTaskParams);
    thread.start();
  }

  @Override
  public void abortTask() {
    if (thread != null) {
      thread.interrupt();
    }
  }

  @Override
  public Thread getThread() {
    return thread;
  }

  @Override
  public String getTaskName() {
    return this.taskName;
  }
}
