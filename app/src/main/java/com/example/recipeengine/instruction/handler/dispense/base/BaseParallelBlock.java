package com.example.recipeengine.instruction.handler.dispense.base;

import com.example.recipeengine.instruction.blocks.ParallelTaskBlock;
import com.example.recipeengine.instruction.task.capture.base.Task;

import java.io.IOException;
import java.util.List;

/**
 * This class implements the methods of {@link ParallelTaskBlock}.
 * It is responsible for running a list of tasks in parallel.
 */
public abstract class BaseParallelBlock implements ParallelTaskBlock {

  private List<? extends Task> parallelTasks;

  /**
   * Constructor for BaseParallelBlock.
   *
   * @param parallelTasks list of parallel tasks.
   */
  public BaseParallelBlock(
      List<? extends Task> parallelTasks) {
    this.parallelTasks = parallelTasks;
  }

  /**
   * Runs tasks sequentially by calling{@link Task#beginTask()}.
   * methods.
   *
   * @throws InterruptedException Exception
   */
  @Override
  public void startParallelTasks() throws InterruptedException {
    for (Task task : parallelTasks) {
      task.beginTask();
    }
    for (Task task : parallelTasks) {
      task.getThread().join();
    }
  }

  @Override
  public List<? extends Task> getParallelTasks() {
    return parallelTasks;
  }


  public abstract String getBlockStatus();

  @Override
  public void pause() throws Exception {
    for (Task task : parallelTasks) {
      task.getThread().interrupt();
    }
  }
  @Override
  public void close() throws IOException {

  }
}
