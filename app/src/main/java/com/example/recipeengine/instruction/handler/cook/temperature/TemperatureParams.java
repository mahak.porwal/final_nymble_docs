package com.example.recipeengine.instruction.handler.cook.temperature;

public class TemperatureParams {
  Integer temperature;
  Double standardDeviation;

  public TemperatureParams(Integer temperature, Double standardDeviation) {
    this.temperature = temperature;
    this.standardDeviation = standardDeviation;
  }

  public Integer getTemperature() {
    return this.temperature;
  }

  public Double getStandardDeviation() {
    return this.standardDeviation;
  }

}
