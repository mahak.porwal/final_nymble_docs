package com.example.recipeengine.instruction.rectifier;

import com.example.recipeengine.instruction.blocks.ExecutionBlock;

public interface InstructionRectifier extends ExecutionBlock {
  void rectify();
}
