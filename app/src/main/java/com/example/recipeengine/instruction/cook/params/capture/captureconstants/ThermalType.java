package com.example.recipeengine.instruction.cook.params.capture.captureconstants;

import androidx.annotation.StringDef;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ThermalType {
  public static final String MATRIX = "MATRIX";
  public static final String IMAGE = "IMAGE";
  private String thermalType;

  public ThermalType(@AnnotationThermalType String thermalType) {
    this.thermalType = thermalType;
  }

  public String getThermalType() {
    return thermalType;
  }

  @StringDef({MATRIX, IMAGE})
  @Retention(RetentionPolicy.SOURCE)
  public @interface AnnotationThermalType {
  }

}
