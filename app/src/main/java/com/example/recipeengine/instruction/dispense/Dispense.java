package com.example.recipeengine.instruction.dispense;

import com.example.recipeengine.instruction.BaseInstruction;
import com.example.recipeengine.instruction.adaptor.DispenseAdaptor;
import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;
import java.util.List;

/**
 * This class is a blueprint of a Dispense Object. It implements {@link BaseDispense}
 * and extends {@link BaseInstruction}.
 * The methods this class overrides are {@link #getQuantityInGrams()} which returns quantity
 * of the Dispense Instruction
 * {@link #getIngredients()} which returns the list of ingredients.
 * {@link #adapt()} which calls the {@link DispenseAdaptor#adapt()} function and returns the
 * adaptedDispense.
 */
public class Dispense extends BaseInstruction<BaseDispense> implements BaseDispense {
  private Integer quantityInGrams;
  private List<String> ingredients;
  private BaseDispense adaptedDispense;
  private DispenseAdaptor dispenseAdaptor;
  private PreDispenseBlock preDispenseBlock;
  private DispenseBlock dispenseBlock;
  private PostDispenseBlock postDispenseBlock;
  private List<InferInstruction> inferInstructions;

  /**
   * Constructor for Dispense Instruction.
   *
   * @param instructionId     instructionId
   * @param attemptNum        attemptNum
   * @param quantityInGrams   quantityInGrams
   * @param ingredients       ingredients
   * @param preDispenseBlock  preDispenseBlock
   * @param dispenseBlock     dispenseBlock
   * @param postDispenseBlock postDispenseBlock
   * @param inferInstructions inferInstructions
   */
  public Dispense(Integer instructionId, Integer attemptNum, Integer quantityInGrams,
                  List<String> ingredients,
                  PreDispenseBlock preDispenseBlock,
                  DispenseBlock dispenseBlock, PostDispenseBlock postDispenseBlock,
                  List<InferInstruction> inferInstructions) {
    super(instructionId, attemptNum);
    this.quantityInGrams = quantityInGrams;
    this.ingredients = ingredients;
    this.preDispenseBlock = preDispenseBlock;
    this.dispenseBlock = dispenseBlock;
    this.postDispenseBlock = postDispenseBlock;
    this.inferInstructions = inferInstructions;
  }

  @Override
  public Integer getQuantityInGrams() {
    return quantityInGrams;
  }

  @Override
  public List<String> getIngredients() {
    return ingredients;
  }

  @Override
  public BaseDispense adapt() {
    dispenseAdaptor = makeDispenseAdaptorObject(this);
    adaptedDispense = dispenseAdaptor.adapt();
    return adaptedDispense;
  }

  public DispenseAdaptor makeDispenseAdaptorObject(Dispense dispense) {
    return new DispenseAdaptor(dispense);
  }

  public BaseDispense getAdaptedDispense() {
    return adaptedDispense;
  }

  public PreDispenseBlock getPreDispenseBlock() {
    return preDispenseBlock;
  }

  public DispenseBlock getDispenseBlock() {
    return dispenseBlock;
  }

  public PostDispenseBlock getPostDispenseBlock() {
    return postDispenseBlock;
  }

  public List<InferInstruction> getInferInstructions() {
    return inferInstructions;
  }
}
