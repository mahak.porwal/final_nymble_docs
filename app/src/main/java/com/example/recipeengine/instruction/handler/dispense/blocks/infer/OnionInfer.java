package com.example.recipeengine.instruction.handler.dispense.blocks.infer;

import static com.example.recipeengine.util.ImageUtil.centerCrop;
import static com.example.recipeengine.util.ImageUtil.scaleBitmapAndKeepRatio;

import android.graphics.Bitmap;
import android.util.Log;
import androidx.annotation.NonNull;
import com.example.recipeengine.BuildConfig;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.handler.cook.infer.BaseInferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.util.FileUtil;
import io.reactivex.subjects.PublishSubject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import timber.log.Timber;

public class OnionInfer extends BaseInferBlock {

  private static final int INPUT_MODEL_IMAGE_SIZE = 256;
  //Please verify Crop values
  private static final Point INPUT_IMAGE_CROP_ORIGIN = new Point(432, 12);
  private static final int INPUT_IMAGE_CROP_SIZE = 1056;
  private IngredientSegmentation ingredientSegmentation;
  List<Double> result = new ArrayList<>();
  private double onionPercentage;
  private double backgroundPercentage;
  private double babyCornPercentage;
  private double eggPlantPercentage;


  /**
   * Constructor for BaseInferBlock.
   *
   * @param recipeContext information about recipe
   * @param inferParams   {@link InferParams} object
   * @param uiEmitter     RxJava subject to send results to UI
   * @param dataLogger    RxJava subject to log results
   */
  public OnionInfer(
      RecipeContext recipeContext,
      InferParams inferParams,
      PublishSubject<UIData> uiEmitter,
      PublishSubject<ProgressData> dataLogger) {
    super(recipeContext, inferParams, uiEmitter, dataLogger);
    this.ingredientSegmentation = new IngredientSegmentation(inferParams.getModelFileName());
    try {
      loadModel();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  @Override
  public InferredResult infer(List<CapturedTaskData> capturedTaskData) throws Exception {
    List<Bitmap> bitmaps = new ArrayList<>();
    for (CapturedTaskData capturedTaskDataItem : capturedTaskData) {
      bitmaps.addAll(capturedTaskDataItem.getVisionCamera());
    }

    final UIData uiData = new UIData(null, null);
    final ProgressData progressData = new ProgressData(null, null, null);
    this.result.clear();
    if (BuildConfig.DEBUG && 2 > bitmaps.size()) {
      throw new AssertionError("Assertion failed");
    }
    for (int i = 0; i < bitmaps.size(); i++) {
      final Bitmap bitmap =
          centerCrop(bitmaps.get(i), INPUT_IMAGE_CROP_ORIGIN, INPUT_IMAGE_CROP_SIZE);
      final Bitmap scaledBitmap =
          scaleBitmapAndKeepRatio(bitmap, INPUT_MODEL_IMAGE_SIZE, INPUT_MODEL_IMAGE_SIZE);
      //The food mask with pixel values
      final Mat foodMask = this.ingredientSegmentation.runSegmentation(scaledBitmap);
      uiData.setImage(_parseBitmap(foodMask));
      getUiEmitter().onNext(uiData);
      result.add(postProcessModelOutput(foodMask));
    }
    return new InferredResult(this.result);
  }


  public void loadModel() throws IOException {
    this.ingredientSegmentation.loadModel();
  }

  /**
   * Getting the food pixel percentage from food mask.
   *
   * @param mask output of model
   * @return double
   */
  private double postProcessModelOutput(Mat mask) {
    int[] classSegregate = new int[4];
    Arrays.fill(classSegregate, 0);
    for (int x = 0; IngredientSegmentation.OUTPUT_SIZE > x; x++) {
      for (int y = 0; IngredientSegmentation.OUTPUT_SIZE > y; y++) {
        byte[] data = new byte[1];
        mask.get(x, y, data);
        if (0 == data[0]) {
          classSegregate[0]++;
        } else if (50 == data[0]) {
          classSegregate[1]++;
        } else if (100 == data[0]) {
          classSegregate[2]++;
        } else if (127 == data[0]) {
          classSegregate[3]++;
        }
      }
    }

    backgroundPercentage =
        (double) classSegregate[0] /
            (IngredientSegmentation.OUTPUT_SIZE * IngredientSegmentation.OUTPUT_SIZE);
    babyCornPercentage =
        (double) classSegregate[1] /
            (IngredientSegmentation.OUTPUT_SIZE * IngredientSegmentation.OUTPUT_SIZE);
    eggPlantPercentage =
        (double) classSegregate[2] /
            (IngredientSegmentation.OUTPUT_SIZE * IngredientSegmentation.OUTPUT_SIZE);
    onionPercentage =
        (double) classSegregate[3] /
            (IngredientSegmentation.OUTPUT_SIZE * IngredientSegmentation.OUTPUT_SIZE);

    return 0;
  }

  private void saveImage(@NonNull Bitmap image1, int number, double mean) {
    final String filename1 =
        "Image_" + number + "_" + (mean * 100) + ".jpg";
    final File imageFile = FileUtil.getMacroSizeImageFile(filename1);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image1.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename1);
    }
  }

  /**
   * Convert Opencv.Mat to Bitmap
   *
   * @param mat opencv mat object
   * @return Bitmap
   */
  private Bitmap _parseBitmap(Mat mat) {
    Bitmap bmp = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
    Utils.matToBitmap(mat, bmp);
    return bmp;
  }

  /**
   * Method to update the infer status string for debugging.
   */
  private void updateInferStatus() {
    this.inferStatus =
        "Background Percentage : " + backgroundPercentage + "\nBabyCorn Percentage : " +
            babyCornPercentage + "\nEggPlant Percentage : " + eggPlantPercentage +
            "\nOnion Percentage : " + onionPercentage;
    Log.d("DISPENSE_INFER", this.inferStatus);
  }

  /**
   * Method to halt the current operations of size reduction saute.
   */
  @Override
  public void pause() {
    this.ingredientSegmentation.close();
  }

  @Override
  public void close() throws IOException {
    this.ingredientSegmentation.close();
  }
}
