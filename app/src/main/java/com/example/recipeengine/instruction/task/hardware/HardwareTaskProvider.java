package com.example.recipeengine.instruction.task.hardware;

import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseHwInformation;
import com.example.recipeengine.instruction.task.capture.base.Task;

import java.util.List;

public interface HardwareTaskProvider {

  List<Task> hwTasksProvider();

  List<Task> hwTasksProvider(DispenseHwInformation[] dispenseHwInformation);

}
