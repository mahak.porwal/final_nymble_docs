package com.example.recipeengine.instruction.handler.cook.consistency;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.handler.cook.infer.BaseInferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.util.FileUtil;
import io.reactivex.subjects.PublishSubject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.pytorch.IValue;
import org.pytorch.Module;
import org.pytorch.Tensor;
import org.pytorch.torchvision.TensorImageUtils;
import timber.log.Timber;

/**
 * Infer class for consistency instruction.
 * Only responsible for giving inferred results for consistency.
 *
 * @author Pranshu Gupta
 */
public class ConsistencyInfer extends BaseInferBlock {

  private static final int NO_OF_IMAGES_REQUIRED = 4;
  private final InferParams inferParameters;
  private final int servingSize;
  List<Double> result = new LinkedList<>();
  private Module model;

  /**
   * Constructor of consistency infer class, child class of baseInferBlock.
   * Responsible for core functioning of consistency
   *
   * @param inferParameters Contains instruction specific information
   */
  public ConsistencyInfer(InferParams inferParameters,
                          RecipeContext recipeContext,
                          PublishSubject<UIData> uiEmitter,
                          PublishSubject<ProgressData> dataLogger) {
    super(recipeContext, inferParameters, uiEmitter, dataLogger);
    this.inferParameters = inferParameters;
    this.servingSize = 1;
    try {
      loadModel();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Method to load model file in memory. Uses infer parameters.
   *
   * @throws Exception exception
   */
  public void loadModel() throws Exception {
    final String modelData = fetchModelFile(this.inferParameters.getModelFileName());
    if (null != modelData) {
      this.model = Module.load(modelData);
    }
    if (null == this.model) {
      throw new Exception("Model not loaded");
    }
  }

  /**
   * Infer function contains core functionality of vision model.
   *
   * @param captureData CapturedData
   * @return InferredResult
   */
  @Override
  @Nullable
  public InferredResult infer(List<CapturedTaskData> captureData) {
    this.result.clear();
    final ProgressData progressData = new ProgressData(null, null, null);
    final UIData uiData = new UIData(null, null);
    CapturedTaskData capturedTaskData = captureData.get(0);
    final List<Bitmap> bmps = capturedTaskData.getVisionCamera();
    if (null != bmps && NO_OF_IMAGES_REQUIRED == bmps.size()) {
      final long[] data = {this.servingSize};
      final long[] shape = {1};
      final Tensor input5 = Tensor.fromBlob(data, shape);

      final Tensor model_out = this.model.forward(getImageTensor(bmps.get(0)),
          getImageTensor(bmps.get(1)),
          getImageTensor(bmps.get(2)),
          getImageTensor(bmps.get(3)),
          IValue.from(input5)).toTensor();

      progressData.setImageData(new Pair<>("image1", bmps.get(0)));
      uiData.setImage(bmps.get(0));
      final float[] out_arr = model_out.getDataAsFloatArray();
      this.result.add(Double.valueOf(out_arr[0]));
      ///Filling ui and progress data
      final Map<String, String> modelOut = new ConcurrentHashMap<>();
      modelOut.put("Consistency score", String.valueOf(this.result.get(0)));
      uiData.setData(modelOut);
      progressData.setOutputData(
          new Pair(OutputParams.CONSISTENCY_SCORE,
              this.result.get(0)));
      //emitting ui and progress data
      getUiEmitter().onNext(uiData);
      getDataLogger().onNext(progressData);

      for (int i = 0; i < bmps.size(); i++) {
        saveImage(bmps.get(i), i + 1);
      }

      Log.d("CONSISTENCY_IMAGE", "Model output : " + this.result.get(0));
      return new InferredResult(this.result);
    } else {
      return null;
    }

  }

  private void saveImage(@NonNull Bitmap image1, int number) {
    final String filename1 =
        "Image_" + number + "_" + this.result.get(0) + ".jpg";
    final File imageFile = FileUtil.getConsistencyImageFile(filename1);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image1.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename1);
    }
  }

  /**
   * Method to close the current infer operation and release the loaded model from memory.
   */
  @Override
  public void pause() {
    this.model.destroy();
  }


  /**
   * Creating a IValue object used for pytorch inferring of received Bitmap.
   *
   * @param bmp Input image
   * @return IValue of input image
   */
  private IValue getImageTensor(Bitmap bmp) {
    final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, 224, 224, true);
    final Tensor input = TensorImageUtils.bitmapToFloat32Tensor(
        scaledBitmap,
        TensorImageUtils.TORCHVISION_NORM_MEAN_RGB,
        TensorImageUtils.TORCHVISION_NORM_STD_RGB
    );
    return IValue.from(input);
  }

  /**
   * Loading the model from the internal memory.
   *
   * @param modelName model file name
   * @return file's absolute path
   */
  @VisibleForTesting
  @Nullable
  String fetchModelFile(final String modelName) {
    final File file = FileUtil.getModelFile(modelName);
    if (file.exists() && file.length() > 0) {
      return file.getAbsolutePath();
    }
    System.out.printf("File not found");
    return null;
  }

  public void setModel(Module model) {
    this.model = model;
  }

  @Override
  public void close() throws IOException {
    this.model.destroy();
  }
}
