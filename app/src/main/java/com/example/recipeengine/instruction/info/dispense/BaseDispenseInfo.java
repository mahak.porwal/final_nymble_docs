package com.example.recipeengine.instruction.info.dispense;

import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;
import com.example.recipeengine.util.EquationResolver;
import com.example.recipeengine.util.MxEquationResolver;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class BaseDispenseInfo implements DispenseInfo {

  private String quantityInGramsString;
  private List<String> ingredients;
  private Map<String, String> parametricFields;
  private EquationResolver equationResolver;
  private PreDispenseBlock preDispenseBlock;
  private DispenseBlock dispenseBlock;
  private PostDispenseBlock postDispenseBlock;
  private List<InferInstruction> inferInstructions;
  private Integer instructionId;
  private Integer attemptNum;

  public BaseDispenseInfo(String quantityInfo, List<String> ingredients,
                          PreDispenseBlock preDispenseBlock, DispenseBlock dispenseBlock,
                          PostDispenseBlock postDispenseBlock,
                          List<InferInstruction> inferInstructions, Map<String, String> parametricFields,
                          Integer instructionId, Integer attemptNum) {
    this.preDispenseBlock = preDispenseBlock;
    this.dispenseBlock = dispenseBlock;
    this.postDispenseBlock = postDispenseBlock;
    this.inferInstructions = inferInstructions;
    this.quantityInGramsString = quantityInfo;
    this.ingredients = ingredients;
    this.parametricFields = parametricFields;
    this.equationResolver = new MxEquationResolver();
    this.instructionId = instructionId;
    this.attemptNum = attemptNum;
  }


  @Override
  public String getQuantityInfo() {
    return this.quantityInGramsString;
  }

  @Override
  public List<String> getIngredients() {
    return this.ingredients;
  }

  @Override
  public Integer getInstructionId() {
    return this.instructionId;
  }

  @Override
  public Integer getAttemptNum() {
    return this.attemptNum;
  }

  @Override
  public String getInstructionType() {
    return "Dispense";
  }

  @Override
  public Map<String, String> getParametricFields() {
    return Collections.unmodifiableMap(this.parametricFields);
  }

  @Override
  public void resolveParametricFields(Map<String, String> parameters) {
    Iterator<Entry<String, String>> iter = this.parametricFields.entrySet().iterator();
    while (iter.hasNext()) {
      Map.Entry<String, String> parametricField = iter.next();
      this.equationResolver.setParametricField(parametricField.getKey(), parametricField.getValue());
      this.equationResolver.resolveParametricField(parametricField.getKey(), parameters);
      if (this.equationResolver.isFieldResolved(parametricField.getKey()) == true) {
        Double resolvedValue = this.equationResolver.getResolvedValue(parametricField.getKey());
        this.setResolvedValue(parametricField.getKey(), Double.toString(resolvedValue));
        iter.remove();
      }
    }
  }

  private void setResolvedValue(String parametricField, String resolvedValue) {
    //use reflection to set the parametric field with the resolvedValue
    //find in which the parametric field resides
    String parameterFieldString = parametricField +
      "String";
    MxEquationResolver.setResolvedValue(parameterFieldString, resolvedValue, this);
  }

  @Override
  public boolean isExecutable() {
    return this.parametricFields.isEmpty();
  }

  public PreDispenseBlock getPreDispenseInfo() {
    return preDispenseBlock;
  }

  public DispenseBlock getDispenseInfo() {
    return dispenseBlock;
  }

  public PostDispenseBlock getPostDispenseInfo() {
    return postDispenseBlock;
  }

  public List<InferInstruction> getInferInstructionsInfo() {
    return inferInstructions;
  }
}
