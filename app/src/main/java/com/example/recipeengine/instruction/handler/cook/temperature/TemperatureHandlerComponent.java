package com.example.recipeengine.instruction.handler.cook.temperature;

import com.example.recipeengine.instruction.handler.cook.handlermodules.TemperatureHandlerModule;

import dagger.Component;

@Component(modules = TemperatureHandlerModule.class)
public interface TemperatureHandlerComponent {
  TemperatureHandler buildTemperatureHandlerObject();
}
