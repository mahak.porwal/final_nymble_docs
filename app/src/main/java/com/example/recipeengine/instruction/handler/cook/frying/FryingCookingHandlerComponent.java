package com.example.recipeengine.instruction.handler.cook.frying;

import com.example.recipeengine.instruction.handler.cook.handlermodules.FryingHandlerModule;
import dagger.Component;

@Component(modules = FryingHandlerModule.class)
public interface FryingCookingHandlerComponent {

  FryingCookingHandler buildFryingHandlerObject();

}
