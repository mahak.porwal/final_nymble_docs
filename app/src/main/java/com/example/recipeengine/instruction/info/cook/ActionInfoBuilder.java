package com.example.recipeengine.instruction.info.cook;

import java.io.IOException;
import java.util.Map;
import javax.inject.Inject;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;


public class ActionInfoBuilder {
  @Inject
  public ActionInfoBuilder() {
  }

  public ActionInfo provideActionInformation(JSONObject jsonObject) {
    ActionInfo actionInfo = null;
    try {
      actionInfo = createActionInfoObject(jsonObject);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return actionInfo;
  }

  private ActionInfo createActionInfoObject(JSONObject jsonObject)
      throws IOException, ParseException {

    Map address = ((Map) jsonObject.get("actionParams"));

    return new ActionInformation((String) address.getOrDefault("targetTemperature", null),
        (String) address.getOrDefault("timeToCook", null),
        (String) address.getOrDefault("visualScore", null),
        (String) address.getOrDefault("thermalScore", null),
        (String) address.getOrDefault("defaultWattage", null));
  }


}
