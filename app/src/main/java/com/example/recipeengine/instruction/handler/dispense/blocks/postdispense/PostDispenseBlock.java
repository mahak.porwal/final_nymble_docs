package com.example.recipeengine.instruction.handler.dispense.blocks.postdispense;

import com.example.recipeengine.instruction.handler.dispense.base.BaseSequentialBlock;
import com.example.recipeengine.instruction.task.capture.base.Task;
import java.util.List;

/**
 * This class contains tasks to be run after Dispense.
 */
public class PostDispenseBlock extends BaseSequentialBlock {
  private List<? extends Task> sequentialTasks;
  private String blockStatus;

  /**
   * Constructor of PostDispenseBlock.
   *
   * @param sequentialTasks List of Tasks
   */
  public PostDispenseBlock(
      List<? extends Task> sequentialTasks) {
    super(sequentialTasks);
    this.sequentialTasks = sequentialTasks;
    this.blockStatus = "Post Dispense Block";
  }

  @Override
  public String getBlockStatus() {
    return blockStatus;
  }


  @Override
  public List<? extends Task> getSequentialTasks() {
    return sequentialTasks;
  }
}
