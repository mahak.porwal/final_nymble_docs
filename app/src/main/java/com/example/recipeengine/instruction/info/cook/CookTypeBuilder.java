package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.instruction.cook.CookType;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

import javax.inject.Inject;

public class CookTypeBuilder {
  @Inject
  public CookTypeBuilder() {
  }

  public CookType provideCookType(String filename) {
    Object object = null;
    JSONObject jsonObject = null;
    try {
      object = new JSONParser().parse(new FileReader(filename));
      jsonObject = (JSONObject) object;
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    CookType cookType = null;
    try {
      cookType = createCookType(jsonObject);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return cookType;
  }
  public CookType provideCookType(JSONObject jsonObject) {
    CookType cookType = null;
    try {
      cookType = createCookType(jsonObject);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return cookType;
  }

  private CookType createCookType(JSONObject jsonObject) throws IOException, ParseException {
    String cookType = ((String) jsonObject.get("cookType"));
    return new CookType(cookType);
  }
}
