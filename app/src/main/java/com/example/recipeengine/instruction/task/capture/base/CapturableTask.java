package com.example.recipeengine.instruction.task.capture.base;

import com.example.recipeengine.instruction.cook.params.capture.captureconstants.CaptureSensor;
import com.example.recipeengine.instruction.task.capture.SensorParams;

public interface CapturableTask extends Task, CapturedDataProvider {
  CaptureSensor getSensor();

  SensorParams getParams();

  void resetCapturedTaskData();

}
