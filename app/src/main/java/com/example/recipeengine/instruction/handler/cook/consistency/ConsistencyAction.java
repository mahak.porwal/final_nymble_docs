package com.example.recipeengine.instruction.handler.cook.consistency;

import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.handler.cook.action.BaseActionBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import io.reactivex.annotations.NonNull;
import io.reactivex.subjects.PublishSubject;
import java.util.List;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.heat.Heat;
import som.instruction.request.heat.HeatPowerLevel;
import som.instruction.request.stir.Stir;
import som.instruction.request.stir.StirringProfile;

/**
 * Action class for consistency.
 *
 * @author Pranshu Gupta
 */
public class ConsistencyAction extends BaseActionBlock {

  public static final int SUCCESS_THRESH = 95;
  public static final int REACHING_THRESH = 70;
  private final Stir stirring;
  private boolean isCompleted;


  /**
   * Consistency action constructor.
   * Initializes heat, stir, dataLogger, UiEmitter and hwRequestHandler.
   */
  public ConsistencyAction(RequestHandler hwRequestHandler,
                           PublishSubject<UIData> uiEmitter,
                           PublishSubject<ProgressData> dataLogger) {
    super(hwRequestHandler, uiEmitter, dataLogger);
    this.isCompleted = false;
    this.stirring = new Stir(null);
  }

  /**
   * Taking actions according to inferred results.
   *
   * @param inferredResult   Results from the infer class
   * @param actionParameters Target action values from recipe
   * @throws Exception exception
   */
  @Override
  public void takeAction(InferredResult inferredResult, ActionParams actionParameters)
      throws Exception {
    if (null != inferredResult) {
      final List<Double> res = inferredResult.getResult();
      if (1 > res.size()) {
        throw new AssertionError("Assertion failed");
      }
      final double target = actionParameters.getVisualScore();
      final double current_score = res.get(0) * 100;
      final double score_target_per = current_score / target * 100;
      if (!(SUCCESS_THRESH <= score_target_per)) {
        if (REACHING_THRESH <= score_target_per) {
          this.isCompleted = false;
        this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_FIVE);
          this.stirring.setStirringProfile(StirringProfile.MEDIUM);
        } else {
          this.isCompleted = false;
        this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_SEVEN);
          this.stirring.setStirringProfile(StirringProfile.MEDIUM);
        }
      } else {
        this.isCompleted = true;
      this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_ZERO);
      }
    } else {
      throw new IllegalArgumentException("Infer result cannot be null");
    }
  }

  /**
   * Method to check if the infer execution is completed.
   *
   * @return boolean variable set from success of infer
   */
  @Override
  public Boolean isInstCompleted() {
    return this.isCompleted;
  }

  /**
   * Method to pause the execution of saute
   * Stop heating, stop stirring etc.
   *
   * @throws Exception exception
   */
  @Override
  public void pause() throws Exception {
  this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_ZERO);
  }

  /**
   * Method to calculate and set the remaining time for the executing saute.
   *
   * @return Integer remaining time in seconds
   */
  @Override
  protected Integer calcRemainingTimeSec() {
    //Calculate remaining time logic to be added here.
    return 0;
  }
}
