package com.example.recipeengine.instruction.handler.cook.base;

import com.example.recipeengine.instruction.blocks.Block;
import com.example.recipeengine.instruction.blocks.InstructionHandler;
import com.example.recipeengine.instruction.blocks.capture.CaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.Cook;
import com.example.recipeengine.instruction.handler.cook.action.ActionBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;
import com.example.recipeengine.instruction.task.capture.base.CapturedDataProvider;
import java.util.List;
import som.hardware.message.action.ActionRequestParameters.ActuatorDirection;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.actuator.Actuator;
import som.instruction.request.actuator.Actuators;


/**
 * This abstract class implements {@link InstructionHandler} and
 * overrides its methods. This class contains the logic for setting
 * up the stirring in {@link #setUpStirring(Cook)} and handling
 * the execution in {@link #handle()}.
 */
public abstract class BaseCookingHandler implements InstructionHandler {

  protected BaseInstStatus instructionStatus;
  private Cook cook;
  private CaptureBlock captureBlock;
  private InferBlock inferBlock;
  private ActionBlock actionBlock;
  private volatile boolean isCompleted;
  private Thread stirrerThread;
  private Block currentBlock;

  /**
   * Constructor for BaseCookingHandler.
   *
   * @param cook         Cook object
   * @param captureBlock CaptureBlock object
   * @param inferBlock   InferBlock object
   * @param actionBlock  ActionBlock object
   */
  public BaseCookingHandler(Cook cook,
                            CaptureBlock captureBlock,
                            InferBlock inferBlock,
                            ActionBlock actionBlock) {
    this.cook = cook;
    this.captureBlock = captureBlock;
    this.inferBlock = inferBlock;
    this.actionBlock = actionBlock;
  }

  /**
   * This method is responsible for starting the stirring action.
   * 1. First it creates a new thread using lambda
   * 2. Set the thread name to Stirring Action so we can track later
   * 3. New stir request is made and the required parameters are set.
   * 4. The stirring action needs to happen in a loop that's why we
   * use isCompleted boolean
   * 5. Run Stir request at given speed for given percentage of time.
   * 6. Remaining percentage of time set the Stir speed to 0.
   * 7. The above logic runs until isCompleted is set to true.
   */
  protected void setUpStirring(Cook cook) {
    RequestHandler requestHandler = getNewRequestHandlerInstance();
    stirrerThread = new Thread(() -> {
      Thread.currentThread().setName("Stirring Action");
      System.out.println(Thread.currentThread().getName());
      Actuator actuator = new Actuator(null);
      actuator.setActuator(Actuators.STIRRER);
      actuator.setActuatorDirection(ActuatorDirection.DIRECTION_ANTICLOCKWISE);
      int count = 0;
      while (!isCompleted) {
        if (count++ == 2) {
          count = 1;
          toggleActuatorDirection(actuator);
        }
        actuator.setActuatorSpeed(cook.getStirringSpeed());
        try {
          requestHandler.handleRequest(actuator);
          Thread.currentThread().sleep(
              cook.getCycleDurationSeconds() * 10
                  * cook.getStirringPercent());
        } catch (Exception e) {
          e.printStackTrace();
        }
        actuator.setActuatorSpeed(0);
        try {
          requestHandler.handleRequest(actuator);
          Thread.currentThread().sleep(cook.getCycleDurationSeconds() * 10
              * (100 - cook.getStirringPercent()));
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      if (isCompleted) {
        try {
          actuator.setActuatorSpeed(0);
          requestHandler.handleRequest(actuator);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
    stirrerThread.start();
  }

  private void toggleActuatorDirection(Actuator actuator) {

    if (actuator.getActuatorDirection()
        .equals(ActuatorDirection.DIRECTION_CLOCKWISE.getParameter())) {
      actuator.setActuatorDirection(ActuatorDirection.DIRECTION_ANTICLOCKWISE);
    } else {
      actuator.setActuatorDirection(ActuatorDirection.DIRECTION_CLOCKWISE);
    }
  }

  public RequestHandler getNewRequestHandlerInstance() {
    return new RequestHandler();
  }

  /**
   * This method is responsible to run Capture, Infer and Action blocks
   * sequentially along with Stirring action.
   * 1. Call setUpStirring() method.
   * 2. In a do while loop CapturedData is received by calling
   * CaptureBlock's startCapture() method. Also the currentBlock
   * member variable is updated for each block.
   * 3. Then the CapturedData is passed as an argument to InferBlock's
   * infer() method.
   * 4. Then the Action Block's takeAction is called with arguments
   * InferredResult and ActionParams.
   * 5. Check if the isCompleted is set to true.
   */
  @Override
  public void handle() throws Exception {
    setUpStirring(cook);
    do {
      currentBlock = captureBlock;
      List<CapturedTaskData> capturedTaskData = captureBlock.getCapturedData();
      currentBlock = inferBlock;
      if (capturedTaskData.size() == 0) {
        capturedTaskData.add(null);
      }
      long startTime = System.currentTimeMillis();
      //Log.d("INFER_IMAGE", "Start time of Infer Block " + String.valueOf(startTime));
      InferredResult inferredResult = inferBlock.infer(capturedTaskData);
      long endTime = System.currentTimeMillis();
      long totalTime = endTime - startTime;
      //Log.d("INFER_IMAGE","Total Time for Infer Block" + String.valueOf(totalTime));
      currentBlock = actionBlock;
      actionBlock.takeAction(inferredResult, cook);
      //runInductionPower();
//      Thread.sleep(60000);
//      setIsCompleted(true);
      if (actionBlock.isInstCompleted()) {
        setIsCompleted(true);
        Actuator actuator = new Actuator(null);
        actuator.setActuator(Actuators.INDUCTION_POWER);
        RequestHandler requestHandler = new RequestHandler();
        actuator.setInductionPowerLevel(Actuator.createHeatLevel(0));
        try {
          requestHandler.handleRequest(actuator);
        } catch (Exception e) {
          e.printStackTrace();
        }
        actuator.setActuator(Actuators.STIRRER);
        actuator.setActuatorSpeed(0);
        try {
          requestHandler.handleRequest(actuator);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } while (!isCompleted);
    this.captureBlock.close();
    this.inferBlock.close();
    this.actionBlock.close();
  }

  //Test method only
  private void runInductionPower() throws Exception {
    Actuator actuator = new Actuator(null);
    actuator.setActuator(Actuators.INDUCTION_POWER);
    RequestHandler requestHandler = getNewRequestHandlerInstance();
    Integer[] heatLevels = new Integer[] {
        200, 400, 600, 800, 1000, 1200, 1400, 1200, 1000, 800, 600, 400, 200
    };
    for (Integer heatLevel : heatLevels) {
      actuator.setInductionPowerLevel(Actuator.createHeatLevel(heatLevel));
      requestHandler.handleRequest(actuator);
      Thread.sleep(10000);
    }
    //actuator.setInductionPowerLevel(Actuator.createHeatLevel(0));
    //requestHandler.handleRequest(actuator);
  }

  @Override
  public abstract BaseInstStatus getStatus();

  /**
   * This method is responsible for stopping the execution.
   * 1. First we check if the threads and blocks are not null.
   * 2. Then we stop the stirrerThread.
   * 3. Finally we run currentBlock's pause() method.
   */
  @Override
  public void pause() throws Exception {
    if (stirrerThread != null && currentBlock != null) {
      stirrerThread.interrupt();
      currentBlock.pause();
    }
  }

  @Override
  public List<CapturedDataProvider> getExecutedTasks() {
    return null;
  }

  public Block getCurrentBlock() {
    return currentBlock;
  }

  public void setCurrentBlock(Block currentBlock) {
    this.currentBlock = currentBlock;
  }


  public boolean getIsCompleted() {
    return isCompleted;
  }

  public void setIsCompleted(boolean isCompleted) {
    this.isCompleted = isCompleted;
  }
}
