package com.example.recipeengine.instruction.handler.cook.wattage;

import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.handler.cook.infer.BaseInferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;

import java.io.IOException;

import io.reactivex.subjects.PublishSubject;
import java.util.List;

public class WattageInfer extends BaseInferBlock {
  /**
   * Constructor for BaseInferBlock.
   *
   * @param recipeContext information about recipe
   * @param inferParams   {@link InferParams} object
   * @param uiEmitter     RxJava subject to send results to UI
   * @param dataLogger    RxJava subject to log results
   */
  public WattageInfer(RecipeContext recipeContext,
                      PublishSubject<UIData> uiEmitter, PublishSubject<ProgressData> dataLogger) {
    super(recipeContext, null, uiEmitter, dataLogger);
  }

  @Override
  public void pause() {

  }

  @Override
  public InferredResult infer(List<CapturedTaskData> capturedTaskData) throws Exception {
    return null;
  }

  @Override
  public void close() throws IOException {

  }
}
