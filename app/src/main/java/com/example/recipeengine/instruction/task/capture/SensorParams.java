package com.example.recipeengine.instruction.task.capture;

import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;

/**
 * This class contains all the required params related to sensor.
 */
public class SensorParams {
  private VisualCaptureType visualCaptureType;
  private ThermalType thermalType;
  private Integer numOfSamples;
  private Integer timeBetweenCaptureMS;
  private TriggerType triggerEvent;
  private ImageValidationType validationType;
  private Double validationLowerScore;
  private Double validationHigherScore;

  /**
   * Constructor for SensorParams class.
   *
   * @param visualCaptureType     type of image to be captured
   * @param thermalType           form of the image
   * @param numOfSamples          number of samples required
   * @param timeBetweenCaptureMS  time gap in between the samples
   * @param triggerEvent          wait for an event to occur
   * @param validationType        validation type of the image
   * @param validationLowerScore  lower bound score
   * @param validationHigherScore upper bound score
   */
  public SensorParams(
      VisualCaptureType visualCaptureType,
      ThermalType thermalType, Integer numOfSamples, Integer timeBetweenCaptureMS,
      TriggerType triggerEvent,
      ImageValidationType validationType, Double validationLowerScore,
      Double validationHigherScore) {
    this.visualCaptureType = visualCaptureType;
    this.thermalType = thermalType;
    this.numOfSamples = numOfSamples;
    this.timeBetweenCaptureMS = timeBetweenCaptureMS;
    this.triggerEvent = triggerEvent;
    this.validationType = validationType;
    this.validationLowerScore = validationLowerScore;
    this.validationHigherScore = validationHigherScore;
  }

  public VisualCaptureType getVisualCaptureType() {
    return visualCaptureType;
  }

  public ThermalType getThermalType() {
    return thermalType;
  }

  public Integer getNumOfSamples() {
    return numOfSamples;
  }

  public Integer getTimeBetweenCaptureMS() {
    return timeBetweenCaptureMS;
  }

  public TriggerType getTriggerEvent() {
    return triggerEvent;
  }

  public void setTriggerEvent(
      TriggerType triggerEvent) {
    this.triggerEvent = triggerEvent;
  }

  public ImageValidationType getValidationType() {
    return validationType;
  }

  public Double getValidationLowerScore() {
    return validationLowerScore;
  }

  public Double getValidationHigherScore() {
    return validationHigherScore;
  }
}
