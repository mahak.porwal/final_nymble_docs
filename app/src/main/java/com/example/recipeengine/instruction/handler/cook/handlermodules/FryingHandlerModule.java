package com.example.recipeengine.instruction.handler.cook.handlermodules;

import com.example.recipeengine.instruction.blocks.capture.BaseCaptureBlock;
import com.example.recipeengine.instruction.blocks.capture.CaptureBlock;
import com.example.recipeengine.instruction.cook.Cook;


import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.handler.cook.action.ActionBlock;

import com.example.recipeengine.instruction.handler.cook.factory.ActionBlockFactory;
import com.example.recipeengine.instruction.handler.cook.factory.CaptureBlockFactory;
import com.example.recipeengine.instruction.handler.cook.factory.InferBlockFactory;
import com.example.recipeengine.instruction.handler.cook.infer.InferBlock;

import com.example.recipeengine.instruction.task.capture.base.CapturableTask;

import dagger.Module;
import dagger.Provides;

import java.util.List;

import static com.example.recipeengine.instruction.cook.CookType.FRYING;


@Module
public class FryingHandlerModule {

  Cook cook;

  public FryingHandlerModule(Cook cook) {
    this.cook = cook;
  }

  @Provides
  public Cook provideCookObject() {
    return this.cook;
  }

  @Provides
  public CaptureBlock provideCaptureBlock() {
    return new BaseCaptureBlock(new CaptureParams() {
      @Override
      public List<CapturableTask> getCaptureTasks() {
        return cook.getCaptureTasks();
      }

      @Override
      public List<String> getSourceTasks() {
        return cook.getSourceTasks();
      }
    });
  }

  @Provides
  public InferBlock provideInferBlock() {
    return InferBlockFactory.getInferBlockObject(FRYING, this.cook);
  }

  @Provides
  public ActionBlock provideActionBlock() {
    return ActionBlockFactory.getActionBlockObject(FRYING);
  }
}
