package com.example.recipeengine.instruction.cookmodules;

import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ImageValidationType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.ThermalType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.TriggerType;
import com.example.recipeengine.instruction.cook.params.capture.captureconstants.VisualCaptureType;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.task.capture.SensorParams;
import com.example.recipeengine.instruction.task.capture.base.CapturableTask;
import com.example.recipeengine.instruction.task.capture.vision.VisionCapture;
import com.example.recipeengine.instruction.task.capture.weight.WeightCapture;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * This class provides helper methods for {@link CookParamsModuleHelper}.
 */
public class CookParamsModuleHelper {

  static String filename = CookFactory.filename;

  /**
   * This method helps in creating {@link VisionCapture} object.
   *
   * @param captureTaskMap Map object of {@link CapturableTask}
   * @return {@link VisionCapture} object
   */
  public static CapturableTask resolveVisionCaptureTask(Map captureTaskMap) {
    SensorParams sensorParams = resolveSensorParams(captureTaskMap);

    return new VisionCapture(
        (String) captureTaskMap.getOrDefault("taskName", null), sensorParams);
  }


  /**
   * This method helps in creating {@link WeightCapture} object.
   *
   * @param captureTaskMap Map object of {@link CapturableTask}
   * @return {@link WeightCapture} object.
   */
  public static CapturableTask resolveWeightCaptureTask(Map captureTaskMap) {
    SensorParams sensorParams = resolveSensorParams(captureTaskMap);
    return new WeightCapture(
        (String) captureTaskMap.getOrDefault("taskName", null), sensorParams
    );
  }

  private static SensorParams resolveSensorParams(Map captureTaskMap) {
    Map sensorParamsMap = (Map) captureTaskMap.get("sensorParameters");
    SensorParams sensorParams = new SensorParams(
        resolveVisualCaptureType((String) sensorParamsMap.getOrDefault("visualCaptureType", null)),
        resolveThermalType((String) sensorParamsMap.getOrDefault("thermalType", null)),
        Integer.valueOf((String) sensorParamsMap.getOrDefault("numOfSamples", "0")),
        Integer.valueOf((String) sensorParamsMap.getOrDefault("timeBetweenCaptureMS", "0")),
        resolveTriggerEvent((String) sensorParamsMap.getOrDefault("triggerEvent", null)),
        resolveImageValidationType((String) sensorParamsMap.getOrDefault("validationType", null)),
        Double.valueOf((String) sensorParamsMap.getOrDefault("validationLowerScore", "0.0")),
        Double.valueOf((String) sensorParamsMap.getOrDefault("validationHigherScore", "0.0")));

    return sensorParams;
  }

  private static ImageValidationType resolveImageValidationType(String validationType) {
    if (validationType != null) {
      if (validationType.equalsIgnoreCase("ExposureValidation")) {
        return new ImageValidationType(ImageValidationType.EXPOSURE_VALIDATION);
      }
    }
    return null;
  }

  private static TriggerType resolveTriggerEvent(String triggerEvent) {
    if (triggerEvent != null) {
      if (triggerEvent.equalsIgnoreCase("StirrerStarts")) {
        return new TriggerType(TriggerType.STIRRER_STARTS);
      } else if (triggerEvent.equalsIgnoreCase("StirrerStops")) {
        return new TriggerType(TriggerType.STIRRER_STOPS);
      }
    }
    return null;
  }

  private static ThermalType resolveThermalType(String thermalType) {
    if (thermalType != null) {
      if (thermalType.equalsIgnoreCase("Matrix")) {
        return new ThermalType(ThermalType.MATRIX);
      } else if (thermalType.equalsIgnoreCase("Image")) {
        return new ThermalType(ThermalType.IMAGE);
      }
    }
    return null;
  }


  private static VisualCaptureType resolveVisualCaptureType(String visualCaptureType) {
    if (visualCaptureType != null) {
      if (visualCaptureType.equalsIgnoreCase("VisionCamera")) {
        return new VisualCaptureType(VisualCaptureType.VISION_CAMERA);
      } else if (visualCaptureType.equalsIgnoreCase("ThermalCamera")) {
        return new VisualCaptureType(VisualCaptureType.THERMAL_CAMERA);
      } else if (visualCaptureType.equalsIgnoreCase("SyncThermalVisual")) {
        return new VisualCaptureType(VisualCaptureType.SYNC_THERMAL_VISUAL);
      }
    }
    return null;
  }

  /**
   * Reads JSON values for {@link com.example.recipeengine.instruction.BaseInstruction}
   * class.
   *
   * @return Object[] list of BaseInstruction values
   * @throws IOException    IOException
   * @throws ParseException ParseException
   */
  public static Integer[] getBaseInstructionObjects(String filename) throws IOException, ParseException {
    Object object = new JSONParser().parse(new FileReader(filename));
    JSONObject jsonObject = (JSONObject) object;
    Integer[] objects = new Integer[2];
    objects[0] = ((Long) jsonObject.get("instructionId")).intValue();
    objects[1] = ((Long) jsonObject.get("attemptNum")).intValue();
    return objects;
  }
}
