package com.example.recipeengine.instruction.handler.cook.temperature;

import static com.example.recipeengine.instruction.handler.cook.segmentation.FoodSegmentation.OUT_ACTUAL_SIZE;
import static com.example.recipeengine.util.ImageUtil._cropImage;
import static com.example.recipeengine.util.ImageUtil._parseBitmap;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.handler.cook.infer.BaseInferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.segmentation.FoodSegmentation;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.resources.JuliaStateDemo;
import com.example.recipeengine.util.FileUtil;
import com.example.recipeengine.util.ImageUtil;
import com.example.recipeengine.util.Native.NativeUtil;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import som.instruction.request.sensor.Sensors;
import timber.log.Timber;

public class TemperatureInfer extends BaseInferBlock {

  public static final int SEGMENTED_MASK_WIDTH = 480;
  public static final int SEGMENTED_MASK_HEIGHT = 480;
  public static final int BORDER_LEFT = 80;
  public static final int BORDER_RIGHT = 80;
  public static final int BORDER_RED_VALUE = 255;
  public static final int BORDER_GREEN_VALUE = 255;
  public static final int BORDER_BLUE_VALUE = 255;
  public static final int OVERLAY_WIDTH = 400;
  public static final int OVERLAY_HEIGHT = 400;
  public static final double TRANSFORMED_IMAGE_WEIGHT = 0.7;
  public static final double THERMAL_IMAGE_WEIGHT = 0.3;
  private final FoodSegmentation foodSegmentation;
  private boolean isModelLoaded;
  private AtomicInteger synchronous_image_number;
  private PublishSubject<Bitmap> overlayEmitter;
  private PublishSubject<Bitmap> overlayThermalMaskEmitter;
  private AtomicInteger synchronous_image_number_thermal;
  private long timeLogger;
  /**
   * Constructor for BaseInferBlock.
   *
   * @param recipeContext information about recipe
   * @param inferParams   {@link InferParams} object
   * @param uiEmitter     RxJava subject to send results to UI
   * @param dataLogger    RxJava subject to log results
   */
  @SuppressLint("CheckResult")
  public TemperatureInfer(InferParams inferParams, RecipeContext recipeContext,
                          PublishSubject<UIData> uiEmitter,
                          PublishSubject<ProgressData> dataLogger) {
    super(recipeContext, inferParams, uiEmitter, dataLogger);
    this.foodSegmentation = new FoodSegmentation(inferParams.getModelFileName());
    //creating a publish subject
    overlayEmitter = PublishSubject.create();
    overlayThermalMaskEmitter = PublishSubject.create();
    //subscribing to a publish subject on a different thread
    //observeOn enables us to run contents of the subscription on a thread from the
    //thread pool of Schedulers.io
    //the second argument to subscribe enables us to handle any error that may occur
    //while emitting
    overlayEmitter.observeOn(Schedulers.io()).subscribe(this::saveTransformedMask, Timber::e);
    overlayThermalMaskEmitter.observeOn(Schedulers.io())
        .subscribe(this::saveTransformedThermalMask, Timber::e);
    synchronous_image_number = new AtomicInteger(0);
    synchronous_image_number_thermal = new AtomicInteger(0);
  }

  /**
   * Triggers the segmentation class to load model
   *
   * @throws IOException
   */
  private void loadInferenceModel() throws IOException {
    if (!this.isModelLoaded) {
      this.foodSegmentation.loadModel();
      this.isModelLoaded = true;
    }
  }

  @Override
  public void pause() {

  }

  @Override
  public InferredResult infer(List<CapturedTaskData> capturedTaskDataList) throws Exception {
    this.timeLogger = System.currentTimeMillis();
    this.loadInferenceModel();
    Timber.d("Inference model load time %d",System.currentTimeMillis()-this.timeLogger);
    CapturedTaskData capturedTaskData = capturedTaskDataList.get(0);
    final Map<Sensors, List<List<Double>>> sensorDataMap = capturedTaskData.getSensorData();
    if (null == sensorDataMap) {
      throw new Exception("Null sensor map");
    }
    final List<List<Double>> thermalDataList = sensorDataMap.getOrDefault(Sensors.THERMAL_CAMERA,
        null);
    if (null == thermalDataList) {
      throw new Exception("No thermal camera data available in the sensor map");
    }

    final List<Double> thermalMatrix = thermalDataList.get(0);

    final List<Bitmap> visionImageList = capturedTaskData.getVisionCamera();

    if (null == visionImageList) {
      throw new Exception("Null vision image list");
    }

    final Bitmap visionCameraImage = visionImageList.get(0);

    if (null == visionCameraImage) {
      throw new Exception("Null vision image");
    }
    this.timeLogger = System.currentTimeMillis();
    final double[] temperatureMatrixArray = new double[1024];
    int matrixIndex = 0;
    for (Double pixelVal : thermalMatrix) {
      temperatureMatrixArray[matrixIndex] = pixelVal;
      matrixIndex++;
    }
    Timber.d("Temperature matrix creation time %d",
      System.currentTimeMillis()-this.timeLogger);
    this.timeLogger = System.currentTimeMillis();
    //Create thermal image
    NativeUtil.CreateThermalImage(temperatureMatrixArray);
    final Mat thermalMat = new Mat();
    NativeUtil.PopulateThermalImage(thermalMat.getNativeObjAddr());
    final Bitmap thermalImageInUse = _parseBitmap(thermalMat);
    final Bitmap visualImageInUse = _cropImage(visionCameraImage);
    Timber.d("Create thermal image time %d",
      System.currentTimeMillis()-this.timeLogger);

    //Emit thermal and vision image to the Ui
    final UIData thermalUiData = new UIData(thermalImageInUse, null);
    final UIData visualUiData = new UIData(visualImageInUse, null);
    getUiEmitter().onNext(thermalUiData);
    getUiEmitter().onNext(visualUiData);

    //Rgb and Thermal Sync data received,
    //Infer the visual data, get the segmented image mask and emit it
    final Bitmap inferMaskInUse = this.inferVisualImage(visualImageInUse);
    final UIData inferMaskUiData = new UIData(inferMaskInUse, null);
    getUiEmitter().onNext(inferMaskUiData);

    final TemperatureParams[] temperatureParams =
        this.transformMaskAndCalculateTempParams(inferMaskInUse, thermalImageInUse);
    final TemperatureParams foodTempParams = temperatureParams[0];
    final TemperatureParams panTempParams = temperatureParams[1];
    final List<Double> inferredParameters = new ArrayList<>();
    inferredParameters.add(foodTempParams.getTemperature().doubleValue());
    inferredParameters.add(foodTempParams.getStandardDeviation());
    inferredParameters.add(panTempParams.getTemperature().doubleValue());
    inferredParameters.add(panTempParams.getStandardDeviation());

    return new InferredResult(inferredParameters);
  }

  /**
   * @param visualImage : Visual image to be inferred
   * @return : the segmented image
   * Infer the visual image and return 3-channel Segmented image
   */
  private Bitmap inferVisualImage(Bitmap visualImage) {
    this.timeLogger = System.currentTimeMillis();
    final Mat segmentedImage = this.foodSegmentation.runSegmentation(visualImage);
    Timber.d("Segmentation time %d",
      System.currentTimeMillis()-this.timeLogger);
    Imgproc.resize(segmentedImage, segmentedImage,
        new Size(OUT_ACTUAL_SIZE, OUT_ACTUAL_SIZE), 0, 0, Imgproc.INTER_NEAREST);
    return ImageUtil.parseBitmapGrey(segmentedImage);
  }


  /**
   * Uses the segmented mask and the thermal image to calculate food and pan temperature parameters
   *
   * @param segmentedImageMask segmented mask
   * @param thermalImageInUse  thermal image
   * @return temperature params array, where the first index holds food temperature params
   */
  private TemperatureParams[] transformMaskAndCalculateTempParams(
      final Bitmap segmentedImageMask, final Bitmap thermalImageInUse) {

    // Convert the Bitmap mask into Mat type single channel,
    // Now we have the mask, pass this mask to native code to calculate the average temperature of food
    // Temperature of the food is acquired , pass this temperature to TempControlSystem


    final Mat temp = new Mat();
    Utils.bitmapToMat(segmentedImageMask, temp);
    Imgproc.resize(temp, temp, new Size(SEGMENTED_MASK_WIDTH, SEGMENTED_MASK_HEIGHT), 0, 0,
        Imgproc.INTER_NEAREST);
    org.opencv.core.Core.copyMakeBorder(temp, temp, 0, 0, BORDER_LEFT, BORDER_RIGHT,
        Core.BORDER_CONSTANT, new Scalar(BORDER_RED_VALUE, BORDER_GREEN_VALUE, BORDER_BLUE_VALUE));
    final Mat transformedImage = new Mat();
    final Mat transformationMatrix = JuliaStateDemo.getTransformationMatrix();
    Imgproc.warpPerspective(temp, transformedImage, transformationMatrix,
        new Size(OVERLAY_WIDTH, OVERLAY_HEIGHT));

    final Mat thermalImage = new Mat();
    final Mat weightedImage = new Mat();
    Utils.bitmapToMat(thermalImageInUse, thermalImage);

    org.opencv.core.Core.addWeighted(transformedImage, TRANSFORMED_IMAGE_WEIGHT, thermalImage,
        THERMAL_IMAGE_WEIGHT, 0, weightedImage);
    Imgproc.cvtColor(weightedImage, weightedImage, Imgproc.COLOR_BGRA2RGB);
    final Bitmap image = _parseBitmap(weightedImage);
    overlayThermalMaskEmitter.onNext(image);
    final UIData thermalVisualOverlay = new UIData(image, null);
    getUiEmitter().onNext(thermalVisualOverlay);

    Imgproc.resize(transformedImage, transformedImage, new Size(100, 100), 0, 0,
        Imgproc.INTER_NEAREST);
    Imgproc.cvtColor(transformedImage, transformedImage, Imgproc.COLOR_BGR2GRAY);

    final Bitmap imageMask = _parseBitmapGrey(transformedImage);
    overlayEmitter.onNext(imageMask);
    //this.saveTransformedMask(imageMask);

    this.timeLogger = System.currentTimeMillis();
    final TemperatureParams foodTempParams = NativeUtil.CalculateFoodTemperature(
        transformedImage.getNativeObjAddr());
    Timber.d("Food Temperature  calc time %d",
      System.currentTimeMillis()-this.timeLogger);
    this.timeLogger = System.currentTimeMillis();
    final TemperatureParams panTempParams = NativeUtil.CalculatePanTemperature(
        transformedImage.getNativeObjAddr());
    Timber.d("Pan Temperature calc time %d",
      System.currentTimeMillis()-this.timeLogger);
    final TemperatureParams[] temperatureParams = new TemperatureParams[2];
    temperatureParams[0] = foodTempParams;
    temperatureParams[1] = panTempParams;

    return temperatureParams;
    //this.tempController.tempControl(foodTemperature, this.standardDeviation, panTemp, panTempSD);
  }

  @Override
  public void close() throws IOException {
    this.foodSegmentation.close();
  }

  private void saveTransformedMask(final Bitmap mask) {

    final String filename =
        "Mask_image_" + this.synchronous_image_number.incrementAndGet() + ".png";
    final File imageFile = FileUtil.getMaskFile(filename);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      mask.compress(Bitmap.CompressFormat.PNG, 60, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename);
    }
  }

  private void saveTransformedThermalMask(final Bitmap mask) {

    final String filename =
        "Thermal_Mask_image_" + this.synchronous_image_number_thermal.incrementAndGet() + ".png";
    final File imageFile = FileUtil.getMaskFile(filename);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      mask.compress(Bitmap.CompressFormat.PNG, 60, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename);
    }
  }

  private Bitmap _parseBitmapGrey(final Mat mat) {
    Bitmap bmp = null;
    Mat img = new Mat();
    Imgproc.cvtColor(mat, img, Imgproc.COLOR_GRAY2RGB);
    try {
      bmp = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
      Utils.matToBitmap(img, bmp);
    } catch (CvException e) {
      Timber.e(e);
    }
    return bmp;
  }
}
