package com.example.recipeengine.instruction.info.dispense;

import com.example.recipeengine.instruction.factory.dispense.DispenseFactory;
import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;

import org.json.simple.JSONObject;

import java.util.List;

import javax.inject.Inject;

public class InferInstructionListBuilder {
  @Inject
  public InferInstructionListBuilder() {
  }

  public List<InferInstruction> provideListInferInstructions(JSONObject instructionJSON) {
    return DispenseFactory.resolveInferInstructions(instructionJSON);
  }
}
