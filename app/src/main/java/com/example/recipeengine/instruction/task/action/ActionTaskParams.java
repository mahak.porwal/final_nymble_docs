package com.example.recipeengine.instruction.task.action;

import com.example.recipeengine.instruction.cook.params.stirrer.StirrerParams;

public class ActionTaskParams {

  private final StirrerParams stirrerParams;
  private final Double actuatorSpeed;
  private final Integer actuatorFreq;
  private final Integer heatLevel;
  private final Boolean actuatorState;

  public ActionTaskParams(
      StirrerParams stirrerParams, Double actuatorSpeed, Integer actuatorFreq,
      Integer heatLevel, Boolean actuatorState) {
    this.stirrerParams = stirrerParams;
    this.actuatorSpeed = actuatorSpeed != null ? Math.max(0, Math.min(actuatorSpeed, 100)) : null;
    this.actuatorFreq = actuatorFreq;
    this.heatLevel = heatLevel;
    this.actuatorState = actuatorState;
  }

  public StirrerParams getStirrerParams() {
    return this.stirrerParams;
  }

  public Double getActuatorSpeed() {
    return this.actuatorSpeed;
  }

  public Integer getActuatorFreq() {
    return this.actuatorFreq;
  }

  public Integer getHeatLevel() {
    return this.heatLevel;
  }

  public Boolean getActuatorState() {
    return this.actuatorState;
  }
}
