package com.example.recipeengine.instruction.rectifier;

import com.example.recipeengine.instruction.handler.cook.status.BaseInstStatus;

import javax.inject.Inject;

public class RectifierFactory {
  @Inject
  public RectifierFactory() {
  }

  public InstructionRectifier createInstructionRectifier() {
    return new InstructionRectifier() {
      @Override
      public void rectify() {

      }

      @Override
      public BaseInstStatus getStatus() {
        return null;
      }

      @Override
      public void pause() throws Exception {

      }
    };
  }
}
