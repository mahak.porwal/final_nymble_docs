package com.example.recipeengine.instruction.cook.params.infer;

/**
 * Infer model file name parameters.
 */
public interface InferParams {

  String getModelId();

  String getModelFileName();

}
