package com.example.recipeengine.instruction.task.action;

import som.hardware.request.handler.RequestHandler;
import som.hardware.response.HWActionResponse;

import som.instruction.request.actuator.Actuator;
import som.instruction.request.actuator.Actuators;
import som.instruction.request.actuator.Actuators.AnnotationActuator;
import som.instruction.request.heat.HeatPowerLevel;

public class ActionTaskThread extends Thread {

  private final ActionTaskParams actionTaskParams;
  private final String actuator;
  private RequestHandler requestHandler;

  ActionTaskThread(String name, @AnnotationActuator String actuator, ActionTaskParams actionTaskParams) {
    super(name);
    this.actionTaskParams = actionTaskParams;
    this.actuator = actuator;
  }

  @Override
  public void run() {
    this.requestHandler = getRequestHandlerInstance();

    final Actuator actuator = new Actuator(null);
    actuator.setActuator(this.actuator);
    if (null != this.actionTaskParams.getHeatLevel()) {
      final HeatPowerLevel powerLevel = Actuator.createHeatLevel(this.actionTaskParams.getHeatLevel());
      actuator.setInductionPowerLevel(powerLevel);
    } else if (null != this.actionTaskParams.getActuatorState()) {
      actuator.setActuatorState(this.actionTaskParams.getActuatorState());
    } else {
      actuator.setActuatorSpeed(this.actionTaskParams.getActuatorSpeed().intValue());
    }

    final HWActionResponse hwActionResponse;
    try {
      hwActionResponse = (HWActionResponse) this.requestHandler.handleRequest(actuator);
      if (this.actuator.equalsIgnoreCase(Actuators.EXHAUST_FAN)) {
        if (null != this.actionTaskParams.getActuatorState()) {
          if (!this.actionTaskParams.getActuatorState()) {
            Thread.sleep(4000);
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public RequestHandler getRequestHandlerInstance() {
    return new RequestHandler();
  }
}
