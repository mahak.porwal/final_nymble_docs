package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.instruction.cook.params.infer.InferParameters;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Map;

import javax.inject.Inject;

public class InferInfoBuilder {
  @Inject
  public InferInfoBuilder() {
  }

  public InferParams provideInferParameters(JSONObject jsonObject) {
    InferParams inferParams = null;
    try {
      inferParams = createInferParamsObject(jsonObject);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return inferParams;
  }

  private InferParams createInferParamsObject(JSONObject jsonObject) throws IOException, ParseException {
    Map address = ((Map) jsonObject.get("inferParams"));

    return new InferParameters((String) address.get("modelId"),
      (String) address.get("modelFileName"));
  }
}
