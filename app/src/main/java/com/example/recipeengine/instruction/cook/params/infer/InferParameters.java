package com.example.recipeengine.instruction.cook.params.infer;


/**
 * This class contains model file parameters for saute instructions.
 */
public class InferParameters implements InferParams {

  private final String modelId;
  private final String modelFileName;

  /**
   * Constructor for Infer parameters.
   *
   * @param modelId       id of the model being used
   * @param modelFileName filename of the model that the
   *                      infer block will use to load the model and infer
   */
  public InferParameters(String modelId, String modelFileName) {
    this.modelId = modelId;
    this.modelFileName = modelFileName;
  }


  @Override
  public String getModelId() {
    return this.modelId;
  }

  @Override
  public String getModelFileName() {
    return this.modelFileName;
  }
}
