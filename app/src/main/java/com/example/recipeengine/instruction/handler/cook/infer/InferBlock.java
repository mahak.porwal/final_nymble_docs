package com.example.recipeengine.instruction.handler.cook.infer;

import com.example.recipeengine.instruction.blocks.Block;
import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import java.util.List;

public interface InferBlock extends Block {
  InferredResult infer(List<CapturedTaskData> capturedTaskData) throws Exception;
}
