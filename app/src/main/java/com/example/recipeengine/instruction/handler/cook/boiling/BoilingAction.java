package com.example.recipeengine.instruction.handler.cook.boiling;

import androidx.annotation.VisibleForTesting;
import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.handler.cook.action.BaseActionBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import io.reactivex.annotations.NonNull;
import io.reactivex.subjects.PublishSubject;
import java.util.List;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.heat.Heat;
import som.instruction.request.heat.HeatPowerLevel;


/**
 * Action class for boiling.
 *
 * @author Pranshu Gupta
 */
public class BoilingAction extends BaseActionBlock {


  private boolean isCompleted;

  /**
   * Constructor of action blocks.
   *
   * @param requestHandler For any hardware requests
   * @param uiEmitter      For any ui updates
   * @param dataLogger     For any data logging
   */
  public BoilingAction(RequestHandler requestHandler,
                       PublishSubject<UIData> uiEmitter,
                       PublishSubject<ProgressData> dataLogger) {
    super(requestHandler, uiEmitter, dataLogger);
    this.isCompleted = false;

  }

  /**
   * Taking actions according to inferred results.
   *
   * @param inferredResult   Results from the infer class
   * @param actionParameters Target action values from recipe
   * @throws Exception exception
   */
  @Override
  public void takeAction(InferredResult inferredResult, ActionParams actionParameters)
      throws Exception {
    if (null != inferredResult) {
      final List<Double> res = inferredResult.getResult();
      if (1 > res.size()) {
        throw new AssertionError();
      }
      final double current_score = res.get(0);

      updateScoreStatus(current_score);    //Updating the scoreStatus string to give to user
      updateActionStatus();             //Updating the actionStatus string for debugging

      if (1.0 <= current_score) {  // Success boiling
        this.isCompleted = true;
      this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_ZERO);
      } else {
      this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_SEVEN);
      }
    } else {
      throw new IllegalArgumentException("Infer result cannot be null");
    }
  }


  /**
   * Method to check if the infer execution is completed.
   *
   * @return boolean variable set from success of infer
   */
  @Override
  public Boolean isInstCompleted() {
    return this.isCompleted;
  }

  /**
   * This method updates the score status string for giving updates to User about the current saute.
   *
   * @param score current reached score percentage.
   */
  @VisibleForTesting
  void updateScoreStatus(double score) {
    this.scoreStatus = "Julia is trying to reach the target boiling by "
        + "constantly heating and stirring. "
        + "Currently, it has achieved "
        + score
        + " of the target and will reach the target "
        + "in approximately " + calcRemainingTimeSec() + " seconds";
  }

  @VisibleForTesting
  void updateActionStatus() {
    this.actionStatus = "Boiling saute in progress";
  }


  /**
   * Method to pause the execution of saute
   * Stop heating, stop stirring etc.
   *
   * @throws Exception exception
   */
  @Override
  public void pause() throws Exception {
  this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_ZERO);
  }


  /**
   * Method to calculate and set the remaining time for the executing saute.
   *
   * @return Integer remaining time sec
   */
  @Override
  protected Integer calcRemainingTimeSec() {
    return 0;
  }
}
