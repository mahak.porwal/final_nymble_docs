package com.example.recipeengine.instruction.info.cook;

import com.example.recipeengine.instruction.cook.CookType;
import com.example.recipeengine.instruction.cook.params.capture.CaptureParams;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.info.InstructionInfoBuilder;
import com.example.recipeengine.util.JSONObjectUtil;

import org.json.simple.JSONObject;

import java.util.Map;

import javax.inject.Inject;

public class CookInfoFactory {

  private CaptureInfoBuilder captureInfoBuilder;
  private InferInfoBuilder inferInfoBuilder;
  private ActionInfoBuilder actionInfoBuilder;
  private StirrerInfoBuilder stirrerInfoBuilder;
  private ParametricFieldsBuilder parametricFieldsBuilder;
  private CookTypeBuilder cookTypeBuilder;

  @Inject
  public CookInfoFactory(CookTypeBuilder cookTypeBuilder, CaptureInfoBuilder captureInfoBuilder, InferInfoBuilder inferInfoBuilder,
                         ActionInfoBuilder actionInfoBuilder, StirrerInfoBuilder stirrerInfoBuilder,
                         ParametricFieldsBuilder parametricFieldsBuilder) {
    this.captureInfoBuilder = captureInfoBuilder;
    this.inferInfoBuilder = inferInfoBuilder;
    this.actionInfoBuilder = actionInfoBuilder;
    this.stirrerInfoBuilder = stirrerInfoBuilder;
    this.parametricFieldsBuilder = parametricFieldsBuilder;
    this.cookTypeBuilder = cookTypeBuilder;
  }

  /**
   * Creates {@link CookInfo} object from a json present in the filename path
   *
   * @param filename path to the instruction json
   * @return cook info object
   */
  public CookInfo createCookInfo(String filename) {
    JSONObject jsonObject = JSONObjectUtil.createJSONObject(filename);
    return this.createCookInfo(jsonObject);
  }

  /**
   * Creates {@link CookInfo} object from jsonObject of a cook instruction
   *
   * @param jsonObject {@link JSONObject} of a cook instruction
   * @return
   */
  public CookInfo createCookInfo(JSONObject jsonObject) {

    final CookType cookType = this.cookTypeBuilder.provideCookType(jsonObject);
    final StirrerInfo stirrerInfo = this.stirrerInfoBuilder.provideStirrerProfile(jsonObject);
    final CaptureParams captureParams = this.captureInfoBuilder.provideCaptureParameters(jsonObject);
    final InferParams inferParams = this.inferInfoBuilder.provideInferParameters(jsonObject);
    final ActionInfo actionInfo = this.actionInfoBuilder.provideActionInformation(jsonObject);
    final Map<String, String> parametricFields = this.parametricFieldsBuilder.provideParametricFields(jsonObject);
    Integer instructionId = InstructionInfoBuilder.buildInstructionId(jsonObject);
    Integer attemptNum = InstructionInfoBuilder.buildAttemptNum(jsonObject);
    return new BaseCookInfo(cookType, actionInfo, stirrerInfo, captureParams,
      inferParams, parametricFields, instructionId, attemptNum);
  }
}
