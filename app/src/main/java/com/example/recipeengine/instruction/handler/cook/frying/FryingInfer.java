package com.example.recipeengine.instruction.handler.cook.frying;

import static com.example.recipeengine.util.ImageUtil.centerCrop;
import static org.opencv.core.CvType.CV_32FC3;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

import com.example.recipeengine.instruction.blocks.capture.CapturedTaskData;
import com.example.recipeengine.instruction.cook.params.infer.InferParams;
import com.example.recipeengine.instruction.decorator.exchanges.OutputParams;
import com.example.recipeengine.instruction.handler.cook.infer.BaseInferBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import com.example.recipeengine.util.FileUtil;

import io.reactivex.subjects.PublishSubject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.pytorch.IValue;
import org.pytorch.Module;
import org.pytorch.Tensor;
import org.pytorch.torchvision.TensorImageUtils;
import timber.log.Timber;

/**
 * Frying image classifier.
 *
 * @author Pranshu Gupta
 */
public class FryingInfer extends BaseInferBlock {


  public static final int WHITE_PIXELS_UPPER_THERSH = 245;
  public static final double MAX_FRAC_PERCENTAGE = 5.0;
  public static final int MODEL_INPUT_IMAGE_SIZE = 224;
  private static final int NO_OF_IMAGES_REQUIRED = 2;
  private static final Point INPUT_IMAGE_CROP_ORIGIN = new Point(420, 0);
  private static final int INPUT_IMAGE_CROP_SIZE = 1080;
  private final InferParams inferParameters;
  private Module model;
  private int synchronous_number = 1;

  /**
   * BaseInfer class constructor. Parent class for all vision based sautes.
   *
   * @param recipeContext Contains recipe specific information.
   * @param uiEmitter     Publisher for UI
   * @param dataLogger    Publisher for data logging
   */
  public FryingInfer(InferParams inferParameters,
                     RecipeContext recipeContext,
                     PublishSubject<UIData> uiEmitter,
                     PublishSubject<ProgressData> dataLogger) {
    super(recipeContext, inferParameters, uiEmitter, dataLogger);
    this.inferParameters = inferParameters;
    try {
      loadModel();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  /**
   * Method to load model file in memory. Uses infer parameters.
   *
   * @throws Exception exception
   */
  public void loadModel() throws Exception {
    final String modelData = fetchModelFile(this.inferParameters.getModelFileName());
    if (null != modelData) {
      this.model = Module.load(modelData);
    }
    if (null == this.model) {
      throw new Exception("Model not loaded");
    }
  }

  /**
   * Infer method for running model for frying task
   * Depends on modelID, Works different for onion browning and for different ingredients.
   *
   * @param captureData Contains all the captured data
   * @return Inferred results to be passed to action block.
   */
  @Override
  public InferredResult infer(List<CapturedTaskData> captureData) {
    CapturedTaskData capturedTaskData = captureData.get(0);
    final List<Bitmap> bmps = capturedTaskData.getVisionCamera();
    Double res;
    if (this.inferParameters.getModelId().contains("onion")) {
      res = runInferForOnionBrowning(bmps);
    } else {
      res = runInferForIngredientFrying(bmps,
        0/*ingredientId*/);   /// Need a parameter/id for which ingredient to get fry results for.
    }
    if (null != res) {
      updateInferStatus(res);
    }
    final List<Double> inferRes = new ArrayList<>();
    inferRes.add(res);
    Log.d("INFER_IMAGE","Model output : " + String.valueOf(res));
    return new InferredResult(inferRes);
  }

  /**
   * Core method for onion browning predictions.
   *
   * @param bmps captured images
   * @return Mean score from all images
   */
  private Double runInferForOnionBrowning(List<Bitmap> bmps) {
    final ProgressData progressData = new ProgressData(null, null, null);
    final UIData uiData = new UIData(null, null);

    final List<Double> result = new LinkedList<>();
    if (null != bmps && NO_OF_IMAGES_REQUIRED == bmps.size()) {
      for (Bitmap bmp : bmps) {
        final Mat image = new Mat();
        Utils.bitmapToMat(bmp, image);
//        final boolean isValid = isValidImageBrightness(image);
        if (true) {
          //infer here when image is fine
          bmp = centerCrop(bmp, INPUT_IMAGE_CROP_ORIGIN, INPUT_IMAGE_CROP_SIZE);
          final Bitmap croppedBmp = _parseBitmap(image);
          final Tensor output = this.model.forward(getImageTensor(bmp)).toTensor();
          final float[] score_arr = output.getDataAsFloatArray();
          result.add(Double.valueOf(score_arr[0]));

          final Pair<OutputParams, String> logData =
            new Pair<>(OutputParams.FRYING_SCORE, String.valueOf(result.get(0)));
          progressData.setOutputData(logData);
          getDataLogger().onNext(progressData);
          saveImage(bmp);
          uiData.setImage(bmp);
          getUiEmitter().onNext(uiData);
        }
      }
      double mean = 0.0;
      for (int i = 0; i < result.size(); i++) {
        mean += result.get(i);
      }
      mean /= result.size();
      return mean;
    } else {
      return null;
    }
  }

  /**
   * Core method to run infer on images for any ingredient frying.
   *
   * @param bmps         - List of images for running infer
   * @param ingredientId - Ingredient index
   *                     (from ingredient index table - eg-Potato=1, cauliflower = 2 , etc..)
   * @return Mean double value of frying on all images.
   */
  private Double runInferForIngredientFrying(List<Bitmap> bmps, int ingredientId) {
    final ProgressData progressData = new ProgressData(null, null, null);
    final UIData uiData = new UIData(null, null);

    final List<Double> result = new LinkedList<>();
    if (null != bmps && NO_OF_IMAGES_REQUIRED == bmps.size()) {
      for (Bitmap bmp : bmps) {
        final IValue[] out = this.model.forward(getImageTensor(bmp)).toTuple();
        final Tensor val1 = out[ingredientId - 1].toTensor();
        final float[] score_arr = val1.getDataAsFloatArray();
        result.add(Double.valueOf(score_arr[0]));
        final Pair<OutputParams, String> logData =
          new Pair<OutputParams, String>(OutputParams.FRYING_SCORE,
            String.valueOf(result.get(0)));
        uiData.setImage(bmp);
        getUiEmitter().onNext(uiData);
        progressData.setOutputData(logData);
        getDataLogger().onNext(progressData);
      }
      double mean = 0.0;
      for (int i = 0; i < result.size(); i++) {
        mean += result.get(i);
      }
      mean /= result.size();
      return mean;
    } else {
      return null;
    }
  }

  /**
   * Validating the images by calculating the %age of bright/white pixels in image.
   *
   * @param image input image
   * @return boolean for isValid image or not
   */
  private boolean isValidImageBrightness(Mat image) {
    final Mat image_bak = new Mat();
    image.convertTo(image_bak, CV_32FC3);   //Validating image brightness
    Imgproc.cvtColor(image_bak, image_bak, Imgproc.COLOR_BGR2YCrCb);
    float mask_sum = 0;
    for (int i = 0; i < image_bak.rows(); i++) {
      for (int j = 0; j < image_bak.cols(); j++) {
        final float[] data = new float[3];
        image_bak.get(i, j, data);
        if (WHITE_PIXELS_UPPER_THERSH < data[0]) {
          mask_sum += 1;
        }
      }
    }
    final float frac = (mask_sum * 100) / (image.rows() * image.cols());
    System.out.printf("Chef image white pixel fractions = %f", frac);
    if (MAX_FRAC_PERCENTAGE < frac) {
      System.out.printf("Image too bright");
      return false;
    } else {
      return true;
    }
  }

  /**
   * Pausing or halting the current frying operation.
   */
  @Override
  public void pause() {
    this.model.destroy();
  }

  /**
   * Method to update the infer status string for debugging.
   *
   * @param mean score from infer results
   */
  private void updateInferStatus(double mean) {
    this.inferStatus =
      "Reaching target frying, current infer result is = " + mean * 100 + " percent fried";
  }

  /**
   * Creating a IValue object used for pytorch inferring of received Bitmap.
   *
   * @param bmp Input image
   * @return IValue of input image
   */
  private IValue getImageTensor(Bitmap bmp) {
    final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, 224, 224, true);
    final Tensor input = TensorImageUtils.bitmapToFloat32Tensor(
      scaledBitmap,
      TensorImageUtils.TORCHVISION_NORM_MEAN_RGB,
      TensorImageUtils.TORCHVISION_NORM_STD_RGB
    );
    return IValue.from(input);
  }


  /**
   * Convert Mat to Bitmap.
   */
  private Bitmap _parseBitmap(final Mat mat) {
    final Bitmap bmp = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
    Utils.matToBitmap(mat, bmp);
    return bmp;
  }

  /**
   * Loading the model from the internal memory.
   *
   * @param modelName model file name
   * @return file's absolute path
   */
  @VisibleForTesting
  @Nullable
  String fetchModelFile(final String modelName) {
    final File file = FileUtil.getModelFile(modelName);
    if (file.exists() && file.length() > 0) {
      return file.getAbsolutePath();
    }
    System.out.printf("File not found");
    return null;
  }


  @Override
  public void close() throws IOException {
    this.model.destroy();
  }

  private void saveImage(@NonNull Bitmap image1) {
    final String number = String.valueOf(this.synchronous_number);
    if(synchronous_number != 1) {
      return;
    }
    final String filename1 = "FryingInfer.jpg";
    final File imageFile = FileUtil.getDryWetImageFile(filename1);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image1.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename1);
    }
    this.synchronous_number += 1;
  }

}
