package com.example.recipeengine.instruction.dispense.decorator;

import androidx.annotation.VisibleForTesting;
import com.example.recipeengine.instruction.decorator.exchanges.ExecutionParams;
import com.example.recipeengine.instruction.decorator.source.InstructionLog;
import com.example.recipeengine.instruction.dispense.BaseDispense;

/**
 * This class extends {@link BaseDispenseDecorator} and applies One Time Decoration to a
 * {@link com.example.recipeengine.instruction.dispense.Dispense} Object.
 * It overrides the {@link #getQuantityInGrams()} which returns the modified
 * quantityInGrams value.
 */

public class DispensedQuantityDecorator extends BaseDispenseDecorator {

  private InstructionLog instructionLog;
  private Double currentWeight;

  /**
   * Constructor for DispensedQuantityDecorator.
   *
   * @param wrappedObject contains the dispense object here.
   * @param instructionLog log of instruction that want to
   *                       decorate the incoming dispense instruction.
   * @param currentWeight Current weight of the system
   */
  public DispensedQuantityDecorator(
      BaseDispense wrappedObject,
      InstructionLog instructionLog, Double currentWeight) {
    super(wrappedObject);
    this.instructionLog = instructionLog;
    this.currentWeight = currentWeight;
  }

  @Override
  public Integer getQuantityInGrams() {
    Integer quantity = wrappedObject.getQuantityInGrams() - deltaDispensedQuantity();

    if (quantity > wrappedObject.getQuantityInGrams()) {
      return wrappedObject.getQuantityInGrams();
    }

    return quantity <= Integer.valueOf(0) ? Integer.valueOf(0) : quantity;
  }

  /**
   * This method is responsible for calculating the difference in quantity
   * that must be added to original quantity
   * 1. Compute the difference of currentWeight - preDispenseWeight.
   * This gives us a value of how much quantity
   * has already been dispensed.
   * 2. We need to accommodate these changes thus the above value becomes the delta
   * which is returned to caller method
   *
   * @return Integer This returns change in value of quantity in grams.
   */
  private Integer deltaDispensedQuantity() {
    return (int) (currentWeight - Double.parseDouble(
        instructionLog.getExecutionParams().get(ExecutionParams.PRE_DISPENSE_WEIGHT)));
  }
}
