package com.example.recipeengine.instruction.factory.dispense;

import com.example.recipeengine.instruction.handler.dispense.blocks.infer.InferInstruction;
import com.example.recipeengine.instruction.handler.dispense.blocks.dispense.DispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.postdispense.PostDispenseBlock;
import com.example.recipeengine.instruction.handler.dispense.blocks.predispense.PreDispenseBlock;
import com.example.recipeengine.instruction.info.dispense.DispenseInfo;

import java.util.List;

public class DispenseInfoConvertor {
  private DispenseInfoConvertor() {
  }

  public static Integer provideQuantityInGrams(DispenseInfo dispenseInfo) {
    return Integer.valueOf(Double.valueOf(dispenseInfo.getQuantityInfo()).intValue());
  }

  public static PreDispenseBlock providePreDispenseBlock(DispenseInfo dispenseInfo) {
    return dispenseInfo.getPreDispenseInfo();
  }

  public static DispenseBlock provideDispenseBlock(DispenseInfo dispenseInfo) {
    return dispenseInfo.getDispenseInfo();
  }

  public static PostDispenseBlock providePostDispenseBlock(DispenseInfo dispenseInfo) {
    return dispenseInfo.getPostDispenseInfo();
  }

  public static List<InferInstruction> provideInferInstructions(DispenseInfo dispenseInfo) {
    return dispenseInfo.getInferInstructionsInfo();
  }
}
