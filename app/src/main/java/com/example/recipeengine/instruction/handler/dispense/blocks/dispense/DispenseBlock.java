package com.example.recipeengine.instruction.handler.dispense.blocks.dispense;

import com.example.recipeengine.instruction.handler.dispense.base.BaseParallelBlock;
import com.example.recipeengine.instruction.task.capture.base.Task;
import com.example.recipeengine.instruction.task.dispense.DispenseTask;

import java.util.List;

/**
 * This class contains tasks to be run during Dispense.
 */
public class DispenseBlock extends BaseParallelBlock {
  private List<? extends Task> parallelTasks;
  private DispenseTask dispenseTask;
  private DispenseHwInformation dispenseHwInformation;
  private String blockStatus;

  /**
   * Constructor of DispenseBlock.
   *
   * @param parallelTasks         List of parallel tasks.
   * @param dispenseHwInformation Information related to DispenseHW
   */
  public DispenseBlock(
    List<? extends Task> parallelTasks,
    DispenseHwInformation dispenseHwInformation) {
    super(parallelTasks);
    this.parallelTasks = parallelTasks;
    this.dispenseHwInformation = dispenseHwInformation;
    this.blockStatus = "Dispense Block";
  }

  @Override
  public String getBlockStatus() {
    return blockStatus;
  }

  public DispenseHwInformation getDispenseHwInformation() {
    return dispenseHwInformation;
  }

  @Override
  public List<? extends Task> getParallelTasks() {
    return parallelTasks;
  }

  @Override
  public void startParallelTasks() throws InterruptedException {
    this.dispenseTask = new DispenseTask(this.dispenseHwInformation);
    for (Task task : parallelTasks) {
      task.beginTask();
    }
    this.dispenseTask.beginTask();
    for (Task task : parallelTasks) {
      task.getThread().join();
    }
    this.dispenseTask.getThread().join();
  }

  @Override
  public void pause() throws Exception {
    for (Task task : parallelTasks) {
      task.getThread().interrupt();
    }
    this.dispenseTask.getThread().interrupt();
  }
}
