package com.example.recipeengine.instruction.info.cook;


import dagger.Component;

//@Component(modules = CookInfoModule.class)
public interface CookInfoComponent {
  /**
   * Returns a CookInfo object.
   *
   * @return
   */
  BaseCookInfo buildCookInfo();
}
