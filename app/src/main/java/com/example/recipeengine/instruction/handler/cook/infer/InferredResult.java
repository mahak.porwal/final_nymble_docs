package com.example.recipeengine.instruction.handler.cook.infer;

import java.util.List;

/**
 * This class is responsible for storing the
 * {@link InferBlock} results.
 */
public class InferredResult {

  private List<Double> result;

  /**
   * Constructor of InferredResult.
   *
   * @param result List of results
   */
  public InferredResult(List<Double> result) {
    this.result = result;
  }

  public List<Double> getResult() {
    return result;
  }

  public void setResult(List<Double> result) {
    this.result = result;
  }
}
