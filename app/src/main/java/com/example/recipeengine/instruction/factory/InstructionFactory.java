package com.example.recipeengine.instruction.factory;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.factory.cook.CookFactory;
import com.example.recipeengine.instruction.factory.dispense.DispenseFactory;
import com.example.recipeengine.instruction.info.InstructionInfo;
import com.example.recipeengine.instruction.info.cook.CookInfo;
import com.example.recipeengine.instruction.info.dispense.DispenseInfo;

import java.util.Map;

import javax.inject.Inject;

public class InstructionFactory {

  private CookFactory cookFactory;
  private DispenseFactory dispenseFactory;

  @Inject
  public InstructionFactory(CookFactory cookFactory, DispenseFactory dispenseFactory) {
    this.cookFactory = cookFactory;
    this.dispenseFactory = dispenseFactory;
  }

  public Instruction createInstruction(InstructionInfo instructionInfo,
                                       Map<String, String> parameters) {
    Instruction instruction = null;
    if (instructionInfo.getInstructionType() == "Dispense") {
      try {
        instruction = dispenseFactory.createDispenseObject((DispenseInfo) instructionInfo,parameters);
      } catch (Exception e) {
        e.printStackTrace();
      }
    } else {
      try {
        instruction = cookFactory.createCookObject((CookInfo) instructionInfo, parameters);
      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }
    }
    return instruction;
  }
}
