package com.example.recipeengine.instruction.task.capture.vision;

import static com.example.recipeengine.util.ImageUtil._parseBitmap;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.example.recipeengine.util.FileUtil;
import com.example.recipeengine.util.ImageUtil;
import com.example.recipeengine.util.Native.NativeUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import som.hardware.response.SensorResponse;
import timber.log.Timber;

/**
 * Utility class which provides functions to convert the {@link SensorResponse}
 * sensor data which is of type List Double to Bitmap.
 */
public class BitmapUtility {

  int count = 0;

  public Bitmap convertToBitmap(List<Double> sensorResponse) {

    final double[] temperatureMatrixArray = new double[1024];
    int matrixIndex = 0;
    for (Double pixelVal : sensorResponse) {
      temperatureMatrixArray[matrixIndex] = pixelVal;
      matrixIndex++;
    }

    NativeUtil.CreateThermalImage(temperatureMatrixArray);
    final Mat thermalMat = new Mat();
    NativeUtil.PopulateThermalImage(thermalMat.getNativeObjAddr());
    final Bitmap thermalImageInUse = ImageUtil._parseBitmap(thermalMat);
    this.saveImage(thermalImageInUse,"thermal_image_"+String.valueOf(count++)+".jpeg");
    return thermalImageInUse;
  }

  private void saveImage(@NonNull Bitmap image, @NonNull String filename) {
    final File imageFile = FileUtil.getAsFile("/camera_test2/", filename);
    try (final FileOutputStream fos = new FileOutputStream(imageFile)) {
      image.compress(Bitmap.CompressFormat.JPEG, 100, fos);
    } catch (IOException e) {
      Timber.e(e, "Error saving image: %s", filename);
    }
  }

}
