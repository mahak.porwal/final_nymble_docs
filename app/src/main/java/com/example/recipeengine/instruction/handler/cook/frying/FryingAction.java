package com.example.recipeengine.instruction.handler.cook.frying;

import com.example.recipeengine.instruction.cook.params.action.ActionParams;
import com.example.recipeengine.instruction.handler.cook.action.BaseActionBlock;
import com.example.recipeengine.instruction.handler.cook.infer.InferredResult;
import com.example.recipeengine.instruction.handler.cook.status.ProgressData;
import com.example.recipeengine.instruction.handler.cook.status.UIData;
import io.reactivex.annotations.NonNull;
import io.reactivex.subjects.PublishSubject;
import java.util.List;
import som.hardware.request.handler.RequestHandler;
import som.instruction.request.heat.Heat;
import som.instruction.request.heat.HeatPowerLevel;

/**
 * Frying action class for taking actions on frying food saute.
 *
 * @author Pranshu Gupta
 */
public class FryingAction extends BaseActionBlock {

  public static final int SUCCESS_THRESH = 95;
  public static final int REACHING_THRESH = 70;
  private boolean isCompleted;


  /**
   * Constructor of action blocks.
   *
   * @param requestHandler For any hardware requests
   * @param uiEmitter      For any ui updates
   * @param dataLogger     For any data logging
   */
  public FryingAction(RequestHandler requestHandler,
                      PublishSubject<UIData> uiEmitter,
                      PublishSubject<ProgressData> dataLogger) {
    super(requestHandler, uiEmitter, dataLogger);
    this.isCompleted = false;
  }

  @Override
  public void takeAction(InferredResult inferredResult, ActionParams actionParameters)
      throws Exception {
    if (null != inferredResult) {
      final List<Double> res = inferredResult.getResult();
      if (1 > res.size()) {
        throw new AssertionError();
      }
      final double target = actionParameters.getVisualScore();
      final double current_score = res.get(0) * 100;
      final double score_target_per = current_score / target * 100;

      updateScoreStatus(score_target_per);    //Updating the scoreStatus string to give to user
      updateActionStatus();                  //Updating the actionStatus string for debugging

      if (!(SUCCESS_THRESH <= score_target_per)) {
        if (REACHING_THRESH <= score_target_per) {
          this.isCompleted = false;
        this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_FIVE);
        } else {
          this.isCompleted = false;
        this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_SEVEN);
        }
      } else {
        this.isCompleted = true;
      this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_ZERO);
      }
    } else {
      throw new IllegalArgumentException("Infer result cannot be null");
    }
  }


  /**
   * Method to check if the infer execution is completed.
   *
   * @return boolean variable set from success of infer
   */
  @Override
  public Boolean isInstCompleted() {
    return this.isCompleted;
  }


  /**
   * This method updates the score status string for giving updates to User about the current saute.
   *
   * @param score current reached score percentage.
   */
  private void updateScoreStatus(double score) {
    this.scoreStatus = "Julia is trying to reach the target frying by "
        + "constantly heating and stirring. "
        + "Currently, it has achieved "
        + score
        + " of the target and will reach the target "
        + "in approximately " + calcRemainingTimeSec() + " seconds";
  }


  /**
   * Update action score for debugging.
   */
  private void updateActionStatus() {
    this.actionStatus = "Frying saute in progress";
  }


  /**
   * Method to pause the execution of saute
   * Stop heating, stop stirring etc.
   *
   * @throws Exception exception
   */
  @Override
  public void pause() throws Exception {
  this.changePowerLevel(HeatPowerLevel.POWER_LEVEL_ZERO);
  }

  /**
   * Method to calculate and set the remaining time for the executing saute.
   *
   * @return Integer remaining time in seconds
   */
  @Override
  protected Integer calcRemainingTimeSec() {
    //Calculate remaining time logic to be added here.
    return 0;
  }
}
