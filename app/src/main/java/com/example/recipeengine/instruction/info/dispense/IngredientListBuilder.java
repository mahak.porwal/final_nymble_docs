package com.example.recipeengine.instruction.info.dispense;

import org.json.simple.JSONObject;

import java.util.List;

import javax.inject.Inject;

public class IngredientListBuilder {
  @Inject
  public IngredientListBuilder() {
  }

  public List<String> provideIngredientList(JSONObject jsonObject) {
    List ingredientList = (List) jsonObject.get("ingredients");
    return ingredientList;
  }
}
