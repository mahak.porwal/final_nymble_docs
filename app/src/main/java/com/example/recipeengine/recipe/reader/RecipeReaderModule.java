package com.example.recipeengine.recipe.reader;

import org.json.simple.JSONObject;

import java.util.Map;

import dagger.Module;
import dagger.Provides;

@Module
public class RecipeReaderModule {

  private int startIndex;
  private Map<String, String> parameters;
  private JSONObject recipeJson;

  public RecipeReaderModule(JSONObject recipeJson, int startIndex, Map<String, String> parameters) {
    this.recipeJson = recipeJson;
    this.parameters = parameters;
    this.startIndex = startIndex;
  }

  @Provides
  int providesStartIndex() {
    return this.startIndex;
  }

  @Provides
  Map<String, String> providesParameters() {
    return this.parameters;
  }

  @Provides
  JSONObject providesRecipeJSON() {
    return this.recipeJson;
  }
}
