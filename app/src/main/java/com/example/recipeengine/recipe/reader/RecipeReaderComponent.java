package com.example.recipeengine.recipe.reader;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = RecipeReaderModule.class)
public interface RecipeReaderComponent {
  RecipeReader buildRecipeReader();
}
