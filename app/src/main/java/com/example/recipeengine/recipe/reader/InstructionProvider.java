package com.example.recipeengine.recipe.reader;

import com.example.recipeengine.instruction.Instruction;

import java.util.List;

public interface InstructionProvider {
  List<Instruction> getNextInstructions();
}
