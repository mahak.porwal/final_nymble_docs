package com.example.recipeengine.recipe.reader;

import com.example.recipeengine.instruction.Instruction;
import com.example.recipeengine.instruction.factory.InstructionFactory;
import com.example.recipeengine.instruction.info.InstructionInfo;
import com.example.recipeengine.instruction.info.InstructionInfoBuilder;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * A class to provide instructions to execute
 * The responsibility of the {@link RecipeReader} is to provide it's client with a {@link List}
 * of executable {@link Instruction} objects that needs to be executed in parallel manner
 */
@Singleton
public class RecipeReader implements InstructionProvider {

  public static String filename =
    "./src/main/java/com/example/recipeengine/resources/null";

  InstructionFactory instructionFactory;
  InstructionInfoBuilder instructionInfoBuilder;
  List<InstructionInfo> infoList;
  Iterator<InstructionInfo> infoIterator;
  Map<String, String> parametersMap;
  int startIndex;

  @Inject
  public RecipeReader(InstructionInfoBuilder infoBuilder, InstructionFactory instructionFactory,
                      JSONObject recipe, Map<String, String> parameters, int startIndex) {
    this.instructionInfoBuilder = infoBuilder;
    this.infoList = this.instructionInfoBuilder.buildInstructionInfo(((List) recipe.get("instructions")));
    this.parametersMap = parameters;
    this.instructionFactory = instructionFactory;
    this.startIndex = startIndex;
    this.infoIterator = this.infoList.iterator();
    //TODO : Advance the iterator, according to the startIndex
  }

  /**
   * Converts the next {@link InstructionInfo} object in the infoList into
   * an executable instruction and returns the {@link Instruction} object in a list.
   *
   * @return List of instructions to be executed parallely
   */
  @Override
  public List<Instruction> getNextInstructions() {
    if (!infoIterator.hasNext()) {
      return null;
    }
    List<Instruction> instructionList = new ArrayList<>();
    instructionList.add(this.instructionFactory.createInstruction(infoIterator.next(),
        this.parametersMap));
    return instructionList;
  }
}
