package com.example.recipeengine;

public enum GpioOutputPins {
  MUX_A0,
  MUX_A1,
  FAN_ON_OFF,
  HEATER_ENABLE
}
