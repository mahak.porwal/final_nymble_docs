package com.example.recipeengine.util.debug.components.liqStirFan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentTemperatureSenseBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.function.Supplier;

import io.reactivex.disposables.Disposable;

public final class TemperatureComponent extends DebugComponent<ComponentTemperatureSenseBinding> {
  public static final String[] TEMPERATURE_INPUTS = {"Water Temp", "Body Temp"};
  private Disposable disposable;

  public TemperatureComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentTemperatureSenseBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentTemperatureSenseBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentTemperatureSenseBinding binding) {
    binding.temperatureInputs.setAdapter(new SpinnerAdapter(getContext(), TEMPERATURE_INPUTS));
    binding.start.setOnClickListener(v -> {
      final int inputIndex = binding.temperatureInputs.getSelectedItemPosition();
      final int freq = Integer.parseInt(binding.frequency.getText().toString());
      this.start(inputIndex, freq);
    });
    binding.stop.setOnClickListener(v -> {
      this.stop();
    });
    binding.stop.setEnabled(false);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
    this.setStatus(message);
  }

  private void start(final int inputIndex, final int frequency) {
    getBinding().start.setEnabled(false);
    getBinding().stop.setEnabled(true);
    final int periodInMilliSecond = 1000 / frequency;
    final Supplier<String> stringSupplier;
    switch (inputIndex) {
      case 0:
        stringSupplier = () -> DebugGuiMethods.getWaterTempSensor().toString();
        break;
      case 1:
        stringSupplier = () -> DebugGuiMethods.getBodyTempSensor().toString();
        break;
      default:
        return;
    }
    this.disposable =
      FrequencyScheduler.scheduleAPeriodicPollAndConsumeOnMainThread(periodInMilliSecond,
        stringSupplier, this::onResponse);
  }

  private void stop() {
    this.disposable.dispose();
    getBinding().start.setEnabled(true);
    getBinding().stop.setEnabled(false);
  }


  private void setStatus(@NonNull final String reading) {
    getBinding().status.setText(reading);
  }
}
