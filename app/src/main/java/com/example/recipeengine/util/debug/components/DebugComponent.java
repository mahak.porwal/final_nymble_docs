package com.example.recipeengine.util.debug.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;

import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.camera.ArukoAngleComponent;
import com.example.recipeengine.util.debug.components.camera.StirrerTestComponent;
import com.example.recipeengine.util.debug.components.camera.ThermalCaptureComponent;
import com.example.recipeengine.util.debug.components.cb.HeartbeatComponent;
import com.example.recipeengine.util.debug.components.cb.I2cCbComponent;
import com.example.recipeengine.util.debug.components.cb.InputPinsCbComponent;
import com.example.recipeengine.util.debug.components.cb.OutputPinsCbComponent;
import com.example.recipeengine.util.debug.components.cb.ResetCbComponent;
import com.example.recipeengine.util.debug.components.commands.HeatCommandComponent;
import com.example.recipeengine.util.debug.components.commands.LiqStirCommandComponent;
import com.example.recipeengine.util.debug.components.commands.MacroCommandComponent;
import com.example.recipeengine.util.debug.components.commands.MicroCommandComponent;
import com.example.recipeengine.util.debug.components.eeprom.LoadCellScalingComponent;
import com.example.recipeengine.util.debug.components.eeprom.MacroHomingComponent;
import com.example.recipeengine.util.debug.components.eeprom.OilPumpFactorComponent;
import com.example.recipeengine.util.debug.components.eeprom.PcaFactorComponent;
import com.example.recipeengine.util.debug.components.eeprom.WaterPumpFactorComponent;
import com.example.recipeengine.util.debug.components.induction.DacInductionComponent;
import com.example.recipeengine.util.debug.components.induction.InputPinsInductionComponent;
import com.example.recipeengine.util.debug.components.induction.OutputPinsInductionComponent;
import com.example.recipeengine.util.debug.components.induction.PanDetectComponent;
import com.example.recipeengine.util.debug.components.induction.PowerLevelComponent;
import com.example.recipeengine.util.debug.components.induction.SensorInductionComponent;
import com.example.recipeengine.util.debug.components.ledBuzzer.BuzzerControlComponent;
import com.example.recipeengine.util.debug.components.ledBuzzer.IlluminationLedComponent;
import com.example.recipeengine.util.debug.components.ledBuzzer.LedControlComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.ExFanComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.FlowMeterComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.HeatingElementComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.LiqComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.LiqLoadCellComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.StirComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.StirFBComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.TachometerComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.TemperatureComponent;
import com.example.recipeengine.util.debug.components.macro.MacroLifterComponent;
import com.example.recipeengine.util.debug.components.macro.MacroLoadCellComponent;
import com.example.recipeengine.util.debug.components.macro.MacroLoadSensorComponent;
import com.example.recipeengine.util.debug.components.macro.MotorRotationComponent;
import com.example.recipeengine.util.debug.components.micro.PodAngleComponent;
import com.example.recipeengine.util.debug.components.micro.PodHomingComponent;
import com.example.recipeengine.util.debug.components.micro.PodRotationComponent;
import com.example.recipeengine.util.debug.components.system.CurrentSystemComponent;
import com.example.recipeengine.util.debug.components.system.I2cSystemComponent;
import com.example.recipeengine.util.debug.components.system.InputIna300ssSystemComponent;
import com.example.recipeengine.util.debug.components.system.Lm2678SystemComponent;
import com.example.recipeengine.util.debug.components.system.OutputIna300ssSystemComponent;
import com.example.recipeengine.util.debug.components.system.PcbTempSystemComponent;
import com.example.recipeengine.util.debug.renderer.ComponentHolder;

import java.io.Closeable;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

/**
 * The base debugger component.
 *
 * @author Abarajithan
 */
public abstract class DebugComponent<B extends ViewDataBinding> implements Closeable {

    private final Context context;
    private final DebugViewModel viewModel;
    private final CompositeDisposable disposables;

    private B binding;

    public DebugComponent(
            @NonNull final Context context,
            @NonNull final DebugViewModel viewModel) {
        this.context = context;
        this.viewModel = viewModel;
        this.disposables = new CompositeDisposable();
    }

    /**
     * Create the component.
     * Initialize the ViewBinding class.
     */
    public final ComponentHolder<B> onCreate(@NonNull final ViewGroup parent) {
        /*add(this.viewModel.feedbackMessages()
                .filter(this::filterCondition)
                .subscribe(this::onResponse, Timber::e));*/
        //
        this.binding = onInflateView(LayoutInflater.from(context), parent);
        return new ComponentHolder<B>(binding) {
            @Override
            protected void onCreateHolder(@NonNull B binding) {
                onCreateView(binding);
            }
        };
    }

    /**
     * Add the rx-disposable to the {@link CompositeDisposable} object.
     *
     * @param disposable the disposable to add
     */
    protected final void add(@NonNull final Disposable disposable) {
        this.disposables.add(disposable);
    }

    /**
     * @return the bounded DebugViewModel
     */
    public DebugViewModel getViewModel() {
        return viewModel;
    }

    /**
     * @return the context
     */
    public Context getContext() {
        return context;
    }

    /**
     * @return the UI binding
     */
    public B getBinding() {
        return this.binding;
    }

    /**
     * Create & Initialize the View binding data object here.
     * <p>
     * Called once when {@link ComponentsAdapter#onCreateViewHolder(ViewGroup, int)}
     * method is called.
     *
     * @param inflater the layout inflator
     * @param parent   the parent view
     * @return the View binding data class
     */
    public abstract B onInflateView(
            @NonNull final LayoutInflater inflater,
            @NonNull final ViewGroup parent
    );

    /**
     * Initialize the view defaults here.
     * Called once inside the ViewHolder's constructor.
     *
     * @param binding the View binding data class
     */
    public abstract void onCreateView(@NonNull final B binding);

    /**
     * Bind the data and work on the view here.
     * Called by {@link ComponentsAdapter#onBindViewHolder(ComponentHolder, int)}.
     */
    public abstract void onBindView();

    /**
     * Called when a feedback response is received from MCU.
     * By default, this is called on Main thread.
     *
     * @param message the feedback message from MCU
     */
    public abstract void onResponse(final String message);

    /**
     * Filter the feedback messages with a condition.
     * <p>
     * Returning false will give no response
     * (ie) the method {@link #onResponse(String)} will never be called.
     * <p>
     * Returning true will allow all messages
     * (ie) the method {@link #onResponse(String)} will be called for all MCU responses.
     *
     * @param message the feedback message from MCU
     * @return a condition to filter feedback messages
     */
    //public abstract boolean filterCondition();

    @Override
    public void close() {
        this.disposables.dispose();
    }

    /**
     * Factory class to create a DebugComponent
     */
    public static final class Factory {

        private final Context context;
        private final DebugViewModel viewModel;

        public Factory(
                @NonNull final Context context,
                @NonNull final DebugViewModel viewModel
        ) {
            this.context = context;
            this.viewModel = viewModel;
        }

        public <T extends DebugComponent> T create(@NonNull Class<T> componentClass) {
            // fixme: Use DI to provide instance
            if (componentClass.isAssignableFrom(HeartbeatComponent.class)) {
                return (T) new HeartbeatComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(ResetCbComponent.class)) {
                return (T) new ResetCbComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(InputPinsCbComponent.class)) {
                return (T) new InputPinsCbComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(OutputPinsCbComponent.class)) {
                return (T) new OutputPinsCbComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(I2cCbComponent.class)) {
                return (T) new I2cCbComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(I2cSystemComponent.class)) {
                return (T) new I2cSystemComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(PcbTempSystemComponent.class)) {
                return (T) new PcbTempSystemComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(CurrentSystemComponent.class)) {
                return (T) new CurrentSystemComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(Lm2678SystemComponent.class)) {
                return (T) new Lm2678SystemComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(InputIna300ssSystemComponent.class)) {
                return (T) new InputIna300ssSystemComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(OutputIna300ssSystemComponent.class)) {
                return (T) new OutputIna300ssSystemComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(ArukoAngleComponent.class)) {
                return (T) new ArukoAngleComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(StirrerTestComponent.class)) {
                return (T) new StirrerTestComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(ThermalCaptureComponent.class)) {
                return (T) new ThermalCaptureComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(StirComponent.class)) {
                return (T) new StirComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(StirFBComponent.class)) {
                return (T) new StirFBComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(LiqComponent.class)) {
                return (T) new LiqComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(LiqLoadCellComponent.class)) {
                return (T) new LiqLoadCellComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(ExFanComponent.class)) {
                return (T) new ExFanComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(TachometerComponent.class)) {
                return (T) new TachometerComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(SensorInductionComponent.class)) {
                return (T) new SensorInductionComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(PowerLevelComponent.class)) {
                return (T) new PowerLevelComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(PanDetectComponent.class)) {
                return (T) new PanDetectComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(InputPinsInductionComponent.class)) {
                return (T) new InputPinsInductionComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(OutputPinsInductionComponent.class)) {
                return (T) new OutputPinsInductionComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(MotorRotationComponent.class)) {
                return (T) new MotorRotationComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(MacroLifterComponent.class)) {
                return (T) new MacroLifterComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(MacroLoadCellComponent.class)) {
                return (T) new MacroLoadCellComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(MacroLoadSensorComponent.class)) {
                return (T) new MacroLoadSensorComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(PodRotationComponent.class)) {
                return (T) new PodRotationComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(PodAngleComponent.class)) {
                return (T) new PodAngleComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(PodHomingComponent.class)) {
                return (T) new PodHomingComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(HeatCommandComponent.class)) {
                return (T) new HeatCommandComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(MicroCommandComponent.class)) {
                return (T) new MicroCommandComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(MacroCommandComponent.class)) {
                return (T) new MacroCommandComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(LiqStirCommandComponent.class)) {
                return (T) new LiqStirCommandComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(LedControlComponent.class)) {
                return (T) new LedControlComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(BuzzerControlComponent.class)) {
                return (T) new BuzzerControlComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(LoadCellScalingComponent.class)) {
                return (T) new LoadCellScalingComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(OilPumpFactorComponent.class)) {
                return (T) new OilPumpFactorComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(WaterPumpFactorComponent.class)) {
                return (T) new WaterPumpFactorComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(PcaFactorComponent.class)) {
                return (T) new PcaFactorComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(MacroHomingComponent.class)) {
                return (T) new MacroHomingComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(DacInductionComponent.class)) {
                return (T) new DacInductionComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(FlowMeterComponent.class)) {
                return (T) new FlowMeterComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(HeatingElementComponent.class)) {
                return (T) new HeatingElementComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(TemperatureComponent.class)) {
                return (T) new TemperatureComponent(context, viewModel);
            } else if (componentClass.isAssignableFrom(IlluminationLedComponent.class)) {
                return (T) new IlluminationLedComponent(context, viewModel);
            }
            throw new IllegalArgumentException("Unsupported Debug component class: '"
                    + componentClass.getSimpleName() + "' Did you forget to register?");
        }
    }
}
