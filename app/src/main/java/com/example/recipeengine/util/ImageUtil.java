package com.example.recipeengine.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import timber.log.Timber;

public final class ImageUtil {
  private ImageUtil() {
  }

  /**
   * Convert Mat to Bitmap
   */
  public static Bitmap _parseBitmap(final Mat mat) {
    Bitmap bmp = null;
    final Mat img = new Mat();
    Imgproc.cvtColor(mat, img, Imgproc.COLOR_BGR2RGB);
    try {
      bmp = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
      Utils.matToBitmap(img, bmp);
    } catch (CvException e) {
    }
    return bmp;
  }


  /**
   * Resize the image to 400x400
   */
  public static Bitmap _cropImage(Bitmap image) {
    final Mat imageToMat = new Mat();
    Utils.bitmapToMat(image, imageToMat);
    final Point origin;
    final Point size;
    origin = new Point(420, 0);
    size = new Point(1500, 1080);
    final Rect region_of_interest = new Rect(origin, size);
    final Mat a = new Mat();
    //Imgproc.resize(imageToMat, a, new Size(400, 300));
    Timber.d("size = %s", a.size());
    Imgproc.resize(imageToMat.submat(region_of_interest), a, new Size(400, 400));
    //Utils.matToBitmap(a,image);
    Imgproc.cvtColor(a, a, Imgproc.COLOR_BGRA2RGB);
    image = _parseBitmap(a);

    return image;
  }


  /**
   * Resize the image to 400x400
   */
  public static Mat _cropImage(final Mat image) {
    Point origin, size;
    origin = new Point(420, 0);
    size = new Point(1500, 1080);
    final Rect region_of_interest = new Rect(origin, size);
    final Mat a = new Mat();
    //Imgproc.resize(imageToMat, a, new Size(400, 300));
//        Timber.d("size = %s", a.size());
    Imgproc.resize(image.submat(region_of_interest), a, new Size(400, 400));
    return a;
  }

  // This value is 2 ^ 18 - 1, and is used to clamp the RGB values before their ranges
  // are normalized to eight bits.
  static final int kMaxChannelValue = 262143;

  /**
   * Utility method to compute the allocated size in bytes of a YUV420SP image of the given
   * dimensions.
   */
  public static int getYUVByteSize(final int width, final int height) {
    // The luminance plane requires 1 byte per pixel.
    final int ySize = width * height;

    // The UV plane works on 2x2 blocks, so dimensions with odd size must be rounded up.
    // Each 2x2 block takes 2 bytes to encode, one each for U and V.
    final int uvSize = ((width + 1) / 2) * ((height + 1) / 2) * 2;

    return ySize + uvSize;
  }

  /**
   * Saves a Bitmap object to disk for analysis.
   *
   * @param bitmap The bitmap to save.
   */
  public static void saveBitmap(final Bitmap bitmap) {
    saveBitmap(bitmap, "preview.png");
  }

  /**
   * Saves a Bitmap object to disk for analysis.
   *
   * @param bitmap   The bitmap to save.
   * @param filename The location to save the bitmap to.
   */
  public static void saveBitmap(final Bitmap bitmap, final String filename) {
    final String root =
        Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "tensorflow";
    Timber.i("Saving %dx%d bitmap to %s.", bitmap.getWidth(), bitmap.getHeight(), root);
    final File myDir = new File(root);

    if (!myDir.mkdirs()) {
      Timber.i("Make dir failed");
    }

    final String fname = filename;
    final File file = new File(myDir, fname);
    if (file.exists()) {
      file.delete();
    }
    try {
      final FileOutputStream out = new FileOutputStream(file);
      bitmap.compress(Bitmap.CompressFormat.PNG, 99, out);
      out.flush();
      out.close();
    } catch (final Exception e) {
      Timber.e(e, "Exception!");
    }
  }

  public static void convertYUV420SPToARGB8888(byte[] input, int width, int height, int[] output) {
    final int frameSize = width * height;
    for (int j = 0, yp = 0; j < height; j++) {
      int uvp = frameSize + (j >> 1) * width;
      int u = 0;
      int v = 0;

      for (int i = 0; i < width; i++, yp++) {
        int y = 0xff & input[yp];
        if ((i & 1) == 0) {
          v = 0xff & input[uvp++];
          u = 0xff & input[uvp++];
        }

        output[yp] = YUV2RGB(y, u, v);
      }
    }
  }

  private static int YUV2RGB(int y, int u, int v) {
    // Adjust and check YUV values
    y = (y - 16) < 0 ? 0 : (y - 16);
    u -= 128;
    v -= 128;

    // This is the floating point equivalent. We do the conversion in integer
    // because some Android devices do not have floating point in hardware.
    // nR = (int)(1.164 * nY + 2.018 * nU);
    // nG = (int)(1.164 * nY - 0.813 * nV - 0.391 * nU);
    // nB = (int)(1.164 * nY + 1.596 * nV);
    int y1192 = 1192 * y;
    int r = (y1192 + 1634 * v);
    int g = (y1192 - 833 * v - 400 * u);
    int b = (y1192 + 2066 * u);

    // Clipping RGB values to be inside boundaries [ 0 , kMaxChannelValue ]
    r = r > kMaxChannelValue ? kMaxChannelValue : (r < 0 ? 0 : r);
    g = g > kMaxChannelValue ? kMaxChannelValue : (g < 0 ? 0 : g);
    b = b > kMaxChannelValue ? kMaxChannelValue : (b < 0 ? 0 : b);

    return 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
  }

  public static void convertYUV420ToARGB8888(
      byte[] yData,
      byte[] uData,
      byte[] vData,
      int width,
      int height,
      int yRowStride,
      int uvRowStride,
      int uvPixelStride,
      int[] out) {
    int yp = 0;
    for (int j = 0; j < height; j++) {
      int pY = yRowStride * j;
      int pUV = uvRowStride * (j >> 1);

      for (int i = 0; i < width; i++) {
        int uv_offset = pUV + (i >> 1) * uvPixelStride;

        out[yp++] = YUV2RGB(0xff & yData[pY + i], 0xff & uData[uv_offset], 0xff & vData[uv_offset]);
      }
    }
  }

  public static Bitmap centerCrop(Bitmap image, Point origin, int size) {
    final Mat imageToMat = new Mat();
    Utils.bitmapToMat(image, imageToMat);
    final Point crop_size;
    crop_size = new Point(origin.x + size, origin.y + size);
    final Rect region_of_interest = new Rect(origin, crop_size);
    final Mat a = new Mat();
    Imgproc.resize(imageToMat.submat(region_of_interest), a, new Size(size, size));
    return _parseBitmap(a);
  }

  /**
   * Returns a transformation matrix from one reference frame into another. Handles cropping (if
   * maintaining aspect ratio is desired) and rotation.
   *
   * @param srcWidth            Width of source frame.
   * @param srcHeight           Height of source frame.
   * @param dstWidth            Width of destination frame.
   * @param dstHeight           Height of destination frame.
   * @param applyRotation       Amount of rotation to apply from one frame to another. Must be a multiple
   *                            of 90.
   * @param maintainAspectRatio If true, will ensure that scaling in x and y remains constant,
   *                            cropping the image if necessary.
   * @return The transformation fulfilling the desired requirements.
   */
  public static Matrix getTransformationMatrix(
      final int srcWidth,
      final int srcHeight,
      final int dstWidth,
      final int dstHeight,
      final int applyRotation,
      final boolean maintainAspectRatio) {
    final Matrix matrix = new Matrix();

    if (applyRotation != 0) {
      if (applyRotation % 90 != 0) {
        Timber.w("Rotation of %d % 90 != 0", applyRotation);
      }

      // Translate so center of image is at origin.
      matrix.postTranslate(-srcWidth / 2.0f, -srcHeight / 2.0f);

      // Rotate around origin.
      matrix.postRotate(applyRotation);
    }

    // Account for the already applied rotation, if any, and then determine how
    // much scaling is needed for each axis.
    final boolean transpose = (Math.abs(applyRotation) + 90) % 180 == 0;

    final int inWidth = transpose ? srcHeight : srcWidth;
    final int inHeight = transpose ? srcWidth : srcHeight;

    // Apply scaling if necessary.
    if (inWidth != dstWidth || inHeight != dstHeight) {
      final float scaleFactorX = dstWidth / (float) inWidth;
      final float scaleFactorY = dstHeight / (float) inHeight;

      if (maintainAspectRatio) {
        // Scale by minimum factor so that dst is filled completely while
        // maintaining the aspect ratio. Some image may fall off the edge.
        final float scaleFactor = Math.max(scaleFactorX, scaleFactorY);
        matrix.postScale(scaleFactor, scaleFactor);
      } else {
        // Scale exactly to fill dst from src.
        matrix.postScale(scaleFactorX, scaleFactorY);
      }
    }

    if (applyRotation != 0) {
      // Translate back from origin centered reference to destination frame.
      matrix.postTranslate(dstWidth / 2.0f, dstHeight / 2.0f);
    }

    return matrix;
  }

  /**
   * Converting bitmap image to tensorflow lite byte buffer for model's input
   *
   * @param mean
   * @param std
   * @param inputImage
   * @return ByteBuffer
   */
  public static ByteBuffer bitmapToByteBuffer(Bitmap inputImage, int imageSize, float[] mean,
                                              float[] std) {
    final Bitmap image = scaleBitmapAndKeepRatio(inputImage, imageSize, imageSize);
    final ByteBuffer inputBuffer = ByteBuffer.allocateDirect(imageSize * imageSize * 3 * 4);
    inputBuffer.order(ByteOrder.nativeOrder());
    inputBuffer.rewind();
    final int[] intValues = new int[imageSize * imageSize];
    image.getPixels(intValues, 0, imageSize, 0, 0, imageSize, imageSize);
    int pixel = 0;
    for (int i = 0; imageSize > i; i++) {
      for (int j = 0; imageSize > j; j++) {
        final int value = intValues[pixel];
        pixel++;
        inputBuffer.putFloat((((((value >> 16) & 0xff) / 255.0f)) - mean[0]) / std[0]);
        inputBuffer.putFloat((((((value >> 8) & 0xff) / 255.0f)) - mean[1]) / std[1]);
        inputBuffer.putFloat((((((value) & 0xff) / 255.0f)) - mean[2]) / std[2]);

      }
    }
    //inputBuffer.rewind();
    return inputBuffer;
  }

  /**
   * Scaling bitmap by given width and hight while keeping the aspect ratio same.
   *
   * @param targetBmp
   * @param width
   * @param height
   * @return
   */
  public static Bitmap scaleBitmapAndKeepRatio(Bitmap targetBmp, int width, int height) {
    if (targetBmp.getWidth() == height && targetBmp.getHeight() == height) {
      return targetBmp;
    }
    final float scaleW = ((float) width) / targetBmp.getWidth();
    final float scaleH = ((float) height) / targetBmp.getHeight();
    Timber.d("size = %d,%d", targetBmp.getWidth(), targetBmp.getHeight());
    final Matrix matrix = new Matrix();
    matrix.postScale(scaleW, scaleH);
    return Bitmap
        .createBitmap(targetBmp, 0, 0, targetBmp.getWidth(), targetBmp.getHeight(), matrix, true);
  }


  /**
   * Convert Gray scale Mat to Bitmap
   */
  public static Bitmap parseBitmapGrey(Mat mat) {
    Bitmap bmp = null;
    final Mat img = new Mat();
    Imgproc.cvtColor(mat, img, Imgproc.COLOR_GRAY2RGB);
    try {
      bmp = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);
      Utils.matToBitmap(img, bmp);
    } catch (CvException e) {
      Timber.e(e);
    }
    return bmp;
  }


  /**
   * @param bmp        input bitmap
   * @param contrast   0..10, 1 is default
   * @param brightness -255..255, 0 is default
   * @return new bitmap
   */
  public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast,
                                                      float brightness) {
    final ColorMatrix cm = new ColorMatrix(new float[]
        {
            contrast, 0, 0, 0, brightness,
            0, contrast, 0, 0, brightness,
            0, 0, contrast, 0, brightness,
            0, 0, 0, 1, 0
        });

    final Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

    final Canvas canvas = new Canvas(ret);

    final Paint paint = new Paint();
    paint.setColorFilter(new ColorMatrixColorFilter(cm));
    canvas.drawBitmap(bmp, 0, 0, paint);

    return ret;
  }

}
