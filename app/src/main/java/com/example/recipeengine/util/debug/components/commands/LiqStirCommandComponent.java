package com.example.recipeengine.util.debug.components.commands;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentLiqStirCommandBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.LiqComponent;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import timber.log.Timber;

/**
 * Selection of Pump, with selection of qty; and a working STOP Button
 *
 * @author Abarajithan
 */
public final class LiqStirCommandComponent extends DebugComponent<ComponentLiqStirCommandBinding> {

  public LiqStirCommandComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentLiqStirCommandBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentLiqStirCommandBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentLiqStirCommandBinding binding) {
    binding.pumps.setAdapter(new SpinnerAdapter(getContext(), LiqComponent.PUMPS));
    binding.start.setOnClickListener(v -> {
      final int pumpIndex = binding.pumps.getSelectedItemPosition();
      final String quantityStr = binding.quantity.getText().toString();
      if (quantityStr.trim().isEmpty()) {
        Toast.makeText(getContext(), "Quantity required", Toast.LENGTH_SHORT).show();
        return;
      }
      final int quantity = Integer.parseInt(quantityStr.trim());
      start(pumpIndex, quantity);
    });
    binding.stop.setOnClickListener(v -> {
      final int pumpIndex = binding.pumps.getSelectedItemPosition();
      stop(pumpIndex);
    });
  }

  private void start(final int pumpIndex, final int quantity) {
    enableStartButton(false);
    final Action action;
    if (0 == pumpIndex) {
      //water
      action = () -> {
        DebugGuiMethods.waterDispense(quantity);
      };
    } else {
      //oil
      action = () -> {
        DebugGuiMethods.oilDispense(quantity);
      };
    }
    FrequencyScheduler.runAsynchronously(action,
      () -> {
        enableStartButton(true);
      });
        /*enableStartButton(false);

        final int podId = pumpIndex == 0 ? Constants.Liquid_Water_Pump : Constants.Liquid_Oil_Pump;
        final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuAction);
        message.setSubSystem(Constants.Liquid);
        message.setId(podId);
        message.setValue(Collections.singletonMap(String.valueOf(Constants.Primary), quantity));

        add(Observable.just(1)
                .delay(1, TimeUnit.SECONDS)
                .doOnNext(l -> getViewModel().visionController().sensorDataController(0))
                .concatMap(l -> Observable.just(1).delay(2, TimeUnit.SECONDS))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(l -> {
                    getViewModel().sendToHardware(message);

                    final LogDetail logDetail = new LogDetail(LoggingEvents.DISPENSE_START,
                            TriggerEvents.MANUAL, Constants.nameOf(podId),
                            Collections.singletonMap(LogDetail.eventAmount, quantity));
                    getViewModel().visionController().logDetail(logDetail);

                    Toast.makeText(getContext(), "Dispense in progress..", Toast.LENGTH_SHORT).show();
                }));*/
  }

  private void enableStartButton(final boolean enable) {
    getBinding().start.setEnabled(enable);
  }

  private void enableStopButton(final boolean enable) {
    getBinding().stop.setEnabled(enable);
  }

  private void stop(final int pumpIndex) {
    enableStopButton(false);
    final Action action;
    if (0 == pumpIndex) {
      //water
      action = DebugGuiMethods::abortWaterDispense;
    } else {
      //oil
      action = DebugGuiMethods::abortOilDispense;
    }
    FrequencyScheduler.runAsynchronously(action, () -> enableStopButton(true));
        /*final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuAction);
        message.setSubSystem(Constants.Liquid);
        message.setId(Constants.Liquid_Abort_Dispense);
        getViewModel().sendToHardware(message);*/
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(@NonNull final String message) {
        /*this.enableStartButton(true);
        final Message ackResponse = Messages.ack(Board.CONTROL);
        ackResponse.setSeqNo(message.getSeqNo());
        getViewModel().sendACK(ackResponse);

        final LogDetail logDetail = new LogDetail(LoggingEvents.DISPENSE_STOP,
                TriggerEvents.MANUAL, Constants.nameOf(message.getId()));
        getViewModel().visionController().logDetail(logDetail);

        add(Observable.just(1)
                .doOnNext(l -> getViewModel().visionController().sensorDataController(1))
                .concatMap(l -> Observable.just(l).delay(1, TimeUnit.SECONDS))
                .observeOn(AndroidSchedulers.mainThread())
                .map(integer -> "a")
                .doOnNext(l -> Timber.d(l))
                .subscribe(l -> Toast.makeText(getContext(),
                        "Liquid dispense done!", Toast.LENGTH_SHORT).show()));*/
  }
}
