package com.example.recipeengine.util.debug.fragment;

import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_STIR_LIQ_FAN_FAN;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_STIR_LIQ_FAN_HEAT_ELEMENT;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_STIR_LIQ_FAN_LIQ;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_STIR_LIQ_FAN_STIR;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * Stirrer, Liquid and Exhaust fan controls
 *
 * @author Abarajithan
 */
public final class StirLiqFanDebugFragment extends ComponentTabsFragment {

    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_liq_stir_fan_fragment_title);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{
                ComponentListFragment.create(ID_STIR_LIQ_FAN_STIR),
                ComponentListFragment.create(ID_STIR_LIQ_FAN_LIQ),
                ComponentListFragment.create(ID_STIR_LIQ_FAN_FAN),
                ComponentListFragment.create(ID_STIR_LIQ_FAN_HEAT_ELEMENT)
        };
    }
}
