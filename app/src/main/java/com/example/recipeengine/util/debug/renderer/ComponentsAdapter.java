package com.example.recipeengine.util.debug.renderer;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recipeengine.util.debug.components.DebugComponent;

import java.io.Closeable;
import java.util.List;

/**
 * @author Abarajithan
 */
public class ComponentsAdapter extends RecyclerView.Adapter<ComponentHolder> implements Closeable {

    private final List<? extends DebugComponent> components;

    public ComponentsAdapter(@NonNull final List<? extends DebugComponent> components) {
        this.components = components;
    }

    @NonNull
    @Override
    public ComponentHolder<?> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return components.get(viewType).onCreate(parent);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull ComponentHolder holder, int position) {
        components.get(position).onBindView();
    }

    @Override
    public int getItemCount() {
        return components.size();
    }

    @Override
    public void close() {
        for (final DebugComponent component : components) {
            component.close();
        }
    }

}
