package com.example.recipeengine.util.debug.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;
import com.google.android.material.tabs.TabLayout;

/**
 * Base fragment for tabbed debug screens
 *
 * @author Abarajithan
 */
public abstract class ComponentTabsFragment extends Fragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_base_tabbed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager = view.findViewById(R.id.view_pager);
        tabLayout = view.findViewById(R.id.tab_layout);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final String[] titles = tabTitles();
        final ComponentListFragment[] fragments = fragments();
        if (titles.length != fragments.length) {
            throw new IllegalStateException("No of fragments are not equal to the titles provided");
        }

        final TabAdapter tabAdapter = new TabAdapter(getChildFragmentManager(), titles, fragments);
        viewPager.setOffscreenPageLimit(fragments.length);
        viewPager.setAdapter(tabAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    public abstract String[] tabTitles();

    public abstract ComponentListFragment[] fragments();

    static class TabAdapter extends FragmentStatePagerAdapter {

        private final String[] tabTitles;
        private final Fragment[] fragments;

        public TabAdapter(
                @NonNull final FragmentManager fm,
                @NonNull final String[] tabTitles,
                @NonNull final ComponentListFragment[] fragments
        ) {
            super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
            this.tabTitles = tabTitles;
            this.fragments = fragments;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragments[position];
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        @Override
        public int getCount() {
            return tabTitles.length;
        }
    }

}
