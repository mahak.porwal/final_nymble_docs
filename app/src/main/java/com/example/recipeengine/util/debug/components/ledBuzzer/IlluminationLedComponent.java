package com.example.recipeengine.util.debug.components.ledBuzzer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentIlluminationLedControlBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.components.DebugComponent;

public final class IlluminationLedComponent extends DebugComponent<ComponentIlluminationLedControlBinding> {
  public IlluminationLedComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentIlluminationLedControlBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentIlluminationLedControlBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentIlluminationLedControlBinding binding) {

    binding.set.setOnClickListener(v -> {
      int brightness;
      try {
        brightness = Integer.parseInt(binding.brightness.getText().toString());
      } catch (NumberFormatException e) {
        brightness = 5;
      }
      this.setBrightness(brightness);
    });
    binding.off.setOnClickListener(v -> {
      this.turnOffLed();
    });
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }

  private void setBrightness(final int brightness) {
    FrequencyScheduler.runAsynchronously(() -> DebugGuiMethods.setIlluminationLed(brightness), () -> {
    });
  }

  private void turnOffLed() {
    FrequencyScheduler.runAsynchronously(DebugGuiMethods::illuminationLedOff, () -> {
    });
  }
}

