package com.example.recipeengine.util.debug.components.commands;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentMicroCommandsBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;
import com.example.recipeengine.util.debug.components.micro.PodRotationComponent;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;

/**
 * Selection of Micro and no of turns
 *
 * @author Abarajithan
 */
public final class MicroCommandComponent extends DebugComponent<ComponentMicroCommandsBinding> {
  public static final String[] PODS = {"Pod 1", "Pod 2", "Pod 3", "Pod 4", "Pod 5", "Pod 6"};

  public MicroCommandComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentMicroCommandsBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentMicroCommandsBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentMicroCommandsBinding binding) {
    binding.pods.setAdapter(new SpinnerAdapter(getContext(), PODS));
    binding.start.setOnClickListener(v -> {
      final int podIndex = binding.pods.getSelectedItemPosition();
      final String turnsStr = binding.turns.getText().toString().trim();
      if (turnsStr.isEmpty()) {
        Toast.makeText(getContext(), "Turns required", Toast.LENGTH_SHORT).show();
        return;
      }
      final int turns = Integer.parseInt(turnsStr);
      start(podIndex, turns);
    });
    binding.stop.setOnClickListener(v -> {
      final int podIndex = binding.pods.getSelectedItemPosition();
      stop(podIndex);
    });
  }

  private void start(final int podIndex, final int turns) {
    enableStartButton(false);
    enableStopButton(true);
    final Action action;
    switch (podIndex) {
      case 0:
        action = () -> DebugGuiMethods.microDispense_Micro_1(turns);
        break;
      case 1:
        action = () -> DebugGuiMethods.microDispense_Micro_2(turns);
        break;
      case 2:
        action = () -> DebugGuiMethods.microDispense_Micro_3(turns);
        break;
      case 3:
        action = () -> DebugGuiMethods.microDispense_Micro_4(turns);
        break;
      case 4:
        action = () -> DebugGuiMethods.microDispense_Micro_5(turns);
        break;
      case 5:
        action = () -> DebugGuiMethods.microDispense_Micro_6(turns);
        break;
      default:
        return;
    }
    FrequencyScheduler.runAsynchronously(action, () -> {
      enableStartButton(true);
      enableStopButton(false);
    });
  }

  private void enableStartButton(final boolean enable) {
    getBinding().start.setEnabled(enable);
  }

  private void enableStopButton(final boolean enable) {
    getBinding().stop.setEnabled(enable);
  }

  private void stop(final int podIndex) {
    enableStopButton(false);
    final Action action;
    switch (podIndex) {
      case 0:
        action = DebugGuiMethods::abortMicroDispense_Micro_1;
        break;
      case 1:
        action = DebugGuiMethods::abortMicroDispense_Micro_2;
        break;
      case 2:
        action = DebugGuiMethods::abortMicroDispense_Micro_3;
        break;
      case 3:
        action = DebugGuiMethods::abortMicroDispense_Micro_4;
        break;
      case 4:
        action = DebugGuiMethods::abortMicroDispense_Micro_5;
        break;
      case 5:
        action = DebugGuiMethods::abortMicroDispense_Micro_6;
        break;
      default:
        return;
    }
    FrequencyScheduler.runAsynchronously(action, () -> {
    });
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
  }
}
