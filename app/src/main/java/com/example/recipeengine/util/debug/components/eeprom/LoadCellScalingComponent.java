package com.example.recipeengine.util.debug.components.eeprom;

import static com.example.recipeengine.util.debug.NonVolatileKeys.LOAD_CELL_FACTOR;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentLoadCellScalingEepromBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.ValueSaver;
import com.example.recipeengine.util.debug.components.DebugComponent;

/**
 * Load cell Scaling factor
 *
 * @author Abarajithan
 */
public final class LoadCellScalingComponent extends DebugComponent<ComponentLoadCellScalingEepromBinding> {
  private final ValueSaver valueSaver;

  public LoadCellScalingComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
    this.valueSaver = viewModel.getValueSaver();
  }

  @Override
  public ComponentLoadCellScalingEepromBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentLoadCellScalingEepromBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentLoadCellScalingEepromBinding binding) {
    binding.set.setOnClickListener(v -> {
      try {
        final double scalingFactor = Double.parseDouble(
          binding.scalingFactor.getText().toString());
        setScalingFactor(scalingFactor);
      } catch (NumberFormatException e) {
        Toast.makeText(getContext(), "Wrong format", Toast.LENGTH_LONG);
      }
    });
    binding.read.setOnClickListener(v -> readEeprom());
  }

  private void readEeprom() {
    final String value = this.valueSaver.getValue(LOAD_CELL_FACTOR.name());
    if (null != value) {
      this.setStatus(value);
    } else {
      this.setStatus("No value saved !!");
    }
  }

  private void setScalingFactor(final double factor) {
    this.valueSaver.setKey(LOAD_CELL_FACTOR.name(), Double.valueOf(factor).toString());
    //NativeUtil.setLoadCellScalingFactor(factor);
  }

  private void setStatus(@NonNull final String message) {
    this.getBinding().statusText.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }
}
