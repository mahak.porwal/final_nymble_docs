package com.example.recipeengine.util.debug.components.commands;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import com.example.recipeengine.databinding.ComponentHeatCommandsBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

/**
 * Record Thermal toggle &
 * Power Level Set
 *
 * @author Abarajithan
 */
public final class HeatCommandComponent extends DebugComponent<ComponentHeatCommandsBinding> {

    public HeatCommandComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentHeatCommandsBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentHeatCommandsBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentHeatCommandsBinding binding) {/*
        binding.recordThermal.setChecked(AppPrefs.getBoolean(AppPrefs.KEY_DEBUG_RECORD_THERMAL, false));
        binding.recordThermal.setOnCheckedChangeListener((b, c) -> {
            AppPrefs.save(AppPrefs.KEY_DEBUG_RECORD_THERMAL, c);
            toggleRecordThermal(c);
        });
        binding.start.setOnClickListener(v -> {
            final int powerLevel = Integer.parseInt(binding.powerLevel.getText().toString());
            setPowerLevel(powerLevel);
        });
        binding.stop.setOnClickListener(v -> setPowerLevel(0));*/
        binding.start.setOnClickListener(v -> {
            final int powerLevel = Integer.parseInt(binding.powerLevel.getText().toString());
            setPowerLevel(powerLevel);
        });
        binding.stop.setOnClickListener(v -> setPowerLevel(0));
    }

    private void setPowerLevel(final int level) {
        if (level < 0 || level > 7) {
            Toast.makeText(getContext(), "Invalid power level", Toast.LENGTH_SHORT).show();
            return;
        }
        DebugGuiMethods.setInductionPowerLevel(level);
       /* if (level < 0 || level > 7) {
            Toast.makeText(getContext(), "Invalid power level", Toast.LENGTH_SHORT).show();
            return;
        }

        sendFanMessage(55);
        sendIndFanMessage(1);

        final Message message = new Message(Board.INDUCTION);
        message.setType(Constants.MpuMcuAction);
        message.setSubSystem(Constants.HeatingSystem);
        message.setId(level + Constants.Set_Power_Level_0);
        Timber.d("Sensor Induction: %d", level);

        if (getViewModel().getExhaustFanState().get()) {
            add(Observable.just(1)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(integer -> {
                        enableButtons(false);
                        sendFanMessage(55);
                    })
                    .concatMap(l -> Observable.just(1).delay(1, TimeUnit.SECONDS))
                    .doOnNext(l -> sendIndFanMessage(1))
                    .concatMap(l -> Observable.just(1).delay(1, TimeUnit.SECONDS))
                    .doOnNext(l -> getViewModel().sendToHardware(message))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(l -> {
                        enableButtons(true);
                        getViewModel().getExhaustFanState().set(true);
                    }));
        } else {
            getViewModel().sendToHardware(message);
        }
        final LogDetail logDetail = new LogDetail(LoggingEvents.WATTAGE_CHANGE,
                TriggerEvents.MANUAL, Constants.nameOf(message.getId()));
        getViewModel().visionController().logDetail(logDetail);*/
    }

    private void enableButtons(final boolean enable) {
        getBinding().start.setEnabled(enable);
        getBinding().stop.setEnabled(enable);
    }

    /**
     * Send ExFan message to MCU
     *
     * @param primary the primary value
     */
    private void sendFanMessage(final int primary) {
        /*final Message fanMessage = new Message(Board.CONTROL);
        fanMessage.setType(Constants.MpuMcuCommand);
        fanMessage.setSubSystem(Constants.Micro);
        fanMessage.setId(Constants.Exhaust_Fan);
        fanMessage.setValue(Collections.singletonMap(String.valueOf(Constants.Primary), primary));

        getViewModel().sendToHardware(fanMessage);*/
    }

    /**
     * Send Induction fan message to MCU
     *
     * @param primary the primary value
     */
    private void sendIndFanMessage(final int primary) {
        /*final Message indFanMessage = new Message(Board.CONTROL);
        indFanMessage.setType(Constants.MpuMcuCommand);
        indFanMessage.setSubSystem(Constants.Micro);
        indFanMessage.setId(Constants.Induction_Fan);
        indFanMessage.setValue(Collections.singletonMap(String.valueOf(Constants.Primary), primary));

        getViewModel().sendToHardware(indFanMessage);*/
    }

    /**
     * Toggle thermal video recording
     *
     * @param record true to record the thermal video
     */
    private void toggleRecordThermal(final boolean record) {
       /* if (record) {
            getViewModel().visionController().startVideoCapture();
            final LogDetail logDetail = new LogDetail(LoggingEvents.VIDEO_START, TriggerEvents.MANUAL);
            getViewModel().visionController().logDetail(logDetail);
        } else {
            getViewModel().visionController().stopVideoCapture();
        }*/
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {

    }
}
