package com.example.recipeengine.util.debug.components.macro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentLoadCellMacroBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import io.reactivex.disposables.Disposable;

/**
 * Load cell value, with freq control. Choice of grams or uV
 *
 * @author Abarajithan
 */
public final class MacroLoadCellComponent extends DebugComponent<ComponentLoadCellMacroBinding> {

  private static final String[] UNITS = {"Grams", "uV"};
  private Disposable disposable;

  public MacroLoadCellComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentLoadCellMacroBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentLoadCellMacroBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentLoadCellMacroBinding binding) {
    binding.units.setAdapter(new SpinnerAdapter(getContext(), UNITS));
    binding.start.setOnClickListener(v -> {
      final int unitIndex = binding.units.getSelectedItemPosition();
      final int freq = Integer.parseInt(binding.frequency.getText().toString());
      start(unitIndex, freq);
    });
    binding.stop.setOnClickListener(v -> {
      final int unitIndex = binding.units.getSelectedItemPosition();
      stop(unitIndex);
    });
  }

  private void start(final int unitIndex, final int freq) {
    getBinding().start.setEnabled(false);
    getBinding().stop.setEnabled(true);
    final Supplier<Float> supplier;
    switch (UNITS[unitIndex]) {
      case "Grams":
        supplier = DebugGuiMethods::getLoadCellReading_Grams;
        break;
      case "uV":
        supplier = DebugGuiMethods::getLoadCellReading_MicroVolts;
        break;
      default:
        return;
    }
    final int periodInMilliSecond = 1000 / freq;
    this.disposable = FrequencyScheduler.scheduleAPeriodicPollAndConsumeOnMainThread(
      periodInMilliSecond, supplier, this::onResponse);
  }

  private void stop(final int unitIndex) {
    getBinding().start.setEnabled(true);
    getBinding().stop.setEnabled(false);
    this.disposable.dispose();
  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
    this.setStatus(message);
        /*final Double sensorValue = message.getDoubleKeyValue(Constants.Primary);
        this.setStatus(String.valueOf(sensorValue));*/
  }
}
