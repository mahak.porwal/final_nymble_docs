package com.example.recipeengine.util;

import org.mariuszgromada.math.mxparser.Expression;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class MxEquationResolver implements EquationResolver {


  public static boolean setResolvedValue(String parametricField, String resolvedValue, Object object) {
    //use reflection to set the parametric field with the resolvedValue
    //find in which the parametric field resides
    final Field[] fields = object.getClass().getDeclaredFields();
    for (Field field :
      fields) {
      String fieldName = field.getName();
      if (fieldName.compareTo(parametricField) == 0) {
        try {
          field.setAccessible(true);
          field.set(object, resolvedValue);
          return true;
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        }
      }
    }
    return false;
  }

  public static boolean isResolvedExpression(String expression) {
    Expression expression1 = new Expression(expression);
    if (expression1.getMissingUserDefinedArguments().length == 0) {
      return true;
    }
    return false;
  }

  private Map<String, Expression> parametricFields;

  public MxEquationResolver() {
    this.parametricFields = new HashMap<>();
  }

  @Override
  public void setParametricField(String fieldName, String expression) {
    if (!this.parametricFields.containsKey(fieldName)) {
      Expression expression1 = new Expression(expression);
      this.parametricFields.put(fieldName, expression1);
    }
  }

  @Override
  public boolean isFieldResolved(String fieldName) {
    if (parametricFields.containsKey(fieldName) == true) {
      Expression eh = parametricFields.get(fieldName);
      if (eh.getMissingUserDefinedArguments().length == 0) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void resolveParametricField(String fieldName, Map<String, String> parametersMap) {
    Expression eh = null;
    if (parametricFields.containsKey(fieldName) == true) {
      eh = parametricFields.get(fieldName);
      String[] constants = eh.getMissingUserDefinedArguments();
      for (String constant :
        constants) {
        if (parametersMap.containsKey(constant) == true) {
          eh.defineConstant(constant, Double.valueOf(parametersMap.get(constant)));
        }
      }
    }
  }

  @Override
  public Double getResolvedValue(String fieldName) {
    Double resolvedValue = Double.NaN;
    if (this.parametricFields.containsKey(fieldName) == true) {
      Expression expression = this.parametricFields.get(fieldName);
      resolvedValue = expression.calculate();
    }
    return resolvedValue;
  }

}
