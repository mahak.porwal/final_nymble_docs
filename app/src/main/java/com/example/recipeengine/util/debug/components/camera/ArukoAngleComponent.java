package com.example.recipeengine.util.debug.components.camera;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentArukoAngleCameraBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.Optional;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Aruko marker detection - Display angle
 *
 * @author Abarajithan
 */
public final class ArukoAngleComponent extends DebugComponent<ComponentArukoAngleCameraBinding> {

    public ArukoAngleComponent(@NonNull final Context context, @NonNull final DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentArukoAngleCameraBinding onInflateView(@NonNull final LayoutInflater inflater, @NonNull final ViewGroup parent) {
        return ComponentArukoAngleCameraBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull final ComponentArukoAngleCameraBinding binding) {
        binding.displayAngle.setOnClickListener(v -> displayAngle());
    }

    private void displayAngle() {/*
        setStatus("");
        enableButton(false);
        add(getViewModel().visionController().getArucoAngleForDebug()
                .doOnSubscribe(l->getViewModel().visionController().getArucoAngle())
                .filter(Optional::isPresent)
                .take(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(angle -> {
                    setStatus(String.format("Degrees: %f", angle.get()));
                    enableButton(true);
                }));*/
    }

    private void enableButton(final boolean enable) {
        getBinding().displayAngle.setEnabled(enable);
    }

    private void setStatus(@NonNull final String message) {
        getBinding().status.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(final String message) {

    }
}
