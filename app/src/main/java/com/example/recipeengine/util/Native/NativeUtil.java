package com.example.recipeengine.util.Native;


import com.example.recipeengine.instruction.handler.cook.temperature.TemperatureParams;

import java.util.HashMap;
import java.util.Map;

import som.hardware.request.HWRequest;
import som.hardware.response.HWActionResponse;
import som.hardware.response.SensorResponse;

public class NativeUtil {

  static {
    System.loadLibrary("native-nymble");
  }

  public static native void StartHardwareService();

  public static native void StartDummyHardwareService();

  public static native void StopHardwareService();

  public static native HWActionResponse ExecuteHWActionRequest(HWRequest actionRequest);

  public static native SensorResponse ExecuteSensorRequest(HWRequest sensorRequest);

  public static native HashMap ScanI2CBus(int bus);

  public static native void StartInductionControl();
  public static native void StopInductionControl();

  public static native void InitThermalCamera();

  public static native void SetInductionControlInput(String controlInput);

  public static native void CreateThermalImage(double[] temperatureMatrix);

  public static native void PopulateThermalImage(long address);

  public static native TemperatureParams CalculateFoodTemperature(long address);

  public static native TemperatureParams CalculatePanTemperature(long address);

  public static native void setLoadCellScalingFactor(double scalingFactor);

  public static native double getLoadCellScalingFactor();

  public static native void populateHardwareFactors(Map<String, String> hardwareFactors);

}