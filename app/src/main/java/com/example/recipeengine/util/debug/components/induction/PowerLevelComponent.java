package com.example.recipeengine.util.debug.components.induction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentPowerLevelBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * Induction Power Level controls
 *
 * @author Abarajithan
 */
public final class PowerLevelComponent extends DebugComponent<ComponentPowerLevelBinding> {

    private final static int EXHAUST_FAN_FULL_SPEED = 50;
    public final static int INDUCTION_FAN_FULL_SPEED = 99;

    public PowerLevelComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentPowerLevelBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentPowerLevelBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentPowerLevelBinding binding) {
        binding.start.setOnClickListener(v -> {
            final int powerLevel = Integer.parseInt(binding.powerLevel.getText().toString());
            setPowerLevel(powerLevel);
        });
        binding.stop.setOnClickListener(v -> setPowerLevel(0));
    }

    private void setPowerLevel(final int level) {
        if (level < 0 || level > 7) {
            Toast.makeText(getContext(), "Invalid power level", Toast.LENGTH_SHORT).show();
            return;
        }
        DebugGuiMethods.setInductionPowerLevel(level);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {
    }
}
