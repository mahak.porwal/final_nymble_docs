package com.example.recipeengine.util;

import android.icu.text.SimpleDateFormat;
import android.os.Environment;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.Date;
import java.util.Locale;

public class FileUtil {
  private static final String BASE_DIR = "julia_files";
  public static final String LOGS_DIR = "logs";
  private static final String VIDEO_DIR = "videos";
  private static final String MODELS_DIR = "models";
  private static final String CONFIG_DIR = "configs";
  private static final String PLAYBACK_DIR = "playbacks";
  private static final String OTA_DIR = "ota_updates";
  public static final String TEMPLATES_DIR = "templates";
  private static final String MASK_TEMP_DIR = "mask_temp_matrix"; // Also for images session dir
  private static final String DRY_WET_DIR = "dry_wet_model";
  private static final String CONSISTENCY_DIR = "consistency_model";
  private static final String MACRO_SIZE_DIR = "macro_size_model";

  /* Cooking logs */
  public static final String COOKING_LOGS_DIR = "cooking_logs";
  public static final String COOKING_IMAGES_DIR = "cooking_images";
  private static final String SESSION_DIR_PREFIX = "session_";

  // log and playback files
  public static final String LOG_FILE_PREFIX = "logs_";
  public static final String LOG_FILE_EXT = ".log";
  public static final String PLAYBACK_FILE_PREFIX = "playback_";
  public static final String PLAYBACK_FILE_EXT = ".txt";

  // Image Dirs
  private static final String INFER_IMAGES_DIR = "infer";
  private static final String PROGRESS_IMAGES_DIR = "cooking_progress_images";
  private static final String PROGRESS_VIDEOS_DIR = "cooking_progress_videos";
  private static final String STIRRER_DETECT_DIR = "stirrer_detect";
  private static final String DISPENSE_IMAGES_DIR = "dispense_images";
  private static final String BLUR_MASK_DIR = "blur_detect";

  // Data files
  private static final String MASK_TEMP_FILE = "mask_temp.csv";
  private static final String SESSION_DEBUG_DATA = "debug_data";
  private static final String EVENT_LOG_FILE = "event_log.json";
  private static final String TEMP_MATRIX_FILE = "temp_data.csv";
  private static final String ARUCO_RETRY_FILE = "stir_data.csv";
  private static final String SENSOR_LOG_FILE = "sensor_data.csv";
  private static final String TEMP_CONTROL_FILE = "temperature_control.csv";

  private static final String CONFIG_FILE = "config.json";
  private static final String OTA_FILE = "ota_update.apk";

  public static final String CHECKS_FILE = "checks.json";
  public static final String THERMAL_DATA_FILE = "thermal_data.json";
  public static final String INDUCTION_PARAMS = "induction_params.json";
  public static final String CAMERA_PARAMS = "camera_params.json";

  private static String debugSessionId = "debugSession";

  static {
    final SimpleDateFormat format = new SimpleDateFormat("ddMMyy-hhmm", Locale.getDefault());
    debugSessionId = format.format(new Date());
  }

  /**
   * Get the base directory.
   */
  public static File baseDir() {
    File baseDir = new File(Environment.getExternalStorageDirectory(), BASE_DIR);
    if (!baseDir.exists()) {
      baseDir.mkdirs();
    }
    return baseDir;
  }

  /**
   * Get a file from the base-dir/dir.
   * <p>
   * Should be called only if base directory is the root directory.
   * For child directories, use their respective methods.
   */
  public static File getAsFile(String dir, String name) {
    final File parentDir = new File(baseDir(), dir);
    if (!parentDir.exists()) {
      parentDir.mkdirs();
    }
    return new File(parentDir, name);
  }

  public static File getLogFile(@NonNull final String sessionId) {
    return getAsFile(LOGS_DIR, LOG_FILE_PREFIX + sessionId + LOG_FILE_EXT);
  }

  public static File getTemplateFile(String filename) {
    return getAsFile(TEMPLATES_DIR, filename);
  }

  public static File getVideoFile(String filename) {
    return getAsFile(VIDEO_DIR, filename);
  }

  public static File getChecksFile() {
    return new File(baseDir(), CHECKS_FILE);
  }

  public static File getModelFile(String filename) {
    return getAsFile(MODELS_DIR, filename);
  }

  /**
   * Get Thermal data file at {@link #CONFIG_DIR}/{@link #THERMAL_DATA_FILE}
   */
  public static File getThermalData() {
    return getAsFile(CONFIG_DIR, THERMAL_DATA_FILE);
  }

  /**
   * Get Induction params file at {@link #CONFIG_DIR}/{@link #INDUCTION_PARAMS}
   */
  public static File getInductionParams() {
    return getAsFile(CONFIG_DIR, INDUCTION_PARAMS);
  }

  /**
   * Get Camera params file at {@link #CONFIG_DIR}/{@link #CAMERA_PARAMS}
   */
  public static File getCameraParams() {
    return getAsFile(CONFIG_DIR, CAMERA_PARAMS);
  }

  /**
   * Get Config file at {@link #CONFIG_DIR}/{@link #CONFIG_FILE}
   */
  public static File getConfigFile() {
    return getAsFile(CONFIG_DIR, CONFIG_FILE);
  }

  public static File getOtaFile() {
    return getAsFile(OTA_DIR, OTA_FILE);
  }

  /**
   * Count all files in base-dir/dir.
   */
  public static int countFiles(String dir) {
    File file = new File(baseDir(), dir);
    return file.listFiles().length;
  }

  /**
   * Get Recipe Playback file at {@link #PLAYBACK_DIR}
   */
  public static File getPlaybackFile(@NonNull final String sessionId) {
    return getAsFile(PLAYBACK_DIR, PLAYBACK_FILE_PREFIX + sessionId + PLAYBACK_FILE_EXT);
  }

  // Cooking logs

  /**
   * Get the session directory suffixed by the cooking session ID.
   *
   * @param sessionId the cooking session id
   * @return the session directory
   */
  public static File getCookingSessionDir(@NonNull final String sessionId) {
    final File sessionDir = getAsFile(COOKING_LOGS_DIR, SESSION_DIR_PREFIX + sessionId);
    if (!sessionDir.exists()) {
      sessionDir.mkdirs();
    }
    return sessionDir;
  }

  /**
   * Get the image session directory suffixed by the cooking session ID.
   *
   * @param sessionId the cooking session id
   * @return the image session directory
   */
  public static File getCookingSessionImageDir(@NonNull final String sessionId) {
    final File imageSessionDir = getAsFile(COOKING_IMAGES_DIR, SESSION_DIR_PREFIX + sessionId);
    if (!imageSessionDir.exists()) {
      imageSessionDir.mkdirs();
    }
    return imageSessionDir;
  }

  /**
   * Get the video session directory suffixed by the cooking session ID.
   *
   * @param sessionId the cooking session id
   * @return the video session directory
   */
  public static File getCookingSessionVideoDir(@NonNull final String sessionId) {
    final File videoSessionDir = getAsFile(PROGRESS_VIDEOS_DIR, SESSION_DIR_PREFIX + sessionId);
    if (!videoSessionDir.exists()) {
      videoSessionDir.mkdirs();
    }
    return videoSessionDir;
  }

  /**
   * Get the session directory suffixed by the Infer images ID.
   *
   * @param inferId the cooking images id
   * @return the Infer directory
   */
  public static File getInferImageFile(@NonNull final String inferId) {
    return _getImageSessionChildFile(INFER_IMAGES_DIR, inferId);
  }

  /**
   * Get the session directory suffixed by the Infer images ID.
   *
   * @param filename the cooking images id
   * @return the image saving directory
   */
  public static File getProgressImageFile(@NonNull final String filename) {
    return _getImageSessionChildFile(PROGRESS_IMAGES_DIR, filename);
  }

  /**
   * Get the blur detect directory suffixed by the blur detected and mask images ID.
   *
   * @param filename -> Image ID
   * @return the image saving directory
   */
  public static File getBlurMaskImageFile(@NonNull final String filename) {
    return _getImageSessionChildFile(BLUR_MASK_DIR, filename);
  }


  /**
   * Get the dispense image {@link File} object, created from the filename.
   * The format of the filename would be
   * <TimeOfAction>_<Action>_<Ingredient_Names>_<Instance_Number>.jpeg
   *
   * @param filename the cooking images id
   * @return the image saving directory
   */
  public static File getDispenseImageFile(@NonNull final String filename) {
    final File dispenseImageDirectory = getProgressImageFile(DISPENSE_IMAGES_DIR);
    if (!dispenseImageDirectory.exists()) {
      dispenseImageDirectory.mkdirs();
    }
    return new File(dispenseImageDirectory, filename);
  }

  /**
   * Get the session directory from session id preferences.
   * <p>
   * This will be called either after cooking session,
   * or when using debugger.
   *
   * @return the session directory
   */
  private static File getCookingSessionDir() {
    final String sessionId = AppPrefs.getString(AppPrefs.KEY_COOKING_SESSION_ID, debugSessionId);
    if (sessionId == null) {
      throw new IllegalStateException("Session ID not available");
    }
    return getCookingSessionDir(sessionId);
  }

  /**
   * Get the images session directory from session id preferences.
   * <p>
   * This will be called either after cooking session,
   * or when using debugger.
   *
   * @return the image session directory
   */
  private static File getCookingSessionImageDir() {
    final String sessionId = AppPrefs.getString(AppPrefs.KEY_COOKING_SESSION_ID, debugSessionId);
    if (sessionId == null) {
      throw new IllegalStateException("Session ID not available");
    }
    return getCookingSessionImageDir(sessionId);
  }

  /**
   * Get the videos session directory from session id preferences.
   *
   * @return the video session directory
   */
  private static File getCookingSessionVideoDir() {
    final String sessionId = AppPrefs.getString(AppPrefs.KEY_COOKING_SESSION_ID, debugSessionId);
    if (sessionId == null) {
      throw new IllegalStateException("Session ID not available");
    }
    return getCookingSessionVideoDir(sessionId);
  }

  /**
   * Get the session file
   *
   * @param filename the name of the session file
   */
  private static File _getSessionFile(@NonNull final String filename) {
    return new File(getCookingSessionDir(), filename);
  }

  /**
   * Get the corresponding session file respective to the session child folder.
   *
   * @param childDir the directory inside {@link #COOKING_LOGS_DIR} directory.
   * @param filename the file inside the {@param childDir} directory.
   */
  private static File _getSessionChildFile(
    @NonNull final String childDir,
    @NonNull final String filename
  ) {
    final File sessionDir = new File(getCookingSessionDir(), childDir);
    if (!sessionDir.exists()) {
      sessionDir.mkdirs();
    }
    return new File(sessionDir, filename);
  }

  /**
   * Get the corresponding image session file respective to the image session child folder.
   *
   * @param childDir the directory inside {@link #COOKING_IMAGES_DIR} directory.
   * @param filename the file inside the {@param childDir} directory.
   */
  private static File _getImageSessionChildFile(
    @NonNull final String childDir,
    @NonNull final String filename
  ) {
    final File imageSessionDir = new File(getCookingSessionImageDir(), childDir);
    if (!imageSessionDir.exists()) {
      imageSessionDir.mkdirs();
    }
    return new File(imageSessionDir, filename);
  }

  /**
   * Get the corresponding video session file.
   *
   * @param filename the file inside the {@link #PROGRESS_VIDEOS_DIR} video directory.
   */
  public static File getVideoSessionFile(
    @NonNull final String filename
  ) {
    final File videoSessionDir = getCookingSessionVideoDir();
    return new File(videoSessionDir, filename);
  }

  /**
   * Get the mask file from {@link #MASK_TEMP_DIR} directory.
   *
   * @param filename the name of the file
   */
  public static File getMaskFile(@NonNull final String filename) {
    return _getImageSessionChildFile(MASK_TEMP_DIR, filename);
  }

  /**
   * Get the Stirrer detect file from {@link #STIRRER_DETECT_DIR} directory.
   *
   * @param filename the name of the file
   */
  public static File getStirrerDetectImageFile(@NonNull final String filename) {
    return _getImageSessionChildFile(STIRRER_DETECT_DIR, filename);
  }

  /**
   * Get the debug data file from {@link #SESSION_DEBUG_DATA} directory.
   *
   * @param filename the name of the file
   */
  public static File getDebugDataFile(@NonNull final String filename) {
    return _getSessionChildFile(SESSION_DEBUG_DATA, filename);
  }

  /**
   * Get the aruko retry file from current session directory.
   */
  public static File getArukoRetryFile() {
    return _getSessionFile(ARUCO_RETRY_FILE);
  }

  /**
   * Get the temp matrix file from current session directory.
   */
  public static File getTempMatrixFile() {
    return _getSessionFile(TEMP_MATRIX_FILE);
  }

  /**
   * Get the event log file from current session directory.
   */
  public static File getEventLogFile() {
    return _getSessionFile(EVENT_LOG_FILE);
  }

  /**
   * Get the session log file from current session directory.
   */
  public static File getSensorLogFile() {
    return _getSessionFile(SENSOR_LOG_FILE);
  }

  /**
   * Get the temp control data file from current session directory.
   */
  public static File getTempControlDataFile() {
    return _getSessionFile(TEMP_CONTROL_FILE);
  }

  /**
   * Get the mask file from current session directory.
   */
  public static File getMaskTempFile() {
    return _getSessionChildFile(MASK_TEMP_DIR, MASK_TEMP_FILE);
  }

  /**
   * Get the dry wet detect file from {@link #DRY_WET_DIR} directory.
   *
   * @param filename the name of the file
   */
  public static File getDryWetImageFile(@NonNull final String filename) {
    return _getImageSessionChildFile(DRY_WET_DIR, filename);
  }

  public static File getConsistencyImageFile(@NonNull final String filename) {
    return _getImageSessionChildFile(CONSISTENCY_DIR, filename);
  }

  public static File getMacroSizeImageFile(@NonNull final String filename) {
    return _getImageSessionChildFile(MACRO_SIZE_DIR, filename);
  }

}