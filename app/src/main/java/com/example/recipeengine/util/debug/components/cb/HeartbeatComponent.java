package com.example.recipeengine.util.debug.components.cb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentHeartbeatCbBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

/**
 * Debug component for checking MCU Heartbeat
 *
 * @author Abarajithan
 */
public final class HeartbeatComponent extends DebugComponent<ComponentHeartbeatCbBinding> {

    private ComponentHeartbeatCbBinding binding;

    public HeartbeatComponent(
            @NonNull final Context context,
            @NonNull final DebugViewModel viewModel
    ) {
        super(context, viewModel);
    }

    @Override
    public ComponentHeartbeatCbBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentHeartbeatCbBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentHeartbeatCbBinding binding) {
        this.binding = binding;
        binding.heartbeat.setOnClickListener(v -> this.heartBeatCheck());
    }

    private void heartBeatCheck() {
        /*final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuRequest);
        message.setSubSystem(Constants.ControlBoard);
        message.setId(Constants.HeartBeat);
        this.binding.statusText.setText("");
        getViewModel().sendToHardware(message);*/
    }

    @Override
    public void onBindView() {
        //
    }

    @Override
    public void onResponse(String message) {
        /*if (Constants.HeartBeat == message.getId()) {
            this.binding.statusText.setText("CB Good");
        }*/
    }
}
