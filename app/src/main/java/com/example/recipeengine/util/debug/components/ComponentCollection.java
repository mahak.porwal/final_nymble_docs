package com.example.recipeengine.util.debug.components;

import com.example.recipeengine.util.debug.components.cb.HeartbeatComponent;
import com.example.recipeengine.util.debug.components.cb.InputPinsCbComponent;
import com.example.recipeengine.util.debug.components.cb.OutputPinsCbComponent;
import com.example.recipeengine.util.debug.components.cb.ResetCbComponent;


import com.example.recipeengine.util.debug.components.camera.ArukoAngleComponent;
import com.example.recipeengine.util.debug.components.camera.StirrerTestComponent;
import com.example.recipeengine.util.debug.components.camera.ThermalCaptureComponent;
import com.example.recipeengine.util.debug.components.cb.HeartbeatComponent;
import com.example.recipeengine.util.debug.components.cb.I2cCbComponent;
import com.example.recipeengine.util.debug.components.cb.InputPinsCbComponent;
import com.example.recipeengine.util.debug.components.cb.OutputPinsCbComponent;
import com.example.recipeengine.util.debug.components.cb.ResetCbComponent;
import com.example.recipeengine.util.debug.components.commands.HeatCommandComponent;
import com.example.recipeengine.util.debug.components.commands.LiqStirCommandComponent;
import com.example.recipeengine.util.debug.components.commands.MacroCommandComponent;
import com.example.recipeengine.util.debug.components.commands.MicroCommandComponent;
import com.example.recipeengine.util.debug.components.eeprom.LoadCellScalingComponent;
import com.example.recipeengine.util.debug.components.eeprom.MacroHomingComponent;
import com.example.recipeengine.util.debug.components.eeprom.OilPumpFactorComponent;
import com.example.recipeengine.util.debug.components.eeprom.PcaFactorComponent;
import com.example.recipeengine.util.debug.components.eeprom.WaterPumpFactorComponent;
import com.example.recipeengine.util.debug.components.induction.DacInductionComponent;
import com.example.recipeengine.util.debug.components.induction.InputPinsInductionComponent;
import com.example.recipeengine.util.debug.components.induction.OutputPinsInductionComponent;
import com.example.recipeengine.util.debug.components.induction.PanDetectComponent;
import com.example.recipeengine.util.debug.components.induction.PowerLevelComponent;
import com.example.recipeengine.util.debug.components.induction.SensorInductionComponent;
import com.example.recipeengine.util.debug.components.ledBuzzer.BuzzerControlComponent;
import com.example.recipeengine.util.debug.components.ledBuzzer.IlluminationLedComponent;
import com.example.recipeengine.util.debug.components.ledBuzzer.LedControlComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.ExFanComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.FlowMeterComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.HeatingElementComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.LiqComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.LiqLoadCellComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.StirComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.StirFBComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.TachometerComponent;
import com.example.recipeengine.util.debug.components.liqStirFan.TemperatureComponent;
import com.example.recipeengine.util.debug.components.macro.MacroLifterComponent;
import com.example.recipeengine.util.debug.components.macro.MacroLoadCellComponent;
import com.example.recipeengine.util.debug.components.macro.MacroLoadSensorComponent;
import com.example.recipeengine.util.debug.components.macro.MotorRotationComponent;
import com.example.recipeengine.util.debug.components.micro.PodAngleComponent;
import com.example.recipeengine.util.debug.components.micro.PodHomingComponent;
import com.example.recipeengine.util.debug.components.micro.PodRotationComponent;
import com.example.recipeengine.util.debug.components.system.CurrentSystemComponent;
import com.example.recipeengine.util.debug.components.system.I2cSystemComponent;
import com.example.recipeengine.util.debug.components.system.InputIna300ssSystemComponent;
import com.example.recipeengine.util.debug.components.system.Lm2678SystemComponent;
import com.example.recipeengine.util.debug.components.system.OutputIna300ssSystemComponent;
import com.example.recipeengine.util.debug.components.system.PcbTempSystemComponent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to register all the components
 *
 * @author Abarajithan
 */
public final class ComponentCollection {

  // CB
  public static final int ID_CB_MAIN = 101;
  public static final int ID_CB_GPIO = 102;
  public static final int ID_CB_I2C_MCU = 103;
  // System
  public static final int ID_SYSTEM_I2C = 201;
  public static final int ID_SYSTEM_TEMP_CURR = 202;
  public static final int ID_SYSTEM_GPIO = 203;
  // Camera
  public static final int ID_CAMERA_ARUKO_MARKER = 301;
  public static final int ID_CAMERA_THERMAL = 302;
  // Stir, Liq & Fan
  public static final int ID_STIR_LIQ_FAN_STIR = 401;
  public static final int ID_STIR_LIQ_FAN_LIQ = 402;
  public static final int ID_STIR_LIQ_FAN_FAN = 403;
  public static final int ID_STIR_LIQ_FAN_HEAT_ELEMENT = 404;
  // Induction
  public static final int ID_INDUCTION_MAIN = 501;
  public static final int ID_INDUCTION_GPIO = 502;
  // Macro
  public static final int ID_MACRO_MOTOR = 601;
  public static final int ID_MACRO_LOAD_CELL = 602;
  // Micro
  public static final int ID_MICRO_MAIN = 701;
  public static final int ID_MICRO_HOMING = 702;
  // Commands
  public static final int ID_COMMANDS_HEAT = 801;
  public static final int ID_COMMANDS_MICRO = 802;
  public static final int ID_COMMANDS_MACRO = 803;
  public static final int ID_COMMANDS_LIQ_STIR = 804;
  // LED Buzzer
  public static final int ID_LED_BUZZER_MAIN = 901;
  // EEPROM
  public static final int ID_EEPROM_LOAD_CELL = 1001;
  public static final int ID_EEPROM_LIQUID = 1002;
  public static final int ID_EEPROM_PCA = 1003;
  public static final int ID_EEPROM_MACRO = 1004;
  // Device Controls
  public static final int ID_DEVICE_CONTROLS_MAIN = 1101;

  private static final Map<Integer, List<Class<? extends DebugComponent>>> COMPONENTS
    = new HashMap<>();

  public static void init() {
    register(ID_CB_MAIN, HeartbeatComponent.class, ResetCbComponent.class);
    register(ID_CB_GPIO, InputPinsCbComponent.class, OutputPinsCbComponent.class);
    register(ID_CB_I2C_MCU, I2cCbComponent.class);
    //
    register(ID_SYSTEM_I2C, I2cSystemComponent.class);
    register(ID_SYSTEM_TEMP_CURR, CurrentSystemComponent.class);
    register(ID_SYSTEM_GPIO, Lm2678SystemComponent.class,
      InputIna300ssSystemComponent.class, OutputIna300ssSystemComponent.class);
    //
    register(ID_CAMERA_ARUKO_MARKER, ArukoAngleComponent.class, StirrerTestComponent.class);
    register(ID_CAMERA_THERMAL, ThermalCaptureComponent.class);
    //
    register(ID_STIR_LIQ_FAN_STIR, StirComponent.class, StirFBComponent.class);
    register(ID_STIR_LIQ_FAN_LIQ, LiqComponent.class, LiqLoadCellComponent.class, FlowMeterComponent.class);
    register(ID_STIR_LIQ_FAN_FAN, ExFanComponent.class);
    register(ID_STIR_LIQ_FAN_HEAT_ELEMENT, LiqComponent.class, HeatingElementComponent.class,
      TemperatureComponent.class);
    //
    register(ID_INDUCTION_MAIN, SensorInductionComponent.class, PowerLevelComponent.class,
      PanDetectComponent.class);
    register(ID_INDUCTION_GPIO, InputPinsInductionComponent.class,
      OutputPinsInductionComponent.class, DacInductionComponent.class);
    //
    register(ID_MACRO_MOTOR, MotorRotationComponent.class, MacroLifterComponent.class);
    register(ID_MACRO_LOAD_CELL, MacroLoadCellComponent.class, MacroLoadSensorComponent.class);
    //
    register(ID_MICRO_MAIN, PodRotationComponent.class, PodAngleComponent.class);
    register(ID_MICRO_HOMING, PodHomingComponent.class);
    //
    register(ID_COMMANDS_HEAT, HeatCommandComponent.class);
    register(ID_COMMANDS_MICRO, MicroCommandComponent.class);
    register(ID_COMMANDS_MACRO, MacroCommandComponent.class, LiqLoadCellComponent.class);
    register(ID_COMMANDS_LIQ_STIR, StirComponent.class, LiqStirCommandComponent.class);
    //
    register(ID_LED_BUZZER_MAIN, LedControlComponent.class, BuzzerControlComponent.class,
      IlluminationLedComponent.class);
    //
    register(ID_EEPROM_LOAD_CELL, LoadCellScalingComponent.class);
    register(ID_EEPROM_LIQUID, OilPumpFactorComponent.class, WaterPumpFactorComponent.class);
    register(ID_EEPROM_PCA, PcaFactorComponent.class);
    register(ID_EEPROM_MACRO, MacroHomingComponent.class);
    //
    //register(ID_DEVICE_CONTROLS_MAIN, DeviceAdminPolicyComponent.class, RecipeCacheComponent.class);
  }

  public static List<Class<? extends DebugComponent>> getComponents(final int id) {
    return COMPONENTS.getOrDefault(id, new ArrayList<>());
  }

  private static void register(final int id, final Class<? extends DebugComponent>... components) {
    COMPONENTS.put(id, Arrays.asList(components));
  }

  private ComponentCollection() {
  }

}
