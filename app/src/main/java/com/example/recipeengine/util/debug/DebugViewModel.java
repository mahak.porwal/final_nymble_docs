package com.example.recipeengine.util.debug;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;

import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.util.ValueSaver;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import som.hardware.service.HWService;

/**
 * Common ViewModel class for built-in debugger
 *
 * @author Abarajithan
 */
public final class DebugViewModel extends ViewModel {

  private final AtomicInteger SEQ_NO = new AtomicInteger(0);
  private ValueSaver valueSaver;

    /*private final BaseHardwareService hardwareService;
    private final VisionController visionController;*/

  /**
   * The state of the exhause fan.
   * Accessible across all the debug components.
   */
  private final AtomicBoolean exhaustFanState = new AtomicBoolean(false);

  public DebugViewModel(@NonNull final RecipeContext recipeContext, @NonNull ValueSaver valueSaver) {
    this.valueSaver = valueSaver;
        /*this.hardwareService = Engine.getAppComponent().hardwareService();
        this.visionController = Engine.getAppComponent().newDebugModuleFactory()
                .create(recipeContext)
                .visionController();
        AppPrefs.save(AppPrefs.KEY_COOKING_SESSION_ID, recipeContext.getRecipeId());*/
  }

  /**
   * Initialize controllers
   */
  public void init(Context context) throws InterruptedException {
    HWService.startHwService(context);
  }

  /**
   * Get feedback messages from MCU.
   *
   * @return The Observable of messages
   */
    /*public Observable<Message> feedbackMessages() {
        return hardwareService.feedbackMessages()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }*/

  /**
   * Send a message to MCU.
   * The Sequence number is handled automatically.
   *
   * @param message The message to send
   */
    /*public void sendToHardware(@NonNull final Message message) {
        message.setSeqNo(SEQ_NO.incrementAndGet());
        hardwareService.sendToHardware(message);
    }*/

  /**
   * Send an ACK to MCU.
   *
   * @param message The ACK message
   */
    /*public void sendACK(@NonNull final Message message) {
        //hardwareService.sendToHardware(message);
    }*/

  /**
   * Detect the available I2C device.
   *
   * @param addr the address to check
   * @return -1 if device not found, else not -1
   */
    /*public int checkI2CDevice(final byte addr) {
        return hardwareService.detectI2CDevice(addr);
    }*/

  /**
   * Get the debug scoped VisionController
   *
   * @return VisionController instance
   */
    /*public VisionController visionController() {
        return this.visionController;
    }*/

  /**
   * Get the HardwareService
   *
   * @return HardwareService instance
   */
    /*public BaseHardwareService hardwareService() {
        return this.hardwareService;
    }*/

  /**
   * @return the exhaust fan state
   */
    /*public AtomicBoolean getExhaustFanState() {
        return this.exhaustFanState;
    }*/
  @Override
  protected void onCleared() {
    super.onCleared();
        /*this.visionController.close();
        // Remove the cooking session ID entry
        AppPrefs.remove(AppPrefs.KEY_COOKING_SESSION_ID);
        // Remove the debug thermal record
        AppPrefs.remove(AppPrefs.KEY_DEBUG_RECORD_THERMAL);*/
  }

  public ValueSaver getValueSaver() {
    return this.valueSaver;
  }
}
