package com.example.recipeengine.util.debug.components.cb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentOutputpinsCbBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * GPIO Output pin status
 *
 * @author Abarajithan
 */
public final class OutputPinsCbComponent extends DebugComponent<ComponentOutputpinsCbBinding> {

    private static final String[] OUTPUT_PINS = {"BOOT0", "BOOT1", "RESET407"};

    public OutputPinsCbComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentOutputpinsCbBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentOutputpinsCbBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentOutputpinsCbBinding binding) {
        binding.outputPins.setAdapter(new SpinnerAdapter(getContext(), OUTPUT_PINS));
        binding.high.setOnClickListener(v -> {
            final int pinIndex = binding.outputPins.getSelectedItemPosition();
            //sendGpioCommand(pinIndex,GPIOEnum.HIGH);
        });
        binding.low.setOnClickListener(v -> {
            final int pinIndex = binding.outputPins.getSelectedItemPosition();
            //sendGpioCommand(pinIndex,GPIOEnum.LOW);
        });
    }

    private void sendGpioCommand(final int pinIndex, int state) {
        /*final Message message = new Message(Board.MPU);
        message.setType(Constants.MpuMcuCommand);
        Map<String,Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary), state);
        message.setValue(value);
        switch (OUTPUT_PINS[pinIndex]){
            case "BOOT1":
                message.setId(Constants.Boot_1);
                break;
            case "BOOT0":
                message.setId(Constants.Boot_0);
                break;
            case "RESET407":
                message.setId(Constants.S_RESET_407);
                break;
        }
        getViewModel().sendToHardware(message);*/
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {

    }
}
