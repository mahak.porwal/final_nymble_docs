package com.example.recipeengine.util.debug;

import android.content.Context;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

/**
 * The spinner adapter implementing the default {@link ArrayAdapter} class.
 *
 * @author Abarajithan
 */
public final class SpinnerAdapter extends ArrayAdapter<String> {

    public SpinnerAdapter(@NonNull final Context context, @NonNull final String[] items) {
        super(context, android.R.layout.simple_dropdown_item_1line, items);
    }
}
