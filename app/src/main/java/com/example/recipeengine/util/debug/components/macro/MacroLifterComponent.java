package com.example.recipeengine.util.debug.components.macro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentMacroLifterBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;

/**
 * Macro lifter Angle readings, with freq control
 *
 * @author Abarajithan
 */
public final class MacroLifterComponent extends DebugComponent<ComponentMacroLifterBinding> {

  private Disposable disposable;

  public MacroLifterComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentMacroLifterBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentMacroLifterBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentMacroLifterBinding binding) {
    binding.motors.setAdapter(new SpinnerAdapter(getContext(), MotorRotationComponent.MACROS));
    binding.start.setOnClickListener(v -> {
      final int motorIndex = binding.motors.getSelectedItemPosition();
      final int freq = Integer.parseInt(binding.frequency.getText().toString());
      start(motorIndex, freq);
    });
    binding.stop.setOnClickListener(v -> {
      final int motorIndex = binding.motors.getSelectedItemPosition();
      stop(motorIndex);
    });
    getBinding().start.setEnabled(true);
    getBinding().stop.setEnabled(false);
  }

  private void start(final int motorIndex, final int freq) {
    getBinding().start.setEnabled(false);
    getBinding().stop.setEnabled(true);
    final int periodInMilliSecond = 1000 / freq;
    final Supplier<Float> supplier;
    switch (motorIndex) {
      case 0:
        supplier = DebugGuiMethods::macroServoFeedback_Macro_1;
        break;
      case 1:
        supplier = DebugGuiMethods::macroServoFeedback_Macro_2;
        break;
      case 2:
        supplier = DebugGuiMethods::macroServoFeedback_Macro_3;
        break;
      case 3:
        supplier = DebugGuiMethods::macroServoFeedback_Macro_4;
        break;
      default:
        return;
    }
    this.disposable = FrequencyScheduler.scheduleAPeriodicPollAndConsumeOnMainThread(
      periodInMilliSecond, supplier, this::onResponse);
  }

  private void stop(final int motorIndex) {
    getBinding().start.setEnabled(true);
    getBinding().stop.setEnabled(false);
    this.disposable.dispose();
  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
    this.setStatus(message);
  }
}
