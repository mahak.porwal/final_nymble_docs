package com.example.recipeengine.util.debug;

import androidx.annotation.NonNull;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class FrequencyScheduler {
  private static final CompositeDisposable compositeDisposable = new CompositeDisposable();

  private FrequencyScheduler() {
  }

  public static Disposable scheduleAPeriodicPoll(int periodInMilliSeconds,
                                                 Consumer<? super Long> consumer) {
    final Disposable disposable = Observable
      .interval(periodInMilliSeconds, TimeUnit.MILLISECONDS)
      .subscribe(consumer, Timber::e);
    compositeDisposable.add(disposable);
    return disposable;
  }

  public static Disposable scheduleAPeriodicPollAndConsumeOnMainThread(
    int periodInMilliSeconds, Supplier supplier, Consumer<String> stringConsumer) {
    final Disposable disposable = Observable
      .interval(periodInMilliSeconds, TimeUnit.MILLISECONDS)
      .map(l -> {
        return supplier.get().toString();
      })
      .doOnNext(s -> {
        Timber.d("Poll string %s", s);
      })
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(stringConsumer::accept, Timber::e);
    compositeDisposable.add(disposable);
    return disposable;
  }

  public static void runAsynchronously(Action action, Action postAction) {
    compositeDisposable.add(Observable.just(1)
      .observeOn(Schedulers.computation())
      .doOnNext(l -> action.run())
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(l -> postAction.run(), Timber::e));
  }

  public static void runAsynchronouslyAndConsumeOnMainThread(final Supplier supplier,
                                                             final Consumer<String> consumer) {
    compositeDisposable.add(Observable.just(1)
      .observeOn(Schedulers.computation())
      .map(l -> {
        return supplier.get().toString();
      })
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(consumer::accept, Timber::e));
  }

  public static void disposeAll() {
    compositeDisposable.dispose();
  }
}
