package com.example.recipeengine.util.debug.components.camera;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentStirrerTestCameraBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Stirrer Test - Rotate stirrer with steps and check angles
 *
 * @author Abarajithan
 */
public final class StirrerTestComponent extends DebugComponent<ComponentStirrerTestCameraBinding> {

    public StirrerTestComponent(@NonNull final Context context, @NonNull final DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentStirrerTestCameraBinding onInflateView(@NonNull final LayoutInflater inflater,
                                                           @NonNull final ViewGroup parent) {
        return ComponentStirrerTestCameraBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull final ComponentStirrerTestCameraBinding binding) {
        binding.rotate.setOnClickListener(v -> rotateStirrer());
    }

    private void rotateStirrer() {
        getBinding().rotate.setEnabled(false);
        this.setStatus("Testing...");
        this.startStirrerTest();
        this.pollStirrerTestStatus();
    }

    private void setStatus(@NonNull final String message) {
        getBinding().status.setText(message);
    }

    @Override
    public void onBindView() {

    }

    /**
     * Initiates stirrer test
     */
    private void startStirrerTest() {
        /*add(Observable.just(1)
                .observeOn(Schedulers.computation())
                .subscribe(testState -> {
                    getViewModel().visionController().stirrerTest();
                }));*/
    }

    /**
     * Poll stirrer test status
     */
    private void pollStirrerTestStatus() {
        /*add(Observable
                .interval(2, 2, TimeUnit.SECONDS)
                .map(aLong -> {
                    final Boolean testStatus = getViewModel().visionController().stirrerTestStatus();
                    if (null != testStatus) {
                        return Optional.of(testStatus);
                    } else {
                        return Optional.<Boolean>empty();
                    }
                })
                .filter(Optional::isPresent)
                .observeOn(AndroidSchedulers.mainThread())
                .take(1)
                .subscribe(status -> {
                    if (status.get()) {
                        this.setStatus("Passed !!");
                    } else {
                        this.setStatus("Failed !!");
                    }
                    getBinding().rotate.setEnabled(true);
                })
        );*/
    }

    @Override
    public void onResponse(@NonNull final String message) {

        /*if (!getBinding().rotate.isEnabled()) {
            final Message ackResponse = Messages.ack(Board.CONTROL);
            ackResponse.setSeqNo(message.getSeqNo());
            getViewModel().sendACK(ackResponse);
            this.startStirrerTest();
        }*/
    }
}
