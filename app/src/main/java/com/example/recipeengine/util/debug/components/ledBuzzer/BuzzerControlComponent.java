package com.example.recipeengine.util.debug.components.ledBuzzer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentBuzzerControlBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * Buzzer Level Control, with OFF Button
 *
 * @author Abarajithan
 */
public final class BuzzerControlComponent extends DebugComponent<ComponentBuzzerControlBinding> {

  public BuzzerControlComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentBuzzerControlBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentBuzzerControlBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentBuzzerControlBinding binding) {
    binding.set.setOnClickListener(v -> {
      final int volume = Integer.parseInt(binding.volume.getText().toString());
      setVolume(volume);
    });
    binding.turnOffBuzzer.setOnClickListener(v -> turnOffBuzzer());
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }

  private void setVolume(final int volume) {
    FrequencyScheduler.runAsynchronously(() -> DebugGuiMethods.setBuzzerVolume(volume), () -> {
    });
  }

  private void turnOffBuzzer() {
    FrequencyScheduler.runAsynchronously(DebugGuiMethods::turnOffBuzzer, () -> {
      }
    );
  }
}
