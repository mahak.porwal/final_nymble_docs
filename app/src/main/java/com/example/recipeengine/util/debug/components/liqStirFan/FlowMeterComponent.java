package com.example.recipeengine.util.debug.components.liqStirFan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentFlowMeterBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.function.Supplier;

import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public final class FlowMeterComponent extends DebugComponent<ComponentFlowMeterBinding> {
  private Disposable disposable;
  public static final String[] FLOW_METER_INPUTS = {"Water", "Oil", "Ex Fan"};
  private long tareValue;
  private volatile boolean isTareRequested;

  public FlowMeterComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentFlowMeterBinding onInflateView(@NonNull LayoutInflater inflater,
                                                 @NonNull ViewGroup parent) {
    return ComponentFlowMeterBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentFlowMeterBinding binding) {
    binding.flowMeterInputs.setAdapter(new SpinnerAdapter(getContext(), FLOW_METER_INPUTS));
    binding.start.setOnClickListener(v -> {
      final int inputIndex = binding.flowMeterInputs.getSelectedItemPosition();
      final int freq = Integer.parseInt(binding.frequency.getText().toString());
      this.start(inputIndex, freq);
    });
    binding.stop.setOnClickListener(v -> {
      this.stop();
    });
    binding.flowMeterTare.setOnClickListener(v -> {
      this.tareRequested();
    });
    binding.stop.setEnabled(false);
  }

  private void start(final int inputIndex, final int frequency) {
    getBinding().start.setEnabled(false);
    getBinding().stop.setEnabled(true);
    final int periodInMilliSecond = 1000 / frequency;
    final Supplier<String> stringSupplier;
    switch (inputIndex) {
      case 0:
        stringSupplier = () -> DebugGuiMethods.getWaterFlowCounter().toString();
        break;
      case 1:
        stringSupplier = () -> DebugGuiMethods.getOilFlowCounter().toString();
        break;
      case 2:
        stringSupplier = () -> DebugGuiMethods.getExhaustFanCounter().toString();
        break;
      default:
        return;
    }
    this.disposable =
      FrequencyScheduler.scheduleAPeriodicPollAndConsumeOnMainThread(periodInMilliSecond,
        stringSupplier, this::onResponse);
  }

  private void stop() {
    this.disposable.dispose();
    getBinding().start.setEnabled(true);
    getBinding().stop.setEnabled(false);
  }

  private void tareRequested() {
    getBinding().flowMeterTare.setEnabled(false);
    this.isTareRequested = true;
  }


  private void setStatus(@NonNull final Long reading) {
    final Long afterTareValue = reading - this.tareValue;
    getBinding().status.setText(afterTareValue.toString());
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
    if (this.isTareRequested) {
      try {
        this.tareValue = Long.valueOf(message).longValue();
        this.isTareRequested = false;
        getBinding().flowMeterTare.setEnabled(true);
      } catch (NumberFormatException e) {
        Timber.e("Tare request failed");
      }
    }
    try {
      final Long reading = Long.valueOf(message);
      this.setStatus(reading);
    } catch (NumberFormatException e) {
      Timber.e("Wrong long format");
    }
  }
}
