package com.example.recipeengine.util.debug.components.induction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentDacInductionBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * Text input field with "Set" and "Stop" Button
 *
 * @author Abarajithan
 */
public class DacInductionComponent extends DebugComponent<ComponentDacInductionBinding> {

    public DacInductionComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentDacInductionBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentDacInductionBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentDacInductionBinding binding) {
        binding.start.setOnClickListener(v -> {
            final int value = Integer.valueOf(binding.dacValue.getText().toString());
            setDacValue(value);
        });
        binding.stop.setOnClickListener(v -> {
            setDacValue(0);
        });
    }

    private void setDacValue(int value) {
        int modifiedValue = value;
        if (value > 100) {
            modifiedValue = 100;
        }

        if (value < 0) {
            modifiedValue = 0;
        }
        DebugGuiMethods.setInductionPwm(modifiedValue);
        /*final Message message = new Message(Board.INDUCTION);
        message.setMcuId(Board.INDUCTION);
        message.setType(Constants.MpuMcuCommand);
        message.setId(Constants.Induction_Pwm_Value);
        final Map<String, Object> valueMap = new HashMap<>();
        valueMap.put(String.valueOf(Constants.Primary), modifiedValue);
        message.setValue(valueMap);
        getViewModel().sendToHardware(message);*/
    }

    private void setStatus(@NonNull final String message) {
        this.getBinding().status.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {

    }
}
