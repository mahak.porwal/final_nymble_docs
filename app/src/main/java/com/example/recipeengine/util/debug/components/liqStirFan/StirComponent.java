package com.example.recipeengine.util.debug.components.liqStirFan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentStirBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.functions.Action;

/**
 * Stirrer Speed and Direction Control with Stop Button
 *
 * @author Abarajithan
 */
public final class StirComponent extends DebugComponent<ComponentStirBinding> {

  private static final String[] DIRECTIONS = {"Clockwise", "Anti-Clockwise"};

  public StirComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentStirBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentStirBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentStirBinding binding) {
    binding.directions.setAdapter(new SpinnerAdapter(getContext(), DIRECTIONS));
    binding.start.setOnClickListener(v -> {
      final int dirIndex = binding.directions.getSelectedItemPosition();
      final int speed = Integer.parseInt(binding.speed.getText().toString());
      start(dirIndex, speed);
    });
    binding.stop.setOnClickListener(v -> stop());
  }

  private void start(final int dirIndex, final int speed) {
    final Action action;
    switch (DIRECTIONS[dirIndex]) {
      case "Clockwise":
        action = () -> DebugGuiMethods.actuateStirrerClockwise(speed);
        break;
      case "Anti-Clockwise":
        action = () -> DebugGuiMethods.actuateStirrerAntiClockwise(speed);
        break;
      default:
        return;
    }
    FrequencyScheduler.runAsynchronously(action, () -> {
    });
        /*final Message message = new Message(Board.CONTROL);
        int modifiedSpeed = speed;
        message.setType(Constants.MpuMcuCommand);
        message.setSubSystem(Constants.Stirrer);
        message.setId(Constants.Stirrer_Motor);
        switch (DIRECTIONS[dirIndex]) {
            case "Clockwise":
                break;
            case "Anti-Clockwise":
                modifiedSpeed *= -1;
                break;
        }
        final Map<String, Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary), modifiedSpeed);
        message.setValue(value);
        getViewModel().sendToHardware(message);*/
  }

  private void stop() {
    FrequencyScheduler.runAsynchronously(DebugGuiMethods::stopStirrer, () -> {
    });
  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }
}
