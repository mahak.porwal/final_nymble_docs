package com.example.recipeengine.util.debug.components.ledBuzzer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentLedControlBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.components.DebugComponent;

/**
 * LED Brightness Level Control, with OFF Button
 *
 * @author Abarajithan
 */
public final class LedControlComponent extends DebugComponent<ComponentLedControlBinding> {

  public LedControlComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentLedControlBinding onInflateView(@NonNull LayoutInflater inflater,
                                                  @NonNull ViewGroup parent) {
    return ComponentLedControlBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentLedControlBinding binding) {
    binding.on.setOnClickListener(v -> {
      this.turnOnLed();
    });
    binding.off.setOnClickListener(v -> turnOffLed());
  }

  private void turnOnLed() {
    FrequencyScheduler.runAsynchronously(DebugGuiMethods::designLedOn,
      () -> {
      });
  }

  private void turnOffLed() {
    FrequencyScheduler.runAsynchronously(DebugGuiMethods::designLedOff,
      () -> {
      });
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }
}
