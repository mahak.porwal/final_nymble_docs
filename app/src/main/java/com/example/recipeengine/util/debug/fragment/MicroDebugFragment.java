package com.example.recipeengine.util.debug.fragment;

import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_MICRO_HOMING;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_MICRO_MAIN;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * Micro debug fragment
 *
 * @author Abarajithan
 */
public final class MicroDebugFragment extends ComponentTabsFragment {
    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_micro_fragment_title);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{
                ComponentListFragment.create(ID_MICRO_MAIN),
                ComponentListFragment.create(ID_MICRO_HOMING),
        };
    }
}
