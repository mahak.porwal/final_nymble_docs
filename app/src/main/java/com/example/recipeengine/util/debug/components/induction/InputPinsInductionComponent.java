package com.example.recipeengine.util.debug.components.induction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentInputPinsInductionBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * Input Pins - S_GOOD_IH, with freq control
 *
 * @author Abarajithan
 */
public final class InputPinsInductionComponent extends DebugComponent<ComponentInputPinsInductionBinding> {

    private static final String[] PINS = {"S_GOOD_IH"};

    public InputPinsInductionComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentInputPinsInductionBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentInputPinsInductionBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentInputPinsInductionBinding binding) {
        binding.inputPins.setAdapter(new SpinnerAdapter(getContext(), PINS));
        binding.start.setOnClickListener(v -> {
            final int pinIndex = binding.inputPins.getSelectedItemPosition();
            final int freq = Integer.parseInt(binding.frequency.getText().toString());
            start(pinIndex, freq);
        });
        binding.stop.setOnClickListener(v -> {
            final int pinIndex = binding.inputPins.getSelectedItemPosition();
            stop(pinIndex);
        });
    }

    private void start(final int pinIndex, final int freq) {/*
        final Message message = new Message(Board.MPU);
        message.setType(Constants.MpuMcuRequest);
        switch (PINS[pinIndex]) {
            case "S_GOOD_IH":
                message.setId(Constants.S_GOOD_IH);
                break;
        }
        final Map<String, Object> valueMap = new HashMap<>();
        valueMap.put(String.valueOf(Constants.Primary), freq);
        message.setValue(valueMap);
        getViewModel().sendToHardware(message);*/
    }

    private void stop(final int pinIndex) {
        this.start(pinIndex, 0);
    }

    private void setStatus(@NonNull final String message) {
        getBinding().status.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {
        /*final Integer pinState = message.getIntegerKeyValue(Constants.Primary);
        if (GPIOEnum.LOW == pinState) {
            this.setStatus("LOW");
        } else {
            this.setStatus("HIGH");
        }*/
    }
}
