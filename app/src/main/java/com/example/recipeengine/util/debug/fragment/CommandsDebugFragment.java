package com.example.recipeengine.util.debug.fragment;


import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_COMMANDS_HEAT;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_COMMANDS_LIQ_STIR;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_COMMANDS_MACRO;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_COMMANDS_MICRO;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * Commands debug fragment
 *
 * @author Abarajithan
 */
public final class CommandsDebugFragment extends ComponentTabsFragment {
    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_commands_fragment_title);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{
                ComponentListFragment.create(ID_COMMANDS_HEAT),
                ComponentListFragment.create(ID_COMMANDS_MICRO),
                ComponentListFragment.create(ID_COMMANDS_MACRO),
                ComponentListFragment.create(ID_COMMANDS_LIQ_STIR)
        };
    }
}
