package com.example.recipeengine.util;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ValueSaverModule.class)
public interface ValueSaverComponent {

}
