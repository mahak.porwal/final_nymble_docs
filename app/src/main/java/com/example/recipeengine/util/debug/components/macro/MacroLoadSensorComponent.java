package com.example.recipeengine.util.debug.components.macro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentLoadSensorMacroBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * Load sensor analysis block
 *
 * @author Abarajithan
 */
public final class MacroLoadSensorComponent extends DebugComponent<ComponentLoadSensorMacroBinding> {

    private static final String[] DATA_TYPES = {"Reference", "ENOB", "NFB", "RMS",
            "Peek-to-Peek", "Internal-Offset", "Offset", "AVDD"};

    public MacroLoadSensorComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentLoadSensorMacroBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentLoadSensorMacroBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentLoadSensorMacroBinding binding) {
        binding.dataTypes.setAdapter(new SpinnerAdapter(getContext(), DATA_TYPES));
        binding.read.setOnClickListener(v -> {
            final int time = Integer.parseInt(binding.time.getText().toString());
            final int dataTypeIndex = binding.dataTypes.getSelectedItemPosition();
            start(time, dataTypeIndex);
        });
    }

    private void start(final int time, final int dataTypeIndex) {
        /*getBinding().read.setEnabled(false);
        this.setStatus("Calculating..");
        final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuRequest);
        message.setSubSystem(Constants.Macro);
        switch (DATA_TYPES[dataTypeIndex]) {
            case "Reference":
                message.setId(Constants.Pan_LC_Data_Reference);
                break;
            case "ENOB":
                message.setId(Constants.Pan_LC_Data_ENOB_Value);
                break;
            case "NFB":
                message.setId(Constants.Pan_LC_Data_NFB_Value);
                break;
            case "RMS":
                message.setId(Constants.Pan_LC_Data_RMS_Noise);
                break;
            case "Peek-to-Peek":
                message.setId(Constants.Pan_LC_Data_P2P_Noise);
                break;
            case "Internal-Offset":
                message.setId(Constants.Pan_LC_Data_Int_Offset);
                break;
            case "Offset":
                message.setId(Constants.Pan_LC_Data_Offset);
                break;
            case "AVDD":
                message.setId(Constants.Pan_LC_Data_AVDD);
                break;
        }
        final Map<String, Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary), time);
        message.setValue(value);
        getViewModel().sendToHardware(message);*/
    }

    private void setStatus(@NonNull final String message) {
        getBinding().status.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {
        /*final Double sensorValue = message.getDoubleKeyValue(Constants.Primary);
        this.setStatus(String.valueOf(sensorValue));
        getBinding().read.setEnabled(true);*/
    }
}
