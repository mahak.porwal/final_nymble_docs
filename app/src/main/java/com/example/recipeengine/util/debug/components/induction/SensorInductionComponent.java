package com.example.recipeengine.util.debug.components.induction;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentSensorInductionBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;

/**
 * Sensor Values - IGBT Temp, Pan Temp, Current, Voltage - with Freq control
 *
 * @author Abarajithan
 */
public final class SensorInductionComponent extends DebugComponent<ComponentSensorInductionBinding> {

  private static final String[] SENSORS = {"IGBT Temp", "Pan Temp", "Current", "Voltage"};
  private Disposable disposable;
  private int sensorIndex;

  public SensorInductionComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentSensorInductionBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentSensorInductionBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentSensorInductionBinding binding) {
    binding.sensors.setAdapter(new SpinnerAdapter(getContext(), SENSORS));
    binding.start.setOnClickListener(v -> {
      this.sensorIndex = binding.sensors.getSelectedItemPosition();
      final int freq = Integer.parseInt(binding.frequency.getText().toString());
      start(freq);
    });
    binding.stop.setOnClickListener(v -> {
      final int sensorIndex = binding.sensors.getSelectedItemPosition();
      stop(sensorIndex);
    });
  }

  private void start(final int freq) {
    final Supplier<Float> action;
    switch (SENSORS[this.sensorIndex]) {
      case "IGBT Temp":
        action = DebugGuiMethods::getIgbtTemperature;
        break;
      case "Pan Temp":
        action = DebugGuiMethods::getPanTemperature;
        break;
      case "Current":
        action = DebugGuiMethods::getInductionCurrent;
        break;
      case "Voltage":
        action = DebugGuiMethods::getInductionVoltage;
        break;
      default:
        return;
    }
    getBinding().start.setEnabled(false);
    getBinding().stop.setEnabled(true);
    final int periodInMilliSecond = 1000 / freq;
    this.disposable = FrequencyScheduler.scheduleAPeriodicPollAndConsumeOnMainThread(
      periodInMilliSecond, action, this::onResponse);
  }

  private void stop(final int sensorIndex) {
    this.disposable.dispose();
    getBinding().start.setEnabled(true);
    getBinding().stop.setEnabled(false);
  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @SuppressLint("DefaultLocale")
  @Override
  public void onResponse(String message) {
    switch (SENSORS[this.sensorIndex]) {
      case "IGBT Temp":
      case "Pan Temp":
        this.setStatus(String.format("%.2f degrees", Float.valueOf(message).floatValue()));
        break;
      case "Current":
        this.setStatus(String.format("%.2f A", Float.valueOf(message).floatValue()));
        break;
      case "Voltage":
        this.setStatus(String.format("%.2f V", Float.valueOf(message).floatValue()));
        break;
    }
  }
}
