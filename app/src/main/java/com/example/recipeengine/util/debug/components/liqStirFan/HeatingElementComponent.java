package com.example.recipeengine.util.debug.components.liqStirFan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentHeatingElementBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.components.DebugComponent;

public final class
HeatingElementComponent extends DebugComponent<ComponentHeatingElementBinding> {
  public HeatingElementComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentHeatingElementBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentHeatingElementBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentHeatingElementBinding binding) {
    getBinding().on.setOnClickListener(v -> {
      this.turnOnHeatingElement();
    });
    getBinding().off.setOnClickListener(v -> {
      this.turnOffHeatingElement();
    });
    getBinding().off.setEnabled(false);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }

  private void turnOnHeatingElement() {
    getBinding().on.setEnabled(false);
    getBinding().off.setEnabled(true);
    FrequencyScheduler.runAsynchronously(DebugGuiMethods::switchHeaterState, () -> {
    });
  }

  private void turnOffHeatingElement() {
    getBinding().on.setEnabled(true);
    getBinding().off.setEnabled(false);
    FrequencyScheduler.runAsynchronously(DebugGuiMethods::switchHeaterState, () -> {
    });
  }
}
