package com.example.recipeengine.util.debug.components.micro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentPodAngleBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import io.reactivex.disposables.Disposable;

/**
 * Pod Angle readings, with freq control
 *
 * @author Abarajithan
 */
public final class PodAngleComponent extends DebugComponent<ComponentPodAngleBinding> {

  private Disposable disposable;

  public PodAngleComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentPodAngleBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentPodAngleBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentPodAngleBinding binding) {
    binding.motors.setAdapter(new SpinnerAdapter(getContext(), PodRotationComponent.MOTORS));
    binding.start.setOnClickListener(v -> {
      final int podIndex = binding.motors.getSelectedItemPosition();
      final int freq = Integer.parseInt(binding.frequency.getText().toString());
      start(podIndex, freq);
    });
    binding.stop.setOnClickListener(v -> {
      final int podIndex = binding.motors.getSelectedItemPosition();
      stop(podIndex);
    });
  }

  private void start(final int podIndex, final int freq) {
    getBinding().start.setEnabled(false);
    getBinding().stop.setEnabled(true);
    final int periodInMilliSecond = 1000 / freq;
    final Supplier<Float> supplier;
    switch (podIndex) {
      case 0:
        supplier = DebugGuiMethods::microServoFeedback_Micro_2;
        break;
      case 1:
        supplier = DebugGuiMethods::microServoFeedback_Micro_1;
        break;
      default:
        return;
    }
    this.disposable = FrequencyScheduler.scheduleAPeriodicPollAndConsumeOnMainThread(
      periodInMilliSecond, supplier, this::onResponse
    );
  }

  private void stop(final int podIndex) {
    getBinding().start.setEnabled(true);
    getBinding().stop.setEnabled(false);
    this.disposable.dispose();
  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
    this.setStatus(message);
  }
}
