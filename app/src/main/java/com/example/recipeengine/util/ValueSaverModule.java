package com.example.recipeengine.util;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ValueSaverModule {
  @Binds
  abstract ValueSaver bindValueSaver(ValueSaverImpl valueSaver);
}
