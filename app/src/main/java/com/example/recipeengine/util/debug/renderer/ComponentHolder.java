package com.example.recipeengine.util.debug.renderer;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

/**
 * The ViewHolder that holds a component view initialization.
 * This is same as RecyclerView's ViewHolder {@link RecyclerView.ViewHolder}
 * but restricts it to a component level implementation.
 *
 * @param <B> The generated View Binding class implementation
 * @author Abarajithan
 */
public abstract class ComponentHolder<B extends ViewDataBinding> extends RecyclerView.ViewHolder {

    public ComponentHolder(@NonNull B binding) {
        super(binding.getRoot());
        onCreateHolder(binding);
    }

    /**
     * Initialize the component UIs here.
     *
     * @param binding The View Binding class
     */
    protected abstract void onCreateHolder(@NonNull B binding);
}
