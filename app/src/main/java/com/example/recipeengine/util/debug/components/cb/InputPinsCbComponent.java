package com.example.recipeengine.util.debug.components.cb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentInputpinsCbBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * GPIO Input pin status
 *
 * @author Abarajithan
 */
public final class InputPinsCbComponent extends DebugComponent<ComponentInputpinsCbBinding> {

    private static final String[] INPUT_PINS = {"CB", "PGOOD407"};

    public InputPinsCbComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentInputpinsCbBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentInputpinsCbBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentInputpinsCbBinding binding) {
        binding.inputPins.setAdapter(new SpinnerAdapter(getContext(), INPUT_PINS));
        binding.start.setOnClickListener(v -> {
            final int pinIndex = binding.inputPins.getSelectedItemPosition();
            final int frequency = Integer.parseInt(binding.frequency.getText().toString());
            start(pinIndex, frequency);
        });
        binding.stop.setOnClickListener(v -> {
            final int pinIndex = binding.inputPins.getSelectedItemPosition();
            stop(pinIndex);
        });
    }

    private void start(final int pinIndex, final int frequency) {
        /*final Message message = new Message(Board.MPU);
        message.setType(Constants.MpuMcuRequest);
        final Map<String, Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary), frequency);
        message.setValue(value);
        switch (INPUT_PINS[pinIndex]) {
            case "CB":
                message.setId(Constants.CON_CB);
                break;
            case "PGOOD407":
                message.setId(Constants.S_PGOOD_407);
                break;
        }
        getViewModel().sendToHardware(message);*/
    }

    private void stop(int pinIndex) {
        this.start(pinIndex, 0);
    }

    private void setOutputStatus(@NonNull final String message) {
        getBinding().inputPinStatus.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {

    }
}
