package com.example.recipeengine.util.debug.fragment;


import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_INDUCTION_GPIO;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_INDUCTION_MAIN;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * Induction debug fragment
 *
 * @author Abarajithan
 */
public final class InductionDebugFragment extends ComponentTabsFragment {
    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_induction_fragment_title);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{
                ComponentListFragment.create(ID_INDUCTION_MAIN),
                ComponentListFragment.create(ID_INDUCTION_GPIO)
        };
    }
}
