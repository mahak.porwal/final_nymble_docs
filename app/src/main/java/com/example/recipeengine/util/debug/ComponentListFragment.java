package com.example.recipeengine.util.debug;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.recipeengine.databinding.FragmentComponentListBinding;
import com.example.recipeengine.util.debug.components.ComponentCollection;
import com.example.recipeengine.util.debug.components.DebugComponent;
import com.example.recipeengine.util.debug.renderer.ComponentsAdapter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Base Debug Fragment with Component list.
 *
 * @author Abarajithan
 */
public final class ComponentListFragment extends Fragment {

    private static final String KEY_COMPONENTS_ID = "components_list_id";

    public static ComponentListFragment create(@NonNull final int componentsId) {
        final ComponentListFragment fragment = new ComponentListFragment();
        final Bundle args = new Bundle();
        args.putInt(KEY_COMPONENTS_ID, componentsId);
        fragment.setArguments(args);
        return fragment;
    }

    private FragmentComponentListBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.binding = FragmentComponentListBinding.inflate(inflater, container, false);
        return this.binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.binding.componentList.setHasFixedSize(true);
        this.binding.componentList.setItemAnimator(new DefaultItemAnimator());
        this.binding.componentList.setLayoutManager(new LinearLayoutManager(requireContext()));

        final int id = requireArguments().getInt(KEY_COMPONENTS_ID);
        final DebugViewModel viewModel = new ViewModelProvider(requireActivity()).get(DebugViewModel.class);
        final DebugComponent.Factory factory = new DebugComponent.Factory(requireContext(), viewModel);
        final List<? extends DebugComponent> components = ComponentCollection.getComponents(id)
                .stream().map(component -> factory.create(component)).collect(Collectors.toList());
        final ComponentsAdapter adapter = new ComponentsAdapter(components);
        this.binding.componentList.setAdapter(adapter);
    }
}
