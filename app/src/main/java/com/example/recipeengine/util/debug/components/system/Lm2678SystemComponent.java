package com.example.recipeengine.util.debug.components.system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentLm2678SystemBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * LM2678 Read with frequency control and
 * LM2678 Control High & Low.
 *
 * @author Abarajithan
 */
public final class Lm2678SystemComponent extends DebugComponent<ComponentLm2678SystemBinding> {

    private final static int TURN_ON_LM2678 = 0;
    private final static int TURN_OFF_LM2678 = 1;

    public Lm2678SystemComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentLm2678SystemBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentLm2678SystemBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentLm2678SystemBinding binding) {
        binding.read.setOnClickListener(v -> {
            final int frequency = Integer.parseInt(binding.frequency.getText().toString());
            read(frequency);
        });
        binding.high.setOnClickListener(v -> setState(TURN_ON_LM2678));
        binding.low.setOnClickListener(v -> setState(TURN_OFF_LM2678));
    }

    private void read(final int frequency) {
        /*final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuRequest);
        message.setSubSystem(Constants.Micro);
        message.setId(Constants.LM2678);
        Map<String, Object> valueMap = new HashMap<>();
        valueMap.put(String.valueOf(Constants.Primary), frequency);
        message.setValue(valueMap);
        getViewModel().sendToHardware(message);*/
    }

    private void setState(int state) {
        /*final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuCommand);
        message.setSubSystem(Constants.Micro);
        message.setId(Constants.LM2678);
        Map<String, Object> valueMap = new HashMap<>();
        valueMap.put(String.valueOf(Constants.Primary), state);
        message.setValue(valueMap);
        getViewModel().sendToHardware(message);*/
    }

    private void setStatus(@NonNull final String message) {
        getBinding().status.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {
        /*final Integer pinState = message.getIntegerKeyValue(Constants.Primary);
        if (GPIOEnum.LOW == pinState) {
            this.setStatus("OFF");
        } else {
            this.setStatus("ON");
        }*/
    }
}
