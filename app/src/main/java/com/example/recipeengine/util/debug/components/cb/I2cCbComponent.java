package com.example.recipeengine.util.debug.components.cb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentI2cCbBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CB I2C Device list
 *
 * @author Abarajithan
 */
public final class I2cCbComponent extends DebugComponent<ComponentI2cCbBinding> {

    private final Map<Integer, String> mcuI2cDeviceMap = new HashMap<>();

    private static final int ADS122C04 = 0x47;

    private static final int PCA9685 = 0x42;

    private static final int INA219B = 0x40;

    private static final int FRAM_MB85 = 0x50;

    private static final int TEMP_SENSOR_MCP9805 = 0x18;


    public I2cCbComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
        this.mcuI2cDeviceMap.put(ADS122C04, "ADS122C04");
        this.mcuI2cDeviceMap.put(PCA9685, "PCA9685");
        this.mcuI2cDeviceMap.put(INA219B, "INA219B");
        this.mcuI2cDeviceMap.put(FRAM_MB85, "FRAM_MB85");
        this.mcuI2cDeviceMap.put(TEMP_SENSOR_MCP9805, "TEMP_SENSOR_MCP9805");
    }

    @Override
    public ComponentI2cCbBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentI2cCbBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentI2cCbBinding binding) {
        binding.i2cDeviceList.setOnClickListener(v -> detectDeviceList());
    }

    private void detectDeviceList() {
        enableButton(false);
        setStatus("");

        /*final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuRequest);
        message.setSubSystem(Constants.ControlBoard);
        message.setId(Constants.I2C_Address_List);
        getViewModel().sendToHardware(message);*/
    }

    private void enableButton(final boolean enable) {
        getBinding().i2cDeviceList.setEnabled(enable);
    }

    private void setStatus(@NonNull final String message) {
        getBinding().statusText.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(@NonNull final String message) {
        /*final StringBuilder builder = new StringBuilder();
        final List<Double> i2cList = (List<Double>) message.getValue()
                .get(String.valueOf(Constants.Primary));
        for (double device : i2cList) {
            final int value = (int) Math.floor(device);
            if(this.mcuI2cDeviceMap.containsKey(value)){
                builder.append(String.format("0x%02X - %s\n", value,this.mcuI2cDeviceMap.get(value)));
            }else{
                builder.append(String.format("0x%02X - Unknown\n", value));
            }
        }
        setStatus(builder.toString());
        enableButton(true);*/
    }
}
