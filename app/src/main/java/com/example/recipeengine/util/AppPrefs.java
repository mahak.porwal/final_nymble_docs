package com.example.recipeengine.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Map;

/**
 * App level preference util class.
 * <p>
 * Note: Underscorizes the keys automatically.
 *
 * @author Abarajithan
 */
public class AppPrefs {

    public static final String KEY_FIRST_RUN = "is_first_run";
    public static final String KEY_GET_THERMAL_DATA = "force_get_thermal_data";
    public static final String KEY_GET_IND_PARAMS = "force_get_ind_params";
    public static final String KEY_GET_CAMERA_PARAMS = "force_get_camera_params";
    public static final String KEY_USE_OZ_UNIT = "use_oz_unit";

    public static final String KEY_COOKING_SESSION_ID = "cooking_session_id";
    public static final String KEY_SESSION_UPLOADING = "is_session_uploading";

    public static final String KEY_DEBUG_RECORD_THERMAL = "debug_recordThermal";

    private static final String[] TEMP_KEYS = {KEY_COOKING_SESSION_ID, KEY_SESSION_UPLOADING};

    private static SharedPreferences INSTANCE = null;

    public static void init(Context c) {
        if (INSTANCE == null) {
            INSTANCE = PreferenceManager.getDefaultSharedPreferences(c.getApplicationContext());
        }
        // Remove temporary preferences
        for (String key : TEMP_KEYS) {
            remove(key);
        }
    }

    public static boolean contains(String key) {
        return get().contains(key);
    }

    public static <T> void save(String key, T val) {
        SharedPreferences.Editor editor = get().edit();
        if (val instanceof String) {
            editor.putString(key, (String) val).apply();
        } else if (val instanceof Integer) {
            editor.putInt(key, (Integer) val).apply();
        } else if (val instanceof Boolean) {
            editor.putBoolean(key, (Boolean) val).apply();
        } else if (val instanceof Float) {
            editor.putFloat(key, (Float) val).apply();
        } else if (val instanceof Long) {
            editor.putLong(key, (Long) val).apply();
        } else {
            System.out.printf("Unknown supported type for preferences: %s", val.getClass());
        }
    }

    public static String getString(String key, String defVal) {
        return get().getString(key, defVal);
    }

    public static int getInt(String key, int defVal) {
        return get().getInt(key, defVal);
    }

    public static void remove(String key) {
        get().edit().remove(key).apply();
    }

    private static SharedPreferences get() {
        if (INSTANCE == null) {
            throw new IllegalStateException("AppPrefs not initialize. Did you call init() method?");
        }
        return INSTANCE;
    }

    public static boolean getBoolean(String key, boolean defVal) {
        return get().getBoolean(key, defVal);
    }

    /**
     * Check the class type of an entry
     *
     * @param key   the key of the preference
     * @param clazz the class type
     * @return true, if it's of given type
     */
    public static <T> boolean isType(final String key, Class<T> clazz) {
        return get().getAll().get(key).getClass().equals(clazz);
    }

    /**
     * Get all preference values
     *
     * @return
     */
    public static Map<String, ?> getAll() {
        return get().getAll();
    }

}
