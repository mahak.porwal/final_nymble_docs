package com.example.recipeengine.util.debug.components.eeprom;

import static com.example.recipeengine.util.debug.NonVolatileKeys.OIL_PUMP_FACTOR;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentOilPumpEepromBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.ValueSaver;
import com.example.recipeengine.util.debug.components.DebugComponent;

/**
 * Oil Pump Factor
 *
 * @author Abarajithan
 */
public final class OilPumpFactorComponent extends DebugComponent<ComponentOilPumpEepromBinding> {
  private final ValueSaver valueSaver;

  public OilPumpFactorComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
    this.valueSaver = viewModel.getValueSaver();
  }

  @Override
  public ComponentOilPumpEepromBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentOilPumpEepromBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentOilPumpEepromBinding binding) {
    binding.set.setOnClickListener(v -> {
      final int factor = Integer.parseInt(binding.factor.getText().toString());
      setFactor(factor);
    });
    binding.read.setOnClickListener(v -> readEeprom());
  }

  private void readEeprom() {
    final String value = this.valueSaver.getValue(OIL_PUMP_FACTOR.name());
    if (null != value) {
      this.setStatus(value);
    } else {
      this.setStatus("No value found !!");
    }
  }

  private void setFactor(final int factor) {
    this.valueSaver.setKey(OIL_PUMP_FACTOR.name(), Integer.valueOf(factor).toString());
  }

  private void setStatus(@NonNull final String message) {
    this.getBinding().statusText.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
        /*final Double eepromValue = message.getDoubleKeyValue(Constants.Primary);
        this.setStatus(String.valueOf(eepromValue));*/
  }
}
