package com.example.recipeengine.util.debug.components.micro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentPodRotationBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.functions.Action;


/**
 * Pod rotation - Pod, angle and speed selection; with STOP functionality
 *
 * @author Abarajithan
 */
public final class PodRotationComponent extends DebugComponent<ComponentPodRotationBinding> {

  public static final String[] MOTORS = {"Dispenser", "Carousel"};

  public PodRotationComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentPodRotationBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentPodRotationBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentPodRotationBinding binding) {
    binding.motor.setAdapter(new SpinnerAdapter(getContext(), MOTORS));
    binding.start.setOnClickListener(v -> {
      final int podIndex = binding.motor.getSelectedItemPosition();
      final int angle = Integer.parseInt(binding.angle.getText().toString());
      final int speed = Integer.parseInt(binding.speed.getText().toString());
      start(podIndex, angle, speed);
    });
    binding.stop.setOnClickListener(v -> {
      final int podIndex = binding.motor.getSelectedItemPosition();
      stop(podIndex);
    });
    getBinding().start.setEnabled(true);
    getBinding().stop.setEnabled(false);
  }

  private void start(final int podIndex, final int angle, int speed) {
    final Action action;
    getBinding().start.setEnabled(false);
    getBinding().stop.setEnabled(true);
    switch (podIndex) {
      case 0:
        action = () -> DebugGuiMethods.microServoSequence_Micro_2(angle, speed);
        break;
      case 1:
        action = () -> DebugGuiMethods.microServoSequence_Micro_1(angle, speed);
        break;
      default:
        return;
    }
    FrequencyScheduler.runAsynchronously(action, () -> {
      getBinding().start.setEnabled(true);
      getBinding().stop.setEnabled(false);
    });
        /*if(speed > 10){
            speed = 10;
        }
        final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuCommand);
        message.setSubSystem(Constants.Micro);
        message.setId(Constants.Vib_Mot_1 + podIndex);
        Map<String, Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary), angle);
        value.put(String.valueOf(Constants.Secondary), speed);
        message.setValue(value);
        getViewModel().sendToHardware(message);*/
  }

  private void stop(final int podIndex) {

  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }
}
