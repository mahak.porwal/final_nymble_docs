package com.example.recipeengine.util.debug;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.util.ValueSaver;

/**
 * Factory class to create an instance of {@link DebugViewModel}
 *
 * @author Abarajithan
 */
public final class DebugViewModelFactory implements ViewModelProvider.Factory {

  private final RecipeContext recipeContext;
  private final ValueSaver valueSaver;

  public DebugViewModelFactory(@NonNull final RecipeContext recipeContext, ValueSaver valueSaver) {
    this.recipeContext = recipeContext;
    this.valueSaver = valueSaver;
  }

  @NonNull
  @Override
  public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
    if (modelClass.isAssignableFrom(DebugViewModel.class)) {
      return (T) new DebugViewModel(recipeContext, this.valueSaver);
    }
    throw new IllegalArgumentException("Unsupported ViewModel class: " + modelClass.getSimpleName());
  }
}
