package com.example.recipeengine.util.debug.components.eeprom;

import static com.example.recipeengine.util.debug.NonVolatileKeys.WATER_PUMP_FACTOR;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentWaterPumpEepromBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.ValueSaver;
import com.example.recipeengine.util.debug.components.DebugComponent;

/**
 * Water Pump Factor
 *
 * @author Abarajithan
 */
public final class WaterPumpFactorComponent extends DebugComponent<ComponentWaterPumpEepromBinding> {
  private final ValueSaver valueSaver;

  public WaterPumpFactorComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
    this.valueSaver = viewModel.getValueSaver();
  }

  @Override
  public ComponentWaterPumpEepromBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentWaterPumpEepromBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentWaterPumpEepromBinding binding) {
    binding.set.setOnClickListener(v -> {
      final int factor = Integer.parseInt(binding.factor.getText().toString());
      setFactor(factor);
    });
    binding.read.setOnClickListener(v -> readEeprom());
  }

  private void readEeprom() {
    final String value = this.valueSaver.getValue(WATER_PUMP_FACTOR.name());
    if (null != value) {
      this.setStatus(value);
    } else {
      this.setStatus("No value found !!");
    }
  }

  private void setFactor(final int factor) {
    this.valueSaver.setKey(WATER_PUMP_FACTOR.name(), Integer.valueOf(factor).toString());
  }

  private void setStatus(@NonNull final String message) {
    this.getBinding().statusText.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
        /*final Double eepromValue = message.getDoubleKeyValue(Constants.Primary);
        this.setStatus(String.valueOf(eepromValue));*/
  }
}
