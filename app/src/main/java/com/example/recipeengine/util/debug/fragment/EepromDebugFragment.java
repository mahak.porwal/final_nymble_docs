package com.example.recipeengine.util.debug.fragment;


import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_EEPROM_LIQUID;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_EEPROM_LOAD_CELL;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_EEPROM_MACRO;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_EEPROM_PCA;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * EEPROM debug fragment
 *
 * @author Abarajithan
 */
public final class EepromDebugFragment extends ComponentTabsFragment {
    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_eeprom_fragment_title);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{
                ComponentListFragment.create(ID_EEPROM_LOAD_CELL),
                ComponentListFragment.create(ID_EEPROM_LIQUID),
                ComponentListFragment.create(ID_EEPROM_PCA),
                ComponentListFragment.create(ID_EEPROM_MACRO),
        };
    }
}
