package com.example.recipeengine.util.debug.components.liqStirFan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentStirFbBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.function.Supplier;

/**
 * Stir FB Test
 *
 * @author Abarajithan
 */
public final class StirFBComponent extends DebugComponent<ComponentStirFbBinding> {

  private final static int STIRRER_CONNECTED = 1;

  private final static int STIRRER_DISCONNECTED = 0;

  public StirFBComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentStirFbBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentStirFbBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentStirFbBinding binding) {
    binding.testStirrerFb.setOnClickListener(v -> testStirrerFB());
  }

  private void testStirrerFB() {
    final Supplier supplier = DebugGuiMethods::getStirrerFeedback;
    FrequencyScheduler.runAsynchronouslyAndConsumeOnMainThread(supplier, this::onResponse);
  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
    this.setStatus(message);
  }
}
