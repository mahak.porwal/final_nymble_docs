package com.example.recipeengine.util.debug.components.eeprom;

import static com.example.recipeengine.util.debug.NonVolatileKeys.PCA_FACTOR;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentPcaFactorEepromBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.ValueSaver;
import com.example.recipeengine.util.debug.components.DebugComponent;

/**
 * PCA Factor
 *
 * @author Abarajithan
 */
public final class PcaFactorComponent extends DebugComponent<ComponentPcaFactorEepromBinding> {
  private final ValueSaver valueSaver;

  public PcaFactorComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
    this.valueSaver = viewModel.getValueSaver();
  }

  @Override
  public ComponentPcaFactorEepromBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentPcaFactorEepromBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentPcaFactorEepromBinding binding) {
    binding.test.setOnClickListener(v -> {
      final int factor = Integer.parseInt(binding.factor.getText().toString());
      testPcaFactor(factor);
    });
    binding.set.setOnClickListener(v -> {
      final int factor = Integer.parseInt(binding.factor.getText().toString());
      setFactor(factor);
    });
    binding.read.setOnClickListener(v -> readEeprom());
  }

  private void readEeprom() {
    final String value = this.valueSaver.getValue(PCA_FACTOR.name());
    if (null != value) {
      this.setStatus(value);
    } else {
      this.setStatus("No value found !!");
    }
  }

  private void setFactor(final int factor) {
    this.valueSaver.setKey(PCA_FACTOR.name(), Integer.valueOf(factor).toString());
  }

  private void testPcaFactor(final int factor) {
    this.setFactor(factor);
  }

  private void setStatus(@NonNull final String message) {
    this.getBinding().statusText.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
        /*final Integer eepromValue = message.getIntegerKeyValue(Constants.Primary);
        this.setStatus(String.valueOf(eepromValue));*/
  }
}
