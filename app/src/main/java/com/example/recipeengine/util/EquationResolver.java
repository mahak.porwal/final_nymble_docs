package com.example.recipeengine.util;

import java.util.Map;

public interface EquationResolver {
  void setParametricField(String fieldName, String expression);

  boolean isFieldResolved(String fieldName);

  void resolveParametricField(String fieldName, Map<String, String> parametersMap);

  Double getResolvedValue(String fieldName);
}
