package com.example.recipeengine.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import androidx.annotation.NonNull;

import java.util.Set;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ValueSaverImpl implements ValueSaver {

  private final Editor editor;
  private final SharedPreferences sharedPreferences;

  @Inject
  public ValueSaverImpl(Context context) {
    this.sharedPreferences = context.getSharedPreferences(ValueSaver.hardwareFactorFile,
      Context.MODE_PRIVATE);
    this.editor = this.sharedPreferences.edit();
  }

  @Override
  public void setKey(@NonNull String key, @NonNull String value) {
    this.editor.putString(key, value);
    this.editor.apply();
  }

  @Override
  public String getValue(@NonNull String key) {
    return this.sharedPreferences.getString(key, null);
  }

  public Set<String> getKeySet() {
    return this.sharedPreferences.getAll().keySet();
  }
}
