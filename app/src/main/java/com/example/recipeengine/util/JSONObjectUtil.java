package com.example.recipeengine.util;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class JSONObjectUtil {
  public static JSONObject createJSONObject(String filename) {
    Object object = null;
    JSONObject jsonObject = null;
    try {
      object = new JSONParser().parse(new FileReader(filename));
      jsonObject = (JSONObject) object;
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return jsonObject;
  }
}
