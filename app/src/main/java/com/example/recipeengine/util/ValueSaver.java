package com.example.recipeengine.util;

import androidx.annotation.NonNull;

import java.util.Set;

public interface ValueSaver {
  String hardwareFactorFile = "HARDWARE_FACTORS";

  void setKey(@NonNull final String key, @NonNull final String value);

  String getValue(@NonNull final String key);

  Set<String> getKeySet();
}
