package com.example.recipeengine.util.debug;

import android.content.Context;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.recipeengine.R;
import com.example.recipeengine.instruction.handler.cook.status.RecipeContext;
import com.example.recipeengine.util.FullScreenActivity;
import com.example.recipeengine.util.ValueSaver;
import com.example.recipeengine.util.ValueSaverImpl_Factory;
import com.example.recipeengine.util.debug.components.ComponentCollection;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;

import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Provider;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

/**
 * @author Abarajithan
 */
public final class DebugActivity extends FullScreenActivity {

  private final CompositeDisposable disposables = new CompositeDisposable();
  private DebugViewModel viewModel;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_debug);

    final MaterialToolbar toolbar = findViewById(R.id.debug_toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setTitle(R.string.debug_cb_fragment);

    final NavController navController = Navigation.findNavController(this,
      R.id.debug_nav_host_fragment);

    final NavigationView navigationView = findViewById(R.id.debug_nav_view);
    NavigationUI.setupWithNavController(navigationView, navController);

    final DrawerLayout drawerLayout = findViewById(R.id.debug_drawer_layout);
    final AppBarConfiguration config = new AppBarConfiguration.Builder(navController.getGraph())
      .setOpenableLayout(drawerLayout)
      .build();
    NavigationUI.setupWithNavController(toolbar, navController, config);

    ComponentCollection.init();
    // Create a dummy recipe context
    final SimpleDateFormat format = new SimpleDateFormat("ddMMyy-hhmm", Locale.getDefault());
    final String debugSession = format.format(new Date());
    final RecipeContext recipeContext = new RecipeContext(1, "");
    final ValueSaver valueSaver = ValueSaverImpl_Factory.provideInstance(this::getApplicationContext);
    final DebugViewModelFactory factory = new DebugViewModelFactory(recipeContext, valueSaver);
    // Init View Model
    this.viewModel = new ViewModelProvider(this, factory).get(DebugViewModel.class);
    try {
      this.viewModel.init(getApplicationContext());
    } catch (InterruptedException e) {
      Timber.e(e);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.debug_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (R.id.menu_debug_exit == item.getItemId()) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

    /*private boolean filterCondition(Message message) {
        return *//*Constants.Macro == message.getSubSystem()
                && Constants.Pan_Loadcell == message.getId()
                && Constants.Request_Response == message.getType();*//*true;
    }*/

  @Override
  protected void onStart() {
    super.onStart();
    subscribeMessages();
  }

  private void subscribeMessages() {

  }

    /*VisionController getVisionController() {
        return this.viewModel.visionController();
    }*/

    /*BaseHardwareService getHardwareService() {
        return this.viewModel.hardwareService();
    }*/

  @Override
  protected void onStop() {
    super.onStop();
    //this.viewModel.hardwareService().cleanup();
    if (!this.disposables.isDisposed()) {
      this.disposables.dispose();
    }
    FrequencyScheduler.disposeAll();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    FrequencyScheduler.disposeAll();
    //NativeUtil.StopInductionControl();
    //NativeUtil.StopHardwareService();
  }
}
