package com.example.recipeengine.util.debug.fragment;


import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_DEVICE_CONTROLS_MAIN;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * Fragment for device related controls
 *
 * @author Abarajithan
 */
public class DeviceControlsDebugFragment extends ComponentTabsFragment {

    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_device_controls_titles);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{ComponentListFragment.create(ID_DEVICE_CONTROLS_MAIN)};
    }
}
