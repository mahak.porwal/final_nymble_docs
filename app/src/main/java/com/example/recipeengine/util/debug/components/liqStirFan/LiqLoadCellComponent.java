package com.example.recipeengine.util.debug.components.liqStirFan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentLiqLoadCellBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * Load cell values (in grams) with freq control
 *
 * @author Abarajithan
 */
public final class LiqLoadCellComponent extends DebugComponent<ComponentLiqLoadCellBinding> {

  private Disposable disposable;


  public LiqLoadCellComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentLiqLoadCellBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentLiqLoadCellBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentLiqLoadCellBinding binding) {
    binding.start.setOnClickListener(v -> {
      final int freq = Integer.parseInt(binding.frequency.getText().toString());
      start(freq);
    });
    binding.stop.setOnClickListener(v -> stop());
    getBinding().stop.setEnabled(false);
  }

  private void start(final int freq) {
    getBinding().start.setEnabled(false);
    getBinding().stop.setEnabled(true);
    final int periodInMilliSecond = 1000 / freq;
    final Supplier<Float> supplier = DebugGuiMethods::getLoadCellReading_Grams;
    this.disposable = FrequencyScheduler.scheduleAPeriodicPollAndConsumeOnMainThread(
      periodInMilliSecond, supplier, this::onResponse);
  }

  private void stop() {
    this.disposable.dispose();
    getBinding().start.setEnabled(true);
    getBinding().stop.setEnabled(false);
  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
        /*final Double sensorValue = message.getDoubleKeyValue(Constants.Primary);
        this.setStatus(String.valueOf(sensorValue));*/
    this.setStatus(message);
  }
}
