package com.example.recipeengine.util.debug.components.liqStirFan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentLiqBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.functions.Action;

/**
 * ON and OFF Control for oil and water pump, with STOP button (not weight dependent)
 *
 * @author Abarajithan
 */
public final class LiqComponent extends DebugComponent<ComponentLiqBinding> {

  public static final String[] PUMPS = {"Water", "Oil"};

  private static final int PUMP_ON = 1;

  private static final int PUMP_OFF = 0;

  public LiqComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentLiqBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentLiqBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentLiqBinding binding) {
    binding.pumps.setAdapter(new SpinnerAdapter(getContext(), PUMPS));
    binding.start.setOnClickListener(v -> {
      final int pumpIndex = binding.pumps.getSelectedItemPosition();
      setPumpState(pumpIndex, PUMP_ON);
    });
    binding.stop.setOnClickListener(v -> {
      final int pumpIndex = binding.pumps.getSelectedItemPosition();
      setPumpState(pumpIndex, PUMP_OFF);
    });
  }

  private void setPumpState(final int pumpIndex, final int state) {
    final Action action;
    switch (PUMPS[pumpIndex]) {
      case "Water":
        if (PUMP_ON == state) {
          action = DebugGuiMethods::waterPumpOn;
        } else {
          action = DebugGuiMethods::waterPumpOff;
        }
        break;
      case "Oil":
        if (PUMP_ON == state) {
          action = DebugGuiMethods::oilPumpOn;
        } else {
          action = DebugGuiMethods::oilPumpOff;
        }
        break;
      default:
        return;
    }
    FrequencyScheduler.runAsynchronously(action, () -> {
    });
  }


  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }
}
