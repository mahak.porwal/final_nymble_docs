package com.example.recipeengine.util;

import io.reactivex.functions.Consumer;


public final class Retry {
  private Retry() {
  }

  /**
   * Calls consumer with the argument argumentForConsumer, if the call completes without an
   * exception, the method returns.
   * If the call doesn't completes and throws an error, then the error can be of type expectedException
   * or any other type.
   * If error of expectedException type, then the method call is retried maximum of maxAttemptCount.
   * If the error is not of expectedException type, then the method bubbles up the error.
   *
   * @param consumer            method to execute
   * @param argumentForConsumer argument passed to the method
   * @param maxAttemptCount     maximum number of retries after which the method will throw an error
   * @param expectedException   expected exception, after which method will retry
   * @param errMessage          error message to be thrown, when the retries are exceeded
   * @param <T>                 type of the argument for the consumer
   * @param <E>                 expected exception type
   * @throws Exception
   */
  public static <T, E> void Do(Consumer<T> consumer, T argumentForConsumer,
                               int maxAttemptCount, Class<E> expectedException,
                               String errMessage) throws Exception {

    for (int i = 0; i < maxAttemptCount; ) {
      try {
        consumer.accept(argumentForConsumer);
        return;
      } catch (Exception exception) {
        if (expectedException.isInstance(exception)) {
          i++;
        } else {
          throw exception;
        }
      }
    }
    throw new Exception(errMessage);
  }
}
