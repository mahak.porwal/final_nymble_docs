package com.example.recipeengine.util.debug.components.system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentI2cSystemBinding;
import com.example.recipeengine.util.Native.NativeUtil;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.lang.annotation.Native;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

/**
 * System I2C Device list
 *
 * @author Abarajithan
 */
public final class I2cSystemComponent extends DebugComponent<ComponentI2cSystemBinding> {

  public I2cSystemComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  private static final List<Integer> i2cBuses = new ArrayList<>();

  private static final Map<Byte, String> i2cAddressList = new HashMap<>();

  private static final byte PCA_9535 = 0X20;
  private static final byte PCA_9685 = 0X42;
  private static final byte STIRRER_DS105 = 0X2E;
  private static final byte LED_DS105 = 0X2F;
  private static final byte EX_FAN_DS105 = 0X28;
  private static final byte COUNTER_IC = 0X32;
  private static final byte MCP3426_ADC = 0X6D;
  private static final byte LOAD_CELL_ADC = 0X47;
  private static final byte INA219_ADC = 0X40;

  @Override
  public ComponentI2cSystemBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentI2cSystemBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentI2cSystemBinding binding) {
    i2cBuses.add(2);
    i2cBuses.add(3);
    i2cBuses.add(7);
    binding.i2cDeviceList.setOnClickListener(v -> detectDeviceList());
    Timber.d("Device requested");
    binding.i2cDeviceList.setOnClickListener(v -> detectDeviceList());
    this.i2cAddressList.put(PCA_9535, "PCA_9535");
    this.i2cAddressList.put(PCA_9685, "PCA_9685");
    this.i2cAddressList.put(EX_FAN_DS105, "EX_FAN_DS105");
    this.i2cAddressList.put(LED_DS105, "LED_DS105");
    this.i2cAddressList.put(STIRRER_DS105, "STIRRER_DS105");
    this.i2cAddressList.put(COUNTER_IC, "COUNTER_IC");
    this.i2cAddressList.put(MCP3426_ADC, "MCP3426_ADC");
    this.i2cAddressList.put(LOAD_CELL_ADC, "LOAD_CELL_ADC");
    this.i2cAddressList.put(INA219_ADC, "INA219_ADC");
  }

  private void detectDeviceList() {
    final StringBuilder stringBuilder = new StringBuilder();
    getBinding().i2cDeviceList.setEnabled(false);
    getBinding().statusText.setText("");
    for (Integer bus : i2cBuses) {
      final Map<String, List<Double>> map = NativeUtil.ScanI2CBus(bus.intValue());
      List<Double> i2cDevices = null;
      for (List<Double> list :
        map.values()) {
        i2cDevices = list;
      }
      stringBuilder.append("I2C-" + bus + " \n");

      if (null != i2cDevices) {
        for (Double device : i2cDevices) {
          final byte device_byte = device.byteValue();
          stringBuilder
            .append(String.format(this.i2cAddressList.getOrDefault(device_byte, "Unknown") +
              "~" + "0x%02X \n", device_byte));
        }
      }
    }

    this.setStatus(stringBuilder.toString());
    getBinding().i2cDeviceList.setEnabled(true);
    Toast.makeText(getContext(), "Updated device list", Toast.LENGTH_LONG);
  }

  private void setStatus(@NonNull final String message) {
    getBinding().statusText.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }
}
