package com.example.recipeengine.util.debug.components.system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentInputIna300ssSystemBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;
/**
 * Input Read - INA300_SS Alert, INA300 Alert
 *
 * @author Abarajithan
 */
public final class InputIna300ssSystemComponent extends DebugComponent<ComponentInputIna300ssSystemBinding> {

    private static final String[] INPUT_PINS = {"INA300_SS Alert", "INA300 Alert"};

    public InputIna300ssSystemComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentInputIna300ssSystemBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentInputIna300ssSystemBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentInputIna300ssSystemBinding binding) {
        binding.inputPins.setAdapter(new SpinnerAdapter(getContext(), INPUT_PINS));
        binding.start.setOnClickListener(v -> {
            final int pinIndex = binding.inputPins.getSelectedItemPosition();
            final int freq = Integer.parseInt(binding.frequency.getText().toString());
            start(pinIndex, freq);
        });
        binding.stop.setOnClickListener(v -> {
            final int pinIndex = binding.inputPins.getSelectedItemPosition();
            stop(pinIndex);
        });
    }

    private void start(final int pinIndex, final int freq) {
        /*final Message message = new Message(Board.MPU);
        message.setType(Constants.MpuMcuRequest);
        Map<String, Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary), freq);
        switch (INPUT_PINS[pinIndex]) {
            case "INA300_SS Alert":
                message.setId(Constants.S_INA300_SS_Alert);
                break;
            case "INA300 Alert":
                message.setId(Constants.S_INA300_Alert);
                break;
        }
        message.setValue(value);
        getViewModel().sendToHardware(message);*/
    }

    private void stop(final int pinIndex) {
        this.start(pinIndex, 0);
    }

    private void setStatus(@NonNull final String message) {
        getBinding().status.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {
        /*final Integer pinState = message.getIntegerKeyValue(Constants.Primary);
        if (GPIOEnum.LOW == pinState) {
            this.setStatus("LOW");
        } else {
            this.setStatus("HIGH");
        }*/
    }
}
