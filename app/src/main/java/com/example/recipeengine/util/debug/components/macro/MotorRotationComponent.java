package com.example.recipeengine.util.debug.components.macro;

import android.content.Context;
import android.os.Debug;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentMotorRotationMacroBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.functions.Action;

/**
 * Macro Motor rotation - Macro container, angle and speed selection; with STOP functionality
 *
 * @author Abarajithan
 */
public final class MotorRotationComponent extends DebugComponent<ComponentMotorRotationMacroBinding> {

  public static final String[] MACROS = {"Macro 1", "Macro 2", "Macro 3", "Macro 4"};

  public MotorRotationComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentMotorRotationMacroBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentMotorRotationMacroBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentMotorRotationMacroBinding binding) {
    binding.motors.setAdapter(new SpinnerAdapter(getContext(), MACROS));
    binding.start.setOnClickListener(v -> {
      final int speed = Integer.parseInt(binding.speed.getText().toString());
      final int angle = Integer.parseInt(binding.angle.getText().toString());
      final int motorIndex = binding.motors.getSelectedItemPosition();
      start(speed, angle, motorIndex);
    });
    binding.stop.setOnClickListener(v -> {
      final int motorIndex = binding.motors.getSelectedItemPosition();
      stop(motorIndex);
    });
  }

  private void start(int speed, final int angle, final int motorIndex) {
    getBinding().start.setEnabled(false);
    getBinding().stop.setEnabled(true);
    final Action action;
    switch (motorIndex) {
      case 0:
        action = () -> DebugGuiMethods.macroServoSequence_Macro_1(angle, speed);
        break;
      case 1:
        action = () -> DebugGuiMethods.macroServoSequence_Macro_2(angle, speed);
        break;
      case 2:
        action = () -> DebugGuiMethods.macroServoSequence_Macro_3(angle, speed);
        break;
      case 3:
        action = () -> DebugGuiMethods.macroServoSequence_Macro_4(angle, speed);
        break;
      default:
        return;
    }
    FrequencyScheduler.runAsynchronously(action, () -> {
      getBinding().start.setEnabled(true);
      getBinding().stop.setEnabled(false);
    });
        /*if(speed > 10){
            speed = 10;
        }
        double speed_ = speed;
        speed_ = speed/5.0;
        final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuCommand);
        message.setSubSystem(Constants.Macro);
        message.setId(Constants.Macro_Mot_1 + motorIndex);
        Map<String, Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary), angle);
        value.put(String.valueOf(Constants.Secondary), speed_);
        message.setValue(value);
        getViewModel().sendToHardware(message);*/
  }

  private void stop(final int motorIndex) {
  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }
}
