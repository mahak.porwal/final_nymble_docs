package com.example.recipeengine.util.debug.components.liqStirFan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentExfanBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * Exhaust Fan speed control, with STOP Button
 *
 * @author Abarajithan
 */
public final class ExFanComponent extends DebugComponent<ComponentExfanBinding> {

  public ExFanComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentExfanBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentExfanBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentExfanBinding binding) {
    binding.start.setOnClickListener(v -> {
      Integer speed;
      try {
        speed = Integer.parseInt(binding.speed.getText().toString());
      } catch (NumberFormatException e) {
        speed = 0;
      }
      start(speed);
    });
    binding.stop.setOnClickListener(v -> stop());
  }

  private void start(final Integer speed) {
    FrequencyScheduler.runAsynchronously(() -> DebugGuiMethods.turnOnExhaustFan(speed),
      () -> {
      });
  }

  private void stop() {
    FrequencyScheduler.runAsynchronously(DebugGuiMethods::turnOffExhaustFan,
      () -> {
      });
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }
}
