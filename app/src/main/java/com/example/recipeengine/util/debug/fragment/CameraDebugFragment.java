package com.example.recipeengine.util.debug.fragment;

import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_CAMERA_ARUKO_MARKER;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_CAMERA_THERMAL;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * Camera related debug screens
 *
 * @author Abarajithan
 */
public final class CameraDebugFragment extends ComponentTabsFragment {
    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_camera_fragment_title);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{
                ComponentListFragment.create(ID_CAMERA_ARUKO_MARKER),
                ComponentListFragment.create(ID_CAMERA_THERMAL)
        };
    }
}
