package com.example.recipeengine.util.debug.components.camera;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentThermalCaptureCameraBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.concurrent.TimeUnit;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Thermal image button - Shows thermal image on the screen
 *
 * @author Abarajithan
 */
public final class ThermalCaptureComponent extends DebugComponent<ComponentThermalCaptureCameraBinding> {

    private static final int TIMEOUT_FOR_SAVING_THERMAL_AND_RGB_IMAGE = 4000;

    public ThermalCaptureComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentThermalCaptureCameraBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentThermalCaptureCameraBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentThermalCaptureCameraBinding binding) {
        binding.capture.setOnClickListener(v -> captureThermalImage());
        binding.save.setOnClickListener(v -> saveBitmap());
        subscribeToThermalImages();
    }

    private void saveBitmap() {/*
        add(getViewModel()
                .visionController()
                .getArucoAngleForDebug()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(l -> {
                    getBinding().save.setEnabled(false);
                    getViewModel().visionController().saveThermalAndRgb();
                })
                .filter(object->!object.isPresent())
                .timeout(TIMEOUT_FOR_SAVING_THERMAL_AND_RGB_IMAGE, TimeUnit.MILLISECONDS)
                .take(1)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(l -> Toast.makeText(getContext(),
                        "Images saved !!", Toast.LENGTH_SHORT).show())
                .subscribe(l -> getBinding().save.setEnabled(true), err -> {
                    Toast.makeText(getContext(),
                            "Failed to save image !!", Toast.LENGTH_SHORT).show();
                    getBinding().save.setEnabled(true);
                }));*/
    }

    private void subscribeToThermalImages() {
        /*add(getViewModel()
                .visionController()
                .getThermalImageForDebug()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setPreviewImage));*/
    }

    private void captureThermalImage() {
        /*getViewModel().visionController().getThermalImage();*/
    }

    private void setPreviewImage(@NonNull final Bitmap bitmap) {
        /*Glide.with(getContext()).asBitmap().load(bitmap).into(getBinding().thermalPreview);
        Toast.makeText(getContext(), "Image captured!", Toast.LENGTH_SHORT).show();*/
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {

    }
}
