package com.example.recipeengine.util.debug.fragment;


import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_MACRO_LOAD_CELL;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_MACRO_MOTOR;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * Macro debugging fragment
 *
 * @author Abarajithan
 */
public final class MacroDebugFragment extends ComponentTabsFragment {
    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_macro_fragment_title);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{
                ComponentListFragment.create(ID_MACRO_MOTOR)/*,
                ComponentListFragment.create(ID_MACRO_LOAD_CELL)*/
        };
    }
}
