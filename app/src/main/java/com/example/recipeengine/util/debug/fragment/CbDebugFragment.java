package com.example.recipeengine.util.debug.fragment;

import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_CB_GPIO;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_CB_I2C_MCU;
import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_CB_MAIN;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * CB Debug fragment
 *
 * @author Abarajithan
 */
public final class CbDebugFragment extends ComponentTabsFragment {

    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_cb_fragment_title);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{
                ComponentListFragment.create(ID_CB_MAIN),
                ComponentListFragment.create(ID_CB_GPIO),
                ComponentListFragment.create(ID_CB_I2C_MCU)
        };
    }
}
