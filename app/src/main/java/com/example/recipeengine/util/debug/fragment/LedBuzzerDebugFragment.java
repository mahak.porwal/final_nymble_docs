package com.example.recipeengine.util.debug.fragment;


import static com.example.recipeengine.util.debug.components.ComponentCollection.ID_LED_BUZZER_MAIN;

import com.example.recipeengine.R;
import com.example.recipeengine.util.debug.ComponentListFragment;

/**
 * Led & Buzzer debug fragment
 *
 * @author Abarajithan
 */
public final class LedBuzzerDebugFragment extends ComponentTabsFragment {
    @Override
    public String[] tabTitles() {
        return getResources().getStringArray(R.array.debug_led_buzzer_fragment_title);
    }

    @Override
    public ComponentListFragment[] fragments() {
        return new ComponentListFragment[]{ComponentListFragment.create(ID_LED_BUZZER_MAIN)};
    }
}
