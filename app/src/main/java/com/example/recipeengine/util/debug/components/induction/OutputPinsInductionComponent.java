package com.example.recipeengine.util.debug.components.induction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentOutputPinsInductionBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.functions.Action;

/**
 * Outputs Pins - S_PAN, S_INT, DAC, Fan control
 *
 * @author Abarajithan
 */
public final class OutputPinsInductionComponent extends DebugComponent<ComponentOutputPinsInductionBinding> {

  private static final String[] PINS = {"S_PAN", "S_INT", "Fan control"};

  public OutputPinsInductionComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentOutputPinsInductionBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentOutputPinsInductionBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentOutputPinsInductionBinding binding) {
    binding.outputPins.setAdapter(new SpinnerAdapter(getContext(), PINS));
    binding.high.setOnClickListener(v -> {
      final int pinIndex = binding.outputPins.getSelectedItemPosition();
      setGpioState(pinIndex, 1);
    });
    binding.low.setOnClickListener(v -> {
      final int pinIndex = binding.outputPins.getSelectedItemPosition();
      setGpioState(pinIndex, 0);
    });
  }

  private void setGpioState(final int pinIndex, final int state) {
    final Action action;
    switch (PINS[pinIndex]) {
      case "S_PAN":
        action = () -> DebugGuiMethods.setInductionPanPin(state);
        break;
      case "S_INT":
        action = () -> DebugGuiMethods.setInductionIntPin(state);
        break;
      case "Fan control":
        action = () -> DebugGuiMethods.setInductionFanPin(state);
                /*message.setMcuId(Board.CONTROL);
                message.setSubSystem(Constants.Micro);
                message.setId(Constants.Induction_Fan);
                value.put(String.valueOf(Constants.Primary),
                  GPIOEnum.HIGH == state ?
                    PowerLevelComponent.INDUCTION_FAN_FULL_SPEED : 0);*/
        break;
      default:
        return;
    }
    FrequencyScheduler.runAsynchronously(action, () -> {
    });
        /*final Message message = new Message(Board.INDUCTION);
        message.setType(Constants.MpuMcuCommand);
        final Map<String, Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary), state);
        switch (PINS[pinIndex]) {
            case "S_PAN":
                message.setId(Constants.S_PAN);
                break;
            case "S_INT":
                message.setId(Constants.S_INT);
                break;
            case "Fan control":
                message.setMcuId(Board.CONTROL);
                message.setSubSystem(Constants.Micro);
                message.setId(Constants.Induction_Fan);
                value.put(String.valueOf(Constants.Primary),
                        GPIOEnum.HIGH == state ?
                                PowerLevelComponent.INDUCTION_FAN_FULL_SPEED : 0);
                break;
        }
        message.setValue(value);
        getViewModel().sendToHardware(message);*/
  }


  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {

  }
}
