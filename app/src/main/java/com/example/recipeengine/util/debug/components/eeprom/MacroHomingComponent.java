package com.example.recipeengine.util.debug.components.eeprom;

import static com.example.recipeengine.util.debug.NonVolatileKeys.MACRO_FOUR_HOME;
import static com.example.recipeengine.util.debug.NonVolatileKeys.MACRO_ONE_HOME;
import static com.example.recipeengine.util.debug.NonVolatileKeys.MACRO_THREE_HOME;
import static com.example.recipeengine.util.debug.NonVolatileKeys.MACRO_TWO_HOME;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentMacroHomingEepromBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.ValueSaver;
import com.example.recipeengine.util.debug.components.DebugComponent;
import com.example.recipeengine.util.debug.components.macro.MotorRotationComponent;

/**
 * Homing position for each Macro container
 *
 * @author Abarajithan
 */
public final class MacroHomingComponent extends DebugComponent<ComponentMacroHomingEepromBinding> {
  private final ValueSaver valueSaver;

  public MacroHomingComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
    this.valueSaver = viewModel.getValueSaver();
  }

  @Override
  public ComponentMacroHomingEepromBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentMacroHomingEepromBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentMacroHomingEepromBinding binding) {
    binding.macros.setAdapter(new SpinnerAdapter(getContext(), MotorRotationComponent.MACROS));
    binding.set.setOnClickListener(v -> {
      final int macroIndex = binding.macros.getSelectedItemPosition();
      final int value = Integer.parseInt(binding.homingValue.getText().toString());
      setHomingValue(macroIndex, value);
    });
    binding.read.setOnClickListener(v -> {
      final int macroIndex = binding.macros.getSelectedItemPosition();
      readEeprom(macroIndex);
    });
  }

  private void readEeprom(final int macroIndex) {
    final String key;
    switch (macroIndex) {
      case 0:
        key = MACRO_ONE_HOME.name();
        break;
      case 1:
        key = MACRO_TWO_HOME.name();
        break;
      case 2:
        key = MACRO_THREE_HOME.name();
        break;
      case 3:
        key = MACRO_FOUR_HOME.name();
        break;
      default:
        return;
    }
    final String value = this.valueSaver.getValue(key);
    if (null != value) {
      this.setStatus(value);
    } else {
      this.setStatus("No value found");
    }
  }

  private void setHomingValue(final int macroIndex, final int value) {
    final String key;
    switch (macroIndex) {
      case 0:
        key = MACRO_ONE_HOME.name();
        break;
      case 1:
        key = MACRO_TWO_HOME.name();
        break;
      case 2:
        key = MACRO_THREE_HOME.name();
        break;
      case 3:
        key = MACRO_FOUR_HOME.name();
        break;
      default:
        return;
    }
    this.valueSaver.setKey(key, Integer.valueOf(value).toString());
  }

  private void setStatus(@NonNull final String message) {
    this.getBinding().statusText.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
        /*final Integer eepromValue = message.getIntegerKeyValue(Constants.Primary);
        this.setStatus(String.valueOf(eepromValue));*/
  }
}
