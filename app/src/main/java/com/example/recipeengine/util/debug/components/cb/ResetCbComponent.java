package com.example.recipeengine.util.debug.components.cb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentResetCbBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

/**
 * Debug component for resetting MCU
 *
 * @author Abarajithan
 */
public final class ResetCbComponent extends DebugComponent<ComponentResetCbBinding> {

    public ResetCbComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentResetCbBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentResetCbBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentResetCbBinding binding) {
        /*binding.resetControl.setOnClickListener(v ->
                getViewModel().hardwareService().resetBoard(null));*/
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {

    }
}
