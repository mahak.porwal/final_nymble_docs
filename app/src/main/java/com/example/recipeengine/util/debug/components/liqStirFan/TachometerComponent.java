package com.example.recipeengine.util.debug.components.liqStirFan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentTachometerBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

/**
 * Tachometer reading display, with freq control
 *
 * @author Abarajithan
 */
public final class TachometerComponent extends DebugComponent<ComponentTachometerBinding> {

    public TachometerComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentTachometerBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentTachometerBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentTachometerBinding binding) {
        binding.start.setOnClickListener(v -> {
            final int freq = Integer.parseInt(binding.frequency.getText().toString());
            start(freq);
        });
        binding.stop.setOnClickListener(v -> stop());
    }

    private void start(final int freq) {
        // TODO
    }

    private void stop() {
        // TODO
    }

    private void setStatus(@NonNull final String message) {
        getBinding().status.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {

    }
}
