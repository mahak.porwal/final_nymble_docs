package com.example.recipeengine.util.debug.components.micro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentPodHomingBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.Collections;
import java.util.concurrent.TimeUnit;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Homing feature, with selection of Pod
 *
 * @author Abarajithan
 */
public final class PodHomingComponent extends DebugComponent<ComponentPodHomingBinding> {

    public PodHomingComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentPodHomingBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentPodHomingBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentPodHomingBinding binding) {
        binding.pods.setAdapter(new SpinnerAdapter(getContext(), PodRotationComponent.MOTORS));
        binding.homing.setOnClickListener(v -> {
            final int podIndex = binding.pods.getSelectedItemPosition();
            podHoming(podIndex);
        });
    }

    private void podHoming(final int podIndex) {
        /*final int microId = podIndex + Constants.Vib_Mot_1;

        final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuAction);
        message.setSubSystem(Constants.Micro);
        message.setId(microId);
        message.setValue(Collections.singletonMap(
                String.valueOf(Constants.Primary), 1));
        getViewModel().sendToHardware(message);*/
    }

    private void setStatus(@NonNull final String message) {
        getBinding().status.setText(message);
    }

    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {
        /*final Message ackResponse = Messages.ack(Board.CONTROL);
        ackResponse.setSeqNo(message.getSeqNo());
        getViewModel().sendACK(ackResponse);

        final LogDetail logDetail = new LogDetail(LoggingEvents.DISPENSE_STOP,
                TriggerEvents.MANUAL, Constants.nameOf(message.getId()));
        getViewModel().visionController().logDetail(logDetail);

        add(Observable.just(1)
                .delay(1, TimeUnit.SECONDS)
                .doOnNext(l -> getViewModel().visionController().sensorDataController(1))
                .concatMap(l -> Observable.just(l).delay(1, TimeUnit.SECONDS))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(l -> getBinding().homing.setEnabled(true))
                .subscribe());*/
    }
}
