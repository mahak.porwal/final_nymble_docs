package com.example.recipeengine.util.debug.components.induction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentPanDetectBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.function.Supplier;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
 * Detect the pan
 *
 * @author Abarajithan
 */
public final class PanDetectComponent extends DebugComponent<ComponentPanDetectBinding> {

  public PanDetectComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentPanDetectBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentPanDetectBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentPanDetectBinding binding) {
    binding.panDetect.setOnClickListener(v -> detectPan());
  }

  private void detectPan() {
    getBinding().panDetect.setEnabled(false);
    final Supplier<String> panDetector = DebugGuiMethods::getPanDetectionStatus;
    final Consumer<String> panDetectStatusConsumer = this::onResponse;
    FrequencyScheduler.runAsynchronouslyAndConsumeOnMainThread(panDetector,
      panDetectStatusConsumer);
  }

  private void setStatus(@NonNull final String message) {
    getBinding().status.setText(message);
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
    this.setStatus(message);
    getBinding().panDetect.setEnabled(true);
  }
}
