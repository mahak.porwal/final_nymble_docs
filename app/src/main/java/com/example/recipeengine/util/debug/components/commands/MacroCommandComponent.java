package com.example.recipeengine.util.debug.components.commands;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentMacroCommandsBinding;
import com.example.recipeengine.util.DebugGuiMethods;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.FrequencyScheduler;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;
import com.example.recipeengine.util.debug.components.macro.MotorRotationComponent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import timber.log.Timber;

/**
 * Selection of Macro and no of hits, with checkbox for liquid ingredient
 *
 * @author Abarajithan
 */
public final class MacroCommandComponent extends DebugComponent<ComponentMacroCommandsBinding> {

  public MacroCommandComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
    super(context, viewModel);
  }

  @Override
  public ComponentMacroCommandsBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
    return ComponentMacroCommandsBinding.inflate(inflater, parent, false);
  }

  @Override
  public void onCreateView(@NonNull ComponentMacroCommandsBinding binding) {
    binding.macros.setAdapter(new SpinnerAdapter(getContext(), MotorRotationComponent.MACROS));
    binding.start.setOnClickListener(v -> {
      final int macroIndex = binding.macros.getSelectedItemPosition();
      final String triesStr = binding.tries.getText().toString().trim();
      if (triesStr.isEmpty()) {
        Toast.makeText(getContext(), "Tries required!", Toast.LENGTH_SHORT).show();
        return;
      }
      final int tries = Integer.parseInt(triesStr);
      final boolean isLiquid = binding.isLiquidCheckbox.isChecked();
      start(macroIndex, tries, isLiquid);
    });
    binding.stop.setOnClickListener(v -> {
      final int macroIndex = binding.macros.getSelectedItemPosition();
      stop(macroIndex);
    });
  }

  private void start(final int macroIndex, final int tries, final boolean isLiquid) {
    final Action action;
    enableStartButton(false);
    enableStopButton(true);
    switch (macroIndex) {
      case 0:
        if (isLiquid) {
          action = () -> DebugGuiMethods.macroDispense_Macro_1(tries,
            "MACRO_LIQUID_DISPENSE");
        } else {
          action = () -> DebugGuiMethods.macroDispense_Macro_1(tries,
            "MACRO_NON_LIQUID_DISPENSE");
        }
        break;
      case 1:
        if (isLiquid) {
          action = () -> DebugGuiMethods.macroDispense_Macro_2(tries,
            "MACRO_LIQUID_DISPENSE");
        } else {
          action = () -> DebugGuiMethods.macroDispense_Macro_2(tries,
            "MACRO_NON_LIQUID_DISPENSE");
        }
        break;
      case 2:
        if (isLiquid) {
          action = () -> DebugGuiMethods.macroDispense_Macro_3(tries,
            "MACRO_LIQUID_DISPENSE");
        } else {
          action = () -> DebugGuiMethods.macroDispense_Macro_3(tries,
            "MACRO_NON_LIQUID_DISPENSE");
        }
        break;
      case 3:
        if (isLiquid) {
          action = () -> DebugGuiMethods.macroDispense_Macro_4(tries,
            "MACRO_LIQUID_DISPENSE");
        } else {
          action = () -> DebugGuiMethods.macroDispense_Macro_4(tries,
            "MACRO_NON_LIQUID_DISPENSE");
        }
        break;
      default:
        return;
    }

    FrequencyScheduler.runAsynchronously(action, () -> {
      enableStartButton(true);
      enableStopButton(false);
    });
        /*enableStartButton(false);
        enableStopButton(true);

        final int macroId = macroIndex + Constants.Macro_Mot_1;
        final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuAction);
        message.setSubSystem(Constants.Macro);
        message.setId(macroId);

        final int liqId = isLiquid ? Liquid_Macro : Non_Liquid_Macro;
        final Map<String, Object> valueMap = new HashMap<>();
        valueMap.put(String.valueOf(Constants.Primary), tries);
        valueMap.put(String.valueOf(Constants.Secondary), liqId);
        message.setValue(valueMap);

        add(Observable.just(1)
                .delay(1, TimeUnit.SECONDS)
                .doOnNext(l -> getViewModel().visionController().sensorDataController(0))
                .concatMap(l -> Observable.just(l).delay(2, TimeUnit.SECONDS))
                .subscribe(l -> {
                    getViewModel().sendToHardware(message);
                    final Map<String, Object> logDetailValue = new HashMap<>();
                    logDetailValue.put(LogDetail.eventAmount, tries);
                    logDetailValue.put(LogDetail.eventType, isLiquid ? nameOf(Liquid_Macro) : nameOf(Non_Liquid_Macro));

                    final LogDetail logDetail = new LogDetail(LoggingEvents.DISPENSE_START,
                            TriggerEvents.MANUAL, nameOf(macroId), logDetailValue);
                    getViewModel().visionController().logDetail(logDetail);
                }));*/
  }

  private void enableStartButton(final boolean enable) {
    getBinding().start.setEnabled(enable);
  }

  private void enableStopButton(final boolean enable) {
    getBinding().stop.setEnabled(enable);
  }

  private void stop(final int macroIndex) {
    enableStopButton(false);
    final Action action;
    switch (macroIndex) {
      case 0:
        action = DebugGuiMethods::abortMacroDispense_Macro_1;
        break;
      case 1:
        action = DebugGuiMethods::abortMacroDispense_Macro_2;
        break;
      case 2:
        action = DebugGuiMethods::abortMacroDispense_Macro_3;
        break;
      case 3:
        action = DebugGuiMethods::abortMacroDispense_Macro_4;
        break;
      default:
        return;
    }
    FrequencyScheduler.runAsynchronously(action, () -> {
    });
    //this.enableStopButton(false);
        /*final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuAction);
        message.setSubSystem(Constants.Macro);
        message.setId(Constants.Macro_Abort_Dispense);
        getViewModel().sendToHardware(message);*/
  }

  @Override
  public void onBindView() {

  }

  @Override
  public void onResponse(String message) {
        /*final Message ackResponse = Messages.ack(Board.CONTROL);
        ackResponse.setSeqNo(message.getSeqNo());
        getViewModel().sendACK(ackResponse);

        final LogDetail logDetail = new LogDetail(LoggingEvents.DISPENSE_STOP,
                TriggerEvents.MANUAL, Constants.nameOf(message.getId()));
        getViewModel().visionController().logDetail(logDetail);

        add(Observable.just(1)
                .doOnNext(l -> getViewModel().visionController().sensorDataController(1))
                .concatMap(l -> Observable.just(l).delay(1, TimeUnit.SECONDS))
                .observeOn(AndroidSchedulers.mainThread())
                .map(integer -> "a")
                .doOnNext(l -> Timber.d(l))
                .subscribe(l -> Toast.makeText(getContext(),
                        "Macro dispense done!", Toast.LENGTH_SHORT).show()));
        enableStartButton(true);
        enableStopButton(true);*/
  }
}
