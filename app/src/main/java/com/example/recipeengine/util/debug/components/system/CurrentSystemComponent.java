package com.example.recipeengine.util.debug.components.system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentCurrentSystemBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * Supply current with Freq control
 *
 * @author Abarajithan
 */
public final class CurrentSystemComponent extends DebugComponent<ComponentCurrentSystemBinding> {

    public CurrentSystemComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentCurrentSystemBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentCurrentSystemBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentCurrentSystemBinding binding) {
        binding.start.setOnClickListener(v -> {
            final int frequency = Integer.parseInt(binding.frequency.getText().toString());
            start(frequency);
        });
        binding.stop.setOnClickListener(v -> stop());
    }

    private void start(final int frequency) {
        /*final Message message = new Message(Board.CONTROL);
        message.setType(Constants.MpuMcuRequest);
        message.setSubSystem(Constants.ControlBoard);
        message.setId(Constants.SupplyCurrent);
        Map<String, Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary), frequency);
        message.setValue(value);
        getViewModel().sendToHardware(message);*/
    }

    private void stop() {
        this.start(0);
    }

    @Override
    public void onBindView() {

    }

    private void setStatus(@NonNull final String message) {
        getBinding().inputPinStatus.setText(message + " mA");
    }

    @Override
    public void onResponse(String message) {
        /*final Double sensorValue = message.getDoubleKeyValue(Constants.Primary);
        this.setStatus(String.valueOf(sensorValue));*/
    }
}
