package com.example.recipeengine.util.debug;

public enum NonVolatileKeys {
  LOAD_CELL_FACTOR,
  MACRO_ONE_HOME,
  MACRO_TWO_HOME,
  MACRO_THREE_HOME,
  MACRO_FOUR_HOME,
  OIL_PUMP_FACTOR,
  WATER_PUMP_FACTOR,
  PCA_FACTOR
}
