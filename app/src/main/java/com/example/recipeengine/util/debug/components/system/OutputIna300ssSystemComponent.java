package com.example.recipeengine.util.debug.components.system;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;

import com.example.recipeengine.databinding.ComponentOutputIna300ssSystemBinding;
import com.example.recipeengine.util.debug.DebugViewModel;
import com.example.recipeengine.util.debug.SpinnerAdapter;
import com.example.recipeengine.util.debug.components.DebugComponent;

import java.util.HashMap;
import java.util.Map;

/**
 * Output - INA300_SS Alert, INA300_SS Latch, INA300 Alert, INA300 Latch
 *
 * @author Abarajithan
 */
public final class OutputIna300ssSystemComponent extends DebugComponent<ComponentOutputIna300ssSystemBinding> {

    private static final int TURN_ON_GPIO = 1;
    private static final int TURN_OFF_GPIO = 0;
    private static final String[] OUTPUT_PINS = {
            "INA300_SS Alert", "INA300_SS Latch", "INA300 Alert", "INA300 Latch"
    };

    public OutputIna300ssSystemComponent(@NonNull Context context, @NonNull DebugViewModel viewModel) {
        super(context, viewModel);
    }

    @Override
    public ComponentOutputIna300ssSystemBinding onInflateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent) {
        return ComponentOutputIna300ssSystemBinding.inflate(inflater, parent, false);
    }

    @Override
    public void onCreateView(@NonNull ComponentOutputIna300ssSystemBinding binding) {
        binding.outputPins.setAdapter(new SpinnerAdapter(getContext(), OUTPUT_PINS));
        binding.high.setOnClickListener(v -> {
            final int pinIndex = binding.outputPins.getSelectedItemPosition();
            this.setState(pinIndex,TURN_ON_GPIO);
        });
        binding.low.setOnClickListener(v -> {
            final int pinIndex = binding.outputPins.getSelectedItemPosition();
            this.setState(pinIndex,TURN_OFF_GPIO);
        });
    }

    private void setState(final int pinIndex,final int state) {
        /*final Message message = new Message(Board.MPU);
        message.setType(Constants.MpuMcuCommand);
        Map<String,Object> value = new HashMap<>();
        value.put(String.valueOf(Constants.Primary),state);
        switch (OUTPUT_PINS[pinIndex]){
            case "INA300_SS Alert":
                message.setId(Constants.S_INA300_SS_Alert);
                break;
            case "INA300 Alert":
                message.setId(Constants.S_INA300_Alert);
                break;
            case "INA300_SS Latch":
                message.setId(Constants.INA300_SS_L);
                break;
            case "INA300 Latch":
                message.setMcuId(Board.CONTROL);
                message.setSubSystem(Constants.Micro);
                message.setId(Constants.INA300_L);
                break;
        }
        message.setValue(value);
        getViewModel().sendToHardware(message);*/
    }


    @Override
    public void onBindView() {

    }

    @Override
    public void onResponse(String message) {

    }
}
