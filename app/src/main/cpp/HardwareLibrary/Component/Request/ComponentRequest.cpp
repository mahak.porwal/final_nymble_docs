//
// Created by fedal on 12/4/21.
//

#include "ComponentRequest.h"

ComponentRequest_e ComponentRequest::GetRequestType() const {
    return request_type_;
}

ComponentRequest &ComponentRequest::SetRequestType(ComponentRequest_e requestType) {
    request_type_ = requestType;
    return *this;
}

float ComponentRequest::GetQuantity() const {
    return quantity_;
}

ComponentRequest &ComponentRequest::SetQuantity(float quantity) {
    quantity_ = quantity;
    return *this;
}

int ComponentRequest::GetSpeed() const {
    return speed_;
}

ComponentRequest &ComponentRequest::SetSpeed(int speed) {
    speed_ = speed;
    return *this;
}

int ComponentRequest::GetPosition() const {
    return position_;
}

ComponentRequest &ComponentRequest::SetPosition(int position) {
    position_ = position;
    return *this;
}

int ComponentRequest::GetFrequency() const {
    return frequency_;
}

ComponentRequest &ComponentRequest::SetFrequency(int frequency) {
    frequency_ = frequency;
    return *this;
}

ActuatorDirections_e ComponentRequest::GetDirection() const {
    return direction_;
}

ComponentRequest &ComponentRequest::SetDirection(ActuatorDirections_e direction) {
    direction_ = direction;
    return *this;
}

OnOffState_e ComponentRequest::GetState() const {
    return state_;
}

ComponentRequest &ComponentRequest::SetState(OnOffState_e state) {
    state_ = state;
    return *this;
}

int ComponentRequest::GetMuxSelect() const {
    return mux_select_;
}

ComponentRequest &ComponentRequest::SetMuxSelect(int muxSelect) {
    mux_select_ = muxSelect;
    return *this;
}

SequenceBehavior_e ComponentRequest::GetSequence() const {
    return sequence_;
}

ComponentRequest &
ComponentRequest::SetSequence(SequenceBehavior_e sequence) {
    sequence_ = sequence;
    return *this;
}

Component_e ComponentRequest::GetComponent() const {
    return component_;
}

ComponentRequest &ComponentRequest::SetComponent(Component_e component) {
    component_ = component;
    return *this;
}

SensorReadingUnits_e ComponentRequest::GetSensorUnit() const {
    return this->sensor_reading_unit_;
}

ComponentRequest &ComponentRequest::SetSensorUnit(SensorReadingUnits_e unit) {
    this->sensor_reading_unit_ = unit;
    return *this;
}

bool ComponentRequest::IsAbortSequence() const {
    return abort_sequence_;
}

void ComponentRequest::SetAbortSequence(bool abortSequence) {
    abort_sequence_ = abortSequence;
}

induction_control_input::InductionControlInput_e
ComponentRequest::GetInductionControlInput() const {
    return induction_control_input_;
}

ComponentRequest &
ComponentRequest::SetInductionControlInput(induction_control_input::InductionControlInput_e input) {
    induction_control_input_ = input;
    return *this;
}

ComponentRequest &ComponentRequest::SetInductionMachineReq(InductionStateMachineReq_e
                                                           state_machine_req) {
    this->state_machine_req_ = state_machine_req;
    return *this;
}

InductionStateMachineReq_e ComponentRequest::GetInductionMachineReq() const {
    return state_machine_req_;
}

ComponentRequest &ComponentRequest::SetEepromData(uint16_t data) {
    this->eeprom_data_ = data;
    return *this;
}

ComponentRequest &ComponentRequest::SetEepromNumOfSeqReads(uint16_t sequential_reads) {
    this->num_of_sequential_reads_ = sequential_reads;
    return *this;
}

ComponentRequest &ComponentRequest::SetEepromAddr(uint16_t address) {
    this->eeprom_address_ = address;
    return *this;
}

int ComponentRequest::GetEepromAddr() const {
    return this->eeprom_address_;
}

int ComponentRequest::GetEepromData() const {
    return this->eeprom_data_;
}

int ComponentRequest::GetEepromNumOfSeqReads() const {
    return this->num_of_sequential_reads_;
}

DigitalPotRequest_e ComponentRequest::GetDigitalPotReq() const {
    return this->digital_pot_request_;
}

ComponentRequest &ComponentRequest::SetDigitalPotReq(DigitalPotRequest_e digital_pot_req) {
    this->digital_pot_request_ = digital_pot_req;
    return *this;
}

ComponentRequest &ComponentRequest::SetCounterRequest(CounterRequest_e
                                                      counter_req) {
    this->counter_request = counter_req;
    return *this;
}

CounterRequest_e ComponentRequest::GetCounterRequest() const {
    return this->counter_request;
}
