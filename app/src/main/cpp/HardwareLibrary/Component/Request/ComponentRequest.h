//
// Created by fedal on 8/4/21.
//

#ifndef JULIA_ANDROID_COMPONENTREQUEST_H
#define JULIA_ANDROID_COMPONENTREQUEST_H

#include "ComponentRequestEnum.h"
#include "../../InductionControl/InductionControlEnums.h"

typedef enum {
    WRITE_POT,
    READ_POT,
    INCREMENT_POT,
    DECREMENT_POT
} DigitalPotRequest_e;
typedef enum {
    HARDWARE_REQUEST,
    SEQUENCE_REQUEST
} ComponentRequest_e;

typedef enum {
    INDUCTION_STATE,
    INDUCTION_ERROR,
    INDUCTION_POWER_LEVEL,
    INDUCTION_PAN_DETECT,
    NULL_REQUEST
} InductionStateMachineReq_e;

typedef enum {
    COUNTER_READ,
    COUNTER_RESET
} CounterRequest_e;

class ComponentRequest {
private:
    ComponentRequest_e request_type_ = HARDWARE_REQUEST;
    float quantity_ = -1;
public:
    ComponentRequest_e GetRequestType() const;

    ComponentRequest &SetRequestType(ComponentRequest_e requestType);

    float GetQuantity() const;

    ComponentRequest &SetQuantity(float quantity);

    int GetSpeed() const;

    ComponentRequest &SetSpeed(int speed);

    int GetPosition() const;

    ComponentRequest &SetPosition(int position);

    int GetFrequency() const;

    ComponentRequest &SetFrequency(int frequency);

    ActuatorDirections_e GetDirection() const;

    ComponentRequest &SetDirection(ActuatorDirections_e direction);

    OnOffState_e GetState() const;

    ComponentRequest &SetState(OnOffState_e state);

    int GetMuxSelect() const;

    ComponentRequest &SetMuxSelect(int muxSelect);

    SequenceBehavior_e GetSequence() const;

    ComponentRequest &SetSequence(SequenceBehavior_e sequence);

    Component_e GetComponent() const;

    ComponentRequest &SetComponent(Component_e component);

    SensorReadingUnits_e GetSensorUnit() const;

    ComponentRequest &SetSensorUnit(SensorReadingUnits_e);

    induction_control_input::InductionControlInput_e GetInductionControlInput() const;

    ComponentRequest &SetInductionControlInput(induction_control_input::InductionControlInput_e);

    ComponentRequest &SetInductionMachineReq(InductionStateMachineReq_e);

    ComponentRequest &SetEepromData(uint16_t data);

    ComponentRequest &SetEepromAddr(uint16_t address);

    ComponentRequest &SetEepromNumOfSeqReads(uint16_t sequential_reads);

    ComponentRequest &SetDigitalPotReq(DigitalPotRequest_e digital_pot_req);

    int GetEepromAddr() const;

    int GetEepromData() const;

    int GetEepromNumOfSeqReads() const;

    InductionStateMachineReq_e GetInductionMachineReq() const;

    DigitalPotRequest_e GetDigitalPotReq() const;

    ComponentRequest &SetCounterRequest(CounterRequest_e);

    CounterRequest_e GetCounterRequest() const;

private:
    int speed_ = -1;
    int position_ = -1;
    int frequency_ = -1;
    ActuatorDirections_e direction_ = DIRECTION_DEFAULT;
    OnOffState_e state_ = DEFAULT_ON_OFF_STATE;
    int mux_select_ = -1;
    bool abort_sequence_ = false;
    int eeprom_address_ = -1;
    int eeprom_data_ = -1;
    int num_of_sequential_reads_ = -1;
public:
    bool IsAbortSequence() const;

    void SetAbortSequence(bool abortSequence);

private:
    SequenceBehavior_e sequence_ = DEFAULT_BEHAVIOR;
    Component_e component_ = DEFAULT_COMPONENT;
    SensorReadingUnits_e sensor_reading_unit_ = DEFAULT_SENSOR_READING_UNIT;
    induction_control_input::InductionControlInput_e induction_control_input_ =
            induction_control_input::Null_Input;
    InductionStateMachineReq_e state_machine_req_ = NULL_REQUEST;
    DigitalPotRequest_e digital_pot_request_ = READ_POT;
    CounterRequest_e counter_request = COUNTER_READ;
};

#endif //JULIA_ANDROID_COMPONENTREQUEST_H
