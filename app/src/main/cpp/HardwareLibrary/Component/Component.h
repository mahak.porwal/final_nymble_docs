//
// Created by fedal on 8/4/21.
//

#ifndef JULIA_ANDROID_COMPONENT_H
#define JULIA_ANDROID_COMPONENT_H

#include "Request/ComponentRequest.h"
#include "../CommunicationBridge/ResponseParameters.h"

class Component {
private:
    Component_e component_;
public:
    Component(Component_e component) {
        this->component_ = component;
    }

    Component_e GetComponentId() {
        return this->component_;
    }

    virtual std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request) = 0;

    virtual ~Component() {}
};


#endif //JULIA_ANDROID_COMPONENT_H
