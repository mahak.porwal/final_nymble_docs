//
// Created by fedal on 8/4/21.
//

#ifndef JULIA_ANDROID_STANDALONECOMPONENT_H
#define JULIA_ANDROID_STANDALONECOMPONENT_H


#include "../Component.h"
#include "../../CommunicationBridge/ResponseParameters.h"


class StandaloneComponent : public Component {

private:
    std::unordered_map<SequenceBehavior_e, std::unordered_set<Component_e>> component_map_for_sequence_behavior_;
public:
    StandaloneComponent(Component_e component) : Component(component) {}

    virtual std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request) = 0;
};


#endif //JULIA_ANDROID_STANDALONECOMPONENT_H
