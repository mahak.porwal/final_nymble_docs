//
// Created by fedal on 2/5/21.
//

#ifndef RECIPEENGINE_COMPONENTSTATUS_H
#define RECIPEENGINE_COMPONENTSTATUS_H


class ComponentStatus {

private:
    std::unordered_set<Component_e> busy_components_;
    std::unordered_map<Component_e, int> component_process_map_;

protected:
    static ComponentStatus *component_status_;

    ComponentStatus() {}

public:
    static ComponentStatus &GetComponentStatus();

    void MarkBusy(Component_e component, int request_id);

    void MarkAvailable(Component_e component, int request_id);

    bool IsComponentBusy(Component_e component);

    bool IsComponentBusyForRequest(Component_e component, int request_id);

    std::unordered_set<Component_e> GetBusyComponentsForRequest(std::unordered_set<Component_e>
            requested_component_set);

    std::unordered_map<Component_e,int>::const_iterator GetBusyComponentMapStart();
    std::unordered_map<Component_e,int>::const_iterator GetBusyComponentMapEnd();
};

ComponentStatus *ComponentStatus::component_status_ = nullptr;

ComponentStatus &ComponentStatus::GetComponentStatus() {
    if (component_status_ == nullptr) {
        component_status_ = new ComponentStatus();
    }
    return *component_status_;
}

void ComponentStatus::MarkBusy(Component_e component, int request_id) {
    if (this->busy_components_.find(component) != this->busy_components_.end()) {
        //component already marked busy
        return;
    }
    this->busy_components_.emplace(component);
    this->component_process_map_.emplace(component, request_id);
}

void ComponentStatus::MarkAvailable(Component_e component, int request_id) {
    if (this->busy_components_.find(component) == this->busy_components_.end()) {
        //component already available
        return;
    }
    if (this->component_process_map_.at(component) == request_id) {
        this->busy_components_.erase(component);
        this->component_process_map_.erase(component);
    } else {
        throw std::invalid_argument("request id marking availability is incorrect");
    }

}

bool ComponentStatus::IsComponentBusy(Component_e component) {
    return this->busy_components_.find(component) != this->busy_components_.end();
}

bool ComponentStatus::IsComponentBusyForRequest(Component_e component, int request_id) {
    if (this->busy_components_.find(component) == this->busy_components_.end()) {
        //component not marked busy
        return true;
    }
    return this->component_process_map_.at(component) != request_id;
}

std::unordered_set<Component_e> ComponentStatus::GetBusyComponentsForRequest(
        std::unordered_set<Component_e> requested_component_set) {
    std::unordered_set<Component_e> intersection_set;
    std::unordered_set<Component_e>::iterator itr = this->busy_components_.begin();
    for(;itr!=busy_components_.end();itr++){
        if(requested_component_set.find(itr.operator*())!=requested_component_set.end()){
            intersection_set.emplace(itr.operator*());
        }
    }
    return std::unordered_set<Component_e>();
}

std::unordered_map<Component_e, int>::const_iterator ComponentStatus::GetBusyComponentMapStart() {
    return this->component_process_map_.cbegin();
}

std::unordered_map<Component_e, int>::const_iterator ComponentStatus::GetBusyComponentMapEnd() {
    return this->component_process_map_.cend();
}


#endif //RECIPEENGINE_COMPONENTSTATUS_H
