//
// Created by fedal on 8/4/21.
//

#ifndef JULIA_ANDROID_SUBSYSTEMCOMPONENT_H
#define JULIA_ANDROID_SUBSYSTEMCOMPONENT_H


#include "../Component.h"


class SubsystemComponent : public Component {

protected:
    const Subsystem &subsystem_;
public:
    SubsystemComponent(Component_e component, const Subsystem &subsystem)
            : Component(component), subsystem_(subsystem) {}

    virtual std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request) = 0;


};


#endif //JULIA_ANDROID_SUBSYSTEMCOMPONENT_H
