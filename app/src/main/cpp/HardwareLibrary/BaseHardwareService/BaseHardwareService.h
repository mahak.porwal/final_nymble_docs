//
// Created by fedal on 30/4/21.
//

#ifndef RECIPEENGINE_BASEHARDWARESERVICE_H
#define RECIPEENGINE_BASEHARDWARESERVICE_H


class BaseHardwareService {

public:

    virtual std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessComponentRequest(const ComponentRequest &) = 0;

    virtual void StartInductionControl() = 0;

    virtual void StopInductionControl() = 0;

    virtual void InitThermalCamera() = 0;

    virtual ~BaseHardwareService() {}
};

#endif //RECIPEENGINE_BASEHARDWARESERVICE_H
