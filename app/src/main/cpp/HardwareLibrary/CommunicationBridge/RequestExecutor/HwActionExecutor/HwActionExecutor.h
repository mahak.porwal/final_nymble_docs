//
// Created by fedal on 20/7/21.
//

#ifndef RECIPEENGINE_HWACTIONEXECUTOR_H
#define RECIPEENGINE_HWACTIONEXECUTOR_H

#include"../Base/BaseRequestExecutor.h"

class HwActionExecutor : public BaseRequestExecutor {
public:
    HwActionExecutor(JNIEnv *env) : BaseRequestExecutor(env) {}

    ~HwActionExecutor() {}

private:
    jobject
    BuildRequestResponse(
            std::unordered_map<ResponseParameters_e, std::vector<std::string>> response) {
        //Create response
        CompResponseCreator component_response_creator;

        unordered_map<std::string, std::string> hw_action_response =
                component_response_creator.CreateHwActionResponse(
                        response);


        jclass response_class = this->env->FindClass(
                "som/hardware/response/HWActionResponse");
        jmethodID constructor = env->GetMethodID(response_class, "<init>", "(IZLjava/util/Map;)V");

        /*map<string, string> *response_map = new map<string, string>;
        char dispensed_value[20];
        sprintf(dispensed_value, "%.2f", 2.3010);
        response_map->insert(std::pair<string, string>("Weight_Dispensed", dispensed_value));*/


        jobject response_hash_map = this->CreateHashMapObject(env, hw_action_response);

        jobject response_object = (*env).NewObject(response_class, constructor,
                                                   113, true, response_hash_map);
        return response_object;
    }

    jobject CreateHashMapObject(JNIEnv *env, unordered_map<string, string> response_map) {
        jclass hashMapClass = env->FindClass("java/util/HashMap");
        jmethodID hashMapInit = env->GetMethodID(hashMapClass, "<init>", "(I)V");
        jint size = response_map.size();
        jobject hashMapObj = env->NewObject(hashMapClass, hashMapInit, size);
        jmethodID hashMapOut = env->GetMethodID(hashMapClass, "put",
                                                "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

        for (auto it : response_map) {
            env->CallObjectMethod(hashMapObj, hashMapOut, env->NewStringUTF(it.first.c_str()),
                                  env->NewStringUTF(it.second.c_str()));
        }

        return hashMapObj;
    }
};


#endif //RECIPEENGINE_HWACTIONEXECUTOR_H
