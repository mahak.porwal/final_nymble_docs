//
// Created by fedal on 20/7/21.
//

#ifndef RECIPEENGINE_SENSOREXECUTOR_H
#define RECIPEENGINE_SENSOREXECUTOR_H

#include"../Base/BaseRequestExecutor.h"

class SensorExecutor : public BaseRequestExecutor {
public:
    SensorExecutor(JNIEnv *env) : BaseRequestExecutor(env) {}

    jobject CreateI2CHashMap(std::vector<string> i2c_devices) {
        std::unordered_map<std::string, std::vector<std::string>> devices;
        devices.emplace("I2C_DEVICES", i2c_devices);
        return CreateHashMapObject(env,devices);
    }

    ~SensorExecutor() {}

private:
    jobject
    BuildRequestResponse(
            std::unordered_map<ResponseParameters_e, std::vector<std::string>> response) {
        CompResponseCreator component_response_creator;

        std::unordered_map<std::string, std::vector<std::string>> sensor_response =
                component_response_creator.CreateSensorReqResponse(
                        response);

        jclass response_class = this->env->FindClass(
                "som/hardware/response/SensorResponse");
        jmethodID constructor = env->GetMethodID(response_class, "<init>", "(IZLjava/util/Map;)V");

        jobject response_hash_map = this->CreateHashMapObject(env, sensor_response);

        jobject response_object = (*env).NewObject(response_class, constructor,
                                                   113, true, response_hash_map);
        return response_object;
    }

    jobject CreateHashMapObject(JNIEnv *env,
                                unordered_map<string, std::vector<std::string>> response_map) {
        jclass hashMapClass = env->FindClass("java/util/HashMap");
        jmethodID hashMapInit = env->GetMethodID(hashMapClass, "<init>", "(I)V");
        jint size = response_map.size();
        jobject hashMapObj = env->NewObject(hashMapClass, hashMapInit, size);
        jmethodID hashMapPut = env->GetMethodID(hashMapClass, "put",
                                                "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");


        for (auto it : response_map) {

            //for each of the map entry
            //create list jobject
            jclass array_list_class = env->FindClass("java/util/ArrayList");
            jmethodID array_list_init = env->GetMethodID(array_list_class, "<init>", "(I)V");
            int array_list_size = response_map.size();
            jmethodID array_list_add = env->GetMethodID(array_list_class, "add",
                                                        "(Ljava/lang/Object;)Z");
            jobject array_list_object = env->NewObject(array_list_class, array_list_init,
                                                       array_list_size);

            for (int i = 0; i < it.second.size(); i++) {
                jdouble value = std::stod(it.second[i]);
                jclass intClass = env->FindClass("java/lang/Double");
                jmethodID initInt = env->GetMethodID(intClass, "<init>", "(D)V");
                jobject newIntObj = env->NewObject(intClass, initInt, value);
                jboolean res = env->CallBooleanMethod(array_list_object, array_list_add, newIntObj);
            }

            //and then place the list object using the put method

            env->CallObjectMethod(hashMapObj, hashMapPut, env->NewStringUTF(it.first.c_str()),
                                  array_list_object);
        }

        return hashMapObj;
    }
};

#endif //RECIPEENGINE_SENSOREXECUTOR_H
