//
// Created by fedal on 20/7/21.
//

#ifndef RECIPEENGINE_REQUESTEXECUTOR_H
#define RECIPEENGINE_REQUESTEXECUTOR_H


class RequestExecutor {
public :
    virtual ~RequestExecutor() {}

    virtual jobject Execute(jobject request) = 0;
};


#endif //RECIPEENGINE_REQUESTEXECUTOR_H
