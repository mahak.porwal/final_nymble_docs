//
// Created by fedal on 20/7/21.
//

#ifndef RECIPEENGINE_BASEREQUESTEXECUTOR_H
#define RECIPEENGINE_BASEREQUESTEXECUTOR_H

#include "../RequestExecutor.h"

class BaseRequestExecutor : public RequestExecutor {

protected:
    JNIEnv *env;

public:

    BaseRequestExecutor(JNIEnv *env) {
        this->env = env;
    }

    virtual ~BaseRequestExecutor() {}

    jobject Execute(jobject _request);

private:

    jobject GetHWMessageObject(JNIEnv *env, jobject hw_action_request);

    void PopulateMessageMap(JNIEnv *env, jobject hw_message_object,
                            map<std::string, std::string> &map_to_populate);

    virtual jobject
    BuildRequestResponse(std::unordered_map<ResponseParameters_e, std::vector<std::string>>) = 0;
};


#endif //RECIPEENGINE_BASEREQUESTEXECUTOR_H
