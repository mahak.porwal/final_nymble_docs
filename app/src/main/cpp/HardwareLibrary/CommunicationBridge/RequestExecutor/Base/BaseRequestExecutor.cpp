//
// Created by fedal on 20/7/21.
//

#include "BaseRequestExecutor.h"


/**
 * Creates a jni object of type HWMessage class in java
 * @param env JNI JNIEnv variable
 * @param hw_action_request HWActionRequest object from java
 * @return HWMessage class object
 */
jobject BaseRequestExecutor::GetHWMessageObject(JNIEnv *env, jobject hw_action_request) {

    jclass cls = env->GetObjectClass(hw_action_request);
    // Find "java/util/List" Class (Standard JAVA Class).
    jclass listClass = env->FindClass("java/util/List");

    // Get List object field.
    jmethodID getListMethodID = env->GetMethodID(cls, "getRequestParams", "()Ljava/util/List;");
    jobject listObject = env->CallObjectMethod(hw_action_request, getListMethodID);


    // Get "java.util.List.get(int location)" MethodID
    jmethodID getMethodID = env->GetMethodID(listClass, "get", "(I)Ljava/lang/Object;");

    // Get "int java.util.List.size()" MethodID
    jmethodID sizeMethodID = env->GetMethodID(listClass, "size", "()I");

    // Call "int java.util.List.size()" method and get count of items in the list.
    int listItemsCount = (int) env->CallIntMethod(listObject, sizeMethodID);


    jobject hwMessageObject = env->CallObjectMethod(listObject, getMethodID, 0);

    return hwMessageObject;
}

/**
 * Populate a given map of type map<std::string, std::string> from the object of Java class HWMessage
 * @param env JNI JNIEnv variable
 * @param hw_message_object object of the Java class HWMessage
 * @param message_map map to populate
 */
void BaseRequestExecutor::PopulateMessageMap(JNIEnv *env, jobject hw_message_object,
                                                map<std::string, std::string> &message_map) {

    jclass hw_message_class = env->GetObjectClass(hw_message_object);

    // Find "java/util/Map" Class (Standard JAVA Class).
    jclass map_class = env->FindClass("java/util/Map");

    // Get Map object field.
    jmethodID getMapMethodID = env->GetMethodID(hw_message_class, "getParametersMap",
                                                "()Ljava/util/Map;");


    jmethodID id_entrySet = env->GetMethodID(map_class, "entrySet", "()Ljava/util/Set;");

    jclass c_entryset = env->FindClass("java/util/Set");

    jmethodID id_iterator = env->GetMethodID(c_entryset, "iterator", "()Ljava/util/Iterator;");

    jclass c_iterator = env->FindClass("java/util/Iterator");
    jmethodID id_hasNext = env->GetMethodID(c_iterator, "hasNext", "()Z");
    jmethodID id_next = env->GetMethodID(c_iterator, "next", "()Ljava/lang/Object;");

    jclass c_entry = env->FindClass("java/util/Map$Entry");
    jmethodID id_getKey = env->GetMethodID(c_entry, "getKey", "()Ljava/lang/Object;");
    jmethodID id_getValue = env->GetMethodID(c_entry, "getValue", "()Ljava/lang/Object;");

    jclass c_string = env->FindClass("java/lang/String");
    jmethodID id_toString = env->GetMethodID(c_string, "toString", "()Ljava/lang/String;");

    //All calls above can be statically set

    jobject map_object = env->CallObjectMethod(hw_message_object, getMapMethodID);


    jobject obj_entrySet = env->CallObjectMethod(map_object, id_entrySet);

    jobject obj_iterator = env->CallObjectMethod(obj_entrySet, id_iterator);

    bool hasNext = (bool) env->CallBooleanMethod(obj_iterator, id_hasNext);

    while (hasNext) {
        jobject entry = env->CallObjectMethod(obj_iterator, id_next);

        jobject key = env->CallObjectMethod(entry, id_getKey);
        jobject value = env->CallObjectMethod(entry, id_getValue);

        jstring jstrKey = (jstring) env->CallObjectMethod(key, id_toString);
        jstring jstrValue = (jstring) env->CallObjectMethod(value, id_toString);

        const char *map_key = env->GetStringUTFChars(jstrKey, 0);
        const char *map_value = env->GetStringUTFChars(jstrValue, 0);

        message_map.insert(std::pair<string, string>(map_key, map_value));
/*
        string map_key = (string) reinterpret_cast<const char *>(env->CallObjectMethod(key,
                                                                                       id_toString));
        string map_value = (string) reinterpret_cast<const char *>(env->CallObjectMethod(value,
                                                                                         id_toString));*/

        //message_map->insert(std::pair<string, string>(map_key, map_value));

        hasNext = (bool) env->CallBooleanMethod(obj_iterator, id_hasNext);
    }
}

/**
 * Executes the HWActionRequest and creates the corresponding response
 * @return HWActionResponse
 */
jobject BaseRequestExecutor::Execute(jobject hw_action_request) {

    __android_log_print(ANDROID_LOG_ERROR, "Chef  native test: ", " #1");

    map<std::string, std::string> message_map;

    //get the HWMessage object from the HWActionRequest
    jobject hw_message_object = this->GetHWMessageObject(this->env, hw_action_request);


    __android_log_print(ANDROID_LOG_ERROR, "Chef  native test: ", " #2");

    //populate the map<string, string> from the object of Java class HWMessage
    this->PopulateMessageMap(this->env, hw_message_object, message_map);

    __android_log_print(ANDROID_LOG_ERROR, "Chef  native test: ", " #3");

    //Build component request
    ComponentReqCreator component_request_creator(message_map.cbegin(), message_map.cend());
    ComponentRequest component_request = component_request_creator.CreateRequest();

    __android_log_print(ANDROID_LOG_ERROR, "Chef  native test: ", " #4");

    //Process request
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response =
            HardwareServiceFactory::GetHardwareService()->ProcessComponentRequest(
                    component_request);

    return this->BuildRequestResponse(response);

    //Create response
    /*CompResponseCreator component_response_creator;
    map<std::string, std::string> hw_action_response = component_response_creator.CreateHwActionResponse(
            response);


    jclass response_class = this->env->FindClass(
            "com/example/recipeengine/som/hardware/response/HWActionResponse");
    jmethodID constructor = env->GetMethodID(response_class, "<init>", "(IZLjava/util/Map;)V");

    *//*map<string, string> *response_map = new map<string, string>;
    char dispensed_value[20];
    sprintf(dispensed_value, "%.2f", 2.3010);
    response_map->insert(std::pair<string, string>("Weight_Dispensed", dispensed_value));*//*


    jobject response_hash_map = this->CreateHashMapObject(env, hw_action_response);

    jobject response_object = (*env).NewObject(response_class, constructor,
                                               113, true, response_hash_map);
    return response_object;*/
}

