//
// Created by fedal on 2/5/21.
//

#ifndef RECIPEENGINE_COMPONENTREQCREATOR_H
#define RECIPEENGINE_COMPONENTREQCREATOR_H


#include <map>
#include "../../../Component/Request/ComponentRequestBuilder.h"
#include "StringToEnums.h"

class ComponentReqCreator {
    typedef void (ComponentReqCreator::*FuncPointer)(std::string);

private:

    std::map<std::string, FuncPointer> function_map;

    std::map<std::string, std::string>::const_iterator message_map_begin;
    std::map<std::string, std::string>::const_iterator message_map_end;

    ComponentRequestBuilder component_request_builder;

    void SetComponent(std::string component) {
        component_request_builder.component_request_.
                SetComponent(StringToEnums::component_map_.at(component));
    }

    void SetSequenceBehavior(std::string behavior) {
        component_request_builder.component_request_.
                SetSequence(StringToEnums::behavior_map_.at(behavior));
    }

    void SetQuantity(std::string quantity) {
        component_request_builder.component_request_.
                SetQuantity(std::stof(quantity));
    }

    void SetRequestType(std::string request_type) {
        component_request_builder.component_request_.
                SetRequestType(StringToEnums::request_type_map_.at(request_type));
    }

    void SetSensorUnits(std::string sensor_unit) {
        component_request_builder.component_request_.
                SetSensorUnit(StringToEnums::sensor_units_map_.at(sensor_unit));
    }

    void SetSpeed(std::string speed) {
        component_request_builder.component_request_.
                SetSpeed(std::stoi(speed));
    }

    void SetAngle(std::string angle) {
        component_request_builder.component_request_.
                SetPosition(std::stoi(angle));
    }

    void SetDirection(std::string direction) {
        component_request_builder.component_request_.
                SetDirection(StringToEnums::actuator_dir_map_.at(direction));
    }

    void SetComponentState(std::string component_state) {
        component_request_builder.component_request_.
                SetState(StringToEnums::component_state_map_.at(component_state));
    }

    void SetMuxSelection(std::string mux_selection) {
        component_request_builder.component_request_.
                SetMuxSelect(stoi(mux_selection));
    }

    void SetInductionControlInput(std::string input) {
        component_request_builder.component_request_.
                SetInductionControlInput(StringToEnums::induction_control_input_map_.at(input));
    }

    void SetInductionMachineReq(std::string state_machine_req) {
        component_request_builder.component_request_.
                SetInductionMachineReq(
                StringToEnums::induction_machine_req_map_.at(state_machine_req));
    }

    void SetDigitalPotReq(std::string digital_pot_req) {
        component_request_builder.component_request_.
                SetDigitalPotReq(
                StringToEnums::digital_pot_req_map_.at(digital_pot_req));
    }

    void SetCounterReq(std::string counter_req) {
        component_request_builder.component_request_.
                SetCounterRequest(
                StringToEnums::counter_request_map_.at(counter_req));
    }

public:
    ComponentReqCreator(std::map<std::string, std::string>::const_iterator
                        message_map_begin,
                        std::map<std::string, std::string>::const_iterator
                        message_map_end) {

        this->function_map.emplace("COMPONENT", &ComponentReqCreator::SetComponent);
        this->function_map.emplace("SENSOR_NAME", &ComponentReqCreator::SetComponent);
        this->function_map.emplace("BEHAVIOR_TYPE", &ComponentReqCreator::SetSequenceBehavior);
        this->function_map.emplace("HARDWARE_UNITS", &ComponentReqCreator::SetQuantity);
        this->function_map.emplace("REQUEST_TYPE", &ComponentReqCreator::SetRequestType);
        this->function_map.emplace("SENSOR_UNIT", &ComponentReqCreator::SetSensorUnits);
        this->function_map.emplace("SPEED", &ComponentReqCreator::SetSpeed);
        this->function_map.emplace("ANGLE", &ComponentReqCreator::SetAngle);
        this->function_map.emplace("DIRECTION", &ComponentReqCreator::SetDirection);
        this->function_map.emplace("COMPONENT_ON_OFF_STATE",
                                   &ComponentReqCreator::SetComponentState);
        this->function_map.emplace("MUX_SELECTION", &ComponentReqCreator::SetMuxSelection);
        this->function_map.emplace("INDUCTION_CONTROL_INPUT",
                                   &ComponentReqCreator::SetInductionControlInput);

        this->function_map.emplace("INDUCTION_MACHINE_REQ",
                                   &ComponentReqCreator::SetInductionMachineReq);
        this->function_map.emplace("DIGITAL_POT_REQ",
                                   &ComponentReqCreator::SetDigitalPotReq);
        this->function_map.emplace("COUNTER_REQ",
                                   &ComponentReqCreator::SetCounterReq);

        this->message_map_begin = message_map_begin;
        this->message_map_end = message_map_end;
    }

    ComponentRequest CreateRequest() {
        for (auto itr = message_map_begin; itr != message_map_end; itr++) {
            FuncPointer func = this->function_map.at(itr->first);
            (this->*func)(itr->second);
        }
        return component_request_builder.component_request_;
    }
};


#endif //RECIPEENGINE_COMPONENTREQCREATOR_H
