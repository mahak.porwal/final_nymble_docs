//
// Created by fedal on 2/5/21.
//

#ifndef RECIPEENGINE_STRINGTOENUMS_H
#define RECIPEENGINE_STRINGTOENUMS_H

class StringToEnums {
public:
    typedef std::map<std::string, Component_e> ComponentMap;
    typedef std::map<std::string, SequenceBehavior_e> BehaviorMap;
    typedef std::map<std::string, ComponentRequest_e> RequestTypeMap;
    typedef std::map<std::string, SensorReadingUnits_e> SensorUnitsMap;
    typedef std::map<std::string, ActuatorDirections_e> ActuatorDirMap;
    typedef std::map<std::string, OnOffState_e> ComponentStateMap;
    typedef std::map<std::string, induction_control_input::InductionControlInput_e>
            InductionControlInputMap;
    typedef std::map<std::string, InductionStateMachineReq_e>
            InductionMachineReqMap;
    typedef std::map<std::string, DigitalPotRequest_e>
            DigitalPotReqMap;
    typedef std::map<std::string, CounterRequest_e>
            CounterReqMap;
    static ComponentMap component_map_;
    static BehaviorMap behavior_map_;
    static RequestTypeMap request_type_map_;
    static SensorUnitsMap sensor_units_map_;
    static ActuatorDirMap actuator_dir_map_;
    static ComponentStateMap component_state_map_;
    static InductionControlInputMap induction_control_input_map_;
    static InductionMachineReqMap induction_machine_req_map_;
    static DigitalPotReqMap digital_pot_req_map_;
    static CounterReqMap counter_request_map_;
};

StringToEnums::ComponentMap  StringToEnums::component_map_ = {


        {"MACRO_SERVO_1",                     MACRO_SERVO_1},
        {"MACRO_SERVO_2",                     MACRO_SERVO_2},
        {"MACRO_SERVO_3",                     MACRO_SERVO_3},
        {"MACRO_SERVO_4",                     MACRO_SERVO_4},

        {"MACRO_SERVO_1_FEEDBACK",            MACRO_SERVO_1_FEEDBACK},
        {"MACRO_SERVO_2_FEEDBACK",            MACRO_SERVO_2_FEEDBACK},
        {"MACRO_SERVO_3_FEEDBACK",            MACRO_SERVO_3_FEEDBACK},
        {"MACRO_SERVO_4_FEEDBACK",            MACRO_SERVO_4_FEEDBACK},

        {"MACRO_POT_MUX_SELECT_1",            MACRO_POT_MUX_SELECT_1},
        {"MACRO_POT_MUX_SELECT_2",            MACRO_POT_MUX_SELECT_2},
        {"MACRO_POT_MUX_SELECT_3",            MACRO_POT_MUX_SELECT_3},
        {"MACRO_MUX",                         MACRO_MUX},


        {"MICRO_SERVO_1",                     MICRO_SERVO_1},
        {"MICRO_SERVO_2",                     MICRO_SERVO_2},
        {"MICRO_SERVO_3",                     MICRO_SERVO_3},
        {"MICRO_SERVO_4",                     MICRO_SERVO_4},
        {"MICRO_SERVO_5",                     MICRO_SERVO_5},
        {"MICRO_SERVO_6",                     MICRO_SERVO_6},

        {"MICRO_SERVO_1_FEEDBACK",            MICRO_SERVO_1_FEEDBACK},
        {"MICRO_SERVO_2_FEEDBACK",            MICRO_SERVO_2_FEEDBACK},
        {"MICRO_SERVO_3_FEEDBACK",            MICRO_SERVO_3_FEEDBACK},
        {"MICRO_SERVO_4_FEEDBACK",            MICRO_SERVO_4_FEEDBACK},
        {"MICRO_SERVO_5_FEEDBACK",            MICRO_SERVO_5_FEEDBACK},
        {"MICRO_SERVO_6_FEEDBACK",            MICRO_SERVO_6_FEEDBACK},


        {"MICRO_POT_MUX_SELECT_1",            MICRO_POT_MUX_SELECT_1},
        {"MICRO_POT_MUX_SELECT_2",            MICRO_POT_MUX_SELECT_2},
        {"MICRO_POT_MUX_SELECT_3",            MICRO_POT_MUX_SELECT_3},
        {"MICRO_MUX",                         MICRO_MUX},


        {"STIRRER_MOTOR",                     STIRRER_MOTOR},
        {"STIRRER_PHASE_GPIO",                STIRRER_PHASE_GPIO},
        {"STIRRER_CONNECTION_FB",             STIRRER_CONNECTION_FB},
        {"STIRRER_CURRENT_SENSOR",            STIRRER_CURRENT_SENSOR},


        {"OIL_PUMP",                          OIL_PUMP},
        {"WATER_PUMP",                        WATER_PUMP},

        {"INDUCTION_HEATER",                  INDUCTION_HEATER},
        {"INDUCTION_ACTUATOR",                INDUCTION_ACTUATOR},
        {"INDUCTION_AC_CURRENT_SENSOR",       INDUCTION_AC_CURRENT_SENSOR},
        {"INDUCTION_AC_VOLTAGE_SENSOR",       INDUCTION_AC_VOLTAGE_SENSOR},
        {"INDUCTION_IGBT_TEMPERATURE_SENSOR", INDUCTION_IGBT_TEMPERATURE_SENSOR},
        {"INDUCTION_PAN_TEMPERATURE_SENSOR",  INDUCTION_PAN_TEMPERATURE_SENSOR},
        {"INDUCTION_PAN_GPIO",                INDUCTION_PAN_GPIO},
        {"INDUCTION_INT_GPIO",                INDUCTION_INT_GPIO},
        {"INDUCTION_FAN_GPIO",                INDUCTION_FAN_GPIO},
        {"INDUCTION_ON_OFF_GPIO",             INDUCTION_ON_OFF_GPIO},
        {"INDUCTION_BUZZER_GPIO",             INDUCTION_BUZZER_GPIO},

        {"SOM_BOARD_CURRENT_SENSOR",          SOM_BOARD_CURRENT_SENSOR},
        {"LOADCELL_SENSOR",                   LOADCELL_SENSOR},
        {"LED_ILLUMINATION",                  LED_ILLUMINATION},
        {"LED_DESIGN",                        LED_DESIGN},
        {"EXHAUST_FAN",                       EXHAUST_FAN},
        {"BUZZER",                            BUZZER},
        {"INA300_LATCH",                      INA300_LATCH},
        {"INA300_ALERT",                      INA300_ALERT},
        {"LM2678_CONTROL",                    LM2678_CONTROL},
        {"MACRO_MICRO_FB_SELECT_1",           MACRO_MICRO_FB_SELECT_1},
        {"MACRO_MICRO_MUX",                   MACRO_MICRO_MUX},
        {"THERMAL_CAMERA_EEPROM",             THERMAL_CAMERA_EEPROM},
        {"THERMAL_CAMERA",                    THERMAL_CAMERA},
        {"INDUCTION_PAN_FREQUENCY_POT",       INDUCTION_PAN_FREQUENCY_POT},
        {"INDUCTION_PAN_DUTY_POT",            INDUCTION_PAN_DUTY_POT},
        {"INDUCTION_PWM_FREQUENCY_POT",       INDUCTION_PWM_FREQUENCY_POT},
        {"INDUCTION_PWM_DUTY_POT",            INDUCTION_PWM_DUTY_POT},
        {"BODY_TEMPERATURE",                  BODY_TEMPERATURE},
        {"WATER_TEMPERATURE",                 WATER_TEMPERATURE},
        {"COUNTER",                           COUNTER},
        {"COUNTER_MUX_SELECT_0",                           COUNTER_MUX_SELECT_0},
        {"COUNTER_MUX_SELECT_1",                           COUNTER_MUX_SELECT_1},
        {"EXHAUST_FAN_COUNTER",               EXHAUST_FAN_COUNTER},
        {"EXHAUST_FAN_ON_OFF",               EXHAUST_FAN_ON_OFF},
        {"HEATER_ENABLE",HEATER_ENABLE},
        {"HEATER_STATUS",HEATER_STATUS},
        {"LIMIT_SWITCH_1",LIMIT_SWITCH_1},
        {"LIMIT_SWITCH_2",LIMIT_SWITCH_2},
        {"INDUCTIVE_SENSOR_1",INDUCTIVE_SENSOR_1},
        {"INDUCTIVE_SENSOR_2",INDUCTIVE_SENSOR_2},
        {"WATER_FLOW_COUNTER",               WATER_FLOW_COUNTER},
        {"OIL_FLOW_COUNTER",               OIL_FLOW_COUNTER},
};

StringToEnums::BehaviorMap  StringToEnums::behavior_map_ = {
        {"MACRO_LIQUID_DISPENSE",         MACRO_LIQUID_DISPENSE},
        {"MACRO_NON_LIQUID_DISPENSE",     MACRO_NON_LIQUID_DISPENSE},
        {"MACRO_REACH_POSITION_AT_SPEED", MACRO_REACH_POSITION_AT_SPEED},
        {"MACRO_RESET_CONTAINER",         MACRO_RESET_CONTAINER},
        {"MICRO_ANTICLOCKWISE_DISPENSE",  MICRO_ANTICLOCKWISE_DISPENSE},
        {"MICRO_CLOCKWISE_DISPENSE",      MICRO_CLOCKWISE_DISPENSE},
        {"MICRO_REACH_ANGLE_WITH_SPEED",  MICRO_REACH_ANGLE_WITH_SPEED},
        {"MICRO_RESET_POD",               MICRO_RESET_POD},
        {"LIQUID_OIL_DISPENSE",           LIQUID_OIL_DISPENSE},
        {"LIQUID_WATER_DISPENSE",         LIQUID_WATER_DISPENSE},
};

StringToEnums::RequestTypeMap  StringToEnums::request_type_map_ = {
        {"ACTION_REQUEST",  SEQUENCE_REQUEST},
        {"COMMAND_REQUEST", HARDWARE_REQUEST},
        {"SENSOR_REQUEST",  HARDWARE_REQUEST}
};

StringToEnums::SensorUnitsMap  StringToEnums::sensor_units_map_ = {
        {"ANGLE_VALUE",   ANGLE_VALUE},
        {"RAW_VALUE",     RAW_VALUE},
        {"MICRO_VOLTS",   MICRO_VOLTS},
        {"MILLI_VOLTS",   MILLI_VOLTS},
        {"MILLI_AMPERES", MILLI_AMPERES},
        {"GRAMS",         GRAMS}
};

StringToEnums::ActuatorDirMap StringToEnums::actuator_dir_map_ = {
        {"DIRECTION_CLOCKWISE",     DIRECTION_CLOCKWISE},
        {"DIRECTION_ANTICLOCKWISE", DIRECTION_ANTICLOCKWISE},
        {"DIRECTION_DEFAULT",       DIRECTION_DEFAULT}
};

StringToEnums::ComponentStateMap StringToEnums::component_state_map_ = {
        {"ON_STATE",        ON_STATE},
        {"OFF_STATE",       OFF_STATE},
        {"OFF_TO_ON_STATE", OFF_TO_ON_STATE},
        {"ON_TO_OFF_STATE", ON_TO_OFF_STATE}
};


StringToEnums::InductionControlInputMap StringToEnums::induction_control_input_map_ = {
        {"Power_Level_One",   induction_control_input::Power_Level_One},
        {"Power_Level_Two",   induction_control_input::Power_Level_Two},
        {"Power_Level_Three", induction_control_input::Power_Level_Three},
        {"Power_Level_Four",  induction_control_input::Power_Level_Four},
        {"Power_Level_Five",  induction_control_input::Power_Level_Five},
        {"Power_Level_Six",   induction_control_input::Power_Level_Six},
        {"Power_Level_Seven", induction_control_input::Power_Level_Seven},
        {"Shutdown",          induction_control_input::Shutdown},
        {"Manual_Mode",       induction_control_input::Manual_Mode}
};

StringToEnums::InductionMachineReqMap StringToEnums::induction_machine_req_map_ = {
        {"INDUCTION_STATE",       INDUCTION_STATE},
        {"INDUCTION_ERROR",       INDUCTION_ERROR},
        {"INDUCTION_POWER_LEVEL", INDUCTION_POWER_LEVEL},
        {"INDUCTION_PAN_DETECT",  INDUCTION_PAN_DETECT}
};

StringToEnums::DigitalPotReqMap StringToEnums::digital_pot_req_map_ = {
        {"READ_POT",      READ_POT},
        {"WRITE_POT",     WRITE_POT},
        {"INCREMENT_POT", INCREMENT_POT},
        {"DECREMENT_POT", DECREMENT_POT}
};
StringToEnums::CounterReqMap  StringToEnums::counter_request_map_ = {
        {"COUNTER_READ",  COUNTER_READ},
        {"COUNTER_RESET", COUNTER_RESET}

};
#endif //RECIPEENGINE_STRINGTOENUMS_H
