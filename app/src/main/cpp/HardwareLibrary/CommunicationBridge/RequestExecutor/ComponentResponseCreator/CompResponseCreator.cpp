//
// Created by fedal on 19/7/21.
//

#include "CompResponseCreator.h"

std::unordered_map<std::string, std::string> CompResponseCreator::CreateHwActionResponse(
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> response) {
    std::unordered_map<std::string, std::string> hw_action_response;
    for (auto itr : response) {
        if (EnumsToString::response_params_map_.find(itr.first) !=
            EnumsToString::response_params_map_.end()) {
            std::vector<std::string> list_string = itr.second;
            hw_action_response.emplace(EnumsToString::response_params_map_.at(itr.first),
                                       list_string.size() != 0 ? list_string.at(0) : 0);
        }
    }
    return hw_action_response;
}

std::unordered_map<std::string, std::vector<std::string>> CompResponseCreator::CreateSensorReqResponse(
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> response) {
    std::unordered_map<std::string, std::vector<std::string>> sensor_response;
    for (auto itr : response) {
        if (EnumsToString::response_params_map_.find(itr.first) !=
            EnumsToString::response_params_map_.end()) {
            std::vector<std::string> list_string = itr.second;

            sensor_response.emplace(EnumsToString::response_params_map_.at(itr.first),
                                    list_string);
        }
    }
    return sensor_response;
}