//
// Created by fedal on 19/7/21.
//

#ifndef RECIPEENGINE_ENUMSTOSTRING_H
#define RECIPEENGINE_ENUMSTOSTRING_H

class EnumsToString {
public:
    typedef std::map <ResponseParameters_e, std::string> ResponseParametersMap;
    static ResponseParametersMap response_params_map_;
};

EnumsToString::ResponseParametersMap EnumsToString::response_params_map_ = {
        {SENSOR_VALUE,            "SENSOR_VALUE"},
        {TARGET_WEIGHT,           "TARGET_WEIGHT"},
        {ACTUAL_DISPENSED_WEIGHT, "ACTUAL_DISPENSED_WEIGHT"},
        {DISPENSED_WEIGHT,        "DISPENSED_WEIGHT"},
        {SEQUENCE_DONE,           "SEQUENCE_DONE"},
        {SEQUENCE_RESULT,         "SEQUENCE_RESULT"},
        {COMMAND_RESULT,          "COMMAND_RESULT"},
        {SEQUENCE_DONE,           "SEQUENCE_DONE"},
        {SEQUENCE_RESULT,         "SEQUENCE_RESULT"},
        {COMMAND_RESULT,          "COMMAND_RESULT"},
};

#endif //RECIPEENGINE_ENUMSTOSTRING_H
