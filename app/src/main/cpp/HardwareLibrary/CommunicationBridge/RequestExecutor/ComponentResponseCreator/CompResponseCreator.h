//
// Created by fedal on 19/7/21.
//

#ifndef RECIPEENGINE_COMPRESPONSECREATOR_H
#define RECIPEENGINE_COMPRESPONSECREATOR_H

#include "EnumsToString.h"
#include <unordered_map>
#include <vector>

class CompResponseCreator {
public:
    std::unordered_map<std::string, std::string> CreateHwActionResponse(
            std::unordered_map<ResponseParameters_e, std::vector<std::string>>);

    std::unordered_map<std::string, std::vector<std::string>> CreateSensorReqResponse(
            std::unordered_map<ResponseParameters_e, std::vector<std::string>> response);
};


#endif //RECIPEENGINE_COMPRESPONSECREATOR_H
