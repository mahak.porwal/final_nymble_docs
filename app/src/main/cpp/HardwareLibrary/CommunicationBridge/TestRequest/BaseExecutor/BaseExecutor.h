//
// Created by fedal on 20/7/21.
//

#ifndef RECIPEENGINE_BASEEXECUTOR_H
#define RECIPEENGINE_BASEEXECUTOR_H

#include "../Executor.h"

class BaseExecutor : public Executor {
protected:
    int data;
public:
    BaseExecutor(int data) {
        this->data = data;
    }

    virtual ~BaseExecutor() {}

    int Execute(int data);


private:
    int CommonStep(int data);

    virtual int StepToOverride(int data) = 0;
};


#endif //RECIPEENGINE_BASEEXECUTOR_H
