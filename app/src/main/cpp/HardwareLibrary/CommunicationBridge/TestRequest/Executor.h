//
// Created by fedal on 20/7/21.
//

#ifndef RECIPEENGINE_EXECUTOR_H
#define RECIPEENGINE_EXECUTOR_H


class Executor {
public:
    virtual ~Executor() {}

    virtual int Execute(int data) = 0;
};


#endif //RECIPEENGINE_EXECUTOR_H
