//
// Created by fedal on 20/7/21.
//

#ifndef RECIPEENGINE_EXECUTORA_H
#define RECIPEENGINE_EXECUTORA_H

#include "../BaseExecutor/BaseExecutor.h"

class ExecutorA : public BaseExecutor {
private:
    int StepToOverride(int data){
        return 0 + data;
    }

public:
    ExecutorA(int data) : BaseExecutor(data) {}

    ~ExecutorA() {}
};



#endif //RECIPEENGINE_EXECUTORA_H
