//
// Created by fedal on 29/12/20.
//

#include "HardwareMessageParser.h"
#include "HardwareMessageParserEnums.h"
#include "../../HardwareService/HardwareService.h"
#include "../../Subsystem/Macro/Macro.h"
#include "../../Subsystem/Micro/Micro.h"
#include "../../Subsystem/Stirrer/Stirrer.h"
#include "../../Subsystem/Liquid/Liquid.h"
#include "../../Subsystem/Induction/Induction.h"
#include "../../HardwareService/SubsystemRequest/SubsystemRequestBuilder.h"


map<int, double> HardwareMessageParser::ParseHardwareMessage() throw(Error) {
    map<int, double> result;
    switch (this->hardware_message_->GetSubsystem()) {
        case MessageConstants::Macro: {
            result = this->ParseMacroMessage();
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro Command ");
        }
            break;
        case MessageConstants::Micro: {
            result = this->ParseMicroMessage();
        }
            break;
        case MessageConstants::HeatingSystem: {
            result = this->ParseInductionMessage();
        }
            break;
        case MessageConstants::Liquid: {
            result = this->ParseLiquidMessage();
        }
            break;
        case MessageConstants::Stirrer: {
            result = this->ParseStirrerMessage();
        }
            break;
        case MessageConstants::Common: {
            result = this->ParseCommonMessage();
        }
            break;
    }
    return result;
}

map<int, double> HardwareMessageParser::ParseMacroMessage() throw(Error) {
    map<int, double> result;
    switch (this->hardware_message_->GetMessageType()) {
        case MessageConstants::MpuMcuCommand: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Macro::CreateSubsystemRequestForMacroCommand(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    MACRO_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);

            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro Command ");
        }
            break;
        case MessageConstants::MpuMcuAction: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Macro::CreateSubsystemRequestForMacroAction(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    MACRO_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);
        }
            break;
        case MessageConstants::MpuMcuRequest: {
            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Macro::CreateSubsystemRequestForMacroRequest(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    MACRO_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);

            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro request %f",
                                result.at(MessageConstants::Primary));
        }
            break;
        default:
            break;
    }
    return result;
}

map<int, double> HardwareMessageParser::ParseMicroMessage() throw(Error) {

    map<int, double> result;
    switch (this->hardware_message_->GetMessageType()) {
        case MessageConstants::MpuMcuCommand: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Micro::CreateSubsystemRequestForMicroCommand(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    MICRO_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);

            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro Command ");
        }
            break;
        case MessageConstants::MpuMcuAction: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Micro::CreateSubsystemRequestForMicroAction(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    MICRO_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);
        }
            break;
        case MessageConstants::MpuMcuRequest: {
            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Micro::CreateSubsystemRequestForMicroRequest(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    MICRO_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);

            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro request %f",
                                result.at(MessageConstants::Primary));
        }
            break;
        default:
            break;
    }
    return result;
}

map<int, double> HardwareMessageParser::ParseStirrerMessage() throw(Error) {

    map<int, double> result;
    switch (this->hardware_message_->GetMessageType()) {
        case MessageConstants::MpuMcuCommand: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Stirrer::CreateSubsystemRequestForStirrerCommand(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    STIRRER_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);

            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro Command ");
        }
            break;
        case MessageConstants::MpuMcuAction: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Stirrer::CreateSubsystemRequestForStirrerAction(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    STIRRER_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);
        }
            break;
        case MessageConstants::MpuMcuRequest: {
            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Stirrer::CreateSubsystemRequestForStirrerRequest(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    STIRRER_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);

            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro request %f",
                                result.at(MessageConstants::Primary));
        }
            break;
        default:
            break;
    }
    return result;
}

map<int, double> HardwareMessageParser::ParseLiquidMessage() throw(Error) {
    using namespace MessageConstants;
    map<int, double> result;
    switch (this->hardware_message_->GetMessageType()) {
        case MpuMcuCommand: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Liquid::CreateSubsystemRequestForLiquidCommand(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    LIQUID_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro Command ");
        }
            break;
        case MpuMcuAction: {


            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Liquid::CreateSubsystemRequestForLiquidAction(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    LIQUID_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);
        }
            break;
        case MpuMcuRequest: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Liquid::CreateSubsystemRequestForLiquidAction(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    LIQUID_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);
        }
            break;
        default:
            break;
    }
    return result;
}

map<int, double> HardwareMessageParser::ParseInductionMessage() throw(Error) {
    using namespace MessageConstants;
    map<int, double> result;
    switch (this->hardware_message_->GetMessageType()) {
        case MpuMcuCommand: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Induction::CreateSubsystemRequestForInductionCommand(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    INDUCTION_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);

            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro Command ");

        }
            break;
        case MpuMcuAction: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Induction::CreateSubsystemRequestForInductionAction(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    INDUCTION_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);

        }
            break;
        case MpuMcuRequest: {

            SubsystemRequestBuilder *subsystemRequestBuilder = new SubsystemRequestBuilder;
            Induction::CreateSubsystemRequestForInductionRequest(
                    *this->hardware_message_, subsystemRequestBuilder->subsystem_request_);
            result = HardwareService::subsystem_object_map_.at(
                    INDUCTION_SUBSYSTEM)->PerformSubsystemRequest(
                    subsystemRequestBuilder->subsystem_request_);
        }
            break;
        default:
            break;
    }

    return result;
}

map<int, double> HardwareMessageParser::ParseCommonMessage() throw(Error) {
    map<int,double> empty_map;
    return empty_map;
}

void HardwareMessageParser::CreateHardwareMessage(JNIEnv *env, jobject message) {

    this->hardware_message_ = new HardwareMessage;

    jclass cls = env->GetObjectClass(message);

    jmethodID get_seq_value = env->GetMethodID(cls, "getSeqNo", "()I");
    jint sequence_number = env->CallIntMethod(message, get_seq_value);
    this->hardware_message_->SetSequenceNumber(sequence_number);


    jmethodID get_id_value = env->GetMethodID(cls, "getId", "()I");
    jint id = env->CallIntMethod(message, get_id_value);
    this->hardware_message_->SetId(id);

    jmethodID get_type_value = env->GetMethodID(cls, "getType", "()I");
    jint type = env->CallIntMethod(message, get_type_value);
    this->hardware_message_->SetMessageType(type);

    jmethodID get_subsystem_value = env->GetMethodID(cls, "getSubSystem", "()I");
    jint subsystem = env->CallIntMethod(message, get_subsystem_value);
    this->hardware_message_->SetSubsystem(subsystem);

    jmethodID get_map_val = env->GetMethodID(cls, "GetMapValue", "(I)Ljava/lang/Double;");
    jobject double_obj = env->CallNonvirtualObjectMethod(message, cls, get_map_val,
                                                         MessageConstants::Primary);
    jdouble primary_val = 0;
    if (double_obj == nullptr) {

    } else {
        jmethodID getDoubleVal = env->GetMethodID(env->GetObjectClass(double_obj), "doubleValue",
                                                  "()D");
        primary_val = env->CallDoubleMethod(double_obj, getDoubleVal);
        this->hardware_message_->GetValueMap()->insert({MessageConstants::Primary, primary_val});
    }

    double_obj = env->CallNonvirtualObjectMethod(message, cls, get_map_val,
                                                 MessageConstants::Secondary);
    jdouble secondary_val = 0;
    if (double_obj == nullptr) {

    } else {
        jmethodID getDoubleVal = env->GetMethodID(env->GetObjectClass(double_obj), "doubleValue",
                                                  "()D");
        secondary_val = env->CallDoubleMethod(double_obj, getDoubleVal);
        this->hardware_message_->GetValueMap()->insert({MessageConstants::Secondary, secondary_val});
    }


    double_obj = env->CallNonvirtualObjectMethod(message, cls, get_map_val,
                                                 MessageConstants::Tertiary);
    jdouble tertiary_val = 0;
    if (double_obj == nullptr) {

    } else {
        jmethodID getDoubleVal = env->GetMethodID(env->GetObjectClass(double_obj), "doubleValue",
                                                  "()D");
        tertiary_val = env->CallDoubleMethod(double_obj, getDoubleVal);
        this->hardware_message_->GetValueMap()->insert({MessageConstants::Tertiary, tertiary_val});
    }

    double_obj = env->CallNonvirtualObjectMethod(message, cls, get_map_val,
                                                 MessageConstants::Abort_Sequence);
    if(double_obj!= nullptr){
        this->hardware_message_->GetValueMap()->insert({MessageConstants::Abort_Sequence,1});
    }
}

jobject HardwareMessageParser::CreateMessageObject(JNIEnv *env, map<int, double> value_map,
                                                   jclass message_object_class) {


    jmethodID set_type_val = env->GetMethodID(message_object_class, "setType",
                                              "(I)V");
    jmethodID constructor = env->GetMethodID(message_object_class, "<init>", "(I)V");
    jobject obj = (*env).NewObject(message_object_class, constructor,
                                   MessageConstants::ControlBoard);

    //Set sequence number field value
    jmethodID set_seq_val = env->GetMethodID(message_object_class, "setSeqNo",
                                            "(I)V");
    env->CallNonvirtualVoidMethod(obj, message_object_class, set_seq_val,
                                  this->hardware_message_->GetSequenceNumber());


    //Set subsystem field value
    jmethodID set_subsystem_val = env->GetMethodID(message_object_class,
                                                   "setSubSystem", "(I)V");
    env->CallNonvirtualVoidMethod(obj, message_object_class, set_subsystem_val,
                                  this->hardware_message_->GetSubsystem());
    //Set id field value
    jmethodID set_id_val = env->GetMethodID(message_object_class, "setId",
                                            "(I)V");
    env->CallNonvirtualVoidMethod(obj, message_object_class, set_id_val,
                                  this->hardware_message_->GetId());

    //Set subsystem type value
    switch (this->hardware_message_->GetMessageType()) {
        case MessageConstants::MpuMcuRequest: {
            env->CallNonvirtualVoidMethod(obj, message_object_class, set_type_val,
                                          MessageConstants::Request_Response);
        }
            break;
        case MessageConstants::MpuMcuCommand: {
            return nullptr;
        }
            break;
        case MessageConstants::MpuMcuAction: {
            env->CallNonvirtualVoidMethod(obj, message_object_class, set_type_val,
                                          MessageConstants::Action_Response);
        }
            break;
        default:
            break;
    }

    jmethodID set_map_value = env->GetMethodID(message_object_class, "SetMapValue", "(ID)V");
    for (auto itr = value_map.begin(); itr != value_map.end(); itr++) {
        env->CallNonvirtualVoidMethod(obj, message_object_class, set_map_value, itr->first,
                                      itr->second);
    }
    return obj;
}

