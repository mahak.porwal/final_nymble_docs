//
// Created by fedal on 29/12/20.
//

#ifndef JULIA_ANDROID_HARDWAREMESSAGEPARSER_H
#define JULIA_ANDROID_HARDWAREMESSAGEPARSER_H


#include "../HardwareMessage/HardwareMessage.h"
#include "../../Error/Error.h"

class HardwareMessageParser {
private:
    HardwareMessage* hardware_message_;
    map<int,double> ParseMacroMessage() throw(Error);
    map<int,double> ParseMicroMessage() throw(Error);
    map<int,double> ParseStirrerMessage() throw(Error);
    map<int,double> ParseLiquidMessage() throw(Error);
    map<int,double> ParseInductionMessage() throw(Error);
    map<int,double> ParseCommonMessage() throw(Error);
    void CreateHardwareMessage(JNIEnv*,jobject);
public:

    HardwareMessageParser(JNIEnv* env,jobject message){
        //Creates an object of HardwareMessage from the Java object of Message class
        this->CreateHardwareMessage(env,message);
    }
    ~HardwareMessageParser(){
        if(this->hardware_message_!= nullptr){
            delete this->hardware_message_;
        }
    }

    map<int,double> ParseHardwareMessage() throw(Error);

    jobject CreateMessageObject(JNIEnv *env, map<int, double> value_map, jclass message_object_class);

};


#endif //JULIA_ANDROID_HARDWAREMESSAGEPARSER_H
