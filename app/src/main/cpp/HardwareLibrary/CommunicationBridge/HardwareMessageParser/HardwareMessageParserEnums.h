//
// Created by fedal on 29/12/20.
//

#ifndef JULIA_ANDROID_HARDWAREMESSAGEPARSERENUMS_H
#define JULIA_ANDROID_HARDWAREMESSAGEPARSERENUMS_H
typedef enum{

}HardwareMessageParseStates_e;
//
// Created by fedal on 29/12/20.
//


// Auto-generated code. Do not edit this manually.

namespace MessageConstants{
    // Message Keys
const int MsgType = 1;
const int Subsystem = 2;
const int ID = 3;
const int Val = 4;

// System ID's
const int MCU_IB = 20;
const int MCU_CB = 21;
const int HCU  = 22;
const int SCU = 23;

// System Tags
const int MCUID = 41;
const int SequenceNum = 42;
const int Message = 43;
const int CRC = 44;

// Type
const int MpuMcuAction = 61;
const int MpuMcuCommand = 62;
const int MpuMcuRequest = 63;
const int MpuMcuAckNack = 64;
const int MpuAction = 65;

// Subsystem
const int Micro = 81;
const int Macro = 82;
const int Liquid = 83;
const int Stirrer = 84;
const int ControlBoard = 85;
const int InductionBoard = 86;
const int HeatingSystem = 87;
const int StirrerSystem = 88;
const int Induction = 89;
const int Common = 90;
// Component ID

/********** MICRO COMPONENT ID **********/

/********** MICRO SENSOR ID **********/
const int Loadcell_1 = 100;
const int Loadcell_2 = 101;
const int Loadcell_Combined = 102;
const int Potentiometer = 103;
const int Barcode = 104;
const int Vib_Current = 105;
const int Rotor_Current = 106;
const int LC_1_Data_ENOB_Value = 107;
const int LC_1_Data_NFB_Value = 108;
const int LC_1_Data_RMS_Noise = 109;
const int LC_1_Data_P2P_Noise = 110;
const int LC_1_Data_Int_Offset = 111;
const int LC_2_Data_ENOB_Value = 112;
const int LC_2_Data_NFB_Value = 113;
const int LC_2_Data_RMS_Noise = 114;
const int LC_2_Data_P2P_Noise = 115;
const int LC_2_Data_Int_Offset = 116;
const int LC_Data_Offset = 117;
const int LC_Data_AVDD = 118;
const int LC_Data_Reference = 119;
const int Barcode_ContinousScan = 120;
const int Barcode_TriggeredScan = 121;
const int Barcode_Trigger = 122;
const int Barcode_Heartbeat = 123;
const int TachometerExhaustFan = 124;

/********** Loadcell Units **********/
const int Grams = 441;
const int MicroVolts = 442;
const int RawValue = 443;

/********** MICRO ACTUATOR ID **********/
const int Vib_Mot_1 = 141;
const int Vib_Mot_2 = 142;
const int Vib_Mot_3 = 143;
const int Vib_Mot_4 = 144;
const int Vib_Mot_5 = 145;
const int Vib_Mot_6 = 146;
const int Vib_Rotor_Arm = 147;
const int Exhaust_Fan = 148;
const int LED_Design = 149;
const int LED_Illumination = 150;
const int LM2678 = 151;
const int AlertINA300 = 152;
const int LatchINA300 = 153;
const int MicroHome = 154;

/********** MACRO COMPONENT ID **********/

/********** MACRO SENSOR ID **********/
const int Macro_Pot_1 = 161;
const int Macro_Pot_2 = 162;
const int Macro_Pot_3 = 163;
const int Macro_Pot_4 = 164;
const int Top_End_SW_1 = 165;
const int Top_End_SW_2 = 166;
const int Top_End_SW_3 = 167;
const int Top_End_SW_4 = 168;
const int Bottom_End_Sw_1 = 169;
const int Bottom_End_Sw_2 = 170;
const int Bottom_End_Sw_3 = 171;
const int Bottom_End_Sw_4 = 172;
const int Macro_Mot_Current = 173;
const int Flap_Mot_Current = 174;
const int Pan_Loadcell = 175;
const int Pan_LC_Data_ENOB_Value = 176;
const int Pan_LC_Data_NFB_Value = 177;
const int Pan_LC_Data_RMS_Noise = 178;
const int Pan_LC_Data_P2P_Noise = 179;
const int Pan_LC_Data_Int_Offset = 180;
const int Pan_LC_Data_Offset = 181;
const int Pan_LC_Data_AVDD = 182;
const int Pan_LC_Data_Reference = 183;

/********** MACRO ACTUATOR ID **********/
const int Macro_Mot_1 = 201;
const int Macro_Mot_2 = 202;
const int Macro_Mot_3 = 203;
const int Macro_Mot_4 = 204;
const int Macro_Flap_Mot = 205;
const int Macro_Solenoid = 206;
const int Macro_ScalingFactor = 207;

/********** Macro Type **********/
const int Non_Liquid_Macro = 651;
const int Liquid_Macro = 652;

/********** Liquid COMPONENT ID **********/

/********** Liquid SENSOR ID **********/
const int Liquid_Current = 221;

/********** Liquid Actuator ID **********/
const int Liquid_Oil_Pump = 241;
const int Liquid_Water_Pump = 242;
const int Liquid_Oil_Pump_PCA = 243;
const int Liquid_Water_Pump_PCA = 244;

/********** Stirrer COMPONENT ID **********/

/********** Stirrer SENSOR ID **********/
const int Stirrer_Current = 261;
const int Stirrer_Feedback = 262;

/********** Stirrer Actuator ID **********/
const int Stirrer_Motor = 281;
const int Stirrer_Profile = 282;

/********** ControlBoard COMPONENT ID **********/
const int Commit_ID = 301;
const int HeartBeat = 302;
const int I2C_Address_List = 303;
const int Control_PCB = 304;
const int Calibrate_Macro = 305;
const int Calibrate_Micro = 306;
const int V_REF = 307;
const int SupplyCurrent = 308;
const int I2C_StateRead = 309;
const int PCA_Frequncy = 310;
const int PCA9685 = 311;
const int INA219 = 312;
const int ADS122C04 = 313;
const int TEMPERATURE = 314;
const int FRAM = 315;

/********** Value Tags **********/
const int Primary  = 321;
const int Secondary = 322;
const int Tertiary = 323;
const int Quaternary = 324;

/********** McuMpu Messages **********/
const int Mcu_Ack_Nack = 401;
const int Request_Response = 402;
const int Action_Response = 403;
const int MpuAction_Response = 404;
const int Error_Alert = 405;

/********** MCU dispense results **********/
const int Dispensed = 421;
const int ActualDispensed = 422;
const int TargetWeight = 423;
const int ErrorCode = 424;

/********** Heat Control unit values **********/
const int TargetTemp = 440;
const int CriticalTemp = 441;
const int TimeToCook = 442;
const int Control = 443;

/********** Induction Actuation ID **********/
const int Set_Power_Level_0 = 500;
const int Set_Power_Level_1 = 501;
const int Set_Power_Level_2 = 502;
const int Set_Power_Level_3 = 503;
const int Set_Power_Level_4 = 504;
const int Set_Power_Level_5 = 505;
const int Set_Power_Level_6 = 506;
const int Set_Power_Level_7 = 507;
const int Set_Power_Level_8 = 508;
const int Set_Power_Level_9 = 509;
const int Set_Power_Level_10 = 510;
const int Set_Power_Level_Idle = 511;

/********** Induction Sensor ID **********/
const int Induction_Current = 601;
const int Induction_Igbt_temp = 602;
const int Induction_Voltage = 603;
const int Induction_Pan_temp = 604;
const int Induction_Pwm_Value = 605;

/********** Induction actuator id **********/
const int Induction_Power = 701;
const int Induction_Fan = 702;
const int Induction_Buzzer = 703;
const int Induction_Int = 704;
const int Induction_Pan = 705;

/********** Induction error codes **********/
const int Induction_Error_No_Pan = 801;
const int Induction_Error_Over_Voltage = 802;
const int Induction_Error_Under_voltage = 803;
const int Induction_Error_Igbt_Temp = 804;
const int Induction_No_Error = 800;

const int Abort_Sequence = 1000;

const string Macro_Container_1 = "Macro_Container_1";

const string Component = "Component";

}
#endif //JULIA_ANDROID_HARDWAREMESSAGEPARSERENUMS_H
