//
// Created by fedal on 29/12/20.
//

#include "HardwareMessage.h"

int HardwareMessage::GetMessageType() const {
    return message_type_;
}

void HardwareMessage::SetMessageType(int messageType) {
    message_type_ = messageType;
}

int HardwareMessage::GetSubsystem() const {
    return subsystem_;
}

void HardwareMessage::SetSubsystem(int subsystem) {
    subsystem_ = subsystem;
}

int HardwareMessage::GetId() const {
    return id_;
}

void HardwareMessage::SetId(int id) {
    id_ = id;
}

map<int,int>* HardwareMessage::GetValueMap() {
    return &(this->value_map_);
}

void HardwareMessage::SetSequenceNumber(int id) {
    this->sequence_number_ = id;
}

int HardwareMessage::GetSequenceNumber() const {
    return this->sequence_number_;
}
