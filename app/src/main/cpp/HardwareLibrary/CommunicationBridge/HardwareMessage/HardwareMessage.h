//
// Created by fedal on 29/12/20.
//

#ifndef JULIA_ANDROID_HARDWAREMESSAGE_H
#define JULIA_ANDROID_HARDWAREMESSAGE_H


class HardwareMessage {
public:
    int GetMessageType() const;

    void SetMessageType(int messageType);

    int GetSubsystem() const;

    void SetSubsystem(int subsystem);

    int GetId() const;

    void SetId(int id);

    int GetSequenceNumber() const;

    void SetSequenceNumber(int id);

    map<int,int>* GetValueMap();

private:
    int message_type_ = -1;
    int subsystem_ = -1;
    int id_ = -1;
    int sequence_number_ = -1;
    map<int,int> value_map_;
};


#endif //JULIA_ANDROID_HARDWAREMESSAGE_H
