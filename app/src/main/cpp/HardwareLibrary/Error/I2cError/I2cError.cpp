//
// Created by fedal on 10/12/20.
//

#include "I2cError.h"

I2cError_e I2cError::getI2CError() const {
    return i2c_error_;
}

void I2cError::setI2CError(I2cError_e i2CError) {
    i2c_error_ = i2CError;
}

I2cDevices_e I2cError::getI2CDevice() const {
    return i2c_device_;
}

void I2cError::setI2CDevice(I2cDevices_e i2CDevice) {
    i2c_device_ = i2CDevice;
}
