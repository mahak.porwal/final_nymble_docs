//
// Created by fedal on 10/12/20.
//

#ifndef JULIA_ANDROID_I2CERROR_H
#define JULIA_ANDROID_I2CERROR_H

#include "../../HardwareService/Peripherals/I2C/I2CRequestProcessor/I2CRequestProcessor.h"

typedef enum{
    DEVICE_NACK,
    BUS_NOT_OPENED
}I2cError_e;
class I2cError {
private:
    I2cError_e i2c_error_;
    I2cDevices_e i2c_device_;
public:
    I2cError_e getI2CError() const;

    void setI2CError(I2cError_e i2CError);

    I2cDevices_e getI2CDevice() const;

    void setI2CDevice(I2cDevices_e i2CDevice);
};


#endif //JULIA_ANDROID_I2CERROR_H
