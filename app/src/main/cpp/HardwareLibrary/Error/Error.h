//
// Created by fedal on 8/12/20.
//

#ifndef JULIA_ANDROID_ERROR_H
#define JULIA_ANDROID_ERROR_H

#include <exception>
#include "ErrorEnums.h"

class Error {
private:
    Error_e error = DEFAULT_ERROR;
public:
    Error(Error_e error) {
        this->error = error;
    }

    Error() {

    }

    Error_e GetError() { return this->error; };
};

#endif //JULIA_ANDROID_ERROR_H
