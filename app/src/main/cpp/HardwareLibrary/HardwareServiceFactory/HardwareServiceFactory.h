//
// Created by fedal on 30/4/21.
//

#ifndef RECIPEENGINE_HARDWARESERVICEFACTORY_H
#define RECIPEENGINE_HARDWARESERVICEFACTORY_H

#include "../BaseHardwareService/BaseHardwareService.h"
#include "../DummyHardwareService/DummyHardwareService.h"
#include "../HardwareService/HardwareService.h"

class HardwareServiceFactory {

private:
    static const bool create_dummy_service_ = false;
protected:
    static BaseHardwareService *hardware_service;

    HardwareServiceFactory(bool create_dummy_service) {
        if (hardware_service == nullptr) {
            if (create_dummy_service) {
                hardware_service = new DummyHardwareService();
            } else {
                hardware_service = new HardwareService();
            }
        }
    }

public:
    static BaseHardwareService *GetHardwareService();

    static void StopHardwareService();
};

BaseHardwareService *HardwareServiceFactory::hardware_service = nullptr;

BaseHardwareService *HardwareServiceFactory::GetHardwareService() {
    if (hardware_service == nullptr) {
        new HardwareServiceFactory(HardwareServiceFactory::create_dummy_service_);
    }
    return hardware_service;
}

void HardwareServiceFactory::StopHardwareService() {
    delete hardware_service;
}

#endif //RECIPEENGINE_HARDWARESERVICEFACTORY_H
