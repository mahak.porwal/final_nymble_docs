//
// Created by fedal on 30/4/21.
//

#include "DummyHardwareService.h"

void DummyHardwareService::StartHardwareService() {

}

void DummyHardwareService::StopHardwareService() {

}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
DummyHardwareService::ProcessComponentRequest(const ComponentRequest &) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response;
    response[SEQUENCE_DONE] = std::vector<std::string>();
    response[SEQUENCE_DONE].push_back(to_string(1));
    return response;
}

void DummyHardwareService::StartInductionControl() {

}

void DummyHardwareService::InitThermalCamera() {

}
const Component &DummyHardwareService::GetComponent(const ComponentRequest &component_request) {
    return this->component_map_->at(component_request.GetComponent());
}

void DummyHardwareService::StopInductionControl() {

}

void DummyHardwareService::PopulateHardwareFactors(JNIEnv *env, jobject object) {

}
