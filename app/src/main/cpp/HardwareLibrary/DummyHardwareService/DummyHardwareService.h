//
// Created by fedal on 30/4/21.
//

#ifndef RECIPEENGINE_DUMMYHARDWARESERVICE_H
#define RECIPEENGINE_DUMMYHARDWARESERVICE_H

#include "../HardwareService/Peripherals/Pwm/PwmRequestProcessor/PwmRequestProcessor.h"
#include "../HardwareService/Peripherals/Gpio/GpioRequestProcessor/GpioRequestProcessor.h"
#include "../HardwareService/Peripherals/Adc/AdcRequestProcessor/AdcRequestProcessor.h"
#include "../HardwareService/Peripherals/I2C/I2C.h"
class DummyHardwareService : public BaseHardwareService {
private:
    void StartHardwareService();

    void StopHardwareService();

    void PopulateComponentMap();

    std::unordered_map<Component_e, const Component &> *component_map_;


public:
    static PwmRequestProcessor *pwm_request_processor_;
    static GpioRequestProcessor *gpio_request_processor_;
    static AdcRequestProcessor *adc_request_processor_;

    static I2C *i2c_;

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessComponentRequest(const ComponentRequest &);

    void StartInductionControl();
    void StopInductionControl();
    void InitThermalCamera();
    void PopulateHardwareFactors(JNIEnv* env, jobject object);
    const Component &GetComponent(const ComponentRequest &component_request);

    DummyHardwareService() {
        this->StartHardwareService();
        this->component_map_ = new std::unordered_map<Component_e, const Component &>;
    }

    ~DummyHardwareService() {
        this->StopHardwareService();
    }
};

PwmRequestProcessor *DummyHardwareService::pwm_request_processor_;
GpioRequestProcessor *DummyHardwareService::gpio_request_processor_;
AdcRequestProcessor *DummyHardwareService::adc_request_processor_;
I2C *DummyHardwareService::i2c_;

#endif //RECIPEENGINE_DUMMYHARDWARESERVICE_H
