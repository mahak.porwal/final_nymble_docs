//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_INDUCTIONSTATEMACHINE_H
#define RECIPEENGINE_INDUCTIONSTATEMACHINE_H

#include "InductionControlEnums.h"
#include "InductionSensor/InductionSensor.h"

using namespace induction_state_machine;

class InductionStateMachine {
public:
    virtual void Execute(InductionSensor) = 0;

    virtual void SetControlEvent(InductionControlEvent_e) = 0;

    virtual InductionControlState_e GetInductionControlState() = 0;

    virtual InductionError_e GetError() = 0;

    virtual ~InductionStateMachine() {}
};


#endif //RECIPEENGINE_INDUCTIONSTATEMACHINE_H
