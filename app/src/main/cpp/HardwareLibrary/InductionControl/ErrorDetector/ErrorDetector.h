//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_ERRORDETECTOR_H
#define RECIPEENGINE_ERRORDETECTOR_H

#include "../InductionControlEnums.h"
#include "../InductionSensor/InductionSensor.h"

class ErrorDetector {
public:
    virtual InductionError_e DetectAndGetError(InductionSensor) = 0;

    virtual void SetNoPanError() = 0;

    virtual void ResetError() = 0;

    virtual ~ErrorDetector() {}
};


#endif //RECIPEENGINE_ERRORDETECTOR_H
