//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_BASEERRORDETECTOR_H
#define RECIPEENGINE_BASEERRORDETECTOR_H

#include "../ErrorDetector.h"

class BaseErrorDetector : public ErrorDetector {
private:
    InductionError_e induction_error_ = No_Error;

    bool IsOutOfRange(float value, float upper_value, float lower_value);

    void DetectErrors(InductionSensor induction_sensor);

    /**
     * Thresholds for error
     */
     float induction_voltage_upper_threshold_;
     float induction_voltage_lower_threshold_;
     float induction_current_upper_threshold_;
     float induction_current_lower_threshold_;
     float induction_igbt_temperature_upper_threshold_;
     float induction_igbt_temperature_lower_threshold_;
     float induction_pan_temperature_upper_threshold_;
     float induction_pan_temperature_lower_threshold_;

public:

    BaseErrorDetector() {
        this->induction_voltage_lower_threshold_ = 90;
        this->induction_voltage_upper_threshold_ = 140;
        this->induction_current_lower_threshold_ = 0;
        this->induction_current_upper_threshold_ = 16;
        this->induction_igbt_temperature_lower_threshold_ = 0;
        this->induction_igbt_temperature_upper_threshold_ = 90;
        this->induction_pan_temperature_lower_threshold_ = 0;
        this->induction_pan_temperature_upper_threshold_ = 200;
    }

    InductionError_e DetectAndGetError(InductionSensor);

    void SetNoPanError();

    void ResetError();

    ~BaseErrorDetector() {}
};


#endif //RECIPEENGINE_BASEERRORDETECTOR_H
