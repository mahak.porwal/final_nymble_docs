//
// Created by fedal on 24/7/21.
//

#include "BaseErrorDetector.h"

InductionError_e BaseErrorDetector::DetectAndGetError(InductionSensor induction_sensors) {
    if (this->induction_error_ == No_Error) {
        this->DetectErrors(induction_sensors);
    }
    return No_Error;
}

void BaseErrorDetector::SetNoPanError() {
    this->induction_error_ = No_Pan_Error;
}

void BaseErrorDetector::ResetError() {
    this->induction_error_ = No_Error;
}

void BaseErrorDetector::DetectErrors(InductionSensor induction_sensor) {
    int result = -1;
    if (IsOutOfRange(induction_sensor.GetInputCurrent(), this->induction_current_upper_threshold_,
                     this->induction_current_lower_threshold_)) {
        this->induction_error_ = Current_Error;
        return;
    }
    if (IsOutOfRange(induction_sensor.GetInputVoltage(), this->induction_voltage_upper_threshold_,
                     this->induction_voltage_lower_threshold_)) {
        this->induction_error_ = Voltage_Error;
        return;
    }
    if (IsOutOfRange(induction_sensor.GetIgbtTemperature(),
                     this->induction_igbt_temperature_upper_threshold_,
                     this->induction_igbt_temperature_lower_threshold_)) {
        this->induction_error_ = Igbt_Temp_Error;
        return;
    }
    /*if (IsOutOfRange(this->induction_pan_temperature_,
                     this->induction_pan_temperature_upper_threshold_,
                     this->induction_pan_temperature_lower_threshold_)) {
        throw new Error;
    }*/
}

bool BaseErrorDetector::IsOutOfRange(float value, float upper_value, float lower_value) {
    //return true;
    return (value >= upper_value || value <= lower_value);
}