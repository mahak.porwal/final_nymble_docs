//
// Created by fedal on 25/7/21.
//

#ifndef RECIPEENGINE_INDUCTIONSTATEMACHINEFACTORY_H
#define RECIPEENGINE_INDUCTIONSTATEMACHINEFACTORY_H

#include "BaseStateMachine/BaseStateMachine.h"
#include "PowerControllerFactory.h"
#include "ErrorDetectorFactory.h"
#include "ErrorResolverFactory.h"

class InductionStateMachineFactory {
private:
    static InductionStateMachine *induction_state_machine_;
public:
    static InductionStateMachine *GetInductionStateMachine() {
        if (induction_state_machine_ == NULL) {

            //Create power controller
            PowerController *power_controller = PowerControllerFactory::GetPowerController();

            //Create error resolver
            ErrorResolver *error_resolver = ErrorResolverFactory::GetErrorResolver();

            //Create error detector
            ErrorDetector *error_detector = ErrorDetectorFactory::GetErrorDetector();

            //Create induction state machine
            induction_state_machine_ = new BaseStateMachine(power_controller, error_resolver,
                                                                  error_detector);
        }
        return induction_state_machine_;
    }
};

InductionStateMachine *InductionStateMachineFactory::induction_state_machine_;

#endif //RECIPEENGINE_INDUCTIONSTATEMACHINEFACTORY_H
