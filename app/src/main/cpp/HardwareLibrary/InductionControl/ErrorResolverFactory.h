//
// Created by fedal on 25/7/21.
//

#ifndef RECIPEENGINE_ERRORRESOLVERFACTORY_H
#define RECIPEENGINE_ERRORRESOLVERFACTORY_H

#include "ErrorResolver/BaseErrorResolver/BaseErrorResolver.h"

class ErrorResolverFactory {
private:
    static ErrorResolver *error_resolver_;
public:
    static ErrorResolver *GetErrorResolver() {
        if (error_resolver_ == NULL) {
            //Create error resolver
            error_resolver_ = new BaseErrorResolver();
        }
        return error_resolver_;
    }
};

ErrorResolver *ErrorResolverFactory::error_resolver_;

#endif //RECIPEENGINE_ERRORRESOLVERFACTORY_H
