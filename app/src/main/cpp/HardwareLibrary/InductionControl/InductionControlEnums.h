//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_INDUCTIONCONTROLENUMS_H
#define RECIPEENGINE_INDUCTIONCONTROLENUMS_H

typedef enum {
    Error_Resolved,
    Error_Not_Resolved,
    Error_Hard_Fault,
    Error_Wait_For_External_Correction
} InductionErrorResolveStatus_e;

typedef enum {
    Igbt_Temp_Error,
    Pan_Temp_Error,
    Current_Error,
    Voltage_Error,
    No_Pan_Error,
    No_Error
} InductionError_e;

namespace induction_state_machine {
    typedef enum {
        Power_Level_One,
        Power_Level_Two,
        Power_Level_Three,
        Power_Level_Four,
        Power_Level_Five,
        Power_Level_Six,
        Power_Level_Seven,
        Shutdown_Control_Event,
        Error_Detected,
        Unable_To_Resolve_Error,
        External_Help_Needed,
        Error_Resolved_Event,
        Detect_Pan,
        Manual_Mode_Requested,
        Null_Event
    } InductionControlEvent_e;

    typedef enum {
        Power_Control_State,
        Error_State,
        Idle_State,
        Waiting_For_Error_Correction_State,
        Pan_Detection_State,
        Hard_Fault_State,
        Manual_Mode_State,
        Null_State
    } InductionControlState_e;
}
namespace induction_control_input {
    typedef enum {
        Power_Level_One,
        Power_Level_Two,
        Power_Level_Three,
        Power_Level_Four,
        Power_Level_Five,
        Power_Level_Six,
        Power_Level_Seven,
        Detect_Pan_Input,
        Shutdown,
        Manual_Mode,
        Null_Input
    } InductionControlInput_e;
}
namespace induction_power_level {
    typedef enum {
        Power_Level_One,
        Power_Level_Two,
        Power_Level_Three,
        Power_Level_Four,
        Power_Level_Five,
        Power_Level_Six,
        Power_Level_Seven
    } InductionPowerLevels_e;
}

#endif //RECIPEENGINE_INDUCTIONCONTROLENUMS_H
