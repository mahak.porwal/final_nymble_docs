//
// Created by fedal on 25/7/21.
//

#ifndef RECIPEENGINE_CONTROLINPUTPROCESSORFACTORY_H
#define RECIPEENGINE_CONTROLINPUTPROCESSORFACTORY_H

#include "ControlInputProcessor.h"

class ControlInputProcessorFactory {
private:
    static ControlInputProcessor *induction_control_processor_;
public:
    static ControlInputProcessor *GetControlInputProcessor() {
        if (induction_control_processor_ == NULL) {
            //Create control input processor
            induction_control_processor_ = new ControlInputProcessor();
        }
        return induction_control_processor_;
    }
};

ControlInputProcessor *ControlInputProcessorFactory::induction_control_processor_;

#endif //RECIPEENGINE_CONTROLINPUTPROCESSORFACTORY_H
