//
// Created by fedal on 25/7/21.
//

#ifndef RECIPEENGINE_POWERCONTROLLERFACTORY_H
#define RECIPEENGINE_POWERCONTROLLERFACTORY_H

#include "PowerController/BasePowerController/BasePowerController.h"

class PowerControllerFactory {
    static PowerController *power_controller_;
public:
    static PowerController *GetPowerController() {
        if (power_controller_ == NULL) {
            //Create power controller
            power_controller_ = new BasePowerController();
        }
        return power_controller_;
    }
};

PowerController *PowerControllerFactory::power_controller_;

#endif //RECIPEENGINE_POWERCONTROLLERFACTORY_H
