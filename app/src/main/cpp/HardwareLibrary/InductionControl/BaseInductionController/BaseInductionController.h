//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_BASEINDUCTIONCONTROLLER_H
#define RECIPEENGINE_BASEINDUCTIONCONTROLLER_H

#include"../InductionController.h"
#include "../ControlInputProcessor.h"
#include "../InductionStateMachine.h"
#include "../../Subsystem/Induction/Utilities/InductionSensorReader/InductionSensorReader.h"

class BaseInductionController : public InductionController {
private :
    ControlInputProcessor *control_input_processor_;
    InductionStateMachine *induction_state_machine_;
    InductionSensorReader *induction_sensor_reader_;
public:

    BaseInductionController(ControlInputProcessor *control_input_processor,
                            InductionStateMachine *induction_state_machine,
                            InductionSensorReader *induction_sensor_reader) {
        this->control_input_processor_ = control_input_processor;
        this->induction_state_machine_ = induction_state_machine;
        this->induction_sensor_reader_ = induction_sensor_reader;
    }

    void StartControl();

    void SetInput(induction_control_input::InductionControlInput_e);

    InductionControlState_e GetInductionControlState();

    InductionError_e GetError();

    ~BaseInductionController() {
        delete this->control_input_processor_;
        delete this->induction_state_machine_;
        delete this->induction_sensor_reader_;
    }
};


#endif //RECIPEENGINE_BASEINDUCTIONCONTROLLER_H
