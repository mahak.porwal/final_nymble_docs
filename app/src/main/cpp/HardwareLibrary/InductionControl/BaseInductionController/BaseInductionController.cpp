//
// Created by fedal on 24/7/21.
//B

#include "BaseInductionController.h"

void BaseInductionController::StartControl() {

    if (!is_induction_controlled_) {
        is_induction_controlled_ = true;
        this->TurnOnInductionFan();
        while (is_induction_controlled_) {

            InductionControlEvent_e control_event = this->control_input_processor_->Process(
                    this->GetInductionControlState()
            );

            if (control_event != Null_Event) {
                this->induction_state_machine_->SetControlEvent(control_event);
            }
            __android_log_print(ANDROID_LOG_ERROR, "native", "\n Poll sensor start");
            //read all sensors
            InductionSensor induction_sensor = this->induction_sensor_reader_->PollSensors();
            __android_log_print(ANDROID_LOG_ERROR, "native", "\n Poll sensor end");

            /*    __android_log_print(ANDROID_LOG_ERROR, "native", "\n Voltage  %.4f\n"
                                                                 "\n Current  %.4f\n"
                                                                 "\n Igbt Temp  %.4f\n"
                                                                 "\n Pan Temp  %.4f\n",
                                    induction_sensor.GetInputVoltage(),
                                    induction_sensor.GetInputCurrent(),
                                    induction_sensor.GetIgbtTemperature(),
                                    induction_sensor.GetPanTemperature());*/

            this->induction_state_machine_->Execute(induction_sensor);
        }
        this->TurnOffInductionIntPin();
        __android_log_print(ANDROID_LOG_ERROR, "native", "\n Exiting induction control");
    }
}

void
BaseInductionController::SetInput(induction_control_input::InductionControlInput_e control_input) {
    this->control_input_processor_->SetControlInput(control_input);
}

InductionControlState_e BaseInductionController::GetInductionControlState() {
    return this->induction_state_machine_->GetInductionControlState();
}

InductionError_e BaseInductionController::GetError() {
    return this->induction_state_machine_->GetError();
}
