//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_CONTROLINPUTPROCESSOR_H
#define RECIPEENGINE_CONTROLINPUTPROCESSOR_H

#include "InductionControlEnums.h"
#include <mutex>

using namespace induction_control_input;

class ControlInputProcessor {
private:
    typedef std::unordered_map<InductionControlInput_e, InductionControlEvent_e>
            TransitionFromStateMap;
    typedef std::unordered_map<InductionControlState_e,
            TransitionFromStateMap> TransitionMap;

    InductionControlInput_e induction_control_input_ = Null_Input;
    std::mutex control_input_lock_;

    static TransitionMap transition_map_;
    static TransitionFromStateMap idle_state_map_;
    static TransitionFromStateMap power_control_state_map_;
    static TransitionFromStateMap pan_detection_state_map_;
    static TransitionFromStateMap waiting_for_error_correction_state_map_;
    static TransitionFromStateMap manual_mode_state_map_;

    //induction_control_input should be guaranteed to be not Null_Input
    InductionControlEvent_e CalculateControlEvent(InductionControlState_e induction_control_state,
                                                  InductionControlInput_e induction_control_input) {
        InductionControlEvent_e control_event = Null_Event;
        __android_log_print(ANDROID_LOG_ERROR, "power set ", " control event generatred %d",
                            control_event);
        if (transition_map_.find(induction_control_state) != transition_map_.end()) {
            TransitionFromStateMap transition_from_state = transition_map_.at(
                    induction_control_state);
            if (transition_from_state.find(induction_control_input) !=
                transition_from_state.end()) {
                control_event = transition_from_state.at(induction_control_input);
                __android_log_print(ANDROID_LOG_ERROR, "power set ", " control event generatred %d",
                                    control_event);
            }
        }
        return control_event;
    }

public:
    void SetControlInput(induction_control_input::InductionControlInput_e induction_control_input) {
        std::lock_guard<std::mutex> set_lock_guard_(this->control_input_lock_);
        __android_log_print(ANDROID_LOG_ERROR, "power set ", " set lock acquired");
        this->induction_control_input_ = induction_control_input;
        __android_log_print(ANDROID_LOG_ERROR, "power set ", " set lock acquired");
    }

    InductionControlEvent_e Process(InductionControlState_e induction_control_state) {
        std::lock_guard<std::mutex> process_lock_guard_(this->control_input_lock_);
        InductionControlEvent_e control_event = Null_Event;
        if (this->induction_control_input_ != Null_Input) {
            __android_log_print(ANDROID_LOG_ERROR, "power set ", " non-null control input found");
            //new induction control input
            //consume it and then mark it Null_Input
            //the lock guard will ensure that while the control input is being consumed, no one
            //will write to the induction_control_input


            //How to consume the control input and spit out the control event?
            //Use the argument induction_control_state and the control input to
            //calculate the control event, i.e.
            //control_event = func(control_input, control_state)
            control_event = this->CalculateControlEvent(induction_control_state,
                                                        this->induction_control_input_);
        }
        this->induction_control_input_ = Null_Input;
        return control_event;
    }

    ~ControlInputProcessor() {

    }
};

ControlInputProcessor::TransitionFromStateMap ControlInputProcessor::idle_state_map_ = {
        {induction_control_input::Power_Level_One,   induction_state_machine::Power_Level_One},
        {induction_control_input::Power_Level_Two,   induction_state_machine::Power_Level_Two},
        {induction_control_input::Power_Level_Three, induction_state_machine::Power_Level_Three},
        {induction_control_input::Power_Level_Four,  induction_state_machine::Power_Level_Four},
        {induction_control_input::Power_Level_Five,  induction_state_machine::Power_Level_Five},
        {induction_control_input::Power_Level_Six,   induction_state_machine::Power_Level_Six},
        {induction_control_input::Power_Level_Seven, induction_state_machine::Power_Level_Seven},
        {Detect_Pan_Input,                           Detect_Pan},
        {Manual_Mode,                                Manual_Mode_Requested}
};

ControlInputProcessor::TransitionFromStateMap ControlInputProcessor::power_control_state_map_ = {
        {induction_control_input::Power_Level_One,   induction_state_machine::Power_Level_One},
        {induction_control_input::Power_Level_Two,   induction_state_machine::Power_Level_Two},
        {induction_control_input::Power_Level_Three, induction_state_machine::Power_Level_Three},
        {induction_control_input::Power_Level_Four,  induction_state_machine::Power_Level_Four},
        {induction_control_input::Power_Level_Five,  induction_state_machine::Power_Level_Five},
        {induction_control_input::Power_Level_Six,   induction_state_machine::Power_Level_Six},
        {induction_control_input::Power_Level_Seven, induction_state_machine::Power_Level_Seven},
        {Shutdown,                                   Shutdown_Control_Event}
};

ControlInputProcessor::TransitionFromStateMap ControlInputProcessor::pan_detection_state_map_ = {
        {Shutdown, Shutdown_Control_Event}
};

ControlInputProcessor::TransitionFromStateMap
        ControlInputProcessor::waiting_for_error_correction_state_map_ = {
        {Shutdown, Shutdown_Control_Event}
};

ControlInputProcessor::TransitionFromStateMap
        ControlInputProcessor::manual_mode_state_map_ = {
        {Shutdown, Shutdown_Control_Event}
};

ControlInputProcessor::TransitionMap ControlInputProcessor::transition_map_ = {
        {Idle_State,                         ControlInputProcessor::idle_state_map_},
        {Power_Control_State,                ControlInputProcessor::power_control_state_map_},
        {Pan_Detection_State,                ControlInputProcessor::pan_detection_state_map_},
        {Waiting_For_Error_Correction_State, ControlInputProcessor::waiting_for_error_correction_state_map_},
        {Manual_Mode_State,                  ControlInputProcessor::manual_mode_state_map_}
};

#endif //RECIPEENGINE_CONTROLINPUTPROCESSOR_H
