//
// Created by fedal on 25/7/21.
//

#ifndef RECIPEENGINE_ERRORDETECTORFACTORY_H
#define RECIPEENGINE_ERRORDETECTORFACTORY_H

#include "ErrorDetector/BaseErrorDetector/BaseErrorDetector.h"

class ErrorDetectorFactory {
private:
    static ErrorDetector *error_detector_;
public:
    static ErrorDetector *GetErrorDetector() {
        if (error_detector_ == NULL) {
            //Create error detector
            error_detector_ = new BaseErrorDetector();
        }
        return error_detector_;
    }
};

ErrorDetector *ErrorDetectorFactory::error_detector_;
#endif //RECIPEENGINE_ERRORDETECTORFACTORY_H
