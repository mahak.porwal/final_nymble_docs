//
// Created by fedal on 25/7/21.
//

#ifndef RECIPEENGINE_INDUCTIONCONTROLLERFACTORY_H
#define RECIPEENGINE_INDUCTIONCONTROLLERFACTORY_H


#include "BaseInductionController/BaseInductionController.h"
#include "ControlInputProcessorFactory.h"
#include "InductionStateMachineFactory.h"
#include "../Subsystem/Induction/Utilities/InductionSensorReader/InductionSensorReader.h"

class InductionControllerFactory {
private:
    static InductionController *induction_controller_;
public:
    static InductionController *GetInductionController() {
        if (induction_controller_ == NULL) {

            //Get control input processor
            ControlInputProcessor *control_input_processor =
                    ControlInputProcessorFactory::GetControlInputProcessor();

            //Get induction state machine
            InductionStateMachine *induction_state_machine =
                    InductionStateMachineFactory::GetInductionStateMachine();

            //Create induction sensor reader
            InductionSensorReader *induction_sensor_reader = new InductionSensorReader();

            //Create induction controller
            induction_controller_ = new BaseInductionController(control_input_processor,
                                                                induction_state_machine,
                                                                induction_sensor_reader);
        }
        return induction_controller_;
    }
};

InductionController *InductionControllerFactory::induction_controller_;

#endif //RECIPEENGINE_INDUCTIONCONTROLLERFACTORY_H
