//
// Created by fedal on 24/7/21.
//

#include "BasePowerController.h"

void BasePowerController::ControlPower(InductionSensor induction_sensor) {
    if (this->power_control_behavior_ != NULL) {
        this->power_control_behavior_->ControlPower(induction_sensor);
    }
}

bool BasePowerController::IsPanDetected() {
    if (this->power_control_behavior_ != NULL) {
        return this->power_control_behavior_->IsPanDetected();
    }
    return true;
}

void BasePowerController::SetPowerLevel(InductionPowerLevels_e power_level) {
    if (this->power_control_behavior_ != NULL) {
        delete this->power_control_behavior_;
    }
    switch (power_level) {
        case Power_Level_One:
            this->power_control_behavior_ = new PowerLevelOne();
            __android_log_print(ANDROID_LOG_ERROR, "power set ", " power level one set");
            break;
        case Power_Level_Two:
            this->power_control_behavior_ = new PowerLevelTwo();
            __android_log_print(ANDROID_LOG_ERROR, "power set ", " power level two set");
            break;
        case Power_Level_Three:
            this->power_control_behavior_ = new PowerLevelThree();
            __android_log_print(ANDROID_LOG_ERROR, "power set ", " power level three set");
            break;
        case Power_Level_Four:
            this->power_control_behavior_ = new PowerLevelFour();
            __android_log_print(ANDROID_LOG_ERROR, "power set ", " power level four set");
            break;
        case Power_Level_Five:
            this->power_control_behavior_ = new PowerLevelFive();
            __android_log_print(ANDROID_LOG_ERROR, "power set ", " power level Five set");
            break;
        case Power_Level_Six:
            this->power_control_behavior_ = new PowerLevelSix();
            __android_log_print(ANDROID_LOG_ERROR, "power set ", " power level six set");
            break;
        case Power_Level_Seven:
            this->power_control_behavior_ = new PowerLevelSeven();
            __android_log_print(ANDROID_LOG_ERROR, "power set ", " power level seven set");
            break;
        default:
            break;
    }
}
