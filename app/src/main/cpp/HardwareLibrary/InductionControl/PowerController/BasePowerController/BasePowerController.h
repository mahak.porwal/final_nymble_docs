//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_BASEPOWERCONTROLLER_H
#define RECIPEENGINE_BASEPOWERCONTROLLER_H

#include "../PowerController.h"
#include "../PowerControlBehavior/PidPowerController/PowerLevelOne.h"
#include "../PowerControlBehavior/PidPowerController/PowerLevelTwo.h"
#include "../PowerControlBehavior/PidPowerController/PowerLevelThree.h"
#include "../PowerControlBehavior/PidPowerController/PowerLevelFive.h"
#include "../PowerControlBehavior/PidPowerController/PowerLevelFour.h"
#include "../PowerControlBehavior/PidPowerController/PowerLevelSix.h"
#include "../PowerControlBehavior/PidPowerController/PowerLevelSeven.h"


class BasePowerController : public PowerController {
private:
    bool pan_detected_;
    PowerControlBehavior *power_control_behavior_;// = new PowerLevelFour;
public:

    void ControlPower(InductionSensor);

    bool IsPanDetected();

    void SetPowerLevel(InductionPowerLevels_e);
};


#endif //RECIPEENGINE_BASEPOWERCONTROLLER_H
