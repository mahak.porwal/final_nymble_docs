//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_POWERCONTROLLER_H
#define RECIPEENGINE_POWERCONTROLLER_H

#include "../InductionSensor/InductionSensor.h"
#include "../InductionControlEnums.h"
#include "../../HardwareServiceFactory/HardwareServiceFactory.h"

using namespace induction_power_level;

class PowerController {
public:

    virtual void ControlPower(InductionSensor) = 0;

    virtual bool IsPanDetected() = 0;

    virtual void SetPowerLevel(InductionPowerLevels_e) = 0;

    void ShutdownInduction() {
        ComponentRequestBuilder component_request_builder;
        component_request_builder.component_request_.SetComponent(
                INDUCTION_ACTUATOR).SetSpeed(0);

        HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_);

        component_request_builder.component_request_.SetComponent(
                INDUCTION_INT_GPIO).SetState(OFF_STATE);
        HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_);
    }

    virtual ~PowerController() {}
};


#endif //RECIPEENGINE_POWERCONTROLLER_H
