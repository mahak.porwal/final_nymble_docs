//
// Created by fedal on 16/10/21.
//

#include "BasePowerControllerV2.h"

void BasePowerControllerV2::ControlPower(InductionSensor induction_sensor) {
    //There are four different states of power control
    //1. Startup
    //2. Default parameters
    //3. Target parameters
    //4. Closed loop control of wattage
    //When the pin K is shifted from low to high state, the state machine enters in the Startup state
    //It then transitions to Default parameters state and stays there until the default parameters are achieved.
    // After that it moves to the Target parameters state. And stays there until the target parameters are achieved.
    //After attaining the target parameters, the state machine moves to the Closed loop control
    //state.
    //While the state machine is in the Closed loop control state, and a change of power level comes in,
    //then it moves to the Target parameters state to attain the target parameters of the new power level.
}

bool BasePowerControllerV2::IsPanDetected() {
    return false;
}

void BasePowerControllerV2::SetPowerLevel(InductionPowerLevels_e induction_power_level) {

}

void BasePowerControllerV2::ShutdownInduction() {
    //The power control state machine resets to the default state
    //
    //Turning off the induction through the K pin
    PowerController::ShutdownInduction();
}
