//
// Created by fedal on 16/10/21.
//

#ifndef RECIPEENGINE_BASEPOWERCONTROLLERV2_H
#define RECIPEENGINE_BASEPOWERCONTROLLERV2_H


class BasePowerControllerV2 : PowerController {
public:
    void ControlPower(InductionSensor);

    bool IsPanDetected();

    void SetPowerLevel(InductionPowerLevels_e);

    void ShutdownInduction();
};


#endif //RECIPEENGINE_BASEPOWERCONTROLLERV2_H
