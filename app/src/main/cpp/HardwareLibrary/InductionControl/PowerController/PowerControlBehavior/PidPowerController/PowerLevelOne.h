//
// Created by fedal on 26/7/21.
//

#ifndef RECIPEENGINE_POWERLEVELONE_H
#define RECIPEENGINE_POWERLEVELONE_H

#include "PidPowerController.h"

class PowerLevelOne : public PidPowerController {
private:
    const int POWER_LEVEL = 200;
public:
    PowerLevelOne() : PidPowerController(Power_Level_One) {
        PidPowerController::induction_control_pid_.SetTargetValue(POWER_LEVEL);
    }

    ~PowerLevelOne() {

    }
};


#endif //RECIPEENGINE_POWERLEVELONE_H
