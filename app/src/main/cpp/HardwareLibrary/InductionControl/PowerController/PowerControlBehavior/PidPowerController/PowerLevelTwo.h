//
// Created by fedal on 26/7/21.
//

#ifndef RECIPEENGINE_POWERLEVELTWO_H
#define RECIPEENGINE_POWERLEVELTWO_H


class PowerLevelTwo : public PidPowerController {
private:
    const int POWER_LEVEL = 400;
public:
    PowerLevelTwo() : PidPowerController(Power_Level_Two) {
        PidPowerController::induction_control_pid_.SetTargetValue(POWER_LEVEL);
    }

    ~PowerLevelTwo() {}
};


#endif //RECIPEENGINE_POWERLEVELTWO_H
