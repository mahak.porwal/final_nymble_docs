//
// Created by fedal on 26/7/21.
//

#ifndef RECIPEENGINE_POWERLEVELTHREE_H
#define RECIPEENGINE_POWERLEVELTHREE_H


class PowerLevelThree : public PidPowerController {
private:
    const int POWER_LEVEL = 600;
public:
    PowerLevelThree() : PidPowerController(Power_Level_Three) {
        PidPowerController::induction_control_pid_.SetTargetValue(POWER_LEVEL);
    }

    ~PowerLevelThree() {}
};


#endif //RECIPEENGINE_POWERLEVELTHREE_H
