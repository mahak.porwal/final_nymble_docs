//
// Created by fedal on 26/7/21.
//

#ifndef RECIPEENGINE_POWERLEVELFIVE_H
#define RECIPEENGINE_POWERLEVELFIVE_H


class PowerLevelFive : public PidPowerController {
private:
    const int POWER_LEVEL = 1000;
public:
    PowerLevelFive() : PidPowerController(Power_Level_Five) {
        PidPowerController::induction_control_pid_.SetTargetValue(POWER_LEVEL);
    }

    ~PowerLevelFive() {}
};


#endif //RECIPEENGINE_POWERLEVELFIVE_H
