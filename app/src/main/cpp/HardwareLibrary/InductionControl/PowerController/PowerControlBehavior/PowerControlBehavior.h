//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_POWERCONTROLBEHAVIOR_H
#define RECIPEENGINE_POWERCONTROLBEHAVIOR_H

#include "../../InductionSensor/InductionSensor.h"
#include "../../InductionControlEnums.h"

class PowerControlBehavior {
public:
    virtual void ControlPower(InductionSensor) = 0;

    virtual bool IsPanDetected() = 0;

    virtual InductionPowerLevels_e GetPowerLevel() = 0;

    virtual ~PowerControlBehavior() {}
};


#endif //RECIPEENGINE_POWERCONTROLBEHAVIOR_H
