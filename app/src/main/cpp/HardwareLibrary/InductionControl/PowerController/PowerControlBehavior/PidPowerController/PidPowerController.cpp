//
// Created by fedal on 24/7/21.
//

#include "PidPowerController.h"

void PidPowerController::ControlPower(InductionSensor induction_sensor) {
    PidPowerController::induction_control_pid_.SetCurrentValue(
            induction_sensor.GetInputCurrent() * induction_sensor.GetInputVoltage());
    double pid_output = PidPowerController::induction_control_pid_.PidLoop();

    if (pid_output > 99) {
        pid_output = 99;
    } else if (pid_output < 0) {
        pid_output = 0;
    }
    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "output %f", pid_output);
    ComponentRequestBuilder component_request_builder;
    component_request_builder.component_request_.SetRequestType(
            HARDWARE_REQUEST).SetComponent(
            INDUCTION_ACTUATOR).SetSpeed((int) pid_output);
    HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(
            component_request_builder.component_request_);

    try {
        PidPowerController::induction_ping_control_.PingControl(induction_sensor.GetInputCurrent(),
                                                                pid_output);
    } catch (Error &error) {
        if (error.GetError() == PAN_NOT_DETECTED) {
            this->pan_detected_ = false;
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "error caught pan not detected");
        } else {
            throw error;
        }
    }
}

bool PidPowerController::IsPanDetected() {
    return this->pan_detected_;
}

InductionPowerLevels_e PidPowerController::GetPowerLevel() {
    return this->power_level_;
}
