//
// Created by fedal on 26/7/21.
//

#ifndef RECIPEENGINE_POWERLEVELSIX_H
#define RECIPEENGINE_POWERLEVELSIX_H


class PowerLevelSix : public PidPowerController {
private:
    const int POWER_LEVEL = 1200;
public:
    PowerLevelSix() : PidPowerController(Power_Level_Six) {
        PidPowerController::induction_control_pid_.SetTargetValue(POWER_LEVEL);
    }

    ~PowerLevelSix() {}
};

#endif //RECIPEENGINE_POWERLEVELSIX_H
