//
// Created by fedal on 26/7/21.
//

#ifndef RECIPEENGINE_POWERLEVELSEVEN_H
#define RECIPEENGINE_POWERLEVELSEVEN_H


class PowerLevelSeven : public PidPowerController {
private:
    const int POWER_LEVEL = 1400;
public:
    PowerLevelSeven() : PidPowerController(Power_Level_Seven) {
        PidPowerController::induction_control_pid_.SetTargetValue(POWER_LEVEL);
    }

    ~PowerLevelSeven() {}
};


#endif //RECIPEENGINE_POWERLEVELSEVEN_H
