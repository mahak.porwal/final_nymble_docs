//
// Created by fedal on 26/7/21.
//

#ifndef RECIPEENGINE_POWERLEVELFOUR_H
#define RECIPEENGINE_POWERLEVELFOUR_H


class PowerLevelFour : public PidPowerController {
private:
    const int POWER_LEVEL = 800;
public:
    PowerLevelFour() : PidPowerController(Power_Level_Four) {
        PidPowerController::induction_control_pid_.SetTargetValue(POWER_LEVEL);
    }

    ~PowerLevelFour() {}
};


#endif //RECIPEENGINE_POWERLEVELFOUR_H
