//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_PIDPOWERCONTROLLER_H
#define RECIPEENGINE_PIDPOWERCONTROLLER_H

#include "../PowerControlBehavior.h"
#include "../../../../Subsystem/Induction/Utilities/InductionControlPid/InductionControlPid.h"
#include "../../../../Subsystem/Induction/Utilities/InductionPingControl/InductionPingControl.h"
#include "../../../../Error/Error.h"

class PidPowerController : public PowerControlBehavior {

protected:
    static InductionControlPid induction_control_pid_;

    static InductionPingControl induction_ping_control_;
private:
    bool pan_detected_ = true;
    InductionPowerLevels_e power_level_;
public:

    PidPowerController(InductionPowerLevels_e power_level) {
        this->power_level_ = power_level;
    }

    void ControlPower(InductionSensor);

    bool IsPanDetected();

    InductionPowerLevels_e GetPowerLevel();

    ~PidPowerController() {}
};

InductionControlPid PidPowerController::induction_control_pid_{0.009, 0.006, 0};
InductionPingControl PidPowerController::induction_ping_control_{2, 50, 4};

#endif //RECIPEENGINE_PIDPOWERCONTROLLER_H
