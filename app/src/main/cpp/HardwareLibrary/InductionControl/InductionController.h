//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_INDUCTIONCONTROLLER_H
#define RECIPEENGINE_INDUCTIONCONTROLLER_H


#include "InductionControlEnums.h"
#include "../HardwareServiceFactory/HardwareServiceFactory.h"

class InductionController {
protected:
    bool is_induction_controlled_ = false;
public:
    virtual void StartControl() = 0;

    virtual void
    SetInput(induction_control_input::InductionControlInput_e) = 0;

    virtual induction_state_machine::InductionControlState_e GetInductionControlState() = 0;

    virtual InductionError_e GetError() = 0;

    void TurnOnInductionFan() {
        ComponentRequestBuilder component_request_builder;
        component_request_builder.component_request_.SetComponent(
                INDUCTION_FAN_GPIO).SetState(ON_STATE);

        HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_);
    }

    void TurnOffInductionFan() {
        ComponentRequestBuilder component_request_builder;
        component_request_builder.component_request_.SetComponent(
                INDUCTION_FAN_GPIO).SetState(OFF_STATE);

        HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_);
    }

    void StopControl(){
        __android_log_print(ANDROID_LOG_ERROR, "native", "\n Ending Induction control");
        if(is_induction_controlled_){
            __android_log_print(ANDROID_LOG_ERROR, "native", "\n Ending Induction control set");
            is_induction_controlled_ = false;
        }
    }

    void TurnOffInductionIntPin(){
        ComponentRequestBuilder component_request_builder;
        component_request_builder.component_request_.SetComponent(
                INDUCTION_INT_GPIO).SetState(OFF_STATE);

        HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_);
    }

    virtual ~InductionController() {}
};


#endif //RECIPEENGINE_INDUCTIONCONTROLLER_H
