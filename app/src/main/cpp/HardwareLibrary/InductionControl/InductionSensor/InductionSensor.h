//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_INDUCTIONSENSOR_H
#define RECIPEENGINE_INDUCTIONSENSOR_H


class InductionSensor {
private :
    double igbt_temperature_;
    double pan_temperature_;
    double input_current_;
    double input_voltage_;

    friend class InductionSensorBuilder;

public:
    double GetIgbtTemperature() {
        return this->igbt_temperature_;
    }

    double GetPanTemperature() {
        return this->pan_temperature_;
    }

    double GetInputCurrent() {
        return
                this->input_current_;
    }

    double GetInputVoltage() {
        return this->input_voltage_;
    }
};


#endif //RECIPEENGINE_INDUCTIONSENSOR_H
