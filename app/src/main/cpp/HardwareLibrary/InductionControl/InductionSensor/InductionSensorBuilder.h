//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_INDUCTIONSENSORBUILDER_H
#define RECIPEENGINE_INDUCTIONSENSORBUILDER_H

#include "InductionSensor.h"

class InductionSensorBuilder {

private:
    InductionSensor induction_sensor_;

public:

    InductionSensorBuilder &SetIgbtTemperature(double igbt_temperature) {
        induction_sensor_.igbt_temperature_ = igbt_temperature;
        return *this;
    }

    InductionSensorBuilder &SetPanTemperature(double pan_temperature) {
        induction_sensor_.pan_temperature_ = pan_temperature;
        return *this;
    }

    InductionSensorBuilder &SetInputCurrent(double input_current) {
        induction_sensor_.input_current_ = input_current;
        return *this;
    }

    InductionSensorBuilder &SetInputVoltage(double input_voltage) {
        induction_sensor_.input_voltage_ = input_voltage;
        return *this;
    }

    InductionSensor Build() {
        return this->induction_sensor_;
    }
};


#endif //RECIPEENGINE_INDUCTIONSENSORBUILDER_H
