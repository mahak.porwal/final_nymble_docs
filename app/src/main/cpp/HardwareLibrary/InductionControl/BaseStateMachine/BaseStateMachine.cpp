//
// Created by fedal on 24/7/21.
//

#include "BaseStateMachine.h"

InductionControlState_e
BaseStateMachine::StateTransition(InductionControlState_e control_state,
                                  InductionControlEvent_e control_event) {
    InductionControlState_e new_control_state = Null_State;
    if (control_event != Null_Event) {
        //new control event
        //consume the event and then set it to Null_Event
        //while consuming the control event no one will be writing to the current_control_event
        InductionStateTransitionMap state_transition_map = state_transition_map_.at(
                control_state);
        if (state_transition_map.find(control_event) !=
            state_transition_map.end()) {
            new_control_state = state_transition_map.at(control_event);
        }
    }
    return new_control_state;
}

void BaseStateMachine::SetControlEvent(InductionControlEvent_e control_event) {
    this->current_control_event_ = control_event;
}

void BaseStateMachine::Execute(InductionSensor induction_sensor) {

    switch (this->current_control_state_) {
        case Power_Control_State: {
            this->power_controller_->ControlPower(induction_sensor);
            if (!this->power_controller_->IsPanDetected()) {
                this->error_detector_->SetNoPanError();
                this->current_control_event_ = Error_Detected;
                this->power_controller_->ShutdownInduction();
            } else {
                InductionError_e error = this->error_detector_->DetectAndGetError(induction_sensor);
                if (error != No_Error) {
                    this->current_control_event_ = Error_Detected;
                    this->power_controller_->ShutdownInduction();
                }
            }
        }
            break;
        case Error_State: {
            switch (this->error_resolver_->ResolveError(
                    this->error_detector_->DetectAndGetError(induction_sensor))) {
                case Error_Resolved:
                    this->current_control_event_ = Error_Resolved_Event;
                    this->error_detector_->ResetError();
                    break;
                case Error_Not_Resolved:

                    break;
                case Error_Hard_Fault:
                    this->current_control_event_ = Unable_To_Resolve_Error;
                    break;
                case Error_Wait_For_External_Correction:
                    this->current_control_event_ = External_Help_Needed;
                    break;
            }
        }
            break;
        case Idle_State: {
            InductionError_e error = this->error_detector_->DetectAndGetError(induction_sensor);
            if (error != No_Error) {
                this->current_control_event_ = Error_Detected;
                this->power_controller_->ShutdownInduction();
            }
        }
            break;
        case Waiting_For_Error_Correction_State:
            break;
        case Pan_Detection_State:
            break;
        case Hard_Fault_State:
            break;
        case Manual_Mode_State: {
            InductionError_e error = this->error_detector_->DetectAndGetError(induction_sensor);
            if (error != No_Error) {
                this->current_control_event_ = Error_Detected;
                this->power_controller_->ShutdownInduction();
            }
        }
            break;
        default:
            break;
    }

    InductionControlState_e temp_state = this->StateTransition(this->current_control_state_,
                                                               this->current_control_event_);
    if (temp_state != Null_State) {
        __android_log_print(ANDROID_LOG_ERROR, "power set ", " state transition");
        this->current_control_state_ = temp_state;
        this->SetPowerLevel(this->current_control_event_);
        if (this->current_control_state_ == Idle_State ||
            this->current_control_state_ == Manual_Mode_State) {
            this->power_controller_->ShutdownInduction();
            this->error_detector_->ResetError();
            this->error_resolver_->ResetError();
        }
        this->current_control_event_ = Null_Event;
    }
}

InductionControlState_e BaseStateMachine::GetInductionControlState() {
    return this->current_control_state_;
}

InductionError_e BaseStateMachine::GetError() {
    return this->error_resolver_->GetError();
}

void BaseStateMachine::SetPowerLevel(InductionControlEvent_e induction_control_event) {
    switch (induction_control_event) {
        case induction_state_machine::Power_Level_One :
            this->power_controller_->SetPowerLevel(induction_power_level::Power_Level_One);
            break;
        case induction_state_machine::Power_Level_Two :
            this->power_controller_->SetPowerLevel(induction_power_level::Power_Level_Two);
            break;
        case induction_state_machine::Power_Level_Three :
            this->power_controller_->SetPowerLevel(induction_power_level::Power_Level_Three);
            break;
        case induction_state_machine::Power_Level_Four :
            this->power_controller_->SetPowerLevel(induction_power_level::Power_Level_Four);
            break;
        case induction_state_machine::Power_Level_Five :
            this->power_controller_->SetPowerLevel(induction_power_level::Power_Level_Five);
            break;
        case induction_state_machine::Power_Level_Six :
            this->power_controller_->SetPowerLevel(induction_power_level::Power_Level_Six);
            break;
        case induction_state_machine::Power_Level_Seven :
            this->power_controller_->SetPowerLevel(induction_power_level::Power_Level_Seven);
            break;
    }
}

