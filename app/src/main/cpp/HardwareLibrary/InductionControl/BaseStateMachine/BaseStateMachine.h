//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_BASESTATEMACHINE_H
#define RECIPEENGINE_BASESTATEMACHINE_H

#include "../InductionStateMachine.h"
#include "../PowerController/PowerController.h"
#include "../ErrorDetector/ErrorDetector.h"
#include "../ErrorResolver/ErrorResolver.h"

using namespace induction_state_machine;

class BaseStateMachine : public InductionStateMachine {
private :
    typedef std::unordered_map<InductionControlEvent_e, InductionControlState_e>
            InductionStateTransitionMap;
    typedef std::unordered_map<InductionControlState_e,
            InductionStateTransitionMap> InductionTransitionMap;

    InductionControlState_e current_control_state_ = Idle_State;
    InductionControlEvent_e current_control_event_ = Null_Event;
    PowerController *power_controller_;
    ErrorResolver *error_resolver_;
    ErrorDetector *error_detector_;

    static InductionStateTransitionMap power_control_state_map_;
    static InductionStateTransitionMap error_state_map_;
    static InductionStateTransitionMap idle_state_map_;
    static InductionStateTransitionMap waiting_for_error_correction_state_map_;
    static InductionStateTransitionMap pan_detection_state_map_;
    static InductionStateTransitionMap hard_fault_state_map_;
    static InductionStateTransitionMap manual_mode_state_map_;


    static InductionTransitionMap state_transition_map_;

    InductionControlState_e StateTransition(InductionControlState_e, InductionControlEvent_e);

    void SetPowerLevel(InductionControlEvent_e);

public:

    BaseStateMachine(PowerController *power_controller, ErrorResolver *error_resolver,
                     ErrorDetector *error_detector) {
        this->power_controller_ = power_controller;
        this->error_resolver_ = error_resolver;
        this->error_detector_ = error_detector;
    }

    void Execute(InductionSensor);

    void SetControlEvent(InductionControlEvent_e);

    InductionControlState_e GetInductionControlState();

    InductionError_e GetError();

    ~BaseStateMachine() {
        delete this->power_controller_;
        delete this->error_resolver_;
        delete this->error_detector_;
    }
};

BaseStateMachine::InductionStateTransitionMap BaseStateMachine::power_control_state_map_ = {
        {induction_state_machine::Power_Level_One,   Power_Control_State},
        {induction_state_machine::Power_Level_Two,   Power_Control_State},
        {induction_state_machine::Power_Level_Three, Power_Control_State},
        {induction_state_machine::Power_Level_Four,  Power_Control_State},
        {induction_state_machine::Power_Level_Five,  Power_Control_State},
        {induction_state_machine::Power_Level_Six,   Power_Control_State},
        {induction_state_machine::Power_Level_Seven, Power_Control_State},
        {Error_Detected,         Error_State},
        {Shutdown_Control_Event, Idle_State}
};

BaseStateMachine::InductionStateTransitionMap BaseStateMachine::error_state_map_ = {
        {Unable_To_Resolve_Error, Hard_Fault_State},
        {Error_Resolved_Event,    Idle_State},
        {External_Help_Needed,    Waiting_For_Error_Correction_State}
};

BaseStateMachine::InductionStateTransitionMap BaseStateMachine::idle_state_map_ = {
        {Error_Detected,                             Error_State},
        {Detect_Pan,                                 Pan_Detection_State},
        {induction_state_machine::Power_Level_One,   Power_Control_State},
        {induction_state_machine::Power_Level_Two,   Power_Control_State},
        {induction_state_machine::Power_Level_Three, Power_Control_State},
        {induction_state_machine::Power_Level_Four,  Power_Control_State},
        {induction_state_machine::Power_Level_Five,  Power_Control_State},
        {induction_state_machine::Power_Level_Six,   Power_Control_State},
        {induction_state_machine::Power_Level_Seven, Power_Control_State},
        {Manual_Mode_Requested,                      Manual_Mode_State}
};

BaseStateMachine::InductionStateTransitionMap
        BaseStateMachine::waiting_for_error_correction_state_map_ = {
        {Shutdown_Control_Event, Idle_State}
};

BaseStateMachine::InductionStateTransitionMap
        BaseStateMachine::pan_detection_state_map_ = {
        {Shutdown_Control_Event, Idle_State}
};

BaseStateMachine::InductionStateTransitionMap
        BaseStateMachine::hard_fault_state_map_ = {
        {Shutdown_Control_Event, Idle_State}
};

BaseStateMachine::InductionStateTransitionMap
        BaseStateMachine::manual_mode_state_map_ = {
        {Shutdown_Control_Event, Idle_State}
};

BaseStateMachine::InductionTransitionMap BaseStateMachine::state_transition_map_ = {
        {Power_Control_State,                BaseStateMachine::power_control_state_map_},
        {Error_State,                        BaseStateMachine::error_state_map_},
        {Idle_State,                         BaseStateMachine::idle_state_map_},
        {Waiting_For_Error_Correction_State, BaseStateMachine::waiting_for_error_correction_state_map_},
        {Pan_Detection_State,                BaseStateMachine::pan_detection_state_map_},
        {Hard_Fault_State,                   BaseStateMachine::hard_fault_state_map_},
        {Manual_Mode_State,                  BaseStateMachine::manual_mode_state_map_}
};

#endif //RECIPEENGINE_BASESTATEMACHINE_H
