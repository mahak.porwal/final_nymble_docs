//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_BASEERRORRESOLVER_H
#define RECIPEENGINE_BASEERRORRESOLVER_H

#include "../ErrorResolver.h"

class BaseErrorResolver : public ErrorResolver {
private:
    InductionError_e induction_error_ = No_Error;
public:
    InductionErrorResolveStatus_e ResolveError(InductionError_e);

    InductionError_e GetError();

    virtual void ResetError() {
        this->induction_error_ = No_Error;
    }

    ~BaseErrorResolver() {}
};


#endif //RECIPEENGINE_BASEERRORRESOLVER_H
