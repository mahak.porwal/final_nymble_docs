//
// Created by fedal on 24/7/21.
//

#include "BaseErrorResolver.h"


InductionErrorResolveStatus_e BaseErrorResolver::ResolveError(InductionError_e induction_error) {
    this->induction_error_ = induction_error;
    if(induction_error == No_Pan_Error){
        return Error_Wait_For_External_Correction;
    }
    return Error_Resolved;
}

InductionError_e BaseErrorResolver::GetError() {
    return this->induction_error_;
}