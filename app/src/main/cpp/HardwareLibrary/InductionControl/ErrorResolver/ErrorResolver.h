//
// Created by fedal on 24/7/21.
//

#ifndef RECIPEENGINE_ERRORRESOLVER_H
#define RECIPEENGINE_ERRORRESOLVER_H

#include "../InductionControlEnums.h"

class ErrorResolver {
public:
    virtual InductionErrorResolveStatus_e ResolveError(InductionError_e) = 0;

    virtual InductionError_e GetError() = 0;

    virtual void ResetError() = 0;

    virtual ~ErrorResolver() {}
};


#endif //RECIPEENGINE_ERRORRESOLVER_H
