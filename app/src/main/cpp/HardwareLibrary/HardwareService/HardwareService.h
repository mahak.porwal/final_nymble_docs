//
// Created by fedal on 8/12/20.
//

#ifndef JULIA_ANDROID_HARDWARESERVICE_H
#define JULIA_ANDROID_HARDWARESERVICE_H

class InductionController;

class ThermalCamera;

#include "../Subsystem/Subsystem.h"
#include "Peripherals/Pwm/PwmRequestProcessor/PwmRequestProcessor.h"
#include "Peripherals/Gpio/GpioRequestProcessor/GpioRequestProcessor.h"
#include "Peripherals/Adc/AdcRequestProcessor/AdcRequestProcessor.h"
#include "../Subsystem/SubsystemEnums.h"
#include "Peripherals/I2C/I2C.h"
#include "../Component/Request/ComponentRequest.h"
#include "../Component/Component.h"
#include "../BaseHardwareService/BaseHardwareService.h"
#include "SequenceExecutor/SequenceExecutor.h"
#include "SequenceExecutor/SequenceExecutorFactory.h"
//#include "../InductionControl/InductionController.h"

class HardwareService : public BaseHardwareService {
private:
    void StartHardwareService();

    void StopHardwareService();

    void PopulateComponentMap();

    std::unordered_map<Component_e, Component &> *component_map_;

    SequenceExecutor sequence_executor_;

    ThermalCamera *thermal_camera_;

    static std::unordered_map<std::string, std::string> hardware_factors;

public:

    //public static members of Hardware service
    static PwmRequestProcessor *pwm_request_processor_;
    static GpioRequestProcessor *gpio_request_processor_;
    static AdcRequestProcessor *adc_request_processor_;
    static InductionController *induction_controller_;
    static I2C *i2c_;

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessComponentRequest(const ComponentRequest &);

    void StartInductionControl();

    void StopInductionControl();

    void InitThermalCamera();

    static void PopulateHardwareFactors(JNIEnv *env, jobject object);

    const Component &GetComponent(const ComponentRequest &component_request);

    HardwareService() {
        this->StartHardwareService();
        this->component_map_ = new std::unordered_map<Component_e, Component &>;
        this->PopulateComponentMap();
    }

    ~HardwareService() {
        this->StopHardwareService();
    }
};

PwmRequestProcessor *HardwareService::pwm_request_processor_;
GpioRequestProcessor *HardwareService::gpio_request_processor_;
AdcRequestProcessor *HardwareService::adc_request_processor_;
InductionController *HardwareService::induction_controller_;
I2C *HardwareService::i2c_;
std::unordered_map<std::string, std::string> HardwareService::hardware_factors;
#endif //JULIA_ANDROID_HARDWARESERVICE_H
