//
// Created by fedal on 21/7/21.
//

#ifndef RECIPEENGINE_SEQUENCEEXECUTOR_H
#define RECIPEENGINE_SEQUENCEEXECUTOR_H

#include <mutex>

class SequenceExecutor {
private:
    /**
     * Map keeps the record of the components whose sequence behavior is in execution
     */
    std::unordered_map<Component_e, SequenceBehavior &> sequences_in_execution_map_;

    /**
     * Lock for map
     */
    std::mutex map_lock;

    /**
     * Thread-safe addition of an element to the sequences_in_execution_map_
     */
    void AddSequenceToSharedMap(SequenceBehavior &);

    /**
     * Retreives the reference of the ComponentRequest for the given component
     * @return Reference of the ComponentRequest
     */
    ComponentRequest *GetComponentRequest(Component_e);

    /**
     * Thread-safe removal of the given component's sequence behavior.
     * The presence of this component key in the sequences_in_execution_map_ must be
     * ensured by the caller of this method
     */
    void RemoveASequence(Component_e);

public:

    /**
     * Accepts a sequence behavior and sets the behavior in execution. The method keeps on calling
     * the ExecuteSequence method of class SequenceBehavior until it returns true
     */
    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    SetSequenceInExecution(SequenceBehavior &);

    /**
     * Accepts a component whose sequence behavior needs to be aborted, if such a sequence behavior is
     * in execution, then it will be aborted and the method will return true.
     * If no such behavior was in execution, then the method will return false.
     * @return boolean representing whether the abort was successful or otherwise
     */
    bool AbortSequence(Component_e);
};


#endif //RECIPEENGINE_SEQUENCEEXECUTOR_H
