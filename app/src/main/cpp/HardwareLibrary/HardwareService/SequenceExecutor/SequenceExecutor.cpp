//
// Created by fedal on 21/7/21.
//

#include "SequenceExecutor.h"

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
SequenceExecutor::SetSequenceInExecution(SequenceBehavior &sequence_behavior) {

    this->AddSequenceToSharedMap(sequence_behavior);
    bool sequence_completed = false;
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> result;

    do {
        sequence_completed = sequence_behavior.ExecuteSequence(&result);
    } while (!sequence_completed && !sequence_behavior.GetComponentRequest()->IsAbortSequence());
    if (sequence_behavior.GetComponentRequest()->IsAbortSequence()) {
        sequence_behavior.ResetBehavior();
    }
    this->RemoveASequence(sequence_behavior.GetComponentRequest()->GetComponent());
    return result;
}

void SequenceExecutor::AddSequenceToSharedMap(SequenceBehavior &sequence_behavior) {
    std::lock_guard(this->map_lock);
    this->sequences_in_execution_map_.emplace(
            sequence_behavior.GetComponentRequest()->GetComponent(), sequence_behavior);
}

bool SequenceExecutor::AbortSequence(Component_e component) {
    ComponentRequest *component_request = this->GetComponentRequest(component);
    if (component_request != NULL) {
        component_request->SetAbortSequence(true);
        return true;
    }
    return false;
}

ComponentRequest *SequenceExecutor::GetComponentRequest(Component_e component) {
    ComponentRequest *component_request = NULL;
    std::lock_guard(this->map_lock);
    if (this->sequences_in_execution_map_.find(component) !=
        this->sequences_in_execution_map_.end()) {
        //component sequence found
        SequenceBehavior &sequence_behavior = this->sequences_in_execution_map_.at(component);
        component_request = sequence_behavior.GetComponentRequest();
    }
    return component_request;
}

void SequenceExecutor::RemoveASequence(Component_e component) {
    std::lock_guard(this->map_lock);
    SequenceBehavior &sequence_behavior = this->sequences_in_execution_map_.at(component);
    delete &sequence_behavior;
    this->sequences_in_execution_map_.erase(component);
}