//
// Created by fedal on 21/7/21.
//

#ifndef RECIPEENGINE_SEQUENCEEXECUTORFACTORY_H
#define RECIPEENGINE_SEQUENCEEXECUTORFACTORY_H

#include "../../Subsystem/SequenceBehavior/MacroSequenceBehaviors/MacroLiquidDispenseBehavior/MacroLiquidDispenseBehavior.h"
#include "../../Subsystem/SequenceBehavior/MacroSequenceBehaviors/MacroContainerBehavior/MacroContainerBehavior.h"
#include "../../Subsystem/SequenceBehavior/MacroSequenceBehaviors/MacroNonLiquidDispenseBehavior/MacroNonLiquidDispenseBehavior.h"
#include "../../Subsystem/SequenceBehavior/MicroSequenceBehaviors/MicroClockwiseDispenseBehavior/MicroClockwiseDispenseBehavior.h"
#include "../../Subsystem/SequenceBehavior/MicroSequenceBehaviors/MicroAntiClockwiseDispenseBehavior/MicroAntiClockwiseDispenseBehavior.h"
#include "../../Subsystem/SequenceBehavior/MicroSequenceBehaviors/MicroPodBehavior/MicroPodBehavior.h"
#include "../../Subsystem/SequenceBehavior/LiquidDispenseBehaviors/OilDispenseBehavior/LiquidOilDispenseBehavior.h"
#include "../../Subsystem/SequenceBehavior/LiquidDispenseBehaviors/WaterDispenseBehavior/LiquidWaterDispenseBehavior.h"

class SequenceExecutorFactory {

public:

    static SequenceBehavior *GetSequenceBehavior(ComponentRequest);

};

SequenceBehavior *SequenceExecutorFactory::GetSequenceBehavior(ComponentRequest component_request) {
    SequenceBehavior *sequence_behavior_reference = NULL;
    switch (component_request.GetSequence()) {
        case MACRO_LIQUID_DISPENSE:
            sequence_behavior_reference = new MacroLiquidDispenseBehavior(component_request);
            break;
        case MACRO_REACH_POSITION_AT_SPEED:
            sequence_behavior_reference = new MacroContainerBehavior(component_request);
            break;
        case MACRO_NON_LIQUID_DISPENSE:
            sequence_behavior_reference = new MacroNonLiquidDispenseBehavior(component_request);
            break;
        case MICRO_CLOCKWISE_DISPENSE:
            sequence_behavior_reference = new MicroClockwiseDispenseBehavior(component_request);
            break;
        case MICRO_ANTICLOCKWISE_DISPENSE:
            sequence_behavior_reference = new MicroAntiClockwiseDispenseBehavior(component_request);
            break;
        case LIQUID_OIL_DISPENSE:
            sequence_behavior_reference = new LiquidOilDispenseBehavior(component_request);
            break;
        case LIQUID_WATER_DISPENSE:
            sequence_behavior_reference = new LiquidWaterDispenseBehavior(component_request);
            break;
        case MICRO_REACH_ANGLE_WITH_SPEED:
            sequence_behavior_reference = new MicroPodBehavior(component_request);
            break;
    }
    return sequence_behavior_reference;
}


#endif //RECIPEENGINE_SEQUENCEEXECUTORFACTORY_H
