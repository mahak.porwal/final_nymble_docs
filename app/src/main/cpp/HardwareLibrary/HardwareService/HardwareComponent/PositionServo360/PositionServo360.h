//
// Created by fedal on 19/11/21.
//

#ifndef RECIPEENGINE_POSITIONSERVO360_H
#define RECIPEENGINE_POSITIONSERVO360_H

#include "../../../Component/SubsystemComponent/SubsystemComponent.h"
class PositionServo360 : public SubsystemComponent {
private:
    int ExecuteServoSequence(uint16_t, float);

    int ExecuteServoRotation(float, ActuatorDirections_e);

    float GetSensorReading();
public:
    PositionServo360(Component_e hardware_component, const Subsystem &subsystem) :
    SubsystemComponent(hardware_component, subsystem) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //RECIPEENGINE_POSITIONSERVO360_H
