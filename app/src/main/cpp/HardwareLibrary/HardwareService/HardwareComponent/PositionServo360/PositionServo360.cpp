//
// Created by fedal on 19/11/21.
//

#include "PositionServo360.h"

std::unordered_map <std::string>

<ResponseParameters_e, vector>
PositionServo360::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map <ResponseParameters_e, std::vector<std::string>> response_map;
    switch (component_request.GetRequestType()) {
        case HARDWARE_REQUEST:
            if (component_request.GetFrequency() != -1) {
                //GetSensorReading
                float reading = this->GetSensorReading();
                std::vector <string> values;
                values.push_back(std::to_string(reading));
                response_map.emplace(SENSOR_VALUE, values);
                //return this->GetSensorReading();
            } else if (component_request.GetSpeed() != -1 &&
                       component_request.GetPosition() != -1) {
                //ExecuteServoSequence
                int result = this->ExecuteServoSequence(component_request.GetPosition(),
                                                        component_request.GetSpeed());
                std::vector <string> values;
                values.push_back(std::to_string(result));
                response_map.emplace(SEQUENCE_RESULT, values);
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                    "Servo Sequence");
            } else if (component_request.GetSpeed() != -1 &&
                       component_request.GetDirection() != DIRECTION_DEFAULT) {
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                    "Servo Rotation");
                //ExecuteServoRotation
                int result = this->ExecuteServoRotation(component_request.GetSpeed(),
                                                        component_request.GetDirection());
                std::vector <string> values;
                values.push_back(std::to_string(result));
                response_map.emplace(SEQUENCE_RESULT, values);
            }
            break;
        case SEQUENCE_REQUEST:
            break;
    }
    return response_map;
}

int PositionServo360::ExecuteServoSequence(uint16_t, float) {
    return 0;
}

int PositionServo360::ExecuteServoRotation(float, ActuatorDirections_e) {
    return 0;
}

float PositionServo360::GetSensorReading() {
    return 0;
}
