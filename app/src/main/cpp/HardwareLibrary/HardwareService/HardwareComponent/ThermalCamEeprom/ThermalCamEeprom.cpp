//
// Created by fedal on 23/8/21.
//

#include "ThermalCamEeprom.h"

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
ThermalCamEeprom::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;

    //need to figure out whether the request is for write byte, read byte or sequential read byte.
    if (component_request.GetEepromNumOfSeqReads() != -1 &&
        component_request.GetEepromAddr() != -1) {
        //sequential reads
        vector<uint8_t> reads = this->thermalCamEeprom->SequentialRead(
                component_request.GetEepromAddr(),
                component_request.GetEepromNumOfSeqReads());
        for (auto read_byte : reads) {
            values.push_back(std::to_string(read_byte));
        }
        response_map.emplace(SENSOR_VALUE, values);
    } else if (component_request.GetEepromData() != -1 &&
               component_request.GetEepromAddr() != -1) {
        //write byte to address request
        this->thermalCamEeprom->WriteByte(
                component_request.GetEepromAddr(),
                component_request.GetEepromData());
        values.push_back(std::to_string(1));
    } else if (component_request.GetEepromAddr() != -1) {
        //read byte from address request
        uint8_t read_byte = this->thermalCamEeprom->ReadByte(
                component_request.GetEepromAddr());
        values.push_back(std::to_string(read_byte));
        response_map.emplace(SENSOR_VALUE, values);
    }
    return response_map;
}