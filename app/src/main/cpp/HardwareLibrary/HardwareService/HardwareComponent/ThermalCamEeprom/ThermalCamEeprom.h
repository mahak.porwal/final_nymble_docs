//
// Created by fedal on 23/8/21.
//

#ifndef RECIPEENGINE_THERMALCAMEEPROM_H
#define RECIPEENGINE_THERMALCAMEEPROM_H

#include "../../Peripherals/Eeprom/Eeprom.h"
#include "../../Peripherals/Eeprom/Devices/24AA64/MicroChip24AA64.cpp"

class ThermalCamEeprom : public StandaloneComponent {
private:
    Eeprom* thermalCamEeprom;
public:
    ThermalCamEeprom(Component_e component) : StandaloneComponent(component) {
        this->thermalCamEeprom = new MicroChip24AA64(0x50);
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);

    ~ThermalCamEeprom() {
        delete thermalCamEeprom;
    }
};


#endif //RECIPEENGINE_THERMALCAMEEPROM_H
