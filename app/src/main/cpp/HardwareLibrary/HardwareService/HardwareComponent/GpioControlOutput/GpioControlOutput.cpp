//
// Created by fedal on 21/12/20.
//

#include "GpioControlOutput.h"

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
GpioControlOutput::ProcessRequest(const ComponentRequest &component_request) {
    double result = -1;
    OnOffState_e state = component_request.GetState();


    if (state == ON_STATE || state == OFF_STATE) {
        GpioRequestBuilder *gpioRequestBuilder = new GpioRequestBuilder;
        gpioRequestBuilder->gpio_request_->SetHardwareComponent(
                component_request.GetComponent())->SetAction(ACTION_WRITE_PIN)->SetState(
                state == ON_STATE ? STATE_HIGH : STATE_LOW);
        result = HardwareService::gpio_request_processor_->ProcessGpioRequest(
                *gpioRequestBuilder->gpio_request_);
        delete gpioRequestBuilder;
    } else if (state == ON_TO_OFF_STATE || state == OFF_TO_ON_STATE) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "inside Gpio control ");
        result = this->EmitEdgeOnGpio(component_request);
    } else {
        throw new Error;
    }
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;
    values.push_back(std::to_string(result));
    response_map.emplace(COMMAND_RESULT, values);
    return response_map;
}

int GpioControlOutput::EmitEdgeOnGpio(const ComponentRequest &component_request) {
    volatile bool is_emission_complete = false;
    while (!is_emission_complete) {
        switch (this->emit_edge_state_) {
            case ACTUATE_LEVEL_ONE: {
                GpioRequestBuilder *gpioRequestBuilder = new GpioRequestBuilder;
                gpioRequestBuilder->gpio_request_->SetHardwareComponent(
                        component_request.GetComponent())->SetAction(ACTION_WRITE_PIN)->SetState(
                        component_request.GetState() == ON_TO_OFF_STATE ? STATE_HIGH : STATE_LOW);
                HardwareService::gpio_request_processor_->ProcessGpioRequest(
                        *gpioRequestBuilder->gpio_request_);
                delete gpioRequestBuilder;
                this->timer_->SetBeginTime();
                this->emit_edge_state_ = WAIT_AFTER_LEVEL_ONE;
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "before ping level one");
            }
                break;
            case WAIT_AFTER_LEVEL_ONE: {
                if (this->timer_->GetTimeElapsed() > 20) {
                    this->emit_edge_state_ = ACTUATE_LEVEL_TWO;
                    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "after ping level one");
                }
            }
                break;
            case ACTUATE_LEVEL_TWO: {

                GpioRequestBuilder *gpioRequestBuilder = new GpioRequestBuilder;
                gpioRequestBuilder->gpio_request_->SetHardwareComponent(
                        component_request.GetComponent())->SetAction(ACTION_WRITE_PIN)->SetState(
                        component_request.GetState() == ON_TO_OFF_STATE ? STATE_LOW : STATE_HIGH);
                HardwareService::gpio_request_processor_->ProcessGpioRequest(
                        *gpioRequestBuilder->gpio_request_);
                delete gpioRequestBuilder;
                this->timer_->SetBeginTime();
                this->emit_edge_state_ = WAIT_AFTER_LEVEL_TWO;
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "before ping level two");
            }
                break;
            case WAIT_AFTER_LEVEL_TWO: {
                if (this->timer_->GetTimeElapsed() > 20) {
                    this->emit_edge_state_ = ACTUATE_LEVEL_ONE;
                    is_emission_complete = true;
                    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "after ping level two");
                }
            }
                break;
        }
    }
    return 0;
}
