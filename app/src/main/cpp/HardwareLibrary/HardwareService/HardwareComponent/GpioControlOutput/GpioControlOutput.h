//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_GPIOCONTROLOUTPUT_H
#define JULIA_ANDROID_GPIOCONTROLOUTPUT_H


typedef enum {
    ACTUATE_LEVEL_ONE,
    WAIT_AFTER_LEVEL_ONE,
    ACTUATE_LEVEL_TWO,
    WAIT_AFTER_LEVEL_TWO
} EmitEdgeStates_e;

class GpioControlOutput : public SubsystemComponent {
private:
    EmitEdgeStates_e emit_edge_state_;

    int EmitEdgeOnGpio(const ComponentRequest &);

    Timer *timer_;

public:
    GpioControlOutput(Component_e component, Subsystem subsystem) :
            SubsystemComponent(component, subsystem) {
        this->emit_edge_state_ = ACTUATE_LEVEL_ONE;
        this->timer_ = new Timer;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);

    ~GpioControlOutput() {
        delete this->timer_;
    }
};


#endif //JULIA_ANDROID_GPIOCONTROLOUTPUT_H
