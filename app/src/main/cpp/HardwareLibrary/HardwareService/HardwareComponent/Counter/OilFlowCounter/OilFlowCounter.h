//
// Created by fedal on 11/10/21.
//

#ifndef RECIPEENGINE_OILFLOWCOUNTER_H
#define RECIPEENGINE_OILFLOWCOUNTER_H


#include "../../../../Component/StandaloneComponent/StandaloneComponent.h"

class OilFlowCounter : public StandaloneComponent{
private:
    Counter *counter_;

public:
    OilFlowCounter(Component_e component, Counter *counter) :
    StandaloneComponent(component) {
            this->counter_ = counter;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //RECIPEENGINE_OILFLOWCOUNTER_H
