//
// Created by fedal on 11/10/21.
//

#ifndef RECIPEENGINE_WATERFLOWCOUNTER_H
#define RECIPEENGINE_WATERFLOWCOUNTER_H


#include "../../../../Component/StandaloneComponent/StandaloneComponent.h"

class WaterFlowCounter : public StandaloneComponent {
private:
    Counter *counter_;

public:
    WaterFlowCounter(Component_e component, Counter *counter) :
            StandaloneComponent(component) {
        this->counter_ = counter;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //RECIPEENGINE_WATERFLOWCOUNTER_H
