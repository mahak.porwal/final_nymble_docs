//
// Created by fedal on 11/10/21.
//

#ifndef RECIPEENGINE_EXHAUSTFANCOUNTER_H
#define RECIPEENGINE_EXHAUSTFANCOUNTER_H


#include "../../../../Component/StandaloneComponent/StandaloneComponent.h"

class ExhaustFanCounter : public StandaloneComponent {
private:
    Counter* counter_;

public:
    ExhaustFanCounter(Component_e component, Counter *counter) :
            StandaloneComponent(component) {
        this->counter_ = counter;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //RECIPEENGINE_EXHAUSTFANCOUNTER_H
