//
// Created by fedal on 05/10/21.
//

#include "Counter.h"

std::vector<uint8_t> Counter::ReadFreeRegister() {
    std::vector<uint8_t> free_register_read_val;
    uint8_t rx_buf[3];
    uint8_t reg_address = 0x01;
    I2CRequestBuilder i2c_request_builder1;
    i2c_request_builder1.i2c_request_
            ->SetAction(I2C_REGISTER_READ)
            ->SetDeviceId(COUNTER_S35770)
            ->SetNumberOfBytes(3)
            ->SetArray(rx_buf)
            ->SetRegisterAddress(reg_address)
            ->SetSlaveAddress(this->device_address_);

    int result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder1.i2c_request_));

    if (result < 0) {
        throw new Error();
    }
    free_register_read_val.push_back(rx_buf[0]);
    free_register_read_val.push_back(rx_buf[1]);
    free_register_read_val.push_back(rx_buf[2]);

    return free_register_read_val;
}

void Counter::WriteFreeRegister(std::vector<uint8_t> free_register_value) {
    uint8_t register_value[4];
    register_value[0] = 0x81;
    register_value[1] = free_register_value.at(0);
    register_value[2] = free_register_value.at(1);
    register_value[3] = free_register_value.at(2);
    I2CRequestBuilder i2c_request_builder;
    i2c_request_builder.i2c_request_
            ->SetAction(I2C_WRITE)
            ->SetDeviceId(COUNTER_S35770)
            ->SetNumberOfBytes(4)
            ->SetArray(register_value)
            ->SetSlaveAddress(this->device_address_);

    int result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));

    if (result < 0) {
        throw new Error();
    }
}

uint32_t Counter::ReadCounter() {
    uint8_t rx_buff[3];
    I2CRequestBuilder i2c_request_builder;
    i2c_request_builder.i2c_request_
            ->SetAction(I2C_READ)
            ->SetDeviceId(COUNTER_S35770)
            ->SetNumberOfBytes(3)
            ->SetArray(rx_buff)
            ->SetSlaveAddress(this->device_address_);

    int result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));

    if (result < 0) {
        throw new Error();
    }

    uint32_t counter_value = (rx_buff[0] << 16) | (rx_buff[1] << 8) | rx_buff[2];

    GpioState_e current_loop_state = GetLoopFlag();
    if(this->counter_loop_state_!=current_loop_state){
        counter_value = ((0x01<<this->number_of_loop_toggles)<<24) | counter_value;
        this->number_of_loop_toggles++;
        this->counter_loop_state_ = current_loop_state;
    }

    return counter_value;
}

void Counter::ResetCounter() {
    //counter resets if rst2 = 0, rst1 = 1 and rst0 = 0
    std::vector<uint8_t> free_counter_value = this->ReadFreeRegister();
    uint8_t last_byte = free_counter_value.at(2);
    last_byte &= ~(0x05);
    last_byte |= (0x02);
    free_counter_value.at(2) = last_byte;
    this->WriteFreeRegister(free_counter_value);
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
Counter::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    CounterRequest_e counter_request = component_request.GetCounterRequest();
    if (counter_request == COUNTER_READ) {
        uint32_t counter_value = this->ReadCounter();
        std::vector<string> values;
        values.push_back(std::to_string(counter_value));
        response_map.emplace(SENSOR_VALUE, values);
    } else if (counter_request == COUNTER_RESET) {
        this->ResetCounter();
        std::vector<string> values;
        values.push_back(std::to_string(1));
        response_map.emplace(SENSOR_VALUE, values);
    }
    return response_map;
}

void Counter::MuxCounterChannel(CounterChannels_e channel) {
    GpioState_e mux_0_state = STATE_DEFAULT;
    GpioState_e mux_1_state = STATE_DEFAULT;
    if (this->GetCurrentChannel() != channel) {
        this->current_channel_ = channel;
        switch (channel) {
            case EXHAUST_FAN_CNT_CHANNEL:
                mux_0_state = STATE_LOW;
                mux_1_state = STATE_HIGH;
                break;
            case WATER_FLOW_CNT_CHANNEL:
                mux_0_state = STATE_HIGH;
                mux_1_state = STATE_LOW;
                break;
            case OIL_FLOW_CNT_CHANNEL:
                mux_0_state = STATE_LOW;
                mux_1_state = STATE_LOW;
                break;
            default:
                break;
        }

        GpioRequestBuilder gpioRequestBuilder;
        gpioRequestBuilder.gpio_request_
                ->SetAction(ACTION_WRITE_PIN)
                ->SetHardwareComponent(COUNTER_MUX_SELECT_0)
                ->SetState(mux_0_state);
        HardwareService::gpio_request_processor_->ProcessGpioRequest(
                *gpioRequestBuilder.gpio_request_);
        gpioRequestBuilder.gpio_request_
                ->SetAction(ACTION_WRITE_PIN)
                ->SetHardwareComponent(COUNTER_MUX_SELECT_1)
                ->SetState(mux_1_state);
        HardwareService::gpio_request_processor_->ProcessGpioRequest(
                *gpioRequestBuilder.gpio_request_);

        //transition time of the multiplexer, TMUX1204 states it to be 14ns
        usleep(10);
    }
}

CounterChannels_e Counter::GetCurrentChannel() {
    return this->current_channel_;
}

GpioState_e Counter::GetLoopFlag() {
    GpioRequestBuilder gpioRequestBuilder;
    gpioRequestBuilder.gpio_request_
            ->SetHardwareComponent(INDUCTION_BUZZER_GPIO)
            ->SetAction(ACTION_READ_PIN);
    return static_cast<GpioState_e>(HardwareService::gpio_request_processor_->ProcessGpioRequest(
            *gpioRequestBuilder.gpio_request_));
}
