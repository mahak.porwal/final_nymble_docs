//
// Created by fedal on 05/10/21.
//

#ifndef RECIPEENGINE_COUNTER_H
#define RECIPEENGINE_COUNTER_H

#include <map>

typedef enum {
    EXHAUST_FAN_CNT_CHANNEL,
    WATER_FLOW_CNT_CHANNEL,
    OIL_FLOW_CNT_CHANNEL
} CounterChannels_e;

class Counter : public StandaloneComponent {
private:

    const uint8_t device_address_ = 0x32;

    CounterChannels_e current_channel_ = WATER_FLOW_CNT_CHANNEL;

    GpioState_e counter_loop_state_ = STATE_LOW;

    uint8_t number_of_loop_toggles = 0;

    std::vector<uint8_t> ReadFreeRegister();

    void WriteFreeRegister(std::vector<uint8_t> free_register_value);

    uint32_t ReadCounter();

    void ResetCounter();

    GpioState_e GetLoopFlag();

public:

    void MuxCounterChannel(CounterChannels_e);

    CounterChannels_e GetCurrentChannel();

    Counter(Component_e param) : StandaloneComponent(param) {
        this->ResetCounter();
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //RECIPEENGINE_COUNTER_H
