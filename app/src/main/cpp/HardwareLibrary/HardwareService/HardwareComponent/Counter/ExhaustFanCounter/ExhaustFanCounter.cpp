//
// Created by fedal on 11/10/21.
//

#include "ExhaustFanCounter.h"

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
ExhaustFanCounter::ProcessRequest(const ComponentRequest &component_request) {

    this->counter_->MuxCounterChannel(EXHAUST_FAN_CNT_CHANNEL);

    ComponentRequest componentRequest;
    componentRequest.SetComponent(COUNTER)
            .SetCounterRequest(component_request.GetCounterRequest());
    return this->counter_->ProcessRequest(component_request);
}