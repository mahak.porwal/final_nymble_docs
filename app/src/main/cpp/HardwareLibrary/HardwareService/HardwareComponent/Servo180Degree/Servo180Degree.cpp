//
// Created by fedal on 17/12/20.
//

#include "Servo180Degree.h"
//
// Created by fedal on 16/12/20.
//

#include "Servo180Degree.h"
#include "../../Peripherals/Pwm/PwmRequest/PwmRequestBuilder.h"
#include "../../Peripherals/Pwm/PwmRequestProcessor/PwmRequestProcessor.h"

#include "../../Peripherals/Adc/AdcRequest/AdcRequestBuilder.h"

bool Servo180Degree::SetActuator(PwmRequest) {
    return false;
}

float Servo180Degree::GetActuationValue() {

    return 0;
}

int Servo180Degree::ExecuteServoSequence(uint16_t angle, float speed) {
    int result = -1;

    angle = abs(this->offset_angle_ - angle);

    switch (this->state_) {
        case SET_TIMER: {
            this->target_achieved_ = false;
            this->timer_->SetBeginTime();
            this->state_ = WAIT_FOR_TIME;
            this->target_actuation_value_ = ConvertAngleToPulseDurationUs(angle);
        }
            break;
        case WAIT_FOR_TIME: {
            if (this->timer_->GetTimeElapsed() > 10) {
                this->state_ = UPDATE_POSITION;
            }
        }
            break;
        case UPDATE_POSITION: {
            this->current_actuation_value_ = this->UpdatePulseDuration(speed);
            if (this->target_achieved_) {
                result = 1;
                this->state_ = SET_TIMER;
            } else {
                PwmRequestBuilder *pwm_request_builder = new PwmRequestBuilder();
                pwm_request_builder->pwm_request_->SetPulseDurationUs(
                        this->current_actuation_value_)->SetHardwareComponent(
                        this->GetComponentId());
                PwmRequestProcessor::ProcessPwmRequest(*pwm_request_builder->pwm_request_);
                delete pwm_request_builder;
                this->timer_->SetBeginTime();
                this->state_ = WAIT_FOR_TIME;
            }
        }
            break;
    }
    return result;
}

int Servo180Degree::ExecuteServoAngle(uint16_t angle) {
    int result = -1;
    angle = abs(this->offset_angle_ - angle);

    if (angle > this->max_angle_) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                            "angle greater than max angle %d",
                            angle);
        throw new Error();
    }

    this->current_actuation_value_ = ConvertAngleToPulseDurationUs(angle);

    PwmRequestBuilder *pwm_request_builder = new PwmRequestBuilder();
    uint16_t pulse_duration_us = (Servo180Degree::ConvertAngleToPulseDurationUs(angle));
    pwm_request_builder->pwm_request_->SetPulseDurationUs(pulse_duration_us)->SetHardwareComponent(
            this->GetComponentId());
    result = PwmRequestProcessor::ProcessPwmRequest(*pwm_request_builder->pwm_request_);
    delete pwm_request_builder;
    return result;
}

float Servo180Degree::GetSensorReading() {

    AdcRequestBuilder *adc_request_builder = new AdcRequestBuilder;
    ComponentRequestBuilder *hardware_request_builder = new ComponentRequestBuilder;
    int result = -1;
    Component_e feedback_component;
    switch (this->GetComponentId()) {
        case MACRO_SERVO_4: {
            feedback_component = MACRO_SERVO_4_FEEDBACK;
        }
            break;
        case MACRO_SERVO_3: {
            feedback_component = MACRO_SERVO_3_FEEDBACK;
        }
            break;
        case MACRO_SERVO_2: {
            feedback_component = MACRO_SERVO_2_FEEDBACK;
        }
            break;
        case MACRO_SERVO_1: {
            feedback_component = MACRO_SERVO_1_FEEDBACK;
        }
            break;
        case MICRO_SERVO_1: {
            feedback_component = MICRO_SERVO_1_FEEDBACK;
        }
            break;
        case MICRO_SERVO_2: {
            feedback_component = MICRO_SERVO_2_FEEDBACK;
        }
            break;
        case MICRO_SERVO_3: {

            feedback_component = MICRO_SERVO_3_FEEDBACK;
        }
            break;
        case MICRO_SERVO_4: {

            feedback_component = MICRO_SERVO_4_FEEDBACK;
        }
            break;
        case MICRO_SERVO_5: {

            feedback_component = MICRO_SERVO_5_FEEDBACK;
        }
            break;
        case MICRO_SERVO_6: {

            feedback_component = MICRO_SERVO_6_FEEDBACK;
        }
            break;
        default:
            break;
    }
    adc_request_builder->adc_request->SetHardwareComponent(feedback_component);

    hardware_request_builder->component_request_.SetMuxSelect(feedback_component).SetComponent(
            MACRO_MICRO_MUX);
    HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(hardware_request_builder->component_request_);

    hardware_request_builder->component_request_.SetMuxSelect(SELECT_MACRO_ADC);
    HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(hardware_request_builder->component_request_);

    result = AdcRequestProcessor::ProcessAdcRequest(*adc_request_builder->adc_request);

    delete hardware_request_builder;
    delete adc_request_builder;
    return result;
}

uint16_t Servo180Degree::ConvertAngleToPulseDurationUs(uint16_t angle) {
    uint16_t pulse_duration;
    float register_value = angle * 11.1 + 500;
    pulse_duration = (uint16_t) register_value;
    return pulse_duration;
}

uint16_t Servo180Degree::ConvertAngleToPulseDurationUs(float angle) {
    uint16_t pulse_duration;
    float register_value = angle * 11.1 + 500;
    pulse_duration = (uint16_t) register_value;
    return pulse_duration;
}

uint16_t Servo180Degree::UpdatePulseDuration(float speed) {
    if (speed > 10) { speed = 10; }
    uint16_t result = this->current_actuation_value_;
    if (this->current_actuation_value_ < this->target_actuation_value_) {
        //result += Servo180Degree::ConvertAngleToPulseDurationUs((float)(speed*0.039735));
        result += 11.1 * (speed * 0.039735);
        if (result > this->target_actuation_value_) {
            this->target_achieved_ = true;
        }
    } else {
        //result -= Servo180Degree::ConvertAngleToPulseDurationUs((float)(speed*0.039735));
        result -= 11.1 * (speed * 0.039735);
        if (result < this->target_actuation_value_) {
            this->target_achieved_ = true;
        }
    }
    return result;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
Servo180Degree::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    switch (component_request.GetRequestType()) {
        case HARDWARE_REQUEST: {
            if (component_request.GetFrequency() != -1) {
                //GetSensorReading
                float reading = this->GetSensorReading();
                std::vector<string> values;
                values.push_back(std::to_string(reading));
                response_map.emplace(SENSOR_VALUE, values);
            } else if (component_request.GetSpeed() != -1 &&
                       component_request.GetPosition() != -1) {
                //ExecuteServoSequence
                int result = this->ExecuteServoSequence(component_request.GetPosition(),
                                                        component_request.GetSpeed());
                std::vector<string> values;
                values.push_back(std::to_string(result));
                response_map.emplace(SEQUENCE_RESULT, values);
            } else if (component_request.GetPosition() != -1) {
                //ExecuteServoAngle
                int result = this->ExecuteServoAngle(component_request.GetPosition());
                std::vector<string> values;
                values.push_back(std::to_string(result));
                response_map.emplace(SEQUENCE_RESULT, values);
            }
        }
            break;
        case SEQUENCE_REQUEST: {
            int result = this->ExecuteServoSequence(component_request.GetPosition(),
                                                    component_request.GetSpeed());
            std::vector<string> values;
            values.push_back(std::to_string(result));
            response_map.emplace(SEQUENCE_RESULT, values);
        }
            break;
    }
    return response_map;
}

