//
// Created by fedal on 17/12/20.
//

#ifndef JULIA_ANDROID_SERVO180DEGREE_H
#define JULIA_ANDROID_SERVO180DEGREE_H


#include "Servo180DegreeEnums.h"
#include "../../../Component/SubsystemComponent/SubsystemComponent.h"

class
Servo180Degree : public SubsystemComponent {
private:
    uint16_t offset_angle_;
    uint16_t current_actuation_value_;
    uint16_t target_actuation_value_;
    uint16_t max_angle_;
    Timer *timer_;
    ServoSequenceState_e state_;

    bool SetActuator(PwmRequest);

    int ExecuteServoSequence(uint16_t, float);

    int ExecuteServoAngle(uint16_t);

    uint16_t UpdatePulseDuration(float speed);

    float GetSensorReading();

    bool target_achieved_;

    static uint16_t ConvertAngleToPulseDurationUs(uint16_t);

    static uint16_t ConvertAngleToPulseDurationUs(float);

public:
    Servo180Degree(Component_e hardware_component, const Subsystem &subsystem,
                   uint16_t offset_angle) :
            SubsystemComponent(hardware_component, subsystem) {
        this->max_angle_ = 180;
        this->timer_ = new Timer;
        this->state_ = SET_TIMER;
        this->offset_angle_ = offset_angle;
        this->current_actuation_value_ = ConvertAngleToPulseDurationUs(this->offset_angle_);
    }

    ~Servo180Degree() {
        delete this->timer_;
    }

    float GetActuationValue();

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);

};


#endif //JULIA_ANDROID_SERVO180DEGREE_H
