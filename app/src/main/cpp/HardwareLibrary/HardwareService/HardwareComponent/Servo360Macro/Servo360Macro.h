//
// Created by fedal on 24/11/21.
//

#ifndef RECIPEENGINE_SERVO360MACRO_H
#define RECIPEENGINE_SERVO360MACRO_H

#include "../../../Component/SubsystemComponent/SubsystemComponent.h"


//>>in clockwise dispense rotation, the analog feedback decreases
//requirements
//need to limit the servo rotation between the two thresholds of the angle
//need to create the interface such that a person can move in the above constrained angles relatively
//0 - to - 180
#define EXTREME_ANGLE_BUFFER 2
class Servo360Macro : public SubsystemComponent {
    typedef enum {
        CALCULATE_ABSOLUTE_VALUES,
        START_SERVO,
        OBSERVE_SERVO_FEEDBACK
    } Servo360MacroSequenceState_e;

private:
    uint16_t home_angle_;
    uint16_t dispense_end_angle_;
    uint16_t home_extreme_angle_;
    uint16_t end_extreme_angle_;
    ActuatorDirections_e decreasing_angle_direction_;
    ActuatorDirections_e increasing_angle_direction_;
    Timer timer_;
    Servo360MacroSequenceState_e state_;
    bool is_target_angle_greater_;
    uint16_t absolute_target_angle_;

    int ExecuteServoSequence(uint16_t position, float speed);

    int ExecuteServoRotation(float, ActuatorDirections_e);

    float GetSensorReading();

    static uint16_t ConvertSpeedToPulseDurationUs(float, ActuatorDirections_e);

public:
    Servo360Macro(Component_e hardware_component, const Subsystem &subsystem,
                  ActuatorDirections_e decreasing_angle_direction, uint16_t home_angle,
                  uint16_t dispense_end_angle) :
            SubsystemComponent(hardware_component, subsystem) {
        this->state_ = CALCULATE_ABSOLUTE_VALUES;
        this->home_angle_ = home_angle;
        this->dispense_end_angle_ = dispense_end_angle;
        this->decreasing_angle_direction_ = decreasing_angle_direction;
        if(this->dispense_end_angle_ > this->home_angle_){
            this->home_extreme_angle_ = this->home_angle_ + EXTREME_ANGLE_BUFFER;
            this->end_extreme_angle_ = this->dispense_end_angle_ - EXTREME_ANGLE_BUFFER;
        }else{
            this->home_extreme_angle_ = this->home_angle_ - EXTREME_ANGLE_BUFFER;
            this->end_extreme_angle_ = this->dispense_end_angle_ + EXTREME_ANGLE_BUFFER;
        }

        this->increasing_angle_direction_ =
                DIRECTION_CLOCKWISE == this->decreasing_angle_direction_ ?
                DIRECTION_ANTICLOCKWISE : DIRECTION_CLOCKWISE;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //RECIPEENGINE_SERVO360MACRO_H
