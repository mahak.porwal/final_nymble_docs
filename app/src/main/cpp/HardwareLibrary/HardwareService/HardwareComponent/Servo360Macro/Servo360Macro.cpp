//
// Created by fedal on 24/11/21.
//

#include "Servo360Macro.h"

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
Servo360Macro::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    switch (component_request.GetRequestType()) {
        case HARDWARE_REQUEST:
            if (component_request.GetFrequency() != -1) {
                //GetSensorReading
                float reading = this->GetSensorReading();
                std::vector<string> values;
                values.push_back(std::to_string(reading));
                response_map.emplace(SENSOR_VALUE, values);
                //return this->GetSensorReading();
            } else if (component_request.GetSpeed() != -1 &&
                       component_request.GetPosition() != -1) {
                //ExecuteServoSequence
                int result = this->ExecuteServoSequence(component_request.GetPosition(),
                                                        component_request.GetSpeed());
                std::vector<string> values;
                values.push_back(std::to_string(result));
                response_map.emplace(SEQUENCE_RESULT, values);
            } else if (component_request.GetSpeed() != -1 &&
                       component_request.GetDirection() != DIRECTION_DEFAULT) {
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                    "Servo Rotation");
                //ExecuteServoRotation

                int result = this->ExecuteServoRotation(component_request.GetSpeed(),
                                                        component_request.GetDirection());
                std::vector<string> values;
                values.push_back(std::to_string(result));
                response_map.emplace(SEQUENCE_RESULT, values);
            }
            break;
        case SEQUENCE_REQUEST:
            break;
    }
    return response_map;
}

int Servo360Macro::ExecuteServoRotation(float speed, ActuatorDirections_e direction) {
    uint16_t pulse_duration_us =
            (Servo360Macro::ConvertSpeedToPulseDurationUs(speed, direction));
    PwmRequestBuilder pwm_request_builder;
    __android_log_print(ANDROID_LOG_ERROR, "ServoMacro  : ",
                        "Servo micro seconds %d",
                        pulse_duration_us);
    pwm_request_builder.pwm_request_->SetPulseDurationUs(
            pulse_duration_us)->SetHardwareComponent(
            this->GetComponentId());
    return PwmRequestProcessor::ProcessPwmRequest(*pwm_request_builder.pwm_request_);

}

int Servo360Macro::ExecuteServoSequence(uint16_t target_angle, float speed) {
    int result = -1;
    switch (this->state_) {
        case CALCULATE_ABSOLUTE_VALUES: {
            if (home_angle_ > dispense_end_angle_) {
                this->absolute_target_angle_ = home_angle_ - target_angle;
            } else {
                this->absolute_target_angle_ = home_angle_ + target_angle;
            }
            uint16_t current_angle = (uint16_t)
                    this->GetSensorReading();
            if (this->absolute_target_angle_ > current_angle) {
                this->is_target_angle_greater_ = true;
            } else {
                this->is_target_angle_greater_ = false;
            }
            __android_log_print(ANDROID_LOG_ERROR, "ServoMacro  : ",
                                "Absolute %d current angle %d", this->absolute_target_angle_,
                                current_angle);
            if (abs(this->absolute_target_angle_ - current_angle) < 3) {
                __android_log_print(ANDROID_LOG_ERROR, "ServoMacro  : ",
                                    "Target position close enough");
                return 1;
            }

            this->state_ = START_SERVO;
        }
            break;
        case START_SERVO: {
            //start servo
            if (this->is_target_angle_greater_) {
                this->ExecuteServoRotation(speed, this->increasing_angle_direction_);
            } else {
                this->ExecuteServoRotation(speed, this->decreasing_angle_direction_);
            }
            this->timer_.SetBeginTime();
            this->state_ = OBSERVE_SERVO_FEEDBACK;
        }
            break;
        case OBSERVE_SERVO_FEEDBACK: {
            uint16_t current_angle = (uint16_t)
                    this->GetSensorReading();
            /*__android_log_print(ANDROID_LOG_ERROR, "ServoMacro  : ",
                                "Current angle %d", current_angle);*/
            //TODO : handle extreme positions

            bool greater_than_extreme_end_angle_ = this->end_extreme_angle_ < current_angle;
            bool lesser_than_extreme_home_angle_ = this->home_extreme_angle_ > current_angle;

            if ((lesser_than_extreme_home_angle_ && !greater_than_extreme_end_angle_ &&
                 !is_target_angle_greater_) ||
                (!lesser_than_extreme_home_angle_ && greater_than_extreme_end_angle_ &&
                 is_target_angle_greater_)) {
                //will go beyond working range
                //stop
                __android_log_print(ANDROID_LOG_ERROR, "ServoMacro  : ",
                                    "Out of range angle reached %d home extreme %d"
                                    "end extreme %d",
                                    current_angle, this->home_extreme_angle_,
                                    this->end_extreme_angle_);
                this->ExecuteServoRotation(0, DIRECTION_CLOCKWISE);
                this->state_ = CALCULATE_ABSOLUTE_VALUES;
                result = 1;
            }else if (is_target_angle_greater_) {
                if (current_angle > this->absolute_target_angle_) {
                    //stop
                    __android_log_print(ANDROID_LOG_ERROR, "ServoMacro  : ",
                                        "Servo current angle %d greater than ", current_angle);
                    this->ExecuteServoRotation(0, DIRECTION_CLOCKWISE);
                    this->state_ = CALCULATE_ABSOLUTE_VALUES;
                    result = 1;
                }
            } else {
                if (current_angle < this->absolute_target_angle_) {
                    //stop
                    __android_log_print(ANDROID_LOG_ERROR, "ServoMacro : ",
                                        "Servo current angle %d less than ", current_angle);
                    this->ExecuteServoRotation(0, DIRECTION_CLOCKWISE);
                    this->state_ = CALCULATE_ABSOLUTE_VALUES;
                    result = 1;
                }
            }


            /*if ((greater_than_extreme_end_angle_ ^ lesser_than_extreme_home_angle_)) {
                if (is_target_angle_greater_) {
                    //greater target angle, in future the current angle will increase
                    if (greater_than_extreme_end_angle_) {
                        //will go beyond working range
                        //stop
                        __android_log_print(ANDROID_LOG_ERROR, "ServoMacro  : ",
                                            "Out of range angle reached %d home extreme %d"
                                            "end extreme %d",
                                            current_angle, this->home_extreme_angle_,
                                            this->end_extreme_angle_);
                        this->ExecuteServoRotation(0, DIRECTION_CLOCKWISE);
                        this->state_ = CALCULATE_ABSOLUTE_VALUES;
                        result = 1;
                    } else {
                        //will go in working range
                    }
                } else {
                    //lesser target angle, in future the current angle will decrease
                    if (greater_than_extreme_end_angle_) {
                        //will go in the working range
                    } else {
                        //will go beyond working range
                        //stop
                        __android_log_print(ANDROID_LOG_ERROR, "ServoMacro  : ",
                                            "Out of range angle reached %d home extreme %d"
                                            "end extreme %d",
                                            current_angle, this->home_extreme_angle_,
                                            this->end_extreme_angle_);
                        this->ExecuteServoRotation(0, DIRECTION_CLOCKWISE);
                        this->state_ = CALCULATE_ABSOLUTE_VALUES;
                        result = 1;
                    }
                }
            } else if (is_target_angle_greater_) {
                if (current_angle > this->absolute_target_angle_) {
                    //stop
                    __android_log_print(ANDROID_LOG_ERROR, "ServoMacro  : ",
                                        "Servo current angle %d greater than ", current_angle);
                    this->ExecuteServoRotation(0, DIRECTION_CLOCKWISE);
                    this->state_ = CALCULATE_ABSOLUTE_VALUES;
                    result = 1;
                }
            } else {
                if (current_angle < this->absolute_target_angle_) {
                    //stop
                    __android_log_print(ANDROID_LOG_ERROR, "ServoMacro : ",
                                        "Servo current angle %d less than ", current_angle);
                    this->ExecuteServoRotation(0, DIRECTION_CLOCKWISE);
                    this->state_ = CALCULATE_ABSOLUTE_VALUES;
                    result = 1;
                }
            }*/
        }
            break;
        default:
            break;
    }
    return result;
}

float Servo360Macro::GetSensorReading() {
    ComponentRequestBuilder hardware_request_builder;
    Component_e feedback_component;
    switch (this->GetComponentId()) {
        case MACRO_SERVO_4: {
            feedback_component = MACRO_SERVO_4_FEEDBACK;
        }
            break;
        case MACRO_SERVO_3: {
            feedback_component = MACRO_SERVO_3_FEEDBACK;
        }
            break;
        case MACRO_SERVO_2: {
            feedback_component = MACRO_SERVO_2_FEEDBACK;
        }
            break;
        case MACRO_SERVO_1: {
            feedback_component = MACRO_SERVO_1_FEEDBACK;
        }
            break;
        default:
            break;
    }

    ComponentRequestBuilder component_request_builder;
    component_request_builder.component_request_.SetRequestType(
            HARDWARE_REQUEST).SetComponent(
            feedback_component).SetSensorUnit(ANGLE_VALUE);
    std::string sensor_value = HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(
            component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                        "Servo pod filtered reading %s",
                        sensor_value.c_str());
    return std::stod(sensor_value.c_str());
}

uint16_t
Servo360Macro::ConvertSpeedToPulseDurationUs(float speed, ActuatorDirections_e direction) {
    if (speed == 0) {
        return 1500;
    }
    uint16_t pulse_duration;
    if (speed < 1) {
        speed = 1;
    } else if (speed > 10) {
        speed = 10;
    }
    float register_value;
    if (DIRECTION_CLOCKWISE == direction) {
        //register_value = speed * 17.8 + 1522;
        register_value = speed * 7.78 + 1532;
    } else {
        //register_value = speed * -17.8 + 1478;
        //register_value = speed * -12.2 + 1472;
        //register_value = speed * -8.89 + 1469;
        //register_value = speed * -6.67 + 1467;
        register_value = speed * -7.78 + 1468;
    }
    pulse_duration = (uint16_t) register_value;
    return pulse_duration;
}