//
// Created by fedal on 21/12/20.
//

#include "ExhaustFan.h"

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
ExhaustFan::ProcessRequest(const ComponentRequest &component_request) {
    double result = -1;
    if (component_request.GetSpeed() != -1) {
        float speed = component_request.GetSpeed();
        if (speed > 10.0) { speed = 10; }
        speed *= 10;
        this->TurnOnOffExhaustFanGpio(ON_STATE);
        PwmRequestBuilder *pwmRequestBuilder = new PwmRequestBuilder;
        pwmRequestBuilder->pwm_request_->SetHardwareComponent(
                this->GetComponentId())->SetDutyCycle(speed);
        result = HardwareService::pwm_request_processor_->ProcessPwmRequest(
                *pwmRequestBuilder->pwm_request_);
        delete pwmRequestBuilder;
    } else if (component_request.GetState() != DEFAULT_ON_OFF_STATE) {
        result = this->TurnOnOffExhaust(component_request.GetState());
    }
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;
    values.push_back(std::to_string(result));
    response_map.emplace(COMMAND_RESULT, values);
    return response_map;
}

int ExhaustFan::TurnOnOffExhaust(OnOffState_e state) {
    int result = -1;
    PwmRequestBuilder *pwmRequestBuilder = new PwmRequestBuilder;
    if (state == ON_STATE) {
        pwmRequestBuilder->pwm_request_->SetHardwareComponent(
                this->GetComponentId())->SetDutyCycle(
                this->turn_on_value_);
        this->TurnOnOffExhaustFanGpio(ON_STATE);
    } else {
        pwmRequestBuilder->pwm_request_->SetHardwareComponent(
                this->GetComponentId())->SetDutyCycle(
                this->turn_off_value_);
        this->TurnOnOffExhaustFanGpio(OFF_STATE);
    }
    result = HardwareService::pwm_request_processor_->ProcessPwmRequest(
            *pwmRequestBuilder->pwm_request_);
    delete pwmRequestBuilder;
    return result;
}

void ExhaustFan::TurnOnOffExhaustFanGpio(OnOffState_e state) {
    GpioRequestBuilder gpioRequestBuilder;
    gpioRequestBuilder.gpio_request_
            ->SetHardwareComponent(EXHAUST_FAN_ON_OFF)
            ->SetAction(ACTION_WRITE_PIN);
    if (ON_STATE == state) {
        gpioRequestBuilder.gpio_request_
                ->SetState(STATE_HIGH);
    } else {
        gpioRequestBuilder.gpio_request_
                ->SetState(STATE_LOW);
    }
    HardwareService::gpio_request_processor_->ProcessGpioRequest(
            *gpioRequestBuilder.gpio_request_);
}
