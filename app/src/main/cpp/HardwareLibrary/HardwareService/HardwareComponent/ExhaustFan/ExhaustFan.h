//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_EXHAUSTFAN_H
#define JULIA_ANDROID_EXHAUSTFAN_H


class ExhaustFan : public StandaloneComponent {
private:
    float turn_on_value_;
    float turn_off_value_;
    int TurnOnOffExhaust(OnOffState_e);
    void TurnOnOffExhaustFanGpio(OnOffState_e);
public:
    ExhaustFan(Component_e hardware_component, float on_value, float off_value) :
            StandaloneComponent(hardware_component) {
        this->turn_on_value_ = on_value;
        this->turn_off_value_ = off_value;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_EXHAUSTFAN_H
