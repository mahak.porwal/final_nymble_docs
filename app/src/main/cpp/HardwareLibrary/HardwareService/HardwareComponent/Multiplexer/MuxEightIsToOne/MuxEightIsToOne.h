//
// Created by fedal on 17/12/20.
//

#ifndef JULIA_ANDROID_MUXEIGHTISTOONE_H
#define JULIA_ANDROID_MUXEIGHTISTOONE_H


#include "EightIsToOneEnums.h"
#include "../../../../Error/Error.h"

class MuxEightIsToOne {
protected:
    virtual int MuxInput(Component_e) = 0;
    virtual ~MuxEightIsToOne() {}
};


#endif //JULIA_ANDROID_MUXEIGHTISTOONE_H
