//
// Created by fedal on 17/12/20.
//

#ifndef JULIA_ANDROID_MUXTWOISTOONE_H
#define JULIA_ANDROID_MUXTWOISTOONE_H

#include "TwoIsToOneEnums.h"
#include "../../../../Error/Error.h"
#include "../../../../Component/StandaloneComponent/StandaloneComponent.h"

class MuxTwoIsToOne : public StandaloneComponent {

private:
    int MuxInput(TwoIsToOne_e);

public:
    MuxTwoIsToOne(Component_e component) :
            StandaloneComponent(component) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_MUXTWOISTOONE_H
