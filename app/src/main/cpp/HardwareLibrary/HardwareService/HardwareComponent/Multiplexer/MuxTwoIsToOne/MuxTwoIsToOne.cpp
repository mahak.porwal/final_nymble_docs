
//
// Created by fedal on 17/12/20.
//

#include "MuxTwoIsToOne.h"
#include "../../../Peripherals/Gpio/GpioRequest/GpioRequestBuilder.h"

int MuxTwoIsToOne::MuxInput(TwoIsToOne_e input) {
    int result = -1;
    GpioRequestBuilder *gpio_request_builder = new GpioRequestBuilder;
    gpio_request_builder->gpio_request_->SetHardwareComponent(MACRO_MICRO_FB_SELECT_1)->SetAction(
            ACTION_WRITE_PIN);
    if (input == SELECT_MACRO_ADC) {
        gpio_request_builder->gpio_request_->SetState(STATE_LOW);
    } else {
        gpio_request_builder->gpio_request_->SetState(STATE_HIGH);
    }
    result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
    return result;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
MuxTwoIsToOne::ProcessRequest(const ComponentRequest &component_request) {
    double result = -1;
    if (component_request.GetMuxSelect() != -1) {
        TwoIsToOne_e select = static_cast<TwoIsToOne_e>(component_request.GetMuxSelect());
        result = (int) MuxInput(select);
    } else {
        throw new Error();
    }
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;
    values.push_back(std::to_string(result));
    response_map.emplace(COMMAND_RESULT, values);
    return response_map;
}

