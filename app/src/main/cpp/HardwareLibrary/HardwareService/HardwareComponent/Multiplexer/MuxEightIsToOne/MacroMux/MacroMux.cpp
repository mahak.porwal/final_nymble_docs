//
// Created by fedal on 17/12/20.
//

#include "MacroMux.h"
#include "../../../../Peripherals/Gpio/GpioRequest/GpioRequestBuilder.h"

int MacroMux::MuxInput(Component_e hardware_component) {
    int result = -1;
    EightIsToOne_e input = MacroMux::component_to_input_map_.at(hardware_component);
    GpioRequestBuilder *gpio_request_builder = new GpioRequestBuilder;
    switch (input) {
        case SELECT_0:
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_1)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_LOW);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_2)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_LOW);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_3)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_LOW);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            break;
        case SELECT_1:
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_1)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_HIGH);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_2)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_LOW);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_3)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_LOW);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            break;
        case SELECT_2:
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_1)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_LOW);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_2)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_HIGH);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_3)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_LOW);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            break;
        case SELECT_3:
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_1)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_HIGH);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_2)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_HIGH);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            gpio_request_builder->gpio_request_->SetHardwareComponent(
                    MACRO_POT_MUX_SELECT_3)->SetAction(ACTION_WRITE_PIN)->SetState(STATE_LOW);
            result = GpioRequestProcessor::ProcessGpioRequest(*gpio_request_builder->gpio_request_);
            break;
        default:
            delete gpio_request_builder;
            throw new Error();
    }

    delete gpio_request_builder;
    return result;
}

void MacroMux::PopulateComponentToInputMap() {
    MacroMux::component_to_input_map_.insert({MACRO_SERVO_1_FEEDBACK, SELECT_0});
    MacroMux::component_to_input_map_.insert({MACRO_SERVO_2_FEEDBACK, SELECT_1});
    MacroMux::component_to_input_map_.insert({MACRO_SERVO_3_FEEDBACK, SELECT_2});
    MacroMux::component_to_input_map_.insert({MACRO_SERVO_4_FEEDBACK, SELECT_3});
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
MacroMux::ProcessRequest(const ComponentRequest &component_request) {
    double result = -1;
    if (component_request.GetMuxSelect() != -1) {
        Component_e select = static_cast<Component_e>(component_request.GetMuxSelect());
        result = (int) MuxInput(select);
    } else {
        throw new Error();
    }
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;
    values.push_back(std::to_string(result));
    response_map.emplace(COMMAND_RESULT, values);
    return response_map;
}

