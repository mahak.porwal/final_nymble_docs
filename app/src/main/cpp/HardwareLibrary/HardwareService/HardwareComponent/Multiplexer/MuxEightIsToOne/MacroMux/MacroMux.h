//
// Created by fedal on 17/12/20.
//

#ifndef JULIA_ANDROID_MACROMUX_H
#define JULIA_ANDROID_MACROMUX_H


#include "../MuxEightIsToOne.h"

class MacroMux : public MuxEightIsToOne, public SubsystemComponent {
    static map<Component_e, EightIsToOne_e> component_to_input_map_;
    static void PopulateComponentToInputMap();

private:
    int MuxInput(Component_e);
public:
    MacroMux(Component_e hardware_component, Subsystem subsystem) :
            SubsystemComponent(hardware_component, subsystem) {
        this->PopulateComponentToInputMap();
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};

map<Component_e, EightIsToOne_e> MacroMux::component_to_input_map_;

#endif //JULIA_ANDROID_MACROMUX_H
