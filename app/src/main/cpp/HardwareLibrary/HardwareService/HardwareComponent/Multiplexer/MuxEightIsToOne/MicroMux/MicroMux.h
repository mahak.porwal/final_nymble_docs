//
// Created by fedal on 17/12/20.
//

#ifndef JULIA_ANDROID_MICROMUX_H
#define JULIA_ANDROID_MICROMUX_H


#include "../EightIsToOneEnums.h"
#include "../../../../../Error/Error.h"

class MicroMux : public MuxEightIsToOne, public SubsystemComponent {
    static map<Component_e, EightIsToOne_e> component_to_input_map_;

    static void PopulateComponentToInputMap();

private:
    int MuxInput(Component_e);

public:
    MicroMux(Component_e hardware_component, Subsystem subsystem) :
            SubsystemComponent(hardware_component, subsystem) {
        this->PopulateComponentToInputMap();
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};

map<Component_e, EightIsToOne_e> MicroMux::component_to_input_map_;

#endif //JULIA_ANDROID_MICROMUX_H
