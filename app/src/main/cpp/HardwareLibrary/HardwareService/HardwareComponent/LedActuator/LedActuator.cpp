//
// Created by fedal on 21/12/20.
//

#include "LedActuator.h"


std::unordered_map<ResponseParameters_e, std::vector<std::string>>
LedActuator::ProcessRequest(const ComponentRequest &component_request) {
    double result = -1;
    if (component_request.GetSpeed() != -1) {
        float speed = component_request.GetSpeed();
        if (speed > 10.0 || speed < 0.0) { speed = 10; }
        PwmRequestBuilder *pwmRequestBuilder = new PwmRequestBuilder;
        pwmRequestBuilder->pwm_request_->SetHardwareComponent(
                this->GetComponentId())->SetDutyCycle(10 * speed);
        result = HardwareService::pwm_request_processor_->ProcessPwmRequest(
                *pwmRequestBuilder->pwm_request_);
        delete pwmRequestBuilder;
    } else if (component_request.GetState() != DEFAULT_ON_OFF_STATE) {
        result = this->TurnOnOffLed(component_request.GetState());
    }
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;
    values.push_back(std::to_string(result));
    response_map.emplace(COMMAND_RESULT, values);
    return response_map;
}

int LedActuator::TurnOnOffLed(OnOffState_e state) {
    int result = -1;
    PwmRequestBuilder *pwmRequestBuilder = new PwmRequestBuilder;
    if (state == ON_STATE) {
        pwmRequestBuilder->pwm_request_->SetHardwareComponent(
                this->GetComponentId())->SetDutyCycle(
                this->turn_on_value_);
    } else {
        pwmRequestBuilder->pwm_request_->SetHardwareComponent(
                this->GetComponentId())->SetDutyCycle(
                this->turn_off_value_);
    }
    result = HardwareService::pwm_request_processor_->ProcessPwmRequest(
            *pwmRequestBuilder->pwm_request_);
    delete pwmRequestBuilder;
    return result;
}