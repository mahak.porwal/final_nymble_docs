//
// Created by fedal on 23/8/21.
//



#include"data.h"

using namespace std;



void setIntData(int gs, int vgrad, int voff, int scaleval) {
    gradscale = gs;
    vddscgrad = vgrad;
    vddscoff = voff;
    pcscaleval = scaleval;
}

void setFloatData(float pmin, float pmax, float pth1, float pth2, float vth1, float vth2, float glob_gain) {
    pixc_min = pmin;
    pixc_max = pmax;
    ptat_th1 = pth1;
    ptat_th2 = pth2;
    vdd_th1 = vth1;
    vdd_th2 = vth2;
    globalgain = glob_gain;
}

void setDoubleData(double pgrad, double poffset) {
    ptat_grad = pgrad;
    ptat_offset = poffset;
}

void setArrayData(short *vddGrad, short *vddOff) {
    vddcompgrad = vddGrad;
    vddcompoff = vddOff;
}

void setMatrixData(short *grad, short *offset, int *pm) {
    thgrad = grad;
    thoffset = offset;
    pmatrix = pm;
}
