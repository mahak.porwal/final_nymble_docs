#include <iostream>
#include<cmath>
#include<vector>
#include<sstream>
#include<string>
#include<time.h>

#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>
#include<unistd.h>


//function to convert two 8bit numbers to 16bit numbers in little endian format
int lend(int, int);

// function to find the sum of all the elements in a matrix
int sum(int *, int);

// function to return sensor (pixel matrix) data
void sensor(int *, int[32][32], int *);

// function to calculate electrical offset values
void elec_off(int *, int *, int *);

// Lookup function
float lookup(double, int);

// Main calculation
std::vector<double> temperature(int[2580], ThermalEepromData);
