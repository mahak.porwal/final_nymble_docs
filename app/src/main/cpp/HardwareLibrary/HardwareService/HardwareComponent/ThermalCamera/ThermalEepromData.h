//
// Created by fedal on 25/8/21.
//

#ifndef RECIPEENGINE_THERMALEEPROMDATA_H
#define RECIPEENGINE_THERMALEEPROMDATA_H


class ThermalEepromData {
private:
    int gradscale;
    int vddscgrad;
    int vddscoff;
    int pcscaleval;

    float pixc_min;
    float pixc_max;
    float ptat_th1;
    float ptat_th2;
    float vdd_th1;
    float vdd_th2;

    double ptat_grad;
    double ptat_offset;

    float globalgain;

    short *vddcompgrad; // [256]
    short *vddcompoff; // [256]

    short *thgrad; // 32x32
    short *thoffset; // 32x32
    int *pmatrix; // 32x32
public:
    int getGradscale() const {
        return gradscale;
    }

    int getVddscgrad() const {
        return vddscgrad;
    }

    int getVddscoff() const {
        return vddscoff;
    }

    int getPcscaleval() const {
        return pcscaleval;
    }

    float getPixcMin() const {
        return pixc_min;
    }

    float getPixcMax() const {
        return pixc_max;
    }

    float getPtatTh1() const {
        return ptat_th1;
    }

    float getPtatTh2() const {
        return ptat_th2;
    }

    float getVddTh1() const {
        return vdd_th1;
    }

    float getVddTh2() const {
        return vdd_th2;
    }

    double getPtatGrad() const {
        return ptat_grad;
    }

    double getPtatOffset() const {
        return ptat_offset;
    }

    float getGlobalgain() const {
        return globalgain;
    }

    short *getVddcompgrad() const {
        return vddcompgrad;
    }

    short *getVddcompoff() const {
        return vddcompoff;
    }

    short *getThgrad() const {
        return thgrad;
    }

    short *getThoffset() const {
        return thoffset;
    }

    int *getPmatrix() const {
        return pmatrix;
    }

    void setIntData(int, int, int, int);

// Set Float data
    void setFloatData(float, float, float, float, float, float, float);

// Set Double data
    void setDoubleData(double, double);

// Set Array data

    void setArrayData(short[256], short[256]);

// Set Matrix data
    void setMatrixData(short[1024], short[1024], int[1024]);
};


#endif //RECIPEENGINE_THERMALEEPROMDATA_H
