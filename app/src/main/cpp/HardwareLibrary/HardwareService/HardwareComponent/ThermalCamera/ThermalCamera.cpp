//
// Created by fedal on 21/8/21.
//

#include "ThermalCamera.h"

void ThermalCamera::ReadEeprom() {
    ComponentRequestBuilder component_request_builder;
    component_request_builder
            .component_request_
            .SetComponent(THERMAL_CAMERA_EEPROM)
            .SetEepromAddr(0x0000)
            .SetEepromNumOfSeqReads(8000);

    auto result = HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(component_request_builder.component_request_);

    vector<std::string> sensor_values = result.at(SENSOR_VALUE);
    int grad_scale = std::stoi(sensor_values.at(0x08));
    int vdd_sc_grad = std::stoi(sensor_values.at(0x4E));
    int vdd_sc_off = std::stoi(sensor_values.at(0x4F));
    uint8_t arr[] = {
            (uint8_t) std::stoi(sensor_values.at(0x00)),
            (uint8_t) std::stoi(sensor_values.at(0x01)),
            (uint8_t) std::stoi(sensor_values.at(0x02)),
            (uint8_t) std::stoi(sensor_values.at(0x03))};
    float pixc_min = *(float *) &arr;

    uint8_t arr1[] = {
            (uint8_t) std::stoi(sensor_values.at(0x04)),
            (uint8_t) std::stoi(sensor_values.at(0x05)),
            (uint8_t) std::stoi(sensor_values.at(0x06)),
            (uint8_t) std::stoi(sensor_values.at(0x07))};
    float pixc_max = *(float *) &arr1;

    uint8_t arr2[] = {
            (uint8_t) std::stoi(sensor_values.at(0x34)),
            (uint8_t) std::stoi(sensor_values.at(0x35)),
            (uint8_t) std::stoi(sensor_values.at(0x36)),
            (uint8_t) std::stoi(sensor_values.at(0x37))};
    float ptat_grad = *(float *) &arr2;

    uint8_t arr3[] = {
            (uint8_t) std::stoi(sensor_values.at(0x38)),
            (uint8_t) std::stoi(sensor_values.at(0x39)),
            (uint8_t) std::stoi(sensor_values.at(0x3A)),
            (uint8_t) std::stoi(sensor_values.at(0x3B))};
    float ptat_offset = *(float *) &arr3;

    float ptat_th1 = (float) (std::stoi(sensor_values.at(0x3C)) +
                              std::stoi(sensor_values.at(0x3D)) * 256);

    float ptat_th2 = (float) (std::stoi(sensor_values.at(0x3E)) +
                              std::stoi(sensor_values.at(0x3F)) * 256);

    float vdd_th1 = (float) (std::stoi(sensor_values.at(0x26)) +
                             std::stoi(sensor_values.at(0x27)) * 256);

    float vdd_th2 = (float) (std::stoi(sensor_values.at(0x28)) +
                             std::stoi(sensor_values.at(0x29)) * 256);

    float global_gain = (std::stoi(sensor_values.at(0x55)) +
                         std::stoi(sensor_values.at(0x56)) * 256);

    vector<short> vdd_comp_grad = this->getVddCompGradMatrix(sensor_values);

    this->vdd_comp_grad = (short *) malloc(vdd_comp_grad.size()*sizeof(short));
    int i = 0;
    for (int element:vdd_comp_grad) {
        *(this->vdd_comp_grad + i++) = element;
    }


    vector<short> vdd_comp_off = this->getVddCompOffMatrix(sensor_values);
    i = 0;
    this->vdd_comp_off = (short *) malloc(vdd_comp_off.size()*sizeof(short));
    for (int element:vdd_comp_off) {
        *(this->vdd_comp_off + i++) = element;
    }

    vector<short> th_grad = this->getThhGradMatrix(sensor_values);
    i = 0;
    this->th_grad = (short *) malloc(th_grad.size()*sizeof(short));
    for (int element:th_grad) {
        *(this->th_grad + i++) = element;
    }

    vector<short> th_offset = this->getThoffsetMatrix(sensor_values);
    i = 0;
    this->th_offset = (short *) malloc(th_offset.size()*sizeof(short));
    for (int element:th_offset) {
        *(this->th_offset + i++) = element;
    }

    vector<int> pixel_matrix = this->getPixelMatrix(sensor_values);
    i = 0;
    this->p_matrix = (int *) malloc(pixel_matrix.size()*sizeof(int));
    for (int element:pixel_matrix) {
        *(this->p_matrix + i++) = element;
    }

    thermal_eeprom_data.setIntData(grad_scale, vdd_sc_grad, vdd_sc_off, this->pc_scale_val);
    thermal_eeprom_data.setFloatData(pixc_min, pixc_max, ptat_th1, ptat_th2, vdd_th1, vdd_th2,
                                     global_gain);
    thermal_eeprom_data.setDoubleData(ptat_grad, ptat_offset);
    thermal_eeprom_data.setArrayData(this->vdd_comp_grad, this->vdd_comp_off);
    thermal_eeprom_data.setMatrixData(this->th_grad, this->th_offset, this->p_matrix);
}

void ThermalCamera::WakeUp() {
    uint8_t arr[2] = {0x01, 0x01};
    I2CRequestBuilder i2c_request_builder;
    i2c_request_builder.i2c_request_
            ->SetAction(I2C_WRITE)
            ->SetDeviceId(THERMAL_CAMERA_DEVICE)
            ->SetNumberOfBytes(2)
            ->SetArray(arr)
            ->SetSlaveAddress(this->device_address_);

    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));
}

void ThermalCamera::Calibrate() {

    for (int i = 0; i < 7; i++) {
        const uint8_t *arr = CALIBRATIONS[i];
        I2CRequestBuilder i2c_request_builder;
        i2c_request_builder.i2c_request_
                ->SetAction(I2C_WRITE)
                ->SetDeviceId(THERMAL_CAMERA_DEVICE)
                ->SetNumberOfBytes(2)
                ->SetArray((uint8_t *) arr)
                ->SetSlaveAddress(this->device_address_);

        I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));
    }
}

vector<short> ThermalCamera::
getVddCompGradMatrix(vector<std::string> eeprom_raw_data) {
    vector<short> arr;
    short matrix[256];
    int rowt = 0, rowb = 7, col = 0, j = 0x0340;

    for (int i = 0; i < 256; i++) {
        if (i < 128) {
            matrix[i] = ((uint8_t) std::stoi(eeprom_raw_data.at(j + rowt * 64 + col))) +
                        (((uint8_t) std::stoi(eeprom_raw_data.at(j + rowt * 64 + col + 1))) << 8);
            col += 2;
            if (col > 63) {
                col = 0;
                rowt += 1;
            }
        } else {
            matrix[i] = ((uint8_t) std::stoi(eeprom_raw_data.at(j + rowb * 64 + col))) +
                        (((uint8_t) std::stoi(eeprom_raw_data.at(j + rowb * 64 + col + 1))) << 8);
            col += 2;
            if (col > 63) {
                rowb -= 1;
                col = 0;
            }
        }
    }
    for (int i = 0; i < 256; i++) {
        arr.push_back(matrix[i]);
    }
    return arr;
}


vector<short> ThermalCamera::
getVddCompOffMatrix(vector<std::string> eeprom_raw_data) {
    short matrix[256];
    vector<short> arr;
    int rowt = 0, rowb = 7, col = 0, j = 0x0540;

    for (int i = 0; i < 256; i++) {
        if (i < 128) {
            matrix[i] = ((uint8_t) std::stoi(eeprom_raw_data.at(j + rowt * 64 + col))) +
                        (((uint8_t) std::stoi(eeprom_raw_data.at(j + rowt * 64 + col + 1))) << 8);
            col += 2;
            if (col > 63) {
                col = 0;
                rowt += 1;
            }
        } else {
            matrix[i] = ((uint8_t) std::stoi(eeprom_raw_data.at(j + rowb * 64 + col))) +
                        (((uint8_t) std::stoi(eeprom_raw_data.at(j + rowb * 64 + col + 1))) << 8);
            col += 2;
            if (col > 63) {
                rowb -= 1;
                col = 0;
            }
        }
    }

    for (int i = 0; i < 256; i++) {
        arr.push_back(matrix[i]);
    }
    return arr;
}


vector<short> ThermalCamera::getThhGradMatrix(vector<std::string> eeprom_raw_data) {
    short matrix[32][32];
    vector<short> vec;
    int base = 0x0740;
    int rowt = 0, rowb = 31, col = 0;
    for (int i = 0; i < 32; i++) {
        for (int j = 0; j < 32; j++) {
            if (i < 16) {
                matrix[i][j] =
                        ((uint8_t) std::stoi(eeprom_raw_data.at(base + (rowt * 64) + col))) +
                        (((uint8_t) std::stoi(eeprom_raw_data.at(base + (rowt * 64) + col + 1)))
                                << 8);
                col += 2;
                if (col > 63) {
                    rowt += 1;
                    col = 0;
                }
            } else {
                matrix[i][j] =
                        ((uint8_t) std::stoi(eeprom_raw_data.at(base + (rowb * 64) + col))) +
                        (((uint8_t) std::stoi(eeprom_raw_data.at(base + (rowb * 64) + col + 1)))
                                << 8);
                col += 2;
                if (col > 63) {
                    rowb -= 1;
                    col = 0;
                }
            }
        }
    }


    for (int i = 0; i < 32; i++) {
        for (int j = 0; j < 32; j++) {
            vec.push_back(matrix[i][j]);
        }
    }
    return vec;
}

vector<short> ThermalCamera::getThoffsetMatrix(vector<std::string> eeprom_raw_data) {
    short matrix[32][32];
    vector<short> vec;
    int base = 0x0f40;
    int rowt = 0, rowb = 31, col = 0;
    for (int i = 0; i < 32; i++) {
        for (int j = 0; j < 32; j++) {
            if (i < 16) {
                matrix[i][j] =
                        ((uint8_t) std::stoi(eeprom_raw_data.at(base + (rowt * 64) + col))) +
                        (((uint8_t) std::stoi(eeprom_raw_data.at(base + (rowt * 64) + col + 1)))
                                << 8);
                col += 2;
                if (col > 63) {
                    rowt += 1;
                    col = 0;
                }
            } else {
                matrix[i][j] =
                        ((uint8_t) std::stoi(eeprom_raw_data.at(base + (rowb * 64) + col))) +
                        (((uint8_t) std::stoi(eeprom_raw_data.at(base + (rowb * 64) + col + 1)))
                                << 8);
                col += 2;
                if (col > 63) {
                    rowb -= 1;
                    col = 0;
                }
            }
        }
    }

    for (int i = 0; i < 32; i++) {
        for (int j = 0; j < 32; j++) {
            vec.push_back(matrix[i][j]);
            __android_log_print(ANDROID_LOG_ERROR, "thrml", "index %d  value %d ", i*32+j,vec.at(i*32+j));
        }
    }
    return vec;
}


vector<int> ThermalCamera::getPixelMatrix(vector<std::string> eeprom_raw_data) {
    int matrix[32][32];
    vector<int> vec;
    int base = 0x1740;
    int rowt = 0, rowb = 31, col = 0;
    for (int i = 0; i < 32; i++) {
        for (int j = 0; j < 32; j++) {
            if (i < 16) {
                matrix[i][j] = std::stoi(eeprom_raw_data.at(base + (rowt * 64) + col)) +
                               std::stoi(eeprom_raw_data.at(base + (rowt * 64) + col + 1)) * 256;
                col += 2;
                if (col > 63) {
                    rowt += 1;
                    col = 0;
                }
            } else {
                matrix[i][j] = std::stoi(eeprom_raw_data.at(base + (rowb * 64) + col)) +
                               std::stoi(eeprom_raw_data.at(base + (rowb * 64) + col + 1)) * 256;
                col += 2;
                if (col > 63) {
                    rowb -= 1;
                    col = 0;
                }
            }
        }
    }

    for (int i = 0; i < 32; i++) {
        for (int j = 0; j < 32; j++) {
            vec.push_back(matrix[i][j]);
        }
    }
    return vec;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
ThermalCamera::ProcessRequest(const ComponentRequest &component_request) {

    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;
    int raw_data[2580];
    int raw_data_index = 0;
    uint8_t top_block[258];
    uint8_t bottom_block[258];

    for (int i = 0; i < 5; i++) {
        uint8_t tx_buff[2] = {0x01, READ[i]};
        I2CRequestBuilder i2c_request_builder;
        i2c_request_builder.i2c_request_
                ->SetAction(I2C_WRITE)
                ->SetDeviceId(THERMAL_CAMERA_DEVICE)
                ->SetNumberOfBytes(2)
                ->SetArray(tx_buff)
                ->SetSlaveAddress(this->device_address_);

        I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));

        usleep(30000);

        I2CRequestBuilder i2c_request_builder1;
        i2c_request_builder1.i2c_request_
                ->SetAction(I2C_REGISTER_READ)
                ->SetDeviceId(THERMAL_CAMERA_DEVICE)
                ->SetNumberOfBytes(258)
                ->SetArray(top_block)
                ->SetRegisterAddress(0x0A)
                ->SetSlaveAddress(this->device_address_);

        I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder1.i2c_request_));

        for (int i = 0; i < 258; i++) {
            raw_data[raw_data_index++] = (int) top_block[i];
        }

        I2CRequestBuilder i2c_request_builder2;
        i2c_request_builder2.i2c_request_
                ->SetAction(I2C_REGISTER_READ)
                ->SetDeviceId(THERMAL_CAMERA_DEVICE)
                ->SetNumberOfBytes(258)
                ->SetArray(bottom_block)
                ->SetRegisterAddress(0x0B)
                ->SetSlaveAddress(this->device_address_);

        I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder2.i2c_request_));

        for (int i = 0; i < 258; i++) {
            raw_data[raw_data_index++] = (int) bottom_block[i];
        }

    }

    std::vector<double> matrix = temperature(raw_data,thermal_eeprom_data);
    for(double mat_element:matrix){
        values.push_back(std::to_string(mat_element));
    }
    response_map.emplace(SENSOR_VALUE,values);
    return response_map;
}

void ThermalCamera::InitThermalCamera() {
    if (is_camera_initialized_ == false) {
        is_camera_initialized_ = true;
        this->ReadEeprom();
        this->WakeUp();
        this->Calibrate();
        this->WakeUp();
    }
}