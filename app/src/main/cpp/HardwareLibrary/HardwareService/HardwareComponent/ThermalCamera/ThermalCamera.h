//
// Created by fedal on 21/8/21.
//

#ifndef RECIPEENGINE_THERMALCAMERA_H
#define RECIPEENGINE_THERMALCAMERA_H


#include "thermalcameradriver_new.cpp"

class ThermalCamera : public StandaloneComponent {
private:
    void ReadEeprom();

    void WakeUp();

    void Calibrate();

    short *vdd_comp_grad;
    short *vdd_comp_off;
    short *th_grad;
    short *th_offset;
    int *p_matrix;
    const int pc_scale_val = 100000000L;

    const uint8_t device_address_ = 0x1A;

    ThermalEepromData thermal_eeprom_data;

    const uint8_t CALIBRATIONS[7][2] = {
            {0x03, 0x2C},
            {0x04, 0x05},
            {0x05, 0x05},
            {0x06, 0x15},
            {0x07, 0x03},
            {0x08, 0x03},
            {0x09, 0x88}
    };

    bool is_camera_initialized_ = false;

    const uint8_t READ[5] = {0x09, 0x19, 0x29, 0x39, 0x0F};

    vector<int> getPixelMatrix(vector<std::string> eeprom_raw_data);

    vector<short> getThoffsetMatrix(vector<std::string> eeprom_raw_data);

    vector<short> getThhGradMatrix(vector<std::string> eeprom_raw_data);

    vector<short> getVddCompOffMatrix(vector<std::string> eeprom_raw_data);

    vector<short> getVddCompGradMatrix(vector<std::string> eeprom_raw_values);

    //vector<int> getVddCompGradMatrix(vector<std::string> eeprom_raw_values);

public:
    ThermalCamera(Component_e component) : StandaloneComponent(component) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);

    void InitThermalCamera();

    ~ThermalCamera() {
        if (vdd_comp_grad != NULL) {
            free(vdd_comp_grad);
        }
        if (vdd_comp_off != NULL) {
            free(vdd_comp_off);
        }
        if (th_grad != NULL) {
            free(th_grad);
        }
        if (th_offset != NULL) {
            free(th_offset);
        }
        if (p_matrix != NULL) {
            free(p_matrix);
        }
    };
};


#endif //RECIPEENGINE_THERMALCAMERA_H
