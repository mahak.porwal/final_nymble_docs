//
// Created by fedal on 23/8/21.
//

#ifndef RECIPEENGINE_DATA_H
#define RECIPEENGINE_DATA_H
int gradscale;
int vddscgrad;
int vddscoff;
int pcscaleval;

float pixc_min;
float pixc_max;
float ptat_th1;
float ptat_th2;
float vdd_th1;
float vdd_th2;

double ptat_grad;
double ptat_offset;

float globalgain;

short *vddcompgrad; // [256]
short *vddcompoff; // [256]

short *thgrad; // 32x32
short *thoffset; // 32x32
int *pmatrix; // 32x32

void setIntData(int, int, int, int);

// Set Float data
void setFloatData(float, float, float, float, float, float, float);

// Set Double data
void setDoubleData(double, double);

// Set Array data

void setArrayData(short[256], short[256]);

// Set Matrix data
void setMatrixData(short[1024], short[1024], int[1024]);
#endif //RECIPEENGINE_DATA_H
