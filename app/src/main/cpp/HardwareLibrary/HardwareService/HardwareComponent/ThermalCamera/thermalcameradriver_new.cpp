#include <android/log.h>
#include"ThermalEepromData.h"
#include"lookup90x90.hpp"
#include"thermalcameradriver.hpp"

/********************************************************************************************************
 *
 *                         Invoke Temperature() for the image as well as temp matrix
 *
 *********************************************************************************************************/
using namespace std;

/**
 * @brief function for concatinating two bytes using little endian
 *
 * @param a - first byte
 * @param b - second byte
 * @return int
 */
int lend(int a, int b) {
    int x;
    b = b << 8;
    x = a | b;
    return x;
}

/**
 * @brief function to find the sum of all the elements in an array a of known
 * size
 *
 * @param a
 * @param size
 * @return int
 */
int sum(int *a, int size) {
    int i;
    int sum = 0;
    size = size / sizeof(a[0]);
    for (i = 0; i < size; i++)
        sum = sum + a[i];
    return sum;
}

/**
 * @brief lookup function uses the lookup table to interpolate to the
 * teperature value using the ambient temperature and v_pix_c vaue.
 * lookuptab - declared in lookup.hpp is sensor type dependent and the one
 * in the header is for heimann 16X16 sensor.
 *
 * @param v_pixc_1
 * @param ta
 * @return float
 */
float lookup(double v_pixc_1, int ta) {
    int i, j, x = -1, y = -1;
    static int failcnt_x = 0;
    static int failcnt_y = 0;
    float x_1, x_2, temp_2, temp_1;
    // 1)Searching for the position of v_pixc_1 in the first column (x).

    for (int i = 0; i < 1595; i++)
        if ((lookuptab[i][0] < v_pixc_1) && (lookuptab[i + 1][0] > v_pixc_1))
            x = i;
    if (x == -1) {
        //__android_log_print(ANDROID_LOG_ERROR, "thrml Fail x", "%d", ++failcnt_x);
        return -5;
    }

    // 2)Searching for the position of ta in the first column (y).
    for (int j = 0; j < NOOFADELEMENTS; j++)
        if ((lookuptab[0][j] <= ta) && (lookuptab[0][j + 1] > ta))
            y = j;

    if (y == -1) {
        //__android_log_print(ANDROID_LOG_ERROR, "thrml Fail y", "%d", ++failcnt_y);
        return -5;
    }
    // 3)Finding the interpolated value using the values obtained in the table.
    x_1 = (v_pixc_1 - lookuptab[x][0]) * (lookuptab[x + 1][y] - lookuptab[x][y]) /
          (lookuptab[x + 1][0] - lookuptab[x][0]) +
          lookuptab[x][y];

    x_2 = (v_pixc_1 - lookuptab[x][0]) *
          (lookuptab[x + 1][y + 1] - lookuptab[x][y + 1]) /
          (lookuptab[x + 1][0] - lookuptab[x][0]) +
          lookuptab[x][y + 1];

    temp_2 = float(ta - lookuptab[0][y]) /
             (lookuptab[0][y + 1] - lookuptab[0][y]) * (x_2 - x_1) +
             x_1;
    temp_1 = temp_2 / 10 - 273.14;
    return temp_1;
}

/**
 * @brief function to calculate sensor data by processing the raw data
 *
 * @param b - pointer to the raw data accquired by the sensor
 * @param sen - 2D array which stores the raw sensor values, value being sent inside
 * @param ptat - Proportional to Absolute Temperature, (Value needed to calculate temperature of a body)
 */
void sensor(int *b, int sen[32][32], int ptat[8]) {
    int i, j;
    int v1[32][64];
    int rowt = 0;
    int rowb = 31;
    int column = 0;
    for (i = 0; i < 2064; i++) {
        if (i % 258 == 0)
            ptat[int(i / 258)] = lend(b[i + 1], b[i]);
        if ((i > 1 && i < 258) || (i > 517 && i < 774) || (i > 1033 && i < 1290) ||
            (i > 1549 && i < 1806)) {
            v1[rowt][column] = b[i];
            column = column + 1;
            if (column > 63) {
                column = 0;
                rowt = rowt + 1;
            }
        }
        if ((i > 259 && i < 516) || (i > 775 && i < 1032) || (i > 1291 && i < 1548) ||
            (i > 1807 && i < 2064)) {
            v1[rowb][column] = b[i];
            column = column + 1;
            if (column > 63) {
                column = 0;
                rowb = rowb - 1;
            }
        }
    }
    for (i = 0; i < 32; i++) {
        for (j = 0; j < 64; j = j + 2) {
            sen[i][j / 2] = lend(v1[i][j + 1], v1[i][j]);
        }
    }
}

/**
 * @brief function to calculate electrical offset data by processing the raw
 * data
 *
 * @param b - pointer to the raw data accquired by the sensor
 * @param vdd
 * @param eloffset
 */
void elec_off(int *b, int vdd[2], int elel[256]) {
    int rowt = 0;
    int rowb = 7;
    int column = 0;
    int eloffset_1[8][32], el[8][64];
    int i, j;

    for (i = 2064; i < 2580; i++) {
        if (i % 258 == 0) {
            vdd[int((i - 2064) / 258)] = lend(b[i + 1], b[i]);
        }
        if (i > 2065 && i < 2322) {
            el[rowt][column] = b[i];
            column = column + 1;
            if (column > 63) {
                column = 0;
                rowt = rowt + 1;
            }
        }
        if (i > 2323 && i < 2580) {
            el[rowb][column] = b[i];
            column = column + 1;
            if (column > 63) {
                column = 0;
                rowb = rowb - 1;
            }
        }
    }

    for (i = 0; i < 8; i++)
        for (j = 0; j < 64; j = j + 2)
            eloffset_1[i][j / 2] = lend(el[i][j + 1], el[i][j]);

    int row = 0;
    int col = 0;
    for (i = 0; i < 256; i++) {
        elel[i] = eloffset_1[row][col];
        col++;
        if (col > 31) {
            col = 0;
            row++;
        }
    }

}

/**
 * @brief function doing the main calculations to convert the raw sensor values
 * into final temperature matrix.
 *
 * @return std::vector<double>
 */
std::vector<double> temperature(int raw_values[2580], ThermalEepromData thermal_eeprom_data) {
    int i, j;
    int pixel_data[32][32], ptat[8], vdd[2], eloffset[256];
    double v_thermal_offset[32][32], v_electrical_offset[32][32], v_vdd_comp[32][32], pixc[32][32], v_pixc[32][32], a, b[32][32], c[32][32], d[32][32];
    vector<double> result;
    double thgrad2D[32][32], thOffset2D[32][32], pMatrix2D[32][32];

    for (int i = 0; i < 1024; i++) {
        thgrad2D[i / 32][i % 32] = thermal_eeprom_data.getThgrad()[i];
        thOffset2D[i / 32][i % 32] = thermal_eeprom_data.getThoffset()[i];
        pMatrix2D[i / 32][i % 32] = thermal_eeprom_data.getPmatrix()[i];
    }

    // 2)Obtaining the sensor values and electrical offsets
    sensor(raw_values, pixel_data, ptat);
    elec_off(raw_values, vdd, eloffset);

    float ptat_av = sum(ptat, sizeof(ptat)) / 8.0;
    float vdd_av = sum(vdd, sizeof(vdd)) / 2.0;

    // 3)Ambient temperature calculation
    float ambient_temp =
            ptat_av * thermal_eeprom_data.getPtatGrad() + thermal_eeprom_data.getPtatOffset();
    int index = 0;
    for (i = 0; i < 32; i++) {
        for (j = 0; j < 32; j++) {
            // 4)Thermal offset calculation
            v_thermal_offset[i][j] =
                    pixel_data[i][j] -
                    (thgrad2D[i][j] * ptat_av) / (pow(2.0, thermal_eeprom_data.getGradscale())) -
                    thOffset2D[i][j];
            if (i < 16) {
                // 5)Electrical offset calculation
                v_electrical_offset[i][j] = v_thermal_offset[i][j] - eloffset[(j + i * 32) % 128];

                // 6)Vdd compensation calculation

                double electrical_and_thermal_compensated_voltage = v_electrical_offset[i][j];
                short vdd_comp_gradient = thermal_eeprom_data.getVddcompgrad()[(j + i * 32) % 128];
                short vdd_comp_offset = thermal_eeprom_data.getVddcompoff()[(j + i * 32) % 128];
                int vdd_gradient_scaling_coefficient = thermal_eeprom_data.getVddscgrad();
                int vdd_offset_scaling_coefficient = thermal_eeprom_data.getVddscoff();
                float vdd_calibration_1 = thermal_eeprom_data.getVddTh1();
                float vdd_calibration_2 = thermal_eeprom_data.getVddTh2();
                float ptat_calibration_1 = thermal_eeprom_data.getPtatTh1();
                float ptat_calibration_2 = thermal_eeprom_data.getPtatTh2();


                double vdd_ptat_grad = (vdd_calibration_2 - vdd_calibration_1) /
                                       (ptat_calibration_2 - ptat_calibration_1);

                double numerator =
                        ((vdd_comp_gradient * ptat_av) / pow(2, vdd_gradient_scaling_coefficient)) +
                        vdd_comp_offset;
                double denominator = pow(2, vdd_offset_scaling_coefficient);

                double deduction = (numerator / denominator) * (vdd_av - vdd_calibration_1 -
                                                                ((vdd_ptat_grad) *
                                                                 (ptat_av - ptat_calibration_1)));

                v_vdd_comp[i][j] = electrical_and_thermal_compensated_voltage - deduction;

            } else if (i > 15) {
                // 5)Electrical offset calculation
                v_electrical_offset[i][j] =
                        v_thermal_offset[i][j] - eloffset[((j + i * 32) % 128) + 128];

                // 6)Vdd compensation calculation
                double electrical_and_thermal_compensated_voltage = v_electrical_offset[i][j];
                short vdd_comp_gradient = thermal_eeprom_data.getVddcompgrad()[
                ((j + i * 32) % 128) + 128];
                short vdd_comp_offset = thermal_eeprom_data.getVddcompoff()[((j + i * 32) % 128) +
                                                                            128];
                int vdd_gradient_scaling_coefficient = thermal_eeprom_data.getVddscgrad();
                int vdd_offset_scaling_coefficient = thermal_eeprom_data.getVddscoff();
                float vdd_calibration_1 = thermal_eeprom_data.getVddTh1();
                float vdd_calibration_2 = thermal_eeprom_data.getVddTh2();
                float ptat_calibration_1 = thermal_eeprom_data.getPtatTh1();
                float ptat_calibration_2 = thermal_eeprom_data.getPtatTh2();


                double vdd_ptat_grad = (vdd_calibration_2 - vdd_calibration_1) /
                                       (ptat_calibration_2 - ptat_calibration_1);

                double numerator =
                        ((vdd_comp_gradient * ptat_av) / pow(2, vdd_gradient_scaling_coefficient)) +
                        vdd_comp_offset;
                double denominator = pow(2, vdd_offset_scaling_coefficient);

                double deduction = (numerator / denominator) * (vdd_av - vdd_calibration_1 -
                                                                ((vdd_ptat_grad) *
                                                                 (ptat_av - ptat_calibration_1)));

                v_vdd_comp[i][j] = electrical_and_thermal_compensated_voltage - deduction;
            }
            usleep(20);

            // 7)Object temperature calculation
            a = thermal_eeprom_data.getPixcMax() - thermal_eeprom_data.getPixcMin();
            b[i][j] = pMatrix2D[i][j] * a;
            c[i][j] = b[i][j] / 65535;
            d[i][j] = c[i][j] + thermal_eeprom_data.getPixcMin();
            pixc[i][j] = d[i][j] * (thermal_eeprom_data.getGlobalgain() / 10000);
            //	pixc[i][j] = (((p[i][j] * (pixc_max-pixc_min))/65535) + pixc_min) * (epsilon/100) * (globalgain/10000);
            v_pixc[i][j] = (v_vdd_comp[i][j] * thermal_eeprom_data.getPcscaleval()) / pixc[i][j];
            //__android_log_print(ANDROID_LOG_ERROR, "test-thermal", "%f", v_pixc[i][j]);
            // 8)Lookup table and final temperature matrix calculation
            double pixel_temperature = lookup(v_pixc[i][j], ambient_temp);
            if (pixel_temperature <= -5 || pixel_temperature >= 300) {
                /* for (int ii = 0; i < 1024; i++) {
                     result.push_back(0);
                 }
                 return result;*/
                result.push_back(pixel_temperature);
                __android_log_print(ANDROID_LOG_ERROR, "thrml", "%f", v_pixc[i][j]);
            } else {
                result.push_back(pixel_temperature);
            }
        }
    }
    for (int i = 0; i < 1024; i++) {
        if (i % 32 == 0) {
            printf("\n");
        }
        printf("%.2lf ", result.at(i));
    }

    return result;

}
