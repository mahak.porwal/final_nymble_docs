//
// Created by fedal on 28/12/20.
//

#ifndef JULIA_ANDROID_INPUTCURRENT_H
#define JULIA_ANDROID_INPUTCURRENT_H


class InputCurrent : public AnalogFeedback, public SubsystemComponent {
private:
    float a, b, c;

    float CurrentAmperes(float voltage);

    double PerformAdcRequest(const ComponentRequest &);

public:
    InputCurrent(Component_e hardware_component, Subsystem subsystem) :
            SubsystemComponent(hardware_component, subsystem) {
        this->a = -0.116;
        this->b = 3.63;
        this->c = 0.155;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_INPUTCURRENT_H
