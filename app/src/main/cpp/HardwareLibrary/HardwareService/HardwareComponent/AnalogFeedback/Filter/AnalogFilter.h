//
// Created by fedal on 23/7/21.
//

#ifndef RECIPEENGINE_ANALOGFILTER_H
#define RECIPEENGINE_ANALOGFILTER_H


class AnalogFilter {
public:
    virtual void SetAnalogValue(int raw_value) = 0;

    virtual int GetFilteredValue() = 0;

    virtual ~AnalogFilter() {}
};


#endif //RECIPEENGINE_ANALOGFILTER_H
