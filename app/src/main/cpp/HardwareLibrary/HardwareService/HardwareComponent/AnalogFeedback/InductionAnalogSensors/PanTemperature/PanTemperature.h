//
// Created by fedal on 28/12/20.
//

#ifndef JULIA_ANDROID_PANTEMPERATURE_H
#define JULIA_ANDROID_PANTEMPERATURE_H


class PanTemperature : public AnalogFeedback, public SubsystemComponent {
private:
    float vcc;
    float a1, a2;
    float b1, b2;

    float TemperatureCelsius(float voltage);

    double PerformAdcRequest(const ComponentRequest &);

public:
    double PerformRequest(ComponentRequest *);

    PanTemperature(Component_e hardware_component, Subsystem subsystem) :
            SubsystemComponent(hardware_component, subsystem) {
        this->vcc = 5.0;
        this->a1 = 23;
        this->b1 = 132;
        this->a2 = 32.7;
        this->b2 = 162;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_PANTEMPERATURE_H
