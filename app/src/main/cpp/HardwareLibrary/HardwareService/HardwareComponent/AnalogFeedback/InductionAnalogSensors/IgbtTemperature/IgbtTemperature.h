//
// Created by fedal on 28/12/20.
//

#ifndef JULIA_ANDROID_IGBTTEMPERATURE_H
#define JULIA_ANDROID_IGBTTEMPERATURE_H


#include "../../AnalogFeedback.h"

class IgbtTemperature : public AnalogFeedback, public SubsystemComponent {
private:
    float vcc;
    float a;
    float b;

    float TemperatureCelsius(float voltage);

    double PerformAdcRequest(const ComponentRequest &);

public:
    IgbtTemperature(Component_e hardware_subsystem, Subsystem subsystem) :
            SubsystemComponent(hardware_subsystem, subsystem) {
        this->vcc = 5.0;
        this->a = 25.5;
        this->b = 84.7;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);

};


#endif //JULIA_ANDROID_IGBTTEMPERATURE_H
