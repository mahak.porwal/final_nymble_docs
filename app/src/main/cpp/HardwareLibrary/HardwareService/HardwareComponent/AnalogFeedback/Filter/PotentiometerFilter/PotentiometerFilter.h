//
// Created by fedal on 23/7/21.
//

#ifndef RECIPEENGINE_POTENTIOMETERFILTER_H
#define RECIPEENGINE_POTENTIOMETERFILTER_H

#include "../AnalogFilter.h"

class PotentiometerFilter : public AnalogFilter {
public :
    void SetAnalogValue(int raw_value);

    int GetFilteredValue();

    ~PotentiometerFilter() {}
};


#endif //RECIPEENGINE_POTENTIOMETERFILTER_H
