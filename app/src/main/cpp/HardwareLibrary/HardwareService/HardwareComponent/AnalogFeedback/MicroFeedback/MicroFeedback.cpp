//
// Created by fedal on 21/12/20.
//

#include "MicroFeedback.h"

std::mutex MicroFeedback::micro_analog_mux_lock_;

double MicroFeedback::PerformAdcRequest(const ComponentRequest &component_request) {
    AdcRequestBuilder *adc_request_builder = new AdcRequestBuilder;
    ComponentRequestBuilder *hardware_request_builder = new ComponentRequestBuilder;
    double result = -1;
    adc_request_builder->adc_request->SetHardwareComponent(this->GetComponentId());

    std::lock_guard<std::mutex> micro_mux_mutex_guard(micro_analog_mux_lock_);

    hardware_request_builder->component_request_.SetComponent(MICRO_MUX).SetMuxSelect(
            this->GetComponentId());
    HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(
            hardware_request_builder->component_request_);

    hardware_request_builder->component_request_.SetComponent(MACRO_MICRO_MUX).SetMuxSelect(
            SELECT_MICRO_ADC);
    HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(
            hardware_request_builder->component_request_);

    //usleep(10000);

    result = AdcRequestProcessor::ProcessAdcRequest(*adc_request_builder->adc_request);

    if(abs(result - last_reading_) <= 1){
        filtered_value_ = result;
    }
    last_reading_ = result;

    switch (component_request.GetSensorUnit()) {
        case ANGLE_VALUE:
            result = MilliVoltsToAngle(filtered_value_);
            break;
        default:
            break;
    }

    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                        "pod filtered reading %f",
                        result);

    delete hardware_request_builder;
    delete adc_request_builder;
    return result;
}

double MicroFeedback::MilliVoltsToAngle(double milli_volts) {
    double angle = DEGREE_PER_MILLI_VOLT * milli_volts;
    return angle;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
MicroFeedback::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    double analog_value = this->PerformAdcRequest(component_request);
    std::vector<string> values;
    values.push_back(std::to_string(analog_value));
    response_map.emplace(SENSOR_VALUE, values);
    return response_map;
}

