//
// Created by fedal on 06/10/21.
//

#include "TemperatureFeedback.h"

double TemperatureFeedback::PerformAdcRequest(const ComponentRequest &component_request) {
    double reading = -1;
    AdcRequestBuilder *adcRequestBuilder = new AdcRequestBuilder;
    adcRequestBuilder->adc_request->SetHardwareComponent(this->GetComponentId());
    reading = HardwareService::adc_request_processor_->ProcessAdcRequest(
            *adcRequestBuilder->adc_request);
    return reading;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
TemperatureFeedback::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    double analog_value = this->PerformAdcRequest(component_request);
    std::vector<string> values;
    values.push_back(std::to_string(analog_value));
    response_map.emplace(SENSOR_VALUE, values);
    return response_map;
}
