//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_ANALOGFEEDBACK_H
#define JULIA_ANDROID_ANALOGFEEDBACK_H


class AnalogFeedback {
protected:
    virtual double PerformAdcRequest(const ComponentRequest&);
};


#endif //JULIA_ANDROID_ANALOGFEEDBACK_H
