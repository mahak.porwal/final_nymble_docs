//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_STIRRERCURRENT_H
#define JULIA_ANDROID_STIRRERCURRENT_H


class StirrerCurrent : public AnalogFeedback, public SubsystemComponent {

private:
    double PerformAdcRequest(const ComponentRequest &);

public:
    StirrerCurrent(Component_e hardware_component, Subsystem subsystem) :
            SubsystemComponent(hardware_component, subsystem) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_STIRRERCURRENT_H
