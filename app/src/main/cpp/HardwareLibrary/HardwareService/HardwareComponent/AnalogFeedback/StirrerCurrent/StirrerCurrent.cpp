//
// Created by fedal on 21/12/20.
//

#include "StirrerCurrent.h"

double StirrerCurrent::PerformAdcRequest(const ComponentRequest &componentRequest) {

    /**
     * SOM Adc
     */

    AdcRequestBuilder adc_request_builder;
    float result = -1;
    adc_request_builder.adc_request->SetHardwareComponent(this->GetComponentId());
    result = AdcRequestProcessor::ProcessAdcRequest(*adc_request_builder.adc_request);
    return result;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
StirrerCurrent::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    double analog_value = this->PerformAdcRequest(component_request);
    std::vector<string> values;
    values.push_back(std::to_string(analog_value));
    response_map.emplace(SENSOR_VALUE, values);
    return response_map;
}
