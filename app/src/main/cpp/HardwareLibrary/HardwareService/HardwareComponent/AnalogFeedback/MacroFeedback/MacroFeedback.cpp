//
// Created by fedal on 21/12/20.
//

#include "MacroFeedback.h"

double MacroFeedback::PerformAdcRequest(const ComponentRequest &componentRequest) {
    AdcRequestBuilder *adc_request_builder = new AdcRequestBuilder;
    ComponentRequestBuilder *hardware_request_builder = new ComponentRequestBuilder;
    int result = -1;
    adc_request_builder->adc_request->SetHardwareComponent(this->GetComponentId());
    hardware_request_builder->component_request_.SetComponent(MACRO_MUX).SetMuxSelect(
            this->GetComponentId());

    HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(
            hardware_request_builder->component_request_);

    hardware_request_builder->component_request_.SetComponent(MACRO_MICRO_MUX).SetMuxSelect(
            SELECT_MACRO_ADC);
    HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(
            hardware_request_builder->component_request_);

    usleep(10000);

    result = AdcRequestProcessor::ProcessAdcRequest(*adc_request_builder->adc_request);
    delete hardware_request_builder;
    delete adc_request_builder;
    return result;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
MacroFeedback::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    double analog_value = this->PerformAdcRequest(component_request);
    std::vector<string> values;
    values.push_back(std::to_string(analog_value));
    response_map.emplace(SENSOR_VALUE, values);
    return response_map;
}

