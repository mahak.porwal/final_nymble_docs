//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_MACROFEEDBACK_H
#define JULIA_ANDROID_MACROFEEDBACK_H

#define MACRO_ADC_TO_ANGLE(X) (-0.0522*X + 196)
class MacroFeedback : public AnalogFeedback, public SubsystemComponent {
private:
    double PerformAdcRequest(const ComponentRequest &);
public:
    MacroFeedback(Component_e hardware_component, Subsystem subsystem) :
            SubsystemComponent(hardware_component, subsystem) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_MACROFEEDBACK_H
