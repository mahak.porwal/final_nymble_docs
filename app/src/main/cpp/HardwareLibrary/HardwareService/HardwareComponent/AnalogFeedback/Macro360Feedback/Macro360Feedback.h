//
// Created by fedal on 25/11/21.
//

#ifndef RECIPEENGINE_MACRO360FEEDBACK_H
#define RECIPEENGINE_MACRO360FEEDBACK_H


class Macro360Feedback  : public AnalogFeedback, public SubsystemComponent {
private:
    double PerformAdcRequest(const ComponentRequest &);

    double MilliVoltsToAngle(double);

    double last_reading_ = 0.0;

    double filtered_value_;

    static double DEGREE_PER_MILLI_VOLT;
public:
    Macro360Feedback(Component_e hardware_component, Subsystem subsystem) :
    SubsystemComponent(hardware_component, subsystem) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};
double Macro360Feedback::DEGREE_PER_MILLI_VOLT = 360.0/1024;

#endif //RECIPEENGINE_MACRO360FEEDBACK_H
