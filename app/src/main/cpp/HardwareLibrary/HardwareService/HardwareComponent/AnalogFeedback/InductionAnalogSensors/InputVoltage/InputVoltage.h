//
// Created by fedal on 28/12/20.
//

#ifndef JULIA_ANDROID_INPUTVOLTAGE_H
#define JULIA_ANDROID_INPUTVOLTAGE_H


class InputVoltage : public AnalogFeedback, public SubsystemComponent {
private:
    float a, b;

    float Voltage(float voltage);

    double PerformAdcRequest(const ComponentRequest &);

public:
    InputVoltage(Component_e hardware_component, Subsystem subsystem) :
            SubsystemComponent(hardware_component, subsystem) {
        this->a = 40.8;
        this->b = 1.64;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_INPUTVOLTAGE_H
