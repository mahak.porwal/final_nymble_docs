//
// Created by fedal on 21/12/20.
//

#include "AnalogFeedback.h"

double AnalogFeedback::PerformAdcRequest(const ComponentRequest &componentRequest) {
    double reading = -1;
    AdcRequestBuilder *adcRequestBuilder = new AdcRequestBuilder;
    adcRequestBuilder->adc_request->SetHardwareComponent(componentRequest.GetComponent());
    reading = HardwareService::adc_request_processor_->ProcessAdcRequest(
            *adcRequestBuilder->adc_request);
    return reading;
}
