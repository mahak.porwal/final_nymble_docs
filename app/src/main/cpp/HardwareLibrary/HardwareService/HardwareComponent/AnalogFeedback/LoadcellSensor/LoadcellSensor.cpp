//
// Created by fedal on 21/12/20.
//

#include "LoadcellSensor.h"

double LoadcellSensor::PerformAdcRequest(const ComponentRequest &componentRequest) {
    float result = -1;
    AdcRequestBuilder *adcRequestBuilder = new AdcRequestBuilder;
    adcRequestBuilder->adc_request->SetHardwareComponent(LOADCELL_SENSOR);
    result = AdcRequestProcessor::ProcessAdcRequest(*adcRequestBuilder->adc_request);
    switch (componentRequest.GetSensorUnit()) {
        case MICRO_VOLTS: {
            double adc_factor, microvolts;
            adcRequestBuilder->adc_request->SetAdcRequestType(REFERENCE_REQUEST);
            adc_factor = AdcRequestProcessor::ProcessAdcRequest(*adcRequestBuilder->adc_request);
            microvolts = result * adc_factor * 1000000;
            return microvolts;
        }
        case RAW_VALUE: {
            return result;
        }
        default:
        case GRAMS: {
            double grams = -1, adc_factor, millivolts;
            adcRequestBuilder->adc_request->SetAdcRequestType(REFERENCE_REQUEST);
            adc_factor = AdcRequestProcessor::ProcessAdcRequest(*adcRequestBuilder->adc_request);
            millivolts = result * adc_factor * 1000000;
            grams = millivolts / SCALING_FACTOR;
            return grams;
        }
    }
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
LoadcellSensor::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    double analog_value = this->PerformAdcRequest(component_request);
    std::vector<string> values;
    values.push_back(std::to_string(analog_value));
    response_map.emplace(SENSOR_VALUE, values);
    return response_map;
}
