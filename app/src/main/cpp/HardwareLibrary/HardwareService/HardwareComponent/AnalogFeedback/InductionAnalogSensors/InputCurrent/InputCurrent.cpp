//
// Created by fedal on 28/12/20.
//

#include "InputCurrent.h"

float InputCurrent::CurrentAmperes(float voltage) {
    float current;
    current = this->c + this->b * voltage + this->a * voltage * voltage;
    return current;
}

double InputCurrent::PerformAdcRequest(const ComponentRequest &component_request) {
    double reading = -1;
    AdcRequestBuilder *adcRequestBuilder = new AdcRequestBuilder;
    adcRequestBuilder->adc_request->SetHardwareComponent(this->GetComponentId());
    reading = HardwareService::adc_request_processor_->ProcessAdcRequest(
            *adcRequestBuilder->adc_request);
    if (component_request.GetSensorUnit() == MILLI_VOLTS) {
        return reading;
    }
    return CurrentAmperes(reading / 1000.0);
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
InputCurrent::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    double analog_value = this->PerformAdcRequest(component_request);
    std::vector<string> values;
    values.push_back(std::to_string(analog_value));
    response_map.emplace(SENSOR_VALUE, values);
    return response_map;
}
