//
// Created by fedal on 28/12/20.
//

#include "IgbtTemperature.h"

float IgbtTemperature::TemperatureCelsius(float voltage) {
    float resistance, igbt_temperature;
    if (voltage == 0)return 0;
    resistance = (this->vcc - voltage) / voltage;
    igbt_temperature = this->b - this->a * log(resistance);
    return igbt_temperature;
}

double IgbtTemperature::PerformAdcRequest(const ComponentRequest &component_request) {
    double reading = -1;
    AdcRequestBuilder *adcRequestBuilder = new AdcRequestBuilder;
    adcRequestBuilder->adc_request->SetHardwareComponent(this->GetComponentId());
    reading = HardwareService::adc_request_processor_->ProcessAdcRequest(
            *adcRequestBuilder->adc_request);
    if (component_request.GetSensorUnit() == MILLI_VOLTS) {
        return reading;
    }
    return TemperatureCelsius(reading / 1000.0);
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
IgbtTemperature::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    double analog_value = this->PerformAdcRequest(component_request);
    std::vector<string> values;
    values.push_back(std::to_string(analog_value));
    response_map.emplace(SENSOR_VALUE, values);
    return response_map;
}

