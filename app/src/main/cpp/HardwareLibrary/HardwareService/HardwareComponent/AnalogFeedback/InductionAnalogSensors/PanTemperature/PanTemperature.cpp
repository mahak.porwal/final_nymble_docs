//
// Created by fedal on 28/12/20.
//

#include "PanTemperature.h"

float PanTemperature::TemperatureCelsius(float voltage) {
    float resistance, pan_temperature;
    if (voltage == 0)return 0;
    resistance = (this->vcc - voltage) / voltage;
    if (resistance > 27.5) {
        pan_temperature = this->b1 - this->a1 * log(resistance);
    } else {
        pan_temperature = this->b2 - this->a2 * log(resistance);
    }
    return pan_temperature;
}

double PanTemperature::PerformAdcRequest(const ComponentRequest &component_request) {
    double reading = -1;
    AdcRequestBuilder *adcRequestBuilder = new AdcRequestBuilder;
    adcRequestBuilder->adc_request->SetHardwareComponent(this->GetComponentId());
    reading = HardwareService::adc_request_processor_->ProcessAdcRequest(
            *adcRequestBuilder->adc_request);
    if (component_request.GetSensorUnit() == MILLI_VOLTS) {
        return reading;
    }
    return TemperatureCelsius(reading / 1000.0);
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
PanTemperature::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    double analog_value = this->PerformAdcRequest(component_request);
    std::vector<string> values;
    values.push_back(std::to_string(analog_value));
    response_map.emplace(SENSOR_VALUE, values);
    return response_map;
}
