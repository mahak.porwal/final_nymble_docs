//
// Created by fedal on 06/10/21.
//

#ifndef RECIPEENGINE_TEMPERATUREFEEDBACK_H
#define RECIPEENGINE_TEMPERATUREFEEDBACK_H


class TemperatureFeedback : public AnalogFeedback, public StandaloneComponent {
private:
    double PerformAdcRequest(const ComponentRequest &);

public:
    TemperatureFeedback(Component_e
                        hardware_component) :
            StandaloneComponent(hardware_component) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //RECIPEENGINE_TEMPERATUREFEEDBACK_H
