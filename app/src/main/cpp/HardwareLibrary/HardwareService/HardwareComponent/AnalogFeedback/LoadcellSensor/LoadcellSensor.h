//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_LOADCELLSENSOR_H
#define JULIA_ANDROID_LOADCELLSENSOR_H


#include "../../../../Component/StandaloneComponent/StandaloneComponent.h"

class LoadcellSensor : public AnalogFeedback, public StandaloneComponent {
private:
    double PerformAdcRequest(const ComponentRequest &);

    double scaling_factor_;
public:
    static volatile double SCALING_FACTOR;

    LoadcellSensor(Component_e hardware_component, double scaling_factor) :
            StandaloneComponent(hardware_component) { scaling_factor_ = scaling_factor; }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};

volatile double LoadcellSensor::SCALING_FACTOR = -0.123;

#endif //JULIA_ANDROID_LOADCELLSENSOR_H
