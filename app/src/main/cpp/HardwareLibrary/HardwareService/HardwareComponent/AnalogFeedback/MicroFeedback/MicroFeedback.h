//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_MICROFEEDBACK_H
#define JULIA_ANDROID_MICROFEEDBACK_H


class MicroFeedback : public AnalogFeedback, public SubsystemComponent {

private:
    double PerformAdcRequest(const ComponentRequest &);

    double MilliVoltsToAngle(double);

    double last_reading_;

    double filtered_value_;

    static double DEGREE_PER_MILLI_VOLT;

    static std::mutex micro_analog_mux_lock_;
public:
    MicroFeedback(Component_e hardware_component, Subsystem subsystem)
            : SubsystemComponent(hardware_component, subsystem) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};
double MicroFeedback::DEGREE_PER_MILLI_VOLT = 360.0/1024;

#endif //JULIA_ANDROID_MICROFEEDBACK_H
