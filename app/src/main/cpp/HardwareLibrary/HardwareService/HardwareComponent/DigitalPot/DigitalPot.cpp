//
// Created by fedal on 12/10/21.
//

#include "DigitalPot.h"

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
DigitalPot::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;
    DigitalPotRequest_e request = component_request.GetDigitalPotReq();
    int result = -1;
    switch (request) {
        case READ_POT:
            result = this->GetPot();
            break;
        case WRITE_POT:
            //this->SetPot(component_request.GetSpeed());
            this->SetPotResistanceOhms(component_request.GetSpeed());
            break;
        case INCREMENT_POT:
            this->IncrementPot();
            break;
        case DECREMENT_POT:
            this->DecrementPot();
            break;
        default:
            throw new Error();
    }
    values.push_back(std::to_string(result == -1 ? 1 : result));
    response_map.emplace(COMMAND_RESULT, values);
    return response_map;
}

void DigitalPot::SetPot(uint16_t steps) {
    this->mcp4662_->SetRheostat(this->rheostats_, steps);
}

void DigitalPot::SetPotResistanceOhms(uint32_t resistance) {
    this->mcp4662_->SetRheostatResistanceOhms(this->rheostats_, resistance);
}

uint16_t DigitalPot::GetPot() {
    return this->mcp4662_->GetRheostat(this->rheostats_);
}

void DigitalPot::IncrementPot() {
    this->mcp4662_->IncrementRheostat(this->rheostats_);
}

void DigitalPot::DecrementPot() {
    this->mcp4662_->DecrementRheostat(this->rheostats_);
}
