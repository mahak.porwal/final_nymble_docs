//
// Created by fedal on 12/10/21.
//

#include "MCP4662.h"

void MCP4662::SetRheostat(Rheostats_e rheostat, uint16_t steps) {
    uint8_t command_mask = MCP4531_WRITE;
    uint8_t address_mask = 0x00;
    uint8_t steps_lower_byte = steps & 0xFFF;
    uint8_t data_bit = steps & 0x0100;
    switch (rheostat) {
        case RHEOSTAT_1:
            address_mask = 0x00;
            break;
        case RHEOSTAT_2:
            address_mask = 0x10;
            break;
    }
    uint8_t tx_buf[2];
    tx_buf[0] = command_mask | address_mask | data_bit;
    tx_buf[1] = steps_lower_byte;
    I2CRequestBuilder i2CRequestBuilder;
    i2CRequestBuilder.i2c_request_
            ->SetDeviceId(POT_MCP4662)
            ->SetAction(I2C_WRITE)
            ->SetArray(tx_buf)
            ->SetNumberOfBytes(2)
            ->SetSlaveAddress(this->device_address_);
    I2CRequestProcessor::ProcessI2cRequest(*i2CRequestBuilder.i2c_request_);
}

uint16_t MCP4662::GetRheostat(Rheostats_e rheostat) {
    uint8_t command_mask = MCP4531_READ;
    uint8_t address_mask = 0x00;
    switch (rheostat) {
        case RHEOSTAT_1:
            address_mask = 0x00;
            break;
        case RHEOSTAT_2:
            address_mask = 0x01;
            break;
    }
    uint8_t rx_buf[2];
    I2CRequestBuilder i2CRequestBuilder;
    i2CRequestBuilder.i2c_request_
            ->SetDeviceId(POT_MCP4662)
            ->SetAction(I2C_REGISTER_READ)
            ->SetRegisterAddress(command_mask | address_mask)
            ->SetArray(rx_buf)
            ->SetNumberOfBytes(2)
            ->SetSlaveAddress(this->device_address_);
    int result = I2CRequestProcessor::ProcessI2cRequest(*i2CRequestBuilder.i2c_request_);
    return (rx_buf[0] << 8) | rx_buf[1];
}

void MCP4662::IncrementRheostat(Rheostats_e rheostat) {
    uint8_t command_mask = MCP4531_INCR;
    uint8_t address_mask = 0x00;
    switch (rheostat) {
        case RHEOSTAT_1:
            address_mask = 0x00;
            break;
        case RHEOSTAT_2:
            address_mask = 0x01;
            break;
    }
    uint8_t tx_buf[1];
    tx_buf[0] = command_mask | address_mask;
    I2CRequestBuilder i2CRequestBuilder;
    i2CRequestBuilder.i2c_request_
            ->SetDeviceId(POT_MCP4662)
            ->SetAction(I2C_WRITE)
            ->SetArray(tx_buf)
            ->SetNumberOfBytes(1)
            ->SetSlaveAddress(this->device_address_);
    I2CRequestProcessor::ProcessI2cRequest(*i2CRequestBuilder.i2c_request_);
}

void MCP4662::DecrementRheostat(Rheostats_e rheostat) {
    uint8_t command_mask = MCP4531_DECR;
    uint8_t address_mask = 0x00;
    switch (rheostat) {
        case RHEOSTAT_1:
            address_mask = 0x00;
            break;
        case RHEOSTAT_2:
            address_mask = 0x01;
            break;
    }
    uint8_t tx_buf[1];
    tx_buf[0] = command_mask | address_mask;
    I2CRequestBuilder i2CRequestBuilder;
    i2CRequestBuilder.i2c_request_
            ->SetDeviceId(POT_MCP4662)
            ->SetAction(I2C_WRITE)
            ->SetArray(tx_buf)
            ->SetNumberOfBytes(1)
            ->SetSlaveAddress(this->device_address_);
    I2CRequestProcessor::ProcessI2cRequest(*i2CRequestBuilder.i2c_request_);
}

void MCP4662::SetRheostatResistanceOhms(Rheostats_e channel, uint32_t resistance_in_ohms) {
    uint16_t steps =
            (uint16_t) (((resistance_in_ohms - this->wiper_resistance_ohms_) * this->resolution_ *
                        1.0f) / this->full_resistance_ohms_);
    __android_log_print(ANDROID_LOG_ERROR, "MainActivity", "steps %d", steps);
    this->SetRheostat(channel, steps);
}
