//
// Created by fedal on 12/10/21.
//

#ifndef RECIPEENGINE_MCP4662_H
#define RECIPEENGINE_MCP4662_H

#define MCP4531_WRITE (0 << 2)
#define MCP4531_INCR  (1 << 2)
#define MCP4531_DECR  (2 << 2)
#define MCP4531_READ  (3 << 2)

typedef enum {
    RHEOSTAT_1,
    RHEOSTAT_2
} Rheostats_e;
typedef enum {
    EIGHT_BIT_RESOLUTION = 256,
    SEVEN_BIT_RESOLUTION = 128
} RheostatStepResolution_e;

class MCP4662 {
private:
    uint8_t device_address_;
    const RheostatStepResolution_e resolution_ = EIGHT_BIT_RESOLUTION;
    const uint32_t wiper_resistance_ohms_ = 100;
    const uint32_t full_resistance_ohms_ = 100000;
public:
    MCP4662(uint8_t device_address) { this->device_address_ = device_address; }

    void SetRheostat(Rheostats_e, uint16_t);

    uint16_t GetRheostat(Rheostats_e);

    void IncrementRheostat(Rheostats_e);

    void DecrementRheostat(Rheostats_e);

    void SetRheostatResistanceOhms(Rheostats_e, uint32_t);
};


#endif //RECIPEENGINE_MCP4662_H
