//
// Created by fedal on 12/10/21.
//

#ifndef RECIPEENGINE_DIGITALPOT_H
#define RECIPEENGINE_DIGITALPOT_H

#include "MCP4662/MCP4662.h"

class DigitalPot : public StandaloneComponent {
private:

    MCP4662 *mcp4662_;

    Rheostats_e rheostats_;

    void SetPot(uint16_t);

    void SetPotResistanceOhms(uint32_t);

    uint16_t GetPot();

    void IncrementPot();

    void DecrementPot();

public:
    DigitalPot(Component_e component, MCP4662 *mcp4662, Rheostats_e rheostats)
            : StandaloneComponent(component) {
        this->mcp4662_ = mcp4662;
        this->rheostats_ = rheostats;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);

    ~DigitalPot(){
        if(nullptr != this->mcp4662_){
            delete this->mcp4662_;
        }
    }
};


#endif //RECIPEENGINE_DIGITALPOT_H
