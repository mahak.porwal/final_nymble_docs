//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_INDUCTIONACTUATOR_H
#define JULIA_ANDROID_INDUCTIONACTUATOR_H


class InductionActuator : public SubsystemComponent {

public:
    InductionActuator(Component_e component, Subsystem subsystem) :
            SubsystemComponent(component, subsystem) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_INDUCTIONACTUATOR_H
