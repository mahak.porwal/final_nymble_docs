//
// Created by fedal on 21/12/20.
//

#include "InductionActuator.h"

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
InductionActuator::ProcessRequest(const ComponentRequest &component_request) {
    double result = -1;
    float duty_cycle = component_request.GetSpeed();
    PwmRequestBuilder *pwmRequestBuilder = new PwmRequestBuilder;
    pwmRequestBuilder->pwm_request_->SetHardwareComponent(this->GetComponentId())->SetDutyCycle(
            duty_cycle);
    result = HardwareService::pwm_request_processor_->ProcessPwmRequest(
            *pwmRequestBuilder->pwm_request_);
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;
    values.push_back(std::to_string(result));
    response_map.emplace(COMMAND_RESULT, values);
    return response_map;
}
