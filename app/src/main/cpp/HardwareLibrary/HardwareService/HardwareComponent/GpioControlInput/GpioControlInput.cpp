//
// Created by fedal on 21/12/20.
//

#include "GpioControlInput.h"

int GpioControlInput::PerformRequest(const ComponentRequest &component_request) {
    GpioState_e state;
    GpioRequestBuilder *gpioRequestBuilder = new GpioRequestBuilder;
    gpioRequestBuilder->gpio_request_->SetHardwareComponent(component_request.GetComponent())
            ->SetAction(
                    ACTION_READ_PIN);
    state = static_cast<GpioState_e>(HardwareService::gpio_request_processor_->ProcessGpioRequest(
            *gpioRequestBuilder->gpio_request_));
    return state;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
GpioControlInput::ProcessRequest(const ComponentRequest &component_request) {
    int result = this->PerformRequest(component_request);
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector<string> values;
    values.push_back(std::to_string(result));
    response_map.emplace(SENSOR_VALUE, values);
    return response_map;
}
