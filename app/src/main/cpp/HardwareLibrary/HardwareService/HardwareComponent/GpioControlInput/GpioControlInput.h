//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_GPIOCONTROLINPUT_H
#define JULIA_ANDROID_GPIOCONTROLINPUT_H


class GpioControlInput : public StandaloneComponent {
private:
    int PerformRequest(const ComponentRequest &component_request);

public:
    GpioControlInput(Component_e component) : StandaloneComponent(component) {}

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_GPIOCONTROLINPUT_H
