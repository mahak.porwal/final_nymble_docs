//
// Created by fedal on 8/12/20.
//

#ifndef JULIA_ANDROID_HARDWARECOMPONENT_H
#define JULIA_ANDROID_HARDWARECOMPONENT_H




class HardwareComponent {
public:
    virtual double PerformRequest(ComponentRequest *) = 0;

    virtual ~HardwareComponent() {}

protected:
   Component_e hardware_component_;
};

#endif //JULIA_ANDROID_HARDWARECOMPONENT_H
