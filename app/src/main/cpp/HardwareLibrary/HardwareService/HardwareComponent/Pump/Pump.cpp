//
// Created by fedal on 21/12/20.
//

#include "Pump.h"

int Pump::TurnOnOffPump(OnOffState_e state) {
    int result = -1;
    PwmRequestBuilder *pwmRequestBuilder = new PwmRequestBuilder;
    if (state == ON_STATE) {
        pwmRequestBuilder->pwm_request_->SetHardwareComponent(
                this->GetComponentId())->SetDutyCycle(
                turn_on_value_);
    } else {
        pwmRequestBuilder->pwm_request_->SetHardwareComponent(
                this->GetComponentId())->SetDutyCycle(
                turn_off_value_);
    }
    result = HardwareService::pwm_request_processor_->ProcessPwmRequest(
            *pwmRequestBuilder->pwm_request_);
    delete pwmRequestBuilder;
    return result;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
Pump::ProcessRequest(const ComponentRequest &component_request) {
    double result = -1;
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    if (component_request.GetSpeed() != -1) {
        float speed = component_request.GetSpeed();
        if (speed > 100.0) { speed = 100; }
        PwmRequestBuilder *pwmRequestBuilder = new PwmRequestBuilder;
        pwmRequestBuilder->pwm_request_->SetHardwareComponent(
                component_request.GetComponent())->SetDutyCycle(speed);
        result = HardwareService::pwm_request_processor_->ProcessPwmRequest(
                *pwmRequestBuilder->pwm_request_);
        delete pwmRequestBuilder;
    } else if (component_request.GetState() != DEFAULT_ON_OFF_STATE) {
        result = TurnOnOffPump(component_request.GetState());
    }
    std::vector<string> values;
    values.push_back(std::to_string(result));
    response_map.emplace(SEQUENCE_RESULT, values);
    return response_map;
}

