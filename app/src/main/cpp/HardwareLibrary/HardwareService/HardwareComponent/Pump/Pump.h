//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_PUMP_H
#define JULIA_ANDROID_PUMP_H


class Pump : public SubsystemComponent {

private:
    float turn_on_value_;
    float turn_off_value_;

    int TurnOnOffPump(OnOffState_e);

public:
    Pump(Component_e hardware_component, float on_value, float off_value,
         const Subsystem &subsystem)
            :
            SubsystemComponent(hardware_component, subsystem) {
        this->turn_on_value_ = on_value;
        this->turn_off_value_ = off_value;
    }

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_PUMP_H
