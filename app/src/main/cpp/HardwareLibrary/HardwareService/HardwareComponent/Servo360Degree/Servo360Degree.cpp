//
// Created by fedal on 16/12/20.
//

#include "Servo360Degree.h"
#include "../../Peripherals/Pwm/PwmRequest/PwmRequestBuilder.h"
#include "../../Peripherals/Pwm/PwmRequestProcessor/PwmRequestProcessor.h"
#include "../../Peripherals/Adc/AdcRequest/AdcRequestBuilder.h"

bool Servo360Degree::SetActuator(PwmRequest) {
    return false;
}

float Servo360Degree::GetActuationValue() {

    return 0;
}

int Servo360Degree::ExecuteServoSequence(uint16_t angle, float speed) {
    return 0;
}

float Servo360Degree::GetSensorReading() {
    //AdcRequestBuilder *adc_request_builder = new AdcRequestBuilder;
    ComponentRequestBuilder component_request_builder;
    float result = -1;
    Component_e feedback_component;
    switch (this->GetComponentId()) {
        case MACRO_SERVO_4: {
            feedback_component = MACRO_SERVO_4_FEEDBACK;
        }
            break;
        case MACRO_SERVO_3: {
            feedback_component = MACRO_SERVO_3_FEEDBACK;
        }
            break;
        case MACRO_SERVO_2: {
            feedback_component = MACRO_SERVO_2_FEEDBACK;
        }
            break;
        case MACRO_SERVO_1: {
            feedback_component = MACRO_SERVO_1_FEEDBACK;
        }
            break;
        case MICRO_SERVO_1: {
            feedback_component = MICRO_SERVO_1_FEEDBACK;
        }
            break;
        case MICRO_SERVO_2: {
            feedback_component = MICRO_SERVO_2_FEEDBACK;
        }
            break;
        case MICRO_SERVO_3: {

            feedback_component = MICRO_SERVO_3_FEEDBACK;
        }
            break;
        case MICRO_SERVO_4: {

            feedback_component = MICRO_SERVO_4_FEEDBACK;
        }
            break;
        case MICRO_SERVO_5: {

            feedback_component = MICRO_SERVO_5_FEEDBACK;
        }
            break;
        case MICRO_SERVO_6: {

            feedback_component = MICRO_SERVO_6_FEEDBACK;
        }
            break;
        default:
            break;
    }

    component_request_builder.component_request_.SetRequestType(HARDWARE_REQUEST).SetComponent(
            feedback_component).SetSensorUnit(MILLI_VOLTS);

    auto response = HardwareServiceFactory::GetHardwareService()->ProcessComponentRequest(
            component_request_builder.component_request_);
    result = std::stof(response.at(SENSOR_VALUE).at(0));

    return result;
}

uint16_t
Servo360Degree::ConvertSpeedToPulseDurationUs(float speed, ActuatorDirections_e direction) {
    if(speed == 0){
        return 1500;
    }
    uint16_t pulse_duration;
    if (speed < 1) {
        speed = 1;
    } else if (speed > 10) {
        speed = 10;
    }
    float register_value;
    if (DIRECTION_CLOCKWISE == direction) {
        register_value = speed * 17.8 + 1522;
    } else {
        register_value = speed * -17.8 + 1478;
    }
    pulse_duration = (uint16_t) register_value;
    return pulse_duration;
}

int Servo360Degree::ExecuteServoRotation(float speed, ActuatorDirections_e direction) {
    int result = -1;
    uint16_t pulse_duration_us;

    if (direction == DIRECTION_DEFAULT) {
        throw new Error;
    }

    pulse_duration_us = (Servo360Degree::ConvertSpeedToPulseDurationUs(speed, direction));

    /*uint16_t pulse_duration = 1500;

    while(1){

        PwmRequestBuilder *pwm_request_builder = new PwmRequestBuilder();

        pwm_request_builder->pwm_request_->SetPulseDurationUs(pulse_duration)->SetHardwareComponent(
                this->GetComponentId());
        pulse_duration += 10;
        result = PwmRequestProcessor::ProcessPwmRequest(*pwm_request_builder->pwm_request_);
        usleep(2000000);
        if(pulse_duration > 1700){
            pulse_duration = 1500;
        }
    }*/

    PwmRequestBuilder *pwm_request_builder = new PwmRequestBuilder();

    pwm_request_builder->pwm_request_->SetPulseDurationUs(pulse_duration_us)->SetHardwareComponent(
            this->GetComponentId());
    result = PwmRequestProcessor::ProcessPwmRequest(*pwm_request_builder->pwm_request_);
    delete pwm_request_builder;
    return result;
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
Servo360Degree::ProcessRequest(const ComponentRequest &component_request) {
    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
    switch (component_request.GetRequestType()) {
        case HARDWARE_REQUEST:
            if (component_request.GetFrequency() != -1) {
                //GetSensorReading
                float reading = this->GetSensorReading();
                std::vector<string> values;
                values.push_back(std::to_string(reading));
                response_map.emplace(SENSOR_VALUE, values);
                //return this->GetSensorReading();
            } else if (component_request.GetSpeed() != -1 &&
                       component_request.GetPosition() != -1) {
                //ExecuteServoSequence
                int result = this->ExecuteServoSequence(component_request.GetPosition(),
                                                        component_request.GetSpeed());
                std::vector<string> values;
                values.push_back(std::to_string(result));
                response_map.emplace(SEQUENCE_RESULT, values);
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                    "Servo Sequence");
            } else if (component_request.GetSpeed() != -1 &&
                       component_request.GetDirection() != DIRECTION_DEFAULT) {
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                    "Servo Rotation");
                //ExecuteServoRotation

                int result = this->ExecuteServoRotation(component_request.GetSpeed(),
                                                        component_request.GetDirection());
                std::vector<string> values;
                values.push_back(std::to_string(result));
                response_map.emplace(SEQUENCE_RESULT, values);
            }
            break;
        case SEQUENCE_REQUEST:
            break;
    }
    return response_map;
}
