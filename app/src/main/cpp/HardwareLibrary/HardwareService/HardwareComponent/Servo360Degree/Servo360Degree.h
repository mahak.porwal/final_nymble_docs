//
// Created by fedal on 16/12/20.
//

#ifndef JULIA_ANDROID_SERVO360DEGREE_H
#define JULIA_ANDROID_SERVO360DEGREE_H


#include "../../../Component/SubsystemComponent/SubsystemComponent.h"

class Servo360Degree : public SubsystemComponent {
private:
    float current_actuation_value_;
    uint16_t max_angle_;
    Timer timer;

    bool SetActuator(PwmRequest);

    int ExecuteServoSequence(uint16_t, float);

    int ExecuteServoRotation(float, ActuatorDirections_e);

    float GetSensorReading();

    static uint16_t ConvertSpeedToPulseDurationUs(float, ActuatorDirections_e);

public:
    Servo360Degree(Component_e hardware_component, const Subsystem &subsystem) :
            SubsystemComponent(hardware_component, subsystem) {}

    float GetActuationValue();

    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);

};


#endif //JULIA_ANDROID_SERVO360DEGREE_H
