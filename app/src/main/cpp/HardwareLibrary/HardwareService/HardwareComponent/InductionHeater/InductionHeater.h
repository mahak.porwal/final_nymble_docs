//
// Created by fedal on 26/7/21.
//

#ifndef RECIPEENGINE_INDUCTIONHEATER_H
#define RECIPEENGINE_INDUCTIONHEATER_H


class InductionHeater : public SubsystemComponent {
public :
    InductionHeater(Subsystem subsystem) :
            SubsystemComponent(INDUCTION_HEATER, subsystem) {}
    std::unordered_map<ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //RECIPEENGINE_INDUCTIONHEATER_H
