//
// Created by fedal on 26/7/21.
//

#include "InductionHeater.h"

using namespace induction_control_input;

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
InductionHeater::ProcessRequest(const ComponentRequest &component_request) {

    if (component_request.GetInductionControlInput() != Null_Input) {
        HardwareService::induction_controller_->SetInput(
                component_request.GetInductionControlInput());
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
        std::vector<string> values;
        values.push_back("input_set");
        response_map.emplace(COMMAND_RESULT, values);
        return response_map;
    }

    if (component_request.GetInductionMachineReq() != NULL_REQUEST) {
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> response_map;
        std::vector<string> values;

        switch (component_request.GetInductionMachineReq()) {
            case INDUCTION_STATE:
                switch (HardwareService::induction_controller_->GetInductionControlState()) {
                    case Power_Control_State:
                        values.push_back("POWER_CONTROL_STATE");
                        break;
                    case Error_State:
                        values.push_back("ERROR_STATE");
                        break;
                    case Idle_State:
                        values.push_back("IDLE_STATE");
                        break;
                    case Waiting_For_Error_Correction_State:
                        values.push_back("WAITING_FOR_ERROR_CORRECTION_STATE");
                        break;
                    case Pan_Detection_State :
                        values.push_back("PAN_DETECTION_STATE");
                        break;
                    case Hard_Fault_State :
                        values.push_back("HARD_FAULT_STATE");
                        break;
                    case Manual_Mode_State:
                        values.push_back("MANUAL_MODE_STATE");
                        break;
                    default:
                        values.push_back("NULL_STATE");
                        break;
                }
                response_map.emplace(COMMAND_RESULT, values);

                break;
            case INDUCTION_ERROR:
                switch (HardwareService::induction_controller_->GetError()) {
                    case Igbt_Temp_Error:
                        values.push_back("IGBT_TEMP_ERROR");
                        break;
                    case Pan_Temp_Error:
                        values.push_back("PAN_TEMP_ERROR");
                        break;
                    case Current_Error:
                        values.push_back("CURRENT_ERROR");
                        break;
                    case Voltage_Error:
                        values.push_back("VOLTAGE_ERROR");
                        break;
                    case No_Pan_Error :
                        values.push_back("NO_PAN_ERROR");
                        break;
                    default:
                        values.push_back("NO_ERROR");
                }
                response_map.emplace(COMMAND_RESULT, values);
                break;
            case INDUCTION_POWER_LEVEL:
                switch (HardwareService::induction_controller_->GetError()) {
                    case Igbt_Temp_Error:
                        values.push_back("IGBT_TEMP_ERROR");
                        break;
                    case Pan_Temp_Error:
                        values.push_back("PAN_TEMP_ERROR");
                        break;
                    case Current_Error:
                        values.push_back("CURRENT_ERROR");
                        break;
                    case Voltage_Error:
                        values.push_back("VOLTAGE_ERROR");
                        break;
                    case No_Pan_Error :
                        values.push_back("NO_PAN_ERROR");
                        break;
                    default:
                        values.push_back("NO_ERROR");
                        break;
                        break;
                }
                response_map.emplace(COMMAND_RESULT, values);
                break;
            case INDUCTION_PAN_DETECT:
                if (HardwareService::induction_controller_->GetInductionControlState() !=
                    Power_Control_State &&
                    HardwareService::induction_controller_->GetInductionControlState() !=
                    Error_State) {
                    PanDetection pan_detection;
                    bool is_pan_detected = pan_detection.DetectPan();
                    if (is_pan_detected == false) {
                        values.push_back("PAN_NOT_DETECTED");
                    } else {
                        values.push_back("PAN_DETECTED");
                    }
                } else {
                    values.push_back("Pan detection not allowed");
                }
                response_map.emplace(COMMAND_RESULT, values);
                break;
            default:
                break;
        }
        return response_map;
    }

}