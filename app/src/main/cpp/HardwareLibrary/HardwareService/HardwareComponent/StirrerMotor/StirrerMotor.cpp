//
// Created by fedal on 21/12/20.
//

#include "StirrerMotor.h"

std::unordered_map <ResponseParameters_e, std::vector<std::string>>
StirrerMotor::ProcessRequest(const ComponentRequest &component_request) {
    ActuatorDirections_e direction = component_request.GetDirection();
    float speed = component_request.GetSpeed();
    GpioRequestBuilder *gpioRequestBuilder = new GpioRequestBuilder;
    switch (direction) {
        case DIRECTION_CLOCKWISE: {
            gpioRequestBuilder->gpio_request_->SetHardwareComponent(STIRRER_PHASE_GPIO)->SetAction(
                    ACTION_WRITE_PIN)->SetState(STATE_HIGH);
        }
            break;
        case DIRECTION_ANTICLOCKWISE: {
            gpioRequestBuilder->gpio_request_->SetHardwareComponent(STIRRER_PHASE_GPIO)->SetAction(
                    ACTION_WRITE_PIN)->SetState(STATE_LOW);
        }
            break;
        default: {
            delete gpioRequestBuilder;
            throw new Error;
        }
    }
    int res = HardwareService::gpio_request_processor_->ProcessGpioRequest(
            *gpioRequestBuilder->gpio_request_);
    PwmRequestBuilder *pwmRequestBuilder = new PwmRequestBuilder;
    pwmRequestBuilder->pwm_request_->SetHardwareComponent(STIRRER_MOTOR)->SetDutyCycle(10 * speed);

    double result = HardwareService::pwm_request_processor_->ProcessPwmRequest(
            *pwmRequestBuilder->pwm_request_);
    delete gpioRequestBuilder;
    delete pwmRequestBuilder;
    std::unordered_map <ResponseParameters_e, std::vector<std::string>> response_map;
    std::vector <string> values;
    values.push_back(std::to_string(result));
    response_map.emplace(COMMAND_RESULT, values);
    return response_map;
}
