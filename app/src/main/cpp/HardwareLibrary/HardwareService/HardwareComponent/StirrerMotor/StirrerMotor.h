//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_DCMOTOR_H
#define JULIA_ANDROID_DCMOTOR_H


#include "../../../Component/SubsystemComponent/SubsystemComponent.h"

class StirrerMotor : public SubsystemComponent {

public:
    StirrerMotor(Component_e hardware_component, Subsystem subsystem) :
            SubsystemComponent(hardware_component, subsystem) {}

    std::unordered_map <ResponseParameters_e, std::vector<std::string>>
    ProcessRequest(const ComponentRequest &component_request);
};


#endif //JULIA_ANDROID_DCMOTOR_H
