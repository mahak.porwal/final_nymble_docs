//
// Created by fedal on 8/12/20.
//


#include "HardwareService.h"
#include "../Subsystem/SubsystemComponent/ComponentEnums.h"
#include "HardwareComponent/Servo360Degree/Servo360Degree.h"
#include "HardwareComponent/Servo360Macro/Servo360Macro.h"
#include "HardwareComponent/Multiplexer/MuxEightIsToOne/MacroMux/MacroMux.h"
#include "HardwareComponent/Multiplexer/MuxTwoIsToOne/MuxTwoIsToOne.h"
#include "HardwareComponent/Multiplexer/MuxEightIsToOne/MicroMux/MicroMux.h"
#include "HardwareComponent/Servo180Degree/Servo180Degree.h"
#include "HardwareComponent/ThermalCamEeprom/ThermalCamEeprom.cpp"
#include "HardwareComponent/ThermalCamera/ThermalCamera.cpp"
#include "../Subsystem/Macro/Macro.h"
#include "../Subsystem/Micro/Micro.h"
#include "../Subsystem/Liquid/Liquid.h"
#include "../Subsystem/Stirrer/Stirrer.h"
#include "../Subsystem/Induction/Induction.h"
#include "HardwareComponent/StirrerMotor/StirrerMotor.h"
#include "HardwareComponent/Pump/Pump.h"
#include "HardwareComponent/LedActuator/LedActuator.h"
#include "HardwareComponent/ExhaustFan/ExhaustFan.h"
#include "HardwareComponent/Buzzer/Buzzer.h"
#include "HardwareComponent/DigitalPot/MCP4662/MCP4662.h"
#include "HardwareComponent/DigitalPot/DigitalPot.h"
#include "HardwareComponent/GpioControlInput/GpioControlInput.h"
#include "HardwareComponent/GpioControlOutput/GpioControlOutput.h"
#include "HardwareComponent/AnalogFeedback/AnalogFeedback.h"
#include "HardwareComponent/InductionActuator/InductionActuator.h"
#include "HardwareComponent/InductionHeater/InductionHeater.h"
#include "HardwareComponent/AnalogFeedback/MicroFeedback/MicroFeedback.h"
#include "HardwareComponent/AnalogFeedback/MacroFeedback/MacroFeedback.h"
#include "Peripherals/Adc/Devices/SOM_Adc/SOM_Adc.h"
#include "HardwareComponent/AnalogFeedback/StirrerCurrent/StirrerCurrent.h"
#include "HardwareComponent/AnalogFeedback/LoadcellSensor/LoadcellSensor.h"
#include "HardwareComponent/AnalogFeedback/InductionAnalogSensors/InputVoltage/InputVoltage.h"
#include "HardwareComponent/AnalogFeedback/InductionAnalogSensors/InputCurrent/InputCurrent.h"
#include "HardwareComponent/AnalogFeedback/InductionAnalogSensors/IgbtTemperature/IgbtTemperature.h"
#include "HardwareComponent/AnalogFeedback/InductionAnalogSensors/PanTemperature/PanTemperature.h"
#include "../Component/StandaloneComponent/StandaloneComponent.h"
#include "../Component/SubsystemComponent/SubsystemComponent.h"
#include "HardwareComponent/AnalogFeedback/TemperatureFeedback/TemperatureFeedback.h"
#include "HardwareComponent/Counter/Counter.h"
#include "HardwareComponent/Counter/ExhaustFanCounter/ExhaustFanCounter.h"
#include "HardwareComponent/Counter/WaterFlowCounter/WaterFlowCounter.h"
#include "HardwareComponent/Counter/OilFlowCounter/OilFlowCounter.h"
#include "NonVolatileFactors.h"
#include "HardwareComponent/AnalogFeedback/Macro360Feedback/Macro360Feedback.h"
#include <string>
#include <vector>

void HardwareService::StartHardwareService() {
    HardwareService::i2c_ = new I2C;
    HardwareService::pwm_request_processor_ = new PwmRequestProcessor(
            this->hardware_factors);
    HardwareService::gpio_request_processor_ = new GpioRequestProcessor;
    HardwareService::adc_request_processor_ = new AdcRequestProcessor;
    HardwareService::induction_controller_ = InductionControllerFactory::GetInductionController();
}

void HardwareService::StopHardwareService() {

    //delete objects of hardware component
    /*for (auto itr = HardwareService::hardware_component_object_map_.begin();
         itr != HardwareService::hardware_component_object_map_.end(); itr++) {
        delete itr->second;
    }
    for (auto itr = HardwareService::subsystem_object_map_.begin();
         itr != HardwareService::subsystem_object_map_.end(); itr++) {
        delete itr->second;
    }*/
    delete HardwareService::pwm_request_processor_;
    delete HardwareService::gpio_request_processor_;
    delete HardwareService::adc_request_processor_;
    delete HardwareService::i2c_;
    delete HardwareService::induction_controller_;
}

void HardwareService::PopulateComponentMap() {
    //Macro subsystem
    Subsystem *macro = new Macro;
    std::string key = MACRO_ONE_HOME;
    uint16_t macro_home = 0;
    if (hardware_factors.find(key) != hardware_factors.end()) {
        macro_home = (uint16_t) std::stod(this->hardware_factors.at(key.c_str()));
    }

    //Macro servos
    Servo180Degree *macro_servo_1 = new Servo180Degree(MACRO_SERVO_1, *macro,
                                                       macro_home);
    this->component_map_->emplace(macro_servo_1->GetComponentId(), *macro_servo_1);

    key = MACRO_TWO_HOME;
    if (hardware_factors.find(key) != hardware_factors.end()) {
        macro_home = (uint16_t) std::stod(this->hardware_factors.at(key.c_str()));
    } else {
        macro_home = 0;
    }

    Servo180Degree *macro_servo_2 = new Servo180Degree(MACRO_SERVO_2, *macro,
                                                       macro_home);
    this->component_map_->emplace(macro_servo_2->GetComponentId(), *macro_servo_2);

    key = MACRO_THREE_HOME;
    if (hardware_factors.find(key) != hardware_factors.end()) {
        macro_home = (uint16_t) std::stod(this->hardware_factors.at(key.c_str()));
    } else {
        macro_home = 180;
    }

    Servo180Degree *macro_servo_3 = new Servo180Degree(MACRO_SERVO_3, *macro,
                                                       macro_home);
    this->component_map_->emplace(macro_servo_3->GetComponentId(), *macro_servo_3);

    key = MACRO_FOUR_HOME;
    if (hardware_factors.find(key) != hardware_factors.end()) {
        macro_home = (uint16_t) std::stod(this->hardware_factors.at(key.c_str()));
    } else {
        macro_home = 239;
    }

    Servo360Macro *macro_servo_4 = new Servo360Macro(MACRO_SERVO_4, *macro,
                                                     DIRECTION_CLOCKWISE,
                                                     239, 79);
    this->component_map_->emplace(macro_servo_4->GetComponentId(), *macro_servo_4);


    //Macro servo analog feedback
    MacroFeedback *macro_servo_1_feedback = new MacroFeedback(
            MACRO_SERVO_1_FEEDBACK, *macro);
    this->component_map_->emplace(macro_servo_1_feedback->GetComponentId(),
                                  *macro_servo_1_feedback);

    MacroFeedback *macro_servo_2_feedback = new MacroFeedback(
            MACRO_SERVO_2_FEEDBACK, *macro);
    this->component_map_->emplace(macro_servo_2_feedback->GetComponentId(),
                                  *macro_servo_2_feedback);

    MacroFeedback *macro_servo_3_feedback = new MacroFeedback(
            MACRO_SERVO_3_FEEDBACK, *macro);
    this->component_map_->emplace(macro_servo_3_feedback->GetComponentId(),
                                  *macro_servo_3_feedback);

    Macro360Feedback *macro_servo_4_feedback = new Macro360Feedback(
            MACRO_SERVO_4_FEEDBACK, *macro);

    this->component_map_->emplace(macro_servo_4_feedback->GetComponentId(),
                                  *macro_servo_4_feedback);


    MacroMux *macro_multiplexer = new MacroMux(MACRO_MUX, *macro);
    this->component_map_->emplace(macro_multiplexer->GetComponentId(), *macro_multiplexer);


    //Macro subsystem
    Subsystem *micro = new Micro;

    //Micro servos
    Servo360Degree *micro_servo_1 = new Servo360Degree(MICRO_SERVO_1, *micro);
    this->component_map_->emplace(micro_servo_1->GetComponentId(), *micro_servo_1);

    Servo360Degree *micro_servo_2 = new Servo360Degree(MICRO_SERVO_2, *micro);
    this->component_map_->emplace(micro_servo_2->GetComponentId(), *micro_servo_2);

    Servo360Degree *micro_servo_3 = new Servo360Degree(MICRO_SERVO_3, *micro);
    this->component_map_->emplace(micro_servo_3->GetComponentId(), *micro_servo_3);

    Servo360Degree *micro_servo_4 = new Servo360Degree(MICRO_SERVO_4, *micro);
    this->component_map_->emplace(micro_servo_4->GetComponentId(), *micro_servo_4);

    Servo360Degree *micro_servo_5 = new Servo360Degree(MICRO_SERVO_5, *micro);
    this->component_map_->emplace(micro_servo_5->GetComponentId(), *micro_servo_5);

    Servo360Degree *micro_servo_6 = new Servo360Degree(MICRO_SERVO_6, *micro);
    this->component_map_->emplace(micro_servo_6->GetComponentId(), *micro_servo_6);


    //micro servo analog feedback
    MicroFeedback *micro_servo_1_feedback = new MicroFeedback(
            MICRO_SERVO_1_FEEDBACK, *micro);
    this->component_map_->emplace(micro_servo_1_feedback->GetComponentId(),
                                  *micro_servo_1_feedback);

    MicroFeedback *micro_servo_2_feedback = new MicroFeedback(
            MICRO_SERVO_2_FEEDBACK, *micro);
    this->component_map_->emplace(micro_servo_2_feedback->GetComponentId(),
                                  *micro_servo_2_feedback);

    MicroFeedback *micro_servo_3_feedback = new MicroFeedback(
            MICRO_SERVO_3_FEEDBACK, *micro);
    this->component_map_->emplace(micro_servo_3_feedback->GetComponentId(),
                                  *micro_servo_3_feedback);

    MicroFeedback *micro_servo_4_feedback = new MicroFeedback(
            MICRO_SERVO_4_FEEDBACK, *micro);
    this->component_map_->emplace(micro_servo_4_feedback->GetComponentId(),
                                  *micro_servo_4_feedback);

    MicroFeedback *micro_servo_5_feedback = new MicroFeedback(
            MICRO_SERVO_5_FEEDBACK, *micro);
    this->component_map_->emplace(micro_servo_5_feedback->GetComponentId(),
                                  *micro_servo_5_feedback);

    MicroFeedback *micro_servo_6_feedback = new MicroFeedback(
            MICRO_SERVO_6_FEEDBACK, *micro);
    this->component_map_->emplace(micro_servo_6_feedback->GetComponentId(),
                                  *micro_servo_6_feedback);

    MicroMux *micro_multiplexer = new MicroMux(MICRO_MUX, *micro);
    this->component_map_->emplace(micro_multiplexer->GetComponentId(),
                                  *micro_multiplexer);

    //Lm2678
    Component *lm2678 = new GpioControlOutput(LM2678_CONTROL, *micro);
    this->component_map_->emplace(lm2678->GetComponentId(), *lm2678);

    //Stirrer subsystem
    Subsystem *stirrer = new Stirrer;

    //Stirrer motor
    StirrerMotor *stirrer_motor = new StirrerMotor(STIRRER_MOTOR, *stirrer);
    this->component_map_->emplace(stirrer_motor->GetComponentId(), *stirrer_motor);


    //Stirrer connection feedback
    GpioControlInput *stirrer_connection_feedback = new GpioControlInput(STIRRER_CONNECTION_FB);
    this->component_map_->emplace(stirrer_connection_feedback->GetComponentId(),
                                  *stirrer_connection_feedback);

    //Stirrer phase gpio
    GpioControlOutput *stirrer_phase_gpio = new GpioControlOutput(STIRRER_PHASE_GPIO,
                                                                  *stirrer);
    this->component_map_->emplace(stirrer_phase_gpio->GetComponentId(), *stirrer_phase_gpio);

    //Stirrer current
    StirrerCurrent *stirrer_current = new StirrerCurrent(STIRRER_CURRENT_SENSOR,
                                                         *stirrer);
    this->component_map_->emplace(stirrer_current->GetComponentId(), *stirrer_current);



    //Induction Subsystem
    Subsystem *induction = new Induction;

    //Induction fan gpio
    GpioControlOutput *induction_fan_gpio = new GpioControlOutput(INDUCTION_FAN_GPIO, *induction);
    this->component_map_->emplace(induction_fan_gpio->GetComponentId(), *induction_fan_gpio);

    //Induction pan gpio
    GpioControlOutput *induction_pan_gpio = new GpioControlOutput(INDUCTION_PAN_GPIO, *induction);
    this->component_map_->emplace(induction_pan_gpio->GetComponentId(), *induction_pan_gpio);

    //Induction INT gpio
    GpioControlOutput *induction_int_gpio = new GpioControlOutput(INDUCTION_INT_GPIO, *induction);
    this->component_map_->emplace(induction_int_gpio->GetComponentId(), *induction_int_gpio);

    //Induction ON OFF GPIO
    GpioControlOutput *induction_on_off_gpio = new GpioControlOutput(INDUCTION_ON_OFF_GPIO,
                                                                     *induction);
    this->component_map_->emplace(induction_on_off_gpio->GetComponentId(), *induction_on_off_gpio);

    //Induction Buzzer GPIO
    GpioControlOutput *induction_buzzer_gpio = new GpioControlOutput(INDUCTION_BUZZER_GPIO,
                                                                     *induction);
    this->component_map_->emplace(induction_buzzer_gpio->GetComponentId(), *induction_buzzer_gpio);

    //Induction input voltage
    InputVoltage *input_voltage = new InputVoltage(INDUCTION_AC_VOLTAGE_SENSOR, *induction);
    this->component_map_->emplace(input_voltage->GetComponentId(), *input_voltage);

    //Induction input current
    InputCurrent *input_current = new InputCurrent(INDUCTION_AC_CURRENT_SENSOR, *induction);
    this->component_map_->emplace(input_current->GetComponentId(), *input_current);

    //Induction igbt temperature
    IgbtTemperature *igbt_temperature_sensor = new IgbtTemperature(
            INDUCTION_IGBT_TEMPERATURE_SENSOR,
            *induction);
    this->component_map_->emplace(igbt_temperature_sensor->GetComponentId(),
                                  *igbt_temperature_sensor);

    //Induction pan temperature
    PanTemperature *induction_pan_temperature = new PanTemperature(
            INDUCTION_PAN_TEMPERATURE_SENSOR, *induction);
    this->component_map_->emplace(induction_pan_temperature->GetComponentId(),
                                  *induction_pan_temperature);

    //Induction power actuator
    InductionActuator *induction_actuator = new InductionActuator(INDUCTION_ACTUATOR, *induction);
    this->component_map_->emplace(induction_actuator->GetComponentId(), *induction_actuator);

    //Induction heater
    InductionHeater *induction_heater = new InductionHeater(*induction);
    this->component_map_->emplace(induction_heater->GetComponentId(), *induction_heater);


    //Liquid Subsystem

    Subsystem *liquid = new Liquid;

    float pump_factor;
    key = WATER_PUMP_FACTOR;
    if (hardware_factors.find(key) != hardware_factors.end()) {
        pump_factor = std::stof(this->hardware_factors.at(key.c_str()));
    } else {
        pump_factor = 47.0;
    }
    Component *liquid_water_pump = new Pump(WATER_PUMP, pump_factor, 0.0, *liquid);
    this->component_map_->emplace(liquid_water_pump->GetComponentId(), *liquid_water_pump);

    key = OIL_PUMP_FACTOR;
    if (hardware_factors.find(key) != hardware_factors.end()) {
        pump_factor = std::stof(this->hardware_factors.at(key.c_str()));
    } else {
        pump_factor = 18.0;
    }

    Component *liquid_oil_pump = new Pump(OIL_PUMP, pump_factor, 0.0, *liquid);
    this->component_map_->emplace(liquid_oil_pump->GetComponentId(), *liquid_oil_pump);


    //Standalone components

    //Macro micro 2 : 1 mux
    Component *macro_micro_mux = new MuxTwoIsToOne(MACRO_MICRO_MUX);
    this->component_map_->emplace(macro_micro_mux->GetComponentId(), *macro_micro_mux);

    //Led illumination
    Component *led_illumination = new LedActuator(LED_ILLUMINATION, 5,
                                                  0);
    this->component_map_->emplace(led_illumination->GetComponentId(), *led_illumination);


    //Led design
    Component *led_design = new LedActuator(LED_DESIGN, 99,
                                            0);
    this->component_map_->emplace(led_design->GetComponentId(), *led_design);

    //Exhaust fan
    Component *exhaust_fan = new ExhaustFan(EXHAUST_FAN, 5, 0);
    this->component_map_->emplace(exhaust_fan->GetComponentId(), *exhaust_fan);

    //Buzzer
    Component *buzzer = new Buzzer(BUZZER, 5, 0);
    this->component_map_->emplace(buzzer->GetComponentId(), *buzzer);

    double load_cell_scaling_factor;
    key = LOAD_CELL_FACTOR;
    if (hardware_factors.find(key) != hardware_factors.end()) {
        load_cell_scaling_factor = std::stod(this->hardware_factors.at(key.c_str()));
    } else {
        pump_factor = -0.12;
    }
    LoadcellSensor *weight_sensor = new LoadcellSensor(LOADCELL_SENSOR, load_cell_scaling_factor);
    this->component_map_->emplace(weight_sensor->GetComponentId(), *weight_sensor);

    ThermalCamEeprom *thermal_camera_eeprom = new ThermalCamEeprom(THERMAL_CAMERA_EEPROM);
    this->component_map_->emplace(thermal_camera_eeprom->GetComponentId(),
                                  *thermal_camera_eeprom);

    ThermalCamera *thermal_camera = new ThermalCamera(THERMAL_CAMERA);
    this->component_map_->emplace(thermal_camera->GetComponentId(),
                                  *thermal_camera);
    this->thermal_camera_ = thermal_camera;
    MCP4662 *mcp4662_pan = new MCP4662(0x2C);
    MCP4662 *mcp4662_pwm = new MCP4662(0x2E);
    DigitalPot *pan_frequency_pot = new DigitalPot(INDUCTION_PAN_FREQUENCY_POT, mcp4662_pan,
                                                   RHEOSTAT_1);
    this->component_map_->emplace(pan_frequency_pot->GetComponentId(),
                                  *pan_frequency_pot);
    DigitalPot *pan_duty_pot = new DigitalPot(INDUCTION_PAN_DUTY_POT, mcp4662_pan,
                                              RHEOSTAT_2);
    this->component_map_->emplace(pan_duty_pot->GetComponentId(),
                                  *pan_duty_pot);

    DigitalPot *pwm_frequency_pot = new DigitalPot(INDUCTION_PWM_FREQUENCY_POT, mcp4662_pwm,
                                                   RHEOSTAT_1);
    this->component_map_->emplace(pwm_frequency_pot->GetComponentId(),
                                  *pwm_frequency_pot);
    DigitalPot *pwm_duty_pot = new DigitalPot(INDUCTION_PWM_DUTY_POT, mcp4662_pwm,
                                              RHEOSTAT_2);
    this->component_map_->emplace(pwm_duty_pot->GetComponentId(),
                                  *pwm_duty_pot);
    TemperatureFeedback *water_temp_feedback = new TemperatureFeedback(WATER_TEMPERATURE);
    this->component_map_->emplace(water_temp_feedback->GetComponentId(),
                                  *water_temp_feedback);

    TemperatureFeedback *body_temp_feedback = new TemperatureFeedback(BODY_TEMPERATURE);
    this->component_map_->emplace(body_temp_feedback->GetComponentId(),
                                  *body_temp_feedback);

    Counter *counter = new Counter(COUNTER);
    this->component_map_->emplace(counter->GetComponentId(),
                                  *counter);

    ExhaustFanCounter *exhaust_fan_counter = new ExhaustFanCounter(
            EXHAUST_FAN_COUNTER, counter);
    this->component_map_->emplace(exhaust_fan_counter->GetComponentId(),
                                  *exhaust_fan_counter);

    WaterFlowCounter *water_flow_counter = new WaterFlowCounter(
            WATER_FLOW_COUNTER, counter);
    this->component_map_->emplace(water_flow_counter->GetComponentId(),
                                  *water_flow_counter);

    OilFlowCounter *oil_flow_counter = new OilFlowCounter(
            OIL_FLOW_COUNTER, counter);
    this->component_map_->emplace(oil_flow_counter->GetComponentId(),
                                  *oil_flow_counter);

    GpioControlOutput *heater_enable = new GpioControlOutput(HEATER_ENABLE, *macro);
    this->component_map_->emplace(heater_enable->GetComponentId(),
                                  *heater_enable);

    GpioControlOutput *counter_mux_0 = new GpioControlOutput(COUNTER_MUX_SELECT_0, *macro);
    this->component_map_->emplace(counter_mux_0->GetComponentId(),
                                  *counter_mux_0);
    GpioControlOutput *counter_mux_1 = new GpioControlOutput(COUNTER_MUX_SELECT_1, *macro);
    this->component_map_->emplace(counter_mux_1->GetComponentId(),
                                  *counter_mux_1);
    GpioControlOutput *exhaust_fan_on_off = new GpioControlOutput(EXHAUST_FAN_ON_OFF, *macro);
    this->component_map_->emplace(exhaust_fan_on_off->GetComponentId(),
                                  *exhaust_fan_on_off);
    GpioControlInput *limit_switch_1 = new GpioControlInput(LIMIT_SWITCH_1);
    this->component_map_->emplace(limit_switch_1->GetComponentId(),
                                  *limit_switch_1);
    GpioControlInput *limit_switch_2 = new GpioControlInput(LIMIT_SWITCH_2);
    this->component_map_->emplace(limit_switch_2->GetComponentId(),
                                  *limit_switch_2);
    GpioControlInput *inductive_sensor_1 = new GpioControlInput(INDUCTIVE_SENSOR_1);
    this->component_map_->emplace(inductive_sensor_1->GetComponentId(),
                                  *inductive_sensor_1);
    GpioControlInput *inductive_sensor_2 = new GpioControlInput(INDUCTIVE_SENSOR_2);
    this->component_map_->emplace(inductive_sensor_2->GetComponentId(),
                                  *inductive_sensor_2);
}

const Component &HardwareService::GetComponent(const ComponentRequest &component_request) {
    return reinterpret_cast<const Component &>(this->component_map_->at(
            component_request.GetComponent()));
}

std::unordered_map<ResponseParameters_e, std::vector<std::string>>
HardwareService::ProcessComponentRequest(const ComponentRequest &component_request) {

    std::unordered_map<ResponseParameters_e, std::vector<std::string>> response;
    if (component_request.GetRequestType() == SEQUENCE_REQUEST) {
        if (component_request.IsAbortSequence() == true) {
            sequence_executor_.AbortSequence(component_request.GetComponent());
        } else {
            SequenceBehavior *sequence_behavior = SequenceExecutorFactory::GetSequenceBehavior(
                    component_request);
            response = sequence_executor_.SetSequenceInExecution(*sequence_behavior);
        }
    } else {
        response = this->component_map_->at(component_request.GetComponent()).ProcessRequest(
                component_request);
    }
    return response;
}

void HardwareService::StartInductionControl() {
    HardwareService::induction_controller_->StartControl();
}

void HardwareService::InitThermalCamera() {
    this->thermal_camera_->InitThermalCamera();
}

void HardwareService::StopInductionControl() {
    HardwareService::induction_controller_->StopControl();
}

void HardwareService::PopulateHardwareFactors(JNIEnv *env, jobject map_object) {

    // Find "java/util/Map" Class (Standard JAVA Class).
    jclass map_class = env->FindClass("java/util/Map");


    jmethodID id_entrySet = env->GetMethodID(map_class, "entrySet", "()Ljava/util/Set;");

    jclass c_entryset = env->FindClass("java/util/Set");

    jmethodID id_iterator = env->GetMethodID(c_entryset, "iterator", "()Ljava/util/Iterator;");

    jclass c_iterator = env->FindClass("java/util/Iterator");
    jmethodID id_hasNext = env->GetMethodID(c_iterator, "hasNext", "()Z");
    jmethodID id_next = env->GetMethodID(c_iterator, "next", "()Ljava/lang/Object;");

    jclass c_entry = env->FindClass("java/util/Map$Entry");
    jmethodID id_getKey = env->GetMethodID(c_entry, "getKey", "()Ljava/lang/Object;");
    jmethodID id_getValue = env->GetMethodID(c_entry, "getValue", "()Ljava/lang/Object;");

    jclass c_string = env->FindClass("java/lang/String");
    jmethodID id_toString = env->GetMethodID(c_string, "toString", "()Ljava/lang/String;");

    //All calls above can be statically set

    jobject obj_entrySet = env->CallObjectMethod(map_object, id_entrySet);

    jobject obj_iterator = env->CallObjectMethod(obj_entrySet, id_iterator);

    bool hasNext = (bool) env->CallBooleanMethod(obj_iterator, id_hasNext);

    hardware_factors.clear();
    while (hasNext) {
        jobject entry = env->CallObjectMethod(obj_iterator, id_next);

        jobject key = env->CallObjectMethod(entry, id_getKey);
        jobject value = env->CallObjectMethod(entry, id_getValue);

        jstring jstrKey = (jstring) env->CallObjectMethod(key, id_toString);
        jstring jstrValue = (jstring) env->CallObjectMethod(value, id_toString);

        const char *map_key = env->GetStringUTFChars(jstrKey, 0);
        const char *map_value = env->GetStringUTFChars(jstrValue, 0);

        hardware_factors.insert(std::pair<string, string>(map_key, map_value));

        hasNext = (bool) env->CallBooleanMethod(obj_iterator, id_hasNext);
    }
    for (auto itr = hardware_factors.begin();
         itr != hardware_factors.end(); itr++) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                            "%s : %s", itr->first.c_str(), itr->second.c_str());
    }
}
