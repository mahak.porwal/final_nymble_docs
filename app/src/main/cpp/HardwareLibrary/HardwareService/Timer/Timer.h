//
// Created by fedal on 14/12/20.
//

#ifndef JULIA_ANDROID_TIMER_H
#define JULIA_ANDROID_TIMER_H

#include <chrono>

class Timer {
    typedef std::chrono::high_resolution_clock Clock;
    typedef std::chrono::milliseconds ms;
    typedef std::chrono::microseconds us;
    typedef std::chrono::duration<float> fsec;
private:
    Clock::time_point begin = Clock::now();
    Clock::time_point end = Clock::now();
public:
    void SetBeginTime() {
        this->begin = Clock::now();
    }

    long long int GetTimeElapsed() {
        this->end = Clock::now();

        ms d = std::chrono::duration_cast<ms>(this->end-this->begin);
        //__android_log_print(ANDROID_LOG_DEBUG, "Chef", "timer %lld",d.count());
        return d.count();
    }

    long long int GetTimeElapsedUs(){
        this->end = Clock::now();

        us d = std::chrono::duration_cast<us>(this->end-this->begin);
        //__android_log_print(ANDROID_LOG_DEBUG, "Chef", "timer %lld",d.count());
        return d.count();
    }
};


#endif //JULIA_ANDROID_TIMER_H
