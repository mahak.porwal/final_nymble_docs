//
// Created by fedal on 10/12/20.
//

#ifndef JULIA_ANDROID_I2CENUMS_H
#define JULIA_ANDROID_I2CENUMS_H

typedef enum {
    I2C_READ,
    I2C_WRITE,
    I2C_REGISTER_READ
} Action_e;

typedef enum {
    I2C_BUS_2,
    I2C_BUS_3,
    I2C_BUS_7
} I2cBus_e;
typedef enum {
    /**
     * PWM Generators
     */
    PWM_PCA9685,
    PWM_MCP4725,
    PWM_MAX6650,
    PWM_DS1050Z_INDUCTION,
    PWM_DS1050Z_STIRRER,
    PWM_DS1050Z_EXHAUST,
    PWM_DS1050Z_LED_ILLUMINATION,
    /**
     * GPIO Expander
     */
    GPIO_PCA9535,
    DEFAULT_GPIO_DEVICE,
    /**
     * ADC Sensors
     */
    ADC_INA219B,
    ADC_ADS122C04,
    ADC_ADS1115,
    ADC_MAX11607,
    ADC_MCP3426,
    DEFAULT_ADC_ENUM,
    /**
     * Eeprom devices
     */
    EEPROM_24AA64,

    /**
     * Thermal Camera
     */
    THERMAL_CAMERA_DEVICE,
    /**
     * Potentiometer
     */
    POT_MCP4662,
    /**
     * Counter
     */
    COUNTER_S35770
} I2cDevices_e;

#endif //JULIA_ANDROID_I2CENUMS_H
