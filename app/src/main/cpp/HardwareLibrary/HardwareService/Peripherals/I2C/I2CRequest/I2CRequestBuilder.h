//
// Created by fedal on 10/12/20.
//

#ifndef JULIA_ANDROID_I2CREQUESTBUILDER_H
#define JULIA_ANDROID_I2CREQUESTBUILDER_H

#include "I2CRequest.h"

class I2CRequestBuilder{
public:
    I2CRequest* i2c_request_;
   void BuildI2cRequest();
   I2CRequestBuilder(){
       this->BuildI2cRequest();
   }
    ~I2CRequestBuilder(){
        delete i2c_request_;
    }
};

void I2CRequestBuilder::BuildI2cRequest() {
    this->i2c_request_ = new I2CRequest();
}

#endif //JULIA_ANDROID_I2CREQUESTBUILDER_H
