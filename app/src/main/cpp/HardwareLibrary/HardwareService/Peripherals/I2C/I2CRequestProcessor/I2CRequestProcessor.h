//
// Created by fedal on 10/12/20.
//

#ifndef JULIA_ANDROID_I2CREQUESTPROCESSOR_H
#define JULIA_ANDROID_I2CREQUESTPROCESSOR_H


#include "../I2CBus/I2CBus.h"
#include "../I2CRequest/I2CRequest.h"
#include <map>
class I2CRequestProcessor {
private:


public:
    static std::map<I2cDevices_e, I2cBus_e> device_to_bus_map_;
    static int ProcessI2cRequest(I2CRequest);
    static void PopulateI2cDeviceToBusMap();
    static std::vector<std::string> ScanBus(uint8_t);
};
map<I2cDevices_e, I2cBus_e> I2CRequestProcessor::device_to_bus_map_;

#endif //JULIA_ANDROID_I2CREQUESTPROCESSOR_H
