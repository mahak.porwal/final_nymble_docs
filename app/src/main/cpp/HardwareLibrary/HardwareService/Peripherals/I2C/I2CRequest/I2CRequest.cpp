//
// Created by fedal on 10/12/20.
//

#include "I2CRequest.h"

I2cDevices_e I2CRequest::GetDeviceId() const {
    return device_id_;
}

I2CRequest* I2CRequest::SetDeviceId(I2cDevices_e deviceId) {
    device_id_ = deviceId;
    return this;
}

Action_e I2CRequest::GetAction() const {
    return action_;
}

I2CRequest* I2CRequest::SetAction(Action_e action) {
    action_ = action;
    return this;
}

const uint8_t &I2CRequest::GetSlaveAddress() const {
    return slave_address_;
}

I2CRequest* I2CRequest::SetSlaveAddress(const uint8_t &slaveAddress) {
    slave_address_ = slaveAddress;
    return this;
}

const uint16_t &I2CRequest::GetNumberOfBytes() const {
    return number_of_bytes_;
}

I2CRequest* I2CRequest::SetNumberOfBytes(const uint16_t &numberOfBytes) {
    number_of_bytes_ = numberOfBytes;
    return this;
}

uint8_t *I2CRequest::GetArray() const {
    return array;
}

I2CRequest* I2CRequest::SetArray(uint8_t *array) {
    I2CRequest::array = array;
    return this;
}

uint8_t I2CRequest::GetRegisterAddress() const {
    return register_address_;
}

I2CRequest* I2CRequest::SetRegisterAddress(uint8_t registerAddress) {
    register_address_ = registerAddress;
    return this;
}
