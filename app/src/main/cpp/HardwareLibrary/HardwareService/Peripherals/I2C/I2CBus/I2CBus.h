//
// Created by fedal on 10/12/20.
//

#ifndef JULIA_ANDROID_I2CBUS_H
#define JULIA_ANDROID_I2CBUS_H

#include <pthread.h>
#include "../../../../Error/Error.h"
#include "../I2CEnums.h"
#include <mutex>
#define MAX_DEVICES_I2C_BUS 128
using namespace std;
class I2CBus {
private:
    uint8_t slave_address_;
    bool error_flag_;
    int file_descriptor_;
    //pthread_mutex_t bus_lock_;
    uint8_t available_addresses_[MAX_DEVICES_I2C_BUS];
    std::mutex bus_mutex_lock_;
public:
    const uint8_t *getAvailableAddresses() const;

private:
    uint8_t number_of_available_devices_;
public:
    uint8_t getSlaveAddress() const;

    void setSlaveAddress(uint8_t slaveAddress);

    uint8_t getNumberOfAvailableDevices() const;

    void setNumberOfAvailableDevices(uint8_t numberOfAvailableDevices);

private:
    I2cBus_e i2c_bus_;
    string filename_ = "";

    static int I2cOpen(char *filename);

public:
    std::vector<std::string> ScanAndReturnDevices();

    int ScanBus();

    int EngageBus(Action_e action, uint8_t slave_address, uint16_t number_of_bytes,
                  uint8_t *array);
    int ReadFromRegister(uint8_t slaveAddress,uint8_t registerAddress,uint8_t* bufArray,uint16_t len);

    int GetFileDescriptor() { return this->file_descriptor_; }

    I2CBus(string filename, I2cBus_e bus){
        this->file_descriptor_ = I2cOpen(const_cast<char *>(filename.c_str()));
        this->i2c_bus_ = bus;
        this->filename_ = filename;
        //__android_log_print(ANDROID_LOG_ERROR, "Chef ", "\n file descriptor %d filename %s\n",this->file_descriptor_,filename_.c_str());
        /*if (pthread_mutex_init(&this->bus_lock_, NULL) != 0) {
            //__android_log_print(ANDROID_LOG_ERROR, "Chef ", "\n Chef mutex init has failed\n");
        }*/
        this->ScanBus();
    }
};


#endif //JULIA_ANDROID_I2CBUS_H
