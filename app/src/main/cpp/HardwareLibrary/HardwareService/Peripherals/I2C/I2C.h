//
// Created by fedal on 10/12/20.
//

#ifndef JULIA_ANDROID_I2C_H
#define JULIA_ANDROID_I2C_H


#include "I2CBus/I2CBus.h"
#include "I2CRequestProcessor/I2CRequestProcessor.h"

class I2C {
public:
    I2CBus *i2c_2_;
    I2CBus *i2c_3_;
    I2CBus *i2c_7_;
    I2C() {
        string i2c_2_filename = "/dev/i2c-2";
        string i2c_3_filename = "/dev/i2c-3";
        string i2c_7_filename = "/dev/i2c-7";
        this->i2c_2_ = new I2CBus(i2c_2_filename, I2C_BUS_2);
        this->i2c_3_ = new I2CBus(i2c_3_filename, I2C_BUS_3);
        this->i2c_7_ = new I2CBus(i2c_7_filename, I2C_BUS_7);
        I2CRequestProcessor::PopulateI2cDeviceToBusMap();
    }

    ~I2C(){
            delete this->i2c_2_;
            delete this->i2c_3_;
            delete this->i2c_7_;
    }

    static void ScanTest();

    static void Pca9685Test();

    static void Ds1050zTest();

    static void Ina219();

    static void Ads122c04();

    static void Ads1115();

};
extern I2C *i2c;


#endif //JULIA_ANDROID_I2C_H
