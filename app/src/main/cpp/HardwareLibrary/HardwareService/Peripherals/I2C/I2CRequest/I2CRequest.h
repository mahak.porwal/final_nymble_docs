//
// Created by fedal on 10/12/20.
//

#ifndef JULIA_ANDROID_I2CREQUEST_H
#define JULIA_ANDROID_I2CREQUEST_H


#include "../I2CEnums.h"

class I2CRequest {
private:
    I2cDevices_e device_id_;
public:
    I2cDevices_e GetDeviceId() const;

    I2CRequest * SetDeviceId(I2cDevices_e deviceId);

    Action_e GetAction() const;

    I2CRequest * SetAction(Action_e action);

    const uint8_t &GetSlaveAddress() const;

    I2CRequest * SetSlaveAddress(const uint8_t &slaveAddress);

    const uint16_t &GetNumberOfBytes() const;

    I2CRequest * SetNumberOfBytes(const uint16_t &numberOfBytes);

    uint8_t *GetArray() const;

    I2CRequest * SetArray(uint8_t *array);

private:
    Action_e action_;
    uint8_t slave_address_;
    uint8_t register_address_;
public:
    uint8_t GetRegisterAddress() const;

    I2CRequest * SetRegisterAddress(uint8_t registerAddress);


private:
    uint16_t number_of_bytes_;
    uint8_t *array;

};


#endif //JULIA_ANDROID_I2CREQUEST_H
