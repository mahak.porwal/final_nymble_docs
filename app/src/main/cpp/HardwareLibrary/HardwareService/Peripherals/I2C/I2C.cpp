//
// Created by fedal on 10/12/20.
//

#include "I2C.h"
#include "../Pwm/Devices/PCA9685/Pca9685.h"
#include "../Pwm/Devices/DS1050Z/DS1050Z.h"
#include "../Gpio/Devices/PCA9535/Pca9535.h"
#include "../Adc/Devices/INA219B/INA219B.h"
#include "../Adc/Devices/ADS122C04/ADS122C04.h"
#include "../Adc/Devices/ADS1115/ADS1115.h"

I2C* i2c = nullptr;


void I2C::ScanTest() {
    i2c->i2c_2_->ScanBus();
    i2c->i2c_3_->ScanBus();
    __android_log_print(ANDROID_LOG_DEBUG, "Chef", "i2c-2 number of devices %d",i2c->i2c_2_->getNumberOfAvailableDevices());
    __android_log_print(ANDROID_LOG_DEBUG, "Chef", "i2c-3 number of devices %d",i2c->i2c_3_->getNumberOfAvailableDevices());
}

void I2C::Pca9685Test() {
    try {
        Pca9685* pca9685 = new Pca9685(0x42,PWM_PCA9685);
        pca9685->SetDutyCycle(CHANNEL_0,50.0);
        pca9685->SetDutyCycle(CHANNEL_1,55.0);
        pca9685->SetDutyCycle(CHANNEL_2,60.0);
        pca9685->SetDutyCycle(CHANNEL_3,65.0);
        pca9685->SetDutyCycle(CHANNEL_4,70.0);
        pca9685->SetDutyCycle(CHANNEL_5,75.0);
        pca9685->SetDutyCycle(CHANNEL_6,80.0);
        pca9685->SetDutyCycle(CHANNEL_7,85.0);
        pca9685->SetDutyCycle(CHANNEL_8,90.0);
        pca9685->SetDutyCycle(CHANNEL_9,95.0);
        pca9685->SetDutyCycle(CHANNEL_10,20.0);
        pca9685->SetDutyCycle(CHANNEL_11,15.0);
        pca9685->SetDutyCycle(CHANNEL_12,25.0);
        pca9685->SetDutyCycle(CHANNEL_13,30.0);
        pca9685->SetDutyCycle(CHANNEL_14,35.0);
        pca9685->SetDutyCycle(CHANNEL_15,40.0);
    }
    catch(Error e ){

    }

}

void I2C::Ds1050zTest() {
    DS1050Z* ExhaustFanPwm = new DS1050Z(0x28,PWM_DS1050Z_EXHAUST);
    DS1050Z* LedPwm = new DS1050Z(0x2F,PWM_DS1050Z_LED_ILLUMINATION);
    DS1050Z* StirrerPwm = new DS1050Z(0x2E,PWM_DS1050Z_STIRRER);
    Pca9535* GpioContorls = new Pca9535(0x20,GPIO_PCA9535);
    /*ExhaustFanPwm->SetDutyCycle(0,25);
    ExhaustFanPwm->SetDutyCycle(0,50);
    ExhaustFanPwm->SetDutyCycle(0,75);
    ExhaustFanPwm->SetDutyCycle(0,100);*/

    StirrerPwm->SetDutyCycle(0,25);
    StirrerPwm->SetDutyCycle(0,50);
    StirrerPwm->SetDutyCycle(0,75);
    StirrerPwm->SetDutyCycle(0,100);


    LedPwm->SetDutyCycle(0,25);
    LedPwm->SetDutyCycle(0,50);
    LedPwm->SetDutyCycle(0,75);
    LedPwm->SetDutyCycle(0,100);


}

void I2C::Ina219() {
    INA219B* ina219 = new INA219B(0x40,ADC_INA219B);
    float curr = ina219->ReadChannel(0);
    curr = ina219->ReadChannel(0);
    curr = ina219->ReadChannel(0);
    curr = ina219->ReadChannel(0);

}

void I2C::Ads122c04() {
    ADS122C04* ads122C04 = new ADS122C04(0x47,ADC_ADS122C04);
    ads122C04->LoadCell_FactorCalc(ads122C04->macro_weight_);
   /* timer->GetTimeElapsed();
    timer->SetBeginTime();*/
}

void I2C::Ads1115() {
    ADS1115* ads1115 = new ADS1115(0x48,ADC_ADS1115);
    float val;
    val = ads1115->ReadChannel(ADS_1115_CHANNEL_0);
    val = ads1115->ReadChannel(ADS_1115_CHANNEL_1);
    val = ads1115->ReadChannel(ADS_1115_CHANNEL_2);
    val = ads1115->ReadChannel(ADS_1115_CHANNEL_3);
}
