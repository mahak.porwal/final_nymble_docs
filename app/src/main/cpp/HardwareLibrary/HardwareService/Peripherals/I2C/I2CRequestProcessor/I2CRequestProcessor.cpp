//
// Created by fedal on 10/12/20.
//

#include "I2CRequestProcessor.h"

void I2CRequestProcessor::PopulateI2cDeviceToBusMap() {
    I2CRequestProcessor::device_to_bus_map_.insert({PWM_PCA9685, I2C_BUS_2});
    I2CRequestProcessor::device_to_bus_map_.insert({PWM_DS1050Z_EXHAUST, I2C_BUS_2});//NOT FITTED
    I2CRequestProcessor::device_to_bus_map_.insert({PWM_MAX6650, I2C_BUS_2});
    I2CRequestProcessor::device_to_bus_map_.insert({PWM_DS1050Z_LED_ILLUMINATION, I2C_BUS_2});
    I2CRequestProcessor::device_to_bus_map_.insert({PWM_DS1050Z_STIRRER, I2C_BUS_2});
    I2CRequestProcessor::device_to_bus_map_.insert({GPIO_PCA9535, I2C_BUS_2});
    I2CRequestProcessor::device_to_bus_map_.insert({ADC_INA219B, I2C_BUS_3});
    I2CRequestProcessor::device_to_bus_map_.insert({ADC_ADS122C04, I2C_BUS_3});
    I2CRequestProcessor::device_to_bus_map_.insert({ADC_ADS1115, I2C_BUS_3});
    I2CRequestProcessor::device_to_bus_map_.insert({PWM_DS1050Z_INDUCTION, I2C_BUS_3});
    I2CRequestProcessor::device_to_bus_map_.insert({POT_MCP4662, I2C_BUS_3});
    I2CRequestProcessor::device_to_bus_map_.insert({EEPROM_24AA64, I2C_BUS_7});
    I2CRequestProcessor::device_to_bus_map_.insert({THERMAL_CAMERA_DEVICE, I2C_BUS_7});
    I2CRequestProcessor::device_to_bus_map_.insert({ADC_MCP3426, I2C_BUS_2});
    I2CRequestProcessor::device_to_bus_map_.insert({ADC_MAX11607, I2C_BUS_3});
    I2CRequestProcessor::device_to_bus_map_.insert({COUNTER_S35770, I2C_BUS_2});
}

int I2CRequestProcessor::ProcessI2cRequest(I2CRequest i2CRequest) {
    I2cBus_e bus = I2CRequestProcessor::device_to_bus_map_.at(i2CRequest.GetDeviceId());
    I2CBus *i2c_bus = nullptr;
    switch (bus) {
        case I2C_BUS_2:
            i2c_bus = HardwareService::i2c_->i2c_2_;
            break;
        case I2C_BUS_3:
            i2c_bus = HardwareService::i2c_->i2c_3_;
            break;
        case I2C_BUS_7:
            i2c_bus = HardwareService::i2c_->i2c_7_;
            break;
    }

    int result = -1;
    switch (i2CRequest.GetAction()) {
        case I2C_READ:
            result = i2c_bus->EngageBus(I2C_READ, i2CRequest.GetSlaveAddress(),
                                        i2CRequest.GetNumberOfBytes(), i2CRequest.GetArray());
            break;
        case I2C_WRITE:
            result = i2c_bus->EngageBus(I2C_WRITE, i2CRequest.GetSlaveAddress(),
                                        i2CRequest.GetNumberOfBytes(), i2CRequest.GetArray());
            break;
        case I2C_REGISTER_READ:
            result = i2c_bus->ReadFromRegister(i2CRequest.GetSlaveAddress(),
                                               i2CRequest.GetRegisterAddress(),
                                               i2CRequest.GetArray(),
                                               i2CRequest.GetNumberOfBytes());
            break;
    }
    return result;
}

std::vector<std::string> I2CRequestProcessor::ScanBus(uint8_t bus) {
    I2CBus *i2CBus = nullptr;
    switch (bus) {
        case 2:
            i2CBus = HardwareService::i2c_->i2c_2_;
            break;
        case 3:
            i2CBus = HardwareService::i2c_->i2c_3_;
            break;
        case 7:
            i2CBus = HardwareService::i2c_->i2c_7_;
            break;
        default:
            break;
    }
    if (nullptr == i2CBus) {
        throw new Error();
    }
    return i2CBus->ScanAndReturnDevices();
}
