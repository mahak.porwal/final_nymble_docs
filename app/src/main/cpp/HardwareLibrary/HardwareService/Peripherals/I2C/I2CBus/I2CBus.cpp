//
// Created by fedal on 10/12/20.
//

#include "I2CBus.h"

int I2CBus::ScanBus() {
    uint8_t devicesFound = 0;
    __android_log_print(ANDROID_LOG_ERROR, "Chef", "I2C scan started");
    for (uint8_t i = 0; i < 128; i++) {
        int detection = i2c_detect(this->file_descriptor_, i);
        if (detection >= 0) {
            *(this->available_addresses_ + (++devicesFound)) = i;
            __android_log_print(ANDROID_LOG_ERROR, "Chef", "Device 0x%02X found on %s ", i,
                                this->filename_.c_str());
        }
    }
    __android_log_print(ANDROID_LOG_ERROR, "Chef", "I2C scan finished");
    this->number_of_available_devices_ = devicesFound;
    return devicesFound;
}

int I2CBus::EngageBus(Action_e action, uint8_t slave_address, uint16_t number_of_bytes,
                      uint8_t *array) {
    std::lock_guard<std::mutex> engage_bus(this->bus_mutex_lock_);
    int result = -1;
    switch (action) {

        case I2C_WRITE:
            result = i2c_write(this->file_descriptor_, slave_address, number_of_bytes, array);
            if (result <= 0) {
                throw (Error());
            }
            break;
        case I2C_READ:
            result = i2c_read(this->file_descriptor_, slave_address, number_of_bytes, array);
            if (result <= 0) {
                throw (Error());
            }
            break;
        default:
            break;
    }
    return result;
}

int I2CBus::I2cOpen(char *filename) {
    int file = open(filename, O_RDWR);
    __android_log_print(ANDROID_LOG_ERROR, "i2c-driver", "opened file %s with %d", filename, file);
    if (file < 0) {
        throw (Error());
    }
    return file;
}

uint8_t I2CBus::getSlaveAddress() const {
    return slave_address_;
}

void I2CBus::setSlaveAddress(uint8_t slaveAddress) {
    std::lock_guard<std::mutex> set_slave_lock(this->bus_mutex_lock_);
    slave_address_ = slaveAddress;
}

uint8_t I2CBus::getNumberOfAvailableDevices() const {
    return number_of_available_devices_;
}

void I2CBus::setNumberOfAvailableDevices(uint8_t numberOfAvailableDevices) {
    number_of_available_devices_ = numberOfAvailableDevices;
}

const uint8_t *I2CBus::getAvailableAddresses() const {
    return available_addresses_;
}

int I2CBus::ReadFromRegister(uint8_t slaveAddress, uint8_t registerAddress,
                             uint8_t *bufArray, uint16_t len) {
    std::lock_guard<std::mutex> read_from_register(this->bus_mutex_lock_);
    int result = i2c_read_from_reg(this->file_descriptor_, slaveAddress, registerAddress, bufArray,
                                   len);
    if (result <= 0) {
        throw (Error());
    }
    return result;
}

std::vector<std::string> I2CBus::ScanAndReturnDevices() {
    std::vector<std::string> devices;
    std::lock_guard<std::mutex> engage_bus(this->bus_mutex_lock_);
    for (uint8_t i = 0; i < 128; i++) {
        int detection = i2c_detect(this->file_descriptor_, i);
        if (detection >= 0) {
            devices.push_back(std::to_string(i));
            __android_log_print(ANDROID_LOG_ERROR, "Chef", "Device 0x%02X found on %s ", i,
                                this->filename_.c_str());
        }
    }
    return devices;
}

