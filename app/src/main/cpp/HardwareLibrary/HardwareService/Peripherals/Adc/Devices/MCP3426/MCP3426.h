//
// Created by fedal on 06/10/21.
//

#ifndef RECIPEENGINE_MCP3426_H
#define RECIPEENGINE_MCP3426_H

typedef enum {
    MCP3426_CHANNEL_0,
    MCP3426_CHANNEL_1
} MCP3462Channels_e;
typedef enum {
    MCP3426_240_SPS,
    MCP3426_60_SPS,
    MCP3426_15_SPS
} MCP3462SampleRates_e;

class MCP3426 : public Adc {
    const uint8_t ONE_SHOT_CHANNEL_0_PGA_1_SPS_15 = 0x18;
    const uint8_t ONE_SHOT_CHANNEL_1_PGA_1_SPS_15 = 0xA8;
    const double LSB_VALUE_SPS_15 = 0.0000625;
    const double LSB_VALUE_SPS_60 = 0.0002500;
    const double LSB_VALUE_SPS_240 = 0.001000;
    const MCP3462SampleRates_e SAMPLE_RATE = MCP3426_15_SPS;
    const bool IS_DIFFERENTIAL_MODE = false;
    const int PGA = 1;
    const double FULL_SCALE_READING = 0xFFFF;
    const int MAX_TIME_TO_GET_READY_MS = 1000;

    int MuxAdc(MCP3462Channels_e);

    float ReadAdc();

    float CalcDifferentialVoltage(std::vector<uint8_t>);

    float CalcAbsoluteVoltage(std::vector<uint8_t>);

    uint8_t IgnoreRepeatedMSBBytes(uint8_t);

    int CalculateTwosCompliment(uint16_t);

    float CalculateVoltage(int,MCP3462Channels_e);

    pthread_mutex_t device_lock_;

    Timer timer_;

    MCP3462Channels_e current_channel_;
public:
    float ReadChannel(uint8_t);

    MCP3426(uint8_t address, I2cDevices_e device_id) {
        this->address_ = address;
        this->device_id_ = device_id;
        if (pthread_mutex_init(&this->device_lock_, NULL) != 0) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef ",
                                "\n Chef mcp3426 mutex init has failed\n");
        }
    }
};


#endif //RECIPEENGINE_MCP3426_H
