//
// Created by fedal on 06/10/21.
//

#include "MCP3426.h"

int MCP3426::MuxAdc(MCP3462Channels_e channel) {
    uint8_t config_reg_value;
    switch (channel) {
        case MCP3426_CHANNEL_0:
            config_reg_value = ONE_SHOT_CHANNEL_0_PGA_1_SPS_15;
            break;
        case MCP3426_CHANNEL_1:
            config_reg_value = ONE_SHOT_CHANNEL_1_PGA_1_SPS_15;
            break;
        default:
            throw new Error();
    }
    this->current_channel_ = channel;
    uint8_t tx_buff[1];
    tx_buff[0] = config_reg_value;
    I2CRequestBuilder i2c_request_builder;
    i2c_request_builder.i2c_request_
            ->SetAction(I2C_WRITE)
            ->SetDeviceId(ADC_MCP3426)
            ->SetNumberOfBytes(1)
            ->SetArray(tx_buff)
            ->SetSlaveAddress(this->address_);
    return I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));
}

float MCP3426::ReadAdc() {
    uint8_t rx_buff[3];
    I2CRequestBuilder i2c_request_builder;
    i2c_request_builder.i2c_request_
            ->SetAction(I2C_READ)
            ->SetDeviceId(ADC_MCP3426)
            ->SetNumberOfBytes(3)
            ->SetArray(rx_buff)
            ->SetSlaveAddress(this->address_);
    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));
    std::vector<uint8_t> register_bytes;
    register_bytes.push_back(rx_buff[0]);
    register_bytes.push_back(rx_buff[1]);
    if (IS_DIFFERENTIAL_MODE) {
        return this->CalcDifferentialVoltage(register_bytes);
    } else {
        return this->CalcAbsoluteVoltage(register_bytes);
    }
}

float MCP3426::ReadChannel(uint8_t channel) {
    pthread_mutex_lock(&this->device_lock_);
    this->MuxAdc(static_cast<MCP3462Channels_e>(channel));
    uint8_t rx_buff[3];
    this->timer_.SetBeginTime();
    do {
        I2CRequestBuilder i2c_request_builder;
        i2c_request_builder.i2c_request_
                ->SetAction(I2C_READ)
                ->SetDeviceId(ADC_MCP3426)
                ->SetNumberOfBytes(3)
                ->SetArray(rx_buff)
                ->SetSlaveAddress(this->address_);
        I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));
    } while ((rx_buff[2] & 0x80) && (this->timer_.GetTimeElapsed()
                                     < MAX_TIME_TO_GET_READY_MS));
    if ((rx_buff[2] & 0x80)) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef ",
                            "\nmcp3426 took greater than a second to get ready\n");
        pthread_mutex_unlock(&this->device_lock_);
        throw new Error(MCP3427_NOT_READY);
    }
    float reading = this->ReadAdc();
    pthread_mutex_unlock(&this->device_lock_);
    __android_log_print(ANDROID_LOG_ERROR, "ServoMacro ",
                        "\nanalog read %f\n", reading);
    return reading;
}

float MCP3426::CalcDifferentialVoltage(std::vector<uint8_t> register_bytes) {
    uint8_t upper_byte = register_bytes.at(0);
    uint8_t lower_byte = register_bytes.at(1);
    uint8_t modified_upper_byte = this->IgnoreRepeatedMSBBytes(upper_byte);
    uint16_t raw_value = modified_upper_byte << 8 | lower_byte;
    int twos_compliment_value = this->CalculateTwosCompliment(raw_value);
    return this->CalculateVoltage(twos_compliment_value, this->current_channel_);
}

float MCP3426::CalcAbsoluteVoltage(std::vector<uint8_t> register_bytes) {
    uint8_t upper_byte = register_bytes.at(0);
    uint8_t lower_byte = register_bytes.at(1);
    __android_log_print(ANDROID_LOG_ERROR, "ServoMacro ",
                        "\nanalog read upper byte %d lower byte %d\n", upper_byte, lower_byte);
    uint8_t modified_upper_byte = this->IgnoreRepeatedMSBBytes(upper_byte);
    uint16_t raw_value = (modified_upper_byte << 8) | lower_byte;
    return this->CalculateVoltage((int) raw_value, this->current_channel_);
}

uint8_t MCP3426::IgnoreRepeatedMSBBytes(uint8_t upper_byte) {
    uint8_t modified_upper_byte = upper_byte;
    uint8_t num_of_bytes_to_ignore = 0;
    switch (SAMPLE_RATE) {
        case MCP3426_60_SPS:
            num_of_bytes_to_ignore = 2;
            break;
        case MCP3426_240_SPS:
            num_of_bytes_to_ignore = 4;
            break;
        case MCP3426_15_SPS:
        default:
            break;
    }
    modified_upper_byte <<= num_of_bytes_to_ignore;
    modified_upper_byte >>= num_of_bytes_to_ignore;

    return modified_upper_byte;
}

int MCP3426::CalculateTwosCompliment(uint16_t register_value) {
    uint16_t compliment_bit_mask = 0x00;
    int result = 0;
    switch (SAMPLE_RATE) {
        case MCP3426_60_SPS:
            compliment_bit_mask = 0x2000;
            break;
        case MCP3426_240_SPS:
            compliment_bit_mask = 0x0100;
            break;
        case MCP3426_15_SPS:
            compliment_bit_mask = 0x8000;
        default:
            break;
    }
    if ((compliment_bit_mask & register_value) == 1) {
        //negative value
        result = register_value - 2 * compliment_bit_mask;
    } else {
        //positive value
        result = register_value;
    }
    return result;
}

float MCP3426::CalculateVoltage(int register_integer_value, MCP3462Channels_e channel) {
    float voltage = 0.0f;
    switch (SAMPLE_RATE) {
        case MCP3426_60_SPS:
            voltage = register_integer_value * (LSB_VALUE_SPS_60 / PGA);
            break;
        case MCP3426_240_SPS:
            voltage = register_integer_value * (LSB_VALUE_SPS_240 / PGA);
            break;
        case MCP3426_15_SPS:
            if (MCP3426_CHANNEL_1 == channel) {
                voltage = (FULL_SCALE_READING - register_integer_value) * (LSB_VALUE_SPS_15 / PGA);
            } else {
                voltage = (register_integer_value) * (LSB_VALUE_SPS_15 / PGA);
            }
        default:
            break;
    }
    __android_log_print(ANDROID_LOG_ERROR, "ServoMacro ",
                        "\nregister value %d , voltage %f\n", register_integer_value, voltage);
    return voltage;
}
