//
// Created by fedal on 9/12/20.
//

#include "INA219B.h"


float INA219B::ReadChannel(uint8_t) {
    return this->INA219GetCurrentValue();
}

float INA219B::INA219GetCurrentValue() {
    float value;
    uint16_t temp;
    temp = this->ReadRegisterINA219(INA219_ADDRESS_CURRENT_REG);
    temp &= MASK_FIRST_BIT;
    value = temp;//in mA
    return value;
}

void INA219B::ConfigINA219() {
    uint8_t Amp_RxBuffer[2];
    uint8_t Amp_TxBuffer[3];
    int result = -1;
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();

    uint16_t config_val = INA219_BUSVOLTAGE_RANGE |
                          INA219_PGA_SHUNT |
                          INA219_BADC |
                          INA219_SADC |
                          INA219_MODE;
    Amp_TxBuffer[0] = INA219_ADDRESS_CONFIGURATION_REG;
    Amp_TxBuffer[1] = config_val >> HIGHER_BYTE;
    Amp_TxBuffer[2] = config_val & LOWER_BYTE;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_INA219B)->SetNumberOfBytes(3)->SetArray(Amp_TxBuffer)->SetSlaveAddress(
            address_);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    //I2C_Transmit(INA219_ADDRESS, Amp_TxBuffer, 3);

    uint16_t calibration_val;
    calibration_val = 1638;//8192;
    Amp_TxBuffer[0] = INA219_ADDRESS_CALIBRATION_REG;
    Amp_TxBuffer[1] = calibration_val >> HIGHER_BYTE;
    Amp_TxBuffer[2] = calibration_val & LOWER_BYTE;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_INA219B)->SetNumberOfBytes(3)->SetArray(Amp_TxBuffer)->SetSlaveAddress(
            address_);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    //I2C_Transmit(INA219_ADDRESS, Amp_TxBuffer, 3);
}

uint16_t INA219B::ReadRegisterINA219(uint8_t Read_Register_Address) {
    uint8_t Amp_RxBuffer[2];
    uint8_t Amp_TxBuffer[3];
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    int result = -1;
    //uint16_t calibration_val = 1638;
    /*calibration_val = 819;
    Amp_TxBuffer[0] = INA219_ADDRESS_CALIBRATION_REG;
    Amp_TxBuffer[1] = calibration_val>>HIGHER_BYTE;
    Amp_TxBuffer[2] = calibration_val&LOWER_BYTE;
    I2C_Transmit(INA219_ADDRESS,Amp_TxBuffer,3);*/


    Amp_TxBuffer[0] = Read_Register_Address;

    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_INA219B)->SetNumberOfBytes(1)->SetArray(Amp_TxBuffer)->SetSlaveAddress(
            address_);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));

    i2c_request_builder->i2c_request_
            ->SetAction(I2C_READ)
            ->SetDeviceId(ADC_INA219B)
            ->SetNumberOfBytes(2)
            ->SetArray(Amp_RxBuffer)
            ->SetSlaveAddress(address_);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));

    return ((Amp_RxBuffer[0] << 8) | Amp_RxBuffer[1]);
}
