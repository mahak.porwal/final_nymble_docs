//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_ADCREQUESTBUILDER_H
#define JULIA_ANDROID_ADCREQUESTBUILDER_H

#include "AdcRequest.h"

class AdcRequestBuilder{
public:
    AdcRequest* adc_request;
    AdcRequestBuilder(){
        this->adc_request = new AdcRequest;
    }
    ~AdcRequestBuilder(){
        delete this->adc_request;
    }
};


#endif //JULIA_ANDROID_ADCREQUESTBUILDER_H
