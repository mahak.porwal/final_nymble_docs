//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_SOM_ADC_H
#define JULIA_ANDROID_SOM_ADC_H


class SOM_Adc : public Adc {
    float ReadChannel(uint8_t);

public:
    SOM_Adc(SOM_Adc_Channels_e channel) {
        this->som_channel_ = channel;
    }
};


#endif //JULIA_ANDROID_SOM_ADC_H
