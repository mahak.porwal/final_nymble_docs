//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_INA219B_H
#define JULIA_ANDROID_INA219B_H


#include "../../Adc.h"

typedef enum {
    INA219_ADDRESS_CONFIGURATION_REG = 0x00,
    INA219_ADDRESS_SHUNT_VOLTAGE_REG = 0x01,
    INA219_ADDRESS_BUS_VOLTAGE_REG = 0x02,
    INA219_ADDRESS_POWER_REG = 0x03,
    INA219_ADDRESS_CURRENT_REG = 0x04,
    INA219_ADDRESS_CALIBRATION_REG = 0x05
} Ina219_Addresses_e;

enum {
    INA219_RESET = (0x8000),//RESET INA219
    INA219_BUSVOLTAGE_RANGE = (0x2000),//BUS VOLTAGE RANGE SET AT 32V
    INA219_PGA_SHUNT = (0x1800),//PGA GAIN SET AT 320mV
    INA219_BADC = (0x0180),//BUS ADC SET AT 12-BIT
    INA219_SADC = (0x0018),//SHUNT ADC SET AT 12-BIT
    INA219_MODE = (0x0007)//MODE SET AT SHUNT AND BUS CONTINUOUS
};

enum Data_Formatting {
    HIGHER_BYTE = 8,
    LOWER_BYTE = 0xFF,
    MASK_FIRST_BIT = ~(0x8000),
    REDUNDANT_BITS = 3,

};

class INA219B : public Adc {

public:
    INA219B(uint8_t address, I2cDevices_e device_id) {
        this->address_ = address;
        this->device_id_ = device_id;
        this->ConfigINA219();
    }

    float ReadChannel(uint8_t);

private:
    float INA219GetCurrentValue();

    uint16_t ReadRegisterINA219(uint8_t Read_Register_Address);

    void ConfigINA219();
};


#endif //JULIA_ANDROID_INA219B_H
