//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_ADC_H
#define JULIA_ANDROID_ADC_H


#include "AdcEnums.h"

class Adc {
protected:
    uint8_t address_;
    I2cDevices_e device_id_ = DEFAULT_ADC_ENUM;
    SOM_Adc_Channels_e som_channel_ = DEFAULT_ADC_CHANNEL;
public:
    I2cDevices_e getDeviceId() const;

    void setDeviceId(I2cDevices_e deviceId);

public:
    virtual float ReadChannel(uint8_t) = 0;

    virtual double GetAdcFactor() {
        return 0;
    }

    virtual ~Adc() {}
};


#endif //JULIA_ANDROID_ADC_H
