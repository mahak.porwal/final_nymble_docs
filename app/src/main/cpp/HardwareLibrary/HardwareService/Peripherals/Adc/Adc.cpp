//
// Created by fedal on 9/12/20.
//

#include "Adc.h"

I2cDevices_e Adc::getDeviceId() const {
    return device_id_;
}

void Adc::setDeviceId(I2cDevices_e deviceId) {
    device_id_ = deviceId;
}
