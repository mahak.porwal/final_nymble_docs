//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_ADS1115_H
#define JULIA_ANDROID_ADS1115_H


#include "../../Adc.h"

typedef enum {
    ADS_1115_CHANNEL_0,
    ADS_1115_CHANNEL_1,
    ADS_1115_CHANNEL_2,
    ADS_1115_CHANNEL_3
} Ads1115Channels_e;

class ADS1115 : public Adc {
    uint16_t ADC_CONFIG_VALUES[4] = {0x4083, 0x5083, 0x6083, 0x7083};

    int MuxAdc(Ads1115Channels_e);

    float ReadAdc();

    pthread_mutex_t device_lock_;

public:
    float ReadChannel(uint8_t);

    ADS1115(uint8_t address, I2cDevices_e device_id) {
        this->address_ = address;
        this->device_id_ = device_id;
        if (pthread_mutex_init(&this->device_lock_, NULL) != 0) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef ",
                                "\n Chef ads1115 mutex init has failed\n");
        }
    }
};


#endif //JULIA_ANDROID_ADS1115_H
