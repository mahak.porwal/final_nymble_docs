//
// Created by fedal on 9/12/20.
//

#include "ADS122C04.h"

float ADS122C04::ReadChannel(uint8_t) {
    LoadCell_Read(this->macro_weight_);
    return this->macro_weight_->loadcell_->loadcell_reading_->raw_data;
}
double
ADS122C04::StatisticalData_Calc(MacroWeight *macroWeight, statistical_type_e stat_type,
                                loadcell_regvalue_e input_config,
                                uint32_t time_duration, loadcell_config_e lc_config) {
    Timer* timer = new Timer;
    double result = 0;
    uint32_t raw_data, min = 0xffffffff, max = 0;
    double temp = 0, sample_count = 0;
    long long int time_elapsed_ms = 0;
    LoadCell *loadcell = new LoadCell;
    loadcell->configuration_value_ = input_config;
    //Change the sampling rate here
    Loadcell_Init(macroWeight->loadcell_,REG1_DR_20_NOR);
    Config_LoadCell(loadcell);

    loadcell->loadcell_offset_->average_ = macroWeight->loadcell_->loadcell_offset_->average_;

    timer->SetBeginTime();

    while (time_elapsed_ms < time_duration) {
        double filtered_val;
        raw_data = ADS_Reference_Read(loadcell);//ADS_Calibration_Read(&loadcell);
        if (stat_type == RMS_DATA) {
            LoadCell_RawData_Process(raw_data, loadcell, &filtered_val);
            filtered_val *= macroWeight->adc_factor_;
        }

        sample_count++;
        switch (stat_type) {
            case RMS_DATA: {
                temp += filtered_val * filtered_val;
            }
                break;
            case P2P_DATA: {
                if (raw_data < min) {
                    min = raw_data;
                }
                if (raw_data > max) {
                    max = raw_data;
                }
            }
                break;
            case AVERAGE_DATA: {
                temp += raw_data;
            }
                break;
        }
        time_elapsed_ms = timer->GetTimeElapsed();
    }

    switch (stat_type) {
        case RMS_DATA: {
            result = sqrt(temp / sample_count);
        }
            break;
        case P2P_DATA: {
            result = (double) (max - min);
        }
            break;
        case AVERAGE_DATA: {
            result = (temp / sample_count);
        }
            break;
    }
    delete timer;
    return result;
}

void ADS122C04::Loadcell_Init(LoadCell *loadCell, loadcell_regvalue_e reg1_value) {
    uint8_t LCi2cRxBuffer[3] = {0x0, 0x0, 0x0};
    uint8_t LCi2cTxBuffer[2] = {0x0, 0x0};
    uint8_t address = 0;
    int result = -1;
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    /*
     * RESET_ADS
     */
    LCi2cTxBuffer[0] = RESET_LOADCELL;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
            LCi2cTxBuffer)->SetNumberOfBytes(1);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    /*
     * Set_Config_Reg 1
     */
    LCi2cTxBuffer[0] = WRITE_CONFIG_REG_1;
    LCi2cTxBuffer[1] = reg1_value;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
            LCi2cTxBuffer)->SetNumberOfBytes(2);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    /*
     * Set_Config_Reg 2
     */
    LCi2cTxBuffer[0] = WRITE_CONFIG_REG_2;
    LCi2cTxBuffer[1] = REG2_VALUE;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
            LCi2cTxBuffer)->SetNumberOfBytes(2);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    /*
     * Set_Config_Reg 3
     */
    LCi2cTxBuffer[0] = WRITE_CONFIG_REG_3;
    LCi2cTxBuffer[1] = REG3_VALUE;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
            LCi2cTxBuffer)->SetNumberOfBytes(2);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
}

void ADS122C04::Config_LoadCell(LoadCell *loadcell) {
    uint8_t LCi2cTxBuffer[2] = {0x0, 0x0};
    int result = -1;
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    /*
	 * Set Config_Reg 0
	 */
    LCi2cTxBuffer[0] = WRITE_CONFIG_REG_0;
    LCi2cTxBuffer[1] = loadcell->configuration_value_;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
            LCi2cTxBuffer)->SetNumberOfBytes(2);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
}

uint32_t ADS122C04::ADS_Reference_Read(LoadCell *loadcell) {

    Timer* timer = new Timer;
    uint8_t LCi2cRxBuffer[3] = {0x0, 0x0, 0x0};
    uint8_t LCi2cTxBuffer[2] = {0x0, 0x0};
    int result = -1;
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    /*
     * START CONVERSION
     */
    LCi2cTxBuffer[0] = START_CONVERSION;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
            LCi2cTxBuffer)->SetNumberOfBytes(1);

    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    bool flag_f = 0;
    timer->SetBeginTime();
    while (timer->GetTimeElapsed() < 60) {
        /*
         * READ CONFIGURATION REGISTER 2
         */
        LCi2cTxBuffer[0] = READ_CONFIG_REG_2;
        i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
                ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
                LCi2cTxBuffer)->SetNumberOfBytes(1);

        result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));

        /*
         * RECEIVE CONFIGURATION REGISTER 2 DATA
         */

        //I2C_Receive(loadcell->address, (uint8_t *) LCi2cRxBuffer, 1);
        i2c_request_builder->i2c_request_->SetAction(I2C_READ)->SetDeviceId(
                ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
                LCi2cRxBuffer)->SetNumberOfBytes(1);

        result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));

        if (LCi2cRxBuffer[0] >> 7 == 0b1) {
            flag_f = 1;
        }

    }

    if (flag_f == 1) {
        flag_f = 0;
        /*
         * TRANSMIT RDATA COMMAND
         */
        LCi2cTxBuffer[0] = R_DATA;
        //I2C_Transmit(loadcell->address_, (uint8_t *) LCi2cTxBuffer, 1);
        i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
                ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
                LCi2cTxBuffer)->SetNumberOfBytes(1);
        result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
        /*
         * READ SENSOR DATA
         */
        //I2C_Receive(loadcell->address_, (uint8_t *) LCi2cRxBuffer, 3);
        i2c_request_builder->i2c_request_->SetAction(I2C_READ)->SetDeviceId(
                ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
                LCi2cRxBuffer)->SetNumberOfBytes(3);
        result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
        delete timer;

        return ((LCi2cRxBuffer[0] << 16) | (LCi2cRxBuffer[1] << 8) | (LCi2cRxBuffer[2]));

    }
    delete timer;
    return 0;
}

void ADS122C04::LoadCell_RawData_Process(uint32_t temp_data, LoadCell *loadcell, double *result) {
    static loadcell_statesign_e local_loadcell_state_sign = DEFAULT_LOAD_CELL_STATE_SIGN;

    if ((temp_data >= 0) && (temp_data <= FULLSCALE_POSITIVE)) {
        local_loadcell_state_sign = POSITIVE;
    } else if ((temp_data >= FULLSCALE_POSITIVE + 1) && (temp_data <= FULLSCALE_NEGATIVE)) {
        local_loadcell_state_sign = NEGATIVE;
    }

    switch (local_loadcell_state_sign) {
        case POSITIVE:
            *result = ((double) (temp_data));
            break;
        case NEGATIVE:
            *result = (((double) (temp_data) - (FULLSCALE_NEGATIVE)));
            break;
        default:
            break;
    }
}

double ADS122C04::Noise_Analysis(MacroWeight *macroWeight, loadcell_config_e config,
                                 noise_anaysis_type_e noise_type, uint32_t time_duration) {
    double result;
    switch (noise_type) {
        case ADS_ENOB: {
            double ref = 0;
            ref = Noise_Analysis(macroWeight, config, ADS_REFERENCE, 100);

            result = Noise_Analysis(macroWeight, config, ADS_INTERNAL_RMS, time_duration);


            result = log(2 * ref / (128 * result)) / log(2);
        }
            break;
        case ADS_NFB: {
            double ref = 0;
            ref = Noise_Analysis(macroWeight, config, ADS_REFERENCE, 100);
            result = Noise_Analysis(macroWeight, config, ADS_INTERNAL_P2P, time_duration);
            result = log(2 * ref / (128 * result)) / log(2);
        }
            break;
        case ADS_REFERENCE: {
            result = StatisticalData_Calc(macroWeight, AVERAGE_DATA, REG0_REFP_REFN, time_duration,
                                          LOADCELL_DEFAULT);
            result = result * 0.00000024414 * 4;//result in volts
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "reference %f",result);
        }
            break;
        case ADS_AVDD: {
            result = StatisticalData_Calc(macroWeight, AVERAGE_DATA, REG0_AVDD_AVSS, time_duration,
                                          LOADCELL_DEFAULT);
            result = result * 0.00000024414 * 4;//result in volts
        }
            break;
        case ADS_INTERNAL_RMS: {

            result = StatisticalData_Calc(macroWeight, RMS_DATA, REG0_AIN0_AIN1, time_duration,
                                          LOADCELL_DEFAULT);
        }
            break;
        case ADS_INTERNAL_P2P: {
            result = Noise_Analysis(macroWeight, config, ADS_INTERNAL_RMS, time_duration);
            //result *= 2.828427;
            result *= 6.6;
            //result = StatisticalData_Calc(weight_type,P2P_DATA,REG0_AIN0_AIN1,time_duration,LOADCELL_DEFAULT);
        }
            break;
        case ADS_INTERNAL_OFFSET: {
            result = StatisticalData_Calc(macroWeight, AVERAGE_DATA, REG0_ZERO_DIFF, time_duration,
                                          LOADCELL_DEFAULT);
        }
            break;
        default:
            break;
    }
    return result;
}

void ADS122C04::LoadCell_FactorCalc(MacroWeight *macroWeight) {
    macroWeight->adc_factor_ =
            Noise_Analysis(macroWeight, LOADCELL_DEFAULT, ADS_REFERENCE, 500) / 128;
    macroWeight->adc_factor_ =
            macroWeight->adc_factor_ / 8388608;//see page 37 , one lsb value calc at gain 128
}

void ADS122C04::LoadCell_Read(MacroWeight *macroWeight) {

    Cal_Mov_Average(MOVING_WINDOW_SIZE, macroWeight->loadcell_, macroWeight->adc_factor_);
    if (macroWeight->loadcell_->new_reading_) {
        macroWeight->total_weight_ =
                macroWeight->loadcell_->loadcell_reading_->raw_data;
        macroWeight->loadcell_->new_reading_ = false;
    }
}

void ADS122C04::Cal_Mov_Average(uint8_t window, LoadCell *loadcell, double adc_factor) {
    uint32_t average_temp = ADS_Read(loadcell);
    //average_temp &= 0xFFFFFF;
    double volt, filtered_val;
    if (average_temp == 0)
        return;
    LoadCell_RawData_Process(average_temp, loadcell, &filtered_val);
    switch (loadcell->loadcell_reading_->moving_avg_->state_) {
        case CALC_MOVING_AVG: {
            loadcell->loadcell_reading_->moving_avg_->total =
                    loadcell->loadcell_reading_->moving_avg_->total -
                    loadcell->loadcell_reading_->moving_avg_->moving_window_
                    [loadcell->loadcell_reading_->moving_avg_->current_index] + filtered_val;

            loadcell->loadcell_reading_->moving_avg_->moving_window_
            [loadcell->loadcell_reading_->moving_avg_->current_index] = filtered_val;

            loadcell->loadcell_reading_->moving_avg_->moving_average =
                    loadcell->loadcell_reading_->moving_avg_->total / window;

            loadcell->loadcell_reading_->moving_avg_->current_index++;

            loadcell->loadcell_reading_->moving_avg_->current_index =
                    loadcell->loadcell_reading_->moving_avg_->current_index > (window - 1) ?
                    0 : loadcell->loadcell_reading_->moving_avg_->current_index;

            volt = ((loadcell->loadcell_reading_->moving_avg_->moving_average) * adc_factor);

            loadcell->loadcell_reading_->raw_data = loadcell->loadcell_reading_->moving_avg_->moving_average;
            loadcell->loadcell_reading_->milli_volts = volt * 1000;
        }
            break;
        case FILL_MOVING_WINDOW: {
            loadcell->loadcell_reading_->moving_avg_->moving_window_
            [loadcell->loadcell_reading_->moving_avg_->current_index] = filtered_val;

            loadcell->loadcell_reading_->moving_avg_->total += filtered_val;

            loadcell->loadcell_reading_->moving_avg_->current_index++;

            if (loadcell->loadcell_reading_->moving_avg_->current_index > (window - 1)) {
                loadcell->loadcell_reading_->moving_avg_->state_ = CALC_MOVING_AVG;
                loadcell->loadcell_reading_->moving_avg_->moving_average =
                        loadcell->loadcell_reading_->moving_avg_->total / window;
                loadcell->loadcell_reading_->moving_avg_->current_index = 0;
            }
        }
            break;
        default:
            break;
    }
}

uint32_t ADS122C04::ADS_Read(LoadCell *loadcell) {
    uint8_t LCi2cRxBuffer[3] = {0x0, 0x0, 0x0};
    uint8_t LCi2cTxBuffer[2] = {0x0, 0x0};
    static loadcell_read_state_e state = BEGIN_CONVERSION;
    static bool flag_conv = false; //Flag for conversion complete
    int result = -1;
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    switch (state) {
        case BEGIN_CONVERSION: {
            Config_LoadCell(loadcell);
            /*
             *START CONVERSION TRIGGER
             */
            LCi2cTxBuffer[0] = START_CONVERSION;
            //I2C_Transmit(loadcell->address_, (uint8_t *) LCi2cTxBuffer, 1);
            i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
                    ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
                    LCi2cTxBuffer)->SetNumberOfBytes(1);
            result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
            state = GET_DATA;
        }
            break;
        case GET_DATA: {
            /*
             * READ CONFIGURATION REGISTER 2
             */
            LCi2cTxBuffer[0] = READ_CONFIG_REG_2;
            //I2C_Transmit(loadcell->address_, (uint8_t *) LCi2cTxBuffer, 1);
            i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
                    ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
                    LCi2cTxBuffer)->SetNumberOfBytes(1);

            result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));

            i2c_request_builder->i2c_request_->SetAction(I2C_READ)->SetDeviceId(
                    ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
                    LCi2cRxBuffer)->SetNumberOfBytes(1);
            //I2C_Receive(loadcell->address_, (uint8_t *) LCi2cRxBuffer, 1);

            result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));

            if (LCi2cRxBuffer[0] >> 7 == 0b1) {
                flag_conv = 1;
            }

            if (flag_conv == 1) {
                flag_conv = 0;
                state = BEGIN_CONVERSION;
                /*
                 * TRANSMIT RDATA COMMAND
                 */
                LCi2cTxBuffer[0] = R_DATA;
                //I2C_Transmit(loadcell->address_, (uint8_t *) LCi2cTxBuffer, 1);
                i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
                        ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
                        LCi2cTxBuffer)->SetNumberOfBytes(1);

                result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
                //I2C_Receive(loadcell->address_, (uint8_t *) LCi2cRxBuffer, 3);
                i2c_request_builder->i2c_request_->SetAction(I2C_READ)->SetDeviceId(
                        ADC_ADS122C04)->SetSlaveAddress(this->address_)->SetArray(
                        LCi2cRxBuffer)->SetNumberOfBytes(3);
                result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
                loadcell->new_reading_ = true;
                //LCi2cRxBuffer[2] &=0xFC;
                return ((LCi2cRxBuffer[0] << 16) | (LCi2cRxBuffer[1] << 8) | (LCi2cRxBuffer[2]));

            } else {
                return 0;
            }
        }
            break;
    }
    return 0;
}

double ADS122C04::Get_LoadCell_Value(loadcell_datatype_e datatype) {
    double result = -1;
    switch(datatype)
    {
        case LOADCELL_GRAMS:
        {
            result = this->macro_weight_->total_weight_;//.loadcell_reading.grams;
        }
            break;
        case LOADCELL_uV:
        {
            result = this->macro_weight_->loadcell_->loadcell_reading_->milli_volts*1000;//.loadcell_reading.grams;
        }
            break;
        case LOADCELL_RAW:
        {
            result = this->macro_weight_->loadcell_->loadcell_reading_->raw_data;//.loadcell_reading.grams;
        }
            break;
    }
    return result;
}

double ADS122C04::GetAdcFactor() {
    return this->macro_weight_->adc_factor_;
}

