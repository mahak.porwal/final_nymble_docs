//
// Created by fedal on 07/10/21.
//

#include "MAX11607.h"

float MAX11607::ReadChannel(uint8_t channel) {
    this->MuxAdcInput(channel);
    usleep(100000);
    return this->ReadAdc();
}

void MAX11607::InitMAX11607() {
    uint8_t tx_buf[1];
    tx_buf[0] = setup_byte;
    I2CRequestBuilder i2c_request_builder;
    i2c_request_builder.i2c_request_->SetAction(I2C_WRITE)
            ->SetDeviceId(ADC_MAX11607)
            ->SetSlaveAddress(this->address_)
            ->SetArray(tx_buf)
            ->SetNumberOfBytes(1);
    I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder.i2c_request_);

    tx_buf[0] = config_byte;

    I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder.i2c_request_);
}

void MAX11607::MuxAdcInput(uint8_t channel) {
    uint8_t tx_buf[1];
    uint8_t channel_selection;
    switch (channel) {
        case MAX11607_CHANNEL_0:
            channel_selection = 0x00;
            break;
        case MAX11607_CHANNEL_1:
            channel_selection = 0x02;
            break;
        case MAX11607_CHANNEL_2:
            channel_selection = 0x04;
            break;
        case MAX11607_CHANNEL_3:
            channel_selection = 0x06;
            break;
        default:
            throw new Error();
    }
    tx_buf[0] = config_byte | channel_selection;

    I2CRequestBuilder i2c_request_builder;
    i2c_request_builder.i2c_request_->SetAction(I2C_WRITE)
            ->SetDeviceId(ADC_MAX11607)
            ->SetSlaveAddress(this->address_)
            ->SetArray(tx_buf)
            ->SetNumberOfBytes(1);
    I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder.i2c_request_);
}

float MAX11607::ReadAdc() {
    uint8_t rx_buf[2];
    I2CRequestBuilder i2c_request_builder;
    Timer timer;
    timer.SetBeginTime();
    i2c_request_builder.i2c_request_->SetAction(I2C_READ)
            ->SetDeviceId(ADC_MAX11607)
            ->SetSlaveAddress(this->address_)
            ->SetArray(rx_buf)
            ->SetNumberOfBytes(2);
    I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder.i2c_request_);

    uint8_t upper_byte = rx_buf[0];
    upper_byte <<= 6;
    upper_byte >>= 6;

    uint16_t raw_value = (upper_byte << 8) | rx_buf[1];
    /*__android_log_print(ANDROID_LOG_ERROR, "MainActivity  : ",
                        "result mcp %lld zero : %d first %d",
                        timer.GetTimeElapsedUs(),upper_byte,rx_buf[1]);*/
    return raw_value * milli_volts_per_bit;
}
