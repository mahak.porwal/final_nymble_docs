//
// Created by fedal on 07/10/21.
//

#ifndef RECIPEENGINE_MAX11607_H
#define RECIPEENGINE_MAX11607_H

typedef enum {
    MAX11607_CHANNEL_0,
    MAX11607_CHANNEL_1,
    MAX11607_CHANNEL_2,
    MAX11607_CHANNEL_3
} Max11607Channels_e;

class MAX11607 : public Adc {
public:
    MAX11607(uint8_t address, I2cDevices_e device_id) {
        this->address_ = address;
        this->device_id_ = device_id;
        this->InitMAX11607();
    }

    float ReadChannel(uint8_t);

private:
    //const uint8_t config_byte = 0x07;
    const uint8_t config_byte = 0x61;
    const uint8_t setup_byte = 0x82;
    const float milli_volts_per_bit = 5060/1024.0;
    //const uint8_t setup_byte = 0xD2;
    //const float milli_volts_per_bit = 2048/1024.0;
    void InitMAX11607();
    void MuxAdcInput(uint8_t channel);
    float ReadAdc();
};


#endif //RECIPEENGINE_MAX11607_H
