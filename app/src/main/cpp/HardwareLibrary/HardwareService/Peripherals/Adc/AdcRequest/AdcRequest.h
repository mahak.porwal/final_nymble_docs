//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_ADCREQUEST_H
#define JULIA_ANDROID_ADCREQUEST_H



#include "AdcRequestEnums.h"

class AdcRequest {
private:
    Component_e hardware_component_;
    AdcRequestType_e adc_request_type_ = READING_REQUEST;
public:
    const AdcRequestType_e GetAdcRequestType() const;

    void SetAdcRequestType(const AdcRequestType_e &adcRequestType);

    Component_e GetHardwareComponent() const;
    void SetHardwareComponent(Component_e hardwareComponent);

};


#endif //JULIA_ANDROID_ADCREQUEST_H
