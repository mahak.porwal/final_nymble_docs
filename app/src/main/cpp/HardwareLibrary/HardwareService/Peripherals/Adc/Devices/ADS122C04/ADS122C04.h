//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_ADS122C04_H
#define JULIA_ANDROID_ADS122C04_H


#include "../../Adc.h"
#include "../../../../Timer/Timer.h"

#define MAX_MOVING_WINDOW_SIZE 100
#define MOVING_WINDOW_SIZE 10
typedef enum LoadCell_Command {
    /*
     * Stand alone commands
     */
    RESET_LOADCELL = 0X06,
    START_CONVERSION = 0X08,
    R_DATA = 0X10,
    /*
     * Read/Write Registers
     */
    WRITE_CONFIG_REG_0 = 0X40,
    WRITE_CONFIG_REG_1 = 0X44,
    WRITE_CONFIG_REG_2 = 0X48,
    WRITE_CONFIG_REG_3 = 0X4C,
    READ_CONFIG_REG_2 = 0x28,

} loadcell_command_e;

typedef enum LoadCell_RegValue {
    REG0_AIN0_AIN1 = 0b00111110,
    REG0_AIN1_AIN0 = 0b00001110,
    REG0_AIN2_AIN3 = 0b01111110,
    REG0_AIN3_AIN2 = 0b01101110,
    REG0_REFP_REFN = 0b11001110,
    REG0_AVDD_AVSS = 0b11011110,
    REG0_ZERO_DIFF = 0b11101110,
    REG1_DR_20_NOR = 0b00001010,
    REG1_DR_45_NOR = 0b00101010,
    REG1_DR_90_NOR = 0b01001010,
    REG1_DR_175_NOR = 0b01101010,
    REG1_DR_330_NOR = 0b10001010,
    REG1_DR_600_NOR = 0b10101010,
    REG1_DR_1000_NOR = 0b11001010,
    REG1_DR_40_TUR = 0b00011010,
    REG1_DR_90_TUR = 0b00111010,
    REG1_DR_180_TUR = 0b01011010,
    REG1_DR_350_TUR = 0b01111010,
    REG1_DR_660_TUR = 0b10011010,
    REG1_DR_1200_TUR = 0b10111010,
    REG1_DR_2000_TUR = 0b11011010,
    REG2_VALUE = 0b10000000,
    REG3_VALUE = 0X00
} loadcell_regvalue_e;

typedef enum ADS122C_DataFormat {
    FULLSCALE_POSITIVE = 8388607,
    FULLSCALE_NEGATIVE = 16777216
} ads_data_format_e;

typedef enum LoadCell_SignState {
    DEFAULT_LOAD_CELL_STATE_SIGN = 0,
    POSITIVE = 1,
    NEGATIVE = 2

} loadcell_statesign_e;

typedef enum {
    LOADCELL_DEFAULT = 0,
    LOADCELL_ONE = 1,     //selects AIN2 and AIN3
    LOADCELL_TWO = 2,     //selects AIN0 and AIN1
    LOADCELL_COMBINED = 3 //selects AIN0 and AIN1 after AIN2 and AIN3s
} loadcell_config_e;
typedef enum {
    LOADCELL_GRAMS = 0,
    LOADCELL_uV,
    LOADCELL_RAW
} loadcell_datatype_e;
typedef enum {
    BEGIN_CONVERSION = 1,
    GET_DATA = 2
} loadcell_read_state_e;
typedef enum {
    FILL_MOVING_WINDOW = 1,
    CALC_MOVING_AVG = 2
} moving_average_state_e;
typedef enum {
    RMS_DATA = 1,
    P2P_DATA = 2,
    AVERAGE_DATA = 3
} statistical_type_e;
typedef enum {
    ADS_ENOB = 1,
    ADS_NFB = 2,
    ADS_AVDD = 3,
    ADS_REFERENCE = 4,
    ADS_OFFSET = 5,
    ADS_INTERNAL_RMS = 6,
    ADS_INTERNAL_P2P = 7,
    ADS_INTERNAL_OFFSET = 8
} noise_anaysis_type_e;

class MovingAverage {
public:
    moving_average_state_e state_;
    double moving_window_[MAX_MOVING_WINDOW_SIZE];
    double total;
    uint8_t current_index;
    double moving_average;

    MovingAverage() {
        this->state_ = FILL_MOVING_WINDOW;
        this->total = 0;
        this->current_index = 0;
        this->moving_average = 0;
    }

};

class LoadCellOffsetCalibration {
public:

    uint16_t time_;
    MovingAverage *moving_avg_;
    double total_;
    double average_;
    uint32_t sample_counts_;

    LoadCellOffsetCalibration() {
        this->moving_avg_ = new MovingAverage;
        this->average_ = 0;
        this->total_ = 0;
        this->sample_counts_ = 0;
    }

    ~LoadCellOffsetCalibration() {
        delete this->moving_avg_;
    }
};

class LoadCellReadingGrams {
public:

    MovingAverage *moving_avg_;
    double grams;
    double milli_volts;
    double raw_data;

    LoadCellReadingGrams() {
        this->moving_avg_ = new MovingAverage;
        this->grams = 0;
        this->milli_volts = 0;
        this->raw_data = 0;
    }


    ~LoadCellReadingGrams() {
        delete this->moving_avg_;
    }
};

class LoadCell {
public:
    LoadCell() {
        this->loadcell_offset_ = new LoadCellOffsetCalibration;
        this->loadcell_reading_ = new LoadCellReadingGrams;
        this->new_reading_ = false;
    }

    uint8_t address_;
    LoadCellOffsetCalibration *loadcell_offset_;
    LoadCellReadingGrams *loadcell_reading_;
    uint8_t configuration_value_;
    bool new_reading_;

    ~LoadCell() {
        delete this->loadcell_reading_;
        delete this->loadcell_offset_;
    }
};

class MacroWeight {
public:
    MacroWeight() {
        this->loadcell_ = new LoadCell;
        this->total_weight_ = 0;
        this->adc_factor_ = 0;
    }

    LoadCell *loadcell_;
    float total_weight_;
    double adc_factor_;

    ~MacroWeight() {
        delete this->loadcell_;
    }
};

class ADS122C04 : public Adc {
private:
    double StatisticalData_Calc(MacroWeight *, statistical_type_e stat_type,
                                loadcell_regvalue_e input_config, uint32_t time_duration,
                                loadcell_config_e lc_config);

    void Loadcell_Init(LoadCell *, loadcell_regvalue_e reg1_value);

    void Config_LoadCell(LoadCell *loadcell);

    uint32_t ADS_Reference_Read(LoadCell *loadcell);

    void LoadCell_RawData_Process(uint32_t temp_data, LoadCell *loadcell, double *result);

    double Noise_Analysis(MacroWeight *, loadcell_config_e config, noise_anaysis_type_e noise_type,
                          uint32_t time_duration);

    void LoadCell_Read(MacroWeight *);

    void Cal_Mov_Average(uint8_t window, LoadCell *loadcell, double adc_factor);

    uint32_t ADS_Read(LoadCell *loadcell);

public:
    ADS122C04(uint8_t address, I2cDevices_e device_id) {
        this->macro_weight_ = new MacroWeight;
        this->address_ = address;
        this->device_id_ = device_id;
        this->LoadCell_FactorCalc(this->macro_weight_);
        this->macro_weight_->loadcell_->configuration_value_ = REG0_AIN0_AIN1;
        //this->Loadcell_Init(this->macro_weight_->loadcell_,REG1_DR_20_NOR);
    }

    float ReadChannel(uint8_t);

    virtual double GetAdcFactor();

    MacroWeight *macro_weight_;

    ~ADS122C04() {
        delete this->macro_weight_;
    }

    void LoadCell_FactorCalc(MacroWeight *);

    double Get_LoadCell_Value(loadcell_datatype_e datatype);
};


#endif //JULIA_ANDROID_ADS122C04_H
