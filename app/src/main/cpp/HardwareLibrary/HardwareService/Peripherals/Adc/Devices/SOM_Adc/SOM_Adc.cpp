//
// Created by fedal on 21/12/20.
//

#include "SOM_Adc.h"
#include <fstream>
#include <string>
float SOM_Adc::ReadChannel(uint8_t)  {

    string filename;

    switch (this->som_channel_){
        case ADC_IN_0:{
            filename = "/sys/devices/platform/ff100000.saradc/iio:device0/in_voltage0_raw";
        }
        break;
        case ADC_IN_2:{
            filename = "/sys/devices/platform/ff100000.saradc/iio:device0/in_voltage2_raw";
        }
        break;
        case ADC_IN_3:{
            filename = "/sys/devices/platform/ff100000.saradc/iio:device0/in_voltage3_raw";
        }
        break;
        default:
            break;
    }
    std::fstream adc_file(filename.c_str(), std::ios_base::in);
    uint16_t value = 0;
    adc_file>>value;
/*    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                        "raw reading %d",
                        value);*/
    return value*1.0;
}
