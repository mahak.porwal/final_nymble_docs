//
// Created by fedal on 9/12/20.
//

#include "AdcRequest.h"

Component_e AdcRequest::GetHardwareComponent() const {
    return hardware_component_;
}

void AdcRequest::SetHardwareComponent(Component_e hardwareComponent) {
    hardware_component_ = hardwareComponent;
}

const AdcRequestType_e AdcRequest::GetAdcRequestType() const {
    return adc_request_type_;
}

void AdcRequest::SetAdcRequestType(const AdcRequestType_e &adcRequestType) {
    adc_request_type_ = adcRequestType;
}
