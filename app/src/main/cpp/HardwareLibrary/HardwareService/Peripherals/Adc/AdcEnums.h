//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_ADCENUMS_H
#define JULIA_ANDROID_ADCENUMS_H
typedef enum{
    ADC_IN_0,
    ADC_IN_2,
    ADC_IN_3,
    DEFAULT_ADC_CHANNEL
}SOM_Adc_Channels_e;
#endif //JULIA_ANDROID_ADCENUMS_H
