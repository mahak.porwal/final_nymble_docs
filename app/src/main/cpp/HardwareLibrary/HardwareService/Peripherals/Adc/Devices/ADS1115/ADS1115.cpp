//
// Created by fedal on 9/12/20.
//

#include "ADS1115.h"

float ADS1115::ReadChannel(uint8_t channel)  {
    float result = -1;
    pthread_mutex_lock(&this->device_lock_);
    this->MuxAdc(static_cast<Ads1115Channels_e>(channel));
    result = this->ReadAdc();
    pthread_mutex_unlock(&this->device_lock_);
    return result;
}

int ADS1115::MuxAdc(Ads1115Channels_e sel)  {
    uint8_t tx[3];
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    int result = -1;
    uint16_t registerValue = (ADC_CONFIG_VALUES[sel] & 0xFFFF);
    uint8_t a_higher = (uint8_t) ((registerValue >> 8) & 0xFF);
    uint8_t a_lower = (uint8_t) ((registerValue) & 0xFF);
    tx[0] = 0x01;
    tx[1] = a_higher;
    tx[2] = a_lower;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_ADS1115)->SetSlaveAddress(this->address_)->SetArray(tx)->SetNumberOfBytes(3);
    result = I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder->i2c_request_);
    return result;
}

float ADS1115::ReadAdc()  {
    uint8_t tx[1] = {0x00};
    uint8_t rx[2] = {0x00, 0x00};
    int result = -1;
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            ADC_ADS1115)->SetSlaveAddress(this->address_)->SetArray(tx)->SetNumberOfBytes(1);
    result = I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder->i2c_request_);
    //HardwareControler.write(i2c_3_fileDesc, arrayToSend);

    usleep(30000);

    i2c_request_builder->i2c_request_->SetAction(I2C_READ)->SetDeviceId(
            ADC_ADS1115)->SetSlaveAddress(this->address_)->SetArray(rx)->SetNumberOfBytes(2);
    result = I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder->i2c_request_);

    uint8_t a0 = rx[0] & 0xFF;
    uint8_t a1 = rx[1] & 0xFF;

    uint16_t raw_val = (a1) | (a0<<8);

    return (raw_val * 187.5) / 1000.0;
}
