//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_ADCREQUESTPROCESSOR_H
#define JULIA_ANDROID_ADCREQUESTPROCESSOR_H


#include "../Adc.h"
#include "../Devices/ADS1115/ADS1115.h"
#include "../AdcRequest/AdcRequest.h"
#include "../Devices/ADS122C04/ADS122C04.h"
#include "../Devices/INA219B/INA219B.h"
#include "../Devices/SOM_Adc/SOM_Adc.h"
#include "../Devices/MAX11607/MAX11607.h"
#include "../Devices/MCP3426/MCP3426.h"

class AdcRequestProcessor {

    static map<Component_e, Adc *> component_to_device_map_;
    static map<Component_e, Ads1115Channels_e> component_to_ads1115Channel_map_;
    static map<Component_e, Max11607Channels_e> component_to_max11607_channel_map_;
    static map<Component_e, MCP3462Channels_e> component_to_mcp3426_map_;
    static ADS1115 *ads1115_;
    static ADS122C04 *ads122C04_;
    static INA219B *ina219b_;
    static MAX11607 *max11607_;
    static SOM_Adc *som_adc_0_;
    static SOM_Adc *som_adc_2_;
    static SOM_Adc *som_adc_3_;
    static MCP3426 *mcp3426_;


    static void PopulateComponentToDeviceMap();

    static void PopulateComponentToInductionMap();

    static void PopulateComponentToMcpChannelMap();

public:
    static float ProcessAdcRequest(AdcRequest);

    AdcRequestProcessor() {
        this->ads1115_ = new ADS1115(0x48, ADC_ADS1115);
        this->ads122C04_ = new ADS122C04(0x47, ADC_ADS122C04);
        this->ina219b_ = new INA219B(0x40, ADC_INA219B);
        this->som_adc_0_ = new SOM_Adc(ADC_IN_0);
        this->som_adc_2_ = new SOM_Adc(ADC_IN_2);
        this->som_adc_3_ = new SOM_Adc(ADC_IN_3);
        //this->max11607_ = new MAX11607(0x34, ADC_MAX11607);
        this->PopulateComponentToInductionMap();
        this->mcp3426_ = new MCP3426(0x6D, ADC_MCP3426);
        this->PopulateComponentToInductionMap();
        this->PopulateComponentToDeviceMap();
        this->PopulateComponentToMcpChannelMap();
    }

    ~AdcRequestProcessor() {
        delete this->som_adc_0_;
        delete this->som_adc_2_;
        delete this->som_adc_3_;
        delete this->ads1115_;
        delete this->ads122C04_;
        delete this->ina219b_;
        delete this->max11607_;
        delete this->mcp3426_;
    }
};

void AdcRequestProcessor::PopulateComponentToDeviceMap() {

    AdcRequestProcessor::component_to_device_map_.insert({MACRO_SERVO_1_FEEDBACK, som_adc_0_});
    AdcRequestProcessor::component_to_device_map_.insert({MACRO_SERVO_2_FEEDBACK, som_adc_0_});
    AdcRequestProcessor::component_to_device_map_.insert({MACRO_SERVO_3_FEEDBACK, som_adc_0_});
    AdcRequestProcessor::component_to_device_map_.insert({MACRO_SERVO_4_FEEDBACK, som_adc_0_});

    AdcRequestProcessor::component_to_device_map_.insert({MICRO_SERVO_1_FEEDBACK, som_adc_0_});
    AdcRequestProcessor::component_to_device_map_.insert({MICRO_SERVO_2_FEEDBACK, som_adc_0_});
    AdcRequestProcessor::component_to_device_map_.insert({MICRO_SERVO_3_FEEDBACK, som_adc_0_});
    AdcRequestProcessor::component_to_device_map_.insert({MICRO_SERVO_4_FEEDBACK, som_adc_0_});
    AdcRequestProcessor::component_to_device_map_.insert({MICRO_SERVO_5_FEEDBACK, som_adc_0_});
    AdcRequestProcessor::component_to_device_map_.insert({MICRO_SERVO_6_FEEDBACK, som_adc_0_});

    AdcRequestProcessor::component_to_device_map_.insert({STIRRER_CURRENT_SENSOR, som_adc_2_});


    AdcRequestProcessor::component_to_device_map_.insert({SOM_BOARD_CURRENT_SENSOR, ina219b_});

    AdcRequestProcessor::component_to_device_map_.insert({INDUCTION_AC_CURRENT_SENSOR, ads1115_});
    AdcRequestProcessor::component_to_device_map_.insert({INDUCTION_AC_VOLTAGE_SENSOR, ads1115_});
    AdcRequestProcessor::component_to_device_map_.insert(
            {INDUCTION_IGBT_TEMPERATURE_SENSOR, ads1115_});
    AdcRequestProcessor::component_to_device_map_.insert(
            {INDUCTION_PAN_TEMPERATURE_SENSOR, ads1115_});

    AdcRequestProcessor::component_to_device_map_.insert({LOADCELL_SENSOR, ads122C04_});

    AdcRequestProcessor::component_to_device_map_.insert(
            {WATER_TEMPERATURE, mcp3426_});

    AdcRequestProcessor::component_to_device_map_.insert(
            {BODY_TEMPERATURE, mcp3426_});
}

void AdcRequestProcessor::PopulateComponentToInductionMap() {

    AdcRequestProcessor::component_to_ads1115Channel_map_.insert(
            {INDUCTION_AC_CURRENT_SENSOR, ADS_1115_CHANNEL_3});
    AdcRequestProcessor::component_to_ads1115Channel_map_.insert(
            {INDUCTION_AC_VOLTAGE_SENSOR, ADS_1115_CHANNEL_2});
    AdcRequestProcessor::component_to_ads1115Channel_map_.insert(
            {INDUCTION_IGBT_TEMPERATURE_SENSOR, ADS_1115_CHANNEL_1});
    AdcRequestProcessor::component_to_ads1115Channel_map_.insert(
            {INDUCTION_PAN_TEMPERATURE_SENSOR, ADS_1115_CHANNEL_0});
}

float AdcRequestProcessor::ProcessAdcRequest(AdcRequest adc_request) {
    Component_e hardware_component = adc_request.GetHardwareComponent();
    Adc *adc = AdcRequestProcessor::component_to_device_map_.at(hardware_component);
    float result;
    if (adc->getDeviceId() == DEFAULT_ADC_ENUM) {
        return adc->ReadChannel(0);
    } else {
        switch (adc->getDeviceId()) {
            case ADC_INA219B:
            case ADC_ADS122C04: {
                if (adc_request.GetAdcRequestType() == REFERENCE_REQUEST) {
                    result = adc->GetAdcFactor();
                } else {
                    result = adc->ReadChannel(0);
                }
            }
                break;
            case ADC_ADS1115: {
                Ads1115Channels_e channel = AdcRequestProcessor::component_to_ads1115Channel_map_.at(
                        hardware_component);
                result = ads1115_->ReadChannel(channel);
            }
                break;
            case ADC_MCP3426: {
                MCP3462Channels_e channel = AdcRequestProcessor::component_to_mcp3426_map_.at(
                        hardware_component);
                result = mcp3426_->ReadChannel(channel);
            }
                break;
            case ADC_MAX11607: {
                Max11607Channels_e channel = AdcRequestProcessor::component_to_max11607_channel_map_.at(
                        hardware_component);
                result = max11607_->ReadChannel(channel);
            }
                break;
            default: {
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                    "Adc error %d",
                                    adc->getDeviceId());
                throw new Error;
            }
        }
    }
    return result;
}

void AdcRequestProcessor::PopulateComponentToMcpChannelMap() {
    AdcRequestProcessor::component_to_mcp3426_map_.insert(
            {WATER_TEMPERATURE, MCP3426_CHANNEL_1});
    AdcRequestProcessor::component_to_mcp3426_map_.insert(
            {BODY_TEMPERATURE, MCP3426_CHANNEL_1});
}

map<Component_e, Adc *> AdcRequestProcessor::component_to_device_map_;
map<Component_e, Ads1115Channels_e> AdcRequestProcessor::component_to_ads1115Channel_map_;
map<Component_e, Max11607Channels_e> AdcRequestProcessor::component_to_max11607_channel_map_;
map<Component_e, MCP3462Channels_e> AdcRequestProcessor::component_to_mcp3426_map_;
ADS1115 *AdcRequestProcessor::ads1115_;
ADS122C04 *AdcRequestProcessor::ads122C04_;
INA219B *AdcRequestProcessor::ina219b_;
MAX11607 *AdcRequestProcessor::max11607_;
SOM_Adc *AdcRequestProcessor::som_adc_0_;
SOM_Adc *AdcRequestProcessor::som_adc_2_;
SOM_Adc *AdcRequestProcessor::som_adc_3_;
MCP3426 *AdcRequestProcessor::mcp3426_;

#endif //JULIA_ANDROID_ADCREQUESTPROCESSOR_H
