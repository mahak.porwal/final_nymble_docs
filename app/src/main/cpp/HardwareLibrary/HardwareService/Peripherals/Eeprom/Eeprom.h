//
// Created by fedal on 22/8/21.
//

#ifndef RECIPEENGINE_EEPROM_H
#define RECIPEENGINE_EEPROM_H


class Eeprom {
private:

    virtual bool IsValidAddrWrite(uint16_t address) = 0;

    virtual bool IsValidAddrRead(uint16_t address) = 0;
protected:
    uint8_t device_address_;
public:
    Eeprom(uint8_t device_address) {
        this->device_address_ = device_address;
    }

    virtual uint8_t ReadByte(uint16_t address) = 0;

    virtual std::vector<uint8_t> SequentialRead(uint16_t address, uint16_t num_of_bytes) = 0;

    virtual void WriteByte(uint16_t address, uint8_t data) = 0;
};


#endif //RECIPEENGINE_EEPROM_H
