//
// Created by fedal on 22/8/21.
//

#include "MicroChip24AA64.h"

bool MicroChip24AA64::IsValidAddrWrite(uint16_t address) {
    return address >= 0x00C8 && address <= 0x033F;
}

bool MicroChip24AA64::IsValidAddrRead(uint16_t address) {
    return address >= 0x0000 && address <= 0x1F3F;
}

std::vector<uint8_t> MicroChip24AA64::SequentialRead(uint16_t address, uint16_t num_of_bytes) {
    if (!this->IsValidAddrRead(address)) {
        throw new Error();
    }
    std::vector<uint8_t> received_data;
    uint8_t Tx_Buffer[2];
    I2CRequestBuilder i2c_request_builder;
    Tx_Buffer[0] = address >> 8;
    Tx_Buffer[1] = address & 0xFF;
    i2c_request_builder.i2c_request_
            ->SetAction(I2C_WRITE)
            ->SetDeviceId(EEPROM_24AA64)
            ->SetNumberOfBytes(2)
            ->SetArray(Tx_Buffer)
            ->SetSlaveAddress(this->device_address_);

    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));

    uint8_t *rx_buffer = (uint8_t *) malloc(num_of_bytes);
    I2CRequestBuilder i2c_request_builder1;
    i2c_request_builder1.i2c_request_
            ->SetAction(I2C_READ)
            ->SetDeviceId(EEPROM_24AA64)
            ->SetNumberOfBytes(num_of_bytes)
            ->SetArray(rx_buffer)
            ->SetSlaveAddress(this->device_address_);

    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder1.i2c_request_));

    for (int i = 0; i < num_of_bytes; i++) {
        received_data.push_back(*(rx_buffer + i));
        //__android_log_print(ANDROID_LOG_ERROR, "thrml", "address 0x%04X  value 0x%04X ", i,*(rx_buffer + i));
    }
    free(rx_buffer);
    return received_data;
}

uint8_t MicroChip24AA64::ReadByte(uint16_t address) {
    if (!this->IsValidAddrRead(address)) {
        throw new Error();
    }

    uint8_t Tx_Buffer[2];
    I2CRequestBuilder i2c_request_builder;
    Tx_Buffer[0] = address >> 8;
    Tx_Buffer[1] = address & 0xFF;
    i2c_request_builder.i2c_request_
            ->SetAction(I2C_WRITE)
            ->SetDeviceId(EEPROM_24AA64)
            ->SetNumberOfBytes(2)
            ->SetArray(Tx_Buffer)
            ->SetSlaveAddress(this->device_address_);

    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));

    uint8_t Rx_Buffer[1];
    I2CRequestBuilder i2c_request_builder1;
    i2c_request_builder1.i2c_request_
            ->SetAction(I2C_READ)
            ->SetDeviceId(EEPROM_24AA64)
            ->SetNumberOfBytes(1)
            ->SetArray(Rx_Buffer)
            ->SetSlaveAddress(this->device_address_);

    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder1.i2c_request_));
    return *Rx_Buffer;
}

void MicroChip24AA64::WriteByte(uint16_t address, uint8_t data) {
    if (!this->IsValidAddrWrite(address)) {
        throw new Error();
    }
    uint8_t Tx_Buffer[3];
    I2CRequestBuilder i2c_request_builder;
    uint8_t upper_address_byte = (address >> 8);
    uint8_t lower_address_byte = (address & 0xFF);
    Tx_Buffer[0] = upper_address_byte;
    Tx_Buffer[1] = lower_address_byte;
    Tx_Buffer[2] = data;
    i2c_request_builder.i2c_request_
            ->SetAction(I2C_WRITE)
            ->SetDeviceId(EEPROM_24AA64)
            ->SetNumberOfBytes(3)
            ->SetArray(Tx_Buffer)
            ->SetSlaveAddress(this->device_address_);
    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder.i2c_request_));
}