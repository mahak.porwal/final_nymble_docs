//
// Created by fedal on 22/8/21.
//

#ifndef RECIPEENGINE_MICROCHIP24AA64_H
#define RECIPEENGINE_MICROCHIP24AA64_H

#include "../../Eeprom.h"
#include "../../../I2C/I2CRequest/I2CRequestBuilder.h"
class MicroChip24AA64 : public Eeprom {
private:

    bool IsValidAddrWrite(uint16_t address);

    bool IsValidAddrRead(uint16_t address);

public:
    MicroChip24AA64(uint8_t device_address) :
            Eeprom(device_address) {}

    uint8_t ReadByte(uint16_t address);

    std::vector<uint8_t> SequentialRead(uint16_t address, uint16_t num_of_bytes);

    void WriteByte(uint16_t address, uint8_t data);
};


#endif //RECIPEENGINE_MICROCHIP24AA64_H
