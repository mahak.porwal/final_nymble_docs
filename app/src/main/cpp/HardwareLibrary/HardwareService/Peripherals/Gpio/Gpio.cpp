//
// Created by fedal on 9/12/20.
//

#include "Gpio.h"

I2cDevices_e Gpio::getDeviceId() const {
    return device_id_;
}

void Gpio::setDeviceId(I2cDevices_e deviceId) {
    device_id_ = deviceId;
}
