//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_GPIOREQUEST_H
#define JULIA_ANDROID_GPIOREQUEST_H


#include "../../../HardwareComponent/HardwareComponent.h"
#include "../Gpio.h"

class GpioRequest {
private:
    GpioAction_e action_ = ACTION_DEFAULT;
public:
    const GpioDirection_e &GetDirection() const;

    GpioRequest* SetDirection(const GpioDirection_e &direction);

    const GpioAction_e &GetAction() const;

    GpioRequest* SetAction(const GpioAction_e &action);

    const GpioState_e &GetState() const;

    GpioRequest* SetState(const GpioState_e &state);

    Component_e GetHardwareComponent() const;

    GpioRequest* SetHardwareComponent(Component_e hardwareComponent);

private:
    GpioDirection_e direction_ = GPIO_DIRECTION_DEFAULT;
    GpioState_e state_ = STATE_DEFAULT;
    Component_e hardware_component_;
};


#endif //JULIA_ANDROID_GPIOREQUEST_H
