//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_PCA9535_H
#define JULIA_ANDROID_PCA9535_H


#include "../../Gpio.h"


class Pca9535 : public Gpio {
private:
    uint8_t pca9535_port_0_dir_ = 0x00;
    uint8_t pca9535_port_1_dir_ = 0x00;
    uint8_t pca9535_port_0_data_ = 0x00;
    uint8_t pca9535_port_1_data_ = 0x00;
    uint8_t address_;

public:
    int SetPinDirection(int, GpioDirection_e);

    int SetPinState(int, GpioState_e);

    GpioState_e ReadPinState(int);

    Pca9535(uint8_t address, I2cDevices_e device_id) {
        this->address_ = address;
        this->device_id_ = device_id;
    }

};


#endif //JULIA_ANDROID_PCA9535_H
