//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_GPIOREQUESTPROCESSOR_H
#define JULIA_ANDROID_GPIOREQUESTPROCESSOR_H


#include "../Gpio.h"
#include "../GpioRequest/GpioRequest.h"
#include "../Devices/PCA9535/Pca9535Enums.h"
#include "../Devices/PCA9535/Pca9535.h"
#include "../Devices/SOM_Gpio/SOM_Gpio.h"
#include "../Devices/SOM_Gpio/SOM_GpioEnums.h"

class GpioRequestProcessor {
private:
    static map<Component_e, Gpio *> component_to_device_map_;
    static map<Component_e, Pca9535Channels_e> component_to_pca_channel_map_;
    static map<Component_e, SomGpioPins_e> component_to_som_channel_map_;
    static map<Component_e, GpioDirection_e> component_to_default_direction_map_;
    static map<Component_e, GpioState_e> component_to_default_state_map_;

    static void PopulateComponentToDeviceMap();

    static void PopulateComponentToChannelMap();

    static void PopulateDefaultDirectionMap();

    static void PopulateDefaultStateMap();

    static void InitializeGpioPins();

public:
    static int ProcessGpioRequest(GpioRequest);

    static Pca9535 *pca9535_;
    static SOM_Gpio *som_gpio_;

    GpioRequestProcessor() {
        this->pca9535_ = new Pca9535(0x20, GPIO_PCA9535);
        __android_log_print(ANDROID_LOG_ERROR, "Chef", "pca9535 done  ");
        this->som_gpio_ = new SOM_Gpio;
        __android_log_print(ANDROID_LOG_ERROR, "Chef", "som done  ");
        this->PopulateComponentToChannelMap();
        this->PopulateComponentToDeviceMap();
        this->PopulateDefaultDirectionMap();
        this->PopulateDefaultStateMap();
        __android_log_print(ANDROID_LOG_ERROR, "Chef", "pca9535 done  ");
        this->InitializeGpioPins();
    }

    ~GpioRequestProcessor() {
        delete this->pca9535_;
        delete this->som_gpio_;
    }
};

Pca9535 *GpioRequestProcessor::pca9535_;
SOM_Gpio *GpioRequestProcessor::som_gpio_;
map<Component_e, Gpio *> GpioRequestProcessor::component_to_device_map_;
map<Component_e, Pca9535Channels_e> GpioRequestProcessor::component_to_pca_channel_map_;
map<Component_e, SomGpioPins_e> GpioRequestProcessor::component_to_som_channel_map_;
map<Component_e, GpioDirection_e> GpioRequestProcessor::component_to_default_direction_map_;
map<Component_e, GpioState_e> GpioRequestProcessor::component_to_default_state_map_;

void GpioRequestProcessor::PopulateComponentToDeviceMap() {

    GpioRequestProcessor::component_to_device_map_.insert(
            {MACRO_POT_MUX_SELECT_1, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {MACRO_POT_MUX_SELECT_2, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {MACRO_POT_MUX_SELECT_3, GpioRequestProcessor::pca9535_});

    GpioRequestProcessor::component_to_device_map_.insert(
            {MICRO_POT_MUX_SELECT_1, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {MICRO_POT_MUX_SELECT_2, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {MICRO_POT_MUX_SELECT_3, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {STIRRER_PHASE_GPIO, GpioRequestProcessor::pca9535_});

    GpioRequestProcessor::component_to_device_map_.insert(
            {EXHAUST_FAN_ON_OFF, GpioRequestProcessor::pca9535_});

    GpioRequestProcessor::component_to_device_map_.insert(
            {COUNTER_MUX_SELECT_0, GpioRequestProcessor::pca9535_});

    GpioRequestProcessor::component_to_device_map_.insert(
            {COUNTER_MUX_SELECT_1, GpioRequestProcessor::pca9535_});

    GpioRequestProcessor::component_to_device_map_.insert(
            {STIRRER_CONNECTION_FB, GpioRequestProcessor::som_gpio_});

    GpioRequestProcessor::component_to_device_map_.insert(
            {INDUCTION_PAN_GPIO, GpioRequestProcessor::som_gpio_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {INDUCTION_INT_GPIO, GpioRequestProcessor::som_gpio_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {INDUCTION_FAN_GPIO, GpioRequestProcessor::som_gpio_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {INDUCTION_ON_OFF_GPIO, GpioRequestProcessor::som_gpio_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {INDUCTION_BUZZER_GPIO, GpioRequestProcessor::som_gpio_});

    GpioRequestProcessor::component_to_device_map_.insert(
            {INA300_LATCH, GpioRequestProcessor::som_gpio_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {INA300_ALERT, GpioRequestProcessor::som_gpio_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {LM2678_CONTROL, GpioRequestProcessor::som_gpio_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {MACRO_MICRO_FB_SELECT_1, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {HEATER_ENABLE, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {INDUCTIVE_SENSOR_1, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {INDUCTIVE_SENSOR_2, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {LIMIT_SWITCH_1, GpioRequestProcessor::pca9535_});
    GpioRequestProcessor::component_to_device_map_.insert(
            {LIMIT_SWITCH_2, GpioRequestProcessor::pca9535_});
}

void GpioRequestProcessor::PopulateComponentToChannelMap() {
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {MICRO_POT_MUX_SELECT_1, GPIO_PIN_0});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {MICRO_POT_MUX_SELECT_2, GPIO_PIN_1});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {MICRO_POT_MUX_SELECT_3, GPIO_PIN_2});

    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {MACRO_POT_MUX_SELECT_1, GPIO_PIN_3});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {MACRO_POT_MUX_SELECT_2, GPIO_PIN_4});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {MACRO_POT_MUX_SELECT_3, GPIO_PIN_5});

    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {STIRRER_PHASE_GPIO, GPIO_PIN_6});

    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {EXHAUST_FAN_ON_OFF, GPIO_PIN_13});

    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {COUNTER_MUX_SELECT_0, GPIO_PIN_17});

    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {COUNTER_MUX_SELECT_1, GPIO_PIN_16});

    GpioRequestProcessor::component_to_som_channel_map_.insert(
            {STIRRER_CONNECTION_FB, SOM_GPIO_PIN_32});

    GpioRequestProcessor::component_to_som_channel_map_.insert(
            {INDUCTION_PAN_GPIO, SOM_GPIO_PIN_54});
    GpioRequestProcessor::component_to_som_channel_map_.insert(
            {INDUCTION_INT_GPIO, SOM_GPIO_PIN_35});
    GpioRequestProcessor::component_to_som_channel_map_.insert(
            {INDUCTION_FAN_GPIO, SOM_GPIO_PIN_50});
    GpioRequestProcessor::component_to_som_channel_map_.insert(
            {INDUCTION_ON_OFF_GPIO, SOM_GPIO_PIN_55});
    GpioRequestProcessor::component_to_som_channel_map_.insert(
            {INDUCTION_BUZZER_GPIO, SOM_GPIO_PIN_41});

    GpioRequestProcessor::component_to_som_channel_map_.insert({INA300_LATCH, SOM_GPIO_PIN_149});
    GpioRequestProcessor::component_to_som_channel_map_.insert({INA300_ALERT, SOM_GPIO_PIN_56});
    GpioRequestProcessor::component_to_som_channel_map_.insert({LM2678_CONTROL, SOM_GPIO_PIN_36});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {MACRO_MICRO_FB_SELECT_1, GPIO_PIN_7});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {HEATER_ENABLE, GPIO_PIN_10});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {INDUCTIVE_SENSOR_1, GPIO_PIN_12});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {INDUCTIVE_SENSOR_2, GPIO_PIN_11});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {LIMIT_SWITCH_1, GPIO_PIN_15});
    GpioRequestProcessor::component_to_pca_channel_map_.insert(
            {LIMIT_SWITCH_2, GPIO_PIN_14});
}

int GpioRequestProcessor::ProcessGpioRequest(GpioRequest gpio_request) {
    Component_e hardware_component = gpio_request.GetHardwareComponent();
    Gpio *gpio = GpioRequestProcessor::component_to_device_map_.at(hardware_component);
    int result = -1;
    switch (gpio->getDeviceId()) {
        case GPIO_PCA9535: {
            Pca9535Channels_e pca_9535_pin = GpioRequestProcessor::component_to_pca_channel_map_.at(
                    hardware_component);
            switch (gpio_request.GetAction()) {
                case ACTION_READ_PIN: {
                    result = GpioRequestProcessor::pca9535_->ReadPinState(pca_9535_pin);
                }
                    break;
                case ACTION_WRITE_PIN: {
                    result = GpioRequestProcessor::pca9535_->SetPinState(pca_9535_pin,
                                                                         gpio_request.GetState());
                }
                    break;
                case ACTION_SET_DIRECTION: {
                    result = GpioRequestProcessor::pca9535_->SetPinDirection(pca_9535_pin,
                                                                             gpio_request.GetDirection());
                }
                default: {
                    throw new Error;
                }
            }
        }
            break;
        case DEFAULT_GPIO_DEVICE: {
            SomGpioPins_e som_pin = GpioRequestProcessor::component_to_som_channel_map_.at(
                    hardware_component);
            switch (gpio_request.GetAction()) {
                case ACTION_READ_PIN: {
                    result = GpioRequestProcessor::som_gpio_->ReadPinState(som_pin);
                }
                    break;
                case ACTION_WRITE_PIN: {
                    if (gpio_request.GetState() == STATE_DEFAULT) {
                        throw new Error;
                    }
                    result = GpioRequestProcessor::som_gpio_->SetPinState(som_pin,
                                                                          gpio_request.GetState());
                }
                    break;
                case ACTION_SET_DIRECTION: {
                    if (gpio_request.GetDirection() == GPIO_DIRECTION_DEFAULT) {
                        throw new Error;
                    }
                    result = GpioRequestProcessor::som_gpio_->SetPinDirection(som_pin,
                                                                              gpio_request.GetDirection());
                }
                    break;
                default: {
                    throw new Error;
                }
            }
        }
            break;
        default: {
            throw new Error;
        }
    }
    return result;
}

void GpioRequestProcessor::InitializeGpioPins() {
    for (auto itr = GpioRequestProcessor::component_to_default_direction_map_.begin();
         itr != GpioRequestProcessor::component_to_default_direction_map_.end(); itr++) {
        Component_e hardware_component = itr->first;
        GpioDirection_e direction = itr->second;
        Gpio *gpio = GpioRequestProcessor::component_to_device_map_.at(hardware_component);

        if (GpioRequestProcessor::component_to_device_map_.at(itr->first)->getDeviceId() ==
            GPIO_PCA9535) {
            gpio->SetPinDirection(
                    GpioRequestProcessor::component_to_pca_channel_map_.at(hardware_component),
                    direction);
            if (GpioRequestProcessor::component_to_default_direction_map_.at(hardware_component) ==
                DIRECTION_OUTPUT) {
                gpio->SetPinState(
                        GpioRequestProcessor::component_to_pca_channel_map_.at(hardware_component),
                        GpioRequestProcessor::component_to_default_state_map_.at(
                                hardware_component));
            }
        } else {
            gpio->SetPinDirection(
                    GpioRequestProcessor::component_to_som_channel_map_.at(hardware_component),
                    direction);
            if (GpioRequestProcessor::component_to_default_direction_map_.at(hardware_component) ==
                DIRECTION_OUTPUT) {
                gpio->SetPinState(
                        GpioRequestProcessor::component_to_som_channel_map_.at(hardware_component),
                        GpioRequestProcessor::component_to_default_state_map_.at(
                                hardware_component));
            }
        }

    }
}

void GpioRequestProcessor::PopulateDefaultDirectionMap() {
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {MACRO_POT_MUX_SELECT_1, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {MACRO_POT_MUX_SELECT_2, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {MACRO_POT_MUX_SELECT_3, DIRECTION_OUTPUT});

    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {MICRO_POT_MUX_SELECT_1, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {MICRO_POT_MUX_SELECT_2, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {MICRO_POT_MUX_SELECT_3, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {STIRRER_PHASE_GPIO, DIRECTION_OUTPUT});


    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {STIRRER_CONNECTION_FB, DIRECTION_OUTPUT});

    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {EXHAUST_FAN_ON_OFF, DIRECTION_OUTPUT});

    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {COUNTER_MUX_SELECT_0, DIRECTION_OUTPUT});

    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {COUNTER_MUX_SELECT_1, DIRECTION_OUTPUT});

    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {INDUCTION_PAN_GPIO, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {INDUCTION_INT_GPIO, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {INDUCTION_FAN_GPIO, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {INDUCTION_ON_OFF_GPIO, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {INDUCTION_BUZZER_GPIO, DIRECTION_INPUT});

    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {INA300_LATCH, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {INA300_ALERT, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {LM2678_CONTROL, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {MACRO_MICRO_FB_SELECT_1, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {HEATER_ENABLE, DIRECTION_OUTPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {LIMIT_SWITCH_1, DIRECTION_INPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {LIMIT_SWITCH_2, DIRECTION_INPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {INDUCTIVE_SENSOR_1, DIRECTION_INPUT});
    GpioRequestProcessor::component_to_default_direction_map_.insert(
            {INDUCTIVE_SENSOR_2, DIRECTION_INPUT});

}

void GpioRequestProcessor::PopulateDefaultStateMap() {


    GpioRequestProcessor::component_to_default_state_map_.insert(
            {MACRO_POT_MUX_SELECT_1, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {MACRO_POT_MUX_SELECT_2, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {MACRO_POT_MUX_SELECT_3, STATE_LOW});

    GpioRequestProcessor::component_to_default_state_map_.insert(
            {MICRO_POT_MUX_SELECT_1, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {MICRO_POT_MUX_SELECT_2, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {MICRO_POT_MUX_SELECT_3, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {STIRRER_PHASE_GPIO, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {EXHAUST_FAN_ON_OFF, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {COUNTER_MUX_SELECT_0, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {COUNTER_MUX_SELECT_1, STATE_LOW});


    GpioRequestProcessor::component_to_default_state_map_.insert(
            {STIRRER_CONNECTION_FB, STATE_LOW});

    GpioRequestProcessor::component_to_default_state_map_.insert(
            {INDUCTION_PAN_GPIO, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {INDUCTION_INT_GPIO, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {INDUCTION_FAN_GPIO, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {INDUCTION_ON_OFF_GPIO, STATE_LOW});

    GpioRequestProcessor::component_to_default_state_map_.insert(
            {INA300_LATCH, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {INA300_ALERT, STATE_HIGH});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {LM2678_CONTROL, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {MACRO_MICRO_FB_SELECT_1, STATE_LOW});
    GpioRequestProcessor::component_to_default_state_map_.insert(
            {HEATER_ENABLE, STATE_LOW});

}


#endif //JULIA_ANDROID_GPIOREQUESTPROCESSOR_H
