//
// Created by fedal on 9/12/20.
//

#include "GpioRequest.h"

const GpioAction_e &GpioRequest::GetAction() const {
    return action_;
}

GpioRequest *GpioRequest::SetAction(const GpioAction_e &action) {
    action_ = action;
    return this;
}

const GpioState_e &GpioRequest::GetState() const {
    return state_;
}

GpioRequest *GpioRequest::SetState(const GpioState_e &state) {
    state_ = state;
    return this;
}

Component_e GpioRequest::GetHardwareComponent() const {
    return hardware_component_;
}

GpioRequest *GpioRequest::SetHardwareComponent(Component_e hardwareComponent) {
    hardware_component_ = hardwareComponent;
    return this;
}

const GpioDirection_e &GpioRequest::GetDirection() const {
    return this->direction_;
}

GpioRequest *GpioRequest::SetDirection(const GpioDirection_e &direction) {
    this->direction_ = direction;
    return this;
}
