//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_GPIOREQUESTBUILDER_H
#define JULIA_ANDROID_GPIOREQUESTBUILDER_H

#include "GpioRequest.h"

class GpioRequestBuilder{
public:
    GpioRequest* gpio_request_;
    GpioRequestBuilder(){
        this->gpio_request_ = new GpioRequest;
    }
    ~GpioRequestBuilder(){
        delete this->gpio_request_;
    }


};


#endif //JULIA_ANDROID_GPIOREQUESTBUILDER_H
