//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_SOM_GPIO_H
#define JULIA_ANDROID_SOM_GPIO_H

#define IN  0
#define OUT 1

#define LOW  0
#define HIGH 1

class SOM_Gpio : public Gpio {
    static int
    GPIOExport(int pin) {
#define BUFFER_MAX 3
        char buffer[BUFFER_MAX];
        ssize_t bytes_written;
        int fd;

        fd = open("/sys/class/gpio/export", O_WRONLY);
        if (-1 == fd) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef",  "Failed to open export for writing!\n");
            return (-1);
        }

        bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
        write(fd, buffer, bytes_written);
        close(fd);
        return (0);
    }

    static int
    GPIOUnexport(int pin) {
        char buffer[BUFFER_MAX];
        ssize_t bytes_written;
        int fd;

        fd = open("/sys/class/gpio/unexport", O_WRONLY);
        if (-1 == fd) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef",  "Failed to open unexport for writing!\n");
            return (-1);
        }

        bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
        write(fd, buffer, bytes_written);
        close(fd);
        return (0);
    }

public:
    GpioState_e ReadPinState(int pin) {
#define VALUE_MAX 30
        char path[VALUE_MAX];
        char value_str[3];
        int fd;

        snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
        fd = open(path, O_RDONLY);
        if (-1 == fd) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef",
                                "Failed to open gpio %d value for reading!\n", pin);
            //throw new Error;
        }

        if (-1 == read(fd, value_str, 3)) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef",  "Failed to read value!\n");
            //throw new Error;
        }

        close(fd);
        int state = atoi(value_str);

        return state == 0 ? STATE_LOW : STATE_HIGH;
    }

    int SetPinState(int pin, GpioState_e value) {
        static const char s_values_str[] = "01";

        char path[VALUE_MAX];
        int fd;

        snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
        fd = open(path, O_WRONLY);
        if (-1 == fd) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef",  "Failed to open gpio value for writing!\n");
            //throw new Error;
        }

        if (1 != write(fd, &s_values_str[STATE_LOW == value ? 0 : 1], 1)) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef",  "Failed to write value!\n");
            //throw new Error;
        }

        close(fd);
        return (0);
    }

    int SetPinDirection(int pin, GpioDirection_e dir) {
        static const char s_directions_str[] = "in\0out";

#define DIRECTION_MAX 35
        char path[DIRECTION_MAX];
        int fd;

        snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
        fd = open(path, O_WRONLY);
        if (-1 == fd) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef",
                                "Failed to open gpio %d direction for writing!\n",pin);
            //throw new Error;
        }

        if (-1 == write(fd, &s_directions_str[DIRECTION_INPUT == dir ? 0 : 3],
                        DIRECTION_INPUT == dir ? 2 : 3)) {
            __android_log_print(ANDROID_LOG_ERROR, "Chef",  "Failed to set direction!\n");
            //throw new Error;
        }

        close(fd);
        return (0);
    }
};


#endif //JULIA_ANDROID_SOM_GPIO_H
