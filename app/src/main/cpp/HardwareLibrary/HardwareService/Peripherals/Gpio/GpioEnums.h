//
// Created by fedal on 16/12/20.
//

#ifndef JULIA_ANDROID_GPIOENUMS_H
#define JULIA_ANDROID_GPIOENUMS_H
typedef enum {
    STATE_HIGH,
    STATE_LOW,
    STATE_DEFAULT
} GpioState_e;
typedef enum {
    DIRECTION_INPUT,
    DIRECTION_OUTPUT,
    GPIO_DIRECTION_DEFAULT
} GpioDirection_e;
typedef enum {
    ACTION_READ_PIN,
    ACTION_WRITE_PIN,
    ACTION_SET_DIRECTION,
    ACTION_DEFAULT
} GpioAction_e;

#endif //JULIA_ANDROID_GPIOENUMS_H
