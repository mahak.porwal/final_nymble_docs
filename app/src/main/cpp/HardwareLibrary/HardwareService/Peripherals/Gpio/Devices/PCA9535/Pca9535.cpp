//
// Created by fedal on 9/12/20.
//

#include "Pca9535.h"
#include "Pca9535Enums.h"

int Pca9535::SetPinDirection(int _pin, GpioDirection_e direction) {
    uint8_t array_to_send[2];
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    int result = -1;
    uint8_t pin = (uint8_t) _pin;
    pca_9535_registers_e direction_pca;
    switch (direction) {
        case DIRECTION_INPUT:
            direction_pca = (CONFIG_INPUT);
            break;
        case DIRECTION_OUTPUT:
            direction_pca = (CONFIG_OUTPUT);
            break;
        default: {
            delete i2c_request_builder;
            throw new Error;
        }
            break;
    }
    if (direction_pca == CONFIG_INPUT) {
        if (pin < 8) {
            this->pca9535_port_0_dir_ = ((this->pca9535_port_0_dir_) | (0x01 << pin));
            array_to_send[0] = CMD_CONFIG_PORT_0;
            array_to_send[1] = this->pca9535_port_0_dir_;
        } else {
            pin -= 8;
            this->pca9535_port_1_dir_ = ((this->pca9535_port_1_dir_) | (0x01 << pin));
            array_to_send[0] = CMD_CONFIG_PORT_1;
            array_to_send[1] = this->pca9535_port_1_dir_;
        }
    } else {
        if (pin < 8) {
            this->pca9535_port_0_dir_ = ((this->pca9535_port_0_dir_) & (~(0x01 << pin)));
            array_to_send[0] = CMD_CONFIG_PORT_0;
            array_to_send[1] = this->pca9535_port_0_dir_;
        } else {
            pin -= 8;
            this->pca9535_port_1_dir_ = ((this->pca9535_port_1_dir_) & (~(0x01 << pin)));
            array_to_send[0] = CMD_CONFIG_PORT_1;
            array_to_send[1] = this->pca9535_port_1_dir_;
        }
    }

    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            GPIO_PCA9535)->SetNumberOfBytes(2)->SetArray(array_to_send)->SetSlaveAddress(
            this->address_);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    //usleep(100000);
    return result;
}

int Pca9535::SetPinState(int _pin, GpioState_e state) {
    uint8_t array_to_send[2];
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    int result = -1;
    uint8_t pin = (uint8_t) _pin;
    if (pin < 8) {
        if (state == STATE_HIGH) {
            this->pca9535_port_0_data_ |= 0x01 << pin;
        } else if (state == STATE_LOW) {
            this->pca9535_port_0_data_ &= ~(0x01 << pin);
        } else {
            return -2;
        }
        array_to_send[0] = CMD_OUTPUT_PORT_0;
        array_to_send[1] = this->pca9535_port_0_data_;
    } else {
        pin -= 8;
        if (state == STATE_HIGH) {
            this->pca9535_port_1_data_ |= 0x01 << pin;
        } else if (state == STATE_LOW) {
            this->pca9535_port_1_data_ &= ~(0x01 << pin);
        } else {
            return -2;
        }
        array_to_send[0] = CMD_OUTPUT_PORT_1;
        array_to_send[1] = this->pca9535_port_1_data_;
    }

    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            GPIO_PCA9535)->SetNumberOfBytes(2)->SetArray(array_to_send)->SetSlaveAddress(
            this->address_);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    //usleep(100000);
    return result;
}

GpioState_e Pca9535::ReadPinState(int _pin) {

    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    uint8_t buff[1];
    int result = -1;
    uint8_t state_read = 0;
    uint8_t data = 0x00;
    uint8_t pin = (uint8_t) _pin;
    pca_9535_registers_e register_access;
    if (pin < 8) {
        register_access = CMD_INPUT_PORT_0;
    } else {
        register_access = CMD_INPUT_PORT_1;
        //read = i2c_read_from_reg(file_descriptor_i2c3,PCA9535_ADDR,CMD_INPUT_PORT_1,buff,1);
        pin -= 8;
    }
    i2c_request_builder->i2c_request_->SetAction(I2C_REGISTER_READ)->SetDeviceId(
            GPIO_PCA9535)->SetRegisterAddress(register_access)->SetNumberOfBytes(1)->SetArray(
            buff)->SetSlaveAddress(this->address_);

    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));

    state_read = (0x01 << pin) & (buff[0]);
    if (state_read) {
        return STATE_HIGH;
    }
    return STATE_LOW;
}
