//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_GPIO_H
#define JULIA_ANDROID_GPIO_H


#include "GpioEnums.h"
#include "../../../Error/Error.h"

class Gpio {

private:
    uint8_t address_;
protected:
    I2cDevices_e device_id_ = DEFAULT_GPIO_DEVICE;
public:
    I2cDevices_e getDeviceId() const;

    void setDeviceId(I2cDevices_e deviceId);

public:

    virtual int SetPinDirection(int, GpioDirection_e) = 0;

    virtual int SetPinState(int, GpioState_e) = 0;

    virtual GpioState_e ReadPinState(int) = 0;

    virtual ~Gpio() {}
};


#endif //JULIA_ANDROID_GPIO_H
