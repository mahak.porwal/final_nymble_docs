//
// Created by fedal on 9/12/20.
//

#include "MAX6650.h"

uint8_t max_tx[5];
uint8_t max_rx[5];

int MAX6650::SetDutyCycle(uint8_t pwm_component_, float duty_cycle_) {
    int result = -1;
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();

    /*if(duty_cycle_==0){
        this->config_register_ |= MAX6650_CFG_MODE_OFF;
        max_tx[0] = MAX6650_REG_CONFIG;
        max_tx[1] = this->config_register_;

        i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
                PWM_MAX6650)->SetNumberOfBytes(2)->SetArray(pca_tx)->SetSlaveAddress(this->address_);

    }else if(duty_cycle_ == 100){
        this->config_register_ |= MAX6650_CFG_MODE_ON;
        max_tx[0] = MAX6650_REG_CONFIG;
        max_tx[1] = this->config_register_;

        i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
                PWM_MAX6650)->SetNumberOfBytes(2)->SetArray(pca_tx)->SetSlaveAddress(this->address_);
    }else{

    }*/

    duty_cycle_ = 100 - duty_cycle_;

    this->dac_register_ = uint8_t((duty_cycle_ / 100) * MAX6550_MAX_DAC_VALUE_12V);
    max_tx[0] = MAX6650_REG_DAC;
    max_tx[1] = this->dac_register_;

    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_MAX6650)->SetNumberOfBytes(2)->SetArray(max_tx)->SetSlaveAddress(this->address_);

    result = I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder->i2c_request_);

    result = this->ReadRegister(MAX6650_REG_DAC);

    __android_log_print(ANDROID_LOG_DEBUG, "Max6650", "DAC %d", max_rx[0]);

    result = this->ReadRegister(MAX6650_REG_CONFIG);

    __android_log_print(ANDROID_LOG_DEBUG, "Max6650", "CONFIG %d", max_rx[0]);

    result = this->ReadRegister(MAX6650_REG_GPIO_DEF);

    __android_log_print(ANDROID_LOG_DEBUG, "Max6650", "GPIO DEFINITION %d", max_rx[0]);

    result = this->ReadRegister(MAX6650_REG_GPIO_STAT);

    __android_log_print(ANDROID_LOG_DEBUG, "Max6650", "GPIO STATUS %d", max_rx[0]);

    result = this->ReadRegister(MAX6650_REG_ALARM);

    __android_log_print(ANDROID_LOG_DEBUG, "Max6650", "GPIO ALARM %d", max_rx[0]);

    result = this->ReadRegister(MAX6650_REG_ALARM_EN);

    __android_log_print(ANDROID_LOG_DEBUG, "Max6650", "GPIO ALARM EN %d", max_rx[0]);


    return result;
}

uint8_t MAX6650::SetOpenLoopControl() {
    int result = -1;
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();

    this->config_register_ |= MAX6650_CFG_MODE_OPEN_LOOP;
    max_tx[0] = MAX6650_REG_CONFIG;
    max_tx[1] = this->config_register_;

    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_MAX6650)->SetNumberOfBytes(2)->SetArray(max_tx)->SetSlaveAddress(this->address_);

    result = I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder->i2c_request_);
    return result;
}

uint8_t MAX6650::ReadRegister(uint8_t read_register) {
    uint8_t result = 0;
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();

    this->config_register_ |= MAX6650_CFG_MODE_OPEN_LOOP;
    max_tx[0] = MAX6650_REG_CONFIG;
    max_tx[1] = this->config_register_;

    i2c_request_builder->i2c_request_->SetAction(I2C_REGISTER_READ)->SetDeviceId(
            PWM_MAX6650)->SetNumberOfBytes(1)->SetArray(max_rx)->SetRegisterAddress(
            read_register)->SetSlaveAddress(this->address_);

    result = I2CRequestProcessor::ProcessI2cRequest(*i2c_request_builder->i2c_request_);
    return result;
}
