//
// Created by fedal on 9/12/20.
//

#include "MCP4725.h"

int MCP4725::SetDutyCycle(uint8_t pwm_component_, float duty_cycle_) {

    uint8_t tx[2];
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    int result = -1;
    float voltage = (duty_cycle_ / 100.0) * 5.0;
    uint16_t vol_corr_reg = (uint16_t) (819 * (voltage));

    uint8_t first8bit = vol_corr_reg & 0xff;
    uint8_t last4bit = (vol_corr_reg >> 8) & 0xff;

    tx[1] = first8bit;
    tx[0] = last4bit;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_MCP4725)->SetSlaveAddress(this->address_)->SetArray(
            tx)->SetNumberOfBytes(2);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));

    return result;
}
