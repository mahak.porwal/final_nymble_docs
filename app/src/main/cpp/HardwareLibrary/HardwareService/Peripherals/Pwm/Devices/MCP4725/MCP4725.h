//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_MCP4725_H
#define JULIA_ANDROID_MCP4725_H


#include "../../Pwm.h"

#define ONE_VOLT_REG_VAL 819;

class MCP4725 : public PwmGenerator {
    uint8_t address_;
public:
    MCP4725(uint8_t address) { this->address_ = address; }

    int SetDutyCycle(uint8_t pwm_component_, float duty_cycle_);
};


#endif //JULIA_ANDROID_MCP4725_H
