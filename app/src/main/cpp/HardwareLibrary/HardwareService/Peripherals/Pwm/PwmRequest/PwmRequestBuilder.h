//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_PWMREQUESTBUILDER_H
#define JULIA_ANDROID_PWMREQUESTBUILDER_H

#include "PwmRequest.h"

class PwmRequestBuilder{
public:
    PwmRequest* pwm_request_;

    PwmRequestBuilder(){
        this->pwm_request_ = new PwmRequest;
    }
    ~PwmRequestBuilder(){
        delete this->pwm_request_;
    }
};
#endif //JULIA_ANDROID_PWMREQUESTBUILDER_H
