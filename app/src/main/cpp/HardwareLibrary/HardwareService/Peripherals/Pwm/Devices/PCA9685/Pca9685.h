//
// Created by fedal on 10/12/20.
//

#ifndef JULIA_ANDROID_PCA9685_H
#define JULIA_ANDROID_PCA9685_H


#include "../../Pwm.h"
#include "PCA9685Enums.h"

typedef enum {
    MODE_1 = 0x00,
    PRE_SCALE = 0xFE,
    LED0_ON_L = 0x06,
    LED0_ON_H,
    LED0_OFF_L,
    LED0_OFF_H
} RegisterAddress_e;

class Pca9685 : public PwmGenerator {
public:
    int SetDutyCycle(uint8_t pwm_component_, float duty_cycle_);

    Pca9685(uint8_t address,I2cDevices_e device_id) {
        this->address_ = address;
        this->PcaInit();
        this->device_id_ = device_id;
    }

private:
    void PcaInit();

    int SetPcaDuty(uint16_t duty_to_set, Pca9685Channels_e);

    int SetPcaFrequency(uint16_t frequency);
};


#endif //JULIA_ANDROID_PCA9685_H
