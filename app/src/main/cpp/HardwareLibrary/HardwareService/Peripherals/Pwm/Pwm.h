//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_PWM_H
#define JULIA_ANDROID_PWM_H

#include "../../../Error/Error.h"
#include "../I2C/I2CRequestProcessor/I2CRequestProcessor.h"


class PwmGenerator {
protected:
    uint8_t address_;
    I2cDevices_e device_id_;
public:
    I2cDevices_e GetDeviceId() const;

    void SetDeviceId(I2cDevices_e deviceId);

public:
    virtual int SetDutyCycle(uint8_t channel, float duty_cycle_) = 0;
};

#endif //JULIA_ANDROID_PWM_H
