//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_PWMREQUESTPROCESSOR_H
#define JULIA_ANDROID_PWMREQUESTPROCESSOR_H


#include "../Devices/PCA9685/PCA9685Enums.h"
#include "../PwmRequest/PwmRequest.h"
#include "../Pwm.h"
#include "../Devices/PCA9685/Pca9685.h"
#include "../Devices/DS1050Z/DS1050Z.h"
#include "../Devices/MAX6650/MAX6650.h"
#include "../../../NonVolatileFactors.h"

class PwmRequestProcessor {
private:
    static map<Component_e, PwmGenerator *> component_to_device_map_;
    static map<Component_e, Pca9685Channels_e> component_to_pca_channel_map_;

    static void PopulateComponentDeviceMap();

    static void PopulateComponentPcaChannelMap();

    static void SetDefaultPwmValue();

    static Pca9685 *pca9685_;
    static DS1050Z *exhaust_fan_;
    static DS1050Z *led_pwm_;
    static DS1050Z *stirrer_pwm_;
    static DS1050Z *induction_pwm_;
    static std::unordered_map<std::string, std::string> hardware_factors_;

public:
    PwmRequestProcessor(std::unordered_map<std::string, std::string> hardware_factors) {
        this->hardware_factors_ = hardware_factors;
        this->pca9685_ = new Pca9685(0x42, PWM_PCA9685);
        this->exhaust_fan_ = new DS1050Z(0x29,
                                         PWM_DS1050Z_EXHAUST);//new MAX6650(0x48, PWM_MAX6650);
        this->led_pwm_ = new DS1050Z(0x2F, PWM_DS1050Z_LED_ILLUMINATION);
        this->stirrer_pwm_ = new DS1050Z(0x2E, PWM_DS1050Z_STIRRER);
        this->induction_pwm_ = new DS1050Z(0x2A, PWM_DS1050Z_INDUCTION);
        PwmRequestProcessor::PopulateComponentDeviceMap();
        PwmRequestProcessor::PopulateComponentPcaChannelMap();
        PwmRequestProcessor::SetDefaultPwmValue();
    }

    ~PwmRequestProcessor() {
        delete this->pca9685_;
        delete this->exhaust_fan_;
        delete this->led_pwm_;
        delete this->stirrer_pwm_;
        delete this->induction_pwm_;
    }

    static int ProcessPwmRequest(PwmRequest pwmRequest);
};

map<Component_e, PwmGenerator *> PwmRequestProcessor::component_to_device_map_;
map<Component_e, Pca9685Channels_e> PwmRequestProcessor::component_to_pca_channel_map_;
DS1050Z *PwmRequestProcessor::stirrer_pwm_;
DS1050Z *PwmRequestProcessor::exhaust_fan_;
DS1050Z *PwmRequestProcessor::led_pwm_;
Pca9685 *PwmRequestProcessor::pca9685_;
DS1050Z *PwmRequestProcessor::induction_pwm_;
std::unordered_map<std::string, std::string> PwmRequestProcessor::hardware_factors_;

void PwmRequestProcessor::PopulateComponentDeviceMap() {
    PwmRequestProcessor::component_to_device_map_.insert(
            {MACRO_SERVO_1, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {MACRO_SERVO_2, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {MACRO_SERVO_3, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {MACRO_SERVO_4, PwmRequestProcessor::pca9685_});

    PwmRequestProcessor::component_to_device_map_.insert(
            {MICRO_SERVO_1, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {MICRO_SERVO_2, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {MICRO_SERVO_3, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {MICRO_SERVO_4, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {MICRO_SERVO_5, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {MICRO_SERVO_6, PwmRequestProcessor::pca9685_});

    PwmRequestProcessor::component_to_device_map_.insert(
            {EXHAUST_FAN, PwmRequestProcessor::exhaust_fan_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {STIRRER_MOTOR, PwmRequestProcessor::stirrer_pwm_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {LED_ILLUMINATION, PwmRequestProcessor::led_pwm_});

    PwmRequestProcessor::component_to_device_map_.insert(
            {LED_DESIGN, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert(
            {WATER_PUMP, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert({OIL_PUMP, PwmRequestProcessor::pca9685_});
    PwmRequestProcessor::component_to_device_map_.insert({BUZZER, PwmRequestProcessor::pca9685_});

    PwmRequestProcessor::component_to_device_map_.insert(
            {INDUCTION_ACTUATOR, PwmRequestProcessor::induction_pwm_});


}

void PwmRequestProcessor::PopulateComponentPcaChannelMap() {

    PwmRequestProcessor::component_to_pca_channel_map_.insert({MACRO_SERVO_1, CHANNEL_12});
    PwmRequestProcessor::component_to_pca_channel_map_.insert({MACRO_SERVO_2, CHANNEL_13});
    PwmRequestProcessor::component_to_pca_channel_map_.insert({MACRO_SERVO_3, CHANNEL_14});
    PwmRequestProcessor::component_to_pca_channel_map_.insert({MACRO_SERVO_4, CHANNEL_15});

    PwmRequestProcessor::component_to_pca_channel_map_.insert({MICRO_SERVO_1, CHANNEL_2});
    PwmRequestProcessor::component_to_pca_channel_map_.insert({MICRO_SERVO_2, CHANNEL_1});
    PwmRequestProcessor::component_to_pca_channel_map_.insert({MICRO_SERVO_3, CHANNEL_4});
    PwmRequestProcessor::component_to_pca_channel_map_.insert({MICRO_SERVO_4, CHANNEL_5});
    PwmRequestProcessor::component_to_pca_channel_map_.insert({MICRO_SERVO_5, CHANNEL_0});
    PwmRequestProcessor::component_to_pca_channel_map_.insert({MICRO_SERVO_6, CHANNEL_1});

    PwmRequestProcessor::component_to_pca_channel_map_.insert({LED_DESIGN, CHANNEL_9});

    PwmRequestProcessor::component_to_pca_channel_map_.insert({BUZZER, CHANNEL_8});

    PwmRequestProcessor::component_to_pca_channel_map_.insert({WATER_PUMP, CHANNEL_6});
    PwmRequestProcessor::component_to_pca_channel_map_.insert({OIL_PUMP, CHANNEL_7});

}

int PwmRequestProcessor::ProcessPwmRequest(PwmRequest pwmRequest) {
    I2cDevices_e device_id = PwmRequestProcessor::component_to_device_map_.at(
            pwmRequest.GetHardwareComponent())->GetDeviceId();
    int result = -1;
    switch (device_id) {
        case PWM_PCA9685: {
            Pca9685Channels_e channel = PwmRequestProcessor::component_to_pca_channel_map_.at(
                    pwmRequest.GetHardwareComponent());
            if (pwmRequest.GetDutyCycle() != -1) {
                result = PwmRequestProcessor::pca9685_->SetDutyCycle(channel,
                                                                     pwmRequest.GetDutyCycle());
            } else if (pwmRequest.GetPulseDurationUs() != -1) {
                float duty_cycle = 0.0;
                duty_cycle = (pwmRequest.GetPulseDurationUs() / 200.0);
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                    "pca dutycycle %.2f", duty_cycle);
                result = PwmRequestProcessor::pca9685_->SetDutyCycle(channel, duty_cycle);
            } else {
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                    "pwm processor no duty cycle and no pulse duration ");
                throw (new Error);
            }
            break;
        }
        case PWM_DS1050Z_INDUCTION:
        case PWM_DS1050Z_LED_ILLUMINATION:
        case PWM_DS1050Z_STIRRER:
        case PWM_MAX6650:
        case PWM_DS1050Z_EXHAUST: {
            PwmGenerator *pwm_generator = PwmRequestProcessor::component_to_device_map_.at(
                    pwmRequest.GetHardwareComponent());
            result = pwm_generator->SetDutyCycle(0, pwmRequest.GetDutyCycle());
            break;
        }
        default:
            break;
    }

    return result;
}

void PwmRequestProcessor::SetDefaultPwmValue() {

    for (auto itr = PwmRequestProcessor::component_to_device_map_.begin();
         itr != PwmRequestProcessor::component_to_device_map_.end(); itr++) {

        if (itr->first == INDUCTION_ACTUATOR /*|| itr->first == EXHAUST_FAN*/
            || itr->first == LED_DESIGN || itr->first == LED_ILLUMINATION)
            continue;
        if (itr->second != PwmRequestProcessor::pca9685_) {
            itr->second->SetDutyCycle(0, 0);
        } else {
            if (itr->first == MACRO_SERVO_1) {
                std::string key = MACRO_ONE_HOME;
                uint16_t macro_home = 0;
                if (hardware_factors_.find(key) != hardware_factors_.end()) {
                    macro_home = (uint16_t) std::stod(hardware_factors_.at(key.c_str()));
                }
                float pulseDuration = macro_home * 11.1 + 500;
                float duty_cycle = pulseDuration / 200;
                itr->second->SetDutyCycle(
                        PwmRequestProcessor::component_to_pca_channel_map_.at(itr->first),
                        duty_cycle);
            } else if (itr->first == MACRO_SERVO_2) {
                std::string key = MACRO_TWO_HOME;
                uint16_t macro_home = 0;
                if (hardware_factors_.find(key) != hardware_factors_.end()) {
                    macro_home = (uint16_t) std::stod(hardware_factors_.at(key.c_str()));
                }
                float pulseDuration = macro_home * 11.1 + 500;
                float duty_cycle = pulseDuration / 200;
                itr->second->SetDutyCycle(
                        PwmRequestProcessor::component_to_pca_channel_map_.at(itr->first),
                        duty_cycle);
            } else if (itr->first == MACRO_SERVO_3) {
                std::string key = MACRO_THREE_HOME;
                uint16_t macro_home = 0;
                if (hardware_factors_.find(key) != hardware_factors_.end()) {
                    macro_home = (uint16_t) std::stod(hardware_factors_.at(key.c_str()));
                }
                float pulseDuration = macro_home * 11.1 + 500;
                float duty_cycle = pulseDuration / 200;
                itr->second->SetDutyCycle(
                        PwmRequestProcessor::component_to_pca_channel_map_.at(itr->first),
                        duty_cycle);
            }/* else if (itr->first == MACRO_SERVO_4) {
                std::string key = MACRO_FOUR_HOME;
                uint16_t macro_home = 0;
                if (hardware_factors_.find(key) != hardware_factors_.end()) {
                    macro_home = (uint16_t) std::stod(hardware_factors_.at(key.c_str()));
                }
                float pulseDuration = macro_home * 11.1 + 500;
                float duty_cycle = pulseDuration / 200;
                itr->second->SetDutyCycle(
                        PwmRequestProcessor::component_to_pca_channel_map_.at(itr->first),
                        duty_cycle);
            }*/ else {
                itr->second->SetDutyCycle(
                        PwmRequestProcessor::component_to_pca_channel_map_.at(itr->first), 0);
            }
        }
    }
}


#endif //JULIA_ANDROID_PWMREQUESTPROCESSOR_H
