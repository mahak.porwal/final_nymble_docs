//
// Created by fedal on 9/12/20.
//

#include "PwmRequest.h"

float PwmRequest::GetDutyCycle() const {
    return duty_cycle_;
}

PwmRequest *PwmRequest::SetDutyCycle(float dutyCycle) {
    duty_cycle_ = dutyCycle;
    return this;
}

float PwmRequest::GetFrequency() const {
    return frequency_;
}

PwmRequest *PwmRequest::SetFrequency(float frequency) {
    frequency_ = frequency;
    return this;
}

Component_e PwmRequest::GetHardwareComponent() const {
    return
            hardware_component_;
}

PwmRequest *PwmRequest::SetHardwareComponent(Component_e hardwareComponent) {
    hardware_component_ = hardwareComponent;
    return this;
}

int PwmRequest::GetPulseDurationUs() const {
    return pulse_duration_us;
}

PwmRequest *PwmRequest::SetPulseDurationUs(uint16_t pulseDurationUs) {
    pulse_duration_us = pulseDurationUs;
    return this;
}
