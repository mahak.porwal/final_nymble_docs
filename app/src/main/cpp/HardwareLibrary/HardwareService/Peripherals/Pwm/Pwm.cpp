//
// Created by fedal on 9/12/20.
//

#include "../I2C/I2CRequestProcessor/I2CRequestProcessor.h"
#include "Pwm.h"

I2cDevices_e PwmGenerator::GetDeviceId() const{
return
device_id_;
}

void PwmGenerator::SetDeviceId(I2cDevices_e deviceId) {
    device_id_ = deviceId;
}
