//
// Created by fedal on 9/12/20.
//

#include "DS1050Z.h"

int DS1050Z::SetDutyCycle(uint8_t pwm_component_, float duty_cycle_) {
    uint8_t ds1050z_tx[1];
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    uint16_t duty_cycle_to_set = 0;
    int result = -1;
    duty_cycle_to_set = (uint16_t) ((duty_cycle_ / 100.0) * 31);
    ds1050z_tx[0] = duty_cycle_to_set;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            this->device_id_)->SetNumberOfBytes(1)->SetArray(ds1050z_tx)->SetSlaveAddress(
            this->address_);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    delete i2c_request_builder;
    return result;
}

