//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_PWMREQUEST_H
#define JULIA_ANDROID_PWMREQUEST_H




class PwmRequest{
private:
    float duty_cycle_ = -1;
    int pulse_duration_us = -1;
public:
    int GetPulseDurationUs() const;

    PwmRequest* SetPulseDurationUs(uint16_t pulseDurationUs);

public:
    float GetDutyCycle() const;

    PwmRequest* SetDutyCycle(float dutyCycle);

    float GetFrequency() const;

    PwmRequest* SetFrequency(float frequency);

    Component_e GetHardwareComponent() const;

    PwmRequest* SetHardwareComponent(Component_e hardwareComponent);

private:
    float frequency_ = -1;
    Component_e hardware_component_;
};
#endif //JULIA_ANDROID_PWMREQUEST_H
