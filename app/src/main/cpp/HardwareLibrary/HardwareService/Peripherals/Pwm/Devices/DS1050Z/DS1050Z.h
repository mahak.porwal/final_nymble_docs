//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_DS1050Z_H
#define JULIA_ANDROID_DS1050Z_H

#include "../../Pwm.h"

class DS1050Z : public PwmGenerator {
public:
    int SetDutyCycle(uint8_t pwm_component_, float duty_cycle_);

    DS1050Z(uint8_t address, I2cDevices_e device_id) {
        this->device_id_ = device_id;
        this->address_ = address;
    }
};

#endif //JULIA_ANDROID_DS1050Z_H