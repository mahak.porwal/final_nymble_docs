//
// Created by fedal on 10/12/20.
//

#include "Pca9685.h"
#include "../../../I2C/I2CRequest/I2CRequestBuilder.h"

uint8_t pca_tx[5];

int Pca9685::SetDutyCycle(uint8_t pwm_component_, float duty_cycle_) {
    uint16_t duty_cycle_to_set = 0;
    int result = -1;
    duty_cycle_to_set = (uint16_t) ((duty_cycle_ / 100.0) * 4095);
    result = this->SetPcaDuty(duty_cycle_to_set, static_cast<Pca9685Channels_e>(pwm_component_));
    return result;
}

void Pca9685::PcaInit() {

    /**
     * Sleep
     */
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    pca_tx[0] = MODE_1;
    pca_tx[1] = 0x31;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_PCA9685)->SetNumberOfBytes(2)->SetArray(pca_tx)->SetSlaveAddress(this->address_);
    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    usleep(100000);
    /**
     * Use external oscillator
     */

    /*
    pca_tx[0] = MODE_1;
    pca_tx[1] = 0xC1;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_PCA9685)->SetNumberOfBytes(2)->SetArray(pca_tx)->SetSlaveAddress(this->address_);
            I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    usleep(100000);*/

    /**
     * Set frequency pre-scaler
     */
    pca_tx[0] = PRE_SCALE;
    pca_tx[1] = 124;//50Hz
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_PCA9685)->SetNumberOfBytes(2)->SetArray(pca_tx)->SetSlaveAddress(this->address_);

    /**
     * Disable sleep
     */
    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    usleep(100000);
    pca_tx[0] = MODE_1;
    pca_tx[1] = 0x61;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_PCA9685)->SetNumberOfBytes(2)->SetArray(pca_tx)->SetSlaveAddress(this->address_);
    I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    usleep(100000);

    delete i2c_request_builder;
}

int Pca9685::SetPcaDuty(uint16_t duty_to_set, Pca9685Channels_e channel) {
    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();
    int result = -1;
    pca_tx[0] = channel;
    pca_tx[1] = 0x00;
    pca_tx[2] = 0x00;
    pca_tx[3] = (duty_to_set) & 0xFF;
    pca_tx[4] = (duty_to_set >> 0x08);
    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                        "Servo Sequence pca channel %d pca duty %d",channel, duty_to_set);
    //i2c_write(file_descriptor_i2c3,PCA_ADDRESS,5,pca_tx);
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_PCA9685)->SetNumberOfBytes(5)->SetArray(pca_tx)->SetSlaveAddress(this->address_);
    result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    delete i2c_request_builder;
    return result;
}

int Pca9685::SetPcaFrequency(uint16_t frequency) {

    int i2c_result = -1;

    I2CRequestBuilder *i2c_request_builder = new I2CRequestBuilder();

    if (frequency > 1526 || frequency < 24)
        return -2;
    double numerator = 6103.515625;
    double result = numerator / frequency;
    uint8_t toWrite = (uint8_t) (floor(result));
    pca_tx[0] = MODE_1;
    pca_tx[1] = 0x31;
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_PCA9685)->SetNumberOfBytes(2)->SetArray(pca_tx)->SetSlaveAddress(this->address_);
    i2c_result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    pca_tx[0] = PRE_SCALE;
    pca_tx[1] = toWrite - 1;//50Hz
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_PCA9685)->SetNumberOfBytes(2)->SetArray(pca_tx)->SetSlaveAddress(this->address_);
    i2c_result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    pca_tx[0] = MODE_1;
    pca_tx[1] = 0x21;//50Hz
    i2c_request_builder->i2c_request_->SetAction(I2C_WRITE)->SetDeviceId(
            PWM_PCA9685)->SetNumberOfBytes(2)->SetArray(pca_tx)->SetSlaveAddress(this->address_);
    i2c_result = I2CRequestProcessor::ProcessI2cRequest(*(i2c_request_builder->i2c_request_));
    delete i2c_request_builder;
    return i2c_result;
}
