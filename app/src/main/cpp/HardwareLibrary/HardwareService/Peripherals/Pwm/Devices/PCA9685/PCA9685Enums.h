//
// Created by fedal on 10/12/20.
//

#ifndef JULIA_ANDROID_PCA9685ENUMS_H
#define JULIA_ANDROID_PCA9685ENUMS_H
typedef enum{
    CHANNEL_0 = 6,
    CHANNEL_1 = 10,
    CHANNEL_2 = 14,
    CHANNEL_3 = 18,
    CHANNEL_4 = 22,
    CHANNEL_5 = 26,
    CHANNEL_6 = 30,
    CHANNEL_7 = 34,
    CHANNEL_8 = 38,
    CHANNEL_9 = 42,
    CHANNEL_10 = 46,
    CHANNEL_11 = 50,
    CHANNEL_12 = 54,
    CHANNEL_13 = 58,
    CHANNEL_14 = 62,
    CHANNEL_15 = 66
}Pca9685Channels_e;
#endif //JULIA_ANDROID_PCA9685ENUMS_H
