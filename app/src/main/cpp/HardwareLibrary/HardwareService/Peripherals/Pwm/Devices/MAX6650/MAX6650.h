//
// Created by fedal on 9/12/20.
//

#ifndef JULIA_ANDROID_MAX6650_H
#define JULIA_ANDROID_MAX6650_H


#include "../../Pwm.h"
#include "MAX6650Enums.h"

class MAX6650 : public PwmGenerator {
private:
    uint8_t config_register_ = MAX6650_CFG_DEFAULT;
    uint8_t dac_register_ = 0x00;

    uint8_t SetOpenLoopControl();

    uint8_t ReadRegister(uint8_t);

public:
    int SetDutyCycle(uint8_t pwm_component_, float duty_cycle_);

    MAX6650(uint8_t address, I2cDevices_e device_id) {
        this->address_ = address;
        this->device_id_ = device_id;
        this->SetOpenLoopControl();
    }
};


#endif //JULIA_ANDROID_MAX6650_H
