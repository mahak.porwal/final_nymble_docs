//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_POWERLEVELTHREEBEHAVIOR_H
#define JULIA_ANDROID_POWERLEVELTHREEBEHAVIOR_H


#include "../PowerLevelControlBehavior.h"

class PowerLevelThreeBehavior : public PowerLevelControlBehavior{
public:
    PowerLevelThreeBehavior(SequenceBehavior_e behaviorE) :
    PowerLevelControlBehavior(behaviorE) {
            this->induction_control_pid_->SetTargetValue(600);
    }

};


#endif //JULIA_ANDROID_POWERLEVELTHREEBEHAVIOR_H
