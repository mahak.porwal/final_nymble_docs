//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_POWERLEVELONECONTROLBEHAVIOR_H
#define JULIA_ANDROID_POWERLEVELONECONTROLBEHAVIOR_H


#include "../../../SequenceBehaviorEnums.h"
#include "../PowerLevelControlBehavior.h"

class PowerLevelOneControlBehavior :public PowerLevelControlBehavior{
public:
    PowerLevelOneControlBehavior(SequenceBehavior_e behaviorE) :
    PowerLevelControlBehavior(behaviorE) {
            this->induction_control_pid_->SetTargetValue(200);
    }
};


#endif //JULIA_ANDROID_POWERLEVELONECONTROLBEHAVIOR_H
