//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_POWERLEVELSIXCONTROLBEHAVIOR_H
#define JULIA_ANDROID_POWERLEVELSIXCONTROLBEHAVIOR_H


#include "../PowerLevelControlBehavior.h"

class PowerLevelSixControlBehavior : public PowerLevelControlBehavior{
public:
    PowerLevelSixControlBehavior(SequenceBehavior_e behaviorE) :
    PowerLevelControlBehavior(behaviorE) {
            this->induction_control_pid_->SetTargetValue(1200);
    }

};


#endif //JULIA_ANDROID_POWERLEVELSIXCONTROLBEHAVIOR_H
