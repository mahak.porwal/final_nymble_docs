//
// Created by fedal on 28/12/20.
//

#include "PowerLevelZeroControlBehavior.h"
#include "../../../../../HardwareService/HardwareService.h"

bool PowerLevelZeroControlBehavior::ExecuteSequence(ComponentRequest *sequenceRequest) {
    this->PollSensors();
    try {
        this->DetectErrors();
    }
    catch (Error e) {
        this->PowerDown();
        throw e;
    }
    if (this->induction_current_ * this->induction_voltage_ > 200) {
        this->PowerDown();
    }
    return true;
}

void PowerLevelZeroControlBehavior::PowerDown() {
    ComponentRequestBuilder *componentRequestBuilder = new ComponentRequestBuilder;
    componentRequestBuilder->component_request_.SetComponent(
            INDUCTION_ACTUATOR).SetSpeed(0);
    HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
            componentRequestBuilder->component_request_);
    delete componentRequestBuilder;

    componentRequestBuilder = new ComponentRequestBuilder;
    componentRequestBuilder->component_request_.SetComponent(
            INDUCTION_INT_GPIO).SetState(OFF_STATE);
    HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
            componentRequestBuilder->component_request_);
    delete componentRequestBuilder;
}
