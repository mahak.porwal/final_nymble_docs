//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_POWERLEVELFIVECONTROLBEHAVIOR_H
#define JULIA_ANDROID_POWERLEVELFIVECONTROLBEHAVIOR_H


#include "../PowerLevelControlBehavior.h"

class PowerLevelFiveControlBehavior : public PowerLevelControlBehavior {
public:
    PowerLevelFiveControlBehavior(SequenceBehavior_e behaviorE) :
            PowerLevelControlBehavior(behaviorE) {
        this->induction_control_pid_->SetTargetValue(1000);
    }
};


#endif //JULIA_ANDROID_POWERLEVELFIVECONTROLBEHAVIOR_H
