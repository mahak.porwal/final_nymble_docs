//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_IDLEBEHAVIOR_H
#define JULIA_ANDROID_IDLEBEHAVIOR_H


#include "../../SequenceBehavior.h"
#include "../PowerControlBehaviors/PowerLevelControlBehavior.h"
#include "../../../../Component/Request/ComponentRequestBuilder.h"

class IdleBehavior : public PowerLevelControlBehavior {

    bool ExecuteSequence(ComponentRequest *sequenceRequest,
                         std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);

public:
    IdleBehavior(SequenceBehavior_e behaviorE) :
            PowerLevelControlBehavior(behaviorE) {
        this->induction_control_pid_->SetTargetValue(0);
    }
};


#endif //JULIA_ANDROID_IDLEBEHAVIOR_H
