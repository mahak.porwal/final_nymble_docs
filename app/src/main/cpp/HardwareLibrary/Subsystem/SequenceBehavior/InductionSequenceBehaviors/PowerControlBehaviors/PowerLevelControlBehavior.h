//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_POWERLEVELCONTROLBEHAVIOR_H
#define JULIA_ANDROID_POWERLEVELCONTROLBEHAVIOR_H


#include "../../SequenceBehavior.h"
#include "../../../Induction/Utilities/InductionControlPid/InductionControlPid.h"
#include "../../../Induction/Utilities/InductionPingControl/InductionPingControl.h"


class PowerLevelControlBehavior : public SequenceBehavior {
protected:
    /**
     * Sensor Reading
     */
    float induction_voltage_;
    float induction_current_;
    float induction_igbt_temperature_;
    float induction_pan_temperature_;
    /**
     * Thresholds for error
     */
    float induction_voltage_upper_threshold_;
    float induction_voltage_lower_threshold_;
    float induction_current_upper_threshold_;
    float induction_current_lower_threshold_;
    float induction_igbt_temperature_upper_threshold_;
    float induction_igbt_temperature_lower_threshold_;
    float induction_pan_temperature_upper_threshold_;
    float induction_pan_temperature_lower_threshold_;
    InductionControlPid *induction_control_pid_;
    InductionPingControl *induction_ping_control_;

    virtual int DetectErrors();

    virtual int PollSensors();

    static bool IsOutOfRange(float, float, float);

    PowerLevelControlBehavior(SequenceBehavior_e behaviorE) {
        this->behaviour_ = behaviorE;
        this->induction_control_pid_ = new InductionControlPid(0.009, 0.006, 0);
        this->induction_ping_control_ = new InductionPingControl(1.5, 50, 4);
        this->induction_voltage_lower_threshold_ = 90;
        this->induction_voltage_upper_threshold_ = 140;
        this->induction_current_lower_threshold_ = 0;
        this->induction_current_upper_threshold_ = 16;
        this->induction_igbt_temperature_lower_threshold_ = 0;
        this->induction_igbt_temperature_upper_threshold_ = 90;
        this->induction_pan_temperature_lower_threshold_ = 0;
        this->induction_pan_temperature_upper_threshold_ = 200;
    }

    virtual bool ExecuteSequence(ComponentRequest *sequenceRequest,
                                 std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);

    virtual ~PowerLevelControlBehavior() {
        delete this->induction_control_pid_;
        delete this->induction_ping_control_;
    }
};


#endif //JULIA_ANDROID_POWERLEVELCONTROLBEHAVIOR_H
