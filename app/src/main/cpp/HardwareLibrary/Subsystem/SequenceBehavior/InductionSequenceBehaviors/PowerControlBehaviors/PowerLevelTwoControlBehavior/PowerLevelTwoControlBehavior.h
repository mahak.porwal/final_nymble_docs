//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_POWERLEVELTWOCONTROLBEHAVIOR_H
#define JULIA_ANDROID_POWERLEVELTWOCONTROLBEHAVIOR_H


#include "../PowerLevelControlBehavior.h"
#include "../../../SequenceBehaviorEnums.h"
class PowerLevelTwoControlBehavior : public PowerLevelControlBehavior{
public:
    PowerLevelTwoControlBehavior(SequenceBehavior_e behaviorE) :
    PowerLevelControlBehavior(behaviorE) {
            this->induction_control_pid_->SetTargetValue(200);
    }
};


#endif //JULIA_ANDROID_POWERLEVELTWOCONTROLBEHAVIOR_H
