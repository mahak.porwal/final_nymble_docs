//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_POWERLEVELFOURCONTROLBEHAVIOR_H
#define JULIA_ANDROID_POWERLEVELFOURCONTROLBEHAVIOR_H


#include "../PowerLevelControlBehavior.h"


class PowerLevelFourControlBehavior : public PowerLevelControlBehavior{
public:
    PowerLevelFourControlBehavior(SequenceBehavior_e behaviorE) :
    PowerLevelControlBehavior(behaviorE) {
            this->induction_control_pid_->SetTargetValue(800);
    }

};


#endif //JULIA_ANDROID_POWERLEVELFOURCONTROLBEHAVIOR_H
