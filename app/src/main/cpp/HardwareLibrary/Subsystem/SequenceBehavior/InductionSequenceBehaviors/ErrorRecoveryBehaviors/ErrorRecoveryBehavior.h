//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_ERRORRECOVERYBEHAVIOR_H
#define JULIA_ANDROID_ERRORRECOVERYBEHAVIOR_H


#include "../../SequenceBehavior.h"
#include "../../SequenceBehaviorEnums.h"

class ErrorRecoveryBehavior : SequenceBehavior{
    ErrorRecoveryBehavior(SequenceBehavior_e behaviorE){
        this->behaviour_ = behaviorE;
    }
};


#endif //JULIA_ANDROID_ERRORRECOVERYBEHAVIOR_H
