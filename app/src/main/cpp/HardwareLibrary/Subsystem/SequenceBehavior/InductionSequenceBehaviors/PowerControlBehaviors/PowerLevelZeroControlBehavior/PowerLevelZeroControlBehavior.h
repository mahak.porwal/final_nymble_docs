//
// Created by fedal on 28/12/20.
//

#ifndef JULIA_ANDROID_POWERLEVELZEROCONTROLBEHAVIOR_H
#define JULIA_ANDROID_POWERLEVELZEROCONTROLBEHAVIOR_H


#include "../PowerLevelControlBehavior.h"

class PowerLevelZeroControlBehavior : public PowerLevelControlBehavior {
private:
    void PowerDown();

public:
    PowerLevelZeroControlBehavior(SequenceBehavior_e behaviorE) :
            PowerLevelControlBehavior(behaviorE) {
        this->induction_control_pid_->SetTargetValue(0);
    }

    bool ExecuteSequence(ComponentRequest *sequenceRequest);
};


#endif //JULIA_ANDROID_POWERLEVELZEROCONTROLBEHAVIOR_H
