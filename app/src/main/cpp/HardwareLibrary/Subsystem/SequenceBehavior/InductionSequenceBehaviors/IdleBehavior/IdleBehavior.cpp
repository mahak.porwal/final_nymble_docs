//
// Created by fedal on 25/12/20.
//

#include "IdleBehavior.h"


bool IdleBehavior::ExecuteSequence(ComponentRequest *sequenceRequest,
                                   std::unordered_map<ResponseParameters_e, std::vector<std::string>> *result) {
    this->PollSensors();
    try {
        this->DetectErrors();
    }
    catch (Error e) {
        ComponentRequestBuilder *componentRequestBuilder = new ComponentRequestBuilder;
        componentRequestBuilder->component_request_.SetComponent(
                INDUCTION_ACTUATOR).SetSpeed(0);
        HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(componentRequestBuilder->component_request_);
        delete componentRequestBuilder;

        componentRequestBuilder = new ComponentRequestBuilder;
        componentRequestBuilder->component_request_.SetComponent(
                INDUCTION_INT_GPIO).SetState(OFF_STATE);
        HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                componentRequestBuilder->component_request_);
        delete componentRequestBuilder;
        throw e;
    }
    return true;
}
