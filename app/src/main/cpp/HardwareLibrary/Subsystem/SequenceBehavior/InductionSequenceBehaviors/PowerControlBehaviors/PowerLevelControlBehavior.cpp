//
// Created by fedal on 25/12/20.
//

#include "PowerLevelControlBehavior.h"


int PowerLevelControlBehavior::PollSensors() {
    float result = -1;
    ComponentRequestBuilder *componentRequestBuilder = new ComponentRequestBuilder;
    componentRequestBuilder->component_request_.SetRequestType(
            HARDWARE_REQUEST).SetComponent(INDUCTION_AC_VOLTAGE_SENSOR).SetFrequency(1);
    std::string sensor_value = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
            componentRequestBuilder->component_request_).at(SENSOR_VALUE).at(0);
    result = std::stof(sensor_value.c_str());
    this->induction_voltage_ = result;

    componentRequestBuilder->component_request_.SetRequestType(
            HARDWARE_REQUEST).SetComponent(INDUCTION_AC_CURRENT_SENSOR).SetFrequency(1);
    sensor_value = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
            componentRequestBuilder->component_request_).at(SENSOR_VALUE).at(0);
    result = std::stof(sensor_value.c_str());

    this->induction_current_ = result;

    componentRequestBuilder->component_request_.SetRequestType(
            HARDWARE_REQUEST).SetComponent(INDUCTION_IGBT_TEMPERATURE_SENSOR).SetFrequency(1);
    sensor_value = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
            componentRequestBuilder->component_request_).at(SENSOR_VALUE).at(0);
    result = std::stof(sensor_value.c_str());
    this->induction_igbt_temperature_ = result;

    componentRequestBuilder->component_request_.SetRequestType(
            HARDWARE_REQUEST).SetComponent(INDUCTION_PAN_TEMPERATURE_SENSOR).SetFrequency(1);
    sensor_value = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
            componentRequestBuilder->component_request_).at(SENSOR_VALUE).at(0);
    result = std::stof(sensor_value.c_str());

    this->induction_pan_temperature_ = result;

    __android_log_print(ANDROID_LOG_ERROR, "Sensors  : ",
                        "igbt temp %f \npan temp %f \ncurrent %f \n voltage %f",
                        this->induction_igbt_temperature_, this->induction_pan_temperature_,
                        this->induction_current_, this->induction_voltage_);

    return 1;
}

int PowerLevelControlBehavior::DetectErrors() {
    int result = -1;
    if (IsOutOfRange(this->induction_current_, this->induction_current_upper_threshold_,
                     this->induction_current_lower_threshold_)) {
        throw new Error;
    }
    if (IsOutOfRange(this->induction_voltage_, this->induction_voltage_upper_threshold_,
                     this->induction_voltage_lower_threshold_)) {
        throw new Error;

    }
    if (IsOutOfRange(this->induction_igbt_temperature_,
                     this->induction_igbt_temperature_upper_threshold_,
                     this->induction_igbt_temperature_lower_threshold_)) {
        throw new Error;
    }
    /*if (IsOutOfRange(this->induction_pan_temperature_,
                     this->induction_pan_temperature_upper_threshold_,
                     this->induction_pan_temperature_lower_threshold_)) {
        throw new Error;
    }*/
    return 0;
}

bool PowerLevelControlBehavior::IsOutOfRange(float value, float upper_value, float lower_value) {
    return (value >= upper_value || value <= lower_value);
}

bool PowerLevelControlBehavior::ExecuteSequence(ComponentRequest *sequenceRequest,
                                                std::unordered_map<ResponseParameters_e, std::vector<std::string>> *result) {
    float output = 0;
    this->PollSensors();
    try {
        this->DetectErrors();
    }
    catch (Error e) {
        ComponentRequestBuilder *componentRequestBuilder = new ComponentRequestBuilder;
        componentRequestBuilder->component_request_.SetComponent(
                INDUCTION_ACTUATOR).SetSpeed(0);

        HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                componentRequestBuilder->component_request_);
        delete componentRequestBuilder;

        componentRequestBuilder = new ComponentRequestBuilder;
        componentRequestBuilder->component_request_.SetComponent(
                INDUCTION_INT_GPIO).SetState(OFF_STATE);
        HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                componentRequestBuilder->component_request_);
        delete componentRequestBuilder;
        throw e;
    }

    this->induction_control_pid_->SetCurrentValue(
            this->induction_voltage_ * this->induction_current_);
    output = this->induction_control_pid_->PidLoop();
    if (output > 99) {
        output = 99;
    } else if (output < 0) {
        output = 0;
    }
    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "output %f", output);
    ComponentRequestBuilder *componentRequestBuilder = new ComponentRequestBuilder;
    componentRequestBuilder->component_request_.SetRequestType(
            HARDWARE_REQUEST).SetComponent(
            INDUCTION_ACTUATOR).SetSpeed((int) output);
    HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
            componentRequestBuilder->component_request_);
    delete componentRequestBuilder;
    this->induction_ping_control_->PingControl(this->induction_current_, output);
    return true;
}
