//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_POWERLEVELSEVENCONTROLBEHAVIOR_H
#define JULIA_ANDROID_POWERLEVELSEVENCONTROLBEHAVIOR_H


#include "../PowerLevelControlBehavior.h"
#include "../../../SequenceBehaviorEnums.h"

class PowerLevelSevenControlBehavior : public PowerLevelControlBehavior{
public:
    PowerLevelSevenControlBehavior(SequenceBehavior_e behaviorE) :
    PowerLevelControlBehavior(behaviorE) {
            this->induction_control_pid_->SetTargetValue(1400);
    }
};


#endif //JULIA_ANDROID_POWERLEVELSEVENCONTROLBEHAVIOR_H
