//
// Created by fedal on 29/12/20.
//

#include "MacroResetBehavior.h"

bool MacroResetBehavior::ExecuteSequence(
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> *result) {
    switch (this->reset_state_) {
        case SET_HOME: {
            ComponentRequestBuilder *componentRequestBuilder = new ComponentRequestBuilder;
            componentRequestBuilder->component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetComponent(component_request_.GetComponent()).SetPosition(
                    0);
            delete componentRequestBuilder;
            return true;
        }
            break;
        default:
            break;
    }
    return false;
}
