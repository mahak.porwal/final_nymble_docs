//
// Created by fedal on 18/12/20.
//

#ifndef JULIA_ANDROID_MACROLIQUIDDISPENSEBEHAVIOR_H
#define JULIA_ANDROID_MACROLIQUIDDISPENSEBEHAVIOR_H


#include "../../SequenceBehavior.h"
#include "MacroLiquidDispenseEnums.h"
#include "../../../../HardwareService/Timer/Timer.h"

class MacroLiquidDispenseBehavior : public SequenceBehavior {
private:
    MacroLiquidDispenseState_e dispense_state_;

    Timer *timer_;
    bool is_sequence_done_;
    uint8_t tries_ = 0;
public:
    MacroLiquidDispenseBehavior(ComponentRequest component_request) :
            SequenceBehavior(component_request) {
        this->dispense_state_ = TIMER_START;
        this->behavior_ = MACRO_LIQUID_DISPENSE;
        this->timer_ = new Timer;
    }

    void ResetBehavior();

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);

    ~MacroLiquidDispenseBehavior() {
        delete this->timer_;
    }
};


#endif //JULIA_ANDROID_MACROLIQUIDDISPENSEBEHAVIOR_H
