//
// Created by fedal on 24/12/20.
//

#ifndef JULIA_ANDROID_MACRONONLIQUIDDISPENSEBEHAVIOR_H
#define JULIA_ANDROID_MACRONONLIQUIDDISPENSEBEHAVIOR_H


#include "../../SequenceBehavior.h"
#include "MacroNonLiquidDispenseEnums.h"
#include "../../../../HardwareService/Timer/Timer.h"
#include "../../../../Component/Request/ComponentRequestBuilder.h"


class MacroNonLiquidDispenseBehavior : public SequenceBehavior {
private:
    MacroNonLiquidDispenseState_e dispense_state_;
    Timer *timer_;
    bool is_sequence_done_;
    uint8_t dispense_count;
public:
    MacroNonLiquidDispenseBehavior(ComponentRequest component_request) :
            SequenceBehavior(component_request) {
        this->dispense_state_ = NON_LIQUID_TIMER_START;
        this->behavior_ = MACRO_NON_LIQUID_DISPENSE;
        this->timer_ = new Timer;
        this->is_sequence_done_ = false;
        this->dispense_count = 0;
    }

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);

    void ResetBehavior();

    ~MacroNonLiquidDispenseBehavior() {
        delete this->timer_;
    }
};


#endif //JULIA_ANDROID_MACRONONLIQUIDDISPENSEBEHAVIOR_H
