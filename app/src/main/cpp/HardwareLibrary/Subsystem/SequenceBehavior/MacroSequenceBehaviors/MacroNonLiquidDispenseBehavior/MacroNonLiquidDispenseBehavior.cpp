//
// Created by fedal on 24/12/20.
//

#include "MacroNonLiquidDispenseBehavior.h"

bool
MacroNonLiquidDispenseBehavior::ExecuteSequence(
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> *result) {

    bool is_sequence_done = false;
    switch (this->dispense_state_) {
        case NON_LIQUID_TIMER_START: {
            this->timer_->SetBeginTime();
            this->dispense_state_ = NON_LIQUID_OPEN_BOX;
        }
            break;
        case NON_LIQUID_OPEN_BOX: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(HARDWARE_REQUEST).SetSpeed(
                    5).SetPosition(95);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = NON_LIQUID_WAIT_AFTER_OPENING;
                this->timer_->SetBeginTime();
            }
        }
            break;
        case NON_LIQUID_WAIT_AFTER_OPENING: {
            if (this->timer_->GetTimeElapsed() > 2000) {
                this->dispense_state_ = NON_LIQUID_PIVOT_POINT;
            }
        }
            break;
        case NON_LIQUID_PIVOT_POINT: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(
                    HARDWARE_REQUEST).SetSpeed(5).SetPosition(33);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = NON_LIQUID_DISPENSE_MACRO;
                this->timer_->SetBeginTime();
            }
        }
            break;
        case NON_LIQUID_DISPENSE_MACRO: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetComponent(
                    component_request_.GetComponent()).SetPosition(95);
            HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            this->dispense_state_ = NON_LIQUID_WAIT_AFTER_DISPENSE;
            this->timer_->SetBeginTime();
        }
            break;
        case NON_LIQUID_WAIT_AFTER_DISPENSE: {
            if (this->timer_->GetTimeElapsed() > 500) {
                this->dispense_state_ = NON_LIQUID_CHECK_TRIES;
            }
        }
            break;
        case NON_LIQUID_CHECK_TRIES: {
            this->dispense_count++;
            if (this->dispense_count >= component_request_.GetQuantity()) {
                this->dispense_count = 0;
                this->dispense_state_ = NON_LIQUID_CLOSE_SEQUENCE;
            } else {
                this->dispense_state_ = NON_LIQUID_PIVOT_POINT;
            }
        }
            break;
        case NON_LIQUID_CLOSE_SEQUENCE: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(
                    HARDWARE_REQUEST).SetSpeed(9).SetPosition(2);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = NON_LIQUID_TIMER_START;
                std::vector<string> values;
                values.push_back("1");
                (*result).emplace(SEQUENCE_DONE, values);
                return true;
            }
        }
            break;
    }
    return is_sequence_done;
}

void MacroNonLiquidDispenseBehavior::ResetBehavior() {
    this->dispense_state_ = NON_LIQUID_TIMER_START;
    this->is_sequence_done_ = false;
    this->dispense_count = 0;
}
