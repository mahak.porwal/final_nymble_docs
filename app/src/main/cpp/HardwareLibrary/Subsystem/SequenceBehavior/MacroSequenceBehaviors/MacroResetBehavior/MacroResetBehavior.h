//
// Created by fedal on 29/12/20.
//

#ifndef JULIA_ANDROID_MACRORESETBEHAVIOR_H
#define JULIA_ANDROID_MACRORESETBEHAVIOR_H


#include "MacroResetBehaviorEnums.h"

class MacroResetBehavior : public SequenceBehavior {
private:
    MacroResetStates_e reset_state_;
    Timer *timer_;
    bool is_sequence_done_;
    uint8_t dispense_count;
public:
    MacroResetBehavior(ComponentRequest component_request) :
            SequenceBehavior(component_request) {
        this->reset_state_ = SET_HOME;
        this->behavior_ = MACRO_RESET_CONTAINER;
        this->timer_ = new Timer;
        this->is_sequence_done_ = false;
        this->dispense_count = 0;
    }

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);

    ~MacroResetBehavior() {
        delete this->timer_;
    }
};


#endif //JULIA_ANDROID_MACRORESETBEHAVIOR_H
