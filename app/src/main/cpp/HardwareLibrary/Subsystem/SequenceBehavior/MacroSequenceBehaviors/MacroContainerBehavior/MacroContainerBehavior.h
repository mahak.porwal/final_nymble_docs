//
// Created by fedal on 22/7/21.
//

#ifndef RECIPEENGINE_MACROCONTAINERBEHAVIOR_H
#define RECIPEENGINE_MACROCONTAINERBEHAVIOR_H


class MacroContainerBehavior : public SequenceBehavior {
public:
    MacroContainerBehavior(ComponentRequest component_request) :
            SequenceBehavior(component_request) {
        this->behavior_ = MACRO_REACH_POSITION_AT_SPEED;
    }

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);

    ~MacroContainerBehavior() {}
};


#endif //RECIPEENGINE_MACROCONTAINERBEHAVIOR_H
