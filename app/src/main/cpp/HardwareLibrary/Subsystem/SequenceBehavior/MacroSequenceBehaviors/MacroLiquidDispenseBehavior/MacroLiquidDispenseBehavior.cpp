//
// Created by fedal on 18/12/20.
//

#include "MacroLiquidDispenseBehavior.h"
#include "../../../../HardwareServiceFactory/HardwareServiceFactory.h"
#include <vector>
#include <unordered_map>

bool MacroLiquidDispenseBehavior::ExecuteSequence(
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> *result) {
    bool is_sequence_done = false;
    switch (this->dispense_state_) {
        case TIMER_START: {
            this->timer_->SetBeginTime();
            this->dispense_state_ = REACH_30_DEGREE;
        }
            break;
        case REACH_30_DEGREE: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(HARDWARE_REQUEST).SetSpeed(
                    5).SetPosition(30);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = WAIT_AFTER_OPENING_30_DEGREE;
                this->timer_->SetBeginTime();
            }
        }
            break;
        case WAIT_AFTER_OPENING_30_DEGREE: {
            if (this->timer_->GetTimeElapsed() > 2000) {
                this->dispense_state_ = REACH_45_DEGREE;
            }
        }
            break;
        case REACH_45_DEGREE: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(HARDWARE_REQUEST).SetSpeed(
                    5).SetPosition(45);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = WAIT_AFTER_OPENING_45_DEGREE;
                this->timer_->SetBeginTime();
            }
        }
            break;
        case WAIT_AFTER_OPENING_45_DEGREE: {
            if (this->timer_->GetTimeElapsed() > 2000) {
                this->dispense_state_ = REACH_60_DEGREE;
            }
        }
            break;
        case REACH_60_DEGREE: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(HARDWARE_REQUEST).SetSpeed(
                    5).SetPosition(60);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = WAIT_AFTER_OPENING_60_DEGREE;
                this->timer_->SetBeginTime();
            }
        }
            break;
        case WAIT_AFTER_OPENING_60_DEGREE: {
            if (this->timer_->GetTimeElapsed() > 2000) {
                this->dispense_state_ = REACH_75_DEGREE;
            }
        }
            break;
        case REACH_75_DEGREE: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(HARDWARE_REQUEST).SetSpeed(
                    5).SetPosition(75);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = WAIT_AFTER_OPENING_75_DEGREE;
                this->timer_->SetBeginTime();
            }
        }
            break;
        case WAIT_AFTER_OPENING_75_DEGREE: {
            if (this->timer_->GetTimeElapsed() > 2000) {
                this->dispense_state_ = PIVOT_POINT;
            }
        }
            break;
        case PIVOT_POINT: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(HARDWARE_REQUEST).SetSpeed(
                    5).SetPosition(33);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = DISPENSE_MACRO;
                this->timer_->SetBeginTime();
            }
        }
            break;
        case DISPENSE_MACRO: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(HARDWARE_REQUEST).SetSpeed(
                    10).SetPosition(100);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = WAIT_AFTER_DISPENSE;
                this->timer_->SetBeginTime();
            }
        }
            break;
        case WAIT_AFTER_DISPENSE: {
            if (this->timer_->GetTimeElapsed() > 5000) {
                this->dispense_state_ = CHECK_TRIES;
            }
        }
            break;
        case CHECK_TRIES: {
            this->tries_++;
            if (this->tries_ >= component_request_.GetQuantity()) {
                this->dispense_state_ = CLOSE_SEQUENCE;
            } else {
                this->dispense_state_ = PIVOT_POINT;
            }
        }
            break;
        case CLOSE_SEQUENCE: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    component_request_.GetComponent()).SetRequestType(HARDWARE_REQUEST).SetSpeed(
                    5).SetPosition(2);
            auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));
            if (sequence_result > 0) {
                this->dispense_state_ = TIMER_START;
                this->tries_ = 0;
                std::vector<string> values;
                values.push_back("1");
                (*result).emplace(SEQUENCE_DONE, values);
                return true;
            }
        }
            break;
    }
    return is_sequence_done;
}

void MacroLiquidDispenseBehavior::ResetBehavior() {
    this->dispense_state_ = TIMER_START;
    this->tries_ = 0;
}
