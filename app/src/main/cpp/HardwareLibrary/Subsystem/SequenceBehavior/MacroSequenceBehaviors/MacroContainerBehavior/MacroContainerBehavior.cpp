//
// Created by fedal on 22/7/21.
//

#include "MacroContainerBehavior.h"

bool MacroContainerBehavior::ExecuteSequence(
        std::unordered_map <ResponseParameters_e, std::vector<std::string>> *result) {
    ComponentRequestBuilder component_request_builder;
    component_request_builder.component_request_.SetComponent(
            component_request_.GetComponent())
            .SetRequestType(HARDWARE_REQUEST)
            .SetSpeed(component_request_.GetSpeed())
            .SetPosition(component_request_.GetPosition());

    auto sequence_result_map = HardwareServiceFactory::GetHardwareService()->
            ProcessComponentRequest(component_request_builder.component_request_);

    int sequence_result = stoi(sequence_result_map.at(SEQUENCE_RESULT).at(0));

    if (sequence_result > 0) {
        return true;
    }
    return false;
}
