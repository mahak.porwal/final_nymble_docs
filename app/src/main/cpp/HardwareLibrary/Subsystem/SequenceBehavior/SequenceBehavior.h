//
// Created by fedal on 8/12/20.
//

#ifndef JULIA_ANDROID_SEQUENCEBEHAVIOR_H
#define JULIA_ANDROID_SEQUENCEBEHAVIOR_H

#include "../../Error/Error.h"


class SequenceBehavior {
protected:
    SequenceBehavior_e behavior_;
    ComponentRequest component_request_;
public:
    SequenceBehavior(ComponentRequest component_request) {
        this->component_request_ = component_request;
    }

    virtual bool
    ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *) = 0;

    virtual void ResetBehavior();

    SequenceBehavior_e GetSequenceBehavior() const { return this->behavior_; }

    ComponentRequest *GetComponentRequest() {
        return &this->component_request_;
    }

    virtual ~SequenceBehavior() {}
};

#endif //JULIA_ANDROID_SEQUENCEBEHAVIOR_H
