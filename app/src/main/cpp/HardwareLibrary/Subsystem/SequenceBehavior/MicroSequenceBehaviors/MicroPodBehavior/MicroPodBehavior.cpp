//
// Created by fedal on 22/7/21.
//

#include "MicroPodBehavior.h"
#include "../../../../Component/Request/ComponentRequestEnum.h"

bool MicroPodBehavior::ExecuteSequence(
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> *result) {

    switch (this->micro_pod_state_) {
        case SET_SERVO_SEQUENCE: {
            Component_e component = component_request_.GetComponent();
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetComponent(
                    component).SetDirection(component_request_.GetDirection()).SetSpeed(
                    component_request_.GetSpeed());
            *result = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    component_request_builder.component_request_);
            this->micro_pod_state_ = OBSERVE_FEEDBACK;
        }
            break;
        case OBSERVE_FEEDBACK: {
            Component_e feedback_component = MICRO_SERVO_1_FEEDBACK;
            switch (component_request_.GetComponent()) {
                case MICRO_SERVO_1: {
                    feedback_component = MICRO_SERVO_1_FEEDBACK;
                }
                    break;
                case MICRO_SERVO_2: {
                    feedback_component = MICRO_SERVO_2_FEEDBACK;
                }
                    break;
                case MICRO_SERVO_3: {

                    feedback_component = MICRO_SERVO_3_FEEDBACK;
                }
                    break;
                case MICRO_SERVO_4: {

                    feedback_component = MICRO_SERVO_4_FEEDBACK;
                }
                    break;
                case MICRO_SERVO_5: {

                    feedback_component = MICRO_SERVO_3_FEEDBACK;
                }
                    break;
                case MICRO_SERVO_6: {

                    feedback_component = MICRO_SERVO_6_FEEDBACK;
                }
                    break;
                default:
                    break;
            }
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetComponent(
                    feedback_component).SetSensorUnit(ANGLE_VALUE);
            std::string sensor_value = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
            double pot_reading = std::stod(sensor_value.c_str());/*
            double last_reading = 0.0;
            while (1){
                ComponentRequestBuilder component_request_builder;
                component_request_builder.component_request_.SetRequestType(
                        HARDWARE_REQUEST).SetComponent(
                        feedback_component);
                std::string sensor_value = HardwareServiceFactory::GetHardwareService()->
                        ProcessComponentRequest(
                        component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
                double pot_reading = std::stod(sensor_value.c_str());
                if(abs(pot_reading - last_reading) <= 1){
                    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                    "raw reading %f",
                                    pot_reading);
                }
                last_reading = pot_reading;
            }*/

            Set_PotValue(pot_reading);

            if (component_request_.GetDirection() == DIRECTION_CLOCKWISE) {
                Potentiometer_Process_C(&micro_dispense_potentiometer);
            } else {
                Potentiometer_Process_A(&micro_dispense_potentiometer);
            }

            micro_dispense_potentiometer.filtered_potentiometer_value = (uint32_t) pot_reading;


            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                "pod reading %f",
                                pot_reading);

            double target_position = (double) component_request_.GetPosition();
            if (target_position > 360 || target_position < 0) {
                target_position = 10;
            }

            /*if (pot_reading >
                    component_request_.GetPosition()-10 &&
                    pot_reading <
                        component_request_.GetPosition()+10) {*/
            if (pot_reading >
                target_position - 1 &&
                pot_reading <
                target_position + 1) {

                ComponentRequestBuilder component_request_builder;
                Component_e component = component_request_.GetComponent();
                component_request_builder.component_request_.SetRequestType(
                        HARDWARE_REQUEST).SetComponent(
                        component).SetDirection(
                        DIRECTION_CLOCKWISE).SetSpeed(0);
                *result = HardwareServiceFactory::GetHardwareService()->
                        ProcessComponentRequest(
                        component_request_builder.component_request_);
                return true;
            }
        }
            break;
    }
    return false;
}