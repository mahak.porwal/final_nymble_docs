//
// Created by fedal on 22/12/20.
//

#include "MicroAntiClockwiseDispenseBehavior.h"


bool MicroAntiClockwiseDispenseBehavior::ExecuteSequence(
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> *result) {
    switch (this->micro_dispense_state_) {
        case TURN_OFF_FAN: {
            //Fan_Control(EXHAUST_FAN,0);
            timer->SetBeginTime();//HAL_Delay(4000);
            this->micro_dispense_state_ = WAIT_FOR_FAN;
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetComponent(
                    LM2678_CONTROL).SetState(ON_STATE);
            *result = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    component_request_builder.component_request_);
            //__android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Timer set %d", result);
        }
            break;
        case WAIT_FOR_FAN: {
            if (timer->GetTimeElapsed() > 4) {
                this->micro_dispense_state_ = SET_SERVO;
            }
        }
            break;
        case SET_SERVO: {
            /**
          * Set servo for fast motion
          */
            Component_e component = component_request_.GetComponent();
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetComponent(
                    component).SetDirection(DIRECTION_ANTICLOCKWISE).SetSpeed(5);
            *result = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    component_request_builder.component_request_);
            //__android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "set servo result %d", result);
            //setPcaDuty(START_MICRO_FAST_A, SERVO_POD_1 + 4 * podnum);
            this->micro_dispense_state_ = OBSERVE_SERVO_FB;
            //setMicro_Mux(VIB_MOTOR_1 + podnum);
            this->timer->SetBeginTime();
            //HAL_Delay(10);

        }
            break;
        case OBSERVE_SERVO_FB: {
            if (timer->GetTimeElapsed() > 10) {
                /**
                 * ANTI-CLOCKWISE SERVO MOVEMENT
                 */

                Component_e component = component_request_.GetComponent();
                ComponentRequestBuilder component_request_builder;
                component_request_builder.component_request_.SetRequestType(
                        HARDWARE_REQUEST).SetComponent(
                        component).SetFrequency(1).SetSensorUnit(ANGLE_VALUE);
                std::string sensor_value = HardwareServiceFactory::GetHardwareService()->
                        ProcessComponentRequest(
                        component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
                float pot_reading = std::stof(sensor_value.c_str());
                //__android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "observe feedback response %f",pot_reading);


                Set_PotValue(pot_reading);

                Potentiometer_Process_A(&micro_dispense_potentiometer);
                micro_dispense_potentiometer.filtered_potentiometer_value = (uint32_t) pot_reading;
                //__android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "observe feedback response %d",micro_dispense_potentiometer.filtered_potentiometer_value);
                switch (this->acc_state_) {
                    case FAST_ACC:
                        if (micro_dispense_potentiometer.filtered_potentiometer_value >
                            SERVO_FULL_ACC_POS_A - 10 &&
                            micro_dispense_potentiometer.filtered_potentiometer_value <
                            SERVO_FULL_ACC_POS_A + 10) {
                            /**
                             * Stop servo
                             */
                            component = component_request_.GetComponent();
                            ComponentRequestBuilder component_request_builder;
                            component_request_builder.component_request_.SetRequestType(
                                    HARDWARE_REQUEST).SetComponent(
                                    component).SetDirection(
                                    DIRECTION_ANTICLOCKWISE).SetSpeed(0);
                            *result = HardwareServiceFactory::GetHardwareService()->
                                    ProcessComponentRequest(
                                    component_request_builder.component_request_);
                            //usleep(5000000);
                            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                                "stop after fast acc %d",
                                                micro_dispense_potentiometer.filtered_potentiometer_value);
                            /**
                             * Set servo for slow motion dispense
                             */
                            component_request_builder.component_request_.SetComponent(
                                    component).SetDirection(
                                    DIRECTION_ANTICLOCKWISE).SetSpeed(2);
                            HardwareServiceFactory::GetHardwareService()->
                                    ProcessComponentRequest(
                                    component_request_builder.component_request_);
                            //setPcaDuty(START_MICRO_SLOW_A, SERVO_POD_1 + 4 * podnum);
                            acc_state_ = DISPENSE_ACC;

                        }
                        break;
                    case DISPENSE_ACC:

                        if (micro_dispense_potentiometer.filtered_potentiometer_value >
                            SERVO_HOME_POS_A - 10 &&
                            micro_dispense_potentiometer.filtered_potentiometer_value <
                            SERVO_HOME_POS_A + 10) {
                            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                                "observe dispense feedback");
                            /**
                             * Stop servo
                             */
                            Component_e component = component_request_.GetComponent();
                            ComponentRequestBuilder component_request_builder;
                            component_request_builder.component_request_.SetRequestType(
                                    HARDWARE_REQUEST).SetComponent(
                                    component).SetDirection(
                                    DIRECTION_ANTICLOCKWISE).SetSpeed(0);
                            *result = HardwareServiceFactory::GetHardwareService()->
                                    ProcessComponentRequest(
                                    component_request_builder.component_request_);
                            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                                "stop after dispense acc %d",
                                                micro_dispense_potentiometer.filtered_potentiometer_value);
                            //usleep(5000000);
                            //setPcaDuty(STOP_MICRO, SERVO_POD_1 + 4 * podnum);
                            //micro_dispense_state = READ_WEIGHT;//CHECK_WEIGHT;
                            this->micro_dispense_state_ = CHECK_REV;
                            this->acc_state_ = FAST_ACC;
                            this->revolutions++;
                            //timer_ = HAL_GetTick();
                        }
                        break;
                }
            }
        }
            break;
        case CHECK_REV:
            if (this->revolutions >= component_request_.GetQuantity()) {
                this->micro_dispense_state_ = CLOSE_SEQ;
            } else {
                this->micro_dispense_state_ = SET_SERVO;
            }
            break;
        case CLOSE_SEQ:
            this->micro_dispense_state_ = TURN_OFF_FAN;
            this->revolutions = 0;
            resetPot();
            return true;
            //Fan_Control(EXHAUST_FAN, 5600);
            break;
        default:
            break;
    }
    return false;
}
