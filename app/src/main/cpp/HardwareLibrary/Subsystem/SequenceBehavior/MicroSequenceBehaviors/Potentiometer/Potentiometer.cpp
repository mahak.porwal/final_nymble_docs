/*
 * potentiometer.c
 *
 *  Created on: Jun 8, 2019
 *      Author: Shubham
 */

#include "Potentiometer.h"


potentiometer_filter_configuration_s micro_dispense_potentiometer = {
        .previous_raw_potentiometer_value = 0,
        .filtered_potentiometer_value = 0,
        .raw_potentiometer_value = 0,
        .temporary_potentiometer_value = 0,
        .deadzone_potentiometer_count_1 = 0,
        .deadzone_potentiometer_count_2 = 0,
        .direction = true
};
uint16_t pot_value;

void Potentiometer_Process_A(potentiometer_filter_configuration_s *potentiometer) {

    static int potentiometer_state = ENTER_DEAD_ZONE_AREA;//Travel only in an unidirectional direction


    //potentiometer->temporary_potentiometer_value = Get_PotValue();
    potentiometer->raw_potentiometer_value = Get_PotValue();

    if (((potentiometer->previous_raw_potentiometer_value -
          potentiometer->raw_potentiometer_value)) < 2 ||
        ((potentiometer->previous_raw_potentiometer_value -
          potentiometer->raw_potentiometer_value)) > -2) {
        potentiometer->temporary_potentiometer_value = potentiometer->raw_potentiometer_value;

    }
    potentiometer->previous_raw_potentiometer_value = potentiometer->raw_potentiometer_value;


    switch (potentiometer_state) {
        case ENTER_DEAD_ZONE_AREA: {
            potentiometer->filtered_potentiometer_value = potentiometer->temporary_potentiometer_value;

            if ((potentiometer->temporary_potentiometer_value >
                 POTENTIOMETER_ENTRY_BOUNDARY_VALUE_L_A)
                && (potentiometer->temporary_potentiometer_value <
                    POTENTIOMETER_ENTRY_BOUNDARY_VALUE_U_A)) {

                potentiometer->deadzone_potentiometer_count_1 =
                        potentiometer->deadzone_potentiometer_count_1 + 1;

                if (potentiometer->deadzone_potentiometer_count_1 >
                    POTENTIOMETER_ENTRY_BOUNDARY_VALUE_COUNT) {
                    //log_printf("\r\nTRUE enter :%d\n",potentiometer->deadzone_potentiometer_count_1);
                    potentiometer_state = EXIT_DEAD_ZONE_AREA;
                    potentiometer->filtered_potentiometer_value = 4010;
                    potentiometer->deadzone_potentiometer_count_1 = 0;
                }
            } else {
                if (potentiometer->deadzone_potentiometer_count_1 != 0) {
                    //log_printf("\r\nenter :%d\n",potentiometer->deadzone_potentiometer_count_1);
                }
                potentiometer->deadzone_potentiometer_count_1 = 0;
                //potentiometer->deadzone_potentiometer_count_2 = 0 ;
            }
        }
            break;
        case EXIT_DEAD_ZONE_AREA: {

            if ((potentiometer->temporary_potentiometer_value >=
                 POTENTIOMETER_EXIT_BOUNDARY_VALUE_L_A) &&
                (potentiometer->temporary_potentiometer_value <=
                 POTENTIOMETER_EXIT_BOUNDARY_VALUE_U_A)) {

                potentiometer->deadzone_potentiometer_count_1 =
                        potentiometer->deadzone_potentiometer_count_1 + 1;

                if (potentiometer->deadzone_potentiometer_count_1 >
                    POTENTIOMETER_EXIT_BOUNDARY_VALUE_COUNT) //30
                {
                    //log_printf("\r\nTRUE exit :%d\n",potentiometer->deadzone_potentiometer_count_1);
                    ////log_printf("\r\n 3 %lu %d\n", potentiometer->temporary_potentiometer_value,potentiometer->deadzone_potentiometer_count_2) ;
                    potentiometer->deadzone_potentiometer_count_1 = 0;
                    potentiometer_state = ENTER_DEAD_ZONE_AREA;
                    /*HAL_Delay(100);
                    setPcaDuty(STOP_MICRO,CHANNEL_2);*/
//					/potentiometer->deadzone_potentiometer_count_1 = 0 ;
                    ////log_printf("\r\n 2d %lu\n", adc_value) ;
                    //log_printf("\r\n 3\n");
                }

            } else {
                if (potentiometer->deadzone_potentiometer_count_1 != 0) {
                    //log_printf("\r\nexit :%d\n",potentiometer->deadzone_potentiometer_count_1);
                }
                potentiometer->deadzone_potentiometer_count_1 = 0;
                //potentiometer->deadzone_potentiometer_count_2 = 0 ;
            }
        }
            break;
        default:
            break;
    }

}

void Potentiometer_Process_C(potentiometer_filter_configuration_s *potentiometer) {

    static int potentiometer_state = ENTER_DEAD_ZONE_AREA;//Travel only in an unidirectional direction


    //potentiometer->temporary_potentiometer_value = Get_PotValue();
    potentiometer->raw_potentiometer_value = Get_PotValue();

    if (((potentiometer->previous_raw_potentiometer_value -
          potentiometer->raw_potentiometer_value)) < 2 ||
        ((potentiometer->previous_raw_potentiometer_value -
          potentiometer->raw_potentiometer_value)) > -2) {
        potentiometer->temporary_potentiometer_value = potentiometer->raw_potentiometer_value;

    }
    potentiometer->previous_raw_potentiometer_value = potentiometer->raw_potentiometer_value;


    switch (potentiometer_state) {
        case ENTER_DEAD_ZONE_AREA: {
            potentiometer->filtered_potentiometer_value = potentiometer->temporary_potentiometer_value;

            if ((potentiometer->temporary_potentiometer_value >
                 POTENTIOMETER_ENTRY_BOUNDARY_VALUE_L_C)
                && (potentiometer->temporary_potentiometer_value <
                    POTENTIOMETER_ENTRY_BOUNDARY_VALUE_U_C)) {

                potentiometer->deadzone_potentiometer_count_1 =
                        potentiometer->deadzone_potentiometer_count_1 + 1;

                if (potentiometer->deadzone_potentiometer_count_1 >
                    POTENTIOMETER_ENTRY_BOUNDARY_VALUE_COUNT) {
                    //log_printf("\r\nTRUE enter :%d\n",potentiometer->deadzone_potentiometer_count_1);
                    potentiometer_state = EXIT_DEAD_ZONE_AREA;
                    potentiometer->filtered_potentiometer_value = 4010;
                    potentiometer->deadzone_potentiometer_count_1 = 0;
                }
            } else {
                if (potentiometer->deadzone_potentiometer_count_1 != 0) {
                    //log_printf("\r\nenter :%d\n",potentiometer->deadzone_potentiometer_count_1);
                }
                potentiometer->deadzone_potentiometer_count_1 = 0;
                //potentiometer->deadzone_potentiometer_count_2 = 0 ;
            }
        }
            break;
        case EXIT_DEAD_ZONE_AREA: {

            if ((potentiometer->temporary_potentiometer_value >=
                 POTENTIOMETER_EXIT_BOUNDARY_VALUE_L_C) &&
                (potentiometer->temporary_potentiometer_value <=
                 POTENTIOMETER_EXIT_BOUNDARY_VALUE_U_C)) {

                potentiometer->deadzone_potentiometer_count_1 =
                        potentiometer->deadzone_potentiometer_count_1 + 1;

                if (potentiometer->deadzone_potentiometer_count_1 >
                    POTENTIOMETER_EXIT_BOUNDARY_VALUE_COUNT) //30
                {
                    //log_printf("\r\nTRUE exit :%d\n",potentiometer->deadzone_potentiometer_count_1);
                    ////log_printf("\r\n 3 %lu %d\n", potentiometer->temporary_potentiometer_value,potentiometer->deadzone_potentiometer_count_2) ;
                    potentiometer->deadzone_potentiometer_count_1 = 0;
                    potentiometer_state = ENTER_DEAD_ZONE_AREA;
                    /*HAL_Delay(100);
                    setPcaDuty(STOP_MICRO,CHANNEL_2);*/
//					/potentiometer->deadzone_potentiometer_count_1 = 0 ;
                    ////log_printf("\r\n 2d %lu\n", adc_value) ;
                    //log_printf("\r\n 3\n");
                }

            } else {
                if (potentiometer->deadzone_potentiometer_count_1 != 0) {
                    //log_printf("\r\nexit :%d\n",potentiometer->deadzone_potentiometer_count_1);
                }
                potentiometer->deadzone_potentiometer_count_1 = 0;
                //potentiometer->deadzone_potentiometer_count_2 = 0 ;
            }
        }
            break;
        default:
            break;

    }

}

void resetPot(void) {
    micro_dispense_potentiometer.deadzone_potentiometer_count_1 = 0;
    micro_dispense_potentiometer.deadzone_potentiometer_count_2 = 0;
    micro_dispense_potentiometer.direction = false;
    micro_dispense_potentiometer.filtered_potentiometer_value = 0;
    micro_dispense_potentiometer.previous_raw_potentiometer_value = 0;
    micro_dispense_potentiometer.raw_potentiometer_value = 0;
    micro_dispense_potentiometer.temporary_potentiometer_value = 0;
}

uint32_t ADC_Value_Frequency(uint32_t raw_value) {
    static uint32_t temp_adc_count = 0, adc_count = 0;

    if (raw_value > 3500 && raw_value <= 3700)
        //if((raw_value>4000))
        //||(raw_value>0&&raw_value<=10))
    {
        temp_adc_count = temp_adc_count + 1;

    } else {
        if (temp_adc_count > 0) {
            //log_printf("\r\n%lu\n", temp_adc_count) ;
            adc_count = temp_adc_count;
            temp_adc_count = 0;
            return adc_count;
        }
    }
    return 0;
}

uint16_t Get_PotValue() {
    //return adc2_value[INDEX_MICRO_ADC];
    return 0;
}
void Set_PotValue(float val){
    pot_value = (uint16_t) val;
}
