//
// Created by fedal on 22/12/20.
//

#ifndef JULIA_ANDROID_MICROANTICLOCKWISEDISPENSEBEHAVIOR_H
#define JULIA_ANDROID_MICROANTICLOCKWISEDISPENSEBEHAVIOR_H


#include "../../SequenceBehavior.h"
#include "../Potentiometer/Potentiometer.h"
#include "../MicroDispenseEnums.h"


class MicroAntiClockwiseDispenseBehavior : public SequenceBehavior {
private:
    MicroDispenseState_e micro_dispense_state_ = TURN_OFF_FAN;
    AccelerationStates_e acc_state_ = FAST_ACC;
    uint8_t revolutions = 0;
    uint16_t filter_value = 0;
    Timer *timer = nullptr;
public:
    MicroAntiClockwiseDispenseBehavior(ComponentRequest component_request) :
            SequenceBehavior(component_request) {
        this->behavior_ = MICRO_ANTICLOCKWISE_DISPENSE;
        this->timer = new Timer;
    }

    ~MicroAntiClockwiseDispenseBehavior() {
        delete this->timer;
    }

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);
};


#endif //JULIA_ANDROID_MICROANTICLOCKWISEDISPENSEBEHAVIOR_H
