//
// Created by fedal on 12/4/21.
//

#ifndef JULIA_ANDROID_MICRODISPENSEENUMS_H
#define JULIA_ANDROID_MICRODISPENSEENUMS_H

#define SERVO_HOME_POS_A 512//2048/4
#define SERVO_FULL_ACC_POS_A 750//3000/4
#define SERVO_HOME_POS_C 2048/4
#define SERVO_FULL_ACC_POS_C 1000/4
typedef enum {
/*@{*/
    DEFAULT_MICRO_STATE = 0,
    SET_SERVO = 1,
    OBSERVE_SERVO_FB = 2,
    CHECK_REV = 3,
    CLOSE_SEQ = 4,
    TURN_OFF_FAN = 5,
    WAIT_FOR_FAN = 6,
    CHECK_WEIGHT = 7,
    READ_WEIGHT = 8
/*@}*/
} MicroDispenseState_e;

typedef enum {
    FAST_ACC,
    DISPENSE_ACC
} AccelerationStates_e;
#endif //JULIA_ANDROID_MICRODISPENSEENUMS_H
