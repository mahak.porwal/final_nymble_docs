/*
 * potentiometer.h
 *
 *  Created on: Jun 8, 2019
 *      Author: Shubham
 */

#ifndef COMPONENT_SENSOR_POTENTIOMETER_POTENTIOMETER_H_
#define COMPONENT_SENSOR_POTENTIOMETER_POTENTIOMETER_H_

#include <stdbool.h>


extern volatile uint32_t adc_value ;

typedef struct
{
	volatile uint32_t raw_potentiometer_value ;
	volatile uint32_t previous_raw_potentiometer_value ;
	volatile uint32_t filtered_potentiometer_value ;
	volatile uint32_t temporary_potentiometer_value  ;
	volatile uint32_t deadzone_potentiometer_count_1  ;
	volatile uint32_t deadzone_potentiometer_count_2  ;
	volatile bool direction;
}potentiometer_filter_configuration_s;

extern potentiometer_filter_configuration_s micro_dispense_potentiometer ;





#define POTENTIOMETER_ENTRY_BOUNDARY_VALUE_L_A  200
#define POTENTIOMETER_ENTRY_BOUNDARY_VALUE_U_A  1000

#define POTENTIOMETER_EXIT_BOUNDARY_VALUE_L_A  3200/4//150
#define POTENTIOMETER_EXIT_BOUNDARY_VALUE_U_A  3900/4//200


#define POTENTIOMETER_ENTRY_BOUNDARY_VALUE_L_C  3000/4
#define POTENTIOMETER_ENTRY_BOUNDARY_VALUE_U_C  3800/4

#define POTENTIOMETER_EXIT_BOUNDARY_VALUE_L_C  0//150
#define POTENTIOMETER_EXIT_BOUNDARY_VALUE_U_C  800/2//200


#define POTENTIOMETER_ENTRY_BOUNDARY_VALUE_COUNT	10000//80000//100000
#define POTENTIOMETER_EXIT_BOUNDARY_VALUE_COUNT		10000

enum potentiometer_sequence_state
{
	DEFAULT_DEAD_ZONE_AREA = 0 ,
	ENTER_DEAD_ZONE_AREA = 1,
	EXIT_DEAD_ZONE_AREA_CLOCKWISE = 2 ,
	ENTER_DEAD_ZONE_AREA_ANTICLOCKWISE = 3,
	EXIT_DEAD_ZONE_AREA_ANTICLOCKWISE = 4,
	EXIT_DEAD_ZONE_AREA = 5
}potentiometer_process_sequence_e;


typedef enum parameters_for_potentiometer
{
	REVOLUTION_COMPLETE_VALUE = 4090,
	CORRECT_VALUE_PERIOD_MILLISECOND = 500,
	UPPER_LIMIT_REVOLUTIONS = 32
}parameters_for_potentiometer_e;


void Potentiometer_Process_A(potentiometer_filter_configuration_s*);
void Potentiometer_Process_C(potentiometer_filter_configuration_s*);
void resetPot(void);
uint16_t Get_PotValue(void);
void Set_PotValue(float);
void Pot_Test(void);
void Pot_Filtered_Freq(void);
#endif /* COMPONENT_SENSOR_POTENTIOMETER_POTENTIOMETER_H_ */
