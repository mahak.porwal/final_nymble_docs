//
// Created by fedal on 22/12/20.
//

#ifndef JULIA_ANDROID_MICROCLOCKWISEDISPENSEBEHAVIOR_H
#define JULIA_ANDROID_MICROCLOCKWISEDISPENSEBEHAVIOR_H


#include "../MicroDispenseEnums.h"
#include "../Potentiometer/Potentiometer.h"

class MicroClockwiseDispenseBehavior : public SequenceBehavior {
private:
    MicroDispenseState_e micro_dispense_state_ = TURN_OFF_FAN;
    AccelerationStates_e acc_state_ = FAST_ACC;
    uint8_t revolutions = 0;
    uint16_t filter_value = 0;
    Timer *timer = nullptr;
public:
    MicroClockwiseDispenseBehavior(ComponentRequest component_request) :
            SequenceBehavior(component_request) {
        this->behavior_ = MICRO_CLOCKWISE_DISPENSE;
        this->timer = new Timer;
    }

    ~MicroClockwiseDispenseBehavior() {
        delete this->timer;
    }

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);
};


#endif //JULIA_ANDROID_MICROCLOCKWISEDISPENSEBEHAVIOR_H
