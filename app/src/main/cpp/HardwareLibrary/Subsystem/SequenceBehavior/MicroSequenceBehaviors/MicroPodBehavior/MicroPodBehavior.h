//
// Created by fedal on 22/7/21.
//

#ifndef RECIPEENGINE_MICROPODBEHAVIOR_H
#define RECIPEENGINE_MICROPODBEHAVIOR_H

#include"MicroPodBehaviorEnums.h"
#include "../Potentiometer/Potentiometer.h"

class MicroPodBehavior : public SequenceBehavior {
private:
    MicroPodBehaviorState_e micro_pod_state_;

public:
    MicroPodBehavior(ComponentRequest component_request) : SequenceBehavior(component_request) {
        this->micro_pod_state_ = SET_SERVO_SEQUENCE;
        this->behavior_ = MICRO_REACH_ANGLE_WITH_SPEED;
    }

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);

    ~MicroPodBehavior() {}
};


#endif //RECIPEENGINE_MICROPODBEHAVIOR_H
