//
// Created by fedal on 24/12/20.
//

#ifndef JULIA_ANDROID_STIRRERTIMEBASEDSEQUENCE_H
#define JULIA_ANDROID_STIRRERTIMEBASEDSEQUENCE_H


#include "../../SequenceBehavior.h"
#include "../../../../HardwareService/Timer/Timer.h"
#include "StirrerTimeBasedSequenceEnums.h"

class StirrerTimeBasedSequence : public SequenceBehavior {
private:
    Timer *timer_;
    StirrerTimeBasedSequenceStates_e state_;
public:
    StirrerTimeBasedSequence(ComponentRequest component_request) :
            SequenceBehavior(component_request) {
        this->behavior_ = STIRRER_TIME_BASED_SEQUENCE;
        this->timer_ = new Timer;
        this->state_ = START_TIMER;
    }

    ~StirrerTimeBasedSequence() {
        delete this->timer_;
    }

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);
};


#endif //JULIA_ANDROID_STIRRERTIMEBASEDSEQUENCE_H
