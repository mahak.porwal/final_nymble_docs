//
// Created by fedal on 24/12/20.
//

#include "StirrerTimeBasedSequence.h"

bool StirrerTimeBasedSequence::ExecuteSequence(
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> *result) {

    switch (this->state_) {
        case START_TIMER: {
            ActuatorDirections_e direction = component_request_.GetDirection();
            ComponentRequestBuilder *componentRequestBuilder = new ComponentRequestBuilder;
            componentRequestBuilder->component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetComponent(
                    STIRRER_MOTOR).SetDirection(direction).SetSpeed(
                    component_request_.GetSpeed());
            *result = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    componentRequestBuilder->component_request_);
            delete componentRequestBuilder;
            this->timer_->SetBeginTime();
            this->state_ = OBSERVE_TIMER;
        }
            break;
        case OBSERVE_TIMER: {
            if (this->timer_->GetTimeElapsed() > component_request_.GetQuantity()) {
                ComponentRequestBuilder *componentRequestBuilder = new ComponentRequestBuilder;
                componentRequestBuilder->component_request_.SetRequestType(
                        HARDWARE_REQUEST).SetComponent(
                        STIRRER_MOTOR).SetDirection(component_request_.GetDirection()).SetSpeed(
                        0);
                *result = HardwareServiceFactory::GetHardwareService()->
                        ProcessComponentRequest(
                        componentRequestBuilder->component_request_);
                delete componentRequestBuilder;
                this->state_ = START_TIMER;
                return true;
            }
        }
            break;
    }
    return false;
}
