//
// Created by fedal on 18/12/20.
//

#ifndef JULIA_ANDROID_SEQUENCEBEHAVIORENUMS_H
#define JULIA_ANDROID_SEQUENCEBEHAVIORENUMS_H

typedef enum {
    /**
     * Macro sequence behaviors
     */
    MACRO_LIQUID_DISPENSE,
    MACRO_NON_LIQUID_DISPENSE,
    MACRO_REACH_POSITION_AT_SPEED,
    MACRO_RESET_CONTAINER,
    MACRO_360_ROTATION,

    /**
     * Micro sequence behaviors
     */
    MICRO_CLOCKWISE_DISPENSE,
    MICRO_ANTICLOCKWISE_DISPENSE,
    MICRO_REACH_ANGLE_WITH_SPEED,
    MICRO_RESET_POD,

    /**
     * Liquid sequence behaviors
     */
    LIQUID_WATER_DISPENSE,
    LIQUID_OIL_DISPENSE,

    /**
     * Stirrer sequence behavior
     */
    STIRRER_TIME_BASED_SEQUENCE,

    /**
     * Induction sequence behavior
     */
    INDUCTION_SEQUENCE,
    INDUCTION_POWER_LEVEL_0,
    INDUCTION_POWER_LEVEL_1,
    INDUCTION_POWER_LEVEL_2,
    INDUCTION_POWER_LEVEL_3,
    INDUCTION_POWER_LEVEL_4,
    INDUCTION_POWER_LEVEL_5,
    INDUCTION_POWER_LEVEL_6,
    INDUCTION_POWER_LEVEL_7,
    INDUCTION_IDLE_BEHAVIOR,
    DEFAULT_BEHAVIOR
} SequenceBehavior_e;


#endif //JULIA_ANDROID_SEQUENCEBEHAVIORENUMS_H
