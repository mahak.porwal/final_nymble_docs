//
// Created by fedal on 24/12/20.
//

#include "LiquidOilDispenseBehavior.h"

using namespace liquid_utility;

bool LiquidOilDispenseBehavior::ExecuteSequence(
        std::unordered_map<ResponseParameters_e, std::vector<std::string>> *result) {

    switch (this->liquid_dispense_state_) {
        case DEFAULT_LIQUID_STATE: {
            this->liquiddispense_lbox_.target_value = component_request_.GetQuantity();
            this->liquid_dispense_state_ = READ_WEIGHT_LBOX;
            this->liquiddispense_lbox_.integral = 0;
            this->timer_->SetBeginTime();
            this->dispense_retry_ = 0;
        }
            break;
        case READ_WEIGHT_LBOX: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetFrequency(1).SetComponent(
                    LOADCELL_SENSOR).SetSensorUnit(GRAMS);
            std::string sensor_value = HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
            this->current_weight_ = std::stod(sensor_value.c_str());

            if (this->current_weight_ != this->pre_weight_) {
                /*new weight*/

                if (this->current_weight_ - this->pre_weight_ < WEIGHT_RANGE
                    && this->current_weight_ - this->pre_weight_
                       > -1 * WEIGHT_RANGE) {
                    /*weight settled*/

                    this->lsamples_++;

                    if (this->lsamples_ > 8) {
                        /*EIGHT STEADY SAMPLE ACHEIVED*/

                        this->dead_reck_weight_ = this->current_weight_;
                        //log_printf("%f %f\n", dead_reck_weight, quantity);
                        //micro_dispense_state =  podnum == PODNUMBER_6? IDENTIFY_POD_1:OPEN_TARGET_POD;
                        this->liquid_dispense_state_ = OPEN_TARGET_LBOX;
                        this->lsamples_ = 0;
                        this->time_out_ms_ = ((this->liquiddispense_lbox_.target_value /
                                               this->flow_rate_grams_per_ms_));
                    }
                } else {
                    /*WEIGHT NOT SETTILED*/
                    this->lsamples_ = 0;
                    if (this->timer_->GetTimeElapsed() > 2000) {
                        //liquid_fault_count += 1;
                        this->dead_reck_weight_ = this->current_weight_;
                        //log_printf("%f %f\n", dead_reck_weight, quantity);
                        //micro_dispense_state =  podnum == PODNUMBER_6? IDENTIFY_POD_1:OPEN_TARGET_POD;
                        this->liquid_dispense_state_ = OPEN_TARGET_LBOX;
                        this->lsamples_ = 0;
                        this->time_out_ms_ = ((this->liquiddispense_lbox_.target_value /
                                               this->flow_rate_grams_per_ms_));
                    }
                }
            }
            this->pre_weight_ = this->current_weight_;
        }
            break;
        case OPEN_TARGET_LBOX: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetComponent(OIL_PUMP).SetState(ON_STATE);
            HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(component_request_builder.component_request_);
            
            this->liquid_dispense_state_ = DISPENSE_LBOX;
            this->timer_->SetBeginTime();
        }
            break;

        case DISPENSE_LBOX: {
            if (timer_->GetTimeElapsed() > this->time_out_ms_) {
                ComponentRequestBuilder component_request_builder;
                component_request_builder.component_request_.SetRequestType(
                        HARDWARE_REQUEST).SetComponent(OIL_PUMP).SetState(
                        OFF_STATE);
                HardwareServiceFactory::GetHardwareService()->
                        ProcessComponentRequest(
                        component_request_builder.component_request_);
                this->liquid_dispense_state_ = UPDATE_STATUS_LBOX;
                this->timer_->SetBeginTime();
                
            } else {
                ComponentRequestBuilder component_request_builder;
                component_request_builder.component_request_.SetRequestType(
                        HARDWARE_REQUEST).SetFrequency(1).SetComponent(
                        LOADCELL_SENSOR).SetSensorUnit(GRAMS);
                std::string sensor_value = HardwareServiceFactory::GetHardwareService()->
                        ProcessComponentRequest(
                        component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
                this->current_weight_ = std::stod(sensor_value.c_str());
                

                if (this->current_weight_ != this->pre_weight_) {
                    //new weight

                    if (this->current_weight_ - this->pre_weight_ < WEIGHT_RANGE
                        && this->current_weight_ - this->pre_weight_
                           > -1 * WEIGHT_RANGE) {
                        //weight settled

                        this->lsamples_++;

                        if (this->lsamples_ > 8) {
                            //EIGHT STEADY SAMPLE ACHEIVED
                            //dead_reck_weight = this->current_weight_;log_printf("%f %f\n",dead_reck_weight,quantity);
                            this->liquiddispense_lbox_.current_value =
                                    (float) ((this->current_weight_)
                                             - (this->dead_reck_weight_));

                            if (this->liquiddispense_lbox_.current_value
                                > component_request_.GetQuantity() * 0.95) {

                                ComponentRequestBuilder component_request_builder;
                                component_request_builder.component_request_.SetRequestType(
                                        HARDWARE_REQUEST).SetComponent(
                                        OIL_PUMP).SetState(OFF_STATE);
                                HardwareServiceFactory::GetHardwareService()->
                                        ProcessComponentRequest(
                                        component_request_builder.component_request_);
                                

                                SetTargetWeight(this->liquiddispense_lbox_.target_value, *result);
                                SetDispensedWeight(this->current_weight_, *result);
                                SetActualDispensedWeight(
                                        this->current_weight_ - this->dead_reck_weight_, *result);

                                this->liquid_dispense_state_ = DEFAULT_LIQUID_STATE;
                                this->dispense_retry_ = 0;
                                this->lsamples_ = 0;
                                std::vector<string> values;
                                values.push_back("1");
                                (*result).emplace(SEQUENCE_DONE, values);
                                return true;
                            }
                        }
                    } else {
                        //WEIGHT NOT SETTILED
                        this->lsamples_ = 0;
                    }
                }
                this->pre_weight_ = this->current_weight_;
            }
        }
            break;
        case CLOSE_TARGET_LBOX: {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetRequestType(
                    HARDWARE_REQUEST).SetComponent(
                    OIL_PUMP).SetState(OFF_STATE);
            HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    component_request_builder.component_request_);
            

            this->liquid_dispense_state_ = UPDATE_STATUS_LBOX;
        }
            break;
        case UPDATE_STATUS_LBOX:

            if (this->dispense_retry_ > 5) {
                this->lsamples_ = 0;
                SetTargetWeight(this->liquiddispense_lbox_.target_value, *result);
                SetDispensedWeight(this->current_weight_, *result);
                SetActualDispensedWeight(
                        this->current_weight_ - this->dead_reck_weight_, *result);
                this->liquid_dispense_state_ = DEFAULT_LIQUID_STATE;
                this->dispense_retry_ = 0;
                std::vector<string> values;
                values.push_back("1");
                (*result).emplace(SEQUENCE_DONE, values);
                return true;
            }
            if (this->timer_->GetTimeElapsed() > 4000) {
                //liquid_fault_count += 2;
                this->lsamples_ = 0;
                SetTargetWeight(this->liquiddispense_lbox_.target_value, *result);
                SetDispensedWeight(this->current_weight_, *result);
                SetActualDispensedWeight(
                        this->current_weight_ - this->dead_reck_weight_, *result);
                this->liquid_dispense_state_ = DEFAULT_LIQUID_STATE;
                this->dispense_retry_ = 0;
                std::vector<string> values;
                values.push_back("1");
                (*result).emplace(SEQUENCE_DONE, values);
                return true;
            } else {
                ComponentRequestBuilder component_request_builder;
                component_request_builder.component_request_.SetRequestType(
                        HARDWARE_REQUEST).SetFrequency(1).SetComponent(
                        LOADCELL_SENSOR).SetSensorUnit(GRAMS);
                std::string sensor_value = HardwareServiceFactory::GetHardwareService()->
                        ProcessComponentRequest(
                        component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
                this->current_weight_ = std::stod(sensor_value.c_str());
                

                if (this->current_weight_ != this->pre_weight_) {
                    /*new weight*/

                    if (this->current_weight_ - this->pre_weight_ < WEIGHT_RANGE
                        && this->current_weight_ - this->pre_weight_
                           > -1 * WEIGHT_RANGE) {
                        /*weight settled*/

                        this->lsamples_++;

                        if (this->lsamples_ > 8) {

                            /*EIGHT STEADY SAMPLE ACHEIVED*/
                            this->liquiddispense_lbox_.current_value =
                                    (float) ((this->current_weight_)
                                             - (this->dead_reck_weight_));

                            if (this->liquiddispense_lbox_.current_value
                                > this->liquiddispense_lbox_.target_value * 0.95) {
                                this->liquiddispense_lbox_.integral +=
                                        this->liquiddispense_lbox_.current_value;

                                this->timer_->SetBeginTime();
                                this->dead_reck_weight_ = this->current_weight_;

                                this->dispense_retry_ = 0;
                                SetTargetWeight(this->liquiddispense_lbox_.target_value, *result);
                                SetDispensedWeight(this->current_weight_, *result);
                                SetActualDispensedWeight(
                                        this->current_weight_ - this->dead_reck_weight_, *result);
                                this->liquid_dispense_state_ = DEFAULT_LIQUID_STATE;
                                this->dispense_retry_ = 0;
                                std::vector<string> values;
                                values.push_back("1");
                                (*result).emplace(SEQUENCE_DONE, values);
                                return true;
                            } else {
                                this->liquiddispense_lbox_.target_value -=
                                        this->liquiddispense_lbox_.current_value;
                                this->liquiddispense_lbox_.integral +=
                                        this->liquiddispense_lbox_.current_value;
                                this->dispense_retry_++;
                                this->liquid_dispense_state_ = READ_WEIGHT_LBOX;
                            }
                        }
                    } else {
                        /*WEIGHT NOT SETTILED*/
                        this->lsamples_ = 0;
                    }
                }
                this->pre_weight_ = this->current_weight_;
            }
            break;
        default:
            break;
    }
    return false;
}
