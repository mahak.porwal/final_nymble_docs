//
// Created by fedal on 24/12/20.
//

#ifndef JULIA_ANDROID_LIQUIDOILDISPENSEBEHAVIOR_H
#define JULIA_ANDROID_LIQUIDOILDISPENSEBEHAVIOR_H


#include "../../SequenceBehavior.h"
#include "LiquidOIlDispenseEnums.h"
#include "../Utililty/Utility.h"

#define WEIGHT_RANGE 1.5

class LiquidOilDispenseBehavior : public SequenceBehavior {
private:
    double dead_reck_weight_ = 0;
    struct DispensePid liquiddispense_lbox_ = {0};
    OilDispenseStates_e liquid_dispense_state_ = DEFAULT_LIQUID_STATE;
    uint8_t lsamples_ = 0;
    double pre_weight_ = 0;
    double current_weight_ = 0;
    Timer *timer_;
    uint8_t dispense_retry_ = 0;
    uint32_t time_out_ms_ = 0;
    double flow_rate_grams_per_ms_ = 0;
public:
    LiquidOilDispenseBehavior(ComponentRequest component_request) :
            SequenceBehavior(component_request) {
        this->behavior_ = LIQUID_OIL_DISPENSE;
        this->flow_rate_grams_per_ms_ = 0.00046;
        this->timer_ = new Timer;
    }

    ~LiquidOilDispenseBehavior() {
        delete this->timer_;
    }

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);
};


#endif //JULIA_ANDROID_LIQUIDOILDISPENSEBEHAVIOR_H
