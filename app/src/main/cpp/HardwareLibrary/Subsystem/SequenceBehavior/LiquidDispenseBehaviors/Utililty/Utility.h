//
// Created by fedal on 12/4/21.
//

#ifndef JULIA_ANDROID_UTILITY_H
#define JULIA_ANDROID_UTILITY_H
namespace liquid_utility {
    void SetTargetWeight(double target_weight,
                         std::unordered_map <ResponseParameters_e, std::vector<std::string>> &result){
        std::string target_weight_string = std::to_string(
                target_weight);
        std::vector<std::string> vector_target_weight{target_weight_string};

        result.emplace(TARGET_WEIGHT, vector_target_weight);
    }

    void SetDispensedWeight(double target_weight,
                             std::unordered_map <ResponseParameters_e, std::vector<std::string>> &result){
        std::string target_weight_string = std::to_string(
                target_weight);
        std::vector<std::string> vector_target_weight{target_weight_string};

        result.emplace(TARGET_WEIGHT, vector_target_weight);
    }

    void SetActualDispensedWeight(double target_weight,
                                  std::unordered_map <ResponseParameters_e, std::vector<std::string>> &result){
        std::string target_weight_string = std::to_string(
                target_weight);
        std::vector<std::string> vector_target_weight{target_weight_string};

        result.emplace(TARGET_WEIGHT, vector_target_weight);
    }
}

#endif //JULIA_ANDROID_UTILITY_H
