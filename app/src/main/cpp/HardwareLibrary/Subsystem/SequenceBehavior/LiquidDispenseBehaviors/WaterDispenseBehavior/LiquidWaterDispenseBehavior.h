//
// Created by fedal on 24/12/20.
//

#ifndef JULIA_ANDROID_LIQUIDWATERDISPENSEBEHAVIOR_H
#define JULIA_ANDROID_LIQUIDWATERDISPENSEBEHAVIOR_H


#include "../PID/PID.h"
#include "../OilDispenseBehavior/LiquidOIlDispenseEnums.h"

class LiquidWaterDispenseBehavior : public SequenceBehavior {
private:
    double dead_reck_weight_ = 0;
    struct DispensePid liquiddispense_lbox_ = {0};
    OilDispenseStates_e liquid_dispense_state_ = DEFAULT_LIQUID_STATE;
    uint8_t lsamples_ = 0;
    double pre_weight_ = 0;
    double current_weight_ = 0;
    Timer *timer_;
    uint8_t dispense_retry_ = 0;
    uint32_t time_out_ms_ = 0;
    double flow_rate_grams_per_ms_ = 0;
public:
    LiquidWaterDispenseBehavior(ComponentRequest component_request) :
            SequenceBehavior(component_request) {
        this->behavior_ = LIQUID_WATER_DISPENSE;
        this->flow_rate_grams_per_ms_ = 0.00728;
        this->timer_ = new Timer;
    }

    ~LiquidWaterDispenseBehavior() {
        delete this->timer_;
    }

    bool ExecuteSequence(std::unordered_map<ResponseParameters_e, std::vector<std::string>> *);
};


#endif //JULIA_ANDROID_LIQUIDWATERDISPENSEBEHAVIOR_H
