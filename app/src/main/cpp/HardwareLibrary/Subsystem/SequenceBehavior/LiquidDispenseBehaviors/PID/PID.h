//
// Created by fedal on 24/12/20.
//

#ifndef JULIA_ANDROID_PID_H
#define JULIA_ANDROID_PID_H
/**
 * A structure to represent PID variables
 */
struct DispensePid{
    /*@{*/
    float kp  ; /**< the Proportional constant Kp */
    float ki  ; /**< the Integral constant Ki */
    float kd  ; /**< the Derivative constant Kd */
    float error; /**< the PID Error   */
    float last_error ; /**< the PID derivative error   */
    float target_value ;  /**< the PID target value   */
    float current_value ; /**< the PID current value   */
    float derivative ; /**< the PID derivative cumulative value  */
    float proportional  ; /**< the PID proportional cumulative value  */
    float integral  ; /**< the PID integral cumulative value  */
    float output  ; /**< the PID output  */
    /*@}*/
};
#endif //JULIA_ANDROID_PID_H
