//
// Created by fedal on 24/12/20.
//

#ifndef JULIA_ANDROID_LIQUIDOILDISPENSEENUMS_H
#define JULIA_ANDROID_LIQUIDOILDISPENSEENUMS_H
/**
 * An enum  to represent Liquiddispense Control State
 */
typedef enum
{
/*@{*/
    DEFAULT_LIQUID_STATE = 0,/**< the liquiddispensing default control state initialization for the Enum */
    IDENTIFY_LBOX       =  1,/**< the liquiddispensing lbox identify control state Enum */
    REACH_TARGET_LBOX   =  2,/**< the liquiddispensing reach target lbox control state Enum */
    OPEN_TARGET_LBOX    =  3,/**< the liquiddispensing open target lbox control state Enum */
    READ_WEIGHT_LBOX    =  4,/**< the liquiddispensing read  lbox weight state Enum */
    DISPENSE_LBOX       =  5,/**< the liquiddispensing dispense target lbox control state Enum */
    CLOSE_TARGET_LBOX   =  6,/**< the liquiddispensing close target lbox control state Enum */
    UPDATE_STATUS_LBOX   =  7,/**< the liquiddispensing increment target lbox control state Enum */
/*@}*/
}OilDispenseStates_e;/**< enum liquid_dispense_state member liquid_dispense_state_e*/

#endif //JULIA_ANDROID_LIQUIDOILDISPENSEENUMS_H
