//
// Created by fedal on 17/12/20.
//

#ifndef JULIA_ANDROID_COMPONENTENUMS_H
#define JULIA_ANDROID_COMPONENTENUMS_H
typedef enum {

    /**
     * Macro Subsystem
     */
    /**
     * 180 DEGREE SERVOS
     */
    MACRO_SERVO_1,
    MACRO_SERVO_2,
    MACRO_SERVO_3,
    MACRO_SERVO_4,
    MACRO_SERVO_5,
    MACRO_SERVO_6,
    MACRO_SERVO_7,
    MACRO_SERVO_8,
    /**
     * ANALOG POT FEEDBACK
     */
    MACRO_SERVO_1_FEEDBACK,
    MACRO_SERVO_2_FEEDBACK,
    MACRO_SERVO_3_FEEDBACK,
    MACRO_SERVO_4_FEEDBACK,
    MACRO_SERVO_5_FEEDBACK,
    MACRO_SERVO_6_FEEDBACK,
    MACRO_SERVO_7FEEDBACK,
    MACRO_SERVO_8_FEEDBACK,
    /**
     * GPIO CONTROLS
     */
    MACRO_POT_MUX_SELECT_1,
    MACRO_POT_MUX_SELECT_2,
    MACRO_POT_MUX_SELECT_3,
    MACRO_MUX,


    /**
     * *****************************************************************************
     */
    /**
     * Micro Subsystem
     */
    /**
     * 360 DEGREE SERVO
     */
    MICRO_SERVO_1,
    MICRO_SERVO_2,
    MICRO_SERVO_3,
    MICRO_SERVO_4,
    MICRO_SERVO_5,
    MICRO_SERVO_6,
    MICRO_SERVO_7,
    MICRO_SERVO_8,
    /**
     * ANALOG POT FEEDBACK
     */
    MICRO_SERVO_1_FEEDBACK,
    MICRO_SERVO_2_FEEDBACK,
    MICRO_SERVO_3_FEEDBACK,
    MICRO_SERVO_4_FEEDBACK,
    MICRO_SERVO_5_FEEDBACK,
    MICRO_SERVO_6_FEEDBACK,
    MICRO_SERVO_7FEEDBACK,
    MICRO_SERVO_8_FEEDBACK,
    /**
     * GPIO CONTROLS
     */
    MICRO_POT_MUX_SELECT_1,
    MICRO_POT_MUX_SELECT_2,
    MICRO_POT_MUX_SELECT_3,
    MICRO_MUX,
    /**
     * Stirrer
     */
    /**
     * DC MOTOR
     */
    STIRRER_MOTOR,
    /**
     * GPIO CONTROLS
     */
    STIRRER_PHASE_GPIO,
    STIRRER_CONNECTION_FB,

    /**
     * ANALOG FEEDBACK
     */
    STIRRER_CURRENT_SENSOR,

    /**
     ******************************************************************************
     **/

    /**
     * Liquid Subsystem
     */
    OIL_PUMP,
    WATER_PUMP,

    /**
     * Induction Subsystem
     */
    INDUCTION_HEATER,
    /**
     * DAC
     */
    INDUCTION_ACTUATOR,
    /**
     * Analog Feedback
     */
    INDUCTION_AC_CURRENT_SENSOR,
    INDUCTION_AC_VOLTAGE_SENSOR,
    INDUCTION_IGBT_TEMPERATURE_SENSOR,
    INDUCTION_PAN_TEMPERATURE_SENSOR,
    /**
     * GPIO Controls
     */
    INDUCTION_PAN_GPIO,
    INDUCTION_INT_GPIO,
    INDUCTION_FAN_GPIO,
    INDUCTION_ON_OFF_GPIO,
    INDUCTION_BUZZER_GPIO,
    INDUCTION_PAN_FREQUENCY_POT,
    INDUCTION_PAN_DUTY_POT,
    INDUCTION_PWM_FREQUENCY_POT,
    INDUCTION_PWM_DUTY_POT,

    /**
     * Common Hardware Component
     */

    /**
    * Analog feedback
    */
    SOM_BOARD_CURRENT_SENSOR,
    LOADCELL_SENSOR,
    /**
    * Led Actuation
    */
    LED_ILLUMINATION,
    LED_DESIGN,

    /**
     * Motor Actuation
     */
    EXHAUST_FAN,
    EXHAUST_FAN_ON_OFF,
    BUZZER,

    /**
     * GPIO controls
     */
    INA300_LATCH,
    INA300_ALERT,
    LM2678_CONTROL,
    MACRO_MICRO_FB_SELECT_1,
    MACRO_MICRO_MUX,

    /**
     * Thermal camera
     */
    THERMAL_CAMERA_EEPROM,
    THERMAL_CAMERA,

    /**
     * Temperature Feedback
     */
    WATER_TEMPERATURE,
    BODY_TEMPERATURE,
    /**
     * Counter
     */
    COUNTER,
    COUNTER_MUX_SELECT_0,
    COUNTER_MUX_SELECT_1,
    EXHAUST_FAN_COUNTER,
    OIL_FLOW_COUNTER,
    WATER_FLOW_COUNTER,

    /**
     * Heater
     */
    HEATER_ENABLE,
    HEATER_STATUS,

    /**
     * Limit Switches
     */
    LIMIT_SWITCH_1,
    LIMIT_SWITCH_2,

    /**
     * Inductive Sense
     */
    INDUCTIVE_SENSOR_1,
    INDUCTIVE_SENSOR_2,

    DEFAULT_COMPONENT

} Component_e;

#endif //JULIA_ANDROID_COMPONENTENUMS_H
