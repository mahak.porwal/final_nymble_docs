//
// Created by fedal on 21/12/20.
//

#include "Micro.h"


/*bool Micro::ResetSubsystem() throw(Error) {
    return false;
}

void Micro::PopulateComponentMap() {
    SubsystemComponent *micro_pod;
    micro_pod = new SubsystemComponent(MICRO_POD_1);
    this->component_map_.insert({MICRO_POD_1,micro_pod});
    micro_pod->AddToComponentSequenceMap(MICRO_ANTICLOCKWISE_DISPENSE,
                                               new MicroAntiClockwiseDispenseBehavior(
                                                       MICRO_ANTICLOCKWISE_DISPENSE));
    micro_pod->AddToComponentSequenceMap(MICRO_CLOCKWISE_DISPENSE,
                                         new MicroClockwiseDispenseBehavior(
                                                 MICRO_CLOCKWISE_DISPENSE));

    micro_pod = new SubsystemComponent(MICRO_POD_2);
    this->component_map_.insert({MICRO_POD_2,micro_pod});
    micro_pod->AddToComponentSequenceMap(MICRO_ANTICLOCKWISE_DISPENSE,
                                         new MicroAntiClockwiseDispenseBehavior(
                                                 MICRO_ANTICLOCKWISE_DISPENSE));
    micro_pod->AddToComponentSequenceMap(MICRO_CLOCKWISE_DISPENSE,
                                         new MicroClockwiseDispenseBehavior(
                                                 MICRO_CLOCKWISE_DISPENSE));
    micro_pod = new SubsystemComponent(MICRO_POD_3);
    this->component_map_.insert({MICRO_POD_3,micro_pod});
    micro_pod->AddToComponentSequenceMap(MICRO_CLOCKWISE_DISPENSE,
                                         new MicroClockwiseDispenseBehavior(
                                                 MICRO_CLOCKWISE_DISPENSE));
    micro_pod->AddToComponentSequenceMap(MICRO_ANTICLOCKWISE_DISPENSE,
                                         new MicroAntiClockwiseDispenseBehavior(
                                                 MICRO_ANTICLOCKWISE_DISPENSE));
    micro_pod = new SubsystemComponent(MICRO_POD_4);
    this->component_map_.insert({MICRO_POD_4,micro_pod});
    micro_pod->AddToComponentSequenceMap(MICRO_ANTICLOCKWISE_DISPENSE,
                                         new MicroAntiClockwiseDispenseBehavior(
                                                 MICRO_ANTICLOCKWISE_DISPENSE));
    micro_pod->AddToComponentSequenceMap(MICRO_CLOCKWISE_DISPENSE,
                                         new MicroClockwiseDispenseBehavior(
                                                 MICRO_CLOCKWISE_DISPENSE));
    micro_pod = new SubsystemComponent(MICRO_POD_5);
    this->component_map_.insert({MICRO_POD_5,micro_pod});
    micro_pod->AddToComponentSequenceMap(MICRO_ANTICLOCKWISE_DISPENSE,
                                         new MicroAntiClockwiseDispenseBehavior(
                                                 MICRO_ANTICLOCKWISE_DISPENSE));
    micro_pod->AddToComponentSequenceMap(MICRO_CLOCKWISE_DISPENSE,
                                         new MicroClockwiseDispenseBehavior(
                                                 MICRO_CLOCKWISE_DISPENSE));
    micro_pod = new SubsystemComponent(MICRO_POD_6);
    this->component_map_.insert({MICRO_POD_6,micro_pod});
    micro_pod->AddToComponentSequenceMap(MICRO_ANTICLOCKWISE_DISPENSE,
                                         new MicroAntiClockwiseDispenseBehavior(
                                                 MICRO_ANTICLOCKWISE_DISPENSE));
    micro_pod->AddToComponentSequenceMap(MICRO_CLOCKWISE_DISPENSE,
                                         new MicroClockwiseDispenseBehavior(
                                                 MICRO_CLOCKWISE_DISPENSE));

    SubsystemComponent* exhaust_fan = new SubsystemComponent(EXHAUST_FAN_COMPONENT);
    this->component_map_.insert({EXHAUST_FAN_COMPONENT,exhaust_fan});
}

void Micro::CreateSubsystemRequestForMicroCommand(HardwareMessage hardwareMessage, SubsystemRequest * subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(HARDWARE_REQUEST);
    switch (hardwareMessage.GetId()) {
        case MessageConstants::Vib_Mot_1: {
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Micro Id 1 ");
            subsystemRequest->SetSubsystemComponent(MICRO_POD_1);
        }
            break;
        case MessageConstants::Vib_Mot_2: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_2);
        }
            break;
        case MessageConstants::Vib_Mot_3: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_3);
        }
            break;
        case MessageConstants::Vib_Mot_4: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_4);
        }
            break;
        case MessageConstants::Vib_Mot_5: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_5);
        }
            break;
        case MessageConstants::Vib_Mot_6: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_6);
        }
            break;
        case MessageConstants::Exhaust_Fan:{
            subsystemRequest->SetSubsystemComponent(EXHAUST_FAN_COMPONENT);
        }
        break;
        default:
            throw new Error;
    }
    if (hardwareMessage.GetValueMap()->find(MessageConstants::Primary) !=
        hardwareMessage.GetValueMap()->end()) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Micro primary val %d",hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
        subsystemRequest->SetSpeed(hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
    } else {
        throw new Error;
    }

    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "exit create subsystem request");
}

void Micro::CreateSubsystemRequestForMicroAction(HardwareMessage hardwareMessage, SubsystemRequest * subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(SEQUENCE_REQUEST);
    switch (hardwareMessage.GetId()) {
        case MessageConstants::Vib_Mot_1: {
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Micro Id 1 ");
            subsystemRequest->SetSubsystemComponent(MICRO_POD_1);
            subsystemRequest->SetSubsystemSequence(MICRO_ANTICLOCKWISE_DISPENSE);
        }
            break;
        case MessageConstants::Vib_Mot_2: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_2);
            subsystemRequest->SetSubsystemSequence(MICRO_ANTICLOCKWISE_DISPENSE);
        }
            break;
        case MessageConstants::Vib_Mot_3: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_3);
            subsystemRequest->SetSubsystemSequence(MICRO_CLOCKWISE_DISPENSE);
        }
            break;
        case MessageConstants::Vib_Mot_4: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_4);
            subsystemRequest->SetSubsystemSequence(MICRO_ANTICLOCKWISE_DISPENSE);
        }
            break;
        case MessageConstants::Vib_Mot_5: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_5);
            subsystemRequest->SetSubsystemSequence(MICRO_ANTICLOCKWISE_DISPENSE);
        }
            break;
        case MessageConstants::Vib_Mot_6: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_6);
            subsystemRequest->SetSubsystemSequence(MICRO_CLOCKWISE_DISPENSE);
        }
            break;
        default:
            throw new Error;
    }

    if (hardwareMessage.GetValueMap()->find(MessageConstants::Primary) !=
        hardwareMessage.GetValueMap()->end()) {
        subsystemRequest->SetQuantity(hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
    } else {
        throw new Error;
    }
}

void Micro::CreateSubsystemRequestForMicroRequest(HardwareMessage hardwareMessage, SubsystemRequest * subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(HARDWARE_REQUEST);
    subsystemRequest->SetFrequency(1);
    switch (hardwareMessage.GetId()) {
        case MessageConstants::Vib_Mot_1: {
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Micro Id 1 ");
            subsystemRequest->SetSubsystemComponent(MICRO_POD_1);
        }
            break;
        case MessageConstants::Vib_Mot_2: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_2);
        }
            break;
        case MessageConstants::Vib_Mot_3: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_3);
        }
            break;
        case MessageConstants::Vib_Mot_4: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_4);
        }
            break;
        case MessageConstants::Vib_Mot_5: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_5);
        }
            break;
        case MessageConstants::Vib_Mot_6: {
            subsystemRequest->SetSubsystemComponent(MICRO_POD_6);
        }
            break;
        default:
            throw new Error;
    }
}*/

void Micro::PopulateComponentSequenceMap() {

}
