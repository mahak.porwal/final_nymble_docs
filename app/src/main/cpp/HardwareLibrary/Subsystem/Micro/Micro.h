//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_MICRO_H
#define JULIA_ANDROID_MICRO_H


#include "../../Error/Error.h"
#include "../Subsystem.h"

class Micro : public Subsystem{
private:
    void PopulateComponentSequenceMap();

public:

    Micro() {
        //this->PopulateComponentSequenceMap();
    }

    ~Micro() {
    }
};


#endif //JULIA_ANDROID_MICRO_H
