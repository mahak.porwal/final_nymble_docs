//
// Created by fedal on 21/12/20.
//

#ifndef JULIA_ANDROID_STIRRER_H
#define JULIA_ANDROID_STIRRER_H


#include "../Subsystem.h"

class Stirrer : public Subsystem {
private:
    void PopulateComponentSequenceMap();

public:

    Stirrer() {
        //this->PopulateComponentSequenceMap();
    }

    ~Stirrer() {
    }
};


#endif //JULIA_ANDROID_STIRRER_H
