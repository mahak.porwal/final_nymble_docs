//
// Created by fedal on 21/12/20.
//

#include "Stirrer.h"
/*#include "../SubystemSequenceBehavior/StirrerSequenceBehaviors/StirrerTimeBasedSequence/StirrerTimeBasedSequence.h"

bool Stirrer::ResetSubsystem() throw(Error) {
    return false;
}

void Stirrer::PopulateComponentMap() {
    SubsystemComponent *stirrer_motor;
    stirrer_motor = new SubsystemComponent(STIRRER_ACTUATOR);
    this->component_map_.insert({STIRRER_ACTUATOR, stirrer_motor});
    stirrer_motor->AddToComponentSequenceMap(STIRRER_TIME_BASED_SEQUENCE,
                                             new StirrerTimeBasedSequence(
                                                     STIRRER_TIME_BASED_SEQUENCE));
}

void Stirrer::CreateSubsystemRequestForStirrerCommand(HardwareMessage hardwareMessage, SubsystemRequest * subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(HARDWARE_REQUEST);
    subsystemRequest->SetSubsystemComponent(STIRRER_ACTUATOR);
    if (hardwareMessage.GetValueMap()->find(MessageConstants::Primary) !=
        hardwareMessage.GetValueMap()->end()) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Micro primary val %d",hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
        subsystemRequest->SetSpeed(abs(hardwareMessage.GetValueMap()->at(MessageConstants::Primary)));
        if(hardwareMessage.GetValueMap()->at(MessageConstants::Primary) > 0){
            subsystemRequest->SetDirection(DIRECTION_ANTICLOCKWISE);
        }else{
            subsystemRequest->SetDirection(DIRECTION_CLOCKWISE);
        }
    } else {
        throw new Error;
    }

    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "exit create subsystem request");
}

void Stirrer::CreateSubsystemRequestForStirrerAction(HardwareMessage hardwareMessage, SubsystemRequest * subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(SEQUENCE_REQUEST);
    subsystemRequest->SetSubsystemComponent(STIRRER_ACTUATOR);
    subsystemRequest->SetSubsystemSequence(STIRRER_TIME_BASED_SEQUENCE);

    if (hardwareMessage.GetValueMap()->find(MessageConstants::Primary) !=
        hardwareMessage.GetValueMap()->end()) {
        if(hardwareMessage.GetValueMap()->at(MessageConstants::Primary) < 0){
            subsystemRequest->SetDirection(DIRECTION_ANTICLOCKWISE);
        }else{
            subsystemRequest->SetDirection(DIRECTION_CLOCKWISE);
        }
        subsystemRequest->SetSpeed(abs(hardwareMessage.GetValueMap()->at(MessageConstants::Primary)));
    } else {
        throw new Error;
    }

    if (hardwareMessage.GetValueMap()->find(MessageConstants::Secondary) !=
        hardwareMessage.GetValueMap()->end()) {
        subsystemRequest->SetQuantity(abs(hardwareMessage.GetValueMap()->at(MessageConstants::Secondary)));
    } else {
        throw new Error;
    }
}

void Stirrer::CreateSubsystemRequestForStirrerRequest(HardwareMessage hardwareMessage, SubsystemRequest * subsystemRequest) throw(Error) {
    //TODO
}*/

void Stirrer::PopulateComponentSequenceMap() {

}
