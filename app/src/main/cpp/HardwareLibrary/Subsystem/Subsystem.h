//
// Created by fedal on 2/12/20.
//

#ifndef JULIA_ANDROID_SUBSYSTEM_H
#define JULIA_ANDROID_SUBSYSTEM_H


#include "../Error/Error.h"
#include "SequenceBehavior/SequenceBehaviorEnums.h"
#include "SubsystemComponent/ComponentEnums.h"
#include<unordered_set>
#include <unordered_map>
class Subsystem {
public :
    std::unordered_map<SequenceBehavior_e,
            std::unordered_set<Component_e>>::const_iterator get_component_sequence_map_begin;
    std::unordered_map<SequenceBehavior_e,
            std::unordered_set<Component_e>>::const_iterator get_component_sequence_map_end;
private:
    std::unordered_map<SequenceBehavior_e,
            std::unordered_set<Component_e> > component_sequence_map_;
};

#endif //JULIA_ANDROID_SUBSYSTEM_H
