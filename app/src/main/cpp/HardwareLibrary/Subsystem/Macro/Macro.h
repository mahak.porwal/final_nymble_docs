//
// Created by fedal on 19/12/20.
//

#ifndef JULIA_ANDROID_MACRO_H
#define JULIA_ANDROID_MACRO_H


#include "../Subsystem.h"

class Macro : public Subsystem {

private:
    void PopulateComponentSequenceMap();

public:

    Macro() {
        //this->PopulateComponentSequenceMap();
    }

    ~Macro() {
    }

};


#endif //JULIA_ANDROID_MACRO_H
