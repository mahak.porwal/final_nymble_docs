//
// Created by fedal on 19/12/20.
//

#include "Macro.h"
//#include "../SubsystemComponent/MacroComponent/MacroContainer/MacroContainer.h"

/*
bool Macro::ResetSubsystem() throw(Error) {

    return false;
}

map<int, double> Macro::PerformSubsystemRequest(SubsystemRequest *subsystem_request) throw(Error) {
    map<int, double> subsystem_request_result;
    SubsystemComponent_e subsystem_component_id = subsystem_request->GetSubsystemComponent();
    SubsystemComponent *subsystem_component = nullptr;
    subsystem_component = this->component_map_.at(subsystem_component_id);
    if (subsystem_request->IsAbortSequence()) {
        subsystem_component->SetAbort();
        return subsystem_request_result;
    }
    if (subsystem_component->IsBusy()) {
        throw new Error;
    }
    if (subsystem_request->GetRequestType() == HARDWARE_REQUEST) {
        subsystem_component->PerformHardwareRequest(subsystem_request, &subsystem_request_result);
    } else {
        subsystem_component->PerformSequenceRequest(subsystem_request, &subsystem_request_result);
    }
    return subsystem_request_result;
}

void Macro::PopulateComponentMap() {
    SubsystemComponent *macro_container;
    macro_container = new MacroContainer(MACRO_CONTAINER_1);
    this->component_map_.insert({MACRO_CONTAINER_1, macro_container});
    macro_container->AddToComponentSequenceMap(MACRO_LIQUID_DISPENSE,
                                               new MacroLiquidDispenseBehavior(
                                                       MACRO_LIQUID_DISPENSE));
    macro_container->AddToComponentSequenceMap(MACRO_NON_LIQUID_DISPENSE,
                                               new MacroNonLiquidDispenseBehavior(
                                                       MACRO_NON_LIQUID_DISPENSE));

    macro_container->AddToComponentSequenceMap(MACRO_RESET,
                                               new MacroResetBehavior(
                                                       MACRO_RESET));


    macro_container = new MacroContainer(MACRO_CONTAINER_2);
    this->component_map_.insert({MACRO_CONTAINER_2, macro_container});
    macro_container->AddToComponentSequenceMap(MACRO_LIQUID_DISPENSE,
                                               new MacroLiquidDispenseBehavior(
                                                       MACRO_LIQUID_DISPENSE));
    macro_container->AddToComponentSequenceMap(MACRO_NON_LIQUID_DISPENSE,
                                               new MacroNonLiquidDispenseBehavior(
                                                       MACRO_NON_LIQUID_DISPENSE));

    macro_container->AddToComponentSequenceMap(MACRO_RESET,
                                               new MacroResetBehavior(
                                                       MACRO_RESET));

    macro_container = new MacroContainer(MACRO_CONTAINER_3);
    this->component_map_.insert({MACRO_CONTAINER_3, macro_container});
    macro_container->AddToComponentSequenceMap(MACRO_LIQUID_DISPENSE,
                                               new MacroLiquidDispenseBehavior(
                                                       MACRO_LIQUID_DISPENSE));

    macro_container->AddToComponentSequenceMap(MACRO_NON_LIQUID_DISPENSE,
                                               new MacroNonLiquidDispenseBehavior(
                                                       MACRO_NON_LIQUID_DISPENSE));

    macro_container->AddToComponentSequenceMap(MACRO_RESET,
                                               new MacroResetBehavior(
                                                       MACRO_RESET));


    macro_container = new MacroContainer(MACRO_CONTAINER_4);
    this->component_map_.insert({MACRO_CONTAINER_4, macro_container});
    macro_container->AddToComponentSequenceMap(MACRO_LIQUID_DISPENSE,
                                               new MacroLiquidDispenseBehavior(
                                                       MACRO_LIQUID_DISPENSE));

    macro_container->AddToComponentSequenceMap(MACRO_NON_LIQUID_DISPENSE,
                                               new MacroNonLiquidDispenseBehavior(
                                                       MACRO_NON_LIQUID_DISPENSE));

    macro_container->AddToComponentSequenceMap(MACRO_RESET,
                                               new MacroResetBehavior(
                                                       MACRO_RESET));


    SubsystemComponent* weight_sensor = new SubsystemComponent(PAN_WEIGHT_SENSOR);
    this->component_map_.insert({PAN_WEIGHT_SENSOR, weight_sensor});

}

void Macro::CreateSubsystemRequestForMacroCommand(HardwareMessage hardwareMessage,
                                                  SubsystemRequest *subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(HARDWARE_REQUEST);
    switch (hardwareMessage.GetId()) {
        case MessageConstants::Macro_Mot_1: {
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro Id 1 ");
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_1);
        }
            break;
        case MessageConstants::Macro_Mot_2: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_2);
        }
            break;
        case MessageConstants::Macro_Mot_3: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_3);
        }
            break;
        case MessageConstants::Macro_Mot_4: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_4);
        }
            break;
        default:
            throw new Error;
    }
    if (hardwareMessage.GetValueMap()->find(MessageConstants::Primary) !=
        hardwareMessage.GetValueMap()->end()) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Macro primary val %d",
                            hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
        subsystemRequest->SetPosition(hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
    } else {
        throw new Error;
    }

    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "exit create subsystem request");
}

void Macro::CreateSubsystemRequestForMacroAction(HardwareMessage hardwareMessage,
                                                 SubsystemRequest *subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(SEQUENCE_REQUEST);
    switch (hardwareMessage.GetId()) {
        case MessageConstants::Macro_Mot_1: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_1);
        }
            break;
        case MessageConstants::Macro_Mot_2: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_2);
        }
            break;
        case MessageConstants::Macro_Mot_3: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_3);
        }
            break;
        case MessageConstants::Macro_Mot_4: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_4);
        }
            break;

            break;
        default:
            throw new Error;
    }

    if (hardwareMessage.GetValueMap()->find(MessageConstants::Abort_Sequence) !=
        hardwareMessage.GetValueMap()->end()) {
        subsystemRequest->SetAbortSequence(true);
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "abort set");
        return;
    } else {
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "abort sequence not found");
    }

    if (hardwareMessage.GetValueMap()->find(MessageConstants::Primary) !=
        hardwareMessage.GetValueMap()->end()) {
        subsystemRequest->SetQuantity(hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
    } else {
        throw new Error;
    }

    if (hardwareMessage.GetValueMap()->find(MessageConstants::Secondary) !=
        hardwareMessage.GetValueMap()->end()) {
        switch (hardwareMessage.GetValueMap()->at(MessageConstants::Secondary)) {
            case MessageConstants::Non_Liquid_Macro: {
                subsystemRequest->SetSubsystemSequence(MACRO_NON_LIQUID_DISPENSE);
            }
                break;
            case MessageConstants::Liquid_Macro: {
                subsystemRequest->SetSubsystemSequence(MACRO_LIQUID_DISPENSE);
            }
                break;
            default:
                throw new Error();
        }
    } else {
        throw new Error;
    }
}

void Macro::CreateSubsystemRequestForMacroRequest(HardwareMessage hardwareMessage,
                                                  SubsystemRequest *subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(HARDWARE_REQUEST);
    subsystemRequest->SetFrequency(1);
    switch (hardwareMessage.GetId()) {
        case MessageConstants::Macro_Pot_1: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_1);
        }
            break;
        case MessageConstants::Macro_Pot_2: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_2);
        }
            break;
        case MessageConstants::Macro_Pot_3: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_3);
        }
            break;
        case MessageConstants::Macro_Pot_4: {
            subsystemRequest->SetSubsystemComponent(MACRO_CONTAINER_4);
        }
            break;
        case MessageConstants::Pan_Loadcell: {
            subsystemRequest->SetSubsystemComponent(PAN_WEIGHT_SENSOR);
            if(hardwareMessage.GetValueMap()->find(MessageConstants::Secondary)!=hardwareMessage.GetValueMap()->end()){
                if(hardwareMessage.GetValueMap()->at(MessageConstants::Secondary)==MessageConstants::Grams){
                    subsystemRequest->SetSensorUnit(GRAMS);
                }
                else{
                    subsystemRequest->SetSensorUnit(MICRO_VOLTS);
                }
            }
            return;
        }
        default:
            throw new Error;
    }
}

*/

void Macro::PopulateComponentSequenceMap() {

}
