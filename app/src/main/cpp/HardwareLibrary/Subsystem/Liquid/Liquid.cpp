//
// Created by fedal on 21/12/20.
//

#include "Liquid.h"
/*
#include "../SubystemSequenceBehavior/LiquidDispenseBehaviors/WaterDispenseBehavior/LiquidWaterDispenseBehavior.h"
#include "../SubystemSequenceBehavior/LiquidDispenseBehaviors/OilDispenseBehavior/LiquidOilDispenseBehavior.h"

bool Liquid::ResetSubsystem() throw(Error) {
    return false;
}

void Liquid::PopulateComponentMap() {

    SubsystemComponent *water_container;
    water_container = new SubsystemComponent(WATER_CONTAINER);
    this->component_map_.insert({WATER_CONTAINER,water_container});
    water_container->AddToComponentSequenceMap(LIQUID_WATER_DISPENSE,new LiquidWaterDispenseBehavior(LIQUID_WATER_DISPENSE));

    SubsystemComponent *oil_container;
    oil_container = new SubsystemComponent(OIL_CONTAINER);
    oil_container->AddToComponentSequenceMap(LIQUID_OIL_DISPENSE,new LiquidOilDispenseBehavior(LIQUID_OIL_DISPENSE));
    this->component_map_.insert({OIL_CONTAINER,oil_container});

}

void Liquid::CreateSubsystemRequestForLiquidCommand(HardwareMessage hardwareMessage, SubsystemRequest *subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(HARDWARE_REQUEST);
    switch (hardwareMessage.GetId()){
        case MessageConstants::Liquid_Water_Pump:{
            subsystemRequest->SetSubsystemComponent(WATER_CONTAINER);
        }
        break;
        case MessageConstants::Liquid_Oil_Pump:{
            subsystemRequest->SetSubsystemComponent(OIL_CONTAINER);
        }
        break;
    }
    if (hardwareMessage.GetValueMap()->find(MessageConstants::Primary) !=
        hardwareMessage.GetValueMap()->end()) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Micro primary val %d",hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
        subsystemRequest->SetQuantity(hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
    } else {
        throw new Error;
    }
    __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "exit create subsystem request");
}

void Liquid::CreateSubsystemRequestForLiquidAction(HardwareMessage hardwareMessage, SubsystemRequest *subsystemRequest) throw(Error) {
    subsystemRequest->SetRequestType(SEQUENCE_REQUEST);
    switch (hardwareMessage.GetId()){
        case MessageConstants::Liquid_Water_Pump:{
            subsystemRequest->SetSubsystemComponent(WATER_CONTAINER);
            subsystemRequest->SetSubsystemSequence(LIQUID_WATER_DISPENSE);
        }
            break;
        case MessageConstants::Liquid_Oil_Pump:{
            subsystemRequest->SetSubsystemComponent(OIL_CONTAINER);
            subsystemRequest->SetSubsystemSequence(LIQUID_OIL_DISPENSE);
        }
            break;
    }

    if (hardwareMessage.GetValueMap()->find(MessageConstants::Primary) !=
        hardwareMessage.GetValueMap()->end()) {
        subsystemRequest->SetQuantity(hardwareMessage.GetValueMap()->at(MessageConstants::Primary));
    } else {
        throw new Error;
    }
}

void Liquid::CreateSubsystemRequestForLiquidRequest(HardwareMessage hardwareMessage, SubsystemRequest *subsystemRequest) throw(Error) {
    //TODO
}
*/

void Liquid::PopulateComponentSequenceMap() {

}
