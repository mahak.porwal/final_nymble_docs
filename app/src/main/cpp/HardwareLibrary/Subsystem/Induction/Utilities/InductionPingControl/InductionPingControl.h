//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_INDUCTIONPINGCONTROL_H
#define JULIA_ANDROID_INDUCTIONPINGCONTROL_H


#include "../../../../Error/Error.h"

class InductionPingControl {
private:
    bool IsPingRequired(float current, float duty_cycle);

    bool PerformPingingAction();

    float current_threshold_;
    float duty_cycle_threshold_;
    volatile uint8_t continuous_ping_retry_;
    uint8_t max_continuous_ping_retry_;
public:
    InductionPingControl(float current_threshold, float duty_cycle_threshold,
                         uint8_t max_continuous_ping_retry) {
        this->current_threshold_ = current_threshold;
        this->duty_cycle_threshold_ = duty_cycle_threshold;
        this->max_continuous_ping_retry_ = max_continuous_ping_retry;
        this->continuous_ping_retry_ = 0;
    }

    bool PingControl(float current, float duty_cycle);
};


#endif //JULIA_ANDROID_INDUCTIONPINGCONTROL_H
