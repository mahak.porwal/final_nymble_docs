//
// Created by fedal on 25/12/20.
//

#include "InductionPingControl.h"

bool InductionPingControl::IsPingRequired(float current, float duty_cycle) {
    return current < this->current_threshold_ && duty_cycle > this->duty_cycle_threshold_;
}

bool InductionPingControl::PerformPingingAction() {
    if (this->continuous_ping_retry_ > this->max_continuous_ping_retry_) {
        this->continuous_ping_retry_ = 0;
        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ", "pan detection failure");
        /**
         * Throw pan not detected
         */
        //throw *(new Error(PAN_NOT_DETECTED));
        return false;
    } else {
        int result = -1;
        this->continuous_ping_retry_++;
        ComponentRequestBuilder component_request_builder;
        component_request_builder.component_request_.SetComponent(INDUCTION_PAN_GPIO).SetState(
                OFF_TO_ON_STATE);
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "pinging %d ", result);

        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ", "num of retries %d ",
                            this->continuous_ping_retry_);

        auto result_map = HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(component_request_builder.component_request_);
        result = stoi(result_map.at(COMMAND_RESULT).at(0));

        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "pinging %d ", result);
    }
    return true;
}

bool InductionPingControl::PingControl(float current, float duty_cycle) {
    if (this->IsPingRequired(current, duty_cycle)) {
        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ", "pan detection required");
        ComponentRequestBuilder component_request_builder;
        component_request_builder.component_request_.SetRequestType(HARDWARE_REQUEST).SetComponent(
                INDUCTION_INT_GPIO).SetState(ON_STATE);
        HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_);
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "gpio result ");
        usleep(200);
        __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "gpio result ");

        return this->PerformPingingAction();
    } else {
        this->continuous_ping_retry_ = 0;
    }
    return true;
}
