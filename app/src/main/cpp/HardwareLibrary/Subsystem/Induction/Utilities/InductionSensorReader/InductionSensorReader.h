//
// Created by fedal on 25/7/21.
//

#ifndef RECIPEENGINE_INDUCTIONSENSORREADER_H
#define RECIPEENGINE_INDUCTIONSENSORREADER_H

#include "../../../../InductionControl/InductionSensor/InductionSensorBuilder.h"
#include "../../../../HardwareServiceFactory/HardwareServiceFactory.h"

class InductionSensorReader {

private:
    InductionSensorBuilder induction_sensor_builder_;

public:
    InductionSensor PollSensors() {
        double voltage = -1;
        ComponentRequestBuilder component_request_builder;
        component_request_builder.component_request_.SetRequestType(
                HARDWARE_REQUEST).SetComponent(INDUCTION_AC_VOLTAGE_SENSOR).SetFrequency(1);
        std::string sensor_value = HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
        voltage = std::stod(sensor_value.c_str());

        double current = -1;

        component_request_builder.component_request_.SetRequestType(
                HARDWARE_REQUEST).SetComponent(INDUCTION_AC_CURRENT_SENSOR).SetFrequency(1);
        sensor_value = HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
        current = std::stod(sensor_value.c_str());

        double igbt_temperature = -1;

        component_request_builder.component_request_.SetRequestType(
                HARDWARE_REQUEST).SetComponent(INDUCTION_IGBT_TEMPERATURE_SENSOR).SetFrequency(1);
        sensor_value = HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
        igbt_temperature = std::stod(sensor_value.c_str());


        double pan_temperature = -1;

        component_request_builder.component_request_.SetRequestType(
                HARDWARE_REQUEST).SetComponent(INDUCTION_PAN_TEMPERATURE_SENSOR).SetFrequency(1);
        sensor_value = HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
        pan_temperature = std::stof(sensor_value.c_str());

        __android_log_print(ANDROID_LOG_ERROR, "Chef  : native",
                            "igbt temp %f \npan temp %f \ncurrent %f \n voltage %f",
                            igbt_temperature, pan_temperature,
                            current, voltage);

        return induction_sensor_builder_.SetInputCurrent(current).SetInputVoltage(voltage).SetIgbtTemperature(
                igbt_temperature).SetPanTemperature(pan_temperature).Build();
    }
};


#endif //RECIPEENGINE_INDUCTIONSENSORREADER_H
