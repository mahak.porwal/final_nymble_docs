//
// Created by fedal on 27/7/21.
//

#include "PanDetection.h"

bool PanDetection::DetectPan() {
    while (true) {

        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ", "while loop pinging");
        //set pwm duty cycle
        ComponentRequestBuilder component_request_builder;
        component_request_builder.component_request_.SetComponent(
                INDUCTION_ACTUATOR).SetSpeed(duty_cycle);

        HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_);

        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ", "duty cycle set");

        //turn on INT pin
        component_request_builder.component_request_.SetComponent(
                INDUCTION_INT_GPIO).SetState(ON_STATE);
        HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_);

        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ", "induction INT on");

        //get induction current
        component_request_builder.component_request_.SetRequestType(
                HARDWARE_REQUEST).SetComponent(INDUCTION_AC_CURRENT_SENSOR).SetFrequency(1);
        auto sensor_value = HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_).at(SENSOR_VALUE).at(0);

        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ", "induction current sensed");

        float induction_current = std::stof(sensor_value.c_str());

        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ", "ping action");

        if (!induction_ping_control_->PingControl(induction_current, duty_cycle)) {
            //turn off INT pin
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    INDUCTION_INT_GPIO).SetState(OFF_STATE);
            HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    component_request_builder.component_request_);
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ",
                                "pan detection error caught pan not detected");
            return false;
        }

        /*try {
            induction_ping_control_->PingControl(induction_current,
                                                 duty_cycle);
        } catch (Error &error) {
            //turn off INT pin
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    INDUCTION_INT_GPIO).SetState(OFF_STATE);
            HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    component_request_builder.component_request_);

            if (error.GetError() == PAN_NOT_DETECTED) {
                __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "pan detection error caught pan not detected");
                return false;
            } else {
                throw error;
            }
        }*/
        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ",
                            "ping action completed, sleep for 200ms");

        usleep(200000);

        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ",
                            "induction current sense start");
        component_request_builder.component_request_.SetRequestType(
                HARDWARE_REQUEST).SetComponent(INDUCTION_AC_CURRENT_SENSOR).SetFrequency(1);
        sensor_value = HardwareServiceFactory::GetHardwareService()->
                ProcessComponentRequest(
                component_request_builder.component_request_).at(SENSOR_VALUE).at(0);
        induction_current = std::stof(sensor_value.c_str());

        __android_log_print(ANDROID_LOG_ERROR, "Pan detection  : ", "induction current sensed");

        if (IsPanDetected(induction_current)) {
            ComponentRequestBuilder component_request_builder;
            component_request_builder.component_request_.SetComponent(
                    INDUCTION_INT_GPIO).SetState(OFF_STATE);
            HardwareServiceFactory::GetHardwareService()->
                    ProcessComponentRequest(
                    component_request_builder.component_request_);
            return true;
        }
    }
}

bool PanDetection::IsPanDetected(float induction_current) {
    if (induction_current > current_threshold) {
        return true;
    }
    return false;
}