//
// Created by fedal on 25/12/20.
//

#include "InductionControlPid.h"

double InductionControlPid::PidLoop() {
    this->error_ = this->target_value_ - this->current_value_;
    this->proportional_ = this->error_;
    this->derivative_ = this->error_ - this->last_error_;
    this->integral_ += this->error_;
    this->output_ = (this->proportional_ * this->kp_ + this->derivative_ * this->kd_ +
                     this->integral_ * this->ki_);
    this->last_error_ = this->error_;
    return this->output_;

}

void InductionControlPid::ResetPid() {
    this->integral_ = 0;
}

void InductionControlPid::SetTargetValue(int targetValue) {
    target_value_ = targetValue;
}

void InductionControlPid::SetCurrentValue(int currentValue) {
    current_value_ = currentValue;
}
