
//
// Created by fedal on 27/7/21.
//

#ifndef RECIPEENGINE_PANDETECTION_H
#define RECIPEENGINE_PANDETECTION_H


class PanDetection {
private:
    InductionPingControl *induction_ping_control_;
    const float duty_cycle = 55;
    const uint8_t max_retry = 4;
    const float current_threshold = 2;

    bool IsPanDetected(float);

public:
    PanDetection() {
        this->induction_ping_control_ = new InductionPingControl(current_threshold, duty_cycle - 5,
                                                                 max_retry);
    }

    bool DetectPan();

    ~PanDetection() {
        delete this->induction_ping_control_;
    }
};


#endif //RECIPEENGINE_PANDETECTION_H
