//
// Created by fedal on 25/12/20.
//

#ifndef JULIA_ANDROID_INDUCTIONCONTROLPID_H
#define JULIA_ANDROID_INDUCTIONCONTROLPID_H


class InductionControlPid {
private:
    double ki_;
    double kd_;
    double kp_;

    double error_;
    double last_error_;
    int target_value_;
    int current_value_;
    double derivative_;
    double proportional_;
    double integral_;
    double output_;
public:
    InductionControlPid(double kp, double ki, double kd) {
        this->ki_ = ki;
        this->kp_ = kp;
        this->kd_ = kd;
        this->error_ = 0;
        this->last_error_ = 0;
        this->target_value_ = 0;
        this->current_value_ = 0;
        this->derivative_= 0;
        this->proportional_ = 0;
        this->integral_ = 0;
    }

    void SetTargetValue(int targetValue);

    void SetCurrentValue(int currentValue);

    double PidLoop();
    void ResetPid();
};


#endif //JULIA_ANDROID_INDUCTIONCONTROLPID_H
