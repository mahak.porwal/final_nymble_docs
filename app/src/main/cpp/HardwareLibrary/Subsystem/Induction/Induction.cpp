//
// Created by fedal on 21/12/20.
//

#include "Induction.h"
/*
#include "../SubystemSequenceBehavior/InductionSequenceBehaviors/PowerControlBehaviors/PowerLevelFiveControlBehavior/PowerLevelFiveControlBehavior.h"
#include "../SubystemSequenceBehavior/InductionSequenceBehaviors/PowerControlBehaviors/PowerLevelOneControlBehavior/PowerLevelOneControlBehavior.h"
#include "../SubystemSequenceBehavior/InductionSequenceBehaviors/PowerControlBehaviors/PowerLevelTwoControlBehavior/PowerLevelTwoControlBehavior.h"
#include "../SubystemSequenceBehavior/InductionSequenceBehaviors/PowerControlBehaviors/PowerLevelZeroControlBehavior/PowerLevelZeroControlBehavior.h"
#include "../SubystemSequenceBehavior/InductionSequenceBehaviors/PowerControlBehaviors/PowerLevelSevenControlBehavior/PowerLevelSevenControlBehavior.h"
#include "../SubystemSequenceBehavior/InductionSequenceBehaviors/PowerControlBehaviors/PowerLevelFourControlBehavior/PowerLevelFourControlBehavior.h"
#include "../SubystemSequenceBehavior/InductionSequenceBehaviors/PowerControlBehaviors/PowerLevelSixControlBehavior/PowerLevelSixControlBehavior.h"
#include "../SubystemSequenceBehavior/InductionSequenceBehaviors/PowerControlBehaviors/PowerLevelThreeControlBehavior/PowerLevelThreeBehavior.h"
#include "../SubystemSequenceBehavior/InductionSequenceBehaviors/IdleBehavior/IdleBehavior.h"

bool Induction::ResetSubsystem() throw(Error) {
    return false;
}

void Induction::PopulateComponentMap() {
    SubsystemComponent *subsystemComponent = new SubsystemComponent(INDUCTION_HEATER);
    this->component_map_.insert({INDUCTION_HEATER, subsystemComponent});
    subsystemComponent->AddToComponentSequenceMap(INDUCTION_POWER_LEVEL_1,
                                                  new PowerLevelOneControlBehavior(INDUCTION_POWER_LEVEL_1));
    subsystemComponent->AddToComponentSequenceMap(INDUCTION_POWER_LEVEL_5,
                                                  new PowerLevelFiveControlBehavior(
                                                          INDUCTION_POWER_LEVEL_5));
    subsystemComponent->AddToComponentSequenceMap(INDUCTION_POWER_LEVEL_2,
                                                  new PowerLevelTwoControlBehavior(
                                                          INDUCTION_POWER_LEVEL_2));
    subsystemComponent->AddToComponentSequenceMap(INDUCTION_POWER_LEVEL_3,
                                                  new PowerLevelThreeBehavior(
                                                          INDUCTION_POWER_LEVEL_3));
    subsystemComponent->AddToComponentSequenceMap(INDUCTION_POWER_LEVEL_4,
                                                  new PowerLevelFourControlBehavior(
                                                          INDUCTION_POWER_LEVEL_4));
    subsystemComponent->AddToComponentSequenceMap(INDUCTION_POWER_LEVEL_6,
                                                  new PowerLevelSixControlBehavior(
                                                          INDUCTION_POWER_LEVEL_6));
    subsystemComponent->AddToComponentSequenceMap(INDUCTION_POWER_LEVEL_7,
                                                  new PowerLevelSevenControlBehavior(
                                                          INDUCTION_POWER_LEVEL_7));

    subsystemComponent->AddToComponentSequenceMap(INDUCTION_POWER_LEVEL_0,
                                                  new PowerLevelZeroControlBehavior(
                                                          INDUCTION_POWER_LEVEL_0));

    subsystemComponent->AddToComponentSequenceMap(INDUCTION_IDLE_BEHAVIOR,
                                                  new IdleBehavior(
                                                          INDUCTION_IDLE_BEHAVIOR));


    subsystemComponent = new SubsystemComponent(INDUCTION_ACTUATOR_COMPONENT);
    this->component_map_.insert({INDUCTION_ACTUATOR_COMPONENT,subsystemComponent});

    subsystemComponent = new SubsystemComponent(INDUCTION_INT);
    this->component_map_.insert({INDUCTION_INT,subsystemComponent});

    subsystemComponent = new SubsystemComponent(INDUCTION_PAN);
    this->component_map_.insert({INDUCTION_PAN,subsystemComponent});

    subsystemComponent = new SubsystemComponent(INDUCTION_FAN);
    this->component_map_.insert({INDUCTION_FAN,subsystemComponent});

    subsystemComponent = new SubsystemComponent(INDUCTION_IGBT_TEMP);
    this->component_map_.insert({INDUCTION_IGBT_TEMP,subsystemComponent});
    subsystemComponent = new SubsystemComponent(INDUCTION_PAN_TEMP);
    this->component_map_.insert({INDUCTION_PAN_TEMP,subsystemComponent});
    subsystemComponent = new SubsystemComponent(INDUCTION_VOLTAGE);
    this->component_map_.insert({INDUCTION_VOLTAGE,subsystemComponent});
    subsystemComponent = new SubsystemComponent(INDUCTION_CURRENT);
    this->component_map_.insert({INDUCTION_CURRENT,subsystemComponent});

}

void Induction::CreateSubsystemRequestForInductionCommand(HardwareMessage hardwareMessage, SubsystemRequest * subsystemRequest) throw(Error) {

    subsystemRequest->SetRequestType(HARDWARE_REQUEST);
    switch (hardwareMessage.GetId()){
        case MessageConstants::Induction_Pan:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_PAN);
        }
            break;
        case MessageConstants::Induction_Fan:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_FAN);
        }
        break;
        case MessageConstants::Induction_Int:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_INT);
        }
        break;
        case MessageConstants::Induction_Buzzer:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_BUZZER);
        }
        break;
        case MessageConstants::Induction_Power:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_ON_OFF);
        }
        break;
        case MessageConstants::Induction_Pwm_Value:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_ACTUATOR_COMPONENT);
        }
        break;
        default:
            throw new Error;
    }
    switch (hardwareMessage.GetId()){
        case MessageConstants::Induction_Fan:
        case MessageConstants::Induction_Int:
        case MessageConstants::Induction_Buzzer:
        case MessageConstants::Induction_Pan:
        case MessageConstants::Induction_Power:{
            if(hardwareMessage.GetValueMap()->find(MessageConstants::Primary)==hardwareMessage.GetValueMap()->end()){
                throw new Error;
            }
            int state = hardwareMessage.GetValueMap()->at(MessageConstants::Primary);
            subsystemRequest->SetState(state?ON_STATE:OFF_STATE);
        }
        break;
        case MessageConstants::Induction_Pwm_Value:{
            if(hardwareMessage.GetValueMap()->find(MessageConstants::Primary)==hardwareMessage.GetValueMap()->end()){
                throw new Error;
            }
            int value = hardwareMessage.GetValueMap()->at(MessageConstants::Primary);
            subsystemRequest->SetSpeed(value);
        }
        break;
        default:break;
    }

}

void Induction::CreateSubsystemRequestForInductionAction(HardwareMessage hardwareMessage, SubsystemRequest * subsystemRequest) throw(Error) {

    subsystemRequest->SetRequestType(SEQUENCE_REQUEST);
    subsystemRequest->SetSubsystemComponent(INDUCTION_HEATER);
    switch (hardwareMessage.GetId()) {
        case MessageConstants::Set_Power_Level_0: {
            __android_log_print(ANDROID_LOG_ERROR, "Chef  : ", "Micro Id 1 ");
            subsystemRequest->SetSubsystemSequence(INDUCTION_POWER_LEVEL_0);
        }
            break;
        case MessageConstants::Set_Power_Level_1: {
            subsystemRequest->SetSubsystemSequence(INDUCTION_POWER_LEVEL_1);
        }
            break;
        case MessageConstants::Set_Power_Level_2: {
            subsystemRequest->SetSubsystemSequence(INDUCTION_POWER_LEVEL_2);
        }
            break;
        case MessageConstants::Set_Power_Level_3: {
            subsystemRequest->SetSubsystemSequence(INDUCTION_POWER_LEVEL_3);
        }
            break;
        case MessageConstants::Set_Power_Level_4: {
            subsystemRequest->SetSubsystemSequence(INDUCTION_POWER_LEVEL_4);;
        }
            break;
        case MessageConstants::Set_Power_Level_5: {
            subsystemRequest->SetSubsystemSequence(INDUCTION_POWER_LEVEL_5);
        }
            break;
        case MessageConstants::Set_Power_Level_6: {
            subsystemRequest->SetSubsystemSequence(INDUCTION_POWER_LEVEL_6);
        }
            break;
        case MessageConstants::Set_Power_Level_7: {
            subsystemRequest->SetSubsystemSequence(INDUCTION_POWER_LEVEL_7);
        }
            break;
        case MessageConstants::Set_Power_Level_Idle: {
            subsystemRequest->SetSubsystemSequence(INDUCTION_IDLE_BEHAVIOR);
        }
            break;
        default:
            throw new Error;
    }

}

void Induction::CreateSubsystemRequestForInductionRequest(HardwareMessage hardwareMessage, SubsystemRequest * subsystemRequest) throw(Error) {

    subsystemRequest->SetRequestType(HARDWARE_REQUEST);
    switch (hardwareMessage.GetId()){
        case MessageConstants::Induction_Current:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_CURRENT);
        }
            break;
        case MessageConstants::Induction_Voltage:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_VOLTAGE);
        }
            break;
        case MessageConstants::Induction_Igbt_temp:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_IGBT_TEMP);
        }
            break;
        case MessageConstants::Induction_Pan_temp:{
            subsystemRequest->SetSubsystemComponent(INDUCTION_PAN_TEMP);
        }
            break;
        default:
            throw new Error;
    }
    subsystemRequest->SetFrequency(1);
}
*/

void Induction::PopulateComponentSequenceMap() {

}
