//
// Created by Abarajithan on 2019-11-21.
//

#include<jni.h>
#include <string>
#include <android/log.h>
#include "i2c/i2c_driver/i2c_driver.cpp"
#include "i2c/i2c_bus/i2c_bus.cpp"

#include "HardwareLibrary/Error/Error.h"
#include "HardwareLibrary/Subsystem/Subsystem.h"
#include "HardwareLibrary/Component/Component.h"
#include "HardwareLibrary/CommunicationBridge/RequestExecutor/ComponentReqCreator/ComponentReqCreator.cpp"
#include "HardwareLibrary/CommunicationBridge/RequestExecutor/ComponentReqCreator/StringToEnums.h"
#include "HardwareLibrary/CommunicationBridge/RequestExecutor/ComponentResponseCreator/CompResponseCreator.cpp"
#include "HardwareLibrary/CommunicationBridge/RequestExecutor/ComponentResponseCreator/EnumsToString.h"
#include "HardwareLibrary/Component/Status/ComponentStatus.h"

#include "HardwareLibrary/Subsystem/SequenceBehavior/LiquidDispenseBehaviors/PID/PID.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/SequenceBehavior.cpp"
#include "HardwareLibrary/HardwareService/SequenceExecutor/SequenceExecutorFactory.h"
#include "HardwareLibrary/InductionControl/ErrorDetector/BaseErrorDetector/BaseErrorDetector.cpp"
#include "HardwareLibrary/InductionControl/ErrorResolver/BaseErrorResolver/BaseErrorResolver.cpp"
#include "HardwareLibrary/InductionControl/PowerController/BasePowerController/BasePowerController.cpp"
#include "HardwareLibrary/InductionControl/PowerController/BasePowerControllerV2/BasePowerControllerV2.cpp"
#include "HardwareLibrary/InductionControl/BaseStateMachine/BaseStateMachine.cpp"
#include "HardwareLibrary/InductionControl/BaseInductionController/BaseInductionController.cpp"
#include "HardwareLibrary/InductionControl/InductionController.h"
#include "HardwareLibrary/InductionControl/PowerController/PowerControlBehavior/PidPowerController/PidPowerController.cpp"
#include "HardwareLibrary/InductionControl/PowerController/PowerControlBehavior/PidPowerController/PowerLevelFour.h"
#include "HardwareLibrary/InductionControl/PowerController/PowerControlBehavior/PidPowerController/PowerLevelOne.h"
#include "HardwareLibrary/InductionControl/InductionControllerFactory.h"

#include "HardwareLibrary/Subsystem/SequenceBehavior/MacroSequenceBehaviors/MacroLiquidDispenseBehavior/MacroLiquidDispenseBehavior.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/MacroSequenceBehaviors/MacroNonLiquidDispenseBehavior/MacroNonLiquidDispenseBehavior.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/MacroSequenceBehaviors/MacroContainerBehavior/MacroContainerBehavior.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/MacroSequenceBehaviors/MacroResetBehavior/MacroResetBehavior.cpp"

#include "HardwareLibrary/Subsystem/SequenceBehavior/MicroSequenceBehaviors/MicroClockwiseDispenseBehavior/MicroClockwiseDispenseBehavior.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/MicroSequenceBehaviors/MicroPodBehavior/MicroPodBehavior.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/MicroSequenceBehaviors/MicroAntiClockwiseDispenseBehavior/MicroAntiClockwiseDispenseBehavior.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/LiquidDispenseBehaviors/OilDispenseBehavior/LiquidOilDispenseBehavior.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/LiquidDispenseBehaviors/WaterDispenseBehavior/LiquidWaterDispenseBehavior.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/LiquidDispenseBehaviors/Utililty/Utility.h"
#include "HardwareLibrary/Subsystem/SequenceBehavior/StirrerSequenceBehaviors/StirrerTimeBasedSequence/StirrerTimeBasedSequence.cpp"
#include "HardwareLibrary/Subsystem/SequenceBehavior/MicroSequenceBehaviors/Potentiometer/Potentiometer.cpp"
#include "HardwareLibrary/Subsystem/Induction/Utilities/InductionControlPid/InductionControlPid.cpp"
#include "HardwareLibrary/Subsystem/Induction//Utilities/InductionPingControl/InductionPingControl.cpp"
#include "HardwareLibrary/Subsystem/Induction//Utilities/PanDetection/PanDetection.cpp"


#include "HardwareLibrary/BaseHardwareService/BaseHardwareService.cpp"
#include "HardwareLibrary/HardwareService/HardwareService.cpp"
#include "HardwareLibrary/DummyHardwareService/DummyHardwareService.cpp"
#include "HardwareLibrary/Component/Request/ComponentRequest.cpp"
#include "HardwareLibrary/Component/Request/ComponentRequestBuilder.h"
#include "HardwareLibrary/HardwareService/SequenceExecutor/SequenceExecutor.cpp"
#include "HardwareLibrary/InductionControl/ControlInputProcessor.h"

#include "HardwareLibrary/HardwareService/HardwareComponent/HardwareComponent.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Multiplexer/MuxEightIsToOne/MacroMux/MacroMux.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Multiplexer/MuxEightIsToOne/MicroMux/MicroMux.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Multiplexer/MuxTwoIsToOne/MuxTwoIsToOne.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Servo360Degree/Servo360Degree.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Servo180Degree/Servo180Degree.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Servo360Macro/Servo360Macro.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/StirrerMotor/StirrerMotor.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Pump/Pump.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/LedActuator/LedActuator.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/ExhaustFan/ExhaustFan.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Buzzer/Buzzer.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/GpioControlOutput/GpioControlOutput.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/ThermalCamera/ThermalEepromData.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/DigitalPot/DigitalPot.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/DigitalPot/MCP4662/MCP4662.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Counter/Counter.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Counter/ExhaustFanCounter/ExhaustFanCounter.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Counter/WaterFlowCounter/WaterFlowCounter.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/Counter/OilFlowCounter/OilFlowCounter.cpp"

#include "HardwareLibrary/HardwareService/HardwareComponent/GpioControlInput/GpioControlInput.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/AnalogFeedback.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/InductionActuator/InductionActuator.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/InductionHeater/InductionHeater.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/InductionAnalogSensors/IgbtTemperature/IgbtTemperature.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/InductionAnalogSensors/PanTemperature/PanTemperature.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/InductionAnalogSensors/InputCurrent/InputCurrent.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/InductionAnalogSensors/InputVoltage/InputVoltage.cpp"

#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/MicroFeedback/MicroFeedback.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/MacroFeedback/MacroFeedback.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/Macro360Feedback/Macro360Feedback.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/LoadcellSensor/LoadcellSensor.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/StirrerCurrent/StirrerCurrent.cpp"
#include "HardwareLibrary/HardwareService/HardwareComponent/AnalogFeedback/TemperatureFeedback/TemperatureFeedback.cpp"

#include "HardwareLibrary/HardwareService/Peripherals/I2C/I2CBus/I2CBus.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/I2C/I2C.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/I2C/I2CRequestProcessor/I2CRequestProcessor.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/I2C/I2CRequest/I2CRequest.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/I2C/I2CRequest/I2CRequestBuilder.h"


#include "HardwareLibrary/HardwareService/Peripherals/Pwm/Pwm.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Pwm/PwmRequest/PwmRequest.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Pwm/PwmRequest/PwmRequestBuilder.h"
#include "HardwareLibrary/HardwareService/Peripherals/Pwm/PwmRequestProcessor/PwmRequestProcessor.h"

#include "HardwareLibrary/HardwareService/Peripherals/Adc/Adc.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Adc/AdcRequestProcessor/AdcRequestProcessor.h"
#include "HardwareLibrary/HardwareService/Peripherals/Adc/AdcRequest/AdcRequest.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Adc/AdcRequest/AdcRequestBuilder.h"


#include "HardwareLibrary/HardwareService/Peripherals/Gpio/Gpio.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Gpio/GpioRequestProcessor/GpioRequestProcessor.h"
#include "HardwareLibrary/HardwareService/Peripherals/Gpio/GpioRequest/GpioRequest.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Gpio/GpioRequest/GpioRequestBuilder.h"


#include "HardwareLibrary/HardwareService/Peripherals/Pwm/Devices/PCA9685/Pca9685.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Pwm/Devices/DS1050Z/DS1050Z.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Pwm/Devices/MAX6650/MAX6650.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Pwm/Devices/MCP4725/MCP4725.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Gpio/Devices/PCA9535/Pca9535.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Adc/Devices/INA219B/INA219B.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Adc/Devices/ADS122C04/ADS122C04.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Adc/Devices/ADS1115/ADS1115.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Adc/Devices/MAX11607/MAX11607.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Adc/Devices/MCP3426/MCP3426.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Adc/Devices/SOM_Adc/SOM_Adc.cpp"
#include "HardwareLibrary/HardwareService/Peripherals/Gpio/Devices/SOM_Gpio/SOM_Gpio.cpp"

#include "HardwareLibrary/HardwareService/Peripherals/Eeprom/Eeprom.h"


#include "HardwareLibrary/CommunicationBridge/ResponseParameters.h"
#include "HardwareLibrary/CommunicationBridge/RequestExecutor/RequestExecutor.h"
#include "HardwareLibrary/CommunicationBridge/RequestExecutor/Base/BaseRequestExecutor.cpp"
#include "HardwareLibrary/CommunicationBridge/RequestExecutor/HwActionExecutor/HwActionExecutor.h"
#include "HardwareLibrary/CommunicationBridge/RequestExecutor/SensorExecutor/SensorExecutor.h"
#include "HardwareLibrary/Component/Component.h"

#include "food_detect/foodDetect.cpp"
#include "food_detect/temp_params_calculator/TempParamsCalc.cpp"
#include "food_detect/temp_params_calculator/TempParamsCalcFactory.h"

extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_StartInductionControl(JNIEnv *env,
                                                                           jclass clazz) {
    BaseHardwareService *hardware_service = HardwareServiceFactory::GetHardwareService();
    hardware_service->StartInductionControl();
}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_InitThermalCamera(JNIEnv *env,
                                                                       jclass clazz) {
    BaseHardwareService *hardware_service = HardwareServiceFactory::GetHardwareService();
    hardware_service->InitThermalCamera();
}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_SetInductionControlInput(JNIEnv *env,
                                                                              jclass clazz) {
    BaseHardwareService *hardware_service = HardwareServiceFactory::GetHardwareService();
    hardware_service->StartInductionControl();
}
extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_ExecuteHWActionRequest(JNIEnv *env,
                                                                            jclass clazz,
                                                                            jobject hw_action_request) {
    RequestExecutor *executor = new HwActionExecutor(env);
    return executor->Execute(hw_action_request);
}extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_ExecuteSensorRequest(JNIEnv *env, jclass clazz,
                                                                          jobject sensor_request) {
    RequestExecutor *sensor_executor = new SensorExecutor(env);
    return sensor_executor->Execute(sensor_request);
}extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_StartHardwareService(JNIEnv *env,
                                                                          jclass clazz) {
    HardwareServiceFactory::GetHardwareService();
}extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_StartDummyHardwareService(JNIEnv *env,
                                                                               jclass clazz) {
    // TODO: implement StartDummyHardwareService()
    HardwareServiceFactory::GetHardwareService();
}extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_StopHardwareService(JNIEnv *env,
                                                                         jclass clazz) {
    // TODO: implement StopHardwareService()
    HardwareServiceFactory::StopHardwareService();
}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_CreateThermalImage(JNIEnv *env, jclass clazz,
                                                                        jdoubleArray temperature_matrix) {
    jdouble *temperature_mat = env->GetDoubleArrayElements(temperature_matrix, nullptr);
    CreateThermalImage(temperature_mat);
}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_PopulateThermalImage(JNIEnv *env, jclass clazz,
                                                                          jlong address) {
    PopulateThermalImage(static_cast<long>(address));
}
extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_CalculateFoodTemperature(JNIEnv *env,
                                                                              jclass clazz,
                                                                              jlong address) {
    TempParamsCalc *temp_params_calc =
            TempParamsCalcFactory::GetTemperatureParamsCalculator(env);
    auto *inputImage = (cv::Mat *) address;
    return temp_params_calc->CalculateAndGetFoodTempParams(inputImage);
}
extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_CalculatePanTemperature(JNIEnv *env,
                                                                             jclass clazz,
                                                                             jlong address) {
    // TODO: implement CalculatePanTemperature()
    TempParamsCalc *temp_params_calc =
            TempParamsCalcFactory::GetTemperatureParamsCalculator(env);
    auto *inputImage = (cv::Mat *) address;
    return temp_params_calc->CalculateAndGetPanTempParams(inputImage);
}
extern "C"
JNIEXPORT jobject JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_ScanI2CBus(JNIEnv *env, jclass clazz,
                                                                jint bus) {
    __android_log_print(ANDROID_LOG_ERROR, "Chef", "Device  ");
    std::vector<std::string> devices = I2CRequestProcessor::ScanBus(bus);
    auto *sensor_executor = new SensorExecutor(env);
    return sensor_executor->CreateI2CHashMap(devices);
}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_setLoadCellScalingFactor(JNIEnv *env,
                                                                              jclass clazz,
                                                                              jdouble scaling_factor) {
    // TODO: implement setLoadCellScalingFactor()
    LoadcellSensor::SCALING_FACTOR = scaling_factor;
}
extern "C"
JNIEXPORT double JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_getLoadCellScalingFactor(JNIEnv *env,
                                                                              jclass clazz) {
    // TODO: implement getLoadCellScalingFactor()
    return LoadcellSensor::SCALING_FACTOR;
}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_StopInductionControl(JNIEnv *env,
                                                                          jclass clazz) {
    // TODO: implement StopInductionControl()
    BaseHardwareService *hardware_service = HardwareServiceFactory::GetHardwareService();
    hardware_service->StopInductionControl();
}
extern "C"
JNIEXPORT void JNICALL
Java_com_example_recipeengine_util_Native_NativeUtil_populateHardwareFactors(JNIEnv *env,
                                                                             jclass clazz,
                                                                             jobject hardware_factors) {
    HardwareService::PopulateHardwareFactors(env, hardware_factors);
}