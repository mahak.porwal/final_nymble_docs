//
// Created by fedal on 2/11/20.
//
#include "MCP3021.h"

uint16_t readMCP3021(void){

    uint8_t receivedArray[2];
    uint8_t received_bytes = i2c_read(file_descriptor_i2c2,MCP3021_ADDRESS,2,receivedArray);
    __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","MCP read %d bytes",received_bytes);
    uint16_t raw_val = 0x0000;
    //Timber.d("REceived data %d ", raw_val);
    receivedArray[1] &= ~(0x03);
    receivedArray[1] >>= 2;
    receivedArray[0] &= (0x0F);
    raw_val = receivedArray[1]|(receivedArray[0]<<6);
    __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","MCP read raw value %d",raw_val);
   // __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","MCP read upper %d, lower %d",receivedArray[0],receivedArray[1]);
    return raw_val;
}