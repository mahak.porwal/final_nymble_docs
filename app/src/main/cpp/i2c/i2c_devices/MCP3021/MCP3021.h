//
// Created by fedal on 2/11/20.
//

#ifndef JULIA_ANDROID_MCP3021_H
#define JULIA_ANDROID_MCP3021_H
#define MCP3021_ADDRESS 0x4B
uint16_t readMCP3021(void);
#endif //JULIA_ANDROID_MCP3021_H
