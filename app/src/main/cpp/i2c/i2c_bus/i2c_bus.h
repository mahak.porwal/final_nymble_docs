//
// Created by fedal on 7/11/20.
//

#ifndef JULIA_ANDROID_I2C_BUS_H
#define JULIA_ANDROID_I2C_BUS_H

#include "../i2c_driver/i2c_driver.hpp"


#define MAX_DEVICES_ON_BUS 10
//class i2c_bus {
//    i2c_bus(char* filename) {
//        this->fileDescriptor = i2c_open(filename);
//        if (pthread_mutex_init(&this->busLock, NULL) != 0) {
//            __android_log_print(ANDROID_LOG_ERROR, "Chef ", "\n Chef mutex init has failed\n");
//        }
//    }
//private:
//    uint8_t slaveAddress;
//    bool errorFlag;
//    int fileDescriptor;
//    pthread_mutex_t busLock;
//    uint8_t availableAddresses[MAX_DEVICES_ON_BUS];
//    uint8_t numberOfAvailableDevices;
//public:
//    enum Action{
//        READ,
//        WRITE
//    };
//    int scan_bus();
//    int engage_bus(Action action,uint8_t slaveAddress,int numberOfBytes,uint8_t* array);
//    int getFileDescriptor();
//};


#endif //JULIA_ANDROID_I2C_BUS_H
