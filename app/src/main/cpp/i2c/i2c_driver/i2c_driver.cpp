//
// Created by fedal on 29/10/20.
//


#include <unistd.h>
#include "i2c_driver.hpp"
int file_descriptor_i2c2 = -1;
int file_descriptor_i2c3 = -1;
int i2c_open(char* filename){
    int file = open(filename,O_RDWR);
    __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","opened file %s with %d",filename,file);
    return file;
}
uint8_t i2c_scan(int fileDescriptor,uint8_t* array){
    uint8_t devicesFound = 0;
    __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","I2C scan started");
    for(uint8_t i = 0;i<128;i++){
        int detection = i2c_detect(fileDescriptor,i);
        if(detection>=0){
            *(array+(++devicesFound)) = i;
            __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","Device 0x%02X found",i);
        }
    }
    __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","I2C scan finished");
    return devicesFound;
}
int i2c_detect(int file,uint8_t slaveAddress){

    int link = ioctl(file,I2C_SLAVE,slaveAddress);
    //__android_log_print(ANDROID_LOG_ERROR,"i2c-driver","link resulted %d",link);
    struct i2c_smbus_ioctl_data args;
    __s32 err;

    args.read_write = I2C_SMBUS_WRITE;
    args.command = 0;
    args.size = I2C_SMBUS_QUICK;
    args.data = NULL;

    err = ioctl(file, I2C_SMBUS, &args);
    //__android_log_print(ANDROID_LOG_ERROR,"i2c-driver","Device detection %d",err);
    uint8_t array[1] = {0};
    //write(file,array,0);
    return err;
}

int i2c_write(int fileDescriptor,uint8_t slaveAddress,int numberOfBytes,uint8_t* array){
    /**
     * Link the file descriptor with the slave address
     */
     int linking = ioctl(fileDescriptor,I2C_SLAVE,slaveAddress);
     if(linking < 0){
         __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","failed to detect %d",slaveAddress);
         return linking;
     }

     /**
      * Slave address was acknowledged and the fileDescriptor was linked successfully with the slave
      */

     int bytesWritten = write(fileDescriptor,array,numberOfBytes);
     if(bytesWritten < 0){
         __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","failed to write 0x%02X",slaveAddress);
     }
     else{
         __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","written %d bytes",bytesWritten);
     }

     return bytesWritten;
}

int i2c_read(int fileDescriptor,uint8_t slaveAddress,int numberOfBytes,uint8_t* array){

    /**
     * Link the file descriptor with the slave address
     */
    int linking = ioctl(fileDescriptor,I2C_SLAVE,slaveAddress);
    if(linking < 0){
        __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","failed to detect %d",slaveAddress);
        return linking;
    }

    /**
     * Slave address was acknowledged and the fileDescriptor was linked successfully with the slave
     */

    int bytesRead = read(fileDescriptor,array,numberOfBytes);
    if(bytesRead < 0){
        __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","failed to read");
    }
    else{
        __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","Read %d bytes",bytesRead);
    }

    return numberOfBytes;
}

int i2c_read_from_reg(int fileDesc,uint8_t slaveAddress,uint8_t registerAddress,uint8_t* bufArray,uint16_t len)
{
    uint8_t register_address[1];
    struct i2c_rdwr_ioctl_data iic_data;
    register_address[0]=registerAddress;
    struct i2c_msg msgs[2] = {
            { .addr = slaveAddress, .flags = 0, .len = 1, .buf = register_address},
            { .addr = slaveAddress,
                    .flags = I2C_M_RD,
                    .len = len,
                    .buf = bufArray},
    };


    /** Prepare I2C transfer structure */
    memset(&iic_data, 0, sizeof(struct i2c_rdwr_ioctl_data));
    iic_data.msgs = msgs;
    iic_data.nmsgs = 2;




    int ret=ioctl(fileDesc,I2C_RDWR,&iic_data);
    if(ret<0){
        __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","failed to read");
    }
    else{
        __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","Read %d bytes",ret);
    }
    usleep(3000);
    return ret;
}
