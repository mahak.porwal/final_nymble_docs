//
// Created by fedal on 29/10/20.
//

#ifndef JULIA_ANDROID_I2C_DRIVER_HPP
#define JULIA_ANDROID_I2C_DRIVER_HPP
#include<sys/ioctl.h>
#include<linux/i2c.h>
#include<linux/i2c-dev.h>
#include <stdint.h>
#include <fcntl.h>
extern int file_descriptor_i2c2;
extern int file_descriptor_i2c3;
int i2c_open(char* filename);
uint8_t i2c_scan(int fileDescriptor,uint8_t* array);
int i2c_detect(int fileDescriptor,uint8_t slaveAddress);
int i2c_write(int fileDescriptor,uint8_t slaveAddress,int numberOfBytes,uint8_t* array);
int i2c_read(int fileDescriptor,uint8_t slaveAddress,int numberOfBytes,uint8_t* array);
int i2c_read_from_reg(int fileDesc,uint8_t slaveAddress,uint8_t registerAddress,uint8_t* bufArray,uint16_t len);
void i2c_test();
//class i2cBus {
//private:
//    pthread_mutex_t busLock;
//    int fileDescriptor;
//    bool errorFlag;
//    uint8_t currentSlaveAddress;
//    uint8_t errorSlaveAddress;
//
//public:
//    enum Action{
//        READ,
//        WRITE
//    };
//
//    i2cBus(char* filename){
//
//        this->fileDescriptor = i2c_open(filename);
//
//        if (pthread_mutex_init(&this->busLock, NULL) != 0) {
//            __android_log_print(ANDROID_LOG_ERROR,"Chef ","\n Chef mutex init has failed\n");
//            //printf("\n Chef mutex init has failed\n");
//        }
//    }
//
//    int getFileDescriptor(){
//        return fileDescriptor;
//    }
//
//    int engage_bus(Action action,uint8_t slaveAddress,int numberOfBytes,uint8_t* array){
//        int mutex_result = pthread_mutex_lock(&this->busLock);
//        this->currentSlaveAddress = slaveAddress;
//        __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","\n Chef mutex started %d\n",mutex_result);
//        int toreturn;
//        switch (action){
//            case READ:
//                toreturn = i2c_read(this->fileDescriptor,this->currentSlaveAddress,numberOfBytes,array);
//                break;
//            case WRITE:
//                toreturn = i2c_write(this->fileDescriptor,this->currentSlaveAddress,numberOfBytes,array);
//                break;
//        }
//        __android_log_print(ANDROID_LOG_ERROR,"i2c-driver","\n Chef mutex done %d\n",mutex_result);
//        mutex_result = pthread_mutex_unlock(&this->busLock);
//        return toreturn;
//    }
//};

#endif //JULIA_ANDROID_I2C_DRIVER_HPP
