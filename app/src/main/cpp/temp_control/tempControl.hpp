#ifndef TEMP_CONTROL_DEF
#define TEMP_CONTROL_DEF

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include<opencv2/opencv.hpp>

#define ROOM_TEMPERATURE 31
#define POSITIVE_WINDOW_SIZE 50
#define NEGATIVE_WINDOW_SIZE -50
#define WINDOW 50

using namespace std::chrono;

typedef enum {
    DEFAULT_STATE = 0,
    STANDBY = 1,
    INITIALIZE_INDUCTION_HEATER = 2,
    TARGET_TEMP_NORMALIZATION = 3,
    RECIPE_TEMP_INSTRUCTION = 4,
    DEINITIALIZE_INDUCTION_HEATER = 5,

} temp_state_e;


int16_t pidLoop(struct PID);

int getInductionPower(int16_t, int, bool);

int reachTarget(int power);

int increasePower(int, bool *);

int pidParameterPreset(struct PID *);

bool tempControlSystem(int, uint32_t, float, float);

#endif
