//
// Created by Abarajithan on 2019-11-21.
//

#include<stdint.h>

/**
 * A structure to represent PID variables
 */
struct PID {
    /*@{*/
    float kp; /**< the Proportional constant Kp */
    float ki; /**< the Integral constant Ki */
    float kd; /**< the Derivative constant Kd */
    float error; /**< the PID Error   */
    float last_error; /**< the PID derivative error   */
    float target_value;  /**< the PID target value   */
    float current_value; /**< the PID current value   */
    float derivative; /**< the PID derivative cumulative value  */
    float proportional; /**< the PID proportional cumulative value  */
    float integral; /**< the PID integral cumulative value  */
    int16_t output; /**< the PID output  */
    /*@}*/
};
