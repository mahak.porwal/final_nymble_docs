#include <iostream>
#include <chrono>
#include <ctime>
#include"tempControl.hpp"
#include"../food_detect/foodDetect.hpp"
#include"pid.h"

#define __CHRONO__

volatile uint32_t curr_time = 0, curr_time_1 = 0;
std::chrono::time_point<std::chrono::system_clock> start;
std::chrono::time_point<std::chrono::system_clock> endTime;

struct PID temperature_control_pid;
volatile int16_t pid_output, pid_out_prev;
static int cooking_speed = 1;

/*
*	Function to control the observed temperature to the target temperature for a specified amount of time
*	params: target temperature, time period to maintain it, observed temperature, max observerd temp, image to write and record the instructions to.
*	ret: boolian value indicating the success of operation.
*/
bool
tempControlSystem(int target_temperature, uint32_t timeperiod_sec, float food_temp, float max_t) {
    static bool success = false, pid_preset = false, reached_target = false;
    static temp_state_e temp_state = STANDBY;
    static int induction_power = 0;

    std::chrono::duration<double> time_elapsed;
    if (!pid_preset) {
        pid_preset = true;
        pidParameterPreset(&temperature_control_pid);
    }
    temperature_control_pid.target_value = target_temperature;
    temperature_control_pid.current_value = food_temp;

    switch (temp_state) {
        case STANDBY:
            temp_state = INITIALIZE_INDUCTION_HEATER;
            break;

        case INITIALIZE_INDUCTION_HEATER:
            pid_output = pidLoop(temperature_control_pid);
            induction_power = getInductionPower(pid_output, induction_power, reached_target);
            temp_state = TARGET_TEMP_NORMALIZATION;
            break;

        case TARGET_TEMP_NORMALIZATION:
            pid_out_prev = pid_output;
            pid_output = pidLoop(temperature_control_pid);
            induction_power = getInductionPower(pid_output, induction_power, reached_target);

            if (food_temp > target_temperature) {
#ifndef __CHRONO__
                curr_time = std::chrono::system_clock::now();
#else
                start = std::chrono::system_clock::now();
#endif
                reached_target = true;
                temp_state = RECIPE_TEMP_INSTRUCTION;
            }
            break;

        case RECIPE_TEMP_INSTRUCTION:
            pid_output = pidLoop(temperature_control_pid);
#ifndef __CHRONO__
            curr_time_1 = std::chrono::system_clock::now();
#else
            endTime = std::chrono::system_clock::now();
            time_elapsed = endTime - start;
#endif
            induction_power = getInductionPower(pid_output, induction_power, reached_target);
#ifndef __CHRONO__
        if((curr_time_1-curr_time) > timeperiod_sec)
            {
#else
            if (std::chrono::duration<double>(endTime - start).count() > timeperiod_sec)
#endif
            {
#ifndef __CHRONO__
                curr_time = std::chrono::system_clock::now();
#endif
                temp_state = DEINITIALIZE_INDUCTION_HEATER;
            }
            break;

        case DEINITIALIZE_INDUCTION_HEATER:
            success = true;
            reached_target = false;
            temp_state = STANDBY;
            break;

        default:
            break;
    }
    if (success) {
        success = false;
        return true;
    } else
        return false;
}


/*
 * 	Function to make induction power from the pid_output
 */
int getInductionPower(int16_t pid, int power, bool reached_target) {
    int output;
    static bool increase_power_flag = false;
    if (!reached_target) {
        if (reachTarget(power) && !increase_power_flag) {
            increase_power_flag = true;
        }
    }

    if (increase_power_flag) {
        output = increasePower(power, &increase_power_flag);
        return output;
    }

    if (power == 0) {
        if ((pid - WINDOW) < 0) {
            output = 0;
            return output;
        } else
            goto COMMON;
    } else if ((pid + WINDOW) >= power && pid < (power + 200)) {
        output = power;
        return output;
    } else
        goto COMMON;

    COMMON:

    if (pid >= 1500)
        output = 1500;
    else if (pid >= 1300)
        output = 1300;
    else if (pid >= 1000)
        output = 1000;
    else if (pid >= 800)
        output = 800;
    else if (pid >= 600)
        output = 600;
    else if (pid >= 400)
        output = 600;
    else if ((pid + WINDOW) >= 0)
        output = 300;
    else if ((pid + WINDOW) <= 0)
        output = 0;

    return output;
}


int reachTarget(int power) {
    static std::chrono::time_point<std::chrono::system_clock> timer1, timer2;
    static bool start_timer_flag = false;
    int increase_power_signal = 1;
    if (power == 300) {
        if (!start_timer_flag) {
            start_timer_flag = true;
            timer1 = std::chrono::system_clock::now();
        } else if (start_timer_flag) {
            timer2 = std::chrono::system_clock::now();
        }
        if ((timer2 - timer1).count() > 180000) {
            start_timer_flag = false;
            return increase_power_signal;
        }
    }
    return 0;
}

int increasePower(int power, bool *increase_power_flag) {
    static std::chrono::time_point<std::chrono::system_clock> timer1, timer2;
    static bool inc_power_flag = false;
    if (!inc_power_flag) {
        power = 800;
        inc_power_flag = true;
        timer1 = std::chrono::system_clock::now();
    } else {
        timer2 = std::chrono::system_clock::now();
    }

    if ((timer2 - timer1).count() > 30000) {
        inc_power_flag = false;
        *increase_power_flag = false;
        power = 300;
    }
    return power;
}


/*
*	Function to preset the pid paramter for the control loop.
*	parmas: struct PID structure pointer.
*/
int pidParameterPreset(struct PID *local_pid_constant) {
    // TODO: get cooking speed from recipe.
    if (cooking_speed == 1) {
        local_pid_constant->kp = 15;
        local_pid_constant->ki = 3;
        local_pid_constant->kd = 0;
    } else if (cooking_speed == 2) {
        local_pid_constant->kp = 22;
        local_pid_constant->ki = 0;
        local_pid_constant->kd = 0;
    } else if (cooking_speed == 3) {
        local_pid_constant->kp = 35;
        local_pid_constant->ki = 3;
        local_pid_constant->kd = 0;
    }
    return cooking_speed;
}

/*
*	PID control function
*	parmas: struct PID instance
*	ret: pid_output 16bit signed integer type
*/
int16_t pidLoop(struct PID local_pid) {
    local_pid.error = local_pid.target_value - local_pid.current_value;
    local_pid.proportional = local_pid.error * local_pid.kp;
    local_pid.integral += local_pid.error;
    local_pid.derivative = local_pid.error - local_pid.last_error;
    local_pid.output = (int16_t) (local_pid.proportional + (local_pid.ki) * (local_pid.integral) +
                                  (local_pid.kd) * (local_pid.derivative));
    local_pid.last_error = local_pid.error;

    return local_pid.output;
}


