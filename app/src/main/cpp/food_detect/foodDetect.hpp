/*
*	This files is responsible for all the operation being done on the temperature matrix data
*	like creating histograms, detecting food objects
*/

#ifndef FOOD_DETECT_DEF
#define FOOD_DETECT_DEF

#include <iostream>
#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/shm.h>

#include<opencv2/opencv.hpp>
#include<opencv2/imgproc.hpp>
#include<opencv2/highgui.hpp>
#include<opencv2/imgcodecs.hpp>
#include<opencv2/videoio.hpp>
#include <vector>
#include<numeric>


#define KMEANS_FRAME_SIZE 100
#define FRAME_SIZE 400

#define FONT_SCALE 0.5
#define FONT_THICKNESS 2

typedef struct {
    double kMean;
    int total_points;
    unsigned long long total_val;
} kMean;
extern double mean, standardDeviation;

extern int lastTemperature;

extern cv::Mat img_gray, thermalImage;
extern int foodPixels;
extern cv::Mat croppedThermal;
/**
 * Creates a thermal image from the thermal matrix of dimensions 32X32
 * @param temp Flatted thermal matrix
 */
void CreateThermalImage(double temp_matrix[1024]);

bool getImage(long iaddr);

void PopulateThermalImage(long address);

bool sanityCheck(cv::Mat *maskPtr, int whichPixels);

void calcStandardDeviation(uint8_t[KMEANS_FRAME_SIZE][KMEANS_FRAME_SIZE], int whichPixels);

int filterAndCalculateMean(cv::Mat *, int);

std::vector<int> initializeMeans(int k, cv::Mat dataPoints, cv::Mat *maskPtr, int whichPixels);

void resetkMeanArray(kMean * kMeanArray, int
numberOfMeans);
void resetkMean(kMean * kMeanArray, int
numberOfMeans);

int argMin(double *array, int length);


std::vector <size_t> smallestMean(kMean * array, int
length,
int *index
);
#endif
