#include"foodDetect.hpp"
#include <android/log.h>
/*
*	Function to start detecting food in the temperature matrix, This function does the following operations
*	 1. Convert temperature data to cv::Mat
*	 2. Normalize
*	 3. Call histograms display
*	 4. Resize
*	 5. GaussianBlur
*	 6. Apllying color map
*	 7. Applying sobel filters
*	 8. Getting the magnitude from the filtered outputs.
*	 9. Thresholding the magnitude
*	 10. Applying canny edge detection
*	 11. Find contours
*	 12. Find Moments
*	 13. Get the valid food points
*	 14. Writing the points to temperature image
*	 15. Averaging out the temperature of collected points
*	 16. return mean value
*/
uint8_t arr[1000];
uint8_t arr_length = 0;
#define YCHANGE 0.7
#define XCHANGE 1
#define MEANS 3
int lastTemperature = 22;

cv::Mat img_gray, thermalImage;
cv::Mat image(KMEANS_FRAME_SIZE, KMEANS_FRAME_SIZE, CV_8UC3);
cv::Mat imageToShow(FRAME_SIZE, FRAME_SIZE, CV_8UC3);
int foodPixels = 0;
cv::Mat croppedThermal;
double mean, standardDeviation;

void CreateThermalImage(double temp_matrix[1024]) {

    //Create a 32X32 float matrix
    img_gray = cv::Mat(32, 32, CV_64F);
    //Copy the temp array into this matrix
    std::memcpy(img_gray.data, temp_matrix, 32 * 32 * sizeof(double));
    //Convert the floating 32X32 matrix into uint8_t 32X32 matrix
    img_gray.convertTo(img_gray, CV_16UC1);
    //Normalize the 32X32 matrix and store the result into thermalImage
    cv::normalize(img_gray, croppedThermal, 0, 255, cv::NORM_MINMAX, CV_8UC1);
    //Resize normalized thermalImage into 400X400
    cv::resize(croppedThermal, croppedThermal, cv::Size(FRAME_SIZE, FRAME_SIZE), 0, 0);
    //Resize the temp matrix img_gray into 100X100 for kmeans++
    cv::resize(img_gray, img_gray, cv::Size(KMEANS_FRAME_SIZE, KMEANS_FRAME_SIZE), 0, 0);
    //rotating thermal image 90 deg clockwise
    cv::rotate(img_gray, img_gray, cv::ROTATE_90_CLOCKWISE);

    /*cv::Rect myRoi(0,12,100-12,100-12);
    cv::Rect mythermalRoi(0,49,400-49,400-49);
    img_gray = img_gray(myRoi);
    thermalImage = croppedThermal(mythermalRoi);*/
    croppedThermal.copyTo(thermalImage);
    //cv::resize(img_gray, img_gray, cv::Size(FRAME_SIZE, FRAME_SIZE),0,0);
    //__android_log_print(ANDROID_LOG_ERROR,"Vision","mean calculated %d",img_gray.type());
    //Resize normalized thermalImage into 400X400
    cv::resize(thermalImage, thermalImage, cv::Size(FRAME_SIZE, FRAME_SIZE), 0, 0);
    //Apply colormap to the thermalImage for enhanced visual understanding
    cv::applyColorMap(thermalImage, thermalImage, cv::COLORMAP_JET);

    cv::rotate(thermalImage, thermalImage, cv::ROTATE_90_CLOCKWISE);
}

bool sanityCheck(cv::Mat *maskPtr, int whichPixels) {

    unsigned long long numOfPixels = 0;

    for (int i = 0; i < maskPtr->rows; i++) {
        for (int j = 0; j < maskPtr->cols; j++) {
            if (maskPtr->at<uchar>(i, j) == whichPixels) {
                numOfPixels++;
            }
        }
    }
    if (numOfPixels == 0)
        return false;
    else
        return true;
}

/**
 * Calculates a cumulative temperature of all corresponding pixels in thermalImage which
 * matches whichPixels in maskPtr
 * @param maskPtr A mask of pixels indicating which pixels need to be considered for calculating
 * the cumulative temperature
 * @param whichPixels A value for using the maskPtr
 * @return cumulative temperature
 */
int CalculateTemperature(cv::Mat *maskPtr, int whichPixels) {

    if (!sanityCheck(maskPtr, whichPixels)) {
        return lastTemperature;
    }

    std::vector<int> v;
    v = initializeMeans(MEANS, img_gray, maskPtr, whichPixels);

    double *kMeans = (double *) malloc(sizeof(double) * MEANS);//free
    double *last_kMeans = (double *) calloc(MEANS, sizeof(double));//free

    uint8_t assignmentMatrix[KMEANS_FRAME_SIZE][KMEANS_FRAME_SIZE];

    kMean *kMeanArray = (kMean *) malloc(sizeof(kMean) * MEANS);
    resetkMeanArray(kMeanArray, MEANS);

    for (int i = 0; i < MEANS; i++) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef init means : ", " %d : %d ",
                            i, v.at(i));
        kMeans[i] = v.at(i);
        kMeanArray[i].kMean = kMeans[i];
    }

    for (int k = 0; k < 100; k++) {

        resetkMean(kMeanArray, MEANS);

        //Assign data points to groups
        for (int i = 0; i < KMEANS_FRAME_SIZE; i++) {
            for (int j = 0; j < KMEANS_FRAME_SIZE; j++) {
                if (maskPtr->at<uchar>(i, j) == whichPixels) {
                    double *distanceFromMean = (double *) malloc(sizeof(double) * MEANS);
                    for (int l = 0; l < MEANS; l++) {
                        //distanceFromMean[l] = floor(sqrt((img_gray.at<double>(i,j) - kMeans[l])*(img_gray.at<double>(i,j) - kMeans[l])));
                        distanceFromMean[l] = (img_gray.at<ushort>(i, j) - kMeans[l]) *
                                              (img_gray.at<ushort>(i, j) - kMeans[l]);
                    }
                    assignmentMatrix[i][j] = argMin(distanceFromMean, MEANS);
                    free(distanceFromMean);
                } else {
                    assignmentMatrix[i][j] = 4;
                }
                /*  double * distanceFromMean = (double*)malloc(sizeof(double)*numberOfMeans);
                      for(int l = 0;l<numberOfMeans;l++){
                          //distanceFromMean[l] = floor(sqrt((img_gray.at<double>(i,j) - kMeans[l])*(img_gray.at<double>(i,j) - kMeans[l])));
                          distanceFromMean[l] = (img_gray.at<uchar>(i,j) - kMeans[l])*(img_gray.at<uchar>(i,j) - kMeans[l]);
                      }
                      assignmentMatrix[i][j] = argMin(distanceFromMean,numberOfMeans);
                     free(distanceFromMean);*/
            }
        }


        //Calculate means for the groups
        for (int i = 0; i < KMEANS_FRAME_SIZE; i++) {
            for (int j = 0; j < KMEANS_FRAME_SIZE; j++) {
                if (assignmentMatrix[i][j] != 4) {
                    uint8_t val = img_gray.at<ushort>(i, j);
                    // //__android_log_print(ANDROID_LOG_ERROR,"seg val","%d -> %llu",assignmentMatrix[i][j],val);
                    kMeanArray[assignmentMatrix[i][j]].total_val += val;
                    kMeanArray[assignmentMatrix[i][j]].total_points++;
                }
            }
        }

        bool kMeansSaturated = true;

        for (int i = 0; i < MEANS; i++) {

            if (kMeanArray[i].total_points != 0) {
                kMeanArray[i].kMean = ((kMeanArray[i].total_val / kMeanArray[i].total_points));
            }

            kMeans[i] = kMeanArray[i].kMean;

            if (kMeans[i] != last_kMeans[i]) {
                kMeansSaturated = false;
                last_kMeans[i] = kMeans[i];
            }

            //__android_log_print(ANDROID_LOG_ERROR,"seg ","%d mean %f",i,kMeanArray[i].kMean);
            //__android_log_print(ANDROID_LOG_ERROR,"seg ","%d total points %d",i,kMeanArray[i].total_points);
            //__android_log_print(ANDROID_LOG_ERROR,"seg ","%d total val %f",i,kMeanArray[i].total_val);
            /*std::cout<<i<<" mean            "<<kMeanArray[i].kMean<<std::endl;
            std::cout<<i<<" total points    "<<kMeanArray[i].total_points<<std::endl;
            std::cout<<i<<" total_val       "<<kMeanArray[i].total_val<<std::endl;
            std::cout<< "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<std::endl;*/
            std::cout << i << " mean            " << kMeanArray[i].kMean << std::endl;
            std::cout << i << " total points    " << kMeanArray[i].total_points << std::endl;
            std::cout << i << " total_val       " << kMeanArray[i].total_val << std::endl;
        }


        if (kMeansSaturated) {
            //__android_log_print(ANDROID_LOG_ERROR,"seg ","break %d",k);
            std::cout << " total iteration " << k << std::endl;
            break;
        }


    }

    int leastMean = 0;
    //int temperature = smallestMean(kMeanArray,numberOfMeans,&leastMean);
    std::vector <size_t> indices;
    indices = smallestMean(kMeanArray, MEANS, &leastMean);

    double temperature; //= kMeanArray[indices.at(0)].kMean;

    for (int i = 0; i < MEANS; i++) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef : ", " %d : %f ",
                            i, kMeanArray[indices.at(i)].kMean);
    }

    for (int i = 0; i < MEANS; i++) {
        if (kMeanArray[indices.at(i)].total_points != 0 && i != 0) {
            temperature = kMeanArray[indices.at(i)].kMean;
            mean = temperature;
            foodPixels = kMeanArray[indices.at(i)].total_points;
            calcStandardDeviation(assignmentMatrix, indices.at(i));
            __android_log_print(ANDROID_LOG_ERROR, "Chef Vision", "sd calculated %f %d",
                                standardDeviation, foodPixels);
            __android_log_print(ANDROID_LOG_ERROR, "learn ", "least mean points %d %d", foodPixels,
                                kMeanArray[indices.at(i)].total_points);
            break;
        }
    }


    free(kMeans);
    free(last_kMeans);
    free(kMeanArray);

    lastTemperature = (int) temperature;
    return (int) temperature;


    unsigned long long sum = 0;
    unsigned long long numOfPixels = 0;
    double average = 0;

    for (int i = 0; i < maskPtr->rows; i++) {
        for (int j = 0; j < maskPtr->cols; j++) {
            if (maskPtr->at<uchar>(i, j) == whichPixels) {
                sum += img_gray.at<ushort>(i, j);
                numOfPixels++;
            }
        }
    }
    average = sum / numOfPixels;
    mean = average;
    __android_log_print(ANDROID_LOG_ERROR, "Vision", "mean calculated %f", mean);
    //calcStandardDeviation(maskPtr,whichPixels);
    __android_log_print(ANDROID_LOG_ERROR, "Vision", "sd calculated %f", standardDeviation);
    int filteredMean = filterAndCalculateMean(maskPtr, whichPixels);
    __android_log_print(ANDROID_LOG_ERROR, "Vision", "filtered mean calculated %d", filteredMean);
    return filteredMean;
}

double GetTemperatureSD() { return standardDeviation; }

void calcStandardDeviation(uint8_t mask[KMEANS_FRAME_SIZE][KMEANS_FRAME_SIZE], int whichPixels) {
    double sum = 0;
    unsigned long long numOfPixels = 0;
    for (int i = 0; i < img_gray.rows; i++) {
        for (int j = 0; j < img_gray.cols; j++) {
            if (mask[i][j] == whichPixels) {
                sum += pow((img_gray.at<ushort>(i, j) - (ushort) mean), 2);
                numOfPixels++;
            }
        }
    }
    __android_log_print(ANDROID_LOG_ERROR, "Vision", "food points %d", (int) numOfPixels);
    sum /= numOfPixels;
    sum = sqrt(sum);
    standardDeviation = sum;
}

int filterAndCalculateMean(cv::Mat *maskPtr, int whichPixels) {
    unsigned long long sum = 0;
    unsigned long long numOfPixels = 0;
    for (int i = 0; i < maskPtr->rows; i++) {
        for (int j = 0; j < maskPtr->cols; j++) {
            int pointValue = img_gray.at<ushort>(i, j);
            int pointMaskValue = maskPtr->at<uchar>(i, j);
            if (pointMaskValue == whichPixels &&
                (pointValue > mean - 3 * standardDeviation &&
                 pointValue < mean + 3 * standardDeviation)) {
                sum += pointValue;
                numOfPixels++;
            }
        }
    }

    __android_log_print(ANDROID_LOG_ERROR, "Chef Vision", "food  filtered points %d",
                        (int) numOfPixels);

    sum /= numOfPixels;
    return sum;
}

std::vector<int> initializeMeans(int k, cv::Mat dataPoints, cv::Mat *maskPtr, int whichPixels) {

    std::vector<int> centroids;

    bool done = false;

    for (int i = 0; i < dataPoints.rows; i++) {
        for (int j = 0; j < dataPoints.cols; j++) {
            if (maskPtr->at<uchar>(i, j) == whichPixels) {
                centroids.insert(centroids.end(), dataPoints.at<ushort>(i, j));
                done = true;
                break;
            }
            if (done)
                break;
        }
    }

    for (uint8_t kmean = 1; kmean < k; kmean++) {
        //for every data pouint8_t
        //find the closest centroid and square the distance between them and store in squreDistance
        std::vector<int> distanceFromClosestCentroid(dataPoints.cols * dataPoints.cols, 0);
        //distanceFromClosestCentroid.reserve(160000);
        for (int i = 0; i < dataPoints.cols; i++) {
            for (int j = 0; j < dataPoints.cols; j++) {
                if (maskPtr->at<uchar>(i, j) == whichPixels) {
                    std::vector<int>::iterator it;
                    std::vector<int> distanceFromCentroids;
                    for (it = centroids.begin(); it != centroids.end(); it++) {
                        uint8_t val = dataPoints.at<ushort>(i, j);
                        distanceFromCentroids.insert(distanceFromCentroids.end(),
                                                     (*it - val) * (*it - val));
                    }
                    distanceFromClosestCentroid.insert(
                            distanceFromClosestCentroid.begin() + dataPoints.cols * i + j,
                            *(std::min_element(distanceFromCentroids.begin(),
                                               distanceFromCentroids.end())));
                    //printf("(%d,%d) ",distanceFromClosestCentroid.at(dataPoints.cols*i+j),dataPoints.at<uint8_t>(i,j));
                    /*iter = (distanceFromClosestCentroid.begin()+ dataPoints.cols*i +j);
                    *iter = *(std::min_element(distanceFromCentroids.begin(),distanceFromCentroids.end()));*/
                }
            }
            //printf("\n");
        }


        int max = *(std::max_element(distanceFromClosestCentroid.begin(),
                                     distanceFromClosestCentroid.end()));
        int c = std::distance(distanceFromClosestCentroid.begin(),
                              std::max_element(distanceFromClosestCentroid.begin(),
                                               distanceFromClosestCentroid.end()));
        int b = c % dataPoints.cols;
        int a = c / dataPoints.cols;
        uint8_t farthestdataPoints = dataPoints.at<ushort>(a, b);
        centroids.insert(centroids.end(), farthestdataPoints);
        //for every pouint8_t in squareDistance , find the pouint8_t which has the max value and store in centroids
        //std::cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<std::endl;
        //std::cout<<max<<" "<<a<<" "<<b<<" "<<centroids.at(kmean)<<std::endl;
        //std::cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<std::endl;
        std::cout << centroids.at(kmean) << std::endl;
    }
    return centroids;
}

std::vector <size_t> ordered(std::vector < double >
const& values) {
std::vector <size_t> indices(values.size());
std::iota(begin(indices), end(indices),
static_cast<size_t>(0));

std::sort(begin(indices), end(indices),
[&](
size_t a, size_t
b){
return values[a] < values[b];
}
);
return
indices;
}

std::vector <size_t> smallestMean(kMean *array, int length, int *index) {
    int l = INT_MAX;
    std::vector<double> data_;
    for (int i = 0; i < length; i++) {
        data_.insert(data_.end(), array[i].kMean);
        /*if(array[i].total_points !=0){
           data_.insert(data_.begin(),array[i].kMean);
       }*/
    }
    return ordered(data_);
}

void resetkMeanArray(kMean *array, int length) {
    for (int i = 0; i < length; i++) {
        array[i].kMean = i + 1;
        array[i].total_points = 0;
        array[i].total_val = 0;
    }
}

void resetkMean(kMean *array, int length) {
    for (int i = 0; i < length; i++) {
        array[i].total_points = 0;
        array[i].total_val = 0;
    }
}
bool getImage(long iaddr) {
    cv::Mat *final_image = (cv::Mat *) iaddr;
    *final_image = imageToShow;
    return true;
}

void PopulateThermalImage(long address) {
    cv::Mat *final_image = (cv::Mat *) address;
    *final_image = thermalImage;
}

int argMin(double *array, int length) {

    double l = 10000000;
    int index = -1;
    for (int i = 0; i < length; i++) {
        //__android_log_print(ANDROID_LOG_ERROR,"seg val","%i -> %f",i,array[i]);
        if (array[i] < l) {
            l = array[i];
            index = i;
        }
    }
    return index;
}
