//
// Created by fedal on 27/08/21.
//

#include "TempParamsCalc.h"

double TempParamsCalc::GetSd() {
    return standardDeviation;
}

jobject TempParamsCalc::CalculateAndGetFoodTempParams(cv::Mat *mask) {
    int food_temperature = this->CalculateAndGetTemp(mask, this->food_pixel_mask_value_);
    double food_standard_deviation = this->GetSd();
    return this->CreateTempParamsObject(food_temperature,food_standard_deviation);
}

jobject TempParamsCalc::CalculateAndGetPanTempParams(cv::Mat *mask) {
    int food_temperature = this->CalculateAndGetTemp(mask, this->pan_pixel_mask_value_);
    double food_standard_deviation = this->GetSd();
    return this->CreateTempParamsObject(food_temperature,food_standard_deviation);
}

int TempParamsCalc::CalculateAndGetTemp(cv::Mat *mask, int whichPixels) {

    if (!sanityCheck(mask, whichPixels)) {
        return lastTemperature;
    }

    std::vector<int> v;
    v = initializeMeans(MEANS, img_gray, mask, whichPixels);

    double *kMeans = (double *) malloc(sizeof(double) * MEANS);//free
    double *last_kMeans = (double *) calloc(MEANS, sizeof(double));//free

    uint8_t assignmentMatrix[KMEANS_FRAME_SIZE][KMEANS_FRAME_SIZE];

    kMean *kMeanArray = (kMean *) malloc(sizeof(kMean) * MEANS);
    resetkMeanArray(kMeanArray, MEANS);

    for (int i = 0; i < MEANS; i++) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef init means : ", " %d : %d ",
                            i, v.at(i));
        kMeans[i] = v.at(i);
        kMeanArray[i].kMean = kMeans[i];
    }

    for (int k = 0; k < 100; k++) {

        resetkMean(kMeanArray, MEANS);

        //Assign data points to groups
        for (int i = 0; i < KMEANS_FRAME_SIZE; i++) {
            for (int j = 0; j < KMEANS_FRAME_SIZE; j++) {
                if (mask->at<uchar>(i, j) == whichPixels) {
                    double *distanceFromMean = (double *) malloc(sizeof(double) * MEANS);
                    for (int l = 0; l < MEANS; l++) {
                        //distanceFromMean[l] = floor(sqrt((img_gray.at<double>(i,j) - kMeans[l])*(img_gray.at<double>(i,j) - kMeans[l])));
                        distanceFromMean[l] = (img_gray.at<ushort>(i, j) - kMeans[l]) *
                                              (img_gray.at<ushort>(i, j) - kMeans[l]);
                    }
                    assignmentMatrix[i][j] = argMin(distanceFromMean, MEANS);
                    free(distanceFromMean);
                } else {
                    assignmentMatrix[i][j] = 4;
                }
                /*  double * distanceFromMean = (double*)malloc(sizeof(double)*numberOfMeans);
                      for(int l = 0;l<numberOfMeans;l++){
                          //distanceFromMean[l] = floor(sqrt((img_gray.at<double>(i,j) - kMeans[l])*(img_gray.at<double>(i,j) - kMeans[l])));
                          distanceFromMean[l] = (img_gray.at<uchar>(i,j) - kMeans[l])*(img_gray.at<uchar>(i,j) - kMeans[l]);
                      }
                      assignmentMatrix[i][j] = argMin(distanceFromMean,numberOfMeans);
                     free(distanceFromMean);*/
            }
        }


        //Calculate means for the groups
        for (int i = 0; i < KMEANS_FRAME_SIZE; i++) {
            for (int j = 0; j < KMEANS_FRAME_SIZE; j++) {
                if (assignmentMatrix[i][j] != 4) {
                    uint8_t val = img_gray.at<ushort>(i, j);
                    // //__android_log_print(ANDROID_LOG_ERROR,"seg val","%d -> %llu",assignmentMatrix[i][j],val);
                    kMeanArray[assignmentMatrix[i][j]].total_val += val;
                    kMeanArray[assignmentMatrix[i][j]].total_points++;
                }
            }
        }

        bool kMeansSaturated = true;

        for (int i = 0; i < MEANS; i++) {

            if (kMeanArray[i].total_points != 0) {
                kMeanArray[i].kMean = ((kMeanArray[i].total_val / kMeanArray[i].total_points));
            }

            kMeans[i] = kMeanArray[i].kMean;

            if (kMeans[i] != last_kMeans[i]) {
                kMeansSaturated = false;
                last_kMeans[i] = kMeans[i];
            }

            //__android_log_print(ANDROID_LOG_ERROR,"seg ","%d mean %f",i,kMeanArray[i].kMean);
            //__android_log_print(ANDROID_LOG_ERROR,"seg ","%d total points %d",i,kMeanArray[i].total_points);
            //__android_log_print(ANDROID_LOG_ERROR,"seg ","%d total val %f",i,kMeanArray[i].total_val);
            /*std::cout<<i<<" mean            "<<kMeanArray[i].kMean<<std::endl;
            std::cout<<i<<" total points    "<<kMeanArray[i].total_points<<std::endl;
            std::cout<<i<<" total_val       "<<kMeanArray[i].total_val<<std::endl;
            std::cout<< "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<std::endl;*/
            std::cout << i << " mean            " << kMeanArray[i].kMean << std::endl;
            std::cout << i << " total points    " << kMeanArray[i].total_points << std::endl;
            std::cout << i << " total_val       " << kMeanArray[i].total_val << std::endl;
        }


        if (kMeansSaturated) {
            //__android_log_print(ANDROID_LOG_ERROR,"seg ","break %d",k);
            std::cout << " total iteration " << k << std::endl;
            break;
        }


    }

    int leastMean = 0;
    //int temperature = smallestMean(kMeanArray,numberOfMeans,&leastMean);
    std::vector<size_t> indices;
    indices = smallestMean(kMeanArray, MEANS, &leastMean);

    double temperature; //= kMeanArray[indices.at(0)].kMean;

    for (int i = 0; i < MEANS; i++) {
        __android_log_print(ANDROID_LOG_ERROR, "Chef : ", " %d : %f ",
                            i, kMeanArray[indices.at(i)].kMean);
    }

    for (int i = 0; i < MEANS; i++) {
        if (kMeanArray[indices.at(i)].total_points != 0 && i != 0) {
            temperature = kMeanArray[indices.at(i)].kMean;
            mean = temperature;
            foodPixels = kMeanArray[indices.at(i)].total_points;
            calcStandardDeviation(assignmentMatrix, indices.at(i));
            __android_log_print(ANDROID_LOG_ERROR, "Chef Vision", "sd calculated %f %d",
                                standardDeviation, foodPixels);
            __android_log_print(ANDROID_LOG_ERROR, "learn ", "least mean points %d %d", foodPixels,
                                kMeanArray[indices.at(i)].total_points);
            break;
        }
    }


    free(kMeans);
    free(last_kMeans);
    free(kMeanArray);

    lastTemperature = (int) temperature;
    return (int) temperature;
}

jobject TempParamsCalc::CreateTempParamsObject(int temperature, double standard_deviation) {
    jclass temp_params_class = this->env->FindClass(
            "com/example/recipeengine/instruction/handler/cook/temperature/TemperatureParams");
    jmethodID constructor = this->env->GetMethodID(temp_params_class, "<init>",
                                                   "(Ljava/lang/Integer;Ljava/lang/Double;)V");
    jclass integer_class = this->env->FindClass("java/lang/Integer");
    jmethodID integer_constructor = this->env->GetMethodID(integer_class, "<init>", "(I)V");
    jobject integer_object = this->env->NewObject(integer_class, integer_constructor,
                                                  temperature);

    jclass double_class = this->env->FindClass("java/lang/Double");
    jmethodID double_constructor = this->env->GetMethodID(double_class, "<init>", "(D)V");
    jobject double_object = this->env->NewObject(double_class, double_constructor,
                                                 standard_deviation);
    jobject temp_params_object = this->env->NewObject(temp_params_class, constructor,
                                                      integer_object, double_object);
    return temp_params_object;
}
