//
// Created by fedal on 27/08/21.
//

#ifndef RECIPEENGINE_TEMPPARAMSCALCFACTORY_H
#define RECIPEENGINE_TEMPPARAMSCALCFACTORY_H

#include "TempParamsCalc.h"

class TempParamsCalcFactory {
private:
    static TempParamsCalc *temp_params_calculator;
public:
    static TempParamsCalc *GetTemperatureParamsCalculator(JNIEnv *env) {
        if (temp_params_calculator == NULL) {
            temp_params_calculator = new TempParamsCalc(env, 127, 50);
        }
        return temp_params_calculator;
    }
};

TempParamsCalc *TempParamsCalcFactory::temp_params_calculator = nullptr;
#endif //RECIPEENGINE_TEMPPARAMSCALCFACTORY_H
