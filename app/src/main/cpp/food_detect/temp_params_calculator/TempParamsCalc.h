//
// Created by fedal on 27/08/21.
//

#ifndef RECIPEENGINE_TEMPPARAMSCALC_H
#define RECIPEENGINE_TEMPPARAMSCALC_H

#include "../foodDetect.hpp"
class TempParamsCalc {

private:
    double standard_deviation_;
    int food_pixel_mask_value_;
    int pan_pixel_mask_value_;
    JNIEnv* env;
    int CalculateAndGetTemp(cv::Mat *mask, int whichPixels);
    double GetSd();
    jobject CreateTempParamsObject(int temperature, double standard_deviation);
public:
    TempParamsCalc(JNIEnv* env,int foodPixelMaskValue, int panPixelMaskValue) {
        this->food_pixel_mask_value_ = foodPixelMaskValue;
        this->pan_pixel_mask_value_ = panPixelMaskValue;
        this->env = env;
    }
    jobject CalculateAndGetFoodTempParams(cv::Mat *mask);
    jobject CalculateAndGetPanTempParams(cv::Mat *mask);
};


#endif //RECIPEENGINE_TEMPPARAMSCALC_H
